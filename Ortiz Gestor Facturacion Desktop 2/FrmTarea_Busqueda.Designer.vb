﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTarea_Busqueda
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTarea_Busqueda))
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.BtnNuevo = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnModificar = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnEliminar = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnSalir = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.BtnExportXls = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnExportPdf = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnExportRtf = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnExportHtml = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.DgvGeneral = New DevExpress.XtraGrid.GridControl()
        Me.VwDgv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DgvGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VwDgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BtnNuevo, Me.BtnExportXls, Me.BtnExportPdf, Me.BtnExportRtf, Me.BtnExportHtml, Me.BtnSalir, Me.BarButtonItem1, Me.BtnModificar, Me.BtnEliminar})
        Me.BarManager1.MaxItemId = 20
        '
        'Bar1
        '
        Me.Bar1.BarName = "Herramientas"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BtnNuevo), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnModificar), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnEliminar), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnSalir)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Herramientas"
        '
        'BtnNuevo
        '
        Me.BtnNuevo.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left
        Me.BtnNuevo.Caption = "Nuevo"
        Me.BtnNuevo.Glyph = CType(resources.GetObject("BtnNuevo.Glyph"), System.Drawing.Image)
        Me.BtnNuevo.Id = 1
        Me.BtnNuevo.ImageIndex = 0
        Me.BtnNuevo.Name = "BtnNuevo"
        Me.BtnNuevo.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnModificar
        '
        Me.BtnModificar.Caption = "Modificar"
        Me.BtnModificar.Glyph = CType(resources.GetObject("BtnModificar.Glyph"), System.Drawing.Image)
        Me.BtnModificar.Id = 17
        Me.BtnModificar.Name = "BtnModificar"
        Me.BtnModificar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnEliminar
        '
        Me.BtnEliminar.Caption = "Eliminar"
        Me.BtnEliminar.Glyph = CType(resources.GetObject("BtnEliminar.Glyph"), System.Drawing.Image)
        Me.BtnEliminar.Id = 18
        Me.BtnEliminar.Name = "BtnEliminar"
        Me.BtnEliminar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnSalir
        '
        Me.BtnSalir.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.BtnSalir.Caption = "Salir"
        Me.BtnSalir.Glyph = CType(resources.GetObject("BtnSalir.Glyph"), System.Drawing.Image)
        Me.BtnSalir.Id = 10
        Me.BtnSalir.ImageIndex = 15
        Me.BtnSalir.Name = "BtnSalir"
        Me.BtnSalir.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(868, 35)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 416)
        Me.barDockControlBottom.Size = New System.Drawing.Size(868, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 35)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 381)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(868, 35)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 381)
        '
        'BtnExportXls
        '
        Me.BtnExportXls.Caption = "Excel"
        Me.BtnExportXls.Id = 4
        Me.BtnExportXls.ImageIndex = 10
        Me.BtnExportXls.Name = "BtnExportXls"
        '
        'BtnExportPdf
        '
        Me.BtnExportPdf.Caption = "Pdf"
        Me.BtnExportPdf.Id = 5
        Me.BtnExportPdf.ImageIndex = 11
        Me.BtnExportPdf.Name = "BtnExportPdf"
        '
        'BtnExportRtf
        '
        Me.BtnExportRtf.Caption = "Rtf"
        Me.BtnExportRtf.Id = 6
        Me.BtnExportRtf.ImageIndex = 12
        Me.BtnExportRtf.Name = "BtnExportRtf"
        '
        'BtnExportHtml
        '
        Me.BtnExportHtml.Caption = "Html"
        Me.BtnExportHtml.Id = 7
        Me.BtnExportHtml.ImageIndex = 13
        Me.BtnExportHtml.Name = "BtnExportHtml"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Cancelar"
        Me.BarButtonItem1.Id = 11
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'DgvGeneral
        '
        Me.DgvGeneral.Cursor = System.Windows.Forms.Cursors.Default
        Me.DgvGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DgvGeneral.Location = New System.Drawing.Point(0, 35)
        Me.DgvGeneral.MainView = Me.VwDgv
        Me.DgvGeneral.Name = "DgvGeneral"
        Me.DgvGeneral.Size = New System.Drawing.Size(868, 381)
        Me.DgvGeneral.TabIndex = 163
        Me.DgvGeneral.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.VwDgv})
        '
        'VwDgv
        '
        Me.VwDgv.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.VwDgv.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.VwDgv.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.VwDgv.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.VwDgv.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.VwDgv.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.VwDgv.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.Empty.BackColor2 = System.Drawing.Color.White
        Me.VwDgv.Appearance.Empty.Options.UseBackColor = True
        Me.VwDgv.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.VwDgv.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.EvenRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.EvenRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.VwDgv.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.VwDgv.Appearance.FilterCloseButton.Options.UseForeColor = True
        Me.VwDgv.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
        Me.VwDgv.Appearance.FilterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FilterPanel.Options.UseBackColor = True
        Me.VwDgv.Appearance.FilterPanel.Options.UseForeColor = True
        Me.VwDgv.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.VwDgv.Appearance.FixedLine.Options.UseBackColor = True
        Me.VwDgv.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
        Me.VwDgv.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.FocusedCell.Options.UseBackColor = True
        Me.VwDgv.Appearance.FocusedCell.Options.UseForeColor = True
        Me.VwDgv.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.VwDgv.Appearance.FocusedRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.FocusedRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FooterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.FooterPanel.Options.UseBackColor = True
        Me.VwDgv.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.VwDgv.Appearance.FooterPanel.Options.UseForeColor = True
        Me.VwDgv.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.GroupButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.GroupButton.Options.UseBackColor = True
        Me.VwDgv.Appearance.GroupButton.Options.UseBorderColor = True
        Me.VwDgv.Appearance.GroupButton.Options.UseForeColor = True
        Me.VwDgv.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.GroupFooter.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.GroupFooter.Options.UseBackColor = True
        Me.VwDgv.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.VwDgv.Appearance.GroupFooter.Options.UseForeColor = True
        Me.VwDgv.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
        Me.VwDgv.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.GroupPanel.Options.UseBackColor = True
        Me.VwDgv.Appearance.GroupPanel.Options.UseForeColor = True
        Me.VwDgv.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.GroupRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.GroupRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.GroupRow.Options.UseBorderColor = True
        Me.VwDgv.Appearance.GroupRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.VwDgv.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.VwDgv.Appearance.HeaderPanel.Options.UseForeColor = True
        Me.VwDgv.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(195, Byte), Integer))
        Me.VwDgv.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.VwDgv.Appearance.HorzLine.Options.UseBackColor = True
        Me.VwDgv.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.VwDgv.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.OddRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.OddRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.VwDgv.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
        Me.VwDgv.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.Preview.Options.UseBackColor = True
        Me.VwDgv.Appearance.Preview.Options.UseFont = True
        Me.VwDgv.Appearance.Preview.Options.UseForeColor = True
        Me.VwDgv.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.VwDgv.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.Row.Options.UseBackColor = True
        Me.VwDgv.Appearance.Row.Options.UseForeColor = True
        Me.VwDgv.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
        Me.VwDgv.Appearance.RowSeparator.Options.UseBackColor = True
        Me.VwDgv.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(107, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(179, Byte), Integer))
        Me.VwDgv.Appearance.SelectedRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.SelectedRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.SelectedRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
        Me.VwDgv.Appearance.TopNewRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.VwDgv.Appearance.VertLine.Options.UseBackColor = True
        Me.VwDgv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10, Me.GridColumn11, Me.GridColumn12, Me.GridColumn13})
        Me.VwDgv.GridControl = Me.DgvGeneral
        Me.VwDgv.Name = "VwDgv"
        Me.VwDgv.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.VwDgv.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.VwDgv.OptionsBehavior.Editable = False
        Me.VwDgv.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.VwDgv.OptionsView.EnableAppearanceEvenRow = True
        Me.VwDgv.OptionsView.EnableAppearanceOddRow = True
        Me.VwDgv.OptionsView.ShowAutoFilterRow = True
        Me.VwDgv.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Descripción"
        Me.GridColumn1.FieldName = "Progra_Descripcion"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 72
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Habilitado"
        Me.GridColumn2.FieldName = "Progra_Habilitado"
        Me.GridColumn2.MaxWidth = 40
        Me.GridColumn2.MinWidth = 40
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 40
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Tip.Doc."
        Me.GridColumn4.FieldName = "Progra_Appli_TipoDoc"
        Me.GridColumn4.MaxWidth = 50
        Me.GridColumn4.MinWidth = 50
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 2
        Me.GridColumn4.Width = 50
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Por Serie"
        Me.GridColumn5.FieldName = "Progra_Appli_Filtra_Serie"
        Me.GridColumn5.MaxWidth = 40
        Me.GridColumn5.MinWidth = 40
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 3
        Me.GridColumn5.Width = 40
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Serie"
        Me.GridColumn6.FieldName = "Progra_Appli_Serie"
        Me.GridColumn6.MaxWidth = 40
        Me.GridColumn6.MinWidth = 40
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 4
        Me.GridColumn6.Width = 40
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Es Resumen Boletas"
        Me.GridColumn7.FieldName = "Progra_EsResumenBoletas"
        Me.GridColumn7.MaxWidth = 60
        Me.GridColumn7.MinWidth = 60
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 5
        Me.GridColumn7.Width = 60
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Es Comunicación de Bajas"
        Me.GridColumn8.FieldName = "Progra_EsComunicacionBaja"
        Me.GridColumn8.MaxWidth = 60
        Me.GridColumn8.MinWidth = 60
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 6
        Me.GridColumn8.Width = 60
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Hora Desde"
        Me.GridColumn9.FieldName = "Progra_FrecuEnvio_Hora_Desde_12"
        Me.GridColumn9.MaxWidth = 80
        Me.GridColumn9.MinWidth = 80
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 7
        Me.GridColumn9.Width = 80
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Hora Hasta"
        Me.GridColumn10.FieldName = "Progra_FrecuEnvio_Hora_Hasta_12"
        Me.GridColumn10.MaxWidth = 80
        Me.GridColumn10.MinWidth = 80
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 8
        Me.GridColumn10.Width = 80
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "p/c Hora"
        Me.GridColumn11.FieldName = "Progra_FrecuEnvio_Hora_XCadaHora"
        Me.GridColumn11.MaxWidth = 40
        Me.GridColumn11.MinWidth = 40
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 9
        Me.GridColumn11.Width = 40
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "F. Desde"
        Me.GridColumn12.FieldName = "Progra_Fecha_Desde"
        Me.GridColumn12.MaxWidth = 70
        Me.GridColumn12.MinWidth = 70
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 10
        Me.GridColumn12.Width = 70
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "F. Hasta"
        Me.GridColumn13.FieldName = "Progra_Fecha_Hasta_Str"
        Me.GridColumn13.MaxWidth = 70
        Me.GridColumn13.MinWidth = 70
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 11
        Me.GridColumn13.Width = 70
        '
        'FrmTarea_Busqueda
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(868, 416)
        Me.Controls.Add(Me.DgvGeneral)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "FrmTarea_Busqueda"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Programación de Envíos"
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DgvGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VwDgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BtnNuevo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnModificar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnEliminar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnSalir As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BtnExportXls As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnExportPdf As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnExportRtf As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnExportHtml As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents DgvGeneral As DevExpress.XtraGrid.GridControl
    Public WithEvents VwDgv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
End Class
