﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEmpresa_Edicion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmEmpresa_Edicion))
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.BtnGuardar = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnSalir = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.BtnExportXls = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnExportPdf = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnExportRtf = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnExportHtml = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TxtRutaXMLCliente = New dhsoft.TextBoxNew(Me.components)
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TxtEtapaDescripcion = New dhsoft.TextBoxNew(Me.components)
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TxtEtapaCodigo = New dhsoft.TextBoxNew(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TxtEmpRazonSocial = New dhsoft.TextBoxNew(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TxtEmpRUC = New dhsoft.TextBoxNew(Me.components)
        Me.ChkActualizarReceptores = New dhsoft.CheckBoxNew()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.TxtSUNATWebConsulta = New dhsoft.TextBoxNew(Me.components)
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TxtSUNATAutorizacion = New dhsoft.TextBoxNew(Me.components)
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TxtSUNATContrasena = New dhsoft.TextBoxNew(Me.components)
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TxtSUNATUsuario = New dhsoft.TextBoxNew(Me.components)
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.ChkCorreoSSLHabilitado = New dhsoft.CheckBoxNew()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TxtCorreoPuerto = New dhsoft.TextBoxNew(Me.components)
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TxtCorreoServidorSMTP = New dhsoft.TextBoxNew(Me.components)
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TxtCorreoContrasena = New dhsoft.TextBoxNew(Me.components)
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TxtCorreoUsuario = New dhsoft.TextBoxNew(Me.components)
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TxtFTPContrasena = New dhsoft.TextBoxNew(Me.components)
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TxtFTPUsuario = New dhsoft.TextBoxNew(Me.components)
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TxtFTPCarpeta = New dhsoft.TextBoxNew(Me.components)
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TxtFTPDireccion = New dhsoft.TextBoxNew(Me.components)
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.XtraTabPage2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.XtraTabPage3.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.SuspendLayout()
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BtnExportXls, Me.BtnExportPdf, Me.BtnExportRtf, Me.BtnExportHtml, Me.BtnSalir, Me.BarButtonItem1, Me.BtnGuardar})
        Me.BarManager1.MaxItemId = 19
        '
        'Bar1
        '
        Me.Bar1.BarName = "Herramientas"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BtnGuardar), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnSalir)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Herramientas"
        '
        'BtnGuardar
        '
        Me.BtnGuardar.Caption = "Guardar"
        Me.BtnGuardar.Glyph = CType(resources.GetObject("BtnGuardar.Glyph"), System.Drawing.Image)
        Me.BtnGuardar.Id = 18
        Me.BtnGuardar.Name = "BtnGuardar"
        Me.BtnGuardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnSalir
        '
        Me.BtnSalir.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.BtnSalir.Caption = "Cancelar"
        Me.BtnSalir.Glyph = CType(resources.GetObject("BtnSalir.Glyph"), System.Drawing.Image)
        Me.BtnSalir.Id = 10
        Me.BtnSalir.ImageIndex = 15
        Me.BtnSalir.Name = "BtnSalir"
        Me.BtnSalir.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(360, 35)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 411)
        Me.barDockControlBottom.Size = New System.Drawing.Size(360, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 35)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 376)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(360, 35)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 376)
        '
        'BtnExportXls
        '
        Me.BtnExportXls.Caption = "Excel"
        Me.BtnExportXls.Id = 4
        Me.BtnExportXls.ImageIndex = 10
        Me.BtnExportXls.Name = "BtnExportXls"
        '
        'BtnExportPdf
        '
        Me.BtnExportPdf.Caption = "Pdf"
        Me.BtnExportPdf.Id = 5
        Me.BtnExportPdf.ImageIndex = 11
        Me.BtnExportPdf.Name = "BtnExportPdf"
        '
        'BtnExportRtf
        '
        Me.BtnExportRtf.Caption = "Rtf"
        Me.BtnExportRtf.Id = 6
        Me.BtnExportRtf.ImageIndex = 12
        Me.BtnExportRtf.Name = "BtnExportRtf"
        '
        'BtnExportHtml
        '
        Me.BtnExportHtml.Caption = "Html"
        Me.BtnExportHtml.Id = 7
        Me.BtnExportHtml.ImageIndex = 13
        Me.BtnExportHtml.Name = "BtnExportHtml"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Cancelar"
        Me.BarButtonItem1.Id = 11
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TxtRutaXMLCliente)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.TxtEtapaDescripcion)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.TxtEtapaCodigo)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TxtEmpRazonSocial)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.TxtEmpRUC)
        Me.GroupBox1.Controls.Add(Me.ChkActualizarReceptores)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 41)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(334, 179)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'TxtRutaXMLCliente
        '
        Me.TxtRutaXMLCliente.BackColor = System.Drawing.Color.White
        Me.TxtRutaXMLCliente.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtRutaXMLCliente.DecimalPrecision = 0
        Me.TxtRutaXMLCliente.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtRutaXMLCliente.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtRutaXMLCliente.Enganche = Nothing
        Me.TxtRutaXMLCliente.EnterEmuleTab = True
        Me.TxtRutaXMLCliente.IsDecimalNegative = False
        Me.TxtRutaXMLCliente.LinkKeyDown = Nothing
        Me.TxtRutaXMLCliente.Location = New System.Drawing.Point(82, 89)
        Me.TxtRutaXMLCliente.MaskFormat = Nothing
        Me.TxtRutaXMLCliente.Name = "TxtRutaXMLCliente"
        Me.TxtRutaXMLCliente.Requiere = False
        Me.TxtRutaXMLCliente.Size = New System.Drawing.Size(244, 20)
        Me.TxtRutaXMLCliente.TabIndex = 16
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(5, 92)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(87, 13)
        Me.Label18.TabIndex = 15
        Me.Label18.Text = "Carpeta Principal"
        '
        'TxtEtapaDescripcion
        '
        Me.TxtEtapaDescripcion.BackColor = System.Drawing.Color.White
        Me.TxtEtapaDescripcion.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtEtapaDescripcion.DecimalPrecision = 0
        Me.TxtEtapaDescripcion.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtEtapaDescripcion.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtEtapaDescripcion.Enabled = False
        Me.TxtEtapaDescripcion.Enganche = Nothing
        Me.TxtEtapaDescripcion.EnterEmuleTab = True
        Me.TxtEtapaDescripcion.IsDecimalNegative = False
        Me.TxtEtapaDescripcion.LinkKeyDown = Nothing
        Me.TxtEtapaDescripcion.Location = New System.Drawing.Point(111, 63)
        Me.TxtEtapaDescripcion.MaskFormat = Nothing
        Me.TxtEtapaDescripcion.Name = "TxtEtapaDescripcion"
        Me.TxtEtapaDescripcion.Requiere = True
        Me.TxtEtapaDescripcion.Size = New System.Drawing.Size(215, 20)
        Me.TxtEtapaDescripcion.TabIndex = 14
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(5, 66)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(35, 13)
        Me.Label11.TabIndex = 12
        Me.Label11.Text = "Etapa"
        '
        'TxtEtapaCodigo
        '
        Me.TxtEtapaCodigo.BackColor = System.Drawing.Color.White
        Me.TxtEtapaCodigo.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtEtapaCodigo.DecimalPrecision = 0
        Me.TxtEtapaCodigo.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtEtapaCodigo.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtEtapaCodigo.Enganche = Me.TxtEtapaDescripcion
        Me.TxtEtapaCodigo.EnterEmuleTab = True
        Me.TxtEtapaCodigo.IsDecimalNegative = False
        Me.TxtEtapaCodigo.LinkKeyDown = Nothing
        Me.TxtEtapaCodigo.Location = New System.Drawing.Point(82, 63)
        Me.TxtEtapaCodigo.MaskFormat = Nothing
        Me.TxtEtapaCodigo.Name = "TxtEtapaCodigo"
        Me.TxtEtapaCodigo.Requiere = False
        Me.TxtEtapaCodigo.Size = New System.Drawing.Size(28, 20)
        Me.TxtEtapaCodigo.TabIndex = 13
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(5, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Razón Social"
        '
        'TxtEmpRazonSocial
        '
        Me.TxtEmpRazonSocial.BackColor = System.Drawing.Color.White
        Me.TxtEmpRazonSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtEmpRazonSocial.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtEmpRazonSocial.DecimalPrecision = 0
        Me.TxtEmpRazonSocial.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtEmpRazonSocial.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtEmpRazonSocial.Enganche = Nothing
        Me.TxtEmpRazonSocial.EnterEmuleTab = True
        Me.TxtEmpRazonSocial.IsDecimalNegative = False
        Me.TxtEmpRazonSocial.LinkKeyDown = Nothing
        Me.TxtEmpRazonSocial.Location = New System.Drawing.Point(82, 37)
        Me.TxtEmpRazonSocial.MaskFormat = Nothing
        Me.TxtEmpRazonSocial.Name = "TxtEmpRazonSocial"
        Me.TxtEmpRazonSocial.Requiere = False
        Me.TxtEmpRazonSocial.Size = New System.Drawing.Size(244, 20)
        Me.TxtEmpRazonSocial.TabIndex = 10
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Núm. RUC"
        '
        'TxtEmpRUC
        '
        Me.TxtEmpRUC.BackColor = System.Drawing.Color.White
        Me.TxtEmpRUC.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtEmpRUC.DecimalPrecision = 0
        Me.TxtEmpRUC.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtEmpRUC.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtEmpRUC.Enganche = Nothing
        Me.TxtEmpRUC.EnterEmuleTab = True
        Me.TxtEmpRUC.IsDecimalNegative = False
        Me.TxtEmpRUC.LinkKeyDown = Nothing
        Me.TxtEmpRUC.Location = New System.Drawing.Point(82, 14)
        Me.TxtEmpRUC.MaskFormat = Nothing
        Me.TxtEmpRUC.Name = "TxtEmpRUC"
        Me.TxtEmpRUC.Requiere = False
        Me.TxtEmpRUC.Size = New System.Drawing.Size(244, 20)
        Me.TxtEmpRUC.TabIndex = 8
        '
        'ChkActualizarReceptores
        '
        Me.ChkActualizarReceptores.AutoSize = True
        Me.ChkActualizarReceptores.Location = New System.Drawing.Point(8, 115)
        Me.ChkActualizarReceptores.Name = "ChkActualizarReceptores"
        Me.ChkActualizarReceptores.Size = New System.Drawing.Size(173, 17)
        Me.ChkActualizarReceptores.TabIndex = 21
        Me.ChkActualizarReceptores.Text = "Actualizar correo de receptores"
        Me.ChkActualizarReceptores.UseVisualStyleBackColor = True
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Location = New System.Drawing.Point(9, 226)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(348, 180)
        Me.XtraTabControl1.TabIndex = 5
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.GroupBox3)
        Me.XtraTabPage1.Controls.Add(Me.GroupBox2)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(342, 152)
        Me.XtraTabPage1.Text = "SUNAT"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.TxtSUNATWebConsulta)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.TxtSUNATAutorizacion)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 73)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(325, 71)
        Me.GroupBox3.TabIndex = 13
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Impresión"
        '
        'TxtSUNATWebConsulta
        '
        Me.TxtSUNATWebConsulta.BackColor = System.Drawing.Color.White
        Me.TxtSUNATWebConsulta.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtSUNATWebConsulta.DecimalPrecision = 0
        Me.TxtSUNATWebConsulta.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtSUNATWebConsulta.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtSUNATWebConsulta.Enganche = Nothing
        Me.TxtSUNATWebConsulta.EnterEmuleTab = True
        Me.TxtSUNATWebConsulta.IsDecimalNegative = False
        Me.TxtSUNATWebConsulta.LinkKeyDown = Nothing
        Me.TxtSUNATWebConsulta.Location = New System.Drawing.Point(79, 42)
        Me.TxtSUNATWebConsulta.MaskFormat = Nothing
        Me.TxtSUNATWebConsulta.Name = "TxtSUNATWebConsulta"
        Me.TxtSUNATWebConsulta.Requiere = False
        Me.TxtSUNATWebConsulta.Size = New System.Drawing.Size(235, 20)
        Me.TxtSUNATWebConsulta.TabIndex = 14
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 45)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(74, 13)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Web Consulta"
        '
        'TxtSUNATAutorizacion
        '
        Me.TxtSUNATAutorizacion.BackColor = System.Drawing.Color.White
        Me.TxtSUNATAutorizacion.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtSUNATAutorizacion.DecimalPrecision = 0
        Me.TxtSUNATAutorizacion.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtSUNATAutorizacion.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtSUNATAutorizacion.Enganche = Nothing
        Me.TxtSUNATAutorizacion.EnterEmuleTab = True
        Me.TxtSUNATAutorizacion.IsDecimalNegative = False
        Me.TxtSUNATAutorizacion.LinkKeyDown = Nothing
        Me.TxtSUNATAutorizacion.Location = New System.Drawing.Point(79, 19)
        Me.TxtSUNATAutorizacion.MaskFormat = Nothing
        Me.TxtSUNATAutorizacion.Name = "TxtSUNATAutorizacion"
        Me.TxtSUNATAutorizacion.Requiere = False
        Me.TxtSUNATAutorizacion.Size = New System.Drawing.Size(235, 20)
        Me.TxtSUNATAutorizacion.TabIndex = 12
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 22)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(65, 13)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Autorización"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.TxtSUNATContrasena)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.TxtSUNATUsuario)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(325, 64)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Credenciales"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Contraseña"
        '
        'TxtSUNATContrasena
        '
        Me.TxtSUNATContrasena.BackColor = System.Drawing.Color.White
        Me.TxtSUNATContrasena.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtSUNATContrasena.DecimalPrecision = 0
        Me.TxtSUNATContrasena.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtSUNATContrasena.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtSUNATContrasena.Enganche = Nothing
        Me.TxtSUNATContrasena.EnterEmuleTab = True
        Me.TxtSUNATContrasena.IsDecimalNegative = False
        Me.TxtSUNATContrasena.LinkKeyDown = Nothing
        Me.TxtSUNATContrasena.Location = New System.Drawing.Point(79, 37)
        Me.TxtSUNATContrasena.MaskFormat = Nothing
        Me.TxtSUNATContrasena.Name = "TxtSUNATContrasena"
        Me.TxtSUNATContrasena.Requiere = False
        Me.TxtSUNATContrasena.Size = New System.Drawing.Size(235, 20)
        Me.TxtSUNATContrasena.TabIndex = 10
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 17)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Usuario"
        '
        'TxtSUNATUsuario
        '
        Me.TxtSUNATUsuario.BackColor = System.Drawing.Color.White
        Me.TxtSUNATUsuario.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtSUNATUsuario.DecimalPrecision = 0
        Me.TxtSUNATUsuario.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtSUNATUsuario.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtSUNATUsuario.Enganche = Nothing
        Me.TxtSUNATUsuario.EnterEmuleTab = True
        Me.TxtSUNATUsuario.IsDecimalNegative = False
        Me.TxtSUNATUsuario.LinkKeyDown = Nothing
        Me.TxtSUNATUsuario.Location = New System.Drawing.Point(79, 14)
        Me.TxtSUNATUsuario.MaskFormat = Nothing
        Me.TxtSUNATUsuario.Name = "TxtSUNATUsuario"
        Me.TxtSUNATUsuario.Requiere = False
        Me.TxtSUNATUsuario.Size = New System.Drawing.Size(235, 20)
        Me.TxtSUNATUsuario.TabIndex = 8
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.GroupBox4)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(342, 152)
        Me.XtraTabPage2.Text = "Correo"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.ChkCorreoSSLHabilitado)
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Controls.Add(Me.TxtCorreoPuerto)
        Me.GroupBox4.Controls.Add(Me.Label9)
        Me.GroupBox4.Controls.Add(Me.TxtCorreoServidorSMTP)
        Me.GroupBox4.Controls.Add(Me.Label8)
        Me.GroupBox4.Controls.Add(Me.TxtCorreoContrasena)
        Me.GroupBox4.Controls.Add(Me.Label7)
        Me.GroupBox4.Controls.Add(Me.TxtCorreoUsuario)
        Me.GroupBox4.Location = New System.Drawing.Point(7, 3)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(325, 110)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        '
        'ChkCorreoSSLHabilitado
        '
        Me.ChkCorreoSSLHabilitado.AutoSize = True
        Me.ChkCorreoSSLHabilitado.Location = New System.Drawing.Point(126, 85)
        Me.ChkCorreoSSLHabilitado.Name = "ChkCorreoSSLHabilitado"
        Me.ChkCorreoSSLHabilitado.Size = New System.Drawing.Size(84, 17)
        Me.ChkCorreoSSLHabilitado.TabIndex = 17
        Me.ChkCorreoSSLHabilitado.Text = "Habilita SSL"
        Me.ChkCorreoSSLHabilitado.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(8, 86)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(38, 13)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "Puerto"
        '
        'TxtCorreoPuerto
        '
        Me.TxtCorreoPuerto.BackColor = System.Drawing.Color.White
        Me.TxtCorreoPuerto.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtCorreoPuerto.DecimalPrecision = 0
        Me.TxtCorreoPuerto.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtCorreoPuerto.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtCorreoPuerto.Enganche = Nothing
        Me.TxtCorreoPuerto.EnterEmuleTab = True
        Me.TxtCorreoPuerto.IsDecimalNegative = False
        Me.TxtCorreoPuerto.LinkKeyDown = Nothing
        Me.TxtCorreoPuerto.Location = New System.Drawing.Point(71, 83)
        Me.TxtCorreoPuerto.MaskFormat = Nothing
        Me.TxtCorreoPuerto.Name = "TxtCorreoPuerto"
        Me.TxtCorreoPuerto.Requiere = False
        Me.TxtCorreoPuerto.Size = New System.Drawing.Size(49, 20)
        Me.TxtCorreoPuerto.TabIndex = 16
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 63)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(79, 13)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "Servidor SMTP"
        '
        'TxtCorreoServidorSMTP
        '
        Me.TxtCorreoServidorSMTP.BackColor = System.Drawing.Color.White
        Me.TxtCorreoServidorSMTP.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtCorreoServidorSMTP.DecimalPrecision = 0
        Me.TxtCorreoServidorSMTP.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtCorreoServidorSMTP.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtCorreoServidorSMTP.Enganche = Nothing
        Me.TxtCorreoServidorSMTP.EnterEmuleTab = True
        Me.TxtCorreoServidorSMTP.IsDecimalNegative = False
        Me.TxtCorreoServidorSMTP.LinkKeyDown = Nothing
        Me.TxtCorreoServidorSMTP.Location = New System.Drawing.Point(93, 60)
        Me.TxtCorreoServidorSMTP.MaskFormat = Nothing
        Me.TxtCorreoServidorSMTP.Name = "TxtCorreoServidorSMTP"
        Me.TxtCorreoServidorSMTP.Requiere = False
        Me.TxtCorreoServidorSMTP.Size = New System.Drawing.Size(222, 20)
        Me.TxtCorreoServidorSMTP.TabIndex = 14
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 40)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(61, 13)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "Contraseña"
        '
        'TxtCorreoContrasena
        '
        Me.TxtCorreoContrasena.BackColor = System.Drawing.Color.White
        Me.TxtCorreoContrasena.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtCorreoContrasena.DecimalPrecision = 0
        Me.TxtCorreoContrasena.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtCorreoContrasena.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtCorreoContrasena.Enganche = Nothing
        Me.TxtCorreoContrasena.EnterEmuleTab = True
        Me.TxtCorreoContrasena.IsDecimalNegative = False
        Me.TxtCorreoContrasena.LinkKeyDown = Nothing
        Me.TxtCorreoContrasena.Location = New System.Drawing.Point(71, 37)
        Me.TxtCorreoContrasena.MaskFormat = Nothing
        Me.TxtCorreoContrasena.Name = "TxtCorreoContrasena"
        Me.TxtCorreoContrasena.Requiere = False
        Me.TxtCorreoContrasena.Size = New System.Drawing.Size(244, 20)
        Me.TxtCorreoContrasena.TabIndex = 12
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 17)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(43, 13)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Usuario"
        '
        'TxtCorreoUsuario
        '
        Me.TxtCorreoUsuario.BackColor = System.Drawing.Color.White
        Me.TxtCorreoUsuario.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtCorreoUsuario.DecimalPrecision = 0
        Me.TxtCorreoUsuario.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtCorreoUsuario.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtCorreoUsuario.Enganche = Nothing
        Me.TxtCorreoUsuario.EnterEmuleTab = True
        Me.TxtCorreoUsuario.IsDecimalNegative = False
        Me.TxtCorreoUsuario.LinkKeyDown = Nothing
        Me.TxtCorreoUsuario.Location = New System.Drawing.Point(71, 14)
        Me.TxtCorreoUsuario.MaskFormat = Nothing
        Me.TxtCorreoUsuario.Name = "TxtCorreoUsuario"
        Me.TxtCorreoUsuario.Requiere = False
        Me.TxtCorreoUsuario.Size = New System.Drawing.Size(244, 20)
        Me.TxtCorreoUsuario.TabIndex = 10
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.GroupBox5)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(342, 152)
        Me.XtraTabPage3.Text = "FTP"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label16)
        Me.GroupBox5.Controls.Add(Me.TxtFTPContrasena)
        Me.GroupBox5.Controls.Add(Me.Label17)
        Me.GroupBox5.Controls.Add(Me.TxtFTPUsuario)
        Me.GroupBox5.Controls.Add(Me.Label14)
        Me.GroupBox5.Controls.Add(Me.TxtFTPCarpeta)
        Me.GroupBox5.Controls.Add(Me.Label15)
        Me.GroupBox5.Controls.Add(Me.TxtFTPDireccion)
        Me.GroupBox5.Location = New System.Drawing.Point(8, 3)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(325, 111)
        Me.GroupBox5.TabIndex = 6
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Credenciales"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(6, 86)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(61, 13)
        Me.Label16.TabIndex = 13
        Me.Label16.Text = "Contraseña"
        '
        'TxtFTPContrasena
        '
        Me.TxtFTPContrasena.BackColor = System.Drawing.Color.White
        Me.TxtFTPContrasena.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtFTPContrasena.DecimalPrecision = 0
        Me.TxtFTPContrasena.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtFTPContrasena.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtFTPContrasena.Enganche = Nothing
        Me.TxtFTPContrasena.EnterEmuleTab = True
        Me.TxtFTPContrasena.IsDecimalNegative = False
        Me.TxtFTPContrasena.LinkKeyDown = Nothing
        Me.TxtFTPContrasena.Location = New System.Drawing.Point(79, 83)
        Me.TxtFTPContrasena.MaskFormat = Nothing
        Me.TxtFTPContrasena.Name = "TxtFTPContrasena"
        Me.TxtFTPContrasena.Requiere = False
        Me.TxtFTPContrasena.Size = New System.Drawing.Size(235, 20)
        Me.TxtFTPContrasena.TabIndex = 14
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(6, 63)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(43, 13)
        Me.Label17.TabIndex = 11
        Me.Label17.Text = "Usuario"
        '
        'TxtFTPUsuario
        '
        Me.TxtFTPUsuario.BackColor = System.Drawing.Color.White
        Me.TxtFTPUsuario.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtFTPUsuario.DecimalPrecision = 0
        Me.TxtFTPUsuario.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtFTPUsuario.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtFTPUsuario.Enganche = Nothing
        Me.TxtFTPUsuario.EnterEmuleTab = True
        Me.TxtFTPUsuario.IsDecimalNegative = False
        Me.TxtFTPUsuario.LinkKeyDown = Nothing
        Me.TxtFTPUsuario.Location = New System.Drawing.Point(79, 60)
        Me.TxtFTPUsuario.MaskFormat = Nothing
        Me.TxtFTPUsuario.Name = "TxtFTPUsuario"
        Me.TxtFTPUsuario.Requiere = False
        Me.TxtFTPUsuario.Size = New System.Drawing.Size(235, 20)
        Me.TxtFTPUsuario.TabIndex = 12
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(6, 40)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(44, 13)
        Me.Label14.TabIndex = 9
        Me.Label14.Text = "Carpeta"
        '
        'TxtFTPCarpeta
        '
        Me.TxtFTPCarpeta.BackColor = System.Drawing.Color.White
        Me.TxtFTPCarpeta.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtFTPCarpeta.DecimalPrecision = 0
        Me.TxtFTPCarpeta.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtFTPCarpeta.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtFTPCarpeta.Enganche = Nothing
        Me.TxtFTPCarpeta.EnterEmuleTab = True
        Me.TxtFTPCarpeta.IsDecimalNegative = False
        Me.TxtFTPCarpeta.LinkKeyDown = Nothing
        Me.TxtFTPCarpeta.Location = New System.Drawing.Point(79, 37)
        Me.TxtFTPCarpeta.MaskFormat = Nothing
        Me.TxtFTPCarpeta.Name = "TxtFTPCarpeta"
        Me.TxtFTPCarpeta.Requiere = False
        Me.TxtFTPCarpeta.Size = New System.Drawing.Size(235, 20)
        Me.TxtFTPCarpeta.TabIndex = 10
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(6, 17)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(52, 13)
        Me.Label15.TabIndex = 7
        Me.Label15.Text = "Dirección"
        '
        'TxtFTPDireccion
        '
        Me.TxtFTPDireccion.BackColor = System.Drawing.Color.White
        Me.TxtFTPDireccion.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtFTPDireccion.DecimalPrecision = 0
        Me.TxtFTPDireccion.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtFTPDireccion.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtFTPDireccion.Enganche = Nothing
        Me.TxtFTPDireccion.EnterEmuleTab = True
        Me.TxtFTPDireccion.IsDecimalNegative = False
        Me.TxtFTPDireccion.LinkKeyDown = Nothing
        Me.TxtFTPDireccion.Location = New System.Drawing.Point(79, 14)
        Me.TxtFTPDireccion.MaskFormat = Nothing
        Me.TxtFTPDireccion.Name = "TxtFTPDireccion"
        Me.TxtFTPDireccion.Requiere = False
        Me.TxtFTPDireccion.Size = New System.Drawing.Size(235, 20)
        Me.TxtFTPDireccion.TabIndex = 8
        '
        'FrmEmpresa_Edicion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(360, 411)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmEmpresa_Edicion"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Empresa Edición"
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.XtraTabPage2.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.XtraTabPage3.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BtnGuardar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnSalir As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BtnExportXls As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnExportPdf As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnExportRtf As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnExportHtml As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TxtEmpRazonSocial As dhsoft.TextBoxNew
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TxtEmpRUC As dhsoft.TextBoxNew
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents TxtSUNATAutorizacion As dhsoft.TextBoxNew
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TxtSUNATContrasena As dhsoft.TextBoxNew
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TxtSUNATUsuario As dhsoft.TextBoxNew
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents TxtSUNATWebConsulta As dhsoft.TextBoxNew
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents ChkCorreoSSLHabilitado As dhsoft.CheckBoxNew
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TxtCorreoPuerto As dhsoft.TextBoxNew
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TxtCorreoServidorSMTP As dhsoft.TextBoxNew
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TxtCorreoContrasena As dhsoft.TextBoxNew
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TxtCorreoUsuario As dhsoft.TextBoxNew
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents ChkActualizarReceptores As dhsoft.CheckBoxNew
    Friend WithEvents TxtEtapaDescripcion As dhsoft.TextBoxNew
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TxtEtapaCodigo As dhsoft.TextBoxNew
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TxtFTPContrasena As dhsoft.TextBoxNew
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TxtFTPUsuario As dhsoft.TextBoxNew
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TxtFTPCarpeta As dhsoft.TextBoxNew
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TxtFTPDireccion As dhsoft.TextBoxNew
    Friend WithEvents TxtRutaXMLCliente As dhsoft.TextBoxNew
    Friend WithEvents Label18 As System.Windows.Forms.Label
End Class
