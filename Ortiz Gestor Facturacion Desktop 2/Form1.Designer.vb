﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CheckBoxNew1 = New dhsoft.CheckBoxNew()
        Me.TextBoxNew1 = New dhsoft.TextBoxNew(Me.components)
        Me.RadioButtonNew1 = New dhsoft.RadioButtonNew()
        Me.DateTimePickerNew1 = New dhsoft.DateTimePickerNew(Me.components)
        Me.TextBoxNew2 = New dhsoft.TextBoxNew(Me.components)
        Me.SuspendLayout()
        '
        'CheckBoxNew1
        '
        Me.CheckBoxNew1.AutoSize = True
        Me.CheckBoxNew1.Location = New System.Drawing.Point(64, 60)
        Me.CheckBoxNew1.Name = "CheckBoxNew1"
        Me.CheckBoxNew1.Size = New System.Drawing.Size(103, 17)
        Me.CheckBoxNew1.TabIndex = 0
        Me.CheckBoxNew1.Text = "CheckBoxNew1"
        Me.CheckBoxNew1.UseVisualStyleBackColor = True
        '
        'TextBoxNew1
        '
        Me.TextBoxNew1.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TextBoxNew1.DecimalPrecision = 0
        Me.TextBoxNew1.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.TextBoxNew1.DisabledForeColor = System.Drawing.Color.Black
        Me.TextBoxNew1.Enganche = Nothing
        Me.TextBoxNew1.EnterEmuleTab = True
        Me.TextBoxNew1.IsDecimalNegative = False
        Me.TextBoxNew1.LinkKeyDown = Nothing
        Me.TextBoxNew1.Location = New System.Drawing.Point(71, 143)
        Me.TextBoxNew1.MaskFormat = Nothing
        Me.TextBoxNew1.Name = "TextBoxNew1"
        Me.TextBoxNew1.Requiere = False
        Me.TextBoxNew1.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxNew1.TabIndex = 1
        '
        'RadioButtonNew1
        '
        Me.RadioButtonNew1.AutoSize = True
        Me.RadioButtonNew1.Location = New System.Drawing.Point(66, 191)
        Me.RadioButtonNew1.Name = "RadioButtonNew1"
        Me.RadioButtonNew1.Size = New System.Drawing.Size(112, 17)
        Me.RadioButtonNew1.TabIndex = 2
        Me.RadioButtonNew1.TabStop = True
        Me.RadioButtonNew1.Text = "RadioButtonNew1"
        Me.RadioButtonNew1.UseVisualStyleBackColor = True
        '
        'DateTimePickerNew1
        '
        Me.DateTimePickerNew1.Location = New System.Drawing.Point(196, 94)
        Me.DateTimePickerNew1.Name = "DateTimePickerNew1"
        Me.DateTimePickerNew1.Size = New System.Drawing.Size(200, 20)
        Me.DateTimePickerNew1.TabIndex = 3
        '
        'TextBoxNew2
        '
        Me.TextBoxNew2.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TextBoxNew2.DecimalPrecision = 0
        Me.TextBoxNew2.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.TextBoxNew2.DisabledForeColor = System.Drawing.Color.Black
        Me.TextBoxNew2.Enganche = Nothing
        Me.TextBoxNew2.EnterEmuleTab = True
        Me.TextBoxNew2.IsDecimalNegative = False
        Me.TextBoxNew2.LinkKeyDown = Nothing
        Me.TextBoxNew2.Location = New System.Drawing.Point(30, 97)
        Me.TextBoxNew2.MaskFormat = Nothing
        Me.TextBoxNew2.Name = "TextBoxNew2"
        Me.TextBoxNew2.Requiere = False
        Me.TextBoxNew2.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxNew2.TabIndex = 4
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.TextBoxNew2)
        Me.Controls.Add(Me.DateTimePickerNew1)
        Me.Controls.Add(Me.RadioButtonNew1)
        Me.Controls.Add(Me.TextBoxNew1)
        Me.Controls.Add(Me.CheckBoxNew1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CheckBoxNew1 As dhsoft.CheckBoxNew
    Friend WithEvents TextBoxNew1 As dhsoft.TextBoxNew
    Friend WithEvents RadioButtonNew1 As dhsoft.RadioButtonNew
    Friend WithEvents DateTimePickerNew1 As dhsoft.DateTimePickerNew
    Friend WithEvents TextBoxNew2 As dhsoft.TextBoxNew
End Class
