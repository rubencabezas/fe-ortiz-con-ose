﻿Public Class FrmVersionesXMLCliente_Visor
    Public FileName As String
    Private Sub FrmVersionesXMLCliente_Visor_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IO.File.Exists(FileName) Then
            Dim VarText As String
            Using streamReader = New IO.StreamReader(FileName, System.Text.Encoding.UTF8)
                VarText = streamReader.ReadToEnd()
            End Using
            RichTextBox1.Text = VarText
        Else
            RichTextBox1.Text = FileName
        End If

    End Sub
End Class