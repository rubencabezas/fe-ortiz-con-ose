﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmBackup_Crear
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TxtFHasta = New dhsoft.MaskedTextBoxNew()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TxtFDesde = New dhsoft.MaskedTextBoxNew()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BtnCrearAhora = New System.Windows.Forms.Button()
        Me.MarqueeProgressBarControl1 = New DevExpress.XtraEditors.MarqueeProgressBarControl()
        Me.GroupBox1.SuspendLayout()
        CType(Me.MarqueeProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TxtFHasta)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.TxtFDesde)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(242, 52)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Crear Backup de documentos emitidos"
        '
        'TxtFHasta
        '
        Me.TxtFHasta.IsValueDate = False
        Me.TxtFHasta.Location = New System.Drawing.Point(163, 19)
        Me.TxtFHasta.Mask = "00/00/0000"
        Me.TxtFHasta.Name = "TxtFHasta"
        Me.TxtFHasta.Requiere = False
        Me.TxtFHasta.Size = New System.Drawing.Size(70, 20)
        Me.TxtFHasta.TabIndex = 8
        Me.TxtFHasta.ValidatingType = GetType(Date)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(122, 22)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Hasta"
        '
        'TxtFDesde
        '
        Me.TxtFDesde.IsValueDate = False
        Me.TxtFDesde.Location = New System.Drawing.Point(46, 19)
        Me.TxtFDesde.Mask = "00/00/0000"
        Me.TxtFDesde.Name = "TxtFDesde"
        Me.TxtFDesde.Requiere = False
        Me.TxtFDesde.Size = New System.Drawing.Size(70, 20)
        Me.TxtFDesde.TabIndex = 6
        Me.TxtFDesde.ValidatingType = GetType(Date)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Desde"
        '
        'BtnCrearAhora
        '
        Me.BtnCrearAhora.Location = New System.Drawing.Point(58, 70)
        Me.BtnCrearAhora.Name = "BtnCrearAhora"
        Me.BtnCrearAhora.Size = New System.Drawing.Size(149, 36)
        Me.BtnCrearAhora.TabIndex = 1
        Me.BtnCrearAhora.Text = "Crear Backup"
        Me.BtnCrearAhora.UseVisualStyleBackColor = True
        '
        'MarqueeProgressBarControl1
        '
        Me.MarqueeProgressBarControl1.EditValue = 0
        Me.MarqueeProgressBarControl1.Location = New System.Drawing.Point(12, 112)
        Me.MarqueeProgressBarControl1.Name = "MarqueeProgressBarControl1"
        Me.MarqueeProgressBarControl1.Properties.MarqueeAnimationSpeed = 50
        Me.MarqueeProgressBarControl1.Properties.ProgressAnimationMode = DevExpress.Utils.Drawing.ProgressAnimationMode.Cycle
        Me.MarqueeProgressBarControl1.Size = New System.Drawing.Size(242, 18)
        Me.MarqueeProgressBarControl1.TabIndex = 2
        Me.MarqueeProgressBarControl1.Visible = False
        '
        'FrmBackup_Crear
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(265, 138)
        Me.Controls.Add(Me.MarqueeProgressBarControl1)
        Me.Controls.Add(Me.BtnCrearAhora)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmBackup_Crear"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Backup"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.MarqueeProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TxtFHasta As dhsoft.MaskedTextBoxNew
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TxtFDesde As dhsoft.MaskedTextBoxNew
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents BtnCrearAhora As System.Windows.Forms.Button
    Friend WithEvents MarqueeProgressBarControl1 As DevExpress.XtraEditors.MarqueeProgressBarControl
End Class
