﻿Public Class FrmBackup

    Private Sub FrmBackup_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub BtnBackupCrear_Click(sender As Object, e As EventArgs) Handles BtnBackupCrear.Click
        Using f As New FrmBackup_Crear
            If f.ShowDialog = DialogResult.OK Then
                MessageBox.Show(DatosActualConexion.Mensaje)
            End If
        End Using
    End Sub

    Private Sub BtnBackupSubir_Click(sender As Object, e As EventArgs) Handles BtnBackupSubir.Click
        Using DlgOpen As New OpenFileDialog
            DlgOpen.Filter = "ZIP files (*.zip)|*.zip"
            If DlgOpen.ShowDialog = DialogResult.OK Then
                Using f As New FrmBackup_SubirData
                    f.PathBackup = DlgOpen.FileName

                    If f.ShowDialog = DialogResult.OK Then
                        MessageBox.Show(DatosActualConexion.Mensaje)
                    End If
                End Using
            End If
        End Using
    End Sub
End Class