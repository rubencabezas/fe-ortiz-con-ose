﻿Imports System.IO
Public Class FrmBackup_SubirData
    Public PathBackup As String
    Dim PathBackup_Descompress As String


    Private Sub FrmBackup_SubirData_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Dim thActualizarTareas As New Threading.Thread(AddressOf Subir_Data)
        thActualizarTareas.Start()

    End Sub
    Sub Subir_Data()
        Try
            If IO.File.Exists(PathBackup) Then
                PathBackup_Descompress = PathBackup.Trim.Substring(0, PathBackup.Trim.Length - 4)

                If IO.Directory.Exists(PathBackup_Descompress) Then
                    IO.Directory.Delete(PathBackup_Descompress, True)
                End If

                Compression.ZipFile.ExtractToDirectory(PathBackup, PathBackup_Descompress)

                Dim Documento As New BACKUP_FACTE

                Dim vfiles As String() = System.IO.Directory.GetFiles(PathBackup_Descompress)

                For Each xfile As String In vfiles
                    If Path.GetFileName(xfile).ToUpper.Contains("FACTE_BCK_") And Path.GetFileName(xfile).ToUpper.Contains(".XML") Then
                        Dim xmlDoc As New Xml.XmlDocument()
                        xmlDoc.PreserveWhitespace = True
                        xmlDoc.Load(xfile)
                        Dim xSeria As New System.Xml.Serialization.XmlSerializer(Documento.[GetType]())
                        Dim xReader As Xml.XmlNodeReader = New Xml.XmlNodeReader(xmlDoc)
                        Documento = DirectCast(xSeria.Deserialize(xReader), BACKUP_FACTE)
                        Exit For
                    End If
                Next

                For Each FileUser In Documento.DocumentosXMLCliente
                    FileUser.Path_Root_App = Ruta_Guardar(FileUser.Doc_Emisor_RUC)
                    If IO.File.Exists(PathBackup_Descompress & "\" & FileUser.XML_Archivo) Then
                        IO.File.Copy(PathBackup_Descompress & "\" & FileUser.XML_Archivo, FileUser.Path_Root_App_Empresa & "\xml_pos\" & FileUser.XML_Archivo, True)
                    End If
                Next
                For Each FileAttach In Documento.DocumentosAdjuntos
                    FileAttach.Path_Root_App = Ruta_Guardar(FileAttach.Doc_Emisor_RUC)
                    If IO.File.Exists(PathBackup_Descompress & "\" & FileAttach.XML_Archivo_NombreLocal) Then
                        IO.File.Copy(PathBackup_Descompress & "\" & FileAttach.XML_Archivo_NombreLocal, FileAttach.Path_Root_App_Empresa & "\xml_pos_attached\" & FileAttach.XML_Archivo_NombreLocal, True)
                    End If
                Next

                For Each FileXMLGen In Documento.Documentos
                    FileXMLGen.Path_Root_App = Ruta_Guardar(FileXMLGen.Doc_Emisor_RUC)
                    If IO.File.Exists(PathBackup_Descompress & "\" & FileXMLGen.Doc_NombreArchivoXML) Then
                        IO.File.Copy(PathBackup_Descompress & "\" & FileXMLGen.Doc_NombreArchivoXML, FileXMLGen.Path_Root_App_Empresa & ServidorRuta_Generado(FileXMLGen.Doc_Tipo) & FileXMLGen.Doc_NombreArchivoXML, True)
                        If FileXMLGen.EstEnv_Codigo = "003" Then
                            IO.File.Copy(PathBackup_Descompress & "\" & FileXMLGen.Doc_NombreArchivoXML, FileXMLGen.Path_Root_App_Empresa & ServidorRuta_Aceptado(FileXMLGen.Doc_Tipo) & FileXMLGen.Doc_NombreArchivoXML, True)
                            If IO.File.Exists(PathBackup_Descompress & "\" & FileXMLGen.Doc_NombreArchivoPDF) Then
                                IO.File.Copy(PathBackup_Descompress & "\" & FileXMLGen.Doc_NombreArchivoPDF, FileXMLGen.Path_Root_App_Empresa & ServidorRuta_Aceptado(FileXMLGen.Doc_Tipo) & FileXMLGen.Doc_NombreArchivoPDF, True)
                            End If
                        End If
                        If FileXMLGen.EstEnv_Codigo = "004" Then
                            IO.File.Copy(PathBackup_Descompress & "\" & FileXMLGen.Doc_NombreArchivoXML, FileXMLGen.Path_Root_App_Empresa & ServidorRuta_Rechazado(FileXMLGen.Doc_Tipo) & FileXMLGen.Doc_NombreArchivoXML, True)
                            'IO.File.Copy(PathBackup_Descompress & "\" & FileXMLGen.Doc_NombreArchivoPDF, FileXMLGen.Path_Root_App_Empresa & "\" & ServidorRuta_Rechazado(FileXMLGen.Doc_Tipo) & FileXMLGen.Doc_NombreArchivoPDF, True)
                        End If
                    End If
                Next

                Dim LogCap As New Log_BACKUP
                If LogCap.RestaurarBackup(Documento) Then
                    If IO.Directory.Exists(PathBackup_Descompress) Then
                        IO.Directory.Delete(PathBackup_Descompress, True)
                    End If
                    DatosActualConexion.Mensaje = "Restauración Correcta"
                    Me.DialogResult = DialogResult.OK

                Else
                    DatosActualConexion.Mensaje = "No se pudo restaurar"
                    Me.DialogResult = DialogResult.OK
                End If
            Else
                DatosActualConexion.Mensaje = "El archivo no existe"
                Me.DialogResult = DialogResult.OK
            End If

        Catch ex As Exception
            DatosActualConexion.Mensaje = "No se pudo restaurar: " & ex.Message
            Me.DialogResult = DialogResult.OK
        End Try
    End Sub
    Function Ruta_Guardar(ByVal RUC As String) As String

        Dim LogCertificado As New Log_CERTIFICADO
        Return LogCertificado.Buscar(RUC.Trim).Path_Root_App
    End Function
    Function ServidorRuta_Generado(DocumentoTipo As String) As String

        'If ResumenDiario Then
        '    Return (ServidorRutaLocal & RutasServidor.ResumenDiario.Generado)
        'End If
        'If ComunicacionBaja Then
        '    Return (ServidorRutaLocal & RutasServidor.ComunicacionBaja.Generado)
        'End If
        Dim var As String = ""
        If DocumentoTipo = SUNATDocumento.Boleta Then
            var = RutasServidor.Boleta.Generado
        ElseIf DocumentoTipo = SUNATDocumento.Factura Then
            var = RutasServidor.Factura.Generado
        ElseIf DocumentoTipo = SUNATDocumento.NotaCredito Then
            var = RutasServidor.NotaCredito.Generado
        ElseIf DocumentoTipo = SUNATDocumento.NotaDebito Then
            var = RutasServidor.NotaDebito.Generado
        ElseIf DocumentoTipo = SUNATDocumento.GuiaRemisionRemitente Then
            var = RutasServidor.GuiaRemisionRemitente.Generado
        ElseIf DocumentoTipo = SUNATDocumento.ComprobantePercepcion Then
            var = RutasServidor.Percepcion.Generado
        ElseIf DocumentoTipo = SUNATDocumento.ComprobanteRetencion Then
            var = RutasServidor.Retencion.Generado
        End If
        Return var

    End Function
    Function ServidorRuta_Aceptado(DocumentoTipo As String) As String
        'Get
        'If ResumenDiario Then
        '    Return (ServidorRutaLocal & RutasServidor.ResumenDiario.Aceptado)
        'End If
        'If ComunicacionBaja Then
        '    Return (ServidorRutaLocal & RutasServidor.ComunicacionBaja.Aceptado)
        'End If
        Dim var As String = ""
        If DocumentoTipo = SUNATDocumento.Boleta Then
            var = RutasServidor.Boleta.Aceptado
        ElseIf DocumentoTipo = SUNATDocumento.Factura Then
            var = RutasServidor.Factura.Aceptado
        ElseIf DocumentoTipo = SUNATDocumento.NotaCredito Then
            var = RutasServidor.NotaCredito.Aceptado
        ElseIf DocumentoTipo = SUNATDocumento.NotaDebito Then
            var = RutasServidor.NotaDebito.Aceptado
        ElseIf DocumentoTipo = SUNATDocumento.GuiaRemisionRemitente Then
            var = RutasServidor.GuiaRemisionRemitente.Aceptado
        ElseIf DocumentoTipo = SUNATDocumento.ComprobantePercepcion Then
            var = RutasServidor.Percepcion.Aceptado
        ElseIf DocumentoTipo = SUNATDocumento.ComprobanteRetencion Then
            var = RutasServidor.Retencion.Aceptado
        End If
        Return var
        'End Get
    End Function
    Function ServidorRuta_Rechazado(DocumentoTipo As String) As String
        'Get
        'If ResumenDiario Then
        '    Return (ServidorRutaLocal & RutasServidor.ResumenDiario.Rechazado)
        'End If
        'If ComunicacionBaja Then
        '    Return (ServidorRutaLocal & RutasServidor.ComunicacionBaja.Rechazado)
        'End If

        Dim var As String = ""
        If DocumentoTipo = SUNATDocumento.Boleta Then
            var = RutasServidor.Boleta.Rechazado
        ElseIf DocumentoTipo = SUNATDocumento.Factura Then
            var = RutasServidor.Factura.Rechazado
        ElseIf DocumentoTipo = SUNATDocumento.NotaCredito Then
            var = RutasServidor.NotaCredito.Rechazado
        ElseIf DocumentoTipo = SUNATDocumento.NotaDebito Then
            var = RutasServidor.NotaDebito.Rechazado
        ElseIf DocumentoTipo = SUNATDocumento.GuiaRemisionRemitente Then
            var = RutasServidor.GuiaRemisionRemitente.Rechazado
        ElseIf DocumentoTipo = SUNATDocumento.ComprobantePercepcion Then
            var = RutasServidor.Percepcion.Rechazado
        ElseIf DocumentoTipo = SUNATDocumento.ComprobanteRetencion Then
            var = RutasServidor.Retencion.Rechazado
        End If
        Return var
        'End Get
    End Function

End Class