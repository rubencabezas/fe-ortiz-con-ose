﻿Imports System.IO.Compression

Public Class FrmBackup_Crear

    Private Sub FrmBackup_Crear_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TxtFDesde.Text = Now.AddDays(-1)
        TxtFHasta.Text = Now
    End Sub

    Sub Generar(ByVal FileName As String)
        Try
            Dim pNombre_Pila As String = "FACTE_BCK_" & CDate(TxtFDesde.Text).Year.ToString & "_" & Format(CDate(TxtFDesde.Text).Month, "00") & "_" & Format(CDate(TxtFDesde.Text).Day, "00") & "_A_" & CDate(TxtFHasta.Text).Year.ToString & "_" & Format(CDate(TxtFHasta.Text).Month, "00") & "_" & Format(CDate(TxtFHasta.Text).Day, "00")
            Dim pNombre_XML As String = pNombre_Pila & ".xml"
            Dim pNombre_ZIP As String = pNombre_Pila & ".zip"

            pNombre_ZIP = FileName 'porsia cambio el nombre al guardar y la ruta
            pNombre_XML = IO.Path.GetDirectoryName(FileName) & "\" & pNombre_XML

            Application.DoEvents()

            'Paso 1: Creo la lista de los registros
            Dim Log As New Log_BACKUP
            Dim Bck As BACKUP_FACTE = Log.Buscar(TxtFDesde.Text, TxtFHasta.Text)

            If IO.File.Exists(pNombre_XML) Then
                IO.File.Delete(pNombre_XML)
            End If

            Dim settings As New Xml.XmlWriterSettings With {.ConformanceLevel = Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}
            Using writer As Xml.XmlWriter = Xml.XmlWriter.Create(pNombre_XML, settings)
                Dim typeToSerialize As Type = GetType(BACKUP_FACTE)
                Dim xs As New Xml.Serialization.XmlSerializer(typeToSerialize)
                xs.Serialize(writer, Bck)
            End Using

            If IO.File.Exists(pNombre_ZIP) Then
                IO.File.Delete(pNombre_ZIP)
            End If

            Using archiveZip As ZipArchive = ZipFile.Open(pNombre_ZIP, ZipArchiveMode.Create)
                ZipFileExtensions.CreateEntryFromFile(archiveZip, pNombre_XML, (pNombre_Pila & ".xml"), CompressionLevel.Optimal)
                IO.File.Delete(pNombre_XML)
                For Each FileUser In Bck.DocumentosXMLCliente
                    If IO.File.Exists(FileUser.Path_Root_App_Empresa & "\xml_pos\" & FileUser.XML_Archivo) Then
                        ZipFileExtensions.CreateEntryFromFile(archiveZip, FileUser.Path_Root_App_Empresa & "\xml_pos\" & FileUser.XML_Archivo, FileUser.XML_Archivo, CompressionLevel.Optimal)
                    End If
                Next
                'Falta Probar
                For Each FileAtachUser In Bck.DocumentosAdjuntos
                    If IO.File.Exists(FileAtachUser.Path_Root_App_Empresa & "\xml_pos_attached\" & FileAtachUser.XML_Archivo_Nombre) Then
                        ZipFileExtensions.CreateEntryFromFile(archiveZip, FileAtachUser.Path_Root_App_Empresa & "\xml_pos\" & FileAtachUser.XML_Archivo_Nombre, FileAtachUser.XML_Archivo_Nombre, CompressionLevel.Optimal)
                    End If
                Next
                'Falta Probar
                For Each FileGenUser In Bck.Documentos
                    ' XML Generado
                    If IO.File.Exists(FileGenUser.Path_Root_App_Empresa & "\" & ServidorRuta_Generado(FileGenUser.Doc_Tipo) & FileGenUser.Doc_NombreArchivoXML) Then
                        ' PDF Aceptado
                        ZipFileExtensions.CreateEntryFromFile(archiveZip, FileGenUser.Path_Root_App_Empresa & "\" & ServidorRuta_Generado(FileGenUser.Doc_Tipo) & FileGenUser.Doc_NombreArchivoXML, FileGenUser.Doc_NombreArchivoXML, CompressionLevel.Optimal)
                        If IO.File.Exists(FileGenUser.Path_Root_App_Empresa & "\" & ServidorRuta_Aceptado(FileGenUser.Doc_Tipo) & FileGenUser.Doc_NombreArchivoPDF) Then
                            ZipFileExtensions.CreateEntryFromFile(archiveZip, FileGenUser.Path_Root_App_Empresa & "\" & ServidorRuta_Aceptado(FileGenUser.Doc_Tipo) & FileGenUser.Doc_NombreArchivoPDF, FileGenUser.Doc_NombreArchivoPDF, CompressionLevel.Optimal)
                        End If
                    End If
                Next
            End Using

            DatosActualConexion.Mensaje = "Backup Creado Correctamente"
            Me.DialogResult = DialogResult.OK

        Catch ex As Exception

        End Try

    End Sub
    Private Sub BtnCrearAhora_Click(sender As Object, e As EventArgs) Handles BtnCrearAhora.Click
        Using SDlg As New SaveFileDialog
            Dim pNombre_Pila As String = "FACTE_BCK_" & CDate(TxtFDesde.Text).Year.ToString & "_" & Format(CDate(TxtFDesde.Text).Month, "00") & "_" & Format(CDate(TxtFDesde.Text).Day, "00") & "_A_" & CDate(TxtFHasta.Text).Year.ToString & "_" & Format(CDate(TxtFHasta.Text).Month, "00") & "_" & Format(CDate(TxtFHasta.Text).Day, "00")
            Dim pNombre_XML As String = pNombre_Pila & ".xml"
            Dim pNombre_ZIP As String = pNombre_Pila & ".zip"
            SDlg.Filter = "ZIP files (*.zip)|*.zip" '"XML files (*.xml)|*.xml|All files (*.*)|*.*"
            SDlg.FileName = pNombre_ZIP
            If SDlg.ShowDialog = DialogResult.OK Then
                pNombre_ZIP = SDlg.FileName  'porsia cambio el nombre al guardar y la ruta
                BtnCrearAhora.Enabled = False
                MarqueeProgressBarControl1.Visible = True
                Dim thActualizarTareas As New Threading.Thread(AddressOf Generar)
                thActualizarTareas.Start(SDlg.FileName)
            End If
        End Using
        

    End Sub
    Function ServidorRuta_Generado(DocumentoTipo As String) As String

        'If ResumenDiario Then
        '    Return (ServidorRutaLocal & RutasServidor.ResumenDiario.Generado)
        'End If
        'If ComunicacionBaja Then
        '    Return (ServidorRutaLocal & RutasServidor.ComunicacionBaja.Generado)
        'End If
        Dim var As String = ""
        If DocumentoTipo = SUNATDocumento.Boleta Then
            var = RutasServidor.Boleta.Generado
        ElseIf DocumentoTipo = SUNATDocumento.Factura Then
            var = RutasServidor.Factura.Generado
        ElseIf DocumentoTipo = SUNATDocumento.NotaCredito Then
            var = RutasServidor.NotaCredito.Generado
        ElseIf DocumentoTipo = SUNATDocumento.NotaDebito Then
            var = RutasServidor.NotaDebito.Generado
        ElseIf DocumentoTipo = SUNATDocumento.GuiaRemisionRemitente Then
            var = RutasServidor.GuiaRemisionRemitente.Generado
        ElseIf DocumentoTipo = SUNATDocumento.ComprobantePercepcion Then
            var = RutasServidor.Percepcion.Generado
        ElseIf DocumentoTipo = SUNATDocumento.ComprobanteRetencion Then
            var = RutasServidor.Retencion.Generado
        End If
        Return var

    End Function
    Function ServidorRuta_Aceptado(DocumentoTipo As String) As String
        'Get
        'If ResumenDiario Then
        '    Return (ServidorRutaLocal & RutasServidor.ResumenDiario.Aceptado)
        'End If
        'If ComunicacionBaja Then
        '    Return (ServidorRutaLocal & RutasServidor.ComunicacionBaja.Aceptado)
        'End If
        Dim var As String = ""
        If DocumentoTipo = SUNATDocumento.Boleta Then
            var = RutasServidor.Boleta.Aceptado
        ElseIf DocumentoTipo = SUNATDocumento.Factura Then
            var = RutasServidor.Factura.Aceptado
        ElseIf DocumentoTipo = SUNATDocumento.NotaCredito Then
            var = RutasServidor.NotaCredito.Aceptado
        ElseIf DocumentoTipo = SUNATDocumento.NotaDebito Then
            var = RutasServidor.NotaDebito.Aceptado
        ElseIf DocumentoTipo = SUNATDocumento.GuiaRemisionRemitente Then
            var = RutasServidor.GuiaRemisionRemitente.Aceptado
        ElseIf DocumentoTipo = SUNATDocumento.ComprobantePercepcion Then
            var = RutasServidor.Percepcion.Aceptado
        ElseIf DocumentoTipo = SUNATDocumento.ComprobanteRetencion Then
            var = RutasServidor.Retencion.Aceptado
        End If
        Return var
        'End Get
    End Function
    'Function ServidorRuta_Rechazado(DocumentoTipo As String) As String
    '    'Get
    '    'If ResumenDiario Then
    '    '    Return (ServidorRutaLocal & RutasServidor.ResumenDiario.Rechazado)
    '    'End If
    '    'If ComunicacionBaja Then
    '    '    Return (ServidorRutaLocal & RutasServidor.ComunicacionBaja.Rechazado)
    '    'End If

    '    Dim var As String = ""
    '    If DocumentoTipo = SUNATDocumento.Boleta Then
    '        var = RutasServidor.Boleta.Rechazado
    '    ElseIf DocumentoTipo = SUNATDocumento.Factura Then
    '        var = RutasServidor.Factura.Rechazado
    '    ElseIf DocumentoTipo = SUNATDocumento.NotaCredito Then
    '        var = RutasServidor.NotaCredito.Rechazado
    '    ElseIf DocumentoTipo = SUNATDocumento.NotaDebito Then
    '        var = RutasServidor.NotaDebito.Rechazado
    '    ElseIf DocumentoTipo = SUNATDocumento.GuiaRemisionRemitente Then
    '        var = RutasServidor.GuiaRemisionRemitente.Rechazado
    '    ElseIf DocumentoTipo = SUNATDocumento.ComprobantePercepcion Then
    '        var = RutasServidor.Percepcion.Rechazado
    '    ElseIf DocumentoTipo = SUNATDocumento.ComprobanteRetencion Then
    '        var = RutasServidor.Retencion.Rechazado
    '    End If
    '    Return var
    '    'End Get
    'End Function
End Class