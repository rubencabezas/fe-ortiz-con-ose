﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmReplicacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ChkReplicar = New dhsoft.CheckBoxNew()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TxtRepliBD = New dhsoft.TextBoxNew(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TxtRepliContrasena = New dhsoft.TextBoxNew(Me.components)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TxtRepliUsuario = New dhsoft.TextBoxNew(Me.components)
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TxtRepliServidor = New dhsoft.TextBoxNew(Me.components)
        Me.BtnProbarConexion = New System.Windows.Forms.Button()
        Me.BtnGuardar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TxtHora_Hasta2 = New DevExpress.XtraEditors.TimeEdit()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TxtHora_Desde2 = New DevExpress.XtraEditors.TimeEdit()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TxtHora_Desde1 = New DevExpress.XtraEditors.TimeEdit()
        Me.RbnHora_Es_XCadaHora = New dhsoft.RadioButtonNew()
        Me.RbnHora_Es_Establecido = New dhsoft.RadioButtonNew()
        Me.TxtServidorArchivos = New dhsoft.TextBoxNew(Me.components)
        Me.CheckBoxNew1 = New dhsoft.CheckBoxNew()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.TxtHora_Hasta2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtHora_Desde2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtHora_Desde1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ChkReplicar
        '
        Me.ChkReplicar.AutoSize = True
        Me.ChkReplicar.Location = New System.Drawing.Point(12, 12)
        Me.ChkReplicar.Name = "ChkReplicar"
        Me.ChkReplicar.Size = New System.Drawing.Size(156, 17)
        Me.ChkReplicar.TabIndex = 1
        Me.ChkReplicar.Text = "Replicar a servidor principal"
        Me.ChkReplicar.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.TxtRepliBD)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.TxtRepliContrasena)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.TxtRepliUsuario)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TxtRepliServidor)
        Me.GroupBox1.Enabled = False
        Me.GroupBox1.Location = New System.Drawing.Point(12, 35)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(208, 118)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Servidor principal"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(11, 45)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(77, 13)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "Base de Datos"
        '
        'TxtRepliBD
        '
        Me.TxtRepliBD.BackColor = System.Drawing.Color.White
        Me.TxtRepliBD.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtRepliBD.DecimalPrecision = 0
        Me.TxtRepliBD.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtRepliBD.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtRepliBD.Enganche = Nothing
        Me.TxtRepliBD.EnterEmuleTab = True
        Me.TxtRepliBD.IsDecimalNegative = False
        Me.TxtRepliBD.LinkKeyDown = Nothing
        Me.TxtRepliBD.Location = New System.Drawing.Point(88, 42)
        Me.TxtRepliBD.MaskFormat = Nothing
        Me.TxtRepliBD.Name = "TxtRepliBD"
        Me.TxtRepliBD.Requiere = False
        Me.TxtRepliBD.Size = New System.Drawing.Size(111, 20)
        Me.TxtRepliBD.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 91)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Contraseña"
        '
        'TxtRepliContrasena
        '
        Me.TxtRepliContrasena.BackColor = System.Drawing.Color.White
        Me.TxtRepliContrasena.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtRepliContrasena.DecimalPrecision = 0
        Me.TxtRepliContrasena.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtRepliContrasena.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtRepliContrasena.Enganche = Nothing
        Me.TxtRepliContrasena.EnterEmuleTab = True
        Me.TxtRepliContrasena.IsDecimalNegative = False
        Me.TxtRepliContrasena.LinkKeyDown = Nothing
        Me.TxtRepliContrasena.Location = New System.Drawing.Point(88, 88)
        Me.TxtRepliContrasena.MaskFormat = Nothing
        Me.TxtRepliContrasena.Name = "TxtRepliContrasena"
        Me.TxtRepliContrasena.Requiere = False
        Me.TxtRepliContrasena.Size = New System.Drawing.Size(111, 20)
        Me.TxtRepliContrasena.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(11, 68)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Usuario"
        '
        'TxtRepliUsuario
        '
        Me.TxtRepliUsuario.BackColor = System.Drawing.Color.White
        Me.TxtRepliUsuario.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtRepliUsuario.DecimalPrecision = 0
        Me.TxtRepliUsuario.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtRepliUsuario.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtRepliUsuario.Enganche = Nothing
        Me.TxtRepliUsuario.EnterEmuleTab = True
        Me.TxtRepliUsuario.IsDecimalNegative = False
        Me.TxtRepliUsuario.LinkKeyDown = Nothing
        Me.TxtRepliUsuario.Location = New System.Drawing.Point(88, 65)
        Me.TxtRepliUsuario.MaskFormat = Nothing
        Me.TxtRepliUsuario.Name = "TxtRepliUsuario"
        Me.TxtRepliUsuario.Requiere = False
        Me.TxtRepliUsuario.Size = New System.Drawing.Size(111, 20)
        Me.TxtRepliUsuario.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(11, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Servidor"
        '
        'TxtRepliServidor
        '
        Me.TxtRepliServidor.BackColor = System.Drawing.Color.White
        Me.TxtRepliServidor.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtRepliServidor.DecimalPrecision = 0
        Me.TxtRepliServidor.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtRepliServidor.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtRepliServidor.Enganche = Nothing
        Me.TxtRepliServidor.EnterEmuleTab = True
        Me.TxtRepliServidor.IsDecimalNegative = False
        Me.TxtRepliServidor.LinkKeyDown = Nothing
        Me.TxtRepliServidor.Location = New System.Drawing.Point(88, 19)
        Me.TxtRepliServidor.MaskFormat = Nothing
        Me.TxtRepliServidor.Name = "TxtRepliServidor"
        Me.TxtRepliServidor.Requiere = False
        Me.TxtRepliServidor.Size = New System.Drawing.Size(111, 20)
        Me.TxtRepliServidor.TabIndex = 2
        '
        'BtnProbarConexion
        '
        Me.BtnProbarConexion.Enabled = False
        Me.BtnProbarConexion.Location = New System.Drawing.Point(12, 185)
        Me.BtnProbarConexion.Name = "BtnProbarConexion"
        Me.BtnProbarConexion.Size = New System.Drawing.Size(97, 23)
        Me.BtnProbarConexion.TabIndex = 3
        Me.BtnProbarConexion.Text = "Probar conexión"
        Me.BtnProbarConexion.UseVisualStyleBackColor = True
        '
        'BtnGuardar
        '
        Me.BtnGuardar.Location = New System.Drawing.Point(115, 185)
        Me.BtnGuardar.Name = "BtnGuardar"
        Me.BtnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.BtnGuardar.TabIndex = 4
        Me.BtnGuardar.Text = "Guardar"
        Me.BtnGuardar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TxtHora_Hasta2)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.TxtHora_Desde2)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.TxtHora_Desde1)
        Me.GroupBox2.Controls.Add(Me.RbnHora_Es_XCadaHora)
        Me.GroupBox2.Controls.Add(Me.RbnHora_Es_Establecido)
        Me.GroupBox2.Enabled = False
        Me.GroupBox2.Location = New System.Drawing.Point(226, 35)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(220, 118)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Frecuencia diaria"
        '
        'TxtHora_Hasta2
        '
        Me.TxtHora_Hasta2.EditValue = New Date(2014, 11, 28, 23, 59, 59, 0)
        Me.TxtHora_Hasta2.Enabled = False
        Me.TxtHora_Hasta2.EnterMoveNextControl = True
        Me.TxtHora_Hasta2.Location = New System.Drawing.Point(119, 92)
        Me.TxtHora_Hasta2.Name = "TxtHora_Hasta2"
        Me.TxtHora_Hasta2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TxtHora_Hasta2.Size = New System.Drawing.Size(91, 20)
        Me.TxtHora_Hasta2.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(29, 95)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(91, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Terminando a las "
        '
        'TxtHora_Desde2
        '
        Me.TxtHora_Desde2.EditValue = New Date(2014, 11, 28, 0, 0, 0, 0)
        Me.TxtHora_Desde2.Enabled = False
        Me.TxtHora_Desde2.EnterMoveNextControl = True
        Me.TxtHora_Desde2.Location = New System.Drawing.Point(119, 67)
        Me.TxtHora_Desde2.Name = "TxtHora_Desde2"
        Me.TxtHora_Desde2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TxtHora_Desde2.Size = New System.Drawing.Size(91, 20)
        Me.TxtHora_Desde2.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(29, 70)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Iniciando a las "
        '
        'TxtHora_Desde1
        '
        Me.TxtHora_Desde1.EditValue = New Date(2014, 11, 28, 0, 0, 0, 0)
        Me.TxtHora_Desde1.EnterMoveNextControl = True
        Me.TxtHora_Desde1.Location = New System.Drawing.Point(119, 18)
        Me.TxtHora_Desde1.Name = "TxtHora_Desde1"
        Me.TxtHora_Desde1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TxtHora_Desde1.Size = New System.Drawing.Size(91, 20)
        Me.TxtHora_Desde1.TabIndex = 1
        '
        'RbnHora_Es_XCadaHora
        '
        Me.RbnHora_Es_XCadaHora.AutoSize = True
        Me.RbnHora_Es_XCadaHora.Location = New System.Drawing.Point(8, 42)
        Me.RbnHora_Es_XCadaHora.Name = "RbnHora_Es_XCadaHora"
        Me.RbnHora_Es_XCadaHora.Size = New System.Drawing.Size(178, 17)
        Me.RbnHora_Es_XCadaHora.TabIndex = 2
        Me.RbnHora_Es_XCadaHora.TabStop = True
        Me.RbnHora_Es_XCadaHora.Text = "Ocurre cada en rango de tiempo"
        Me.RbnHora_Es_XCadaHora.UseVisualStyleBackColor = True
        '
        'RbnHora_Es_Establecido
        '
        Me.RbnHora_Es_Establecido.AutoSize = True
        Me.RbnHora_Es_Establecido.Checked = True
        Me.RbnHora_Es_Establecido.Location = New System.Drawing.Point(8, 19)
        Me.RbnHora_Es_Establecido.Name = "RbnHora_Es_Establecido"
        Me.RbnHora_Es_Establecido.Size = New System.Drawing.Size(105, 17)
        Me.RbnHora_Es_Establecido.TabIndex = 0
        Me.RbnHora_Es_Establecido.TabStop = True
        Me.RbnHora_Es_Establecido.Text = "Se produce a las"
        Me.RbnHora_Es_Establecido.UseVisualStyleBackColor = True
        '
        'TxtServidorArchivos
        '
        Me.TxtServidorArchivos.BackColor = System.Drawing.Color.White
        Me.TxtServidorArchivos.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtServidorArchivos.DecimalPrecision = 0
        Me.TxtServidorArchivos.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtServidorArchivos.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtServidorArchivos.Enganche = Nothing
        Me.TxtServidorArchivos.EnterEmuleTab = True
        Me.TxtServidorArchivos.IsDecimalNegative = False
        Me.TxtServidorArchivos.LinkKeyDown = Nothing
        Me.TxtServidorArchivos.Location = New System.Drawing.Point(126, 159)
        Me.TxtServidorArchivos.MaskFormat = Nothing
        Me.TxtServidorArchivos.Name = "TxtServidorArchivos"
        Me.TxtServidorArchivos.Requiere = False
        Me.TxtServidorArchivos.Size = New System.Drawing.Size(320, 20)
        Me.TxtServidorArchivos.TabIndex = 9
        '
        'CheckBoxNew1
        '
        Me.CheckBoxNew1.AutoSize = True
        Me.CheckBoxNew1.Location = New System.Drawing.Point(12, 159)
        Me.CheckBoxNew1.Name = "CheckBoxNew1"
        Me.CheckBoxNew1.Size = New System.Drawing.Size(108, 17)
        Me.CheckBoxNew1.TabIndex = 10
        Me.CheckBoxNew1.Text = "Copiar archivos a"
        Me.CheckBoxNew1.UseVisualStyleBackColor = True
        '
        'FrmReplicacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(459, 220)
        Me.Controls.Add(Me.CheckBoxNew1)
        Me.Controls.Add(Me.TxtServidorArchivos)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.BtnGuardar)
        Me.Controls.Add(Me.BtnProbarConexion)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ChkReplicar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmReplicacion"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Replicación"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.TxtHora_Hasta2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtHora_Desde2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtHora_Desde1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ChkReplicar As dhsoft.CheckBoxNew
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TxtRepliContrasena As dhsoft.TextBoxNew
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TxtRepliUsuario As dhsoft.TextBoxNew
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TxtRepliServidor As dhsoft.TextBoxNew
    Friend WithEvents BtnProbarConexion As System.Windows.Forms.Button
    Friend WithEvents BtnGuardar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TxtHora_Hasta2 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TxtHora_Desde2 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TxtHora_Desde1 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents RbnHora_Es_XCadaHora As dhsoft.RadioButtonNew
    Friend WithEvents RbnHora_Es_Establecido As dhsoft.RadioButtonNew
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TxtRepliBD As dhsoft.TextBoxNew
    Friend WithEvents TxtServidorArchivos As dhsoft.TextBoxNew
    Friend WithEvents CheckBoxNew1 As dhsoft.CheckBoxNew
End Class
