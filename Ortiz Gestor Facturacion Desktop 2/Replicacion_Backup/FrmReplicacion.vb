﻿Public Class FrmReplicacion

    Private Sub FrmReplicacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim log As New Log_Configuracion_App
        Dim Lista As List(Of eConfiguracionApp) = log.Buscar
        If Lista.Count > 0 Then
            ChkReplicar.Checked = Lista(0).App_Replicacion
            TxtRepliServidor.Text = Lista(0).App_Replicacion_Servidor
            TxtRepliBD.Text = Lista(0).App_Replicacion_BD
            TxtRepliUsuario.Text = Lista(0).App_Replicacion_Usuario
            TxtRepliContrasena.Text = Lista(0).App_Replicacion_Contrasenia

            RbnHora_Es_Establecido.Checked = Lista(0).App_Replicacion_Prog_Hora_Establecido
            RbnHora_Es_XCadaHora.Checked = Lista(0).App_Replicacion_Prog_Hora_CadaRangoHoras

            RbnHora_Es_Establecido.Checked = Lista(0).App_Replicacion_Prog_Hora_Establecido
            RbnHora_Es_XCadaHora.Checked = Lista(0).App_Replicacion_Prog_Hora_CadaRangoHoras

            TxtHora_Desde1.Enabled = RbnHora_Es_Establecido.Checked
            If RbnHora_Es_Establecido.Checked Then
                TxtHora_Desde1.Time = Lista(0).Progra_FrecuEnvio_Hora_Desde_12
                'TxtHora_Desde1.Text = TareaProgramada.Progra_FrecuEnvio_Hora_Desde
                'TareaProgramada.Progra_FrecuEnvio_Hora_Hasta = Date.MaxValue
                'TareaProgramada.Progra_FrecuEnvio_Hora_XCadaHora = 0
            End If
            TxtHora_Desde2.Enabled = RbnHora_Es_XCadaHora.Checked
            TxtHora_Hasta2.Enabled = RbnHora_Es_XCadaHora.Checked
            'TxtXCadaHora1.Enabled = RbnHora_Es_XCadaHora.Checked
            If RbnHora_Es_XCadaHora.Checked Then
                TxtHora_Desde2.Time = Lista(0).Progra_FrecuEnvio_Hora_Desde_12
                TxtHora_Hasta2.Time = Lista(0).Progra_FrecuEnvio_Hora_Hasta_12
                'TxtXCadaHora1.Text = Lista(0).App_Replicacion_Prog_Hora
            End If

            TxtServidorArchivos.Text = Lista(0).App_Replicacion_RutaPrincipalArchivos

        End If
    End Sub

    Private Sub RbnHora_Es_Establecido_CheckedChanged(sender As Object, e As EventArgs) Handles RbnHora_Es_Establecido.CheckedChanged
        TxtHora_Desde1.Enabled = RbnHora_Es_Establecido.Checked
    End Sub

    Private Sub RbnHora_Es_XCadaHora_CheckedChanged(sender As Object, e As EventArgs) Handles RbnHora_Es_XCadaHora.CheckedChanged
        'TxtXCadaHora1.Enabled = RbnHora_Es_XCadaHora.Checked
        TxtHora_Desde2.Enabled = RbnHora_Es_XCadaHora.Checked
        TxtHora_Hasta2.Enabled = RbnHora_Es_XCadaHora.Checked
        'If Not RbnHora_Es_XCadaHora.Checked Then
        '    'TxtXCadaHora1.Text = 0
        'End If
    End Sub
    Private Sub ChkReplicar_CheckedChanged(sender As Object, e As EventArgs) Handles ChkReplicar.CheckedChanged
        GroupBox1.Enabled = ChkReplicar.Checked
        GroupBox2.Enabled = ChkReplicar.Checked
        BtnProbarConexion.Enabled = ChkReplicar.Checked
        If ChkReplicar.Checked Then
            TxtRepliServidor.Focus()
        End If
    End Sub
    Private Sub BtnProbarConexion_Click(sender As Object, e As EventArgs) Handles BtnProbarConexion.Click
        If Not String.IsNullOrEmpty(TxtRepliServidor.Text) And Not String.IsNullOrEmpty(TxtRepliUsuario.Text) And Not String.IsNullOrEmpty(TxtRepliContrasena.Text) Then
            Try
                Using connection As New SqlClient.SqlConnection("Data Source='" + TxtRepliServidor.Text.Trim + "';Initial Catalog='" + TxtRepliBD.Text.Trim + "';User ID='" + TxtRepliUsuario.Text + "';Password='" + TxtRepliContrasena.Text + "'")
                    Try
                        connection.Open()
                        If connection.State = ConnectionState.Open Then
                            ' if connection.Open was successful
                            MessageBox.Show("Conectado correctamente al servidor principal!")
                        Else
                            MessageBox.Show("Error al conectar")
                        End If
                    Catch generatedExceptionName As SqlClient.SqlException
                        MessageBox.Show(generatedExceptionName.Message)
                    End Try
                End Using
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            Finally
            End Try
        Else
            MessageBox.Show("Debe ingresar los datos del servidor")
        End If
    End Sub

    Private Sub BtnGuardar_Click(sender As Object, e As EventArgs) Handles BtnGuardar.Click
        Dim Config As New eConfiguracionApp
        Config.App_Replicacion = ChkReplicar.Checked
        Config.App_Replicacion_Servidor = TxtRepliServidor.Text.Trim
        Config.App_Replicacion_BD = TxtRepliBD.Text.Trim
        Config.App_Replicacion_Usuario = TxtRepliUsuario.Text
        Config.App_Replicacion_Contrasenia = TxtRepliContrasena.Text

        Config.App_Replicacion_Prog_Hora_Establecido = RbnHora_Es_Establecido.Checked
        Config.App_Replicacion_Prog_Hora_CadaRangoHoras = RbnHora_Es_XCadaHora.Checked
        Dim hms As Date
        If RbnHora_Es_Establecido.Checked Then

            hms = TxtHora_Desde1.Text

            Config.App_Replicacion_Prog_Hora_Desde = New TimeSpan(0, hms.Hour, hms.Minute, hms.Second, hms.Millisecond)
            Config.App_Replicacion_Prog_Hora_Hasta = New TimeSpan(0, 23, 59, 59, 0) ' TimeSpan.MaxValue
            Config.App_Replicacion_Prog_Hora = 0
        End If
        If RbnHora_Es_XCadaHora.Checked Then
            hms = TxtHora_Desde2.Text
            Config.App_Replicacion_Prog_Hora_Desde = New TimeSpan(0, hms.Hour, hms.Minute, hms.Second, hms.Millisecond)
            hms = TxtHora_Hasta2.Text
            Config.App_Replicacion_Prog_Hora_Hasta = New TimeSpan(0, hms.Hour, hms.Minute, hms.Second, hms.Millisecond)
            Config.App_Replicacion_Prog_Hora = 0
        End If
        Config.App_Replicacion_RutaPrincipalArchivos = TxtServidorArchivos.Text
        Dim log As New Log_Configuracion_App
        If log.Insertar(Config) Then
            MessageBox.Show("Se guardo la configuración de replicación")
        Else
            MessageBox.Show("No se pudo guardar")
        End If
    End Sub
End Class