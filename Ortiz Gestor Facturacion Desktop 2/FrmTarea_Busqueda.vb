﻿Public Class FrmTarea_Busqueda
    Public Empresa As eEmpresa
    Public Generales As New List(Of eProgramacion)
    Private Sub FrmTarea_Busqueda_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarLista()
    End Sub
    Sub CargarLista()
        Dim log As New Log_FACTE_PROGRAMACIONES_ENVIO
        Generales = log.Buscar_Lista(Empresa.Emp_NumRuc)
        DgvGeneral.DataSource = Nothing
        DgvGeneral.DataSource = Generales
        VwDgv.Focus()
        VwDgv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.AutoFilterRowHandle
    End Sub
    Private Sub BtnNuevo_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnNuevo.ItemClick
        Using f As New FrmTarea_Edicion
            f.Emisor_RUC = Empresa.Emp_NumRuc
            If f.ShowDialog = DialogResult.OK Then
                CargarLista()
            End If
        End Using
    End Sub

    Private Sub BtnModificar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnModificar.ItemClick
        If VwDgv.GetFocusedDataSourceRowIndex > -1 Then
            Using f As New FrmTarea_Edicion
                f.Emisor_RUC = Empresa.Emp_NumRuc
                f.TareaProgramada = Generales(VwDgv.GetFocusedDataSourceRowIndex)
                If f.ShowDialog = DialogResult.OK Then
                    CargarLista()
                End If
            End Using
        End If
    End Sub

    Private Sub BtnEliminar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnEliminar.ItemClick
        If VwDgv.GetFocusedDataSourceRowIndex > -1 Then
            If MessageBox.Show("¿Esta Ud. seguro de eliminar?", "Eliminar", MessageBoxButtons.YesNo) Then
                Dim log As New Log_FACTE_PROGRAMACIONES_ENVIO
                If log.Eliminar(Generales(VwDgv.GetFocusedDataSourceRowIndex)) Then
                    CargarLista()
                End If
            End If
        End If
    End Sub
    Private Sub BtnSalir_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnSalir.ItemClick
        Me.Close()
    End Sub
End Class