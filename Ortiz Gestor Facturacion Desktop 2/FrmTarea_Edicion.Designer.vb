﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTarea_Edicion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTarea_Edicion))
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.BtnGuardar = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnSalir = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.BtnExportXls = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnExportPdf = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnExportRtf = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnExportHtml = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TxtDescripcion = New dhsoft.TextBoxNew(Me.components)
        Me.ChkHabilitado = New dhsoft.CheckBoxNew()
        Me.TxtXCadaHora1 = New dhsoft.TextBoxNew(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.RbnHora_Es_DespuesEmitido = New dhsoft.RadioButtonNew()
        Me.TxtXCadaHora2 = New dhsoft.TextBoxNew(Me.components)
        Me.TxtHora_Hasta2 = New DevExpress.XtraEditors.TimeEdit()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TxtHora_Desde2 = New DevExpress.XtraEditors.TimeEdit()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TxtHora_Desde1 = New DevExpress.XtraEditors.TimeEdit()
        Me.RbnHora_Es_XCadaHora = New dhsoft.RadioButtonNew()
        Me.RbnHora_Es_Establecido = New dhsoft.RadioButtonNew()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.TxtFecha_Hasta = New dhsoft.MaskedTextBoxNew()
        Me.RbnFecha_SinFin = New dhsoft.RadioButtonNew()
        Me.RbnFecha_ConFin = New dhsoft.RadioButtonNew()
        Me.TxtFecha_Desde = New dhsoft.MaskedTextBoxNew()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.ChkComunicacionBaja = New dhsoft.CheckBoxNew()
        Me.ChkResumenBoletas = New dhsoft.CheckBoxNew()
        Me.TxtSerie = New dhsoft.TextBoxNew(Me.components)
        Me.ChkSerie = New dhsoft.CheckBoxNew()
        Me.TxtTipoDocDescripcion = New dhsoft.TextBoxNew(Me.components)
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TxtTipoDocCodigo = New dhsoft.TextBoxNew(Me.components)
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.TxtHora_Hasta2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtHora_Desde2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtHora_Desde1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BtnExportXls, Me.BtnExportPdf, Me.BtnExportRtf, Me.BtnExportHtml, Me.BtnSalir, Me.BarButtonItem1, Me.BtnGuardar})
        Me.BarManager1.MaxItemId = 19
        '
        'Bar1
        '
        Me.Bar1.BarName = "Herramientas"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BtnGuardar), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnSalir)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Herramientas"
        '
        'BtnGuardar
        '
        Me.BtnGuardar.Caption = "Guardar"
        Me.BtnGuardar.Glyph = CType(resources.GetObject("BtnGuardar.Glyph"), System.Drawing.Image)
        Me.BtnGuardar.Id = 18
        Me.BtnGuardar.Name = "BtnGuardar"
        Me.BtnGuardar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnSalir
        '
        Me.BtnSalir.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.BtnSalir.Caption = "Cancelar"
        Me.BtnSalir.Glyph = CType(resources.GetObject("BtnSalir.Glyph"), System.Drawing.Image)
        Me.BtnSalir.Id = 10
        Me.BtnSalir.ImageIndex = 15
        Me.BtnSalir.Name = "BtnSalir"
        Me.BtnSalir.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(598, 35)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 348)
        Me.barDockControlBottom.Size = New System.Drawing.Size(598, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 35)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 313)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(598, 35)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 313)
        '
        'BtnExportXls
        '
        Me.BtnExportXls.Caption = "Excel"
        Me.BtnExportXls.Id = 4
        Me.BtnExportXls.ImageIndex = 10
        Me.BtnExportXls.Name = "BtnExportXls"
        '
        'BtnExportPdf
        '
        Me.BtnExportPdf.Caption = "Pdf"
        Me.BtnExportPdf.Id = 5
        Me.BtnExportPdf.ImageIndex = 11
        Me.BtnExportPdf.Name = "BtnExportPdf"
        '
        'BtnExportRtf
        '
        Me.BtnExportRtf.Caption = "Rtf"
        Me.BtnExportRtf.Id = 6
        Me.BtnExportRtf.ImageIndex = 12
        Me.BtnExportRtf.Name = "BtnExportRtf"
        '
        'BtnExportHtml
        '
        Me.BtnExportHtml.Caption = "Html"
        Me.BtnExportHtml.Id = 7
        Me.BtnExportHtml.ImageIndex = 13
        Me.BtnExportHtml.Name = "BtnExportHtml"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Cancelar"
        Me.BarButtonItem1.Id = 11
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.TxtDescripcion)
        Me.GroupBox1.Controls.Add(Me.ChkHabilitado)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 41)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(573, 44)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Descripción"
        '
        'TxtDescripcion
        '
        Me.TxtDescripcion.BackColor = System.Drawing.Color.White
        Me.TxtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtDescripcion.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtDescripcion.DecimalPrecision = 0
        Me.TxtDescripcion.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtDescripcion.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtDescripcion.Enganche = Nothing
        Me.TxtDescripcion.EnterEmuleTab = True
        Me.TxtDescripcion.IsDecimalNegative = False
        Me.TxtDescripcion.LinkKeyDown = Nothing
        Me.TxtDescripcion.Location = New System.Drawing.Point(82, 14)
        Me.TxtDescripcion.MaskFormat = Nothing
        Me.TxtDescripcion.Name = "TxtDescripcion"
        Me.TxtDescripcion.Requiere = False
        Me.TxtDescripcion.Size = New System.Drawing.Size(403, 20)
        Me.TxtDescripcion.TabIndex = 1
        '
        'ChkHabilitado
        '
        Me.ChkHabilitado.AutoSize = True
        Me.ChkHabilitado.Location = New System.Drawing.Point(491, 16)
        Me.ChkHabilitado.Name = "ChkHabilitado"
        Me.ChkHabilitado.Size = New System.Drawing.Size(73, 17)
        Me.ChkHabilitado.TabIndex = 2
        Me.ChkHabilitado.Text = "Habilitado"
        Me.ChkHabilitado.UseVisualStyleBackColor = True
        '
        'TxtXCadaHora1
        '
        Me.TxtXCadaHora1.BackColor = System.Drawing.Color.White
        Me.TxtXCadaHora1.Contenido = dhsoft.Enums.TipoTexto.OnlyNumber
        Me.TxtXCadaHora1.DecimalPrecision = 0
        Me.TxtXCadaHora1.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtXCadaHora1.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtXCadaHora1.Enabled = False
        Me.TxtXCadaHora1.Enganche = Nothing
        Me.TxtXCadaHora1.EnterEmuleTab = True
        Me.TxtXCadaHora1.IsDecimalNegative = False
        Me.TxtXCadaHora1.LinkKeyDown = Nothing
        Me.TxtXCadaHora1.Location = New System.Drawing.Point(119, 41)
        Me.TxtXCadaHora1.MaskFormat = Nothing
        Me.TxtXCadaHora1.Name = "TxtXCadaHora1"
        Me.TxtXCadaHora1.Requiere = True
        Me.TxtXCadaHora1.Size = New System.Drawing.Size(28, 20)
        Me.TxtXCadaHora1.TabIndex = 3
        Me.TxtXCadaHora1.Text = "0"
        Me.TxtXCadaHora1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.RbnHora_Es_DespuesEmitido)
        Me.GroupBox2.Controls.Add(Me.TxtXCadaHora2)
        Me.GroupBox2.Controls.Add(Me.TxtHora_Hasta2)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.TxtHora_Desde2)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.TxtHora_Desde1)
        Me.GroupBox2.Controls.Add(Me.RbnHora_Es_XCadaHora)
        Me.GroupBox2.Controls.Add(Me.RbnHora_Es_Establecido)
        Me.GroupBox2.Controls.Add(Me.TxtXCadaHora1)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 144)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(573, 92)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Frecuencia diaria"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(153, 67)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(144, 13)
        Me.Label11.TabIndex = 11
        Me.Label11.Text = "Hora(s) emitido el documento"
        '
        'RbnHora_Es_DespuesEmitido
        '
        Me.RbnHora_Es_DespuesEmitido.AutoSize = True
        Me.RbnHora_Es_DespuesEmitido.Location = New System.Drawing.Point(8, 65)
        Me.RbnHora_Es_DespuesEmitido.Name = "RbnHora_Es_DespuesEmitido"
        Me.RbnHora_Es_DespuesEmitido.Size = New System.Drawing.Size(84, 17)
        Me.RbnHora_Es_DespuesEmitido.TabIndex = 9
        Me.RbnHora_Es_DespuesEmitido.Text = "Ocurre cada"
        Me.RbnHora_Es_DespuesEmitido.UseVisualStyleBackColor = True
        '
        'TxtXCadaHora2
        '
        Me.TxtXCadaHora2.BackColor = System.Drawing.Color.White
        Me.TxtXCadaHora2.Contenido = dhsoft.Enums.TipoTexto.OnlyNumber
        Me.TxtXCadaHora2.DecimalPrecision = 0
        Me.TxtXCadaHora2.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtXCadaHora2.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtXCadaHora2.Enabled = False
        Me.TxtXCadaHora2.Enganche = Nothing
        Me.TxtXCadaHora2.EnterEmuleTab = True
        Me.TxtXCadaHora2.IsDecimalNegative = False
        Me.TxtXCadaHora2.LinkKeyDown = Nothing
        Me.TxtXCadaHora2.Location = New System.Drawing.Point(119, 64)
        Me.TxtXCadaHora2.MaskFormat = Nothing
        Me.TxtXCadaHora2.Name = "TxtXCadaHora2"
        Me.TxtXCadaHora2.Requiere = True
        Me.TxtXCadaHora2.Size = New System.Drawing.Size(28, 20)
        Me.TxtXCadaHora2.TabIndex = 10
        Me.TxtXCadaHora2.Text = "0"
        Me.TxtXCadaHora2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TxtHora_Hasta2
        '
        Me.TxtHora_Hasta2.EditValue = New Date(2014, 11, 28, 23, 59, 59, 0)
        Me.TxtHora_Hasta2.Enabled = False
        Me.TxtHora_Hasta2.EnterMoveNextControl = True
        Me.TxtHora_Hasta2.Location = New System.Drawing.Point(473, 41)
        Me.TxtHora_Hasta2.Name = "TxtHora_Hasta2"
        Me.TxtHora_Hasta2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TxtHora_Hasta2.Size = New System.Drawing.Size(91, 20)
        Me.TxtHora_Hasta2.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(383, 44)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(91, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Terminando a las "
        '
        'TxtHora_Desde2
        '
        Me.TxtHora_Desde2.EditValue = New Date(2014, 11, 28, 0, 0, 0, 0)
        Me.TxtHora_Desde2.Enabled = False
        Me.TxtHora_Desde2.EnterMoveNextControl = True
        Me.TxtHora_Desde2.Location = New System.Drawing.Point(278, 41)
        Me.TxtHora_Desde2.Name = "TxtHora_Desde2"
        Me.TxtHora_Desde2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TxtHora_Desde2.Size = New System.Drawing.Size(91, 20)
        Me.TxtHora_Desde2.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(200, 44)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Iniciando a las "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(153, 44)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Hora(s)"
        '
        'TxtHora_Desde1
        '
        Me.TxtHora_Desde1.EditValue = New Date(2014, 11, 28, 0, 0, 0, 0)
        Me.TxtHora_Desde1.EnterMoveNextControl = True
        Me.TxtHora_Desde1.Location = New System.Drawing.Point(119, 18)
        Me.TxtHora_Desde1.Name = "TxtHora_Desde1"
        Me.TxtHora_Desde1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TxtHora_Desde1.Size = New System.Drawing.Size(91, 20)
        Me.TxtHora_Desde1.TabIndex = 1
        '
        'RbnHora_Es_XCadaHora
        '
        Me.RbnHora_Es_XCadaHora.AutoSize = True
        Me.RbnHora_Es_XCadaHora.Location = New System.Drawing.Point(8, 42)
        Me.RbnHora_Es_XCadaHora.Name = "RbnHora_Es_XCadaHora"
        Me.RbnHora_Es_XCadaHora.Size = New System.Drawing.Size(84, 17)
        Me.RbnHora_Es_XCadaHora.TabIndex = 2
        Me.RbnHora_Es_XCadaHora.TabStop = True
        Me.RbnHora_Es_XCadaHora.Text = "Ocurre cada"
        Me.RbnHora_Es_XCadaHora.UseVisualStyleBackColor = True
        '
        'RbnHora_Es_Establecido
        '
        Me.RbnHora_Es_Establecido.AutoSize = True
        Me.RbnHora_Es_Establecido.Checked = True
        Me.RbnHora_Es_Establecido.Location = New System.Drawing.Point(8, 19)
        Me.RbnHora_Es_Establecido.Name = "RbnHora_Es_Establecido"
        Me.RbnHora_Es_Establecido.Size = New System.Drawing.Size(105, 17)
        Me.RbnHora_Es_Establecido.TabIndex = 0
        Me.RbnHora_Es_Establecido.TabStop = True
        Me.RbnHora_Es_Establecido.Text = "Se produce a las"
        Me.RbnHora_Es_Establecido.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.TxtFecha_Hasta)
        Me.GroupBox3.Controls.Add(Me.RbnFecha_SinFin)
        Me.GroupBox3.Controls.Add(Me.RbnFecha_ConFin)
        Me.GroupBox3.Controls.Add(Me.TxtFecha_Desde)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 255)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(573, 47)
        Me.GroupBox3.TabIndex = 7
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Duración"
        '
        'TxtFecha_Hasta
        '
        Me.TxtFecha_Hasta.Enabled = False
        Me.TxtFecha_Hasta.IsValueDate = False
        Me.TxtFecha_Hasta.Location = New System.Drawing.Point(374, 19)
        Me.TxtFecha_Hasta.Mask = "00/00/0000"
        Me.TxtFecha_Hasta.Name = "TxtFecha_Hasta"
        Me.TxtFecha_Hasta.Requiere = False
        Me.TxtFecha_Hasta.Size = New System.Drawing.Size(70, 20)
        Me.TxtFecha_Hasta.TabIndex = 4
        Me.TxtFecha_Hasta.ValidatingType = GetType(Date)
        '
        'RbnFecha_SinFin
        '
        Me.RbnFecha_SinFin.AutoSize = True
        Me.RbnFecha_SinFin.Checked = True
        Me.RbnFecha_SinFin.Location = New System.Drawing.Point(174, 20)
        Me.RbnFecha_SinFin.Name = "RbnFecha_SinFin"
        Me.RbnFecha_SinFin.Size = New System.Drawing.Size(102, 17)
        Me.RbnFecha_SinFin.TabIndex = 2
        Me.RbnFecha_SinFin.TabStop = True
        Me.RbnFecha_SinFin.Text = "No tiene término"
        Me.RbnFecha_SinFin.UseVisualStyleBackColor = True
        '
        'RbnFecha_ConFin
        '
        Me.RbnFecha_ConFin.AutoSize = True
        Me.RbnFecha_ConFin.Location = New System.Drawing.Point(290, 20)
        Me.RbnFecha_ConFin.Name = "RbnFecha_ConFin"
        Me.RbnFecha_ConFin.Size = New System.Drawing.Size(71, 17)
        Me.RbnFecha_ConFin.TabIndex = 3
        Me.RbnFecha_ConFin.Text = "Fecha fín"
        Me.RbnFecha_ConFin.UseVisualStyleBackColor = True
        '
        'TxtFecha_Desde
        '
        Me.TxtFecha_Desde.IsValueDate = False
        Me.TxtFecha_Desde.Location = New System.Drawing.Point(71, 19)
        Me.TxtFecha_Desde.Mask = "00/00/0000"
        Me.TxtFecha_Desde.Name = "TxtFecha_Desde"
        Me.TxtFecha_Desde.Requiere = False
        Me.TxtFecha_Desde.Size = New System.Drawing.Size(70, 20)
        Me.TxtFecha_Desde.TabIndex = 1
        Me.TxtFecha_Desde.ValidatingType = GetType(Date)
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 22)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(64, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Fecha inicio"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.ChkComunicacionBaja)
        Me.GroupBox4.Controls.Add(Me.ChkResumenBoletas)
        Me.GroupBox4.Controls.Add(Me.TxtSerie)
        Me.GroupBox4.Controls.Add(Me.ChkSerie)
        Me.GroupBox4.Controls.Add(Me.TxtTipoDocDescripcion)
        Me.GroupBox4.Controls.Add(Me.Label9)
        Me.GroupBox4.Controls.Add(Me.TxtTipoDocCodigo)
        Me.GroupBox4.Location = New System.Drawing.Point(12, 91)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(573, 47)
        Me.GroupBox4.TabIndex = 5
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Aplica a"
        '
        'ChkComunicacionBaja
        '
        Me.ChkComunicacionBaja.AutoSize = True
        Me.ChkComunicacionBaja.Location = New System.Drawing.Point(430, 17)
        Me.ChkComunicacionBaja.Name = "ChkComunicacionBaja"
        Me.ChkComunicacionBaja.Size = New System.Drawing.Size(131, 17)
        Me.ChkComunicacionBaja.TabIndex = 6
        Me.ChkComunicacionBaja.Text = "Comunicación de baja"
        Me.ChkComunicacionBaja.UseVisualStyleBackColor = True
        '
        'ChkResumenBoletas
        '
        Me.ChkResumenBoletas.AutoSize = True
        Me.ChkResumenBoletas.Location = New System.Drawing.Point(300, 17)
        Me.ChkResumenBoletas.Name = "ChkResumenBoletas"
        Me.ChkResumenBoletas.Size = New System.Drawing.Size(124, 17)
        Me.ChkResumenBoletas.TabIndex = 5
        Me.ChkResumenBoletas.Text = "Resumen de Boletas"
        Me.ChkResumenBoletas.UseVisualStyleBackColor = True
        '
        'TxtSerie
        '
        Me.TxtSerie.BackColor = System.Drawing.Color.White
        Me.TxtSerie.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtSerie.DecimalPrecision = 0
        Me.TxtSerie.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtSerie.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtSerie.Enabled = False
        Me.TxtSerie.Enganche = Nothing
        Me.TxtSerie.EnterEmuleTab = True
        Me.TxtSerie.IsDecimalNegative = False
        Me.TxtSerie.LinkKeyDown = Nothing
        Me.TxtSerie.Location = New System.Drawing.Point(232, 15)
        Me.TxtSerie.MaskFormat = Nothing
        Me.TxtSerie.MaxLength = 4
        Me.TxtSerie.Name = "TxtSerie"
        Me.TxtSerie.Requiere = True
        Me.TxtSerie.Size = New System.Drawing.Size(44, 20)
        Me.TxtSerie.TabIndex = 4
        '
        'ChkSerie
        '
        Me.ChkSerie.AutoSize = True
        Me.ChkSerie.Location = New System.Drawing.Point(184, 17)
        Me.ChkSerie.Name = "ChkSerie"
        Me.ChkSerie.Size = New System.Drawing.Size(50, 17)
        Me.ChkSerie.TabIndex = 3
        Me.ChkSerie.Text = "Serie"
        Me.ChkSerie.UseVisualStyleBackColor = True
        '
        'TxtTipoDocDescripcion
        '
        Me.TxtTipoDocDescripcion.BackColor = System.Drawing.Color.White
        Me.TxtTipoDocDescripcion.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtTipoDocDescripcion.DecimalPrecision = 0
        Me.TxtTipoDocDescripcion.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtTipoDocDescripcion.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtTipoDocDescripcion.Enabled = False
        Me.TxtTipoDocDescripcion.Enganche = Nothing
        Me.TxtTipoDocDescripcion.EnterEmuleTab = True
        Me.TxtTipoDocDescripcion.IsDecimalNegative = False
        Me.TxtTipoDocDescripcion.LinkKeyDown = Nothing
        Me.TxtTipoDocDescripcion.Location = New System.Drawing.Point(93, 15)
        Me.TxtTipoDocDescripcion.MaskFormat = Nothing
        Me.TxtTipoDocDescripcion.Name = "TxtTipoDocDescripcion"
        Me.TxtTipoDocDescripcion.Requiere = True
        Me.TxtTipoDocDescripcion.Size = New System.Drawing.Size(85, 20)
        Me.TxtTipoDocDescripcion.TabIndex = 2
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(9, 18)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 13)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Tipo Doc."
        '
        'TxtTipoDocCodigo
        '
        Me.TxtTipoDocCodigo.BackColor = System.Drawing.Color.White
        Me.TxtTipoDocCodigo.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtTipoDocCodigo.DecimalPrecision = 0
        Me.TxtTipoDocCodigo.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtTipoDocCodigo.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtTipoDocCodigo.Enganche = Me.TxtTipoDocDescripcion
        Me.TxtTipoDocCodigo.EnterEmuleTab = True
        Me.TxtTipoDocCodigo.IsDecimalNegative = False
        Me.TxtTipoDocCodigo.LinkKeyDown = Nothing
        Me.TxtTipoDocCodigo.Location = New System.Drawing.Point(64, 15)
        Me.TxtTipoDocCodigo.MaskFormat = Nothing
        Me.TxtTipoDocCodigo.MaxLength = 2
        Me.TxtTipoDocCodigo.Name = "TxtTipoDocCodigo"
        Me.TxtTipoDocCodigo.Requiere = False
        Me.TxtTipoDocCodigo.Size = New System.Drawing.Size(28, 20)
        Me.TxtTipoDocCodigo.TabIndex = 1
        '
        'FrmTarea_Edicion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(598, 348)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmTarea_Edicion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tarea para envíos de documentos electrónicos"
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.TxtHora_Hasta2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtHora_Desde2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtHora_Desde1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BtnGuardar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnSalir As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BtnExportXls As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnExportPdf As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnExportRtf As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnExportHtml As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TxtXCadaHora1 As dhsoft.TextBoxNew
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TxtDescripcion As dhsoft.TextBoxNew
    Friend WithEvents ChkHabilitado As dhsoft.CheckBoxNew
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TxtHora_Desde1 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents RbnHora_Es_XCadaHora As dhsoft.RadioButtonNew
    Friend WithEvents RbnHora_Es_Establecido As dhsoft.RadioButtonNew
    Friend WithEvents TxtHora_Hasta2 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TxtHora_Desde2 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents TxtFecha_Hasta As dhsoft.MaskedTextBoxNew
    Friend WithEvents RbnFecha_SinFin As dhsoft.RadioButtonNew
    Friend WithEvents RbnFecha_ConFin As dhsoft.RadioButtonNew
    Friend WithEvents TxtFecha_Desde As dhsoft.MaskedTextBoxNew
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents TxtSerie As dhsoft.TextBoxNew
    Friend WithEvents ChkSerie As dhsoft.CheckBoxNew
    Friend WithEvents TxtTipoDocDescripcion As dhsoft.TextBoxNew
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TxtTipoDocCodigo As dhsoft.TextBoxNew
    Friend WithEvents ChkComunicacionBaja As dhsoft.CheckBoxNew
    Friend WithEvents ChkResumenBoletas As dhsoft.CheckBoxNew
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents RbnHora_Es_DespuesEmitido As dhsoft.RadioButtonNew
    Friend WithEvents TxtXCadaHora2 As dhsoft.TextBoxNew
End Class
