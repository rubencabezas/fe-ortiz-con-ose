﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDocumento_Busqueda
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmDocumento_Busqueda))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TxtNumero = New dhsoft.TextBoxNew(Me.components)
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TxtSerie = New dhsoft.TextBoxNew(Me.components)
        Me.Label6 = New System.Windows.Forms.Label()
        Me.BtnBuscar = New System.Windows.Forms.Button()
        Me.TxtTipoDocDescripcion = New dhsoft.TextBoxNew(Me.components)
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TxtTipoDocCodigo = New dhsoft.TextBoxNew(Me.components)
        Me.TxtFHasta = New dhsoft.MaskedTextBoxNew()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TxtFDesde = New dhsoft.MaskedTextBoxNew()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TxtEmpDescripcion = New dhsoft.TextBoxNew(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TxtEmpRucNum = New dhsoft.TextBoxNew(Me.components)
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.BtnEnviarSunat = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnEnviarCorreoCliente = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnVersiones = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnVerXML = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnVerPDF = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnVerCDR = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnVerSunat = New DevExpress.XtraBars.BarButtonItem()
        Me.GalleryDropDown1 = New DevExpress.XtraBars.Ribbon.GalleryDropDown(Me.components)
        Me.BtnVerOSE = New DevExpress.XtraBars.BarButtonItem()
        Me.Bar3 = New DevExpress.XtraBars.Bar()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.GroupBox1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GalleryDropDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TxtNumero)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.TxtSerie)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.BtnBuscar)
        Me.GroupBox1.Controls.Add(Me.TxtTipoDocDescripcion)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TxtTipoDocCodigo)
        Me.GroupBox1.Controls.Add(Me.TxtFHasta)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.TxtFDesde)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TxtEmpDescripcion)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.TxtEmpRucNum)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 39)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(846, 64)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'TxtNumero
        '
        Me.TxtNumero.BackColor = System.Drawing.Color.White
        Me.TxtNumero.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtNumero.DecimalPrecision = 0
        Me.TxtNumero.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtNumero.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtNumero.Enganche = Nothing
        Me.TxtNumero.EnterEmuleTab = True
        Me.TxtNumero.IsDecimalNegative = False
        Me.TxtNumero.LinkKeyDown = Nothing
        Me.TxtNumero.Location = New System.Drawing.Point(314, 37)
        Me.TxtNumero.MaskFormat = Nothing
        Me.TxtNumero.Name = "TxtNumero"
        Me.TxtNumero.Requiere = False
        Me.TxtNumero.Size = New System.Drawing.Size(84, 20)
        Me.TxtNumero.TabIndex = 14
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(264, 40)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(44, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Número"
        '
        'TxtSerie
        '
        Me.TxtSerie.BackColor = System.Drawing.Color.White
        Me.TxtSerie.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtSerie.DecimalPrecision = 0
        Me.TxtSerie.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtSerie.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtSerie.Enganche = Nothing
        Me.TxtSerie.EnterEmuleTab = True
        Me.TxtSerie.IsDecimalNegative = False
        Me.TxtSerie.LinkKeyDown = Nothing
        Me.TxtSerie.Location = New System.Drawing.Point(216, 37)
        Me.TxtSerie.MaskFormat = Nothing
        Me.TxtSerie.Name = "TxtSerie"
        Me.TxtSerie.Requiere = False
        Me.TxtSerie.Size = New System.Drawing.Size(44, 20)
        Me.TxtSerie.TabIndex = 12
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(182, 40)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(31, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Serie"
        '
        'BtnBuscar
        '
        Me.BtnBuscar.Location = New System.Drawing.Point(762, 14)
        Me.BtnBuscar.Name = "BtnBuscar"
        Me.BtnBuscar.Size = New System.Drawing.Size(75, 27)
        Me.BtnBuscar.TabIndex = 18
        Me.BtnBuscar.Text = "Buscar"
        Me.BtnBuscar.UseVisualStyleBackColor = True
        '
        'TxtTipoDocDescripcion
        '
        Me.TxtTipoDocDescripcion.BackColor = System.Drawing.Color.White
        Me.TxtTipoDocDescripcion.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtTipoDocDescripcion.DecimalPrecision = 0
        Me.TxtTipoDocDescripcion.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtTipoDocDescripcion.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtTipoDocDescripcion.Enabled = False
        Me.TxtTipoDocDescripcion.Enganche = Nothing
        Me.TxtTipoDocDescripcion.EnterEmuleTab = True
        Me.TxtTipoDocDescripcion.IsDecimalNegative = False
        Me.TxtTipoDocDescripcion.LinkKeyDown = Nothing
        Me.TxtTipoDocDescripcion.Location = New System.Drawing.Point(91, 37)
        Me.TxtTipoDocDescripcion.MaskFormat = Nothing
        Me.TxtTipoDocDescripcion.Name = "TxtTipoDocDescripcion"
        Me.TxtTipoDocDescripcion.Requiere = True
        Me.TxtTipoDocDescripcion.Size = New System.Drawing.Size(85, 20)
        Me.TxtTipoDocDescripcion.TabIndex = 10
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Tipo Doc."
        '
        'TxtTipoDocCodigo
        '
        Me.TxtTipoDocCodigo.BackColor = System.Drawing.Color.White
        Me.TxtTipoDocCodigo.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtTipoDocCodigo.DecimalPrecision = 0
        Me.TxtTipoDocCodigo.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtTipoDocCodigo.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtTipoDocCodigo.Enganche = Nothing
        Me.TxtTipoDocCodigo.EnterEmuleTab = True
        Me.TxtTipoDocCodigo.IsDecimalNegative = False
        Me.TxtTipoDocCodigo.LinkKeyDown = Nothing
        Me.TxtTipoDocCodigo.Location = New System.Drawing.Point(62, 37)
        Me.TxtTipoDocCodigo.MaskFormat = Nothing
        Me.TxtTipoDocCodigo.Name = "TxtTipoDocCodigo"
        Me.TxtTipoDocCodigo.Requiere = False
        Me.TxtTipoDocCodigo.Size = New System.Drawing.Size(28, 20)
        Me.TxtTipoDocCodigo.TabIndex = 9
        '
        'TxtFHasta
        '
        Me.TxtFHasta.IsValueDate = False
        Me.TxtFHasta.Location = New System.Drawing.Point(190, 14)
        Me.TxtFHasta.Mask = "00/00/0000"
        Me.TxtFHasta.Name = "TxtFHasta"
        Me.TxtFHasta.Requiere = False
        Me.TxtFHasta.Size = New System.Drawing.Size(70, 20)
        Me.TxtFHasta.TabIndex = 4
        Me.TxtFHasta.ValidatingType = GetType(Date)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(137, 17)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "F. Hasta"
        '
        'TxtFDesde
        '
        Me.TxtFDesde.IsValueDate = False
        Me.TxtFDesde.Location = New System.Drawing.Point(62, 14)
        Me.TxtFDesde.Mask = "00/00/0000"
        Me.TxtFDesde.Name = "TxtFDesde"
        Me.TxtFDesde.Requiere = False
        Me.TxtFDesde.Size = New System.Drawing.Size(70, 20)
        Me.TxtFDesde.TabIndex = 2
        Me.TxtFDesde.ValidatingType = GetType(Date)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "F. Desde"
        '
        'TxtEmpDescripcion
        '
        Me.TxtEmpDescripcion.BackColor = System.Drawing.Color.White
        Me.TxtEmpDescripcion.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtEmpDescripcion.DecimalPrecision = 0
        Me.TxtEmpDescripcion.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtEmpDescripcion.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtEmpDescripcion.Enabled = False
        Me.TxtEmpDescripcion.Enganche = Nothing
        Me.TxtEmpDescripcion.EnterEmuleTab = True
        Me.TxtEmpDescripcion.IsDecimalNegative = False
        Me.TxtEmpDescripcion.LinkKeyDown = Nothing
        Me.TxtEmpDescripcion.Location = New System.Drawing.Point(399, 14)
        Me.TxtEmpDescripcion.MaskFormat = Nothing
        Me.TxtEmpDescripcion.Name = "TxtEmpDescripcion"
        Me.TxtEmpDescripcion.Requiere = True
        Me.TxtEmpDescripcion.Size = New System.Drawing.Size(357, 20)
        Me.TxtEmpDescripcion.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(265, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Emisor"
        '
        'TxtEmpRucNum
        '
        Me.TxtEmpRucNum.BackColor = System.Drawing.Color.White
        Me.TxtEmpRucNum.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtEmpRucNum.DecimalPrecision = 0
        Me.TxtEmpRucNum.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtEmpRucNum.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtEmpRucNum.Enganche = Me.TxtEmpDescripcion
        Me.TxtEmpRucNum.EnterEmuleTab = True
        Me.TxtEmpRucNum.IsDecimalNegative = False
        Me.TxtEmpRucNum.LinkKeyDown = Nothing
        Me.TxtEmpRucNum.Location = New System.Drawing.Point(314, 14)
        Me.TxtEmpRucNum.MaskFormat = Nothing
        Me.TxtEmpRucNum.Name = "TxtEmpRucNum"
        Me.TxtEmpRucNum.Requiere = False
        Me.TxtEmpRucNum.Size = New System.Drawing.Size(84, 20)
        Me.TxtEmpRucNum.TabIndex = 6
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.Cursor = System.Windows.Forms.Cursors.Default
        Me.GridControl1.Location = New System.Drawing.Point(12, 111)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(900, 274)
        Me.GridControl1.TabIndex = 6
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn11, Me.GridColumn14, Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10, Me.GridColumn12, Me.GridColumn13})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsBehavior.ReadOnly = True
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowDetailButtons = False
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Estado"
        Me.GridColumn11.FieldName = "EstadoDoc_Descripcion"
        Me.GridColumn11.MaxWidth = 80
        Me.GridColumn11.MinWidth = 80
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)})
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 0
        Me.GridColumn11.Width = 80
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "SUNAT"
        Me.GridColumn14.FieldName = "Estado_Descripcion"
        Me.GridColumn14.MaxWidth = 70
        Me.GridColumn14.MinWidth = 70
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = True
        Me.GridColumn14.VisibleIndex = 1
        Me.GridColumn14.Width = 70
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Emisor RUC"
        Me.GridColumn1.FieldName = "Emisor_RUC"
        Me.GridColumn1.MaxWidth = 80
        Me.GridColumn1.MinWidth = 80
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 2
        Me.GridColumn1.Width = 80
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Doc. Sunat"
        Me.GridColumn2.FieldName = "TpDc_Codigo_St"
        Me.GridColumn2.MaxWidth = 30
        Me.GridColumn2.MinWidth = 30
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 3
        Me.GridColumn2.Width = 30
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Doc. Descripción"
        Me.GridColumn3.FieldName = "TpDc_Descripcion"
        Me.GridColumn3.MinWidth = 100
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 4
        Me.GridColumn3.Width = 100
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Serie"
        Me.GridColumn4.FieldName = "Dvt_VTSerie"
        Me.GridColumn4.MaxWidth = 40
        Me.GridColumn4.MinWidth = 40
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 5
        Me.GridColumn4.Width = 40
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Número"
        Me.GridColumn5.FieldName = "Dvt_VTNumer"
        Me.GridColumn5.MaxWidth = 60
        Me.GridColumn5.MinWidth = 60
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 6
        Me.GridColumn5.Width = 60
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "F.Emisión"
        Me.GridColumn6.FieldName = "Dvt_Fecha_Emision"
        Me.GridColumn6.MaxWidth = 70
        Me.GridColumn6.MinWidth = 70
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 7
        Me.GridColumn6.Width = 70
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Mon."
        Me.GridColumn7.FieldName = "Dvt_Simbol_Moneda"
        Me.GridColumn7.MaxWidth = 40
        Me.GridColumn7.MinWidth = 40
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 8
        Me.GridColumn7.Width = 40
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Importe"
        Me.GridColumn8.DisplayFormat.FormatString = "{0:#,##0.00#}"
        Me.GridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn8.FieldName = "Dvt_Total_Importe"
        Me.GridColumn8.MaxWidth = 60
        Me.GridColumn8.MinWidth = 60
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 9
        Me.GridColumn8.Width = 60
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Cliente Doc."
        Me.GridColumn9.FieldName = "Cliente_Documento_Numero"
        Me.GridColumn9.MaxWidth = 80
        Me.GridColumn9.MinWidth = 80
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 10
        Me.GridColumn9.Width = 80
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Cliente Razón Social"
        Me.GridColumn10.FieldName = "Cliente_RazonSocial_Nombre"
        Me.GridColumn10.MinWidth = 150
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 11
        Me.GridColumn10.Width = 217
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "Correo Enviado"
        Me.GridColumn12.FieldName = "Doc_Correo_Enviado"
        Me.GridColumn12.MaxWidth = 80
        Me.GridColumn12.MinWidth = 80
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 12
        Me.GridColumn12.Width = 80
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "Correo Leido"
        Me.GridColumn13.FieldName = "Doc_Correo_EstaLeido"
        Me.GridColumn13.MaxWidth = 80
        Me.GridColumn13.MinWidth = 80
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 13
        Me.GridColumn13.Width = 80
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1, Me.Bar3})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BtnEnviarSunat, Me.BtnEnviarCorreoCliente, Me.BtnVersiones, Me.BtnVerXML, Me.BtnVerPDF, Me.BtnVerCDR, Me.BtnVerSunat, Me.BtnVerOSE})
        Me.BarManager1.MaxItemId = 10
        Me.BarManager1.StatusBar = Me.Bar3
        '
        'Bar1
        '
        Me.Bar1.BarName = "Herramientas"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BtnEnviarSunat), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnEnviarCorreoCliente), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnVersiones), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnVerXML), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnVerPDF), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnVerCDR), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.BtnVerSunat, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.BtnVerOSE, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.Bar1.Text = "Herramientas"
        '
        'BtnEnviarSunat
        '
        Me.BtnEnviarSunat.Caption = "Enviar a SUNAT"
        Me.BtnEnviarSunat.Glyph = CType(resources.GetObject("BtnEnviarSunat.Glyph"), System.Drawing.Image)
        Me.BtnEnviarSunat.Id = 0
        Me.BtnEnviarSunat.Name = "BtnEnviarSunat"
        Me.BtnEnviarSunat.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnEnviarCorreoCliente
        '
        Me.BtnEnviarCorreoCliente.Caption = "Enviar a Cliente"
        Me.BtnEnviarCorreoCliente.Glyph = CType(resources.GetObject("BtnEnviarCorreoCliente.Glyph"), System.Drawing.Image)
        Me.BtnEnviarCorreoCliente.Id = 2
        Me.BtnEnviarCorreoCliente.Name = "BtnEnviarCorreoCliente"
        Me.BtnEnviarCorreoCliente.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnVersiones
        '
        Me.BtnVersiones.Caption = "Versiones"
        Me.BtnVersiones.Glyph = CType(resources.GetObject("BtnVersiones.Glyph"), System.Drawing.Image)
        Me.BtnVersiones.Id = 3
        Me.BtnVersiones.Name = "BtnVersiones"
        Me.BtnVersiones.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnVerXML
        '
        Me.BtnVerXML.Caption = "Ver XML"
        Me.BtnVerXML.Glyph = CType(resources.GetObject("BtnVerXML.Glyph"), System.Drawing.Image)
        Me.BtnVerXML.Id = 5
        Me.BtnVerXML.Name = "BtnVerXML"
        Me.BtnVerXML.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnVerPDF
        '
        Me.BtnVerPDF.Caption = "Ver PDF"
        Me.BtnVerPDF.Glyph = CType(resources.GetObject("BtnVerPDF.Glyph"), System.Drawing.Image)
        Me.BtnVerPDF.Id = 6
        Me.BtnVerPDF.Name = "BtnVerPDF"
        Me.BtnVerPDF.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnVerCDR
        '
        Me.BtnVerCDR.Caption = "Ver CDR"
        Me.BtnVerCDR.Glyph = CType(resources.GetObject("BtnVerCDR.Glyph"), System.Drawing.Image)
        Me.BtnVerCDR.Id = 7
        Me.BtnVerCDR.Name = "BtnVerCDR"
        Me.BtnVerCDR.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnVerSunat
        '
        Me.BtnVerSunat.ActAsDropDown = True
        Me.BtnVerSunat.Caption = "Ver Estado SUNAT"
        Me.BtnVerSunat.DropDownControl = Me.GalleryDropDown1
        Me.BtnVerSunat.Glyph = CType(resources.GetObject("BtnVerSunat.Glyph"), System.Drawing.Image)
        Me.BtnVerSunat.Id = 8
        Me.BtnVerSunat.Name = "BtnVerSunat"
        '
        'GalleryDropDown1
        '
        Me.GalleryDropDown1.Manager = Me.BarManager1
        Me.GalleryDropDown1.Name = "GalleryDropDown1"
        '
        'BtnVerOSE
        '
        Me.BtnVerOSE.Caption = "Ver Estado OSE"
        Me.BtnVerOSE.Glyph = CType(resources.GetObject("BtnVerOSE.Glyph"), System.Drawing.Image)
        Me.BtnVerOSE.Id = 9
        Me.BtnVerOSE.Name = "BtnVerOSE"
        '
        'Bar3
        '
        Me.Bar3.BarName = "Barra de estado"
        Me.Bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.Bar3.DockCol = 0
        Me.Bar3.DockRow = 0
        Me.Bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar3.OptionsBar.AllowQuickCustomization = False
        Me.Bar3.OptionsBar.DrawDragBorder = False
        Me.Bar3.OptionsBar.UseWholeRow = True
        Me.Bar3.Text = "Barra de estado"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(923, 47)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 391)
        Me.barDockControlBottom.Size = New System.Drawing.Size(923, 23)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 47)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 344)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(923, 47)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 344)
        '
        'FrmDocumento_Busqueda
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(923, 414)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "FrmDocumento_Busqueda"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Busqueda de documentos"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GalleryDropDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BtnBuscar As System.Windows.Forms.Button
    Friend WithEvents TxtTipoDocDescripcion As dhsoft.TextBoxNew
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TxtTipoDocCodigo As dhsoft.TextBoxNew
    Friend WithEvents TxtFHasta As dhsoft.MaskedTextBoxNew
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TxtFDesde As dhsoft.MaskedTextBoxNew
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TxtEmpDescripcion As dhsoft.TextBoxNew
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TxtEmpRucNum As dhsoft.TextBoxNew
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TxtNumero As dhsoft.TextBoxNew
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TxtSerie As dhsoft.TextBoxNew
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BtnEnviarSunat As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnEnviarCorreoCliente As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnVersiones As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Bar3 As DevExpress.XtraBars.Bar
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BtnVerXML As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnVerPDF As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnVerCDR As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnVerSunat As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents GalleryDropDown1 As DevExpress.XtraBars.Ribbon.GalleryDropDown
    Friend WithEvents BtnVerOSE As DevExpress.XtraBars.BarButtonItem
End Class
