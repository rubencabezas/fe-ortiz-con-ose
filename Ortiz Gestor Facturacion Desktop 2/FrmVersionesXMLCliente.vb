﻿Public Class FrmVersionesXMLCliente
    Public DocumentoElectronico As eProvisionFacturacion
    Private Sub FrmVersionesXMLCliente_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim Dt As DataTable
        Dim log As New Log_FACTE_DOCUMENTOS
        Dt = log.Buscar_Documento_Versiones(DocumentoElectronico)
        If Dt.Rows.Count > 0 Then
            GridControl1.DataSource = Dt
        End If
    End Sub

    Private Sub BtnVerArchivo_Click(sender As Object, e As EventArgs) Handles BtnVerArchivo.Click
        If Me.GridView1.GetFocusedRowCellValue("Archivo") Is Nothing Then
            Return
        End If
        Using f As New FrmVersionesXMLCliente_Visor
            f.FileName = DocumentoElectronico.Path_Root_App & "\" & DocumentoElectronico.Emisor_RUC & "\xml_pos\" & Me.GridView1.GetFocusedRowCellValue("Archivo").ToString()
            f.ShowDialog()
        End Using
        'MessageBox.Show("" + Me.GridView1.GetFocusedRowCellValue("Archivo").ToString())
    End Sub
End Class