﻿Public Class FrmEmpresa_Lista_Editable
    Public Generales As New List(Of eEmpresa)
    Private Sub FrmEmpresa_Lista_Editable_Load(sender As Object, e As EventArgs) Handles Me.Load
        CargarLista()
    End Sub
    Sub CargarLista()
        Dim log As New Log_EMPRESA
        Generales = log.Buscar(New eEmpresa)
        DgvGeneral.DataSource = Nothing
        DgvGeneral.DataSource = Generales
        VwDgv.Focus()
        VwDgv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.AutoFilterRowHandle
    End Sub

    Private Sub BtnNuevo_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnNuevo.ItemClick
        Using f As New FrmEmpresa_Edicion
            If f.ShowDialog = DialogResult.OK Then
                CargarLista()
            End If
        End Using
    End Sub

    Private Sub BtnModificar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnModificar.ItemClick
        If VwDgv.GetFocusedDataSourceRowIndex > -1 Then
            Using f As New FrmEmpresa_Edicion
                f.Empresa = Generales(VwDgv.GetFocusedDataSourceRowIndex)
                If f.ShowDialog = DialogResult.OK Then
                    CargarLista()
                End If
            End Using
        End If
    End Sub

    Private Sub BtnEliminar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnEliminar.ItemClick
        If MessageBox.Show("¿Esta Ud. seguro de eliminar?", "Eliminar", MessageBoxButtons.YesNo) Then

        End If
    End Sub
    Private Sub BtnCertificados_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnCertificados.ItemClick
        If VwDgv.GetFocusedDataSourceRowIndex > -1 Then
            Using f As New FrmEmpresa_Certificado_Edicion
                f.Empresa = Generales(VwDgv.GetFocusedDataSourceRowIndex)
                f.ShowDialog()
            End Using
        End If
    End Sub
    Private Sub BtnProgramacionEnvios_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnProgramacionEnvios.ItemClick
        If VwDgv.GetFocusedDataSourceRowIndex > -1 Then
            Using f As New FrmTarea_Busqueda
                f.Empresa = Generales(VwDgv.GetFocusedDataSourceRowIndex)
                f.ShowDialog()
            End Using
        End If
    End Sub
    Private Sub BtnSalir_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnSalir.ItemClick
        Me.Close()
    End Sub
End Class