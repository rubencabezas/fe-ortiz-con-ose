﻿Public Class FrmEtapa_Busqueda
    Public Generales As New List(Of eEtapa)
    Private Sub FrmEtapa_Busqueda_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim log As New Log_FACTE_ETAPA
        Generales = log.Buscar(New eEtapa)
        DgvGeneral.DataSource = Nothing
        DgvGeneral.DataSource = Generales
        VwDgv.Focus()
        VwDgv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.AutoFilterRowHandle
    End Sub
    Private Sub BtnSeleccionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSeleccionar.ItemClick
        If DgvGeneral.ViewCollection.Count > 0 Then 'And GridView1.GetFocusedDataSourceRowIndex > -1 
            DialogResult = DialogResult.OK
        End If
    End Sub

    Private Sub DgvGeneral_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DgvGeneral.DoubleClick
        BtnSeleccionar.PerformClick()
    End Sub
    Private Sub DgvGeneral_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DgvGeneral.KeyDown
        If e.KeyCode = Keys.Enter Then
            BtnSeleccionar.PerformClick()
            e.SuppressKeyPress = True
        End If
    End Sub
    Private Sub DgvGeneral_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DgvGeneral.KeyPress
        If e.KeyChar = Chr(13) Then
            BtnSeleccionar.PerformClick()
            e.KeyChar = Convert.ToChar(Keys.None)
        End If
    End Sub

    Private Sub BtnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSalir.ItemClick
        Close()
    End Sub
End Class