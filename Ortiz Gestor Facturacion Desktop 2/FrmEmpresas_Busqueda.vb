﻿Public Class FrmEmpresas_Busqueda
    Public Generales As New List(Of eEmpresa)
    Private Sub FrmEmpresas_Busqueda_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim log As New Log_EMPRESA
        Generales = log.Buscar(New eEmpresa)
        DgvGeneral.DataSource = Nothing
        DgvGeneral.DataSource = Generales
        VwDgv.Focus()
        VwDgv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.AutoFilterRowHandle
    End Sub

    Private Sub BtnSeleccionar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnSeleccionar.ItemClick
        If DgvGeneral.ViewCollection.Count > 0 Then 'And GridView1.GetFocusedDataSourceRowIndex > -1 
            DialogResult = DialogResult.OK
        End If
    End Sub

    Private Sub BtnSalir_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnSalir.ItemClick
        Me.Close()
    End Sub
    Private Sub DgvGeneral_DoubleClick(sender As Object, e As EventArgs) Handles DgvGeneral.DoubleClick
        BtnSeleccionar.PerformClick()
    End Sub

    Private Sub DgvGeneral_KeyDown(sender As Object, e As KeyEventArgs) Handles DgvGeneral.KeyDown
        If e.KeyCode = Keys.Enter Then
            BtnSeleccionar.PerformClick()
            e.SuppressKeyPress = True
        End If
    End Sub

    Private Sub DgvGeneral_KeyPress(sender As Object, e As KeyPressEventArgs) Handles DgvGeneral.KeyPress
        If e.KeyChar = Chr(13) Then
            BtnSeleccionar.PerformClick()
            e.KeyChar = Convert.ToChar(Keys.None)
        End If
    End Sub
End Class