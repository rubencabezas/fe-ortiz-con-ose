﻿Imports System.Data.SqlClient

Public Class Dat_FACTE_ETAPA
    Public Function Buscar(ByVal pEntidad As eEtapa) As List(Of eEtapa)
        Dim ListaItems As New List(Of eEtapa)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_ETAPA_LISTA", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEtapa_Codigo", SqlDbType.Char, 3).Value = pEntidad.Etapa_Codigo
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read()
                            ListaItems.Add(New eEtapa With {.Etapa_Codigo = Dr.GetString(0), .Etapa_Descripcion = Dr.GetString(1)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
