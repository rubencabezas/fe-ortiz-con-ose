﻿Imports System.Data.SqlClient

Public Class Dat_VTS_TABLAS_DET
    Public Function Buscar(ByVal Clas_Enti As eEstado) As List(Of eEstado)
        Dim ListaItems As New List(Of eEstado)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("paVTS_GNL_TABLAS_DET_Find", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cmd.Parameters.Add("@PTAC_CODIGO", SqlDbType.Char, 4).Value = Clas_Enti.TAC_CODIGO
                    Cmd.Parameters.Add("@PTAD_CODIGO", SqlDbType.Char, 4).Value = Clas_Enti.TAD_CODIGO
                    Cmd.Parameters.Add("@PTAD_CODIGO_OFICIAL", SqlDbType.Char, 6).Value = Clas_Enti.TAD_CODIGO_OFICIAL
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New eEstado With {.TAC_CODIGO = Dr.GetString(0), .TAD_CODIGO = Dr.GetString(1), .TAD_CODIGO_OFICIAL = Dr.GetString(2), .TAD_DESCRIPCION = Dr.GetString(3), .TAD_ABREVIADO = Dr.GetString(4)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
