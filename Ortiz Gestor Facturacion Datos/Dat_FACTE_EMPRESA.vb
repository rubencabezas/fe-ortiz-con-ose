﻿Imports System.Data.SqlClient

Public Class Dat_FACTE_EMPRESA
    Public Sub Buscar_Configuracion(ByVal pEntidad As eEmpresa)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_EMPRESA_ENVIA_RESUMENES", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@Emp_RUC", SqlDbType.Char, 11).Value = pEntidad.Emp_NumRuc
                    Cmd.Parameters.Add("@EnviaResumen", SqlDbType.Bit).Direction = ParameterDirection.Output
                    Cmd.Parameters.Add("@Etapa_Codigo", SqlDbType.Char, 3).Direction = ParameterDirection.Output
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                    pEntidad.Emp_EnviaResumenBoletas = Cmd.Parameters("@EnviaResumen").Value
                    pEntidad.Etapa_Codigo = Cmd.Parameters("@Etapa_Codigo").Value
                End Using
            End Using
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Public Function Buscar_Empresas_Envio_Resumenes() As String
        Dim Lst As String = ""
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTO_EMPRESAS_SUMMARY", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            Lst = Lst & Dr.GetString(0) & ","
                        End While
                    End Using
                End Using
            End Using
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return Lst
    End Function
    Public Sub Insertar(ByVal pEntidad As eEmpresa)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_EMPRESA_INSERT", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_RUC", SqlDbType.Char, 11).Value = pEntidad.Emp_NumRuc
                    Cmd.Parameters.Add("@PEmp_NombreRazonSocial", SqlDbType.VarChar, 150).Value = pEntidad.Emp_NombreRazonSocial
                    'Cmd.Parameters.Add("@PEmp_EnviaResumenBoletas", SqlDbType.Bit).Value = pEntidad.Emp_EnviaResumenBoletas
                    Cmd.Parameters.Add("@PSunat_Usuario", SqlDbType.VarChar, 50).Value = pEntidad.Sunat_Usuario
                    Cmd.Parameters.Add("@PSunat_Contrasena", SqlDbType.VarChar, 50).Value = pEntidad.Sunat_Contrasena
                    Cmd.Parameters.Add("@PSunat_Autorizacion", SqlDbType.VarChar, 50).Value = pEntidad.Sunat_Autorizacion
                    Cmd.Parameters.Add("@PEmisor_Web_Visualizacion", SqlDbType.VarChar, 50).Value = pEntidad.Emisor_Web_Visualizacion
                    Cmd.Parameters.Add("@PCorreo_Usuario", SqlDbType.VarChar, 50).Value = pEntidad.Correo_Usuario
                    Cmd.Parameters.Add("@PCorreo_Contrasena", SqlDbType.VarChar, 50).Value = pEntidad.Correo_Contrasena
                    Cmd.Parameters.Add("@PCorreo_ServidorSMTP", SqlDbType.VarChar, 50).Value = pEntidad.Correo_ServidorSMTP
                    Cmd.Parameters.Add("@PCorreo_ServidorPuerto", SqlDbType.VarChar, 6).Value = pEntidad.Correo_ServidorPuerto
                    Cmd.Parameters.Add("@PCorreo_ServidorSSL", SqlDbType.Bit).Value = pEntidad.Correo_ServidorSSL
                    Cmd.Parameters.Add("@PEtapa_Codigo", SqlDbType.Char, 3).Value = pEntidad.Etapa_Codigo

                    Cmd.Parameters.Add("@PPath_Root_App", SqlDbType.VarChar, 150).Value = pEntidad.Path_Root_App
                    'Cmd.Parameters.Add("@PPathLocal", SqlDbType.VarChar, 150).Value = pEntidad.PathLocal
                    'Cmd.Parameters.Add("@PPathServer", SqlDbType.VarChar, 150).Value = pEntidad.PathServer

                    Cmd.Parameters.Add("@PFTP_Direccion", SqlDbType.VarChar, 50).Value = pEntidad.FTP_Direccion
                    Cmd.Parameters.Add("@PFTP_Carpeta", SqlDbType.VarChar, 200).Value = pEntidad.FTP_Carpeta
                    Cmd.Parameters.Add("@PFTP_Usuario", SqlDbType.VarChar, 50).Value = pEntidad.FTP_Usuario
                    Cmd.Parameters.Add("@PFTP_Contrasena", SqlDbType.VarChar, 50).Value = pEntidad.FTP_Contrasena
                    Cmd.Parameters.Add("@PActualizarClientes", SqlDbType.Bit).Value = pEntidad.ActualizarClientes
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Public Sub Actualizar(ByVal pEntidad As eEmpresa)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_EMPRESA_UPDATE", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_RUC", SqlDbType.Char, 11).Value = pEntidad.Emp_NumRuc
                    Cmd.Parameters.Add("@PEmp_NombreRazonSocial", SqlDbType.VarChar, 150).Value = pEntidad.Emp_NombreRazonSocial
                    'Cmd.Parameters.Add("@PEmp_EnviaResumenBoletas", SqlDbType.Bit).Value = pEntidad.Emp_EnviaResumenBoletas
                    Cmd.Parameters.Add("@PSunat_Usuario", SqlDbType.VarChar, 50).Value = pEntidad.Sunat_Usuario
                    Cmd.Parameters.Add("@PSunat_Contrasena", SqlDbType.VarChar, 50).Value = pEntidad.Sunat_Contrasena
                    Cmd.Parameters.Add("@PSunat_Autorizacion", SqlDbType.VarChar, 50).Value = pEntidad.Sunat_Autorizacion
                    Cmd.Parameters.Add("@PEmisor_Web_Visualizacion", SqlDbType.VarChar, 50).Value = pEntidad.Emisor_Web_Visualizacion
                    Cmd.Parameters.Add("@PCorreo_Usuario", SqlDbType.VarChar, 50).Value = pEntidad.Correo_Usuario
                    Cmd.Parameters.Add("@PCorreo_Contrasena", SqlDbType.VarChar, 50).Value = pEntidad.Correo_Contrasena
                    Cmd.Parameters.Add("@PCorreo_ServidorSMTP", SqlDbType.VarChar, 50).Value = pEntidad.Correo_ServidorSMTP
                    Cmd.Parameters.Add("@PCorreo_ServidorPuerto", SqlDbType.VarChar, 6).Value = pEntidad.Correo_ServidorPuerto
                    Cmd.Parameters.Add("@PCorreo_ServidorSSL", SqlDbType.Bit).Value = pEntidad.Correo_ServidorSSL
                    Cmd.Parameters.Add("@PEtapa_Codigo", SqlDbType.Char, 3).Value = pEntidad.Etapa_Codigo

                    Cmd.Parameters.Add("@PPath_Root_App", SqlDbType.VarChar, 150).Value = pEntidad.Path_Root_App
                    'Cmd.Parameters.Add("@PPathLocal_XMLCliente", SqlDbType.VarChar, 150).Value = pEntidad.PathLocal_XMLCliente
                    'Cmd.Parameters.Add("@PPathLocal", SqlDbType.VarChar, 150).Value = pEntidad.PathLocal
                    'Cmd.Parameters.Add("@PPathServer", SqlDbType.VarChar, 150).Value = pEntidad.PathServer

                    Cmd.Parameters.Add("@PFTP_Direccion", SqlDbType.VarChar, 50).Value = pEntidad.FTP_Direccion
                    Cmd.Parameters.Add("@PFTP_Carpeta", SqlDbType.VarChar, 200).Value = pEntidad.FTP_Carpeta
                    Cmd.Parameters.Add("@PFTP_Usuario", SqlDbType.VarChar, 50).Value = pEntidad.FTP_Usuario
                    Cmd.Parameters.Add("@PFTP_Contrasena", SqlDbType.VarChar, 50).Value = pEntidad.FTP_Contrasena
                    Cmd.Parameters.Add("@PActualizarClientes", SqlDbType.Bit).Value = pEntidad.ActualizarClientes
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
End Class
