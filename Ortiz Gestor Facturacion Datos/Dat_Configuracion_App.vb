﻿Imports System.Data.SqlClient

Public Class Dat_Configuracion_App
    Public Sub Insertar(ByVal pEntidad As eConfiguracionApp)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_CFG_APPLICATION_INSERT_UPDATE", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PApp_Replicacion", SqlDbType.Bit).Value = pEntidad.App_Replicacion
                    Cmd.Parameters.Add("@PApp_Replicacion_Servidor", SqlDbType.VarChar, 100).Value = pEntidad.App_Replicacion_Servidor
                    Cmd.Parameters.Add("@PApp_Replicacion_BD", SqlDbType.VarChar, 50).Value = pEntidad.App_Replicacion_BD
                    Cmd.Parameters.Add("@PApp_Replicacion_Usuario", SqlDbType.VarChar, 50).Value = pEntidad.App_Replicacion_Usuario
                    Cmd.Parameters.Add("@PApp_Replicacion_Contrasenia", SqlDbType.VarChar, 50).Value = pEntidad.App_Replicacion_Contrasenia
                    Cmd.Parameters.Add("@PApp_Replicacion_Prog_Hora_Establecido", SqlDbType.Bit).Value = pEntidad.App_Replicacion_Prog_Hora_Establecido
                    Cmd.Parameters.Add("@PApp_Replicacion_Prog_Hora_CadaRangoHoras", SqlDbType.Bit).Value = pEntidad.App_Replicacion_Prog_Hora_CadaRangoHoras
                    Cmd.Parameters.Add("@PApp_Replicacion_Prog_Hora", SqlDbType.Int).Value = pEntidad.App_Replicacion_Prog_Hora
                    Cmd.Parameters.Add("@PApp_Replicacion_Prog_Hora_Desde", SqlDbType.Time).Value = pEntidad.App_Replicacion_Prog_Hora_Desde
                    Cmd.Parameters.Add("@PApp_Replicacion_Prog_Hora_Hasta", SqlDbType.Time).Value = pEntidad.App_Replicacion_Prog_Hora_Hasta
                    Cmd.Parameters.Add("@PApp_Replicacion_RutaPrincipalArchivos", SqlDbType.VarChar, 250).Value = pEntidad.App_Replicacion_RutaPrincipalArchivos
                    Cmd.Parameters.Add("@PUsuario", SqlDbType.VarChar, 50).Value = "Sistema"
                    Cmd.Parameters.Add("@PEquipo", SqlDbType.VarChar, 50).Value = "Sistema"
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Public Function Buscar() As List(Of eConfiguracionApp)
        Dim rCertificados As New List(Of eConfiguracionApp)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection())
                Using Cmd As New SqlCommand("UP_CFG_APPLICATION_SELECT", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            rCertificados.Add(New eConfiguracionApp With {.App_Replicacion = Dr.GetBoolean(0),
                                                                          .App_Replicacion_Servidor = Dr.GetString(1),
                                                                          .App_Replicacion_BD = Dr.GetString(2),
                                                                          .App_Replicacion_Usuario = Dr.GetString(3),
                                                                          .App_Replicacion_Contrasenia = Dr.GetString(4),
                                                                          .App_Replicacion_Prog_Hora_Establecido = Dr.GetBoolean(5),
                                                                          .App_Replicacion_Prog_Hora_CadaRangoHoras = Dr.GetBoolean(6),
                                                                          .App_Replicacion_Prog_Hora = Dr.GetInt32(7),
                                                                          .App_Replicacion_Prog_Hora_Desde = Dr.GetTimeSpan(8),
                                                                          .App_Replicacion_Prog_Hora_Hasta = Dr.GetTimeSpan(9),
                                                                          .App_Replicacion_RutaPrincipalArchivos = Dr.GetString(10),
                                                                          .App_Es_Hora_Replicar = Dr.GetBoolean(11)})
                        End While
                    End Using
                End Using
            End Using
            Return rCertificados
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
