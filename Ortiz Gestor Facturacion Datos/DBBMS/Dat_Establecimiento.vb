﻿Imports System.Data.SqlClient
Public Class Dat_Establecimiento
    Public Function Buscar(ByVal Clas_Enti As Ent_ESTABLECIMIENTO) As List(Of Ent_ESTABLECIMIENTO)
        Dim ListaItems As New List(Of Ent_ESTABLECIMIENTO)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnectionDBMBS)
                Using Cmd As New SqlCommand("paVTS_VTS_ESTABLECIMIENTO_Find", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PEst_Codigo", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Clas_Enti.Est_Codigo), Nothing, Clas_Enti.Est_Codigo)
                    Cmd.Parameters.Add("@PEst_Nombre", SqlDbType.VarChar, 50).Value = IIf(String.IsNullOrEmpty(Clas_Enti.Est_Nombre), Nothing, Clas_Enti.Est_Nombre)
                    Cmd.Parameters.Add("@PEst_CodTipEstabl", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Clas_Enti.Est_CodTipEstabl), Nothing, Clas_Enti.Est_CodTipEstabl)
                    Cmd.Parameters.Add("@PEst_CodCondicion", SqlDbType.Char, 4).Value = IIf(String.IsNullOrEmpty(Clas_Enti.Est_CodCondicion), Nothing, Clas_Enti.Est_CodCondicion)
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New Ent_Establecimiento With {.Est_Codigo = Dr.GetString(0),
                                                                         .Est_Nombre = Dr.GetString(1),
                                                                         .Est_CodTipEstabl = Dr.GetString(2),
                                                                         .Est_OfiTipEstabl = Dr.GetString(3),
                                                                         .Est_DesTipEstabl = Dr.GetString(4),
                                                                         .Est_CodCondicion = Dr.GetString(5),
                                                                         .Est_OfiCondicion = Dr.GetString(6),
                                                                         .Est_DesCondicion = Dr.GetString(7),
                                                                         .Est_Direccion = Dr.GetString(8),
                                                                         .Est_CodZona = Dr.GetString(9),
                                                                         .Est_OfiZona = Dr.GetString(10),
                                                                         .Est_DesZona = Dr.GetString(11),
                                                                         .Est_NomZona = Dr.GetString(12),
                                                                         .Est_Telefono = Dr.GetString(13),
                                                                         .Est_Fax = Dr.GetString(14),
                                                                         .Est_Referenc = Dr.GetString(15),
                                                                         .Est_CodDpto = Dr.GetString(16),
                                                                         .Est_DesDpto = Dr.GetString(17),
                                                                         .Est_CodProv = Dr.GetString(18),
                                                                         .Est_DesProv = Dr.GetString(19),
                                                                         .Est_CodDist = Dr.GetString(20),
                                                                         .Est_DesDist = Dr.GetString(21)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function Buscar_Direccion(ByVal Clas_Enti As Ent_Establecimiento) As List(Of Ent_Establecimiento)
        Dim ListaItems As New List(Of Ent_Establecimiento)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnectionDBMBS)
                Using Cmd As New SqlCommand("pa_BUSCAR_DIRECCION_ESTABLECIMIENTO", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cmd.Parameters.Add("@PEmpresa", SqlDbType.Char, 3).Value = Clas_Enti.Emp_cCodigo
                    Cmd.Parameters.Add("@PPtoVenta", SqlDbType.Char, 4).Value = IIf(String.IsNullOrWhiteSpace(Clas_Enti.Est_PDV), DBNull.Value, Clas_Enti.Est_PDV)
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New Ent_Establecimiento With {.Est_Direccion = Dr.GetString(0), .Est_Codigo = Dr.GetString(1), .Est_Nombre = Dr.GetString(2)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
