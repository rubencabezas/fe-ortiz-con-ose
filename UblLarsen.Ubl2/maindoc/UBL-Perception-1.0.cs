﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UblLarsen.Ubl2
{
    using UblLarsen.Ubl2.Cac;


    /// <summary>
    ///  The document used to request payment.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("ublxsd", "2.1.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "urn:sunat:names:specification:ubl:peru:schema:xsd:Perception-1")]
    [System.Xml.Serialization.XmlRootAttribute("Perception", Namespace = "urn:sunat:names:specification:ubl:peru:schema:xsd:Perception-1", IsNullable = false)]
    public partial class PerceptionType : UblBaseDocumentType
    {
        private Udt.IdentifierType idField;
        private Udt.DateType issueDateField;
        private SignatureType[] signatureField;
        private PartyType agentPartyTypeField;
        private PartyType receiverPartyField;
        private Udt.CodeType sunatPerceptionSystemCodeField;
        private Udt.PercentType sunatPerceptionPercentField;
        private Udt.TextType noteField;
        private Udt.AmountType totalInvoiceAmountField;
        private Udt.AmountType sunatTotalCashedField;
        private PerceptionDocumentReferenceType[] sunatPerceptionDocumentReference;
        /// <summary>
        ///  An identifier for the Invoice assigned by the Creditor.
        /// </summary>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")]
        public Udt.IdentifierType ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <summary>
        ///  The date assigned by the Creditor on which the Invoice was issued.
        /// </summary>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")]
        public Udt.DateType IssueDate
        {
            get
            {
                return this.issueDateField;
            }
            set
            {
                this.issueDateField = value;
            }
        }

        /// <summary>
        ///  An association to Signature.
        /// </summary>
        [System.Xml.Serialization.XmlElementAttribute("Signature", Namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2")]
        public SignatureType[] Signature
        {
            get
            {
                return this.signatureField;
            }
            set
            {
                this.signatureField = value;
            }
        }

        /// <summary>
        ///  An association to the Accounting Supplier Party.
        /// </summary>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2")]
        public PartyType AgentParty
        {
            get
            {
                return this.agentPartyTypeField;
            }
            set
            {
                this.agentPartyTypeField = value;
            }
        }


        [System.Xml.Serialization.XmlElementAttribute(Namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2")]
        public PartyType ReceiverParty
        {
            get
            {
                return this.receiverPartyField;
            }
            set
            {
                this.receiverPartyField = value;
            }
        }

        /// <summary>
        ///  Code specifying the type of the Invoice.
        /// </summary>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")]
        public Udt.CodeType SUNATPerceptionSystemCode
        {
            get
            {
                return this.sunatPerceptionSystemCodeField;
            }
            set
            {
                this.sunatPerceptionSystemCodeField = value;
            }
        }

        /// <summary>
        ///  Code specifying the type of the Invoice.
        /// </summary>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")]
        public Udt.PercentType SUNATPerceptionPercent
        {
            get
            {
                return this.sunatPerceptionPercentField;
            }
            set
            {
                this.sunatPerceptionPercentField = value;
            }
        }

        /// <summary>
        ///  Code specifying the type of the Invoice.
        /// </summary>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")]
        public Udt.TextType Note
        {
            get
            {
                return this.noteField;
            }
            set
            {
                this.noteField = value;
            }
        }
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")]
        public Udt.AmountType TotalInvoiceAmount
        {
            get
            {
                return this.totalInvoiceAmountField;
            }
            set
            {
                this.totalInvoiceAmountField = value;
            }
        }

        ///<summary>
        /// An association to one or more Invoice Lines.
        ///</summary>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")]
        public Udt.AmountType SUNATTotalCashed
        {
            get
            {
                return this.sunatTotalCashedField;
            }
            set
            {
                this.sunatTotalCashedField = value;
            }
        }

        ///<summary>
        /// An association to one or more Invoice Lines.
        ///</summary>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")]
        public PerceptionDocumentReferenceType[] SUNATPerceptionDocumentReference
        {
            get
            {
                return this.sunatPerceptionDocumentReference;
            }
            set
            {
                this.sunatPerceptionDocumentReference = value;
            }
        }
    }
}
