﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UblLarsen.Ubl2
{
    using UblLarsen.Ubl2.Cac;

    //urn:oasis:names:specification:ubl:schema:xsd:SummaryDocuments-1
    /// <summary>
    ///  The document used to request payment.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("ublxsd", "2.1.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "urn:sunat:names:specification:ubl:peru:schema:xsd:VoidedDocuments-1")]
    [System.Xml.Serialization.XmlRootAttribute("VoidedDocuments", Namespace = "urn:sunat:names:specification:ubl:peru:schema:xsd:VoidedDocuments-1", IsNullable = false)]
    public partial class VoidedDocumentsType : UblBaseDocumentType
    {
        private Udt.IdentifierType idField;
        private Udt.DateType ReferenceDateField;
        private Udt.DateType issueDateField;
        private SignatureType[] signatureField;
        private SupplierPartyType accountingSupplierPartyField;
        private VoidedDocumentsLineType[] invoiceLineField;
        /// <summary>
        ///  An identifier for the Invoice assigned by the Creditor.
        /// </summary>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")]
        public Udt.IdentifierType ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <summary>
        ///  The date assigned by the Creditor on which the Invoice was issued.
        /// </summary>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")]
        public Udt.DateType ReferenceDate
        {
            get
            {
                return this.ReferenceDateField;
            }
            set
            {
                this.ReferenceDateField = value;
            }
        }


        /// <summary>
        ///  The date assigned by the Creditor on which the Invoice was issued.
        /// </summary>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")]
        public Udt.DateType IssueDate
        {
            get
            {
                return this.issueDateField;
            }
            set
            {
                this.issueDateField = value;
            }
        }

        /// <summary>
        ///  An association to Signature.
        /// </summary>
        [System.Xml.Serialization.XmlElementAttribute("Signature", Namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2")]
        public SignatureType[] Signature
        {
            get
            {
                return this.signatureField;
            }
            set
            {
                this.signatureField = value;
            }
        }

        /// <summary>
        ///  An association to the Accounting Supplier Party.
        /// </summary>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2")]
        public SupplierPartyType AccountingSupplierParty
        {
            get
            {
                return this.accountingSupplierPartyField;
            }
            set
            {
                this.accountingSupplierPartyField = value;
            }
        }

         ///<summary>
         /// An association to one or more Invoice Lines.
         ///</summary>
        [System.Xml.Serialization.XmlElement("VoidedDocumentsLine", Namespace = "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")]
        public VoidedDocumentsLineType[] VoidedDocumentsLine
        {
            get
            {
                return this.invoiceLineField;
            }
            set
            {
                this.invoiceLineField = value;
            }
        }
    }
}
