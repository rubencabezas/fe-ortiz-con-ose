﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmHomologacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TxtEmisorDirDistrito = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TxtEmisorDirProvincia = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TxtEmisorDirDepartamento = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TxtEmisorDirUrbanizacion = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TxtEmisorDirCalle = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TxtEmisorRazonSocial = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TxtEmisorUbigeo = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TxtEmisorRUC = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TxtClienteDireccion = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TxtClienteCorreo = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TxtClienteRazonSocial = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TxtClienteRUC = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.BtnIniciar = New System.Windows.Forms.Button()
        Me.BtnCancelar = New System.Windows.Forms.Button()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TxtEmisorDirDistrito)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.TxtEmisorDirProvincia)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.TxtEmisorDirDepartamento)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.TxtEmisorDirUrbanizacion)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.TxtEmisorDirCalle)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.TxtEmisorRazonSocial)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.TxtEmisorUbigeo)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.TxtEmisorRUC)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(360, 133)
        Me.GroupBox2.TabIndex = 10
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Emisor"
        '
        'TxtEmisorDirDistrito
        '
        Me.TxtEmisorDirDistrito.Location = New System.Drawing.Point(280, 108)
        Me.TxtEmisorDirDistrito.Name = "TxtEmisorDirDistrito"
        Me.TxtEmisorDirDistrito.Size = New System.Drawing.Size(68, 20)
        Me.TxtEmisorDirDistrito.TabIndex = 15
        Me.TxtEmisorDirDistrito.Text = "HUARAZ"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(242, 111)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(31, 13)
        Me.Label11.TabIndex = 14
        Me.Label11.Text = "Distr."
        '
        'TxtEmisorDirProvincia
        '
        Me.TxtEmisorDirProvincia.Location = New System.Drawing.Point(162, 108)
        Me.TxtEmisorDirProvincia.Name = "TxtEmisorDirProvincia"
        Me.TxtEmisorDirProvincia.Size = New System.Drawing.Size(68, 20)
        Me.TxtEmisorDirProvincia.TabIndex = 13
        Me.TxtEmisorDirProvincia.Text = "HUARAZ"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(130, 111)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(32, 13)
        Me.Label10.TabIndex = 12
        Me.Label10.Text = "Prov."
        '
        'TxtEmisorDirDepartamento
        '
        Me.TxtEmisorDirDepartamento.Location = New System.Drawing.Point(62, 108)
        Me.TxtEmisorDirDepartamento.Name = "TxtEmisorDirDepartamento"
        Me.TxtEmisorDirDepartamento.Size = New System.Drawing.Size(68, 20)
        Me.TxtEmisorDirDepartamento.TabIndex = 11
        Me.TxtEmisorDirDepartamento.Text = "ANCASH"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(14, 111)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(42, 13)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Depart."
        '
        'TxtEmisorDirUrbanizacion
        '
        Me.TxtEmisorDirUrbanizacion.Location = New System.Drawing.Point(257, 85)
        Me.TxtEmisorDirUrbanizacion.Name = "TxtEmisorDirUrbanizacion"
        Me.TxtEmisorDirUrbanizacion.Size = New System.Drawing.Size(91, 20)
        Me.TxtEmisorDirUrbanizacion.TabIndex = 9
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(231, 88)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(27, 13)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "Urb."
        '
        'TxtEmisorDirCalle
        '
        Me.TxtEmisorDirCalle.Location = New System.Drawing.Point(62, 85)
        Me.TxtEmisorDirCalle.Name = "TxtEmisorDirCalle"
        Me.TxtEmisorDirCalle.Size = New System.Drawing.Size(163, 20)
        Me.TxtEmisorDirCalle.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(17, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(30, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Calle"
        '
        'TxtEmisorRazonSocial
        '
        Me.TxtEmisorRazonSocial.Location = New System.Drawing.Point(62, 41)
        Me.TxtEmisorRazonSocial.Name = "TxtEmisorRazonSocial"
        Me.TxtEmisorRazonSocial.Size = New System.Drawing.Size(286, 20)
        Me.TxtEmisorRazonSocial.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(10, 44)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(50, 13)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "R. Social"
        '
        'TxtEmisorUbigeo
        '
        Me.TxtEmisorUbigeo.Location = New System.Drawing.Point(62, 63)
        Me.TxtEmisorUbigeo.Name = "TxtEmisorUbigeo"
        Me.TxtEmisorUbigeo.Size = New System.Drawing.Size(79, 20)
        Me.TxtEmisorUbigeo.TabIndex = 3
        Me.TxtEmisorUbigeo.Text = "020101"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(17, 66)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Ubigeo"
        '
        'TxtEmisorRUC
        '
        Me.TxtEmisorRUC.Location = New System.Drawing.Point(62, 19)
        Me.TxtEmisorRUC.Name = "TxtEmisorRUC"
        Me.TxtEmisorRUC.Size = New System.Drawing.Size(131, 20)
        Me.TxtEmisorRUC.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(10, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(30, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "RUC"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TxtClienteDireccion)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.TxtClienteCorreo)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.TxtClienteRazonSocial)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.TxtClienteRUC)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 150)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(360, 116)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Cliente"
        '
        'TxtClienteDireccion
        '
        Me.TxtClienteDireccion.Location = New System.Drawing.Point(62, 65)
        Me.TxtClienteDireccion.Name = "TxtClienteDireccion"
        Me.TxtClienteDireccion.Size = New System.Drawing.Size(286, 20)
        Me.TxtClienteDireccion.TabIndex = 9
        Me.TxtClienteDireccion.Text = "Calle Los Geranios 2367 – Urb. Matellini – Chorrillos - Lima"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(10, 68)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(52, 13)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "Dirección"
        '
        'TxtClienteCorreo
        '
        Me.TxtClienteCorreo.Location = New System.Drawing.Point(62, 88)
        Me.TxtClienteCorreo.Name = "TxtClienteCorreo"
        Me.TxtClienteCorreo.Size = New System.Drawing.Size(286, 20)
        Me.TxtClienteCorreo.TabIndex = 7
        Me.TxtClienteCorreo.Text = "donny.hn@gmail.com"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(10, 91)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(38, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Correo"
        '
        'TxtClienteRazonSocial
        '
        Me.TxtClienteRazonSocial.Location = New System.Drawing.Point(62, 42)
        Me.TxtClienteRazonSocial.Name = "TxtClienteRazonSocial"
        Me.TxtClienteRazonSocial.Size = New System.Drawing.Size(286, 20)
        Me.TxtClienteRazonSocial.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "R. Social"
        '
        'TxtClienteRUC
        '
        Me.TxtClienteRUC.Location = New System.Drawing.Point(62, 19)
        Me.TxtClienteRUC.Name = "TxtClienteRUC"
        Me.TxtClienteRUC.Size = New System.Drawing.Size(131, 20)
        Me.TxtClienteRUC.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(10, 22)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(30, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "RUC"
        '
        'BtnIniciar
        '
        Me.BtnIniciar.Location = New System.Drawing.Point(513, 17)
        Me.BtnIniciar.Name = "BtnIniciar"
        Me.BtnIniciar.Size = New System.Drawing.Size(108, 30)
        Me.BtnIniciar.TabIndex = 12
        Me.BtnIniciar.Text = "Iniciar"
        Me.BtnIniciar.UseVisualStyleBackColor = True
        '
        'BtnCancelar
        '
        Me.BtnCancelar.Location = New System.Drawing.Point(627, 17)
        Me.BtnCancelar.Name = "BtnCancelar"
        Me.BtnCancelar.Size = New System.Drawing.Size(108, 30)
        Me.BtnCancelar.TabIndex = 13
        Me.BtnCancelar.Text = "Cancelar"
        Me.BtnCancelar.UseVisualStyleBackColor = True
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.Cursor = System.Windows.Forms.Cursors.Default
        Me.GridControl1.Location = New System.Drawing.Point(387, 57)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(360, 209)
        Me.GridControl1.TabIndex = 14
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(387, 17)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(120, 30)
        Me.Button1.TabIndex = 15
        Me.Button1.Text = "Cargar Casos"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'FrmHomologacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(759, 274)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.BtnCancelar)
        Me.Controls.Add(Me.BtnIniciar)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "FrmHomologacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Modo Homologacion"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TxtEmisorRazonSocial As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TxtEmisorUbigeo As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TxtEmisorRUC As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TxtClienteCorreo As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TxtClienteRazonSocial As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TxtClienteRUC As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents BtnIniciar As System.Windows.Forms.Button
    Friend WithEvents BtnCancelar As System.Windows.Forms.Button
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TxtEmisorDirUrbanizacion As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TxtEmisorDirCalle As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TxtEmisorDirDistrito As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TxtEmisorDirProvincia As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TxtEmisorDirDepartamento As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TxtClienteDireccion As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
End Class
