﻿Public Class Ent_Establecimiento
    Property Emp_cCodigo As String
    Property Est_Codigo As String
    Property Est_Nombre As String
    Property Est_PDV As String

    Property Est_CodTipEstabl As String
    Property Est_OfiTipEstabl As String
    Property Est_DesTipEstabl As String

    Property Est_CodCondicion As String
    Property Est_OfiCondicion As String
    Property Est_DesCondicion As String

    Property Est_Direccion As String

    Property Est_CodZona As String
    Property Est_OfiZona As String
    Property Est_DesZona As String

    Property Est_NomZona As String
    Property Est_Telefono As String
    Property Est_Fax As String
    Property Est_Referenc As String
    Property Est_CodDpto As String
    Property Est_DesDpto As String
    Property Est_CodProv As String
    Property Est_DesProv As String
    Property Est_CodDist As String
    Property Est_DesDist As String
    Sub New()
    End Sub
    Sub New(ByVal c_Emp_cCodigo As String, ByVal c_Est_Codigo As String, ByVal c_Est_Nombre As String, ByVal c_Est_CodTipEstabl As String, ByVal c_Est_CodCondicion As String, ByVal c_Est_Direccion As String, ByVal c_Est_CodZona As String, ByVal c_Est_Telefono As String, ByVal c_Est_Fax As String, ByVal c_Est_Referenc As String, ByVal c_Est_CodDpto As String, ByVal c_Est_CodProv As String, ByVal c_Est_CodDist As String)
        Emp_cCodigo = c_Emp_cCodigo
        Est_Codigo = c_Est_Codigo
        Est_Nombre = c_Est_Nombre
        Est_CodTipEstabl = c_Est_CodTipEstabl
        Est_CodCondicion = c_Est_CodCondicion
        Est_Direccion = c_Est_Direccion
        Est_CodZona = c_Est_CodZona
        Est_Telefono = c_Est_Telefono
        Est_Fax = c_Est_Fax
        Est_Referenc = c_Est_Referenc
        Est_CodDpto = c_Est_CodDpto
        Est_CodProv = c_Est_CodProv
        Est_CodDist = c_Est_CodDist
    End Sub
End Class
