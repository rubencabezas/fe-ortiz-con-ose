﻿Public Class Ent_ENTIDAD_DIRECCION
    Property Emp_cCodigo As String
    Property Ent_cCodEntidad As String
    Property Ent_cDNIRUC As String
    Property Ten_cTipoEntidad As String
    Property Dir_Codigo As String
    Property Dir_Cod_TipoVia As String
    Property Dir_Ofi_TipoVia As String
    Property Dir_Des_TipoVia As String 'Update
    Property Dir_Nomb_TipVia As String
    Property Dir_Num_TipVia As String
    Property Dir_Inte_TipVia As String
    Property Dir_Cod_Zona As String
    Property Dir_Ofi_Zona As String
    Property Dir_Des_Zona As String 'Update
    Property Dir_Nomb_Zona As String
    Property Dir_Referencia As String
    Property Dir_Telef As String
    Property Dir_Fax As String 'Update
    Property Dir_Pais As String

    Property Dir_Depart As String
    Property Dir_Depart_Ds As String
    Property Dir_Provin As String
    Property Dir_Provin_Ds As String
    Property Dir_Distri As String
    Property Dir_Distri_Ds As String

    Property IsDefault As Boolean

    Property Direccion_Completa As String

    ReadOnly Property Dir_Descripcion As String
        Get
            If String.IsNullOrWhiteSpace(Direccion_Completa) Then
                Return (Dir_Des_TipoVia & " " & Dir_Nomb_TipVia) & IIf(Dir_Num_TipVia = "", "", " Nº " & Dir_Num_TipVia) & " " & Dir_Referencia
            Else
                Return Direccion_Completa
            End If

        End Get
    End Property

    Sub New()
    End Sub
    Sub New(ByVal c_Emp_cCodigo As String, ByVal c_Ent_cCodEntidad As String, ByVal c_Ten_cTipoEntidad As String, ByVal c_Dir_Codigo As String, ByVal c_Dir_Cod_TipoVia As String, ByVal c_Dir_Nomb_TipVia As String, ByVal c_Dir_Num_TipVia As String, ByVal c_Dir_Inte_TipVia As String, ByVal c_Dir_Cod_Zona As String, ByVal c_Dir_Nomb_Zona As String, ByVal c_Dir_Referencia As String, ByVal c_Dir_Telef As String, ByVal c_Dir_Pais As String, ByVal c_Dir_Depart As String, ByVal c_Dir_Provin As String, ByVal c_Dir_Distri As String)
        Emp_cCodigo = c_Emp_cCodigo
        Ent_cCodEntidad = c_Ent_cCodEntidad
        Ten_cTipoEntidad = c_Ten_cTipoEntidad
        Dir_Codigo = c_Dir_Codigo
        Dir_Cod_TipoVia = c_Dir_Cod_TipoVia
        Dir_Nomb_TipVia = c_Dir_Nomb_TipVia
        Dir_Num_TipVia = c_Dir_Num_TipVia
        Dir_Inte_TipVia = c_Dir_Inte_TipVia
        Dir_Cod_Zona = c_Dir_Cod_Zona
        Dir_Nomb_Zona = c_Dir_Nomb_Zona
        Dir_Referencia = c_Dir_Referencia
        Dir_Telef = c_Dir_Telef
        Dir_Pais = c_Dir_Pais
        Dir_Depart = c_Dir_Depart
        Dir_Provin = c_Dir_Provin
        Dir_Distri = c_Dir_Distri
    End Sub
End Class
