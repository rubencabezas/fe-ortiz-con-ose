﻿Public Class eProgramacion
    Property Emisor_RUC As String
    Property Progra_Correlativo As Integer
    Property Progra_Descripcion As String
    Property Progra_Habilitado As Boolean
    Property Progra_Appli_TipoDoc As String
    Property Progra_Appli_TipoDoc_Descripcion As String
    Property Progra_Appli_Filtra_Serie As Boolean
    Property Progra_Appli_Serie As String
    Property Progra_EsResumenBoletas As Boolean
    Property Progra_EsComunicacionBaja As Boolean

    Property Progra_FrecuEnvio_Hora_Es_Establecido As Boolean
    Property Progra_FrecuEnvio_Hora_Es_XCadaHora As Boolean
    Property Progra_FrecuEnvio_Hora_Es_DespuesEmitido As Boolean
    Property Progra_FrecuEnvio_Hora_Es_DespuesConfirmado As Boolean
    Property Progra_FrecuEnvio_Hora_Desde As TimeSpan
    Property Progra_FrecuEnvio_Hora_Hasta As TimeSpan
    ReadOnly Property Progra_FrecuEnvio_Hora_Desde_12 As String
        Get
            Return New DateTime(Progra_FrecuEnvio_Hora_Desde.Ticks).ToString("hh:mm:ss tt")
        End Get
    End Property
    ReadOnly Property Progra_FrecuEnvio_Hora_Hasta_12 As String
        Get
            Return New DateTime(Progra_FrecuEnvio_Hora_Hasta.Ticks).ToString("hh:mm:ss tt")
        End Get
    End Property
    Property Progra_FrecuEnvio_Hora_XCadaHora As Integer

    Property Progra_Fecha_Desde As Date
    Property Progra_Fecha_SinFin As Boolean
    Property Progra_Fecha_Hasta As Date
    ReadOnly Property Progra_Fecha_Hasta_Str As String
        Get
            Return IIf(Year(Progra_Fecha_Hasta) = 1, "", Progra_Fecha_Hasta.ToShortDateString())
        End Get
    End Property
    Property Progra_Estado As String
    Property Progra_FechaHoy As Date
End Class
