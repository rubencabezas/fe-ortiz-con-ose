﻿Imports Ortiz
Imports UblLarsen.Ubl2.Cac
Imports UblLarsen.Ubl2.Qdt
Imports UblLarsen.Ubl2.Udt

Public Class Form1
    Private CheckingMail As Boolean
    Private MailRootFolder As String = "D:\Nueva carpeta (13)"
    Private MailRootFolderAttachments As String = MailRootFolder & "/Attachments"
    Private MailRootFolderAttachmentsTemp As String = MailRootFolderAttachments & "/temp"
    Private Mail_Server_Name As String = "pop.gmail.com"
    Private Mail_Server_Port As Integer = 995
    Private Mail_User As String = "e-facturacion@grupoortiz.pe"
    Private Mail_PWD As String = "6666669991011"
    Private Sub TimerCheckMail_Tick(sender As Object, e As EventArgs) Handles TimerCheckMail.Tick
        If CheckingMail = False Then
            CheckingMail = True

            CheckingMailMethod()

        End If

        Try

            If pop3Client.IsConnected = True Then
                CheckingMail = False
            End If

        Catch ex As Exception
        End Try

    End Sub
    Sub CheckingMailMethod()

        '   Primero verificamos la existencia de nuestra carpeta raiz
        If Not System.IO.Directory.Exists(MailRootFolder) Then
            System.IO.Directory.CreateDirectory(MailRootFolder)
        End If

        '   Verificamos nuestra carpeta de adjuntos
        If Not System.IO.Directory.Exists(MailRootFolderAttachments) Then
            System.IO.Directory.CreateDirectory(MailRootFolderAttachments)
        End If

        '   Verificamos nuestra carpeta de adjuntos temporal
        If System.IO.Directory.Exists(MailRootFolderAttachmentsTemp) Then
            System.IO.Directory.Delete(MailRootFolderAttachmentsTemp, True)
        End If


        '   Decimos que si podemos eliminar los correos
        bounceFilter.AllowDelete = True
        bounceFilter.AllowInboxDelete = True

        Dim proxy As New ComponentPro.Net.WebProxyEx()
        pop3Client.Proxy = proxy

        ' Asynchronously connect to the POP3 server.
        pop3Client.Timeout = 30 * 1000

        Dim securityMode As ComponentPro.Net.SecurityMode = CType(ComponentPro.Net.SecurityMode.Implicit, ComponentPro.Net.SecurityMode)

#If Framework4_5 Then
			Try
				Await pop3Client.ConnectAsync(Mail_Server_Name, Mail_Server_Port, securityMode)
			Catch ex As Exception
				Util.ShowError(exc)
				EnableProgress(False)
				Return
			End Try

			Login()
#Else
        pop3Client.ConnectAsync(Mail_Server_Name, Mail_Server_Port, securityMode)


#End If

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
#If Not Framework4_5 Then
        AddHandler Me.pop3Client.AuthenticateCompleted, AddressOf pop3Client_AuthenticateCompleted
        AddHandler Me.pop3Client.ConnectCompleted, AddressOf pop3Client_ConnectCompleted

        AddHandler Me.bounceFilter.ProcessMessagesCompleted, AddressOf bounceFilter_ProcessMessagesCompleted
#End If
    End Sub

    Private Sub pop3Client_AuthenticateCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.AsyncCompletedEventArgs)
        If e.Error IsNot Nothing Then
            pop3Client.Disconnect()
            'Util.ShowError(e.Error)
            'EnableProgress(False)
            Return
        End If

        ProcessMessages()
    End Sub
#If Framework4_5 Then
		Private Async Sub ProcessMessages()
#Else
    Private Sub ProcessMessages()
#End If
        Dim dest As String = MailRootFolder
        If dest.Trim().Length = 0 Then ' When destination directory is empty, all message will be downloaded and processed in memory.
            dest = Nothing
        End If

#If Framework4_5 Then
			Dim result As BounceResultCollection = Nothing
			Try
				' Asynchronously process messages.
				result = Await bounceFilter.ProcessMessagesAsync(pop3Client, dest)
				lblStatus.Text = "Completed"
			Catch ex As Exception
				Util.ShowError(e.Error)
			End Try

			PostProcessMessages(result)
#Else
        ' Asynchronously process messages.
        bounceFilter.ProcessMessagesAsync(pop3Client, dest)
#End If
    End Sub
    Private Sub pop3Client_ConnectCompleted(ByVal sender As Object, ByVal e As ComponentPro.ExtendedAsyncCompletedEventArgs(Of String))
        If e.Error IsNot Nothing Then
            'Util.ShowError(e.Error)
            'EnableProgress(False)
            Return
        End If

        Login()
    End Sub
#If Framework4_5 Then
		Private Async Sub Login()
#Else
    Private Sub Login()
#End If
        'lblStatus.Text = "Logging in with username: " & pUsuario
        ' Login with the provided user name and password.
        Dim pa As ComponentPro.Net.Mail.Pop3AuthenticationMethod = CType(ComponentPro.Net.Mail.Pop3AuthenticationMethod.Auto, ComponentPro.Net.Mail.Pop3AuthenticationMethod)
#If Framework4_5 Then
			Try
				Await pop3Client.AuthenticateAsync(Mail_User, Mail_PWD, pa)
			Catch ex As Exception
				pop3Client.Disconnect()
				Util.ShowError(e.Error)
				EnableProgress(False)
				Return
			End Try

			ProcessMessages()
#Else
        pop3Client.AuthenticateAsync(Mail_User, Mail_PWD, pa)
#End If
    End Sub

    ''' <summary>
    ''' Handles the BounceInspector's ProcessMessagesCompleted event.
    ''' </summary>
    ''' <param name="sender">The BounceInspector object.</param>
    ''' <param name="e">The event arguments.</param>
    Private Sub bounceFilter_ProcessMessagesCompleted(ByVal sender As Object, ByVal e As ComponentPro.ExtendedAsyncCompletedEventArgs(Of ComponentPro.Net.Mail.BounceResultCollection))
        If e.Error IsNot Nothing Then
            'Util.ShowError(e.Error)
        Else
            'lblStatus.Text = "Completed"
        End If

        PostProcessMessages(e.Result)
    End Sub
    Private Sub PostProcessMessages(ByVal result As ComponentPro.Net.Mail.BounceResultCollection)

        ' Disconnect when finish.
        pop3Client.Disconnect()

        If result IsNot Nothing Then
            If result.HasErrors Then
                'Dim dlg As New ScanPop3Sample.Errors()
                'dlg.SetErrors(result)
                'dlg.ShowDialog()
            End If

            Dim found As String = String.Format("{0}/{1} bounced e-mails found.", result.BounceCount, result.Count)

            If result.CancelledIndex <> -1 Then
                MessageBox.Show("Operation has been cancelled by user. " & found, "BounceInspector", MessageBoxButtons.OK)
            Else
                MessageBox.Show(found, "BounceInspector", MessageBoxButtons.OK)
            End If
        End If
    End Sub

    Private Sub bounceFilter_Progress(sender As Object, e As ComponentPro.Net.Mail.ProgressEventArgs) Handles bounceFilter.Progress
        'Dim r As ComponentPro.Net.Mail.BounceResult = e.Result

        'If r.MailMessage Is Nothing Then
        '    Return
        'End If

        'progressBar.Value = CInt(Fix(e.Percentage))
        ''lblStatus.Text = "Processed mail with subject: '" & r.MailMessage.Subject & "'"

        'If r.Identified Then
        '    'lblStatus.Text = ""
        '    ' Add to the result list view.
        '    Dim item As ListViewItem
        '    If r.FilePath IsNot Nothing Then
        '        If r.Addresses.Length > 0 Then
        '            item = New ListViewItem(New String() {Path.GetFileName(r.FilePath), r.MailMessage.Subject, r.Addresses(0), r.BounceCategory.Name, r.BounceType.Name, (r.FileDeleted OrElse r.InboxDeleted).ToString(), r.Dsn.Action.ToString(), r.Dsn.DiagnosticCode})
        '        Else
        '            item = New ListViewItem(New String() {Path.GetFileName(r.FilePath), r.MailMessage.Subject, String.Empty, r.BounceCategory.Name, r.BounceType.Name, (r.FileDeleted OrElse r.InboxDeleted).ToString(), r.Dsn.Action.ToString(), r.Dsn.DiagnosticCode})
        '        End If
        '    Else
        '        If r.Addresses.Length > 0 Then
        '            item = New ListViewItem(New String() {String.Empty, r.MailMessage.Subject, r.Addresses(0), r.BounceCategory.Name, r.BounceType.Name, (r.FileDeleted OrElse r.InboxDeleted).ToString(), r.Dsn.Action.ToString(), r.Dsn.DiagnosticCode})
        '        Else
        '            item = New ListViewItem(New String() {String.Empty, r.MailMessage.Subject, String.Empty, r.BounceCategory.Name, r.BounceType.Name, (r.FileDeleted OrElse r.InboxDeleted).ToString(), r.Dsn.Action.ToString(), r.Dsn.DiagnosticCode})
        '        End If
        '    End If
        '    item.Tag = r.FilePath

        '    ltvResult.Items.Add(item)

        '    'r.Delete = True
        'End If
    End Sub

    Private Sub bounceFilter_Processed(sender As Object, e As ComponentPro.Net.Mail.ProcessedEventArgs) Handles bounceFilter.Processed
        ' Handle the bounce Processed event here.
        Dim r As ComponentPro.Net.Mail.BounceResult = e.Result

        If r.MailMessage Is Nothing Then
            Return
        End If


        If r.MailMessage.Attachments.Count > 0 Then

            '   Verificamos nuestra carpeta de adjuntos temporal
            If System.IO.Directory.Exists(MailRootFolderAttachmentsTemp) Then
                System.IO.Directory.Delete(MailRootFolderAttachmentsTemp, True)
            End If
            System.IO.Directory.CreateDirectory(MailRootFolderAttachmentsTemp)

            Try
                Dim _msg As ComponentPro.Net.Mail.MailMessage
                _msg = New ComponentPro.Net.Mail.MailMessage(r.FilePath)

                For Each att As ComponentPro.Net.Mail.Attachment In _msg.Attachments
                    Dim Ext As String = IO.Path.GetExtension(att.FileName)
                    If Ext.ToUpper = ".ZIP".ToUpper Or Ext.ToUpper = ".XML".ToUpper Then

                        If Ext.ToUpper = ".ZIP" Then
                            att.Save(IO.Path.Combine(MailRootFolderAttachments, att.FileName))
                            IO.Compression.ZipFile.ExtractToDirectory(IO.Path.Combine(MailRootFolderAttachments, att.FileName), MailRootFolderAttachmentsTemp)
                        Else
                            att.Save(IO.Path.Combine(MailRootFolderAttachmentsTemp, att.FileName))
                            'VerificarArchivoXML(IO.Path.Combine(MailRootFolderAttachmentsTemp, att.FileName))
                        End If

                        Dim drTemp As IO.DirectoryInfo = New IO.DirectoryInfo(MailRootFolderAttachmentsTemp)
                        For Each fi As IO.FileInfo In drTemp.GetFiles()
                            VerificarArchivoXML(IO.Path.Combine(MailRootFolderAttachmentsTemp, fi.Name))
                        Next

                        r.InboxDelete = True 'Elimina el correo

                    End If

                Next att

            Catch exc As Exception
                'Util.ShowError(exc)
            End Try
        End If

    End Sub
    Function VerificarArchivoXML(pArchivoPath As String) As Boolean
        Dim xmlDoc As New Xml.XmlDocument()
        xmlDoc.PreserveWhitespace = True
        xmlDoc.Load(pArchivoPath)
        Dim nodoExtension = xmlDoc.GetElementsByTagName("ExtensionContent", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2").Item(0) '1 - 2 nodos, 0 un solo nodo
        If nodoExtension Is Nothing Then
            Throw New InvalidOperationException("No se pudo encontrar el nodo ExtensionContent en el XML")
        End If
        nodoExtension.RemoveAll()

        Try
            Dim nodoExtensiono = xmlDoc.GetElementsByTagName("ExtensionContent", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2").Item(1) '1 - 2 nodos, 0 un solo nodo
            If nodoExtensiono Is Nothing Then
                Throw New InvalidOperationException("No se pudo encontrar el nodo ExtensionContent en el XML")
            End If
            nodoExtensiono.RemoveAll()
        Catch ex As Exception
        End Try

        xmlDoc.Save(pArchivoPath)

        Dim nodo_raiz As String = xmlDoc.DocumentElement.LocalName
        If nodo_raiz.ToUpper = ("Invoice").ToUpper Then
            ConvertirInvoice(xmlDoc)
        ElseIf nodo_raiz.ToUpper = ("Invoice").ToUpper Then
            ConvertirInvoice(xmlDoc)
        ElseIf nodo_raiz.ToUpper = ("CreditNote").ToUpper Then
            'nProceso = Generar_NotaCreditoElectronica(Documento)
        ElseIf nodo_raiz.ToUpper = ("DebitNote").ToUpper Then
            'nProceso = Generar_NotaDebitoElectronica(Documento)
        ElseIf nodo_raiz.ToUpper = ("DespatchAdvice").ToUpper Then
            'nProceso = Generar_GuiaRemisionRemitente(Documento)
            'XMLFirmar0(Documento, CertificadoDigitalCredenciales, nProceso)
        ElseIf nodo_raiz.ToUpper = ("Retention").ToUpper Then
            'nProceso = Generar_ComprobantePercepcion(Documento)
        ElseIf nodo_raiz.ToUpper = ("Perception").ToUpper Then
            'nProceso = Generar_ComprobanteRetencion(Documento)
        End If

    End Function
    Sub ConvertirInvoice(DocXML As Xml.XmlDocument)
        Dim invoice As New UblLarsen.Ubl2.InvoiceType
        Dim Provision As New DocumentoElectronico

        Dim reader As New System.Xml.Serialization.XmlSerializer(invoice.[GetType]())
        Dim reader2 As Xml.XmlNodeReader = New Xml.XmlNodeReader(DocXML)
        invoice = DirectCast(reader.Deserialize(reader2), UblLarsen.Ubl2.InvoiceType)

        Dim ListaDetallesInvoice As List(Of InvoiceLineType) = invoice.InvoiceLine.ToList
        For Each ItemDoc In ListaDetallesInvoice
            Dim ProvisionItem As New DocumentoElectronicoDetalle
            ProvisionItem.Item = ItemDoc.ID.Value
            ProvisionItem.Producto_UnidadMedida_Codigo = ItemDoc.InvoicedQuantity.unitCode
            ProvisionItem.Producto_Cantidad = ItemDoc.InvoicedQuantity.Value
            ProvisionItem.Item_SubTotal = ItemDoc.LineExtensionAmount.Value
            ProvisionItem.Unitario_Valor_Unitario = ItemDoc.PricingReference.AlternativeConditionPrice(0).PriceAmount.Value
            ProvisionItem.Unitario_IGV = ItemDoc.TaxTotal(0).TaxAmount.Value
            ProvisionItem.Item_Tipo_Afectacion_IGV_Codigo = ItemDoc.TaxTotal(0).TaxSubtotal(0).TaxCategory.TaxExemptionReasonCode.Value
            ProvisionItem.Producto_Descripcion = ItemDoc.Item.Description(0).Value
            'ProvisionItem.Unitario_Precio_Venta = ItemDoc.Price.PriceAmount(0).Value
        Next

        'For Each ProvisionItem In Provision.Detalles
        '    Dim ItemDoc As New InvoiceLineType
        '    ItemDoc.ID = ProvisionItem.Item
        '    ItemDoc.InvoicedQuantity = New QuantityType() With {
        '                 .unitCode = ProvisionItem.Producto_UnidadMedida_Codigo,
        '                 .Value = Format(ProvisionItem.Producto_Cantidad, "#0.000")
        '            }
        '    ItemDoc.LineExtensionAmount = Format(ProvisionItem.Item_SubTotal, "#0.00")
        '    'ItemDoc.PricingReference = New PricingReferenceType() With {
        '    '             .AlternativeConditionPrice = New PriceType() {New PriceType() With {.PriceAmount = Format(ProvisionItem.Unitario_Valor_Unitario, "#0.00"), .PriceTypeCode = ProvisionItem.Unitario_Precio_Tipo_Codigo}, New PriceType() With {.PriceAmount = Format(ProvisionItem.Unitario_Precio_Venta, "#0.00"), .PriceTypeCode = "02"}}}

        '    ItemDoc.PricingReference = New PricingReferenceType() With {
        '                .AlternativeConditionPrice = New PriceType() {New PriceType() With {.PriceAmount = Format(ProvisionItem.Unitario_Valor_Unitario, "#0.00"), .PriceTypeCode = ProvisionItem.Unitario_Precio_Tipo_Codigo}}}

        '    ItemDoc.TaxTotal = New TaxTotalType() {New TaxTotalType() With {
        '                 .TaxAmount = Format(ProvisionItem.Unitario_IGV, "#0.00"),
        '                 .TaxSubtotal = New TaxSubtotalType() {New TaxSubtotalType() With {
        '                         .TaxableAmount = Format(ProvisionItem.Unitario_IGV, "#0.00"),
        '                     .TaxAmount = Format(ProvisionItem.Unitario_IGV, "#0.00"),
        '                         .Percent = ProvisionItem.Unitario_IGV_Porcentaje,
        '                     .TaxCategory = New TaxCategoryType() With {
        '                         .TaxExemptionReasonCode = ProvisionItem.Item_Tipo_Afectacion_IGV_Codigo,
        '                         .TaxScheme = New TaxSchemeType() With {
        '                             .ID = "1000",
        '                             .Name = "IGV",
        '                             .TaxTypeCode = "VAT"
        '                        }
        '                    }
        '                }}
        '            }}
        '    ItemDoc.Item = New ItemType() With {
        '                 .Description = New TextType() {ProvisionItem.Producto_Descripcion}
        '            }
        '    ItemDoc.Price = New PriceType() With {
        '                 .PriceAmount = ProvisionItem.Unitario_Precio_Venta
        '            }
        '    ListaDetalles.Add(ItemDoc)
        'Next

        'Creando cabecera
        invoice = New UblLarsen.Ubl2.InvoiceType() With {
            .ID = Provision.TipoDocumento_Serie & "-" & Provision.TipoDocumento_Numero,
             .IssueDate = Provision.Fecha_Emision,
             .InvoiceTypeCode = Provision.TipoDocumento_Codigo,
            .DocumentCurrencyCode = New CurrencyCodeType() With {.Value = Provision.Moneda_Codigo},
            .ExpiryDate = Provision.Fecha_Vencimiento,
            .Signature = New SignatureType() {New SignatureType() With {.ID = Provision.Emisor_Documento_Numero,
                        .SignatoryParty = New PartyType() With {.PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType() With {.ID = Provision.Emisor_Documento_Numero}},
                                                                .PartyName = New PartyNameType() {New PartyNameType() With {.Name = Provision.Emisor_ApellidosNombres_RazonSocial}}},
                        .DigitalSignatureAttachment = New AttachmentType() With {.ExternalReference = New ExternalReferenceType() With {.URI = "#signatureGO"}}}},
             .AccountingSupplierParty = New SupplierPartyType() With {
                 .CustomerAssignedAccountID = Provision.Emisor_Documento_Numero,
                 .AdditionalAccountID = New IdentifierType() {New IdentifierType() With {.Value = "6"}},
                 .Party = New PartyType() With {
                     .PartyName = New PartyNameType() {New PartyNameType() With {.Name = Provision.Emisor_ApellidosNombres_RazonSocial}},
                     .PostalAddress = New AddressType() With {
                         .ID = Provision.Emisor_Direccion_Ubigeo,
                         .StreetName = Provision.Emisor_Direccion_Calle,
                         .CitySubdivisionName = Provision.Emisor_Direccion_Urbanizacion,
                         .CityName = Provision.Emisor_Direccion_Departamento,
                         .CountrySubentity = Provision.Emisor_Direccion_Provincia,
                         .District = Provision.Emisor_Direccion_Distrito,
                         .Country = New CountryType() With {
                             .IdentificationCode = Provision.Emisor_Direccion_PaisCodigo
                        }
                    },
                     .PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType() With {.RegistrationName = Provision.Emisor_ApellidosNombres_RazonSocial}}
                }
            },
             .AccountingCustomerParty = New CustomerPartyType() With {
                 .CustomerAssignedAccountID = Provision.Cliente_Documento_Numero,
                 .AdditionalAccountID = New IdentifierType() {New IdentifierType() With {.Value = Provision.Cliente_Documento_Tipo}},
                 .Party = New PartyType() With {
                     .PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType() With {.RegistrationName = Provision.Cliente_RazonSocial_Nombre}}
                }
            },
             .TaxTotal = New TaxTotalType() {New TaxTotalType() With {
                 .TaxAmount = Format(Provision.Total_IGV, "##0.00"),
                 .TaxSubtotal = New TaxSubtotalType() {New TaxSubtotalType() With {
                     .TaxAmount = Format(Provision.Total_IGV, "##0.00"),
                     .TaxCategory = New TaxCategoryType() With {
                         .TaxScheme = New TaxSchemeType() With {
                             .ID = "1000",
                             .Name = "IGV",
                             .TaxTypeCode = "VAT"
                        }
                    }
                }}
            }},
             .LegalMonetaryTotal = New MonetaryTotalType() With {
                 .PayableAmount = Format(Provision.Total_Importe_Venta, "##0.00")
            }
        }

        If Provision.Fecha_Vencimiento.Year = 1 Then
            invoice.ExpiryDate = Nothing
        Else
            invoice.ExpiryDate = Provision.Fecha_Vencimiento
        End If

    End Sub
End Class
