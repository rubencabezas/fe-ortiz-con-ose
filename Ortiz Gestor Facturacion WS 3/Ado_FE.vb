﻿Imports System.Data.SqlClient

Public Class Ado_FE
    Shared pInstancia As Ado_FE
    Public Shared Function Instancia() As Ado_FE
        If IsNothing(pInstancia) Then
            pInstancia = New Ado_FE
        End If
        Return pInstancia
    End Function

    Public Function Documento_InsertarActualizar(ByVal pEntidad As DocumentoElectronico) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTO_ELECTRONICO_INSERT_UPDATE", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_Documento_Numero
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = pEntidad.TipoDocumento_Codigo
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = pEntidad.TipoDocumento_Serie
                    Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = pEntidad.TipoDocumento_Numero
                    Cmd.Parameters.Add("@PDoc_Moneda", SqlDbType.Char, 3).Value = pEntidad.Moneda_Codigo
                    Cmd.Parameters.Add("@PDoc_FechaEmision", SqlDbType.Date).Value = pEntidad.Fecha_Emision

                    Cmd.Parameters.Add("@PDoc_Oper_Gravadas", SqlDbType.Decimal, 10, 2).Value = pEntidad.Total_ValorVenta_OperacionesGravadas
                    Cmd.Parameters.Add("@PDoc_Oper_Exoneradas", SqlDbType.Decimal, 10, 2).Value = pEntidad.Total_ValorVenta_OperacionesExoneradas
                    Cmd.Parameters.Add("@PDoc_Oper_Inafectas", SqlDbType.Decimal, 10, 2).Value = pEntidad.Total_ValorVenta_OperacionesInafectas
                    Cmd.Parameters.Add("@PDoc_Oper_Gratuitas", SqlDbType.Decimal, 10, 2).Value = pEntidad.Total_ValorVenta_OperacionesGratuitas
                    Cmd.Parameters.Add("@PDoc_ISC", SqlDbType.Decimal, 10, 2).Value = pEntidad.Total_ISC
                    Cmd.Parameters.Add("@PDoc_Otros_Tributos", SqlDbType.Decimal, 10, 2).Value = pEntidad.Total_OtrosTributos
                    Cmd.Parameters.Add("@PDoc_Otros_Conceptos", SqlDbType.Decimal, 10, 2).Value = pEntidad.Total_OtrosCargos

                    Cmd.Parameters.Add("@PDoc_IGV", SqlDbType.Decimal, 10, 2).Value = pEntidad.Total_IGV
                    Cmd.Parameters.Add("@PDoc_Importe", SqlDbType.Decimal, 10, 2).Value = pEntidad.Total_Importe_Venta
                    Cmd.Parameters.Add("@PDoc_TipoDocIdent_Codigo", SqlDbType.Char, 1).Value = pEntidad.Cliente_Documento_Tipo
                    Cmd.Parameters.Add("@PDoc_TipoDocIdent_Numero", SqlDbType.VarChar, 11).Value = pEntidad.Cliente_Documento_Numero
                    Cmd.Parameters.Add("@PDoc_SocNeg_RazonSocialNombres", SqlDbType.VarChar, 150).Value = pEntidad.Cliente_RazonSocial_Nombre

                    Cmd.Parameters.Add("@PDoc_Cliente_Correo", SqlDbType.VarChar, 50).Value = IIf(String.IsNullOrEmpty(pEntidad.Cliente_Correo), Nothing, pEntidad.Cliente_Correo)

                    Cmd.Parameters.Add("@PDoc_Modif_Doc_Tipo", SqlDbType.Char, 2).Value = pEntidad.Modif_Doc_Tipo
                    Cmd.Parameters.Add("@PDoc_Modif_Doc_Serie", SqlDbType.Char, 4).Value = pEntidad.Modif_Doc_Serie
                    Cmd.Parameters.Add("@PDoc_Modif_Doc_Numero", SqlDbType.VarChar, 8).Value = pEntidad.Modif_Doc_Numero

                    Cmd.Parameters.Add("@PDoc_NombreArchivoXML", SqlDbType.VarChar, 50).Value = pEntidad.Doc_SUNAT_NombreArchivoXML

                    Cmd.Parameters.Add("@PDoc_XML_DigestValue", SqlDbType.VarChar, 50).Value = pEntidad.Print_DigestValue
                    Cmd.Parameters.Add("@PDoc_XML_SignatureValue", SqlDbType.VarChar, 500).Value = pEntidad.Print_SignatureValue

                    Cmd.Parameters.Add("@Retorno", SqlDbType.VarChar, 5).Direction = ParameterDirection.Output
                    Cmd.Parameters.Add("@Correlativo", SqlDbType.Int).Direction = ParameterDirection.Output
                    Cmd.Parameters.Add("@PDoc_Usuario", SqlDbType.VarChar, 50).Value = "UserFACTE"
                    Cmd.Parameters.Add("@PDoc_XML_Nombre", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output
                    Cmd.Parameters.Add("@PDoc_NombreArchivoWeb", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                    pEntidad.SQL_Retorno = Cmd.Parameters("@Retorno").Value
                    If Not IsDBNull(Cmd.Parameters("@PDoc_XML_Nombre").Value) Then
                        pEntidad.SQL_XML_Nombre = Cmd.Parameters("@PDoc_XML_Nombre").Value
                    End If
                    If Not IsDBNull(Cmd.Parameters("@Correlativo").Value) Then
                        pEntidad.SQL_XML_Correlativo = Cmd.Parameters("@Correlativo").Value
                    End If
                    If Not IsDBNull(Cmd.Parameters("@PDoc_NombreArchivoWeb").Value) Then
                        pEntidad.Doc_NombreArchivoWeb = Cmd.Parameters("@PDoc_NombreArchivoWeb").Value
                    End If
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Documento_DarBaja(ByVal pEntidad As DocumentoElectronicoComunicacionBaja) As Boolean
        Dim NombreArchivoServer As String
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTO_ELECTRONICO_DISABLE", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cn.Open()
                    For Each Documento In pEntidad.Documentos
                        Cmd.Parameters.Clear()
                        Cmd.CommandText = "UP_FACTE_DOCUMENTO_ELECTRONICO_DISABLE"
                        Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = Documento.Emisor_Documento_Numero
                        Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = Documento.TipoDocumento_Codigo
                        Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = Documento.TipoDocumento_Serie
                        Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = Documento.TipoDocumento_Numero

                        Cmd.Parameters.Add("@PDoc_RazonAnulacion", SqlDbType.VarChar, 150).Value = Documento.RazonAnulacion

                        Cmd.Parameters.Add("@Retorno", SqlDbType.VarChar, 5).Direction = ParameterDirection.Output
                        Cmd.Parameters.Add("@PDoc_Usuario", SqlDbType.VarChar, 50).Value = "UserFACTE"
                        Cmd.Parameters.Add("@PDoc_XML_NombreIN", SqlDbType.VarChar, 150).Value = NombreArchivoServer
                        Cmd.Parameters.Add("@PDoc_XML_Nombre", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output

                        Cmd.ExecuteNonQuery()
                        Documento.SQL_Retorno = Cmd.Parameters("@Retorno").Value
                        If Not IsDBNull(Cmd.Parameters("@PDoc_XML_Nombre").Value) Then
                            NombreArchivoServer = Cmd.Parameters("@PDoc_XML_Nombre").Value
                            Documento.SQL_XML_Nombre = NombreArchivoServer
                        End If
                    Next
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Documento_Estado(ByVal pEntidad As DocumentoElectronico) As List(Of DocumentoElectronico)
        Dim Rslt As New List(Of DocumentoElectronico)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTO_ELECTRONICO_ESTADO", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_Documento_Numero
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = pEntidad.TipoDocumento_Codigo
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = pEntidad.TipoDocumento_Serie
                    Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = pEntidad.TipoDocumento_Numero
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read()
                            Rslt.Add(New DocumentoElectronico With {.Emisor_Documento_Numero = Dr.GetString(0), .TipoDocumento_Codigo = Dr.GetString(1), .TipoDocumento_Serie = Dr.GetString(2), .TipoDocumento_Numero = Dr.GetString(3), .Total_IGV = Dr.GetDecimal(4), .Total_Importe_Venta = Dr.GetDecimal(5), .Fecha_Emision = Dr.GetDateTime(6), .Cliente_Documento_Tipo = Dr.GetString(7), .Cliente_Documento_Numero = Dr.GetString(8), .Doc_XML_Tipo = Dr.GetString(9), .Doc_XML_Estado = Dr.GetString(10), .Print_DigestValue = Dr.GetString(11), .Print_SignatureValue = Dr.GetString(12), .Doc_NombreArchivoWeb = Dr.GetString(13), .Doc_XML_SUNAT_Resp_Code = Dr.GetString(14), .Doc_XML_SUNAT_Resp_Description = Dr.GetString(15), .Doc_XML_Dias_CDR_Aceptado = Dr.GetInt32(16)})
                        End While
                    End Using
                End Using
            End Using
            Return Rslt
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Documento_Modificable(ByVal pEntidad As DocumentoElectronico) As String()
        Dim Rslt As String()
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTO_ELECTRONICO_MODIFICABLE", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_Documento_Numero
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = pEntidad.TipoDocumento_Codigo
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = pEntidad.TipoDocumento_Serie
                    Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = pEntidad.TipoDocumento_Numero
                    Cmd.Parameters.Add("@PEstado", SqlDbType.Bit).Direction = ParameterDirection.Output
                    Cmd.Parameters.Add("@PMensaje", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                    Dim Var1, Var2 As String
                    Var1 = Cmd.Parameters("@PEstado").Value
                    Var2 = Cmd.Parameters("@PMensaje").Value
                    Rslt = New String() {Var1, Var2}
                End Using
            End Using
            Return Rslt
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Documento_Insertar_DocumentoAdjunto(ByVal pEmisor_RUC As String, pDoc_Tipo As String, pDoc_Serie As String, pDoc_Numero As String, pXML_Correlativo As String, pXML_Archivo_Alias As String, pXML_Archivo_Nombre As String) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO_INSERT", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pEmisor_RUC
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = pDoc_Tipo
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = pDoc_Serie
                    Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = pDoc_Numero
                    Cmd.Parameters.Add("@PXML_Correlativo", SqlDbType.Int).Value = pXML_Correlativo
                    Cmd.Parameters.Add("@PXML_Archivo_Item", SqlDbType.Int).Direction = ParameterDirection.Output
                    Cmd.Parameters.Add("@PXML_Archivo_Alias", SqlDbType.VarChar, 30).Value = pXML_Archivo_Alias
                    Cmd.Parameters.Add("@PXML_Archivo_Nombre", SqlDbType.VarChar, 150).Value = pXML_Archivo_Nombre
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                    'pEntidad.SQL_Retorno = Cmd.Parameters("@Retorno").Value
                    'If Not IsDBNull(Cmd.Parameters("@PDoc_XML_Nombre").Value) Then
                    '    pEntidad.SQL_XML_Nombre = Cmd.Parameters("@PDoc_XML_Nombre").Value
                    'End If
                    'If Not IsDBNull(Cmd.Parameters("@Correlativo").Value) Then
                    '    pEntidad.SQL_XML_Correlativo = Cmd.Parameters("@Correlativo").Value
                    'End If
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
