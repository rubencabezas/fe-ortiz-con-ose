﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Xml
Imports UblLarsen.Ubl2.Udt
Imports UblLarsen.Ubl2.Cac
Imports UblLarsen.Ubl2.Qdt
Imports UblLarsen.Ubl2.Ext
Imports System.Xml.Serialization
Imports System.Security.Cryptography.X509Certificates
Imports System.Security.Cryptography.Xml
Imports System.IO.Compression
Imports System.Security.Cryptography
Imports System.IO

'Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
'<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class ws_documentoelectronico
    Inherits System.Web.Services.WebService
    Public path_Virtual As String = "/facte_root"
    <WebMethod()> _
    Public Function Documento_RegistrarActualizar(xmlDoc As XmlDocument) As String
        'Devuelve: ( Value 0 | Value 1 | Value 2 | Value 3 )
        'Devuelve(0): - error, 0 documento no enviado, 1 documento registrado/actualizado
        'Devuelve(1): Key - Nombre unico web
        'Devuelve(2): DigestValue (Codigo Hash)
        'Devuelve(3): Código de Barras
        'Devuelve(4): - |Codigo sunat
        'Devuelve(5): - |Descripcion

        Dim Provision As New DocumentoElectronico
        Try
            'P1: Obtener la ruta de almacenamiento
            Dim RootPathFull As String

            RootPathFull = New IO.DirectoryInfo(Server.MapPath(path_Virtual)).FullName 'Producción
            'RootPathFull = "D:\WebAppsFiles" 'Debug --Es el path que se debe indicar en la conf. de empresa

            'En el despliegue se debe de crear: Root/RUC/ --xml_pos - xml_pos_attached - xml_temp - xml_generated - xml_aceptados - xml_rechazados

            'Convertimos el XML en Objeto
            Dim reader As New System.Xml.Serialization.XmlSerializer(Provision.[GetType]())
            Dim reader2 As XmlNodeReader = New XmlNodeReader(xmlDoc)
            Provision = DirectCast(reader.Deserialize(reader2), DocumentoElectronico)

            Provision.RootPath_Files = RootPathFull
            'Generamos el archivo XML SUNAT, para capturar el digest value y generar el codigo de barras
            CrearDocumento_SUNAT(Provision, False)

            'Guardo el documento como pendiente
            'Ado_FE.Instancia.Documento_InsertarActualizar(Provision)

            'If Not IsNothing(Provision.SQL_XML_Nombre) Then
            '    'Guardo el xml en el directorio virtual
            '    Dim settings As New XmlWriterSettings With {.ConformanceLevel = ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}
            '    Using writer As XmlWriter = XmlWriter.Create(RootPathFull & "\" & Provision.Emisor_Documento_Numero & "\xml_pos\" & Provision.SQL_XML_Nombre, settings)
            '        Dim typeToSerialize As Type = GetType(DocumentoElectronico)
            '        Dim xs As New Serialization.XmlSerializer(typeToSerialize)
            '        xs.Serialize(writer, Provision)
            '    End Using
            'End If
        Catch ex As Exception
            Provision.Doc_XML_SUNAT_Resp_Description = ex.Message
        End Try

        If String.IsNullOrEmpty(Provision.Doc_XML_SUNAT_Resp_Description) Then
            Provision.Doc_XML_SUNAT_Resp_Description = "-"
        End If
        'El último es para los enviados (aceptado o rechazado)
        If Provision.SQL_Retorno = 1 Then
            Return Str(Provision.SQL_XML_Correlativo) & " # " & Provision.Doc_NombreArchivoWeb & " # " & Provision.Print_DigestValue & " # " & Provision.Print_BarCode_Armar & " # - # Guardado Correctamente"
        ElseIf Provision.SQL_Retorno = 0 Then
            Return "0 # " & Provision.Doc_NombreArchivoWeb & " # - # - # - # " & Provision.Doc_XML_SUNAT_Resp_Description
        Else
            Return "- # - # - # - # - # " & Provision.Doc_XML_SUNAT_Resp_Description
        End If
    End Function
    '<WebMethod()> _
    Public Function Documento_RegistrarActualizar_Enviar(xmlDoc As XmlDocument) As String

        Dim Provision As New DocumentoElectronico
        Try
            'P1: Obtener la ruta de almacenamiento
            Dim RootPathFull As String

            RootPathFull = New IO.DirectoryInfo(Server.MapPath(path_Virtual)).FullName 'Producción
            'RootPathFull = "D:\WebAppsFiles" 'Debug --Es el path que se debe indicar en la conf. de empresa

            'En el despliegue se debe de crear: Root/RUC/ --xml_pos - xml_pos_attached - xml_temp - xml_generated - xml_aceptados - xml_rechazados

            'Convertimos el XML en Objeto
            Dim reader As New System.Xml.Serialization.XmlSerializer(Provision.[GetType]())
            Dim reader2 As XmlNodeReader = New XmlNodeReader(xmlDoc)
            Provision = DirectCast(reader.Deserialize(reader2), DocumentoElectronico)

            Provision.RootPath_Files = RootPathFull
            'Generamos el archivo XML SUNAT, para capturar el digest value y generar el codigo de barras
            CrearDocumento_SUNAT(Provision, False)

        Catch ex As Exception

        End Try
        'El último es para los enviados (aceptado o rechazado)
        If Provision.SQL_Retorno = 1 Then
            Return Str(Provision.SQL_XML_Correlativo) & " # " & Provision.Doc_NombreArchivoWeb & " # " & Provision.Print_DigestValue & " # " & Provision.Print_BarCode_Armar & " # " & Provision.Doc_XML_SUNAT_Resp_Code & " # " & Provision.Doc_XML_SUNAT_Resp_Description
        ElseIf Provision.SQL_Retorno = 0 Then
            Return "0 # " & Provision.Doc_NombreArchivoWeb & " # - # - # " & Provision.Doc_XML_SUNAT_Resp_Code & " # " & Provision.Doc_XML_SUNAT_Resp_Description
        Else
            Return "- # - # - # - # - #-"
        End If
    End Function
    <WebMethod()> _
    Public Function Documento_DarBaja(xmlDoc As XmlDocument) As String
        Dim Provision As New DocumentoElectronicoComunicacionBaja
        Try
            '-----------------------Modo final

            'P1: Obtener la ruta de almacenamiento
            Dim RootPathFull As String
            RootPathFull = New IO.DirectoryInfo(Server.MapPath(path_Virtual)).FullName 'Producción
            'RootPathFull = "D:\WebAppsFiles" 'Debug --Es el path que se debe indicar en la conf. de empresa

            'En el despliegue se debe de crear: Root/RUC/ --xml_pos - xml_pos_attached - xml_temp - xml_generated - xml_aceptados - xml_rechazados

            Dim reader As New System.Xml.Serialization.XmlSerializer(Provision.[GetType]())
            Dim reader2 As XmlNodeReader = New XmlNodeReader(xmlDoc)
            Provision = DirectCast(reader.Deserialize(reader2), DocumentoElectronicoComunicacionBaja)

            'Guardo el documento como pendiente
            Ado_FE.Instancia.Documento_DarBaja(Provision)


            If Not IsNothing(Provision.Documentos(0).SQL_XML_Nombre) Then
                'Guardo el xml en el directorio virtual
                Dim settings As New XmlWriterSettings With {.ConformanceLevel = ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}
                Using writer As XmlWriter = XmlWriter.Create(RootPathFull & "\" & Provision.Documentos(0).Emisor_Documento_Numero & "\xml_pos\" & Provision.Documentos(0).SQL_XML_Nombre, settings)
                    Dim typeToSerialize As Type = GetType(DocumentoElectronicoComunicacionBaja)
                    Dim xs As New Serialization.XmlSerializer(typeToSerialize)
                    xs.Serialize(writer, Provision)
                End Using
            End If

        Catch ex As Exception
            'Provision.Documentos(0).SQL_Retorno = "-1"
            Return ex.Message
        End Try

        Return Provision.Documentos(0).SQL_Retorno
    End Function
    'Para subir archivos adjuntos
    '<WebMethod()> _
    Public Function Documento_Adjunto_Subir(FileBinaryArray As [Byte](), FileAliasAttached As String, FileNameAttached As String, ByVal pEmisor_RUC As String, pDoc_Tipo As String, pDoc_Serie As String, pDoc_Numero As String, pXML_Correlativo As String) As Boolean
        'P1: Obtener la ruta de almacenamiento
        Dim RootPathFull As String

        RootPathFull = New IO.DirectoryInfo(Server.MapPath(path_Virtual)).FullName 'Producción
        'RootPathFull = "D:\WebAppsFiles" 'Debug --Es el path que se debe indicar en la conf. de empresa
        Dim strdocPath As String

        strdocPath = RootPathFull & "\" & pEmisor_RUC & "\xml_pos_attached\" & FileAliasAttached
        Dim objfilestream As New IO.FileStream(strdocPath, IO.FileMode.Create, IO.FileAccess.ReadWrite)
        objfilestream.Write(FileBinaryArray, 0, FileBinaryArray.Length)
        objfilestream.Close()

        'Guardo los documentos en la BD
        Ado_FE.Instancia.Documento_Insertar_DocumentoAdjunto(pEmisor_RUC, pDoc_Tipo, pDoc_Serie, pDoc_Numero, pXML_Correlativo, FileAliasAttached, FileNameAttached)
        Return True
    End Function
    '
    <WebMethod()>
    Public Function Documento_Estado(pRuc As String, pDocumentoTipo As String, pDocumentoSerie As String, pDocumentoNumero As String) As String
        'Ultimo_Estado: emitido, actualizado, dado baja
        'estado SUNAT
        'digest_value
        'codigo barras
        'key - codigo unico web
        '---sunat--codigo respuesta
        '---sunat responsecode--descripcion
        Try
            Dim Rslt As New List(Of DocumentoElectronico)
            Rslt = Ado_FE.Instancia.Documento_Estado(New DocumentoElectronico With {.Emisor_Documento_Numero = pRuc, .TipoDocumento_Codigo = pDocumentoTipo, .TipoDocumento_Serie = pDocumentoSerie, .TipoDocumento_Numero = pDocumentoNumero})
            Dim Res_SUNAT As String = "-#-"
            If Rslt.Count > 0 Then
                If (Rslt(0).TipoDocumento_Serie.Trim).Substring(0, 1) <> "B" Then
                    Res_SUNAT = HerramientasFE.Consultar_Estado_Sunat(pRuc, pDocumentoTipo, pDocumentoSerie, pDocumentoNumero)
                Else
                    Res_SUNAT = Rslt(0).Doc_XML_SUNAT_Resp_Description & " # " & Rslt(0).Doc_XML_Estado
                End If
                Return Rslt(0).Doc_XML_Tipo & " # " & Rslt(0).Doc_XML_Estado & " # " & Rslt(0).Print_DigestValue & " # " & Rslt(0).Print_BarCode_Armar & " # " & Rslt(0).Doc_NombreArchivoWeb & " # " & Rslt(0).Doc_XML_SUNAT_Resp_Code.Trim & " # " & Rslt(0).Doc_XML_SUNAT_Resp_Description.Trim & " # " & Res_SUNAT & " # " & Rslt(0).Doc_XML_Dias_CDR_Aceptado.ToString
            Else
                Return "Documento no encontrado"
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
    <WebMethod()> _
    Public Function Documento_EnviarCorreo(pRuc As String, pDocumentoTipo As String, pDocumentoSerie As String, pDocumentoNumero As String, pDocumentoFecha As Date, pClienteCorreo As String) As Boolean
        Try
            Dim Log As New Log_FACTE_DOCUMENTOS
            Dim NBusq As New eProvisionFacturacion With {.Busq_Fecha_Inicio = Now, .Busq_Fecha_Fin = Now}
            Dim Documentos As List(Of eProvisionFacturacion)
            NBusq.Busq_Fecha_Inicio = pDocumentoFecha.Date
            NBusq.Busq_Fecha_Fin = Now.Date
            If Not String.IsNullOrEmpty(pRuc) Then
                NBusq.Emisor_RUC = pRuc
            End If
            If Not String.IsNullOrEmpty(pDocumentoTipo) Then
                NBusq.TpDc_Codigo_St = pDocumentoTipo
            End If
            If Not String.IsNullOrEmpty(pDocumentoSerie) Then
                NBusq.Dvt_VTSerie = pDocumentoSerie
            End If
            If Not String.IsNullOrEmpty(pDocumentoNumero) Then
                NBusq.Dvt_VTNumer = pDocumentoNumero
            End If
            Documentos = log.Buscar_Documento(NBusq)
            If Documentos.Count > 0 Then
                Dim Provision As eProvisionFacturacion = Documentos(0)
                Dim LogCertificado As New Log_CERTIFICADO
                Dim CertificadoDigital As eCertificadoCredencial = LogCertificado.Buscar(Provision.Emisor_RUC, Provision.Dvt_Fecha_Emision, Nothing)
                Provision.CertificadoCredencial = CertificadoDigital
                Provision.Cliente_Correo_Electronico = pClienteCorreo.Trim
                Dim nProceso As New fcProceso
                nProceso.Emisor_RUC = Provision.Emisor_RUC
                nProceso.SUNAT_Usuario = Provision.CertificadoCredencial.Sunat_Usuario
                nProceso.SUNAT_Contrasena = Provision.CertificadoCredencial.Sunat_Contrasena
                nProceso.ServidorRutaLocal = Provision.Path_Root_App & "\" & Provision.Emisor_RUC
                nProceso.DocumentoTipo = Provision.TpDc_Codigo_St
                nProceso.Etapa = Provision.Etapa_Codigo
                nProceso.FileNameWithOutExtension = Path.GetFileNameWithoutExtension(Provision.Nombre_XML)
                If HerramientasFE.EnviarCorreo(Provision, nProceso) Then
                    Dim LogAcep As New Log_FACTE_DOCUMENTOS
                    LogAcep.Actualizar_Envio_Correo(Provision)
                    Return True
                End If
            End If
        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function
#Region "Creación de Documento SUNAT XML"
    Private pDigestValue As String
    Private pSignatureValue As String
    Sub CrearDocumento_SUNAT(Documento As DocumentoElectronico, EnviarAhora As Boolean)
        Dim CertificadoDigitalCredenciales As eCertificadoCredencial
        Dim LogCertificado As New Log_CERTIFICADO
        CertificadoDigitalCredenciales = LogCertificado.Buscar(Documento.Emisor_Documento_Numero, Documento.Fecha_Emision, Documento.TipoDocumento_Codigo)

        If Not IsNothing(CertificadoDigitalCredenciales) Then

            'STEP1: Create format XML
            Dim nProceso As fcProceso
            If Documento.TipoDocumento_Codigo = SUNATDocumento.Boleta Then
                nProceso = Generar_BoletaVentaFactura(Documento)
            ElseIf Documento.TipoDocumento_Codigo = SUNATDocumento.Factura Then
                nProceso = Generar_BoletaVentaFactura(Documento)
            ElseIf Documento.TipoDocumento_Codigo = SUNATDocumento.NotaCredito Then
                nProceso = Generar_NotaCredito(Documento)
            ElseIf Documento.TipoDocumento_Codigo = SUNATDocumento.NotaDebito Then
                nProceso = Generar_NotaDebito(Documento)
            ElseIf Documento.TipoDocumento_Codigo = SUNATDocumento.GuiaRemisionRemitente Then
                nProceso = Generar_GuiaRemisionRemitente(Documento)
            ElseIf Documento.TipoDocumento_Codigo = SUNATDocumento.ComprobantePercepcion Then
                nProceso = Generar_ComprobantePercepcion(Documento)
            ElseIf Documento.TipoDocumento_Codigo = SUNATDocumento.ComprobanteRetencion Then
                nProceso = Generar_ComprobanteRetencion(Documento)
            End If

            If nProceso.ArchivoCreadoCorrectamente Then
                'STEP2: Signing XML
                XMLFirmar(Documento, CertificadoDigitalCredenciales, nProceso)
                If nProceso.ArchivoFirmadoCorrectamente Then
                    'STEP3: Compress File
                    XMLZip(nProceso)
                    If nProceso.ArchivoComprimidoCorrectamente Then

                        'STEP4: Save Document
                        Ado_FE.Instancia.Documento_InsertarActualizar(Documento)

                        'STEP5: Save Document Client
                        If Documento.SQL_Retorno <> "0" And Not IsNothing(Documento.SQL_XML_Nombre) Then
                            'Guardo el xml en el directorio virtual
                            Dim settings As New XmlWriterSettings With {.ConformanceLevel = ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}
                            Using writer As XmlWriter = XmlWriter.Create(Documento.RootPath_Files & "\" & Documento.Emisor_Documento_Numero & "\xml_pos\" & Documento.SQL_XML_Nombre, settings)
                                Dim typeToSerialize As Type = GetType(DocumentoElectronico)
                                Dim xs As New Serialization.XmlSerializer(typeToSerialize)
                                xs.Serialize(writer, Documento)
                            End Using

                            'STEP6: Copy 'Generados' folder
                            XMLCopiarGenerado(nProceso)
                            If nProceso.GeneradosGuardadoServidorCorrectamente And EnviarAhora Then

                                'STEP7: Send SUNAT
                                'nProceso.SUNAT_Servicio_URL = "https://e-beta.sunat.gob.pe/ol-ti-itcpfegem-beta/billService"
                                'nProceso.SUNAT_Servicio_URL = "https://e-beta.sunat.gob.pe/ol-ti-itemision-guia-gem-beta/billService"
                                nProceso.SUNAT_Servicio_URL = CertificadoDigitalCredenciales.Sunat_Servicio_URL
                                nProceso.Etapa = CertificadoDigitalCredenciales.Etapa_Codigo
                                nProceso.SUNAT_Usuario = CertificadoDigitalCredenciales.Sunat_Usuario
                                nProceso.SUNAT_Contrasena = CertificadoDigitalCredenciales.Sunat_Contrasena

                                nProceso.Correo_Servidor_SMTP = CertificadoDigitalCredenciales.Correo_Servidor_SMTP
                                nProceso.Correo_Servidor_Puerto = CertificadoDigitalCredenciales.Correo_Servidor_Puerto
                                nProceso.Correo_Servidor_SSL = CertificadoDigitalCredenciales.Correo_Servidor_SSL
                                nProceso.Correo_Usuario = CertificadoDigitalCredenciales.Correo_Usuario
                                nProceso.Correo_Contrasena = CertificadoDigitalCredenciales.Correo_Contrasena

                                nProceso.Emisor_RUC = Documento.Emisor_Documento_Numero

                                Dim RptaSUNAT As eSunatRespuesta = HerramientasFE.SUNAT_Bill(nProceso)
                                If RptaSUNAT.Estado = EstadoDocumento.Aceptado Or RptaSUNAT.Estado = EstadoDocumento.Rechazado Then
                                    'STEP8: Update Status Sending SUNAT
                                    Documento.Doc_XML_SUNAT_Resp_Code = RptaSUNAT.Codigo
                                    Documento.Doc_XML_SUNAT_Resp_Description = RptaSUNAT.Mensaje
                                    Documento.Doc_XML_Estado = IIf(RptaSUNAT.Estado = EstadoDocumento.Aceptado, "003", "004")

                                    Dim LogAcep As New Log_FACTE_DOCUMENTOS
                                    LogAcep.Documento_CambiarEstado(Documento)
                                Else
                                    'AGREGADO 2016-10-26
                                    '---No se acepto es probable que el servicio de la SUNAT este offline
                                    Documento.Doc_XML_SUNAT_Resp_Code = RptaSUNAT.Codigo
                                    Documento.Doc_XML_SUNAT_Resp_Description = RptaSUNAT.Mensaje
                                End If

                                If RptaSUNAT.Estado = EstadoDocumento.Aceptado Then
                                    XMLCopiarAceptado(nProceso)
                                    'STEP9: Create PDF
                                    Dim GenPDF As New HerramientasFE

                                    GenPDF.GenerarPDF(Documento, nProceso)
                                    'STEP10: Send Mail
                                    HerramientasFE.EnviarCorreo(Documento, nProceso)
                                    If nProceso.Correo_Enviado_Cliente Then
                                        'STEP11: Update status send
                                        Dim LogAcep As New Log_FACTE_DOCUMENTOS
                                        LogAcep.Actualizar_Envio_Correo(Documento)
                                    End If
                                End If
                                If RptaSUNAT.Estado = EstadoDocumento.Rechazado Then
                                    XMLCopiarRechazado(nProceso)
                                End If

                            End If

                        End If

                        'Borrando los archivos temporales (Temp)
                        LimpiarDirectorio(nProceso)
                    End If
                End If

            End If
        End If
    End Sub
    Function Generar_BoletaVentaFactura(Provision As DocumentoElectronico) As fcProceso
        Dim ProcesoCompleto As New fcProceso With {.ArchivoCreadoCorrectamente = False}
        ProcesoCompleto.ServidorRutaLocal = Provision.RootPath_Enterprise
        ProcesoCompleto.DocumentoTipo = Provision.TipoDocumento_Codigo

        Dim Filename As String = Provision.Emisor_Documento_Numero & "-" & Provision.TipoDocumento_Codigo & "-" & Provision.TipoDocumento_Serie & "-" & Provision.TipoDocumento_Numero
        Dim xmlFilename As String = Filename & ".XML"
        Dim ZipFilename As String = Filename & ".ZIP"
        Provision.Doc_SUNAT_NombreArchivoXML = xmlFilename
        Dim invoice As UblLarsen.Ubl2.InvoiceType
        UblLarsen.Ubl2.UblBaseDocumentType.GlbCustomizationID = "2.0"
        AmountType.TlsDefaultCurrencyID = Provision.Moneda_Codigo

        '   14 - 15 Documentos de referencia
        Dim DocumentosRelacionadas As New List(Of DocumentReferenceType)
        If Not IsNothing(Provision.DocumentoRelacionados) Then
            For Each Doc In Provision.DocumentoRelacionados
                If IsNothing(DocumentosRelacionadas) Then
                    DocumentosRelacionadas = New List(Of DocumentReferenceType)
                End If

                Dim Item As New DocumentReferenceType
                Item.ID = Doc.Documento_Serie & "-" & Doc.Documento_Numero
                Item.DocumentTypeCode = Doc.Documento_Tipo
                DocumentosRelacionadas.Add(Item)
            Next
        End If


        '   Creando los detalles
        Dim ListaDetalles As New List(Of InvoiceLineType)
        For Each ProvisionItem In Provision.Detalles
            Dim ItemDoc As New InvoiceLineType
            ItemDoc.ID = ProvisionItem.Item
            ItemDoc.InvoicedQuantity = New QuantityType() With {.Value = Format(ProvisionItem.Producto_Cantidad, "#0.000"),
                                                                .unitCode = ProvisionItem.Producto_UnidadMedida_Codigo,
                                                                .unitCodeListID = "UN/ECE rec 20",
                                                                .unitCodeListAgencyName = "United Nations Economic Commission for Europe"}
            '   27 Valor de venta por item
            ItemDoc.LineExtensionAmount = Format(ProvisionItem.Item_ValorVenta_Total, "#0.00")

            '22 Precio de venta unitario por item y codigo
            ItemDoc.PricingReference = New PricingReferenceType With {
                     .AlternativeConditionPrice = New PriceType() {New PriceType With {.PriceAmount = Format(ProvisionItem.Unitario_Precio_Venta, "#0.00"),
                                                                                       .PriceTypeCode = New CodeType With {.Value = ProvisionItem.Unitario_Precio_Venta_Tipo_Codigo,
                                                                                                                           .listName = "Tipo de Precio",
                                                                                                                           .listAgencyName = "PE:SUNAT",
                                                                                                                           .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo16"}}}}
            '   26 Descuentos por item
            If ProvisionItem.Unitario_Descuento > 0 Then
                ItemDoc.AllowanceCharge = New AllowanceChargeType() {New AllowanceChargeType With {.ChargeIndicator = False,
                                                                                                   .AllowanceChargeReasonCode = New AllowanceChargeReasonCodeType With { _
                                                                                                       .Value = "00", .listAgencyName = "PE:SUNAT",
                                                                                                       .listName = "Cargo/descuento",
                                                                                                       .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo53"},
                                                                                                   .MultiplierFactorNumeric = 0.5,
                                                                                                   .Amount = Format(ProvisionItem.Unitario_Descuento, "#0.00"),
                                                                                                   .BaseAmount = Format(ProvisionItem.Item_Total, "#0.00")}}
            End If

            'Referencia a los tipo de Afectacion
            Dim Tipo_Afectacion As New TaxSchemeType
            If ProvisionItem.Item_Tipo_Afectacion_IGV_Codigo = "10" Then
                Tipo_Afectacion = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "1000",
                                                                                                    .schemeName = "Codigo de tributos",
                                                                                                    .schemeAgencyName = "PE:SUNAT",
                                                                                                    .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                       .Name = "IGV",
                                                                       .TaxTypeCode = "VAT"}
            ElseIf ProvisionItem.Item_Tipo_Afectacion_IGV_Codigo = "20" Then
                Tipo_Afectacion = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "9997",
                                                                                                    .schemeName = "Codigo de tributos",
                                                                                                    .schemeAgencyName = "PE:SUNAT",
                                                                                                    .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                       .Name = "EXO",
                                                                       .TaxTypeCode = "VAT"}
            ElseIf ProvisionItem.Item_Tipo_Afectacion_IGV_Codigo = "30" Then
                Tipo_Afectacion = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "9998",
                                                                                                    .schemeName = "Codigo de tributos",
                                                                                                    .schemeAgencyName = "PE:SUNAT",
                                                                                                    .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                       .Name = "INA",
                                                                       .TaxTypeCode = "FRE"}
            Else
                Tipo_Afectacion = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "9996",
                                                                                                    .schemeName = "Codigo de tributos",
                                                                                                    .schemeAgencyName = "PE:SUNAT",
                                                                                                    .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                       .Name = "GRA",
                                                                       .TaxTypeCode = "FRE"}
            End If

            '   23 Afectacion al IGV por item
            Dim Item_Afectacion_IGV As New TaxTotalType With {
                .TaxAmount = Format(ProvisionItem.Item_IGV_Total, "#0.00"),
                .TaxSubtotal = New TaxSubtotalType() {New TaxSubtotalType With {
                            .TaxableAmount = Format(ProvisionItem.Item_ValorVenta_Total, "#0.00"),
                            .TaxAmount = Format(ProvisionItem.Item_IGV_Total, "#0.00"),
                            .TaxCategory = New TaxCategoryType With { _
                                .Percent = Format(ProvisionItem.Unitario_IGV_Porcentaje, "#0.00"),
                                .TaxExemptionReasonCode = New CodeType With {.Value = ProvisionItem.Item_Tipo_Afectacion_IGV_Codigo,
                                                                             .listName = "Afectacion del IGV",
                                                                             .listAgencyName = "PE:SUNAT",
                                                                             .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo07"},
                                .TaxScheme = Tipo_Afectacion}}}}

            '   24 Afectacion al ISC
            Dim Item_Afectacion_ISC As TaxTotalType
            If ProvisionItem.Item_ISC_Total > 0 Then
                Item_Afectacion_ISC = New TaxTotalType With {
                    .TaxAmount = Format(ProvisionItem.Item_ISC_Total, "#0.00"),
                    .TaxSubtotal = New TaxSubtotalType() {New TaxSubtotalType() With {
                            .TaxableAmount = Format(ProvisionItem.Item_ValorVenta_Total, "#0.00"),
                            .TaxAmount = Format(ProvisionItem.Item_ISC_Total, "#0.00"),
                            .TaxCategory = New TaxCategoryType() With { _
                                .Percent = Format(ProvisionItem.Unitario_IGV_Porcentaje, "#0.00"),
                                .TierRange = ProvisionItem.Unitario_ISC_Codigo,
                                .TaxScheme = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "2000",
                                                                                                    .schemeName = "Codigo de tributos",
                                                                                                    .schemeAgencyName = "PE:SUNAT",
                                                                                                    .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                     .Name = "ISC",
                                                                     .TaxTypeCode = "EXC"}
                                                       }
                                               }}
                                   }
            End If
            ItemDoc.TaxTotal = New TaxTotalType() {Item_Afectacion_IGV, Item_Afectacion_ISC}

            ItemDoc.Item = New ItemType With {.Description = New TextType() {ProvisionItem.Producto_Descripcion}}
            If Not String.IsNullOrEmpty(ProvisionItem.Producto_Codigo) Then
                ItemDoc.Item.SellersItemIdentification = New ItemIdentificationType With {.ID = ProvisionItem.Producto_Codigo}
            End If
            '21 Valor Unitario por Item
            ItemDoc.Price = New PriceType() With {.PriceAmount = ProvisionItem.Unitario_Valor_Unitario}
            '27 Numero de placa del vehiculo Gastos Art 37
            If Not IsNothing(ProvisionItem.Numero_Placa_del_Vehiculo) Then
                If ProvisionItem.Numero_Placa_del_Vehiculo.Trim <> "" Then
                    ItemDoc.Item.AdditionalItemProperty = New ItemPropertyType() { _
                        New ItemPropertyType With {.Value = ProvisionItem.Numero_Placa_del_Vehiculo,
                                                    .Name = "Gastos Art. 37 Renta:  Número de Placa",
                                                    .NameCode = New CodeType With {.Value = "7000",
                                                                                    .listName = "Propiedad del item",
                                                                                    .listAgencyName = "PE:SUNAT",
                                                                                    .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo55"}
                                                    }}
                End If
            End If

            ListaDetalles.Add(ItemDoc)
        Next

        '   Totales de factura
        'Dim LeyendasTotalesDocumento As New List(Of AdditionalMonetaryTotalType)
        ''   28
        'If Provision.Total_ValorVenta_OperacionesGravadas > 0 Then
        '    LeyendasTotalesDocumento.Add(New AdditionalMonetaryTotalType() With {.ID = "1001", .PayableAmount = Provision.Total_ValorVenta_OperacionesGravadas})
        'End If
        ''   29
        'If Provision.Total_ValorVenta_OperacionesInafectas > 0 Then
        '    LeyendasTotalesDocumento.Add(New AdditionalMonetaryTotalType() With {.ID = "1002", .PayableAmount = Provision.Total_ValorVenta_OperacionesInafectas})
        'End If
        ''   30
        'If Provision.Total_ValorVenta_OperacionesExoneradas > 0 Then
        '    LeyendasTotalesDocumento.Add(New AdditionalMonetaryTotalType() With {.ID = "1003", .PayableAmount = Provision.Total_ValorVenta_OperacionesExoneradas})
        'End If
        ''   31
        'If Provision.Total_ValorVenta_OperacionesGratuitas > 0 Then
        '    LeyendasTotalesDocumento.Add(New AdditionalMonetaryTotalType() With {.ID = "1004", .PayableAmount = Provision.Total_ValorVenta_OperacionesGratuitas})
        'End If
        ''   32
        'If Provision.Total_Descuentos > 0 Then
        '    LeyendasTotalesDocumento.Add(New AdditionalMonetaryTotalType() With {.ID = "2005", .PayableAmount = Provision.Total_Descuentos})
        'End If

        Dim ListaSumatorias As New List(Of TaxTotalType)

        Dim Sumatoria As New TaxTotalType
        Sumatoria.TaxAmount = New AmountType With {.Value = (Format(Provision.Total_IGV + Provision.Total_ISC + Provision.Total_OtrosTributos, "##0.00")),
                                                   .currencyID = Provision.Moneda_Codigo}
        Dim DetallesSumatoria As New List(Of TaxSubtotalType)

        '   33 Sumatoria IGV
        If Provision.Total_IGV > 0 Then
            Dim vSumatoria_IGV As TaxSubtotalType = New TaxSubtotalType With {
                .TaxableAmount = New AmountType With {.Value = Format(Provision.Total_ValorVenta_OperacionesGravadas, "##0.00"),
                                                      .currencyID = Provision.Moneda_Codigo},
                .TaxAmount = New AmountType With {.Value = Provision.Total_IGV,
                                                        .currencyID = Provision.Moneda_Codigo},
                .TaxCategory = New TaxCategoryType() With { _
                                                           .TaxScheme = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "1000",
                                                                                                                               .schemeName = "Codigo de tributos",
                                                                                                                               .schemeAgencyName = "PE:SUNAT",
                                                                                                                              .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                                                .Name = "IGV",
                                                                                                .TaxTypeCode = "VAT"}}}
            DetallesSumatoria.Add(vSumatoria_IGV)
        End If

        '   34 Sumatoria ISC
        If Provision.Total_ISC > 0 Then
            Dim vSumatoria_ISC As TaxSubtotalType = New TaxSubtotalType With {
                .TaxableAmount = New AmountType With {.Value = Format(Provision.Total_ValorVenta_OperacionesGravadas, "##0.00"),
                                                      .currencyID = Provision.Moneda_Codigo},
                .TaxAmount = New AmountType With {.Value = Format(Provision.Total_ISC, "##0.00"),
                                                        .currencyID = Provision.Moneda_Codigo},
                .TaxCategory = New TaxCategoryType() With { _
                                                           .TaxScheme = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "2000",
                                                                                                                               .schemeName = "Codigo de tributos",
                                                                                                                               .schemeAgencyName = "PE:SUNAT",
                                                                                                                              .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                                                .Name = "ISC",
                                                                                                .TaxTypeCode = "EXC"}}}
            DetallesSumatoria.Add(vSumatoria_ISC)
        End If

        '   35 Otros Tributos
        If Provision.Total_OtrosTributos > 0 Then
            Dim vSumatoria_Otrib As TaxSubtotalType = New TaxSubtotalType With {
                .TaxableAmount = New AmountType With {.Value = Format(Provision.Total_ValorVenta_OperacionesGravadas, "##0.00"),
                                                      .currencyID = Provision.Moneda_Codigo},
                .TaxAmount = New AmountType With {.Value = Format(Provision.Total_OtrosTributos, "##0.00"),
                                                        .currencyID = Provision.Moneda_Codigo},
                .TaxCategory = New TaxCategoryType() With { _
                                                           .TaxScheme = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "9999",
                                                                                                                               .schemeName = "Codigo de tributos",
                                                                                                                               .schemeAgencyName = "PE:SUNAT",
                                                                                                                              .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                                                .Name = "OTROS",
                                                                                                .TaxTypeCode = "OTH"}}}
            DetallesSumatoria.Add(vSumatoria_Otrib)
        End If

        If Provision.Total_ValorVenta_OperacionesExoneradas > 0 Then
            Dim vSumatoria_Exonerado As TaxSubtotalType = New TaxSubtotalType With {
                .TaxableAmount = New AmountType With {.Value = Format(Provision.Total_ValorVenta_OperacionesExoneradas, "##0.00"),
                                                      .currencyID = Provision.Moneda_Codigo},
                .TaxAmount = New AmountType With {.Value = Format(0, "##0.00"),
                                                        .currencyID = Provision.Moneda_Codigo},
                .TaxCategory = New TaxCategoryType() With { _
                                                           .TaxScheme = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "9997",
                                                                                                                               .schemeName = "Codigo de tributos",
                                                                                                                               .schemeAgencyName = "PE:SUNAT",
                                                                                                                              .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                                                .Name = "EXO",
                                                                                                .TaxTypeCode = "VAT"}}}
            DetallesSumatoria.Add(vSumatoria_Exonerado)
        End If

        If Provision.Total_ValorVenta_OperacionesInafectas > 0 Then
            Dim vSumatoria_Inafecto As TaxSubtotalType = New TaxSubtotalType With {
                .TaxableAmount = New AmountType With {.Value = Format(Provision.Total_ValorVenta_OperacionesInafectas, "##0.00"),
                                                      .currencyID = Provision.Moneda_Codigo},
                .TaxAmount = New AmountType With {.Value = Format(0, "##0.00"),
                                                        .currencyID = Provision.Moneda_Codigo},
                .TaxCategory = New TaxCategoryType() With { _
                                                           .TaxScheme = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "9998",
                                                                                                                               .schemeName = "Codigo de tributos",
                                                                                                                               .schemeAgencyName = "PE:SUNAT",
                                                                                                                              .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                                                .Name = "INA",
                                                                                                .TaxTypeCode = "FRE"}}}
            DetallesSumatoria.Add(vSumatoria_Inafecto)
        End If

        If Provision.Total_ValorVenta_OperacionesGratuitas > 0 Then
            Dim vSumatoria_Gratuito As TaxSubtotalType = New TaxSubtotalType With {
                .TaxableAmount = New AmountType With {.Value = Format(Provision.Total_ValorVenta_OperacionesGratuitas, "##0.00"),
                                                      .currencyID = Provision.Moneda_Codigo},
                .TaxAmount = New AmountType With {.Value = Format(0, "##0.00"),
                                                        .currencyID = Provision.Moneda_Codigo},
                .TaxCategory = New TaxCategoryType() With { _
                                                           .TaxScheme = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "9996",
                                                                                                                               .schemeName = "Codigo de tributos",
                                                                                                                               .schemeAgencyName = "PE:SUNAT",
                                                                                                                              .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                                                .Name = "GRA",
                                                                                                .TaxTypeCode = "FRE"}}}
            DetallesSumatoria.Add(vSumatoria_Gratuito)
        End If

        Sumatoria.TaxSubtotal = DetallesSumatoria.ToArray
        ListaSumatorias.Add(Sumatoria)
        ''

        Dim TotalMonedas As New MonetaryTotalType
        '   36 Descuentos globales ????¿¿¿¿¿ <sac:AdditionalMonetaryTotal><cbc:ID>2005</cbc:ID><cbc:PayableAmount currencyID="PEN">59230.51</cbc:PayableAmount></sac:AdditionalMonetaryTotal>
        If Provision.Total_Descuentos > 0 Then
            TotalMonedas.AllowanceTotalAmount = Format(Provision.Total_Descuentos, "##0.00")
        End If
        '   37 Sumatoria Otros cargos
        If Provision.Total_OtrosCargos > 0 Then
            TotalMonedas.ChargeTotalAmount = New AmountType With {.Value = Format(Provision.Total_OtrosCargos, "##0.00"), .currencyID = Provision.Moneda_Codigo}
        End If
        '   38 Importe total de la venta, cesion en uso o del servicio prestado
        TotalMonedas.LineExtensionAmount = New AmountType With {.Value = Format(Provision.Total_ValorVenta_Total, "##0.00"),
                                                                .currencyID = Provision.Moneda_Codigo}

        TotalMonedas.TaxInclusiveAmount = New AmountType With {.Value = Format(Provision.Total_Importe_Venta, "##0.00"),
                                                                .currencyID = Provision.Moneda_Codigo}

        TotalMonedas.PayableAmount = New AmountType With {.Value = Format((Provision.Total_Importe_Venta - Provision.Total_Descuentos + Provision.Total_OtrosCargos), "##0.00"),
                                                                .currencyID = Provision.Moneda_Codigo}

        '   39 Importe de la percepcion --- en moneda nacional -- revisar
        'If Provision.Total_Importe_Percepcion > 0 Then
        '    LeyendasTotalesDocumento.Add(New AdditionalMonetaryTotalType() With {.ID = "2001", .ReferenceAmount = Provision.Total_Importe_Venta, .PayableAmount = Provision.Total_Importe_Percepcion, .TotalAmount = Provision.Percep_Importe_Total_Cobrado})
        'End If

        '   40-41-42 Anticipos
        Dim Anticipos As New List(Of PaymentType)
        '   43 Total anticipos
        If Provision.Total_Anticipos > 0 Then
            For Each pAnt In Provision.DocumentoAnticipos
                Anticipos.Add(New PaymentType With {.PaidAmount = pAnt.Anticipo_Monto,
                                                    .ID = New IdentifierType With {.schemeID = pAnt.Anticipo_Documento_Tipo,
                                                                                   .Value = pAnt.Anticipo_Documento_Serie & "-" & pAnt.Anticipo_Documento_Numero,
                                                                                   .schemeName = "SUNAT:Identificador de Documentos Relacionados",
                                                                                   .schemeAgencyName = "PE:SUNAT"},
                                                    .InstructionID = New IdentifierType With {.schemeID = pAnt.Anticipo_Emisor_Documento_Tipo, .Value = pAnt.Anticipo_Emisor_Documento_Numero}})
            Next
            TotalMonedas.PrepaidAmount = New AmountType With {.Value = Format(Provision.Total_Anticipos, "##0.00"),
                                                                .currencyID = Provision.Moneda_Codigo}
        End If
        '   44 Codigo del tipo de operacion - Catag. Nº 17
        'Dim TipoOperacion As New List(Of AdditionalPropertyType)
        'If Not String.IsNullOrEmpty(Provision.Tipo_Operacion_Codigo) Then
        '    TipoOperacion.Add(New AdditionalPropertyType With {.ID = Provision.Tipo_Operacion_Codigo})
        'End If

        '   46 Dirección del lugar en el que se entrega el bien o se presta el servicio
        'Dim LugarEntrega As SupplierPartyType
        'If Not String.IsNullOrEmpty(Provision.Adic_Lugar_Entrega_Bien_PrestaServicio_Ubigeo) Then
        '    LugarEntrega = New SupplierPartyType With {.Party = New PartyType With {.PostalAddress = New AddressType() With {
        '                 .ID = Provision.Adic_Lugar_Entrega_Bien_PrestaServicio_Ubigeo,
        '                 .StreetName = Provision.Adic_Lugar_Entrega_Bien_PrestaServicio_Calle,
        '                 .CitySubdivisionName = Provision.Adic_Lugar_Entrega_Bien_PrestaServicio_Urbanizacion,
        '                 .CityName = Provision.Adic_Lugar_Entrega_Bien_PrestaServicio_Provincia,
        '                 .CountrySubentity = Provision.Adic_Lugar_Entrega_Bien_PrestaServicio_Departamento,
        '                 .District = Provision.Adic_Lugar_Entrega_Bien_PrestaServicio_Distrito,
        '                 .Country = New CountryType() With {
        '                     .IdentificationCode = Provision.Adic_Lugar_Entrega_Bien_PrestaServicio_PaisCodigo
        '                }
        '            }}}
        'End If

        '   58, 59, 60, 61 Transportista
        'Dim Transportista As PartyType()
        'If Not String.IsNullOrEmpty(Provision.Transportista_Documento_Tipo) And Not String.IsNullOrEmpty(Provision.Transportista_Documento_Numero) Then
        '    Transportista = New PartyType() {New PartyType With {.PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType With {.ID = New IdentifierType With {.schemeID = Provision.Transportista_Documento_Tipo, .Value = Provision.Transportista_Documento_Numero}}}, .PartyName = New PartyNameType() {New PartyNameType() With {.Name = Provision.Transportista_RazonSocial_Nombre}}}}
        'End If

        '   62, 63 Conductor
        'Dim Conductor As PartyIdentificationType
        'If Not String.IsNullOrEmpty(Provision.Conductor_Documento_Tipo) And Not String.IsNullOrEmpty(Provision.Conductor_Documento_Numero) Then
        '    Conductor = New PartyIdentificationType With {.ID = New IdentifierType With {.schemeID = Provision.Conductor_Documento_Tipo, .Value = Provision.Conductor_Documento_Numero}}
        'End If

        '   68 Placa
        'Dim Placa_Vehiculo As SUNATCostsType
        'If Not String.IsNullOrEmpty(Provision.Numero_Placa_del_Vehiculo) Then
        '    Placa_Vehiculo = New SUNATCostsType With {.RoadTransport = New RoadTransportType With {.LicensePlateID = Provision.Numero_Placa_del_Vehiculo}}
        'End If

        '   Guia Factura
        'Dim GuiaFact As SUNATEmbededDespatchAdviceType
        'If Not String.IsNullOrEmpty(Provision.GuiaFactura_Documento_Tipo_Codigo) And Not String.IsNullOrEmpty(Provision.GuiaFactura_Documento_Serie) And Not String.IsNullOrEmpty(Provision.GuiaFactura_Documento_Numero) Then
        'GuiaFact = New SUNATEmbededDespatchAdviceType
        ''   185-2015 - 48
        'GuiaFact.OriginAddress = New AddressType() With {.ID = Provision.Salida_Ubigeo, .StreetName = Provision.Salida_Direccion}
        'GuiaFact.DeliveryAddress = New AddressType() With {.ID = Provision.Llegada_Ubigeo, .StreetName = Provision.Llegada_Direccion}

        '   47, 48, 49, 50 Transportista - Numero, serie, tipo doc, numero de remitente original
        'GuiaFact.OrderReference = New OrderReferenceType() {New OrderReferenceType With {.DocumentReference = New DocumentReferenceType With {.ID = Provision.GuiaFactura_Documento_Serie & "-" & Provision.GuiaFactura_Documento_Numero, .DocumentTypeCode = Provision.GuiaFactura_Documento_Tipo_Codigo}, .CustomerReference = Provision.GuiaFactura_RemitenteDocumentoOriginal_NumDoc}}

        '   51, 52, 53 Destinatario
        'GuiaFact.DeliveryCustomerParty = New CustomerPartyType With {.CustomerAssignedAccountID = New IdentifierType() With {.schemeID = Provision.Cliente_Documento_Tipo, .Value = Provision.Cliente_Documento_Numero},
        '.Party = New PartyType() With {.PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType() With {.RegistrationName = New NameType With {.Value = Provision.Cliente_RazonSocial_Nombre}}}}}

        '   54, 55, 56, 57 Datos relacionados con el traslado de los bienes, 64, 65 Vehiculo
        'GuiaFact.Shipment = New ShipmentType() With {.ShippingPriorityLevelCode = Provision.MotivoTraslad_Codigo,
        '                                             .Consignment = New ConsignmentType With {.NetWeightMeasure = New MeasureType With {.unitCode = Provision.PesoBrutoTotalGuia_UnidMed, .Value = Provision.PesoBrutoTotalGuia_Valor}},
        '                                             .ShipmentStage = New ShipmentStageType() {New ShipmentStageType With {
        '                                                     .TransportModeCode = Provision.ModalidadTraslado_Codigo,
        '                                                     .TransitPeriod = New PeriodType() With {.StartDate = Provision.Fecha_Inicio_Traslado_o_Entrega_Bienes_Al_Transportista},
        '                                                     .CarrierParty = Transportista,
        '                                                     .DriverPerson = Conductor,
        '                                                     .TransportMeans = New TransportMeansType() With {.RoadTransport = New RoadTransportType() With {.LicensePlateID = Provision.Transportes(0).Placa}, .RegistrationNationalityID = Provision.Transportes(0).NumeroInscripcion_CertificadoHabilitacionVehicular}
        '                                                 }},
        '                                             .Delivery = New DeliveryType With {.DeliveryAddress = New AddressType() With {.ID = Provision.Llegada_Ubigeo, .StreetName = Provision.Llegada_Direccion}},
        '                                             .OriginAddress = New AddressType With {.ID = Provision.Salida_Ubigeo, .StreetName = Provision.Salida_Direccion}
        '                                            }

        'GuiaFact.Shipment = New ShipmentType() With {.ShippingPriorityLevelCode = Provision.MotivoTraslad_Codigo,
        '                                            .Consignment = New ConsignmentType With {.NetWeightMeasure = New MeasureType With {.unitCode = Provision.PesoBrutoTotalGuia_UnidMed, .Value = Provision.PesoBrutoTotalGuia_Valor}},
        '                                            .ShipmentStage = New ShipmentStageType() {New ShipmentStageType With {
        '                                                    .TransportModeCode = Provision.ModalidadTraslado_Codigo,
        '                                                    .TransitPeriod = New PeriodType() With {.StartDate = Provision.Fecha_Inicio_Traslado_o_Entrega_Bienes_Al_Transportista},
        '                                                    .CarrierParty = Transportista,
        '                                                    .DriverPerson = Conductor,
        '                                                    .TransportMeans = New TransportMeansType() With {.RoadTransport = New RoadTransportType() With {.LicensePlateID = Provision.Transportes(0).Placa}, .RegistrationNationalityID = Provision.Transportes(0).NumeroInscripcion_CertificadoHabilitacionVehicular}
        '                                                }},
        '                                            .Delivery = New DeliveryType With {.DeliveryAddress = New AddressType() With {.ID = Provision.Llegada_Ubigeo, .StreetName = Provision.Llegada_Direccion}},
        '                                            .OriginAddress = New AddressType With {.ID = Provision.Salida_Ubigeo, .StreetName = Provision.Salida_Direccion}
        '                                           }

        'If Not String.IsNullOrEmpty(Provision.Transportista_Documento_Tipo) And Not String.IsNullOrEmpty(Provision.Transportista_Documento_Numero) Then
        '    Transportista = New PartyType() {New PartyType With {.PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType With {.ID = New IdentifierType With {.schemeID = Provision.Transportista_Documento_Tipo, .Value = Provision.Transportista_Documento_Numero}}}, .PartyName = New PartyNameType() {New PartyNameType() With {.Name = Provision.Transportista_RazonSocial_Nombre}}}}
        '    GuiaFact.OrderReference = New OrderReferenceType() {New OrderReferenceType With {.DocumentReference = New DocumentReferenceType With {.ID = Provision.Transportista_Documento_Numero, .DocumentTypeCode = Provision.Transportista_Documento_Tipo}}}
        'End If

        'End If

        'Creando cabecera
        invoice = New UblLarsen.Ubl2.InvoiceType() With {
            .ID = Provision.TipoDocumento_Serie & "-" & Provision.TipoDocumento_Numero,
             .IssueDate = Provision.Fecha_Emision,
            .DueDate = Provision.Fecha_Vencimiento,
             .InvoiceTypeCode = New CodeType With {.Value = Provision.TipoDocumento_Codigo,
                                                   .listAgencyName = "PE:SUNAT",
                                                   .listName = "Tipo de Documento",
                                                   .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo01",
                                                   .listID = "0101", .name = "Tipo de Operacion",
                                                   .listSchemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo51"},
            .DocumentCurrencyCode = New CurrencyCodeType() With {.Value = Provision.Moneda_Codigo,
                                                                 .listID = "ISO 4217 Alpha",
                                                                 .listName = "Currency",
                                                                 .listAgencyName = "United Nations Economic Commission for Europe"},
            .Signature = New SignatureType() {New SignatureType() With {.ID = Provision.Emisor_Documento_Numero,
                        .SignatoryParty = New PartyType() With {.PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType() With {.ID = Provision.Emisor_Documento_Numero}},
                                                                .PartyName = New PartyNameType() {New PartyNameType() With {.Name = Provision.Emisor_ApellidosNombres_RazonSocial}}},
                        .DigitalSignatureAttachment = New AttachmentType() With {.ExternalReference = New ExternalReferenceType() With {.URI = "SIGEDGO"}}}},
             .AccountingSupplierParty = New SupplierPartyType() With {
                 .Party = New PartyType() With { _
                     .PartyIdentification = New PartyIdentificationType() _
                                             {New PartyIdentificationType With {.ID = New IdentifierType _
                                                                               With {.Value = Provision.Emisor_Documento_Numero, _
                                                                                     .schemeID = "6", _
                                                                                     .schemeName = "Documento de Identidad", _
                                                                                     .schemeAgencyName = "PE:SUNAT", _
                                                                                     .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"}}},
                    .PartyName = New PartyNameType() {New PartyNameType With {.Name = Provision.Emisor_ApellidosNombres_RazonSocial}},
                    .PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType With {.RegistrationName = Provision.Emisor_ApellidosNombres_RazonSocial,
                                                                                                    .RegistrationAddress = New AddressType With { _
                                                                                                        .AddressTypeCode = New CodeType With {.Value = "0000",
                                                                                                                                            .listAgencyName = "PE:SUNAT",
                                                                                                                                            .listName = "Establecimientos anexos"},
                                                                                                        .ID = New IdentifierType With {.Value = Provision.Emisor_Direccion_Ubigeo,
                                                                                                                                      .schemeAgencyName = "PE:INEI",
                                                                                                                                      .schemeName = "Ubigeos"},
                                                                                                        .AddressLine = New AddressLineType() {New AddressLineType With {.Line = Provision.Emisor_Direccion_Calle}},
                                                                                                        .CitySubdivisionName = Provision.Emisor_Direccion_Urbanizacion,
                                                                                                        .CityName = Provision.Emisor_Direccion_Provincia,
                                                                                                        .CountrySubentity = Provision.Emisor_Direccion_Departamento,
                                                                                                        .District = Provision.Emisor_Direccion_Distrito,
                                                                                                        .Country = New CountryType With {
                                                                                                        .IdentificationCode = New CountryIdentificationCodeType With {.Value = Provision.Emisor_Direccion_PaisCodigo,
                                                                                                                                                                     .listID = "ISO 3166-1",
                                                                                                                                                                     .listAgencyName = "United Nations Economic Commission for Europe",
                                                                                                                                                                      .listName = "Country"}}}}}}},
             .AccountingCustomerParty = New CustomerPartyType() With {
                 .Party = New PartyType() With {
                     .PartyIdentification = New PartyIdentificationType() _
                                             {New PartyIdentificationType With {.ID = New IdentifierType _
                                                                               With {.Value = Provision.Cliente_Documento_Numero, _
                                                                                     .schemeID = Provision.Cliente_Documento_Tipo, _
                                                                                     .schemeName = "Documento de Identidad", _
                                                                                     .schemeAgencyName = "PE:SUNAT", _
                                                                                     .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"}}},
                     .PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType With _
                                                                     {.RegistrationName = Provision.Cliente_RazonSocial_Nombre}}
                }
            },
            .DespatchDocumentReference = DocumentosRelacionadas.ToArray,
             .TaxTotal = ListaSumatorias.ToArray,
             .LegalMonetaryTotal = TotalMonedas,
             .InvoiceLine = ListaDetalles.ToArray,
        .PrepaidPayment = Anticipos.ToArray
        }


        '.PostalAddress = New AddressType() With {
        '    .ID = Provision.Emisor_Direccion_Ubigeo,
        '    .StreetName = Provision.Emisor_Direccion_Calle,
        '    .CitySubdivisionName = Provision.Emisor_Direccion_Urbanizacion,
        '    .CityName = Provision.Emisor_Direccion_Provincia,
        '    .CountrySubentity = Provision.Emisor_Direccion_Departamento,
        '    .District = Provision.Emisor_Direccion_Distrito,
        '    .Country = New CountryType() With {
        '        .IdentificationCode = Provision.Emisor_Direccion_PaisCodigo

        If Provision.Fecha_Vencimiento.Year = 1 Then
            invoice.DueDate = Nothing
        Else
            invoice.DueDate = Provision.Fecha_Vencimiento
        End If



        '.ID = "1001", .PayableAmount = DocumentoXMLCliente.Dvt_Total_Gravado

        'Cargando las leyendas adicionales
        'Dim LeyendasAdicionales As AdditionalPropertyType() = New AdditionalPropertyType(Provision.Leyendas.Count - 1) {}
        'For i = 0 To Provision.Leyendas.Count - 1
        '    LeyendasAdicionales(i) = New AdditionalPropertyType() With {
        '         .ID = Provision.Leyendas(i).Codigo, .Value = Provision.Leyendas(i).Descripcion
        '        }
        'Next

        'Notas
        Dim Notas As New List(Of TextType)
        If Not IsNothing(Provision.Leyendas) Then
            For Each Leyendas In Provision.Leyendas
                Notas.Add(New TextType With {.Value = Leyendas.Descripcion,
                                             .languageLocaleID = Leyendas.Codigo
                          })
            Next
        End If

        invoice.Note = Notas.ToArray
        'invoice.LineCountNumeric = Provision.Detalles.Count


        'Agregando los extension: Firma del documento y campos personalizados
        '.AdditionalProperty = LeyendasAdicionales reemplazado por Notas
        invoice.UBLExtensions = New UBLExtensionType() {New UBLExtensionType() With {.ExtensionContent = New ExtensionContentType()}}
        'New UBLExtensionType() With {.ExtensionContent = New ExtensionContentType() With
        '{.AdditionalInformation = New AdditionalInformationType() With {.AdditionalMonetaryTotal = LeyendasTotalesDocumento.ToArray, .SUNATTransaction = TipoOperacion.ToArray, .SUNATEmbededDespatchAdvice = GuiaFact, .SUNATCosts = Placa_Vehiculo}}},
        'invoice.UBLExtensions = New UBLExtensionType() {New UBLExtensionType() With {.ExtensionContent = New ExtensionContentType()},
        '                                        New UBLExtensionType() With {.ExtensionContent = New ExtensionContentType()}}

        'Agregando namespace
        Dim ns As XmlSerializerNamespaces = New XmlSerializerNamespaces()
        ns.Add("ds", "http://www.w3.org/2000/09/xmldsig#")
        'ns.Add("sac", "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")

        ns.Add("ccts", "urn:oasis:names:specification:ubl:schema:xsd:CoreComponentParameters-2")
        ns.Add("stat", "urn:oasis:names:specification:ubl:schema:xsd:DocumentStatusCode-1.0")



        Dim settings As New XmlWriterSettings With {.ConformanceLevel = ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.GetEncoding("ISO-8859-1")}
        Using writer As XmlWriter = XmlWriter.Create(Provision.RootPath_Enterprise & "\xml_temp\" & xmlFilename, settings)
            Dim typeToSerialize As Type = GetType(UblLarsen.Ubl2.InvoiceType)
            Dim xs As New XmlSerializer(typeToSerialize)
            xs.Serialize(writer, invoice, ns)
        End Using
        ProcesoCompleto.ExtensionContent_Indice = 0

        ProcesoCompleto.FileNameWithOutExtension = Filename
        ProcesoCompleto.ArchivoCreadoCorrectamente = True

        Return ProcesoCompleto
    End Function
    Function Generar_NotaCredito(Provision As DocumentoElectronico) As fcProceso
        Dim ProcesoCompleto As New fcProceso With {.ArchivoCreadoCorrectamente = False}
        ProcesoCompleto.ServidorRutaLocal = Provision.RootPath_Enterprise
        ProcesoCompleto.DocumentoTipo = Provision.TipoDocumento_Codigo

        Dim Filename As String = Provision.Emisor_Documento_Numero & "-" & Provision.TipoDocumento_Codigo & "-" & Provision.TipoDocumento_Serie & "-" & Provision.TipoDocumento_Numero
        Dim xmlFilename As String = Filename & ".XML"
        Dim ZipFilename As String = Filename & ".ZIP"

        Provision.Doc_SUNAT_NombreArchivoXML = xmlFilename

        Dim CreditNote As UblLarsen.Ubl2.CreditNoteType
        UblLarsen.Ubl2.UblBaseDocumentType.GlbCustomizationID = "2.0"
        AmountType.TlsDefaultCurrencyID = Provision.Moneda_Codigo

        'Creando los detalles
        Dim ListaDetalles As New List(Of CreditNoteLineType)
        For Each ProvisionItem In Provision.Detalles
            Dim ItemDoc As New CreditNoteLineType
            ItemDoc.ID = ProvisionItem.Item
            ItemDoc.CreditedQuantity = New QuantityType With { _
                            .Value = Format(ProvisionItem.Producto_Cantidad, "#0.000"),
                            .unitCode = ProvisionItem.Producto_UnidadMedida_Codigo,
                            .unitCodeListID = "UN/ECE rec 20",
                            .unitCodeListAgencyName = "United Nations Economic Commission for Europe"}
            ItemDoc.LineExtensionAmount = Format(ProvisionItem.Item_ValorVenta_Total, "#0.00")
            ItemDoc.PricingReference = New PricingReferenceType With { _
                                    .AlternativeConditionPrice = New PriceType() {New PriceType() With { _
                                                .PriceAmount = Format(ProvisionItem.Unitario_Precio_Venta, "#0.00"),
                                                .PriceTypeCode = New CodeType With {.Value = ProvisionItem.Unitario_Precio_Venta_Tipo_Codigo,
                                                                                    .listName = "Tipo de Precio",
                                                                                    .listAgencyName = "PE:SUNAT",
                                                                                    .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo16"}}}}

            ItemDoc.TaxTotal = New TaxTotalType() {New TaxTotalType() With {
                         .TaxAmount = Format(ProvisionItem.Item_IGV_Total, "#0.00"),
                         .TaxSubtotal = New TaxSubtotalType() {New TaxSubtotalType With {
                                .TaxableAmount = Format(ProvisionItem.Item_ValorVenta_Total, "#0.00"),
                                .TaxAmount = Format(ProvisionItem.Item_IGV_Total, "#0.00"),
                                .TaxCategory = New TaxCategoryType() With {
                                .Percent = Format(ProvisionItem.Unitario_IGV_Porcentaje, "#0.00"),
                                .TaxExemptionReasonCode = New CodeType With {.Value = ProvisionItem.Item_Tipo_Afectacion_IGV_Codigo,
                                                                             .listName = "Afectacion del IGV",
                                                                             .listAgencyName = "PE:SUNAT",
                                                                             .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo07"},
                                .TaxScheme = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "1000",
                                                                                                    .schemeName = "Codigo de tributos",
                                                                                                    .schemeAgencyName = "PE:SUNAT",
                                                                                                    .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                       .Name = "IGV",
                                                                       .TaxTypeCode = "VAT"}
                            }
                        }}
                    }}
            ItemDoc.Item = New ItemType() With {
                         .Description = New TextType() {ProvisionItem.Producto_Descripcion}
                    }
            If Not String.IsNullOrEmpty(ProvisionItem.Producto_Codigo) Then
                ItemDoc.Item.SellersItemIdentification = New ItemIdentificationType With {.ID = ProvisionItem.Producto_Codigo}
            End If
            ItemDoc.Price = New PriceType() With {
                         .PriceAmount = ProvisionItem.Unitario_Valor_Unitario
                    }
            ListaDetalles.Add(ItemDoc)
        Next



        Dim TotalMonedas As New MonetaryTotalType
        TotalMonedas.PayableAmount = New AmountType With { _
            .Value = Format((Provision.Total_Importe_Venta - Provision.Total_Descuentos + Provision.Total_OtrosCargos), "##0.00")}
        'If Provision.Total_Descuentos > 0 Then
        '    TotalMonedas.AllowanceTotalAmount = Format(Provision.Total_Descuentos, "##0.00")
        'End If

        If Provision.Total_OtrosCargos > 0 Then
            TotalMonedas.ChargeTotalAmount = New AmountType With {.Value = Format(Provision.Total_OtrosCargos, "##0.00")}
        End If

        If Provision.Total_Anticipos > 0 Then
            TotalMonedas.PrepaidAmount = New AmountType With {.Value = Format(Provision.Total_Anticipos, "##0.00")}
        End If


        'Creando los documentos afectados con tipo de afectacion
        Dim DiscrepancyList As New List(Of ResponseType)
        'Creando los documentos modifica
        Dim BillingReferenceList As New List(Of BillingReferenceType)

        Dim IDiscrep As New ResponseType
        For Each ItemRel In Provision.DocumentoAfectados
            IDiscrep.ResponseCode = New CodeType With {.Value = ItemRel.Documento_Ref_Tipo,
                                                   .listAgencyName = "PE:SUNAT",
                                                   .listName = "Tipo de nota de credito",
                                                   .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo09"}

            IDiscrep.Description = New TextType() {New TextType With {.Value = Provision.Observaciones}}
            DiscrepancyList.Add(IDiscrep)
            Exit For
        Next
        
        For Each ItemRel In Provision.DocumentoAfectados
            Dim IBillRef As New BillingReferenceType
            IBillRef.InvoiceDocumentReference = New DocumentReferenceType With {.ID = ItemRel.Documento_Serie & "-" & ItemRel.Documento_Numero,
                                                                                .DocumentTypeCode = New CodeType With {.Value = ItemRel.Documento_Tipo,
                                                                                                                       .listAgencyName = "PE:SUNAT",
                                                                                                                       .listName = "Tipo de Documento",
                                                                                                                       .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo01"}}
            BillingReferenceList.Add(IBillRef)
        Next

        'Creando cabecera
        CreditNote = New UblLarsen.Ubl2.CreditNoteType With { _
            .ID = Provision.TipoDocumento_Serie & "-" & Provision.TipoDocumento_Numero, _
            .IssueDate = Provision.Fecha_Emision, _
            .DocumentCurrencyCode = New CurrencyCodeType With {.Value = Provision.Moneda_Codigo}, _
            .DiscrepancyResponse = DiscrepancyList.ToArray, _
            .BillingReference = BillingReferenceList.ToArray, _
            .Signature = New SignatureType() {New SignatureType() With {.ID = Provision.Emisor_Documento_Numero, _
                        .SignatoryParty = New PartyType() With {.PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType() With {.ID = Provision.Emisor_Documento_Numero}},
                                                                .PartyName = New PartyNameType() {New PartyNameType() With {.Name = Provision.Emisor_ApellidosNombres_RazonSocial}}}, _
                        .DigitalSignatureAttachment = New AttachmentType() With {.ExternalReference = New ExternalReferenceType() With {.URI = "SIGEDGO"}}}}, _
             .AccountingSupplierParty = New SupplierPartyType With { _
                 .Party = New PartyType With {.PartyIdentification = New PartyIdentificationType() _
                                             {New PartyIdentificationType With {.ID = New IdentifierType _
                                                                               With {.Value = Provision.Emisor_Documento_Numero, _
                                                                                     .schemeID = "6", _
                                                                                     .schemeName = "Documento de Identidad", _
                                                                                     .schemeAgencyName = "PE:SUNAT", _
                                                                                     .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"}}},
                                              .PartyName = New PartyNameType() {New PartyNameType With {.Name = Provision.Emisor_ApellidosNombres_RazonSocial}},
                                              .PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType With {.RegistrationName = Provision.Emisor_ApellidosNombres_RazonSocial,
                                                                                                                             .RegistrationAddress = New AddressType With {.AddressTypeCode = _
                                                                                                                                 New CodeType With {.Value = "0000",
                                                                                                                                                    .listAgencyName = "PE:SUNAT",
                                                                                                                                                    .listName = "Establecimientos anexos"}}}}}},
             .AccountingCustomerParty = New CustomerPartyType With { _
                 .Party = New PartyType With { _
                     .PartyIdentification = New PartyIdentificationType() _
                                             {New PartyIdentificationType With {.ID = New IdentifierType _
                                                                               With {.Value = Provision.Cliente_Documento_Numero, _
                                                                                     .schemeID = Provision.Cliente_Documento_Tipo, _
                                                                                     .schemeName = "Documento de Identidad", _
                                                                                     .schemeAgencyName = "PE:SUNAT", _
                                                                                     .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"}}},
                     .PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType With _
                                                                     {.RegistrationName = Provision.Cliente_RazonSocial_Nombre}}}}, _
             .TaxTotal = New TaxTotalType() {New TaxTotalType With { _
                 .TaxAmount = Format(Provision.Total_IGV, "##0.00"), _
                 .TaxSubtotal = New TaxSubtotalType() {New TaxSubtotalType() With { _
                         .TaxableAmount = Format(Provision.Total_ValorVenta_OperacionesGravadas, "##0.00"), _
                     .TaxAmount = Format(Provision.Total_IGV, "##0.00"), _
                     .TaxCategory = New TaxCategoryType With { _
                         .TaxScheme = New TaxSchemeType With { _
                             .ID = New IdentifierType With {.Value = "1000",
                                                            .schemeName = "Codigo de tributos",
                                                            .schemeAgencyName = "PE:SUNAT",
                                                            .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                            .Name = "IGV",
                            .TaxTypeCode = "VAT"
                        } _
                    } _
                }} _
            }}, _
             .LegalMonetaryTotal = TotalMonedas, _
            .CreditNoteLine = ListaDetalles.ToArray
        }

        'Agregando los extension: Firma del documento y campos personalizados
        CreditNote.UBLExtensions = New UBLExtensionType() {New UBLExtensionType() With {.ExtensionContent = New ExtensionContentType()}}
        'Agregando namespace
        Dim ns As XmlSerializerNamespaces = New XmlSerializerNamespaces()
        'ns.Add("ccts", "urn:un:unece:uncefact:documentation:2")
        ns.Add("ds", "http://www.w3.org/2000/09/xmldsig#")
        ns.Add("sac", "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")
        'ns.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance")
        'ns.Add("schemaLocation", "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 ..\xsd\maindoc\UBLPE-Invoice-2.0.xsd")

        Dim settings As New XmlWriterSettings With {.ConformanceLevel = ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.GetEncoding("ISO-8859-1")}
        Using writer As XmlWriter = XmlWriter.Create(Provision.RootPath_Enterprise & "\xml_temp\" & xmlFilename, settings)
            Dim typeToSerialize As Type = GetType(UblLarsen.Ubl2.CreditNoteType)
            Dim xs As New XmlSerializer(typeToSerialize)
            xs.Serialize(writer, CreditNote, ns)
        End Using

        ProcesoCompleto.ExtensionContent_Indice = 0

        ProcesoCompleto.FileNameWithOutExtension = Filename
        ProcesoCompleto.ArchivoCreadoCorrectamente = True

        Return ProcesoCompleto
    End Function
    Function Generar_NotaDebito(Provision As DocumentoElectronico) As fcProceso
        Dim ProcesoCompleto As New fcProceso With {.ArchivoCreadoCorrectamente = False}
        ProcesoCompleto.ServidorRutaLocal = Provision.RootPath_Enterprise
        ProcesoCompleto.DocumentoTipo = Provision.TipoDocumento_Codigo

        Dim Filename As String = Provision.Emisor_Documento_Numero & "-" & Provision.TipoDocumento_Codigo & "-" & Provision.TipoDocumento_Serie & "-" & Provision.TipoDocumento_Numero
        Dim xmlFilename As String = Filename & ".XML"
        Dim ZipFilename As String = Filename & ".ZIP"

        Provision.Doc_SUNAT_NombreArchivoXML = xmlFilename

        Dim DebitNote As UblLarsen.Ubl2.DebitNoteType
        UblLarsen.Ubl2.UblBaseDocumentType.GlbCustomizationID = "2.0"
        AmountType.TlsDefaultCurrencyID = Provision.Moneda_Codigo

        'Creando los detalles
        Dim ListaDetalles As New List(Of DebitNoteLineType)
        For Each ProvisionItem In Provision.Detalles
            Dim ItemDoc As New DebitNoteLineType
            ItemDoc.ID = ProvisionItem.Item
            ItemDoc.DebitedQuantity = New QuantityType With { _
                            .Value = Format(ProvisionItem.Producto_Cantidad, "#0.000"),
                            .unitCode = ProvisionItem.Producto_UnidadMedida_Codigo,
                            .unitCodeListID = "UN/ECE rec 20",
                            .unitCodeListAgencyName = "United Nations Economic Commission for Europe"}
            ItemDoc.LineExtensionAmount = Format(ProvisionItem.Item_ValorVenta_Total, "#0.00")
            ItemDoc.PricingReference = New PricingReferenceType With { _
                                    .AlternativeConditionPrice = New PriceType() {New PriceType With { _
                                                .PriceAmount = Format(ProvisionItem.Unitario_Precio_Venta, "#0.00"),
                                                .PriceTypeCode = New CodeType With {.Value = ProvisionItem.Unitario_Precio_Venta_Tipo_Codigo,
                                                                                    .listName = "Tipo de Precio",
                                                                                    .listAgencyName = "PE:SUNAT",
                                                                                    .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo16"}}}}

            ItemDoc.TaxTotal = New TaxTotalType() {New TaxTotalType() With {
                         .TaxAmount = Format(ProvisionItem.Item_IGV_Total, "#0.00"),
                         .TaxSubtotal = New TaxSubtotalType() {New TaxSubtotalType With {
                                .TaxableAmount = Format(ProvisionItem.Item_ValorVenta_Total, "#0.00"),
                                .TaxAmount = Format(ProvisionItem.Item_IGV_Total, "#0.00"),
                                .TaxCategory = New TaxCategoryType() With {
                                .Percent = Format(ProvisionItem.Unitario_IGV_Porcentaje, "#0.00"),
                                .TaxExemptionReasonCode = New CodeType With {.Value = ProvisionItem.Item_Tipo_Afectacion_IGV_Codigo,
                                                                             .listName = "Afectacion del IGV",
                                                                             .listAgencyName = "PE:SUNAT",
                                                                             .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo07"},
                                .TaxScheme = New TaxSchemeType With {.ID = New IdentifierType With {.Value = "1000",
                                                                                                    .schemeName = "Codigo de tributos",
                                                                                                    .schemeAgencyName = "PE:SUNAT",
                                                                                                    .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                                                       .Name = "IGV",
                                                                       .TaxTypeCode = "VAT"}
                            }
                        }}
                    }}
            ItemDoc.Item = New ItemType() With {
                         .Description = New TextType() {ProvisionItem.Producto_Descripcion}
                    }
            If Not String.IsNullOrEmpty(ProvisionItem.Producto_Codigo) Then
                ItemDoc.Item.SellersItemIdentification = New ItemIdentificationType With {.ID = ProvisionItem.Producto_Codigo}
            End If
            ItemDoc.Price = New PriceType() With {
                         .PriceAmount = ProvisionItem.Unitario_Valor_Unitario
                    }
            ListaDetalles.Add(ItemDoc)
        Next

        Dim TotalMonedas As New MonetaryTotalType
        TotalMonedas.PayableAmount = New AmountType With { _
            .Value = Format((Provision.Total_Importe_Venta - Provision.Total_Descuentos + Provision.Total_OtrosCargos), "##0.00")}
        'If Provision.Total_Descuentos > 0 Then
        '    TotalMonedas.AllowanceTotalAmount = Format(Provision.Total_Descuentos, "##0.00")
        'End If

        If Provision.Total_OtrosCargos > 0 Then
            TotalMonedas.ChargeTotalAmount = New AmountType With {.Value = Format(Provision.Total_OtrosCargos, "##0.00")}
        End If
        If Provision.Total_Anticipos > 0 Then
            TotalMonedas.PrepaidAmount = New AmountType With {.Value = Format(Provision.Total_Anticipos, "##0.00")}
        End If


        'Creando los documentos afectados con tipo de afectacion
        Dim DiscrepancyList As New List(Of ResponseType)
        'Creando los documentos modifica
        Dim BillingReferenceList As New List(Of BillingReferenceType)
        For Each ItemRel In Provision.DocumentoAfectados
            Dim IDiscrep As New ResponseType
            'IDiscrep.ReferenceID = ItemRel.Documento_Serie & "-" & ItemRel.Documento_Numero
            IDiscrep.ResponseCode = New CodeType With {.Value = ItemRel.Documento_Ref_Tipo,
                                                       .listAgencyName = "PE:SUNAT",
                                                       .listName = "Tipo de nota de debito",
                                                       .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo10"}
            IDiscrep.Description = New TextType() {New TextType With {.Value = ItemRel.Documento_Ref_Observaciones}}
            DiscrepancyList.Add(IDiscrep)
            Dim IBillRef As New BillingReferenceType
            IBillRef.InvoiceDocumentReference = New DocumentReferenceType With {.ID = ItemRel.Documento_Serie & "-" & ItemRel.Documento_Numero,
                                                                                .DocumentTypeCode = New CodeType With {.Value = ItemRel.Documento_Tipo,
                                                                                                                       .listAgencyName = "PE:SUNAT",
                                                                                                                       .listName = "Tipo de Documento",
                                                                                                                       .listURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo01"}}
            BillingReferenceList.Add(IBillRef)
        Next

        'Creando cabecera
        DebitNote = New UblLarsen.Ubl2.DebitNoteType() With { _
            .ID = Provision.TipoDocumento_Serie & "-" & Provision.TipoDocumento_Numero, _
            .IssueDate = Provision.Fecha_Emision, _
            .DocumentCurrencyCode = New CurrencyCodeType() With {.Value = Provision.Moneda_Codigo}, _
            .DiscrepancyResponse = DiscrepancyList.ToArray, _
            .BillingReference = BillingReferenceList.ToArray, _
            .Signature = New SignatureType() {New SignatureType() With {.ID = Provision.Emisor_Documento_Numero, _
                        .SignatoryParty = New PartyType() With {.PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType() With {.ID = Provision.Emisor_Documento_Numero}},
                                                                .PartyName = New PartyNameType() {New PartyNameType() With {.Name = Provision.Emisor_ApellidosNombres_RazonSocial}}}, _
                        .DigitalSignatureAttachment = New AttachmentType() With {.ExternalReference = New ExternalReferenceType() With {.URI = "SIGEDGO"}}}}, _
             .AccountingSupplierParty = New SupplierPartyType() With { _
                 .Party = New PartyType With {.PartyIdentification = New PartyIdentificationType() _
                                             {New PartyIdentificationType With {.ID = New IdentifierType _
                                                                               With {.Value = Provision.Emisor_Documento_Numero, _
                                                                                     .schemeID = "6", _
                                                                                     .schemeName = "Documento de Identidad", _
                                                                                     .schemeAgencyName = "PE:SUNAT", _
                                                                                     .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"}}},
                                              .PartyName = New PartyNameType() {New PartyNameType With {.Name = Provision.Emisor_ApellidosNombres_RazonSocial}},
                                              .PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType With {.RegistrationName = Provision.Emisor_ApellidosNombres_RazonSocial,
                                                                                                                             .RegistrationAddress = New AddressType With {.AddressTypeCode = _
                                                                                                                                 New CodeType With {.Value = "0000",
                                                                                                                                                    .listAgencyName = "PE:SUNAT",
                                                                                                                                                    .listName = "Establecimientos anexos"}}}}}},
             .AccountingCustomerParty = New CustomerPartyType() With { _
                 .Party = New PartyType With { _
                     .PartyIdentification = New PartyIdentificationType() _
                                             {New PartyIdentificationType With {.ID = New IdentifierType _
                                                                               With {.Value = Provision.Cliente_Documento_Numero, _
                                                                                     .schemeID = Provision.Cliente_Documento_Tipo, _
                                                                                     .schemeName = "Documento de Identidad", _
                                                                                     .schemeAgencyName = "PE:SUNAT", _
                                                                                     .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"}}},
                     .PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType With _
                                                                     {.RegistrationName = Provision.Cliente_RazonSocial_Nombre}}}}, _
             .TaxTotal = New TaxTotalType() {New TaxTotalType With { _
                 .TaxAmount = Format(Provision.Total_IGV, "##0.00"), _
                 .TaxSubtotal = New TaxSubtotalType() {New TaxSubtotalType() With { _
                        .TaxableAmount = Format(Provision.Total_ValorVenta_OperacionesGravadas, "##0.00"), _
                        .TaxAmount = Format(Provision.Total_IGV, "##0.00"), _
                        .TaxCategory = New TaxCategoryType With { _
                            .TaxScheme = New TaxSchemeType With { _
                                .ID = New IdentifierType With {.Value = "1000",
                                                                .schemeName = "Codigo de tributos",
                                                                .schemeAgencyName = "PE:SUNAT",
                                                                .schemeURI = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05"},
                                .Name = "IGV",
                                .TaxTypeCode = "VAT"
                            } _
                    } _
                }} _
            }}, _
             .RequestedMonetaryTotal = TotalMonedas, _
            .DebitNoteLine = ListaDetalles.ToArray
        }

        'Agregando los extension: Firma del documento y campos personalizados
        DebitNote.UBLExtensions = New UBLExtensionType() {New UBLExtensionType() With {.ExtensionContent = New ExtensionContentType()}}
        'Agregando namespace
        Dim ns As XmlSerializerNamespaces = New XmlSerializerNamespaces()
        'ns.Add("ccts", "urn:un:unece:uncefact:documentation:2")
        ns.Add("ds", "http://www.w3.org/2000/09/xmldsig#")
        ns.Add("sac", "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")
        'ns.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance")
        'ns.Add("schemaLocation", "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 ..\xsd\maindoc\UBLPE-Invoice-2.0.xsd")

        Dim settings As New XmlWriterSettings With {.ConformanceLevel = ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.GetEncoding("ISO-8859-1")}
        Using writer As XmlWriter = XmlWriter.Create(Provision.RootPath_Enterprise & "\xml_temp\" & xmlFilename, settings)
            Dim typeToSerialize As Type = GetType(UblLarsen.Ubl2.DebitNoteType)
            Dim xs As New XmlSerializer(typeToSerialize)
            xs.Serialize(writer, DebitNote, ns)
        End Using

        ProcesoCompleto.ExtensionContent_Indice = 0

        ProcesoCompleto.FileNameWithOutExtension = Filename
        ProcesoCompleto.ArchivoCreadoCorrectamente = True

        Return ProcesoCompleto
    End Function

    Function Generar_GuiaRemisionRemitente(Provision As DocumentoElectronico) As fcProceso
        Dim ProcesoCompleto As New fcProceso With {.ArchivoCreadoCorrectamente = False}
        ProcesoCompleto.ServidorRutaLocal = Provision.RootPath_Enterprise
        'ProcesoCompleto.Etapa = Provision.CertificadoCredencial.Etapa_Codigo
        ProcesoCompleto.DocumentoTipo = Provision.TipoDocumento_Codigo

        Dim Filename As String = Provision.Emisor_Documento_Numero & "-" & Provision.TipoDocumento_Codigo & "-" & Provision.TipoDocumento_Serie & "-" & Provision.TipoDocumento_Numero
        Dim xmlFilename As String = Filename & ".XML"
        Dim ZipFilename As String = Filename & ".ZIP"

        Provision.Doc_SUNAT_NombreArchivoXML = xmlFilename

        Dim DespatchAdvice As UblLarsen.Ubl2.DespatchAdviceType
        UblLarsen.Ubl2.UblBaseDocumentType.GlbCustomizationID = "1.0"
        UblLarsen.Ubl2.UblBaseDocumentType.GlbUblVersionID = "2.1"
        AmountType.TlsDefaultCurrencyID = Provision.Moneda_Codigo

        'Creando los detalles
        Dim ListaDetalles As New List(Of DespatchLineType)
        If Not IsNothing(Provision.Detalles) Then
            For Each ProvisionItem In Provision.Detalles
                Dim ItemDoc As New DespatchLineType
                ItemDoc.ID = ProvisionItem.Item
                ItemDoc.OrderLineReference = New OrderLineReferenceType() {New OrderLineReferenceType() With {.LineID = ProvisionItem.Item}} ' ProvisionItem.Item
                ItemDoc.DeliveredQuantity = New QuantityType() With {
                             .unitCode = ProvisionItem.Producto_UnidadMedida_Codigo,
                             .Value = Format(ProvisionItem.Producto_Cantidad, "#0.000")
                        }
                ItemDoc.Item = New ItemType() With {.Name = ProvisionItem.Producto_Descripcion, .SellersItemIdentification = New ItemIdentificationType With {.ID = ProvisionItem.Producto_Codigo}}
                ListaDetalles.Add(ItemDoc)
            Next
        End If

        Dim Tercero_Establecimiento As SupplierPartyType
        If Not String.IsNullOrEmpty(Provision.Tercero_Documento_Tipo) Then
            Tercero_Establecimiento = New SupplierPartyType With {.CustomerAssignedAccountID = New IdentifierType() With {.schemeID = Provision.Tercero_Documento_Tipo, .Value = Provision.Tercero_Documento_Numero},
                .Party = New PartyType() With {.PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType() With {.RegistrationName = New NameType With {.Value = Provision.Tercero_RazonSocial_Nombre}}}}}
        End If


        Dim ListaTransportes As New TransportHandlingUnitType
        If Provision.Transportes.Count > 0 Then
            Dim listEquip As New List(Of TransportEquipmentType)
            For i = 0 To Provision.Transportes.Count - 1
                'If i = 0 Then
                'ListaTransportes.ID = Provision.Transportes(0).Placa
                'Else
                Dim EquipTransp As New TransportEquipmentType()
                EquipTransp.ID = Provision.Transportes(i).Placa
                listEquip.Add(EquipTransp)
                'End If
            Next
            ListaTransportes.TransportEquipment = listEquip.ToArray
        End If

        Dim Contenedor As List(Of TransportHandlingUnitType)
        If Not String.IsNullOrEmpty(Provision.Contenedor_Numero) Then
            Contenedor = New List(Of TransportHandlingUnitType)
            'Dim transpHand As New TransportHandlingUnitType() With {.TransportEquipment = New TransportEquipmentType() {New TransportEquipmentType With {.ID = Provision.Contenedor_Numero}}}
            Dim transpHand As New TransportHandlingUnitType() With {.ID = Provision.Contenedor_Numero}
            Contenedor.Add(transpHand)
        End If

        '---Transportista
        Dim Transportista As PartyType()
        If Not String.IsNullOrEmpty(Provision.Transportista_Documento_Tipo) And Not String.IsNullOrEmpty(Provision.Transportista_Documento_Numero) Then
            Transportista = New PartyType() {New PartyType With {.PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType With {.ID = New IdentifierType With {.schemeID = Provision.Transportista_Documento_Tipo, .Value = Provision.Transportista_Documento_Numero}}}, .PartyName = New PartyNameType() {New PartyNameType() With {.Name = Provision.Transportista_RazonSocial_Nombre}}}}
        End If
        '---Conductor
        Dim Conductor As PartyIdentificationType
        If Not String.IsNullOrEmpty(Provision.Conductor_Documento_Tipo) And Not String.IsNullOrEmpty(Provision.Conductor_Documento_Numero) Then
            Conductor = New PartyIdentificationType With {.ID = New IdentifierType With {.schemeID = Provision.Conductor_Documento_Tipo, .Value = Provision.Conductor_Documento_Numero}}
        End If

        '---Codigo de Aeropuerto/Puerto
        Dim PuertoAeroPuerto As LocationType1
        If Not String.IsNullOrEmpty(Provision.PuertoAeropuerto_EmbarqueDesembarque_Codigo) Then
            PuertoAeroPuerto = New LocationType1() With {.ID = Provision.PuertoAeropuerto_EmbarqueDesembarque_Codigo}
        End If
        'Creando cabecera

        DespatchAdvice = New UblLarsen.Ubl2.DespatchAdviceType() With {
          .ID = Provision.TipoDocumento_Serie & "-" & Provision.TipoDocumento_Numero,
           .IssueDate = Provision.Fecha_Emision,
           .DespatchAdviceTypeCode = Provision.TipoDocumento_Codigo,
          .Note = New TextType() {New TextType With {.Value = Provision.Observaciones}},
          .Signature = New SignatureType() {New SignatureType() With {.ID = Provision.Emisor_Documento_Numero,
                        .SignatoryParty = New PartyType() With {.PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType() With {.ID = Provision.Emisor_Documento_Numero}},
                                                                .PartyName = New PartyNameType() {New PartyNameType() With {.Name = Provision.Emisor_ApellidosNombres_RazonSocial}}},
                        .DigitalSignatureAttachment = New AttachmentType() With {.ExternalReference = New ExternalReferenceType() With {.URI = "SIGEDGO"}}}},
           .AdditionalDocumentReference = New DocumentReferenceType() {},
           .DespatchSupplierParty = New SupplierPartyType() With {.CustomerAssignedAccountID = New IdentifierType() With {.schemeID = Provision.Emisor_Documento_Tipo, .Value = Provision.Emisor_Documento_Numero},
              .Party = New PartyType() With {.PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType With {.RegistrationName = Provision.Emisor_ApellidosNombres_RazonSocial}}}},
           .DeliveryCustomerParty = New CustomerPartyType With {.CustomerAssignedAccountID = New IdentifierType() With {.schemeID = Provision.Cliente_Documento_Tipo, .Value = Provision.Cliente_Documento_Numero},
              .Party = New PartyType() With {.PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType() With {.RegistrationName = Provision.Cliente_RazonSocial_Nombre}}}},
          .SellerSupplierParty = Tercero_Establecimiento,
           .Shipment = New ShipmentType() With {.ID = "1", .HandlingCode = Provision.MotivoTraslad_Codigo, .GrossWeightMeasure = New MeasureType With {.unitCode = Provision.PesoBrutoTotalGuia_UnidMed, .Value = Provision.PesoBrutoTotalGuia_Valor}, .Information = New TextType() With {.Value = Provision.MotivoTraslad_Detalle},
                                                .SplitConsignmentIndicator = Provision.IndicTransProg_Codigo,
               .ShipmentStage = New ShipmentStageType() {New ShipmentStageType With {
                  .TransportModeCode = Provision.ModalidadTraslado_Codigo,
                  .TransitPeriod = New PeriodType() With {.StartDate = Provision.Fecha_Inicio_Traslado_o_Entrega_Bienes_Al_Transportista},
                  .CarrierParty = Transportista,
                  .TransportMeans = New TransportMeansType() With {.RoadTransport = New RoadTransportType() With {.LicensePlateID = Provision.Transportes(0).Placa}},
        .DriverPerson = Conductor
                   }},
                                                .TotalTransportHandlingUnitQuantity = Provision.NumeroBultosoPallets,
              .TransportHandlingUnit = Contenedor.ToArray,
              .FirstArrivalPortLocation = PuertoAeroPuerto,
              .Delivery = New DeliveryType With {.DeliveryAddress = New AddressType() With {.ID = Provision.Llegada_Ubigeo, .StreetName = Provision.Llegada_Direccion}},
        .OriginAddress = New AddressType With {.ID = Provision.Salida_Ubigeo, .StreetName = Provision.Salida_Direccion}
          },
        .DespatchLine = ListaDetalles.ToArray
      }

        DespatchAdvice.UBLExtensions = New UBLExtensionType() {New UBLExtensionType() With {.ExtensionContent = New ExtensionContentType()}}


        'Agregando namespace
        Dim ns As XmlSerializerNamespaces = New XmlSerializerNamespaces()
        ns.Add("ds", "http://www.w3.org/2000/09/xmldsig#")
        ns.Add("sac", "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")

        ns.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance")
        ns.Add("xsd", "http://www.w3.org/2001/XMLSchema")

        Dim settings As New XmlWriterSettings With {.ConformanceLevel = ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.GetEncoding("ISO-8859-1")}
        Using writer As XmlWriter = XmlWriter.Create(Provision.RootPath_Enterprise & "\xml_temp\" & xmlFilename, settings)
            Dim typeToSerialize As Type = GetType(UblLarsen.Ubl2.DespatchAdviceType)
            Dim xs As New XmlSerializer(typeToSerialize)
            xs.Serialize(writer, DespatchAdvice, ns)
        End Using

        ProcesoCompleto.ExtensionContent_Indice = 0

        '-remuevo los namespace q no sirven
        Dim docx As New XmlDocument
        docx.Load(Provision.RootPath_Enterprise & "\xml_temp\" & xmlFilename)
        Dim rootx As XmlElement = docx.DocumentElement
        rootx.RemoveAttributeAt(4)
        rootx.RemoveAttributeAt(5)
        docx.Save(Provision.RootPath_Enterprise & "\xml_temp\" & xmlFilename)

        'Select Only
        'xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:sac="urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2" xmlns="urn:oasis:names:specification:ubl:schema:xsd:DespatchAdvice-2"

        ProcesoCompleto.FileNameWithOutExtension = Filename
        ProcesoCompleto.ArchivoCreadoCorrectamente = True

        Return ProcesoCompleto
    End Function
    Function Generar_ComprobanteRetencion(Provision As DocumentoElectronico) As fcProceso
        Dim ProcesoCompleto As New fcProceso With {.ArchivoCreadoCorrectamente = False}
        ProcesoCompleto.ServidorRutaLocal = Provision.RootPath_Enterprise
        ProcesoCompleto.DocumentoTipo = Provision.TipoDocumento_Codigo

        Dim Filename As String = Provision.Emisor_Documento_Numero & "-" & Provision.TipoDocumento_Codigo & "-" & Provision.TipoDocumento_Serie & "-" & Provision.TipoDocumento_Numero
        Dim xmlFilename As String = Filename & ".XML"
        Dim ZipFilename As String = Filename & ".ZIP"

        Provision.Doc_SUNAT_NombreArchivoXML = xmlFilename

        Dim Retention As UblLarsen.Ubl2.RetentionType
        UblLarsen.Ubl2.UblBaseDocumentType.GlbCustomizationID = "1.0"
        AmountType.TlsDefaultCurrencyID = Provision.Moneda_Codigo

        'Creando los documentos referenciados
        'Dim ListaDetalles As RetentionDocumentReferenceType() = New RetentionDocumentReferenceType(Provision.DocumentoAfectados.Count - 1) {}
        'For i = 0 To Provision.DocumentoAfectados.Count - 1
        '    If Provision.Moneda_Codigo = Provision.Moneda_Codigo Then
        '        ListaDetalles(i) = New RetentionDocumentReferenceType() With {.ID = New IdentifierType() With {.schemeID = Provision(i).Ref_Documento_St, .Value = Provision(i).Ref_Documento_Serie & "-" & Provision(i).Ref_Documento_Numero}, .IssueDate = Provision(i).Ref_Documento_FechaEmision, .TotalInvoiceAmount = New AmountType() With {.Value = Provision(i).Ref_Documento_ImporteTotal, .currencyID = Provision(i).Ref_Documento_MonedaInternacional}, _
        '                                                                              .Payment = New PaymentType() With {.ID = Provision(i).Ref_Pag_Codigo, .PaidAmount = Provision(i).Ref_Pag_ImporteSinRetencion, .PaidDate = Provision(i).Ref_Pag_Fecha}, _
        '                                                                              .SUNATRetentionInformation = New RetentionDocumentReferenceInformationType() With {.SUNATRetentionAmount = Provision(i).Ref_Ret_ImporteRetenido, .SUNATRetentionDate = Provision(i).Dvt_Fecha_Emision, .SUNATNetTotalPaid = Provision(i).Ref_Ret_ImporteTotalInclRetencion}}
        '    Else
        '        ListaDetalles(i) = New RetentionDocumentReferenceType() With {.ID = New IdentifierType() With {.schemeID = Provision(i).Ref_Documento_St, .Value = Provision(i).Ref_Documento_Serie & "-" & Provision(i).Ref_Documento_Numero}, .IssueDate = Provision(i).Ref_Documento_FechaEmision, .TotalInvoiceAmount = New AmountType() With {.Value = Provision(i).Ref_Documento_ImporteTotal, .currencyID = Provision(i).Ref_Documento_MonedaInternacional}, _
        '                                                              .Payment = New PaymentType() With {.ID = Provision(i).Ref_Pag_Codigo, .PaidAmount = Provision(i).Ref_Pag_ImporteSinRetencion, .PaidDate = Provision(i).Ref_Pag_Fecha}, _
        '                                                              .SUNATRetentionInformation = New RetentionDocumentReferenceInformationType() With {.SUNATRetentionAmount = Provision(i).Ref_Ret_ImporteRetenido, .SUNATRetentionDate = Provision(i).Dvt_Fecha_Emision, .SUNATNetTotalPaid = Provision(i).Ref_Ret_ImporteTotalInclRetencion, _
        '                                                                                                                                .ExchangeRate = New ExchangeRateType() With {.SourceCurrencyCode = Provision(i).Ref_Documento_MonedaInternacional, .TargetCurrencyCode = Provision(i).Dvt_Moneda_Internacional, .CalculationRate = Provision(i).Ref_Ret_TC_Aplicado, .Date = Provision(i).Dvt_Fecha_Emision}}}
        '    End If
        'Next
        Dim ListaDetalles As New List(Of RetentionDocumentReferenceType)
        For Each Doc In Provision.DocumentoRelacionados
            Dim Item As New RetentionDocumentReferenceType
            Item.ID = New IdentifierType() With {.schemeID = Doc.Documento_Tipo, .Value = Doc.Documento_Serie & "-" & Doc.Documento_Numero}
            Item.IssueDate = Doc.Documento_Fecha_Emision
            Item.TotalInvoiceAmount = New AmountType() With {.Value = Doc.Documento_Importe_Total, .currencyID = Doc.Documento_Moneda_Codigo}

            Dim TipoDeCambio As ExchangeRateType
            If Provision.Moneda_Codigo <> Doc.Documento_Moneda_Codigo Then
                TipoDeCambio = New ExchangeRateType With {.SourceCurrencyCode = Doc.Documento_Moneda_Codigo, .TargetCurrencyCode = Provision.Moneda_Codigo, .CalculationRate = Doc.Documento_TipoCambio_Valor, .Date = Doc.Documento_TipoCambio_Fecha}
            End If

            Item.Payment = New PaymentType() With {.ID = Doc.Documento_PagoCobro_Correlativo, .PaidAmount = New AmountType With {.Value = Doc.Documento_PagoCobro_SinRetencionPercepcion, .currencyID = Doc.Documento_PagoCobro_Moneda_Codigo}, .PaidDate = Doc.Documento_PagoCobro_Fecha}
            Item.SUNATRetentionInformation = New RetentionDocumentReferenceInformationType() With {.SUNATRetentionAmount = Doc.Documento_Importe_RetenidoPercibido, .SUNATRetentionDate = Doc.Documento_Fecha_RetencionPercepcion, .SUNATNetTotalPaid = Doc.Documento_Total_Pagar_Incluye_RetencionPercepcion, _
                                                                                                                                        .ExchangeRate = TipoDeCambio}
            ListaDetalles.Add(Item)
        Next

        'Creando cabecera
        '-- AgentParty : EMISOR
        '-- ReceiverParty : RECEPTOR
        Retention = New UblLarsen.Ubl2.RetentionType() With { _
    .ID = Provision.TipoDocumento_Serie & "-" & Provision.TipoDocumento_Numero, _
     .IssueDate = Provision.Fecha_Emision, _
    .Signature = New SignatureType() {New SignatureType() With {.ID = Provision.Emisor_Documento_Numero, _
                .SignatoryParty = New PartyType() With {.PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType() With {.ID = Provision.Emisor_Documento_Numero}},
                                                        .PartyName = New PartyNameType() {New PartyNameType() With {.Name = Provision.Emisor_ApellidosNombres_RazonSocial}}}, _
                .DigitalSignatureAttachment = New AttachmentType() With {.ExternalReference = New ExternalReferenceType() With {.URI = "SIGEDGO"}}}}, _
    .AgentParty = New PartyType() With {.PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType With {.ID = New IdentifierType() With {.Value = Provision.Emisor_Documento_Numero, .schemeID = "6"}}}, .PartyName = New PartyNameType() {New PartyNameType With {.Name = Provision.Emisor_ApellidosNombres_RazonSocial}}, _
                                             .PostalAddress = New AddressType() With {
                         .ID = Provision.Emisor_Direccion_Ubigeo,
                         .StreetName = Provision.Emisor_Direccion_Calle,
                         .CitySubdivisionName = Provision.Emisor_Direccion_Urbanizacion,
                         .CityName = Provision.Emisor_Direccion_Provincia,
                         .CountrySubentity = Provision.Emisor_Direccion_Departamento,
                         .District = Provision.Emisor_Direccion_Distrito,
                         .Country = New CountryType() With {
                             .IdentificationCode = Provision.Emisor_Direccion_PaisCodigo
                        }
                    }, _
                       .PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType() With {.RegistrationName = Provision.Emisor_ApellidosNombres_RazonSocial}} _
    }, _
    .ReceiverParty = New PartyType() With {.PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType With {.ID = New IdentifierType() With {.Value = Provision.Cliente_Documento_Numero, .schemeID = Provision.Cliente_Documento_Tipo}}}, .PartyName = New PartyNameType() {New PartyNameType With {.Name = Provision.Cliente_RazonSocial_Nombre}}, _
                       .PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType() With {.RegistrationName = Provision.Cliente_RazonSocial_Nombre}} _
    }, _
    .SUNATRetentionSystemCode = Provision.Retenc_Tipo_Codigo, _
    .SUNATRetentionPercent = Provision.Retenc_Porcentaje, _
    .Note = Provision.Observaciones, _
    .TotalInvoiceAmount = Provision.Retenc_Importe_Total_Retenido, .SUNATTotalPaid = Provision.Retenc_Importe_Total_Pagado,
    .SUNATRetentionDocumentReference = ListaDetalles.ToArray
    }

        'Dim InformacionAdicional As Add

        'Agregando los extension: Firma del documento y campos personalizados
        Retention.UBLExtensions = New UBLExtensionType() {New UBLExtensionType() With {.ExtensionContent = New ExtensionContentType()}}

        'Agregando namespace
        Dim ns As XmlSerializerNamespaces = New XmlSerializerNamespaces()
        ns.Add("ccts", "urn:un:unece:uncefact:documentation:2")
        ns.Add("ds", "http://www.w3.org/2000/09/xmldsig#")
        ns.Add("xsd", "http://www.w3.org/2001/XMLSchema")
        ns.Add("sac", "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")
        ns.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance")
        'ns.Add("schemaLocation", "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 ..\xsd\maindoc\UBLPE-Invoice-2.0.xsd")

        Dim settings As New XmlWriterSettings With {.ConformanceLevel = ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.GetEncoding("ISO-8859-1")}
        Using writer As XmlWriter = XmlWriter.Create(Provision.RootPath_Enterprise & "\xml_temp\" & xmlFilename, settings)
            Dim typeToSerialize As Type = GetType(UblLarsen.Ubl2.RetentionType)
            Dim xs As New XmlSerializer(typeToSerialize)
            xs.Serialize(writer, Retention, ns)
        End Using

        ProcesoCompleto.ExtensionContent_Indice = 1

        ProcesoCompleto.FileNameWithOutExtension = Filename
        ProcesoCompleto.ArchivoCreadoCorrectamente = True

        Return ProcesoCompleto
    End Function
    Function Generar_ComprobantePercepcion(Provision As DocumentoElectronico) As fcProceso
        Dim ProcesoCompleto As New fcProceso With {.ArchivoCreadoCorrectamente = False}
        ProcesoCompleto.ServidorRutaLocal = Provision.RootPath_Enterprise
        ProcesoCompleto.DocumentoTipo = Provision.TipoDocumento_Codigo

        Dim Filename As String = Provision.Emisor_Documento_Numero & "-" & Provision.TipoDocumento_Codigo & "-" & Provision.TipoDocumento_Serie & "-" & Provision.TipoDocumento_Numero
        Dim xmlFilename As String = Filename & ".XML"
        Dim ZipFilename As String = Filename & ".ZIP"

        Provision.Doc_SUNAT_NombreArchivoXML = xmlFilename

        Dim Retention As UblLarsen.Ubl2.PerceptionType
        UblLarsen.Ubl2.UblBaseDocumentType.GlbCustomizationID = "1.0"
        AmountType.TlsDefaultCurrencyID = Provision.Moneda_Codigo

        Dim ListaDetalles As New List(Of PerceptionDocumentReferenceType)
        For Each Doc In Provision.DocumentoRelacionados
            Dim Item As New PerceptionDocumentReferenceType
            Item.ID = New IdentifierType() With {.schemeID = Doc.Documento_Tipo, .Value = Doc.Documento_Serie & "-" & Doc.Documento_Numero}
            Item.IssueDate = Doc.Documento_Fecha_Emision
            Item.TotalInvoiceAmount = New AmountType() With {.Value = Doc.Documento_Importe_Total, .currencyID = Doc.Documento_Moneda_Codigo}

            Dim TipoDeCambio As ExchangeRateType
            If Provision.Moneda_Codigo <> Doc.Documento_Moneda_Codigo Then
                TipoDeCambio = New ExchangeRateType With {.SourceCurrencyCode = Doc.Documento_Moneda_Codigo, .TargetCurrencyCode = Provision.Moneda_Codigo, .CalculationRate = Doc.Documento_TipoCambio_Valor, .Date = Doc.Documento_TipoCambio_Fecha}
            End If

            Item.Payment = New PaymentType() With {.ID = Doc.Documento_PagoCobro_Correlativo, .PaidAmount = New AmountType With {.Value = Doc.Documento_PagoCobro_SinRetencionPercepcion, .currencyID = Doc.Documento_PagoCobro_Moneda_Codigo}, .PaidDate = Doc.Documento_PagoCobro_Fecha}
            Item.SUNATPerceptionInformation = New PerceptionDocumentReferenceInformationType() With {.SUNATPerceptionAmount = Doc.Documento_Importe_RetenidoPercibido, .SUNATPerceptionDate = Doc.Documento_Fecha_RetencionPercepcion, .SUNATNetTotalCashed = Doc.Documento_Total_Pagar_Incluye_RetencionPercepcion, _
                                                                                                                                        .ExchangeRate = TipoDeCambio}
            ListaDetalles.Add(Item)
        Next

        'Creando cabecera
        Retention = New UblLarsen.Ubl2.PerceptionType() With { _
    .ID = Provision.TipoDocumento_Serie & "-" & Provision.TipoDocumento_Numero, _
     .IssueDate = Provision.Fecha_Emision, _
    .Signature = New SignatureType() {New SignatureType() With {.ID = Provision.Emisor_Documento_Numero, _
                .SignatoryParty = New PartyType() With {.PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType() With {.ID = Provision.Emisor_Documento_Numero}},
                                                        .PartyName = New PartyNameType() {New PartyNameType() With {.Name = Provision.Emisor_ApellidosNombres_RazonSocial}}}, _
                .DigitalSignatureAttachment = New AttachmentType() With {.ExternalReference = New ExternalReferenceType() With {.URI = "SIGEDGO"}}}}, _
    .AgentParty = New PartyType() With {.PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType With {.ID = Provision.Emisor_Documento_Numero}}, .PartyName = New PartyNameType() {New PartyNameType With {.Name = Provision.Emisor_ApellidosNombres_RazonSocial}}, _
                                             .PostalAddress = New AddressType() With {
                         .ID = Provision.Emisor_Direccion_Ubigeo,
                         .StreetName = Provision.Emisor_Direccion_Calle,
                         .CitySubdivisionName = Provision.Emisor_Direccion_Urbanizacion,
                         .CityName = Provision.Emisor_Direccion_Provincia,
                         .CountrySubentity = Provision.Emisor_Direccion_Departamento,
                         .District = Provision.Emisor_Direccion_Distrito,
                         .Country = New CountryType() With {
                             .IdentificationCode = Provision.Emisor_Direccion_PaisCodigo
                        }
                    }, _
                       .PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType() With {.RegistrationName = Provision.Emisor_ApellidosNombres_RazonSocial}} _
    }, _
    .ReceiverParty = New PartyType() With {.PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType With {.ID = Provision.Emisor_Documento_Numero}}, .PartyName = New PartyNameType() {New PartyNameType With {.Name = Provision.Emisor_ApellidosNombres_RazonSocial}}, _
                                             .PostalAddress = New AddressType() With { _
                         .ID = Provision.Emisor_Direccion_Ubigeo, _
                         .StreetName = Provision.Emisor_Direccion_Calle, _
                         .CitySubdivisionName = Provision.Emisor_Direccion_Urbanizacion, _
                         .CityName = Provision.Emisor_Direccion_Departamento, _
                         .CountrySubentity = Provision.Emisor_Direccion_Provincia, _
                         .District = Provision.Emisor_Direccion_Distrito, _
                         .Country = New CountryType() With { _
                             .IdentificationCode = Provision.Emisor_Direccion_PaisCodigo _
                        }}, _
                       .PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType() With {.RegistrationName = Provision.Emisor_ApellidosNombres_RazonSocial}} _
    }, _
    .SUNATPerceptionSystemCode = Provision.Percep_Tipo_Codigo, _
    .SUNATPerceptionPercent = Provision.Percep_Porcentaje, _
    .Note = Provision.Observaciones, _
    .TotalInvoiceAmount = Provision.Percep_Importe_Total_Percibido, .SUNATTotalCashed = Provision.Percep_Importe_Total_Cobrado,
    .SUNATPerceptionDocumentReference = ListaDetalles.ToArray
    }

        'Agregando los extension: Firma del documento y campos personalizados
        Retention.UBLExtensions = New UBLExtensionType() {New UBLExtensionType() With {.ExtensionContent = New ExtensionContentType()}}

        'Agregando namespace
        Dim ns As XmlSerializerNamespaces = New XmlSerializerNamespaces()
        ns.Add("ccts", "urn:un:unece:uncefact:documentation:2")
        ns.Add("ds", "http://www.w3.org/2000/09/xmldsig#")
        ns.Add("xsd", "http://www.w3.org/2001/XMLSchema")
        ns.Add("sac", "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")
        ns.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance")
        'ns.Add("schemaLocation", "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 ..\xsd\maindoc\UBLPE-Invoice-2.0.xsd")

        Dim settings As New XmlWriterSettings With {.ConformanceLevel = ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.GetEncoding("ISO-8859-1")}
        Using writer As XmlWriter = XmlWriter.Create(Provision.RootPath_Enterprise & "\xml_temp\" & xmlFilename, settings)
            Dim typeToSerialize As Type = GetType(UblLarsen.Ubl2.PerceptionType)
            Dim xs As New XmlSerializer(typeToSerialize)
            xs.Serialize(writer, Retention, ns)
        End Using

        ProcesoCompleto.ExtensionContent_Indice = 1

        ProcesoCompleto.FileNameWithOutExtension = Filename
        ProcesoCompleto.ArchivoCreadoCorrectamente = True

        Return ProcesoCompleto
    End Function
    Sub XMLFirmar(Provision As DocumentoElectronico, Credenciales As eCertificadoCredencial, pProceso As fcProceso)
        Try
            ' Vamos a firmar el XML con la ruta del certificado que está como serializado.

            Dim certificate = New X509Certificate2()
            certificate.Import(Credenciales.Ruta, Credenciales.Contrasena, X509KeyStorageFlags.MachineKeySet)

            Dim xmlDoc = New XmlDocument()
            xmlDoc.PreserveWhitespace = True
            xmlDoc.Load(Provision.RootPath_Enterprise & "\xml_temp\" & pProceso.FileNameXML)

            Dim nodoExtension = xmlDoc.GetElementsByTagName("ExtensionContent", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2").Item(pProceso.ExtensionContent_Indice)
            'Dim nodoExtension = xmlDoc.GetElementsByTagName("ExtensionContent", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2").Item(pProceso.ExtensionContent_Indice) '1 - 2 nodos, 0 un solo nodo
            If nodoExtension Is Nothing Then
                Throw New InvalidOperationException("No se pudo encontrar el nodo ExtensionContent en el XML")
            End If
            nodoExtension.RemoveAll()

            Dim signedXml = New SignedXml(xmlDoc) With {
            .SigningKey = DirectCast(certificate.PrivateKey, System.Security.Cryptography.RSA)
        }
            signedXml.SigningKey = certificate.PrivateKey
            Dim xmlSignature = signedXml.Signature

            Dim env = New XmlDsigEnvelopedSignatureTransform()

            Dim reference = New Reference(String.Empty)
            reference.AddTransform(env)
            xmlSignature.SignedInfo.AddReference(reference)

            Dim keyInfo = New KeyInfo()
            Dim x509Data = New KeyInfoX509Data(certificate)

            x509Data.AddSubjectName(certificate.Subject)
            keyInfo.AddClause(x509Data)
            xmlSignature.KeyInfo = keyInfo
            xmlSignature.Id = "SIGEDGO"

            signedXml.ComputeSignature()

            If reference.DigestValue IsNot Nothing Then
                Provision.Print_DigestValue = Convert.ToBase64String(reference.DigestValue)
            End If

            Provision.Print_SignatureValue = Convert.ToBase64String(signedXml.SignatureValue)

            nodoExtension.AppendChild(signedXml.GetXml()).Prefix = "ds"

            xmlDoc.Save(Provision.RootPath_Enterprise & "\xml_temp\" & pProceso.FileNameXML)

            pProceso.ArchivoFirmadoCorrectamente = True
        Catch ex As Exception
            pProceso.ArchivoFirmadoCorrectamente = False
        End Try


    End Sub
    Sub XMLZip(pProceso As fcProceso)
        Try
            If IO.File.Exists(pProceso.ServidorRutaLocal & "\xml_temp\" & pProceso.FileNameZIP) Then
                IO.File.Delete(pProceso.ServidorRutaLocal & "\xml_temp\" & pProceso.FileNameZIP)
            End If

            Using archive As ZipArchive = ZipFile.Open(pProceso.ServidorRutaLocal & "\xml_temp\" & pProceso.FileNameZIP, ZipArchiveMode.Update)
                ZipFileExtensions.CreateEntryFromFile(archive, (pProceso.ServidorRutaLocal & "\xml_temp\" & pProceso.FileNameXML), pProceso.FileNameXML, CompressionLevel.Fastest)
            End Using
            pProceso.ArchivoComprimidoCorrectamente = True
        Catch ex As Exception
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "XMLZIP-SUB", ex.Message)
            pProceso.ArchivoComprimidoCorrectamente = False
        End Try
    End Sub
    Sub XMLCopiarGenerado(nProceso As fcProceso)
        Try
            'Creamos el directorio en caso no exista, en caso sea un nuevo documento
            If Not IO.Directory.Exists(nProceso.ServidorRuta_Generado) Then
                IO.Directory.CreateDirectory(nProceso.ServidorRuta_Generado)
            End If
            IO.File.Copy(nProceso.ServidorRuta_Temporal & nProceso.FileNameXML, nProceso.ServidorRuta_Generado & nProceso.FileNameXML, True)
            IO.File.Copy(nProceso.ServidorRuta_Temporal & nProceso.FileNameZIP, nProceso.ServidorRuta_Generado & nProceso.FileNameZIP, True)
            'IO.File.Copy(nProceso.ServidorRuta_Temporal & nProceso.FileNamePDF, nProceso.ServidorRuta_Generado & nProceso.FileNamePDF, True)
            nProceso.GeneradosGuardadoServidorCorrectamente = True
        Catch ex As Exception
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "XMLCOPIAR-GENERADO", ex.Message)
            nProceso.GeneradosGuardadoServidorCorrectamente = False
        End Try
    End Sub
    Sub XMLCopiarAceptado(nProceso As fcProceso)
        Try
            'Creamos el directorio en caso no exista
            If Not IO.Directory.Exists(nProceso.ServidorRuta_Aceptado) Then
                IO.Directory.CreateDirectory(nProceso.ServidorRuta_Aceptado)
            End If
            IO.File.Copy(nProceso.ServidorRuta_Generado & nProceso.FileNameXML, nProceso.ServidorRuta_Aceptado & nProceso.FileNameXML, True)
            IO.File.Copy(nProceso.ServidorRuta_Generado & nProceso.FileNameZIP, nProceso.ServidorRuta_Aceptado & nProceso.FileNameZIP, True)
            If IO.File.Exists(nProceso.ServidorRuta_Generado & nProceso.FileNamePDF) Then
                IO.File.Copy(nProceso.ServidorRuta_Generado & nProceso.FileNamePDF, nProceso.ServidorRuta_Aceptado & nProceso.FileNamePDF, True)
            End If
            If IO.File.Exists(nProceso.ServidorRuta_Temporal & nProceso.CDRNameZIP) Then
                IO.File.Copy(nProceso.ServidorRuta_Temporal & nProceso.CDRNameZIP, nProceso.ServidorRuta_Aceptado & nProceso.CDRNameZIP, True)
            End If

            'Copiamos en el servidor si no tenemos FTP
            If String.IsNullOrEmpty(nProceso.FTP_Carpeta) Then
                IO.File.Copy(nProceso.ServidorRuta_Generado & nProceso.FileNameXML, nProceso.ServidorRutaServidor & nProceso.FileNameXML, True)
                If IO.File.Exists(nProceso.ServidorRuta_Generado & nProceso.FileNamePDF) Then
                    IO.File.Copy(nProceso.ServidorRuta_Generado & nProceso.FileNamePDF, nProceso.ServidorRutaServidor & nProceso.FileNamePDF, True)
                End If
                If IO.File.Exists(nProceso.ServidorRuta_Temporal & nProceso.CDRNameZIP) Then
                    IO.File.Copy(nProceso.ServidorRuta_Temporal & nProceso.CDRNameZIP, nProceso.ServidorRutaServidor & nProceso.CDRNameZIP, True)
                End If
            End If
            'copiamos al FPT
            'FALTA
            nProceso.GeneradosGuardadoServidorCorrectamente = True
        Catch ex As Exception
            'log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "XMLCOPIAR-ACEP-FACTBOL", ex.Message)
            nProceso.GeneradosGuardadoServidorCorrectamente = False
        End Try
    End Sub
    Sub XMLCopiarRechazado(nProceso As fcProceso)
        Try
            'Creamos el directorio en caso no exista
            If Not IO.Directory.Exists(nProceso.ServidorRuta_Rechazado) Then
                IO.Directory.CreateDirectory(nProceso.ServidorRuta_Rechazado)
            End If
            IO.File.Copy(nProceso.FileNameXML, nProceso.ServidorRuta_Rechazado & nProceso.FileNameXML, True)
            IO.File.Copy(nProceso.FileNameZIP, nProceso.ServidorRuta_Rechazado & nProceso.FileNameZIP, True)
            If IO.File.Exists(nProceso.FileNamePDF) Then
                IO.File.Copy(nProceso.FileNamePDF, nProceso.ServidorRuta_Rechazado & nProceso.FileNamePDF, True)
            End If
            If IO.File.Exists(nProceso.CDRNameZIP) Then
                IO.File.Copy(nProceso.CDRNameZIP, nProceso.ServidorRuta_Rechazado & nProceso.CDRNameZIP, True)
            End If
            nProceso.GeneradosGuardadoServidorCorrectamente = True
        Catch ex As Exception
            'log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "XMLCOPIAR-RECHAZ-FACTBOL", ex.Message)
            nProceso.GeneradosGuardadoServidorCorrectamente = False
        End Try
    End Sub
    Sub LimpiarDirectorio(nProceso As fcProceso)
        Try
            IO.File.Delete(nProceso.ServidorRuta_Temporal & nProceso.FileNameXML)
        Catch ex As Exception
            'log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CLEARDIREC", ex.Message)
        End Try
        Try
            IO.File.Delete(nProceso.ServidorRuta_Temporal & nProceso.FileNameZIP)
        Catch ex As Exception
            'log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CLEARDIREC", ex.Message)
        End Try
        Try
            IO.File.Delete(nProceso.ServidorRuta_Temporal & nProceso.FileNamePDF)
        Catch ex As Exception
            'log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CLEARDIREC", ex.Message)
        End Try
        Try
            IO.File.Delete(nProceso.ServidorRuta_Temporal & nProceso.CDRNameZIP)
        Catch ex As Exception
            'log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CLEARDIREC", ex.Message)
        End Try
        Try
            IO.File.Delete(nProceso.ServidorRuta_Temporal & nProceso.CDRNameXML)
        Catch ex As Exception
            'log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CLEARDIREC", ex.Message)
        End Try
    End Sub
#End Region
    'Sub GenerarPDF(Provision As DocumentoElectronico, nProceso As fcProceso)
    '    Try
    '        Dim Lista As New List(Of DocumentoElectronico)
    '        Lista.Add(Provision)

    '        If String.IsNullOrEmpty(Provision.Impresion_Modelo_Codigo) Then
    '            Provision.Impresion_Modelo_Codigo = "000"
    '        End If
    '        If Provision.Impresion_Modelo_Codigo = "000" Then
    '            Dim Rpte As New Print_Invoice_POS
    '            'Para que se autoajuste al tamaño del papel
    '            Rpte.PrintingSystem.Document.AutoFitToPagesWidth = 1
    '            'Rpte.RollPaper = True '16.1
    '            Rpte.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.HundredthsOfAnInch
    '            Rpte.PageWidth = 315 '315 (~ 80 mm)
    '            '   En el reporte en el desing
    '            'Me.BindingSource1.DataSource = GetType(Ortiz.DocumentoElectronico)

    '            'Codigo barras PDF417
    '            Rpte.XrBarCode1.BinaryData = System.Text.Encoding.UTF8.GetBytes(Provision.Print_BarCode)

    '            Rpte.XrLabel28.Text = Now.ToLongTimeString

    '            'Notas adicionales
    '            Dim NotasAdicionales As String = ""
    '            If Not IsNothing(Provision.Leyendas) Then
    '                If Provision.Leyendas.Count > 1 Then
    '                    For i = 1 To Provision.Leyendas.Count - 1
    '                        NotasAdicionales = NotasAdicionales & Provision.Leyendas(0).Descripcion
    '                    Next
    '                End If
    '            End If

    '            ''Fecha vencimiento
    '            'If Provision.Fecha_Emision = Provision.Fecha_Vencimiento Or Provision.Fecha_Vencimiento.Year = 1 Then
    '            '    'Contado - No mostramos nada
    '            '    Rpte.XrLabel16.Visible = False
    '            '    Rpte.XrLabel43.Visible = False
    '            'Else
    '            '    NotasAdicionales = NotasAdicionales & "- EL PAGO DE ESTA FACTURA DESPUES DE LA FECHA DE VENCIMIENTO PODRIA GENERAR INTERESES COMPENSATORIOS Y MORATORIOS A LAS TASAS MAXIMAS PERMITIDAS POR LEY." & vbNewLine & vbNewLine
    '            'End If

    '            'Rpte.XrRichText1.Text = NotasAdicionales

    '            Rpte.DataSource = Lista

    '            Rpte.CreateDocument()
    '            Rpte.ExportToPdf(nProceso.ServidorRuta_Temporal & nProceso.FileNamePDF)

    '        ElseIf Provision.Impresion_Modelo_Codigo = "001" Then
    '            Dim Rpte As New PrintInvoice
    '            'Para que se autoajuste al tamaño del papel
    '            Rpte.PrintingSystem.Document.AutoFitToPagesWidth = 1
    '            'Rpte.RollPaper = True '16.1
    '            Rpte.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.HundredthsOfAnInch
    '            Rpte.PageWidth = 315 '315 (~ 80 mm)
    '            '   En el reporte en el desing
    '            'Me.BindingSource1.DataSource = GetType(Ortiz.DocumentoElectronico)

    '            Dim NotasAdicionales As String = ""
    '            If Not IsNothing(Provision.Leyendas) Then
    '                If Provision.Leyendas.Count > 1 Then
    '                    For i = 1 To Provision.Leyendas.Count - 1
    '                        NotasAdicionales = NotasAdicionales & Provision.Leyendas(0).Descripcion
    '                    Next
    '                End If
    '            End If


    '            'Fecha vencimiento
    '            If Provision.Fecha_Emision = Provision.Fecha_Vencimiento Or Provision.Fecha_Vencimiento.Year = 1 Then
    '                'Contado - No mostramos nada
    '                Rpte.XrLabel16.Visible = False
    '                Rpte.XrLabel43.Visible = False
    '            Else
    '                NotasAdicionales = NotasAdicionales & "- EL PAGO DE ESTA FACTURA DESPUES DE LA FECHA DE VENCIMIENTO PODRIA GENERAR INTERESES COMPENSATORIOS Y MORATORIOS A LAS TASAS MAXIMAS PERMITIDAS POR LEY." & vbNewLine & vbNewLine
    '            End If

    '            'Codigo barras PDF417
    '            Rpte.XrBarCode1.BinaryData = System.Text.Encoding.UTF8.GetBytes(Provision.Print_BarCode)

    '            Rpte.XrLabel8.Text = Now.ToShortTimeString
    '            Rpte.XrLabel28.Text = Now.ToShortDateString

    '            'Notas adicionales
    '            Rpte.XrRichText1.Text = NotasAdicionales

    '            Rpte.DataSource = Lista

    '            Rpte.CreateDocument()
    '            Rpte.ExportToPdf(nProceso.ServidorRuta_Temporal & nProceso.FileNamePDF)
    '        ElseIf Provision.Impresion_Modelo_Codigo = "002" Then 'Nota de Crédito / Nota de Débito

    '        ElseIf Provision.Impresion_Modelo_Codigo = "003" Then 'Guia de Remisión 'Tipo Ticket

    '        ElseIf Provision.Impresion_Modelo_Codigo = "004" Then 'Guia de Remisión

    '        End If
    '    Catch ex As Exception
    '        log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CREATEPDF", ex.Message)
    '    End Try
    'End Sub
End Class