﻿'------------------------------------------------------------------------------
' <auto-generated>
'     Este código fue generado por una herramienta.
'     Versión de runtime:4.0.30319.42000
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Namespace ws_sunat
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ServiceModel.ServiceContractAttribute([Namespace]:="http://service.sunat.gob.pe", ConfigurationName:="ws_sunat.billService")>  _
    Public Interface billService
        
        'CODEGEN: El parámetro 'status' requiere información adicional de esquema que no se puede capturar con el modo de parámetros. El atributo específico es 'System.Xml.Serialization.XmlElementAttribute'.
        <System.ServiceModel.OperationContractAttribute(Action:="urn:getStatus", ReplyAction:="*"),  _
         System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults:=true)>  _
        Function getStatus(ByVal request As ws_sunat.getStatusRequest) As <System.ServiceModel.MessageParameterAttribute(Name:="status")> ws_sunat.getStatusResponse
        
        <System.ServiceModel.OperationContractAttribute(Action:="urn:getStatus", ReplyAction:="*")>  _
        Function getStatusAsync(ByVal request As ws_sunat.getStatusRequest) As System.Threading.Tasks.Task(Of ws_sunat.getStatusResponse)
        
        'CODEGEN: El parámetro 'applicationResponse' requiere información adicional de esquema que no se puede capturar con el modo de parámetros. El atributo específico es 'System.Xml.Serialization.XmlElementAttribute'.
        <System.ServiceModel.OperationContractAttribute(Action:="urn:sendBill", ReplyAction:="*"),  _
         System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults:=true)>  _
        Function sendBill(ByVal request As ws_sunat.sendBillRequest) As <System.ServiceModel.MessageParameterAttribute(Name:="applicationResponse")> ws_sunat.sendBillResponse
        
        <System.ServiceModel.OperationContractAttribute(Action:="urn:sendBill", ReplyAction:="*")>  _
        Function sendBillAsync(ByVal request As ws_sunat.sendBillRequest) As System.Threading.Tasks.Task(Of ws_sunat.sendBillResponse)
        
        'CODEGEN: El parámetro 'ticket' requiere información adicional de esquema que no se puede capturar con el modo de parámetros. El atributo específico es 'System.Xml.Serialization.XmlElementAttribute'.
        <System.ServiceModel.OperationContractAttribute(Action:="urn:sendPack", ReplyAction:="*"),  _
         System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults:=true)>  _
        Function sendPack(ByVal request As ws_sunat.sendPackRequest) As <System.ServiceModel.MessageParameterAttribute(Name:="ticket")> ws_sunat.sendPackResponse
        
        <System.ServiceModel.OperationContractAttribute(Action:="urn:sendPack", ReplyAction:="*")>  _
        Function sendPackAsync(ByVal request As ws_sunat.sendPackRequest) As System.Threading.Tasks.Task(Of ws_sunat.sendPackResponse)
        
        'CODEGEN: El parámetro 'ticket' requiere información adicional de esquema que no se puede capturar con el modo de parámetros. El atributo específico es 'System.Xml.Serialization.XmlElementAttribute'.
        <System.ServiceModel.OperationContractAttribute(Action:="urn:sendSummary", ReplyAction:="*"),  _
         System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults:=true)>  _
        Function sendSummary(ByVal request As ws_sunat.sendSummaryRequest) As <System.ServiceModel.MessageParameterAttribute(Name:="ticket")> ws_sunat.sendSummaryResponse
        
        <System.ServiceModel.OperationContractAttribute(Action:="urn:sendSummary", ReplyAction:="*")>  _
        Function sendSummaryAsync(ByVal request As ws_sunat.sendSummaryRequest) As System.Threading.Tasks.Task(Of ws_sunat.sendSummaryResponse)
    End Interface
    
    '''<comentarios/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1590.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://service.sunat.gob.pe")>  _
    Partial Public Class statusResponse
        Inherits Object
        Implements System.ComponentModel.INotifyPropertyChanged
        
        Private contentField() As Byte
        
        Private statusCodeField As String
        
        '''<comentarios/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType:="base64Binary", Order:=0)>  _
        Public Property content() As Byte()
            Get
                Return Me.contentField
            End Get
            Set
                Me.contentField = value
                Me.RaisePropertyChanged("content")
            End Set
        End Property
        
        '''<comentarios/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, Order:=1)>  _
        Public Property statusCode() As String
            Get
                Return Me.statusCodeField
            End Get
            Set
                Me.statusCodeField = value
                Me.RaisePropertyChanged("statusCode")
            End Set
        End Property
        
        Public Event PropertyChanged As System.ComponentModel.PropertyChangedEventHandler Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged
        
        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As System.ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(WrapperName:="getStatus", WrapperNamespace:="http://service.sunat.gob.pe", IsWrapped:=true)>  _
    Partial Public Class getStatusRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=0),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public ticket As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal ticket As String)
            MyBase.New
            Me.ticket = ticket
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(WrapperName:="getStatusResponse", WrapperNamespace:="http://service.sunat.gob.pe", IsWrapped:=true)>  _
    Partial Public Class getStatusResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=0),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public status As ws_sunat.statusResponse
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal status As ws_sunat.statusResponse)
            MyBase.New
            Me.status = status
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(WrapperName:="sendBill", WrapperNamespace:="http://service.sunat.gob.pe", IsWrapped:=true)>  _
    Partial Public Class sendBillRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=0),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public fileName As String
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=1),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType:="base64Binary")>  _
        Public contentFile() As Byte
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=2),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public partyType As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal fileName As String, ByVal contentFile() As Byte, ByVal partyType As String)
            MyBase.New
            Me.fileName = fileName
            Me.contentFile = contentFile
            Me.partyType = partyType
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(WrapperName:="sendBillResponse", WrapperNamespace:="http://service.sunat.gob.pe", IsWrapped:=true)>  _
    Partial Public Class sendBillResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=0),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType:="base64Binary")>  _
        Public applicationResponse() As Byte
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal applicationResponse() As Byte)
            MyBase.New
            Me.applicationResponse = applicationResponse
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(WrapperName:="sendPack", WrapperNamespace:="http://service.sunat.gob.pe", IsWrapped:=true)>  _
    Partial Public Class sendPackRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=0),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public fileName As String
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=1),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType:="base64Binary")>  _
        Public contentFile() As Byte
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=2),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public partyType As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal fileName As String, ByVal contentFile() As Byte, ByVal partyType As String)
            MyBase.New
            Me.fileName = fileName
            Me.contentFile = contentFile
            Me.partyType = partyType
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(WrapperName:="sendPackResponse", WrapperNamespace:="http://service.sunat.gob.pe", IsWrapped:=true)>  _
    Partial Public Class sendPackResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=0),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public ticket As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal ticket As String)
            MyBase.New
            Me.ticket = ticket
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(WrapperName:="sendSummary", WrapperNamespace:="http://service.sunat.gob.pe", IsWrapped:=true)>  _
    Partial Public Class sendSummaryRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=0),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public fileName As String
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=1),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType:="base64Binary")>  _
        Public contentFile() As Byte
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=2),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public partyType As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal fileName As String, ByVal contentFile() As Byte, ByVal partyType As String)
            MyBase.New
            Me.fileName = fileName
            Me.contentFile = contentFile
            Me.partyType = partyType
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(WrapperName:="sendSummaryResponse", WrapperNamespace:="http://service.sunat.gob.pe", IsWrapped:=true)>  _
    Partial Public Class sendSummaryResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=0),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public ticket As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal ticket As String)
            MyBase.New
            Me.ticket = ticket
        End Sub
    End Class
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")>  _
    Public Interface billServiceChannel
        Inherits ws_sunat.billService, System.ServiceModel.IClientChannel
    End Interface
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")>  _
    Partial Public Class billServiceClient
        Inherits System.ServiceModel.ClientBase(Of ws_sunat.billService)
        Implements ws_sunat.billService
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String)
            MyBase.New(endpointConfigurationName)
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As String)
            MyBase.New(endpointConfigurationName, remoteAddress)
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
            MyBase.New(endpointConfigurationName, remoteAddress)
        End Sub
        
        Public Sub New(ByVal binding As System.ServiceModel.Channels.Binding, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
            MyBase.New(binding, remoteAddress)
        End Sub
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function ws_sunat_billService_getStatus(ByVal request As ws_sunat.getStatusRequest) As ws_sunat.getStatusResponse Implements ws_sunat.billService.getStatus
            Return MyBase.Channel.getStatus(request)
        End Function
        
        Public Function getStatus(ByVal ticket As String) As ws_sunat.statusResponse
            Dim inValue As ws_sunat.getStatusRequest = New ws_sunat.getStatusRequest()
            inValue.ticket = ticket
            Dim retVal As ws_sunat.getStatusResponse = CType(Me,ws_sunat.billService).getStatus(inValue)
            Return retVal.status
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function ws_sunat_billService_getStatusAsync(ByVal request As ws_sunat.getStatusRequest) As System.Threading.Tasks.Task(Of ws_sunat.getStatusResponse) Implements ws_sunat.billService.getStatusAsync
            Return MyBase.Channel.getStatusAsync(request)
        End Function
        
        Public Function getStatusAsync(ByVal ticket As String) As System.Threading.Tasks.Task(Of ws_sunat.getStatusResponse)
            Dim inValue As ws_sunat.getStatusRequest = New ws_sunat.getStatusRequest()
            inValue.ticket = ticket
            Return CType(Me,ws_sunat.billService).getStatusAsync(inValue)
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function ws_sunat_billService_sendBill(ByVal request As ws_sunat.sendBillRequest) As ws_sunat.sendBillResponse Implements ws_sunat.billService.sendBill
            Return MyBase.Channel.sendBill(request)
        End Function
        
        Public Function sendBill(ByVal fileName As String, ByVal contentFile() As Byte, ByVal partyType As String) As Byte()
            Dim inValue As ws_sunat.sendBillRequest = New ws_sunat.sendBillRequest()
            inValue.fileName = fileName
            inValue.contentFile = contentFile
            inValue.partyType = partyType
            Dim retVal As ws_sunat.sendBillResponse = CType(Me,ws_sunat.billService).sendBill(inValue)
            Return retVal.applicationResponse
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function ws_sunat_billService_sendBillAsync(ByVal request As ws_sunat.sendBillRequest) As System.Threading.Tasks.Task(Of ws_sunat.sendBillResponse) Implements ws_sunat.billService.sendBillAsync
            Return MyBase.Channel.sendBillAsync(request)
        End Function
        
        Public Function sendBillAsync(ByVal fileName As String, ByVal contentFile() As Byte, ByVal partyType As String) As System.Threading.Tasks.Task(Of ws_sunat.sendBillResponse)
            Dim inValue As ws_sunat.sendBillRequest = New ws_sunat.sendBillRequest()
            inValue.fileName = fileName
            inValue.contentFile = contentFile
            inValue.partyType = partyType
            Return CType(Me,ws_sunat.billService).sendBillAsync(inValue)
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function ws_sunat_billService_sendPack(ByVal request As ws_sunat.sendPackRequest) As ws_sunat.sendPackResponse Implements ws_sunat.billService.sendPack
            Return MyBase.Channel.sendPack(request)
        End Function
        
        Public Function sendPack(ByVal fileName As String, ByVal contentFile() As Byte, ByVal partyType As String) As String
            Dim inValue As ws_sunat.sendPackRequest = New ws_sunat.sendPackRequest()
            inValue.fileName = fileName
            inValue.contentFile = contentFile
            inValue.partyType = partyType
            Dim retVal As ws_sunat.sendPackResponse = CType(Me,ws_sunat.billService).sendPack(inValue)
            Return retVal.ticket
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function ws_sunat_billService_sendPackAsync(ByVal request As ws_sunat.sendPackRequest) As System.Threading.Tasks.Task(Of ws_sunat.sendPackResponse) Implements ws_sunat.billService.sendPackAsync
            Return MyBase.Channel.sendPackAsync(request)
        End Function
        
        Public Function sendPackAsync(ByVal fileName As String, ByVal contentFile() As Byte, ByVal partyType As String) As System.Threading.Tasks.Task(Of ws_sunat.sendPackResponse)
            Dim inValue As ws_sunat.sendPackRequest = New ws_sunat.sendPackRequest()
            inValue.fileName = fileName
            inValue.contentFile = contentFile
            inValue.partyType = partyType
            Return CType(Me,ws_sunat.billService).sendPackAsync(inValue)
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function ws_sunat_billService_sendSummary(ByVal request As ws_sunat.sendSummaryRequest) As ws_sunat.sendSummaryResponse Implements ws_sunat.billService.sendSummary
            Return MyBase.Channel.sendSummary(request)
        End Function
        
        Public Function sendSummary(ByVal fileName As String, ByVal contentFile() As Byte, ByVal partyType As String) As String
            Dim inValue As ws_sunat.sendSummaryRequest = New ws_sunat.sendSummaryRequest()
            inValue.fileName = fileName
            inValue.contentFile = contentFile
            inValue.partyType = partyType
            Dim retVal As ws_sunat.sendSummaryResponse = CType(Me,ws_sunat.billService).sendSummary(inValue)
            Return retVal.ticket
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function ws_sunat_billService_sendSummaryAsync(ByVal request As ws_sunat.sendSummaryRequest) As System.Threading.Tasks.Task(Of ws_sunat.sendSummaryResponse) Implements ws_sunat.billService.sendSummaryAsync
            Return MyBase.Channel.sendSummaryAsync(request)
        End Function
        
        Public Function sendSummaryAsync(ByVal fileName As String, ByVal contentFile() As Byte, ByVal partyType As String) As System.Threading.Tasks.Task(Of ws_sunat.sendSummaryResponse)
            Dim inValue As ws_sunat.sendSummaryRequest = New ws_sunat.sendSummaryRequest()
            inValue.fileName = fileName
            inValue.contentFile = contentFile
            inValue.partyType = partyType
            Return CType(Me,ws_sunat.billService).sendSummaryAsync(inValue)
        End Function
    End Class
End Namespace
