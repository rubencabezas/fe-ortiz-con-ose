﻿Imports System.ServiceModel
Imports System.ServiceModel.Security
Imports System.ServiceModel.Channels
Imports System.IO
Imports System.Xml
Imports DevExpress.XtraPrinting.Preview
Imports DevExpress.XtraReports.UI
Imports System.Drawing

Public Class HerramientasFE
    Public Shared Function SUNAT_Bill(nProceso As fcProceso) As eSunatRespuesta
        Dim RptaSUNAT As New eSunatRespuesta

        Dim CDR_Name_Zip As String = nProceso.FileNameWithOutExtension & "-CDR.ZIP"
        Dim CDR_Name_Xml As String = "R-" & nProceso.FileNameWithOutExtension & ".XML"

        nProceso.CDRNameZIP = CDR_Name_Zip
        nProceso.CDRNameXML = CDR_Name_Xml

        Dim DataByteArray As Byte() = System.IO.File.ReadAllBytes(nProceso.ServidorRuta_Generado & nProceso.FileNameZIP)

        System.Net.ServicePointManager.UseNagleAlgorithm = True
        System.Net.ServicePointManager.Expect100Continue = False
        System.Net.ServicePointManager.CheckCertificateRevocationList = True

        Dim binding As New ServiceModel.BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential)
        binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None
        binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None
        binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName
        binding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Default
        Dim remoteAddress As New EndpointAddress(nProceso.SUNAT_Servicio_URL)
        Dim client As New ws_sunat.billServiceClient(binding, remoteAddress)

        If nProceso.Etapa = "001" Then
            client.ClientCredentials.UserName.UserName = nProceso.Emisor_RUC & "MODDATOS"
            client.ClientCredentials.UserName.Password = "moddatos"
        Else
            client.ClientCredentials.UserName.UserName = nProceso.Emisor_RUC & nProceso.SUNAT_Usuario.Trim
            client.ClientCredentials.UserName.Password = nProceso.SUNAT_Contrasena.Trim
        End If

        Dim bindingElementsInTopDownChannelStackOrder As BindingElementCollection = client.Endpoint.Binding.CreateBindingElements
        bindingElementsInTopDownChannelStackOrder.Find(Of SecurityBindingElement).EnableUnsecuredResponse = True
        client.Endpoint.Binding = New CustomBinding(bindingElementsInTopDownChannelStackOrder)

        'System.Net.ServicePointManager.ServerCertificateValidationCallback = New System.Net.Security.RemoteCertificateValidationCallback(Function() True)

        Dim RetornoCDR_Byte As Byte()

        Try
            RetornoCDR_Byte = client.sendBill(nProceso.FileNameZIP, DataByteArray,"")
        Catch exception As FaultException
            Dim str10 As String = exception.Code.Name.ToUpper.Replace("SOAP-ENV-SERVER.", "").Replace("SOAP-ENV-SERVER", "").Replace("CLIENT.", "")
            Dim str11 As String = ("FaultException Validación: " & exception.Message) '2335
            RptaSUNAT.Codigo = exception.Message ' str10
            RptaSUNAT.Mensaje = str11
            RptaSUNAT.Estado = EstadoDocumento.Rechazado

            'Log.Instancia.escribirLinea(Log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL", RptaSUNAT.Codigo & "|" & RptaSUNAT.Mensaje)

            'Return RptaSUNAT
        Catch exception3 As Exception
            RptaSUNAT.Codigo = "-1"
            RptaSUNAT.Mensaje = "El archivo XML ha sido generado, pero aún no ha sido válidado con la SUNAT debido a que no hay conexión con la URL: https://www.sunat.gob.pe:443/ol-ti-itcpgem-beta/billService"

            'Log.Instancia.escribirLinea(Log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL", RptaSUNAT.Codigo & "|" & RptaSUNAT.Mensaje)

            Return RptaSUNAT
        End Try

        If (Not RetornoCDR_Byte Is Nothing) Then
            Try
                File.WriteAllBytes(nProceso.ServidorRuta_Temporal & CDR_Name_Zip, RetornoCDR_Byte)
                If File.Exists((nProceso.ServidorRuta_Temporal & CDR_Name_Xml)) Then
                    File.Delete((nProceso.ServidorRuta_Temporal & CDR_Name_Xml))
                End If
                Compression.ZipFile.ExtractToDirectory(nProceso.ServidorRuta_Temporal & CDR_Name_Zip, nProceso.ServidorRuta_Temporal)
                Dim document As New XmlDocument
                document.Load((nProceso.ServidorRuta_Temporal & CDR_Name_Xml))
                Dim childNodes As XmlNodeList = document.GetElementsByTagName("DocumentResponse", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2").Item(0).FirstChild.ChildNodes
                RptaSUNAT.Codigo = childNodes.ItemOf(1).InnerText
                RptaSUNAT.Mensaje = childNodes.ItemOf(2).InnerText
            Catch ex As Exception
                'Sospecho disco lleno
                'Log.Instancia.escribirLinea(Log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL-COMP", ex.Message)
            End Try

        End If

        If (Convert.ToInt32(RptaSUNAT.Codigo) = 0) Then
            RptaSUNAT.Estado = EstadoDocumento.Aceptado
        ElseIf (Convert.ToInt32(RptaSUNAT.Codigo) = -1) Then
            RptaSUNAT.Estado = EstadoDocumento.Invalido
        Else
            RptaSUNAT.Estado = EstadoDocumento.Rechazado
        End If
        Return RptaSUNAT
    End Function
    Public Shared Function EnviarCorreo(Provision As DocumentoElectronico, nProceso As fcProceso) As Boolean
        If Not String.IsNullOrEmpty(Provision.Cliente_Correo) Then
            '-------------------------------------------------------ENVIAR CORREO
            Using mailMsg As New System.Net.Mail.MailMessage()
                Using smtp As New System.Net.Mail.SmtpClient
                    Dim Anexos As New ArrayList()
                    smtp.DeliveryMethod = Net.Mail.SmtpDeliveryMethod.Network
                    smtp.Host = nProceso.Correo_Servidor_SMTP 'servidor de correo
                    smtp.Port = nProceso.Correo_Servidor_Puerto

                    If nProceso.Correo_Servidor_SSL Then
                        smtp.EnableSsl = True
                    End If

                    smtp.Credentials = New System.Net.NetworkCredential(nProceso.Correo_Usuario, nProceso.Correo_Contrasena)

                    Dim Correo_Destinatario As String = Provision.Cliente_Correo 'direccion de correo a la que se le enviará el mail

                    Dim BodyHtml As String = "<div style='width:100%;font-size:13px;font-family:arial,helvetica,sans-serif'><div style='max-width:500px;margin:auto;border:4px solid #cdced6;background:#fafafa;color:#444444'><div style='border:3px solid #97979e'><div style='margin:15px'>"
                    BodyHtml = BodyHtml & "<div style='padding:15px 0;text-align:center;color:#444444'>" & Provision.Emisor_ApellidosNombres_RazonSocial & " te envió una " & Provision.TipoDocumento_Descripcion & "</div>"
                    BodyHtml = BodyHtml & "<h1 style='padding:5px 0;text-align:center;margin:0;color:#444444'>" & Provision.TipoDocumento_Descripcion & "</h1>"
                    BodyHtml = BodyHtml & "<h1 style='padding:5px 0;text-align:center;margin:0;color:#444444'>No. " & Provision.TipoDocumento_Serie & " - " & Provision.TipoDocumento_Numero & "</h1>"
                    BodyHtml = BodyHtml & "<div style='padding:5px 0;text-align:center;color:#444444'>Fecha de emisión:" & Provision.Fecha_Emision.ToShortDateString & "</div>"
                    BodyHtml = BodyHtml & "<h1 style='padding:5px 0;text-align:center;font-size:50px;font-weight:bold;margin:10px 0;color:#444444'>" & Provision.Moneda_Simbolo & " " & Format(Provision.Total_Importe_Venta, "#,###.00") & "</h1>"

                    If Not String.IsNullOrEmpty(Provision.Observaciones) Then
                        BodyHtml = BodyHtml & "<div style='padding:5px 0;text-align:center;color:#444444'>" & Provision.Observaciones & "</div>"
                    End If

                    BodyHtml = BodyHtml & "<div style='padding:5px 0;margin:10px 0'><div style='margin:5px 0'><a href='http://efacturacion.grupoortiz.pe:8078/Documento/" & Provision.PostIDActionModel & "' style='display:block;padding:15px;background:#160064;color:#ffffff;text-align:center;text-decoration:none' target='_blank'>VER DOCUMENTO</a></div></div>"
                    BodyHtml = BodyHtml & "<div style='padding:15px 0 5px;text-align:center;color:#444444'>Si el link no funciona, usa el siguiente enlace en tu navegador: <a style='color:#444444' href='http://efacturacion.grupoortiz.pe:8078/Documento/" & Provision.PostIDActionModel & "' target='_blank'>" & "http://efacturacion.grupoortiz.pe:8078/Documento/" & Provision.PostIDActionModel & "</a></div>"
                    BodyHtml = BodyHtml & "<div style='padding:5px 0;text-align:center;color:#444444'>También se adjunta en PDF el mismo documento. Que puede ser impresa y usada como una Factura emitida de manera tradicional.</div>"
                    BodyHtml = BodyHtml & "</div></div></div></div>"
                    BodyHtml = BodyHtml & "<div><img src='http://efacturacion.grupoortiz.pe:8078/Documento/GetLogoURL_CheckReadMail?RUC_Emisor=" & Provision.Emisor_Documento_Numero & "&ID_Document=" & Provision.Doc_NombreArchivoWeb & "'/></div>"

                    Anexos.Add(nProceso.FileNameXML)
                    Anexos.Add(nProceso.FileNamePDF)
                    'Anexos.Add(nProceso.FileNameCDR)
                    With (mailMsg)
                        .From = New System.Net.Mail.MailAddress(nProceso.Correo_Usuario, Provision.Emisor_ApellidosNombres_RazonSocial, System.Text.Encoding.UTF8)
                        .To.Add(New System.Net.Mail.MailAddress(Correo_Destinatario, "Cliente", System.Text.Encoding.UTF8))
                        '.CC.Add("donny.huaman@grupoortiz.pe")
                        .Subject = Provision.Emisor_ApellidosNombres_RazonSocial & " " & Provision.TipoDocumento_Codigo & " " & Provision.TipoDocumento_Serie & " - " & Provision.Tercero_Documento_Numero
                        .SubjectEncoding = System.Text.Encoding.UTF8
                        .Body = BodyHtml
                        .BodyEncoding = System.Text.Encoding.UTF8
                        .IsBodyHtml = True
                        If (Anexos.Count > 0) Then
                            For i As Integer = 0 To Anexos.Count - 1
                                If (System.IO.File.Exists(Anexos(i))) Then
                                    .Attachments.Add(New System.Net.Mail.Attachment(Anexos(i)))
                                    'Else
                                    '    MsgBox("El archivo " + Anexos(i) + " No existe")
                                    'Exit Function
                                End If
                            Next
                        End If
                    End With
                    Try
                        smtp.Send(mailMsg)
                        nProceso.Correo_Enviado_Cliente = True
                        'MsgBox("Mensaje enviado satisfactoriamente")
                    Catch ex As Exception
                        'log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "SENDMAIL", ex.Message)
                        'MsgBox("ERROR: " & ex.Message)
                        Return False
                    End Try
                End Using
            End Using
        End If

        Return True
        '--------------efactura 6666669991011Aa
    End Function
    Public Sub GenerarPDF(Provision As DocumentoElectronico, nProceso As fcProceso)
        Try
            'Dim ws_Rpta As String = Facte.Consultar_Estado(Provision)
            'Dim datosRpta As String() = ws_Rpta.Split("#")
            'Dim DataVar As String
            'DataVar = datosRpta(0) 'Correlativo '- error, 0 documento no enviado, 1 documento registrado/actualizado
            'DataVar = datosRpta(1) 'Codigo web
            'DataVar = datosRpta(2) 'DigestValue (Código Hash)
            'DataVar = datosRpta(3) 'Valores Codigo de barras PDF
            If Provision.TipoDocumento_Codigo = "07" Or Provision.TipoDocumento_Codigo = "08" Then
                Dim RptPOS As New Print_NC_ND_GO
                If Provision.Emisor_Documento_Numero.Trim = "20571432154" Then 'CGN
                    RptPOS.XrLbEmpresa.ForeColor = Color.Navy
                    RptPOS.XrLbEmpresa.Font = New System.Drawing.Font("Arial", 16.0!, System.Drawing.FontStyle.Bold)
                    RptPOS.XrPbxLogo.Image = My.Resources.NEW_LOGO_CGN
                    RptPOS.XrPbxLogo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
                ElseIf Provision.Emisor_Documento_Numero.Trim = "20571402832" Then ' AMC
                    RptPOS.XrLbEmpresa.Font = New System.Drawing.Font("Arial", 14.0!, System.Drawing.FontStyle.Bold)
                    RptPOS.XrLbEmpresa.ForeColor = Color.YellowGreen
                    RptPOS.XrPbxLogo.Image = My.Resources.Logo_amc
                    RptPOS.XrPbxLogo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
                End If
                'Dim listRpt As New List(Of DocumentoElectronico)
                'listRpt.Add(Provision)

                'listRpt(0).Print_BarCode = datosRpta(3)
                'listRpt(0).Print_DigestValue = datosRpta(2)
                'listRpt(0).Sunat_Autorizacion = "Autorizado mediante Resolución Nº " & listRpt(0).Sunat_Autorizacion ' "018005000949/SUNAT"
                'listRpt(0).Emisor_Web_Visualizacion = "Consulte su documento en " & "www.grupoortiz.pe/facturacion"
                'listRpt(0).RepresentacionImpresaDela = "Representación Impresa de la " & listRpt(0).TipoDocumento_Descripcion

                RptPOS.DataSource = Provision
                RptPOS.PrintingSystem.Document.AutoFitToPagesWidth = 1
                RptPOS.CreateDocument()
                RptPOS.ExportToPdf((nProceso.ServidorRuta_Aceptado & nProceso.FileNamePDF))

            Else

                If String.IsNullOrEmpty(Provision.Impresion_Modelo_Codigo) Then
                    Provision.Impresion_Modelo_Codigo = "000"
                End If
                If Provision.Impresion_Modelo_Codigo = "000" Then
                    Dim RptPOS As New Print_Invoice_POS
                    Dim rptLista As New List(Of DocumentoElectronico)
                    rptLista.Add(Provision)
                    If rptLista(0).Cliente_Documento_Numero.Length <> 11 Then
                        RptPOS.XrLabel13.Text = "DNI:"
                    Else
                        RptPOS.XrLabel13.Text = "RUC:"
                    End If
                    Dim HeightDoc As Double = 775
                    HeightDoc = HeightDoc + (32 * rptLista(0).Detalles.Count)
                    'RptPOS.PrintingSystem.Document.AutoFitToPagesWidth = 1
                    'RptPOS.XrBarCode1.BinaryData = System.Text.Encoding.UTF8.GetBytes(datosRpta(3))
                    'RptPOS.XrLabel28.Text = rptLista(0).Hora_Emision.ToLongTimeString
                    'RptPOS.lblhash.Text = datosRpta(2)

                    'rptLista(0).Sunat_Autorizacion = "Autorizado mediante Resolución Nº " & rptLista(0).Sunat_Autorizacion '"018005000949/SUNAT"
                    'rptLista(0).Emisor_Web_Visualizacion = "Consulte su documento en " & "www.grupoortiz.pe/facturacion"
                    'rptLista(0).RepresentacionImpresaDela = "Representación Impresa de la " & rptLista(0).TipoDocumento_Descripcion
                    'rptLista(0).Print_DigestValue = datosRpta(2).Trim
                    If Not rptLista(0).PagoTarjeta Is Nothing Then
                        If rptLista(0).PagoTarjeta.Count > 0 Then
                            RptPOS.DetailPagoTarjeta.Visible = True
                            HeightDoc = HeightDoc + 35 + (18 * rptLista(0).PagoTarjeta.Count)
                        End If
                    End If
                    RptPOS.PageHeight = HeightDoc
                    RptPOS.DataSource = rptLista
                    RptPOS.CreateDocument()
                    RptPOS.ExportToPdf((nProceso.ServidorRuta_Aceptado & nProceso.FileNamePDF))

                ElseIf Provision.Impresion_Modelo_Codigo = "001" Then 'Estaciones
                    'Ticket

                    Dim RptPOS As New XtraReport
                    If Provision.TipoDocumento_Codigo = "01" Then
                        RptPOS = New Print_FE_ESO
                    ElseIf Provision.TipoDocumento_Codigo = "03" Then
                        RptPOS = New Print_BE_ESO
                    End If
                    RptPOS.PrintingSystem.Document.AutoFitToPagesWidth = 1

                    Dim listRpt As New List(Of DocumentoElectronico)
                    listRpt.Add(Provision)

                    'listRpt(0).Print_BarCode = datosRpta(3)
                    'listRpt(0).Print_DigestValue = datosRpta(2)
                    'listRpt(0).Sunat_Autorizacion = "Autorizado mediante Resolución Nº " & listRpt(0).Sunat_Autorizacion ' "018005000949/SUNAT"
                    'listRpt(0).Emisor_Web_Visualizacion = "Consulte su documento en " & "www.grupoortiz.pe/facturacion"
                    'listRpt(0).RepresentacionImpresaDela = "Representación Impresa de la " & listRpt(0).TipoDocumento_Descripcion

                    RptPOS.DataSource = listRpt
                    RptPOS.CreateDocument()
                    RptPOS.ExportToPdf((nProceso.ServidorRuta_Aceptado & nProceso.FileNamePDF))


                ElseIf Provision.Impresion_Modelo_Codigo = "002" Then
                    'Boleta Factura - Modelo administrativo
                    Dim RptPOS As New XtraReport
                    If Provision.TipoDocumento_Codigo = "01" Then
                        RptPOS = New Print_FE_SO
                    ElseIf Provision.TipoDocumento_Codigo = "03" Then
                        RptPOS = New Print_BE_SO
                    End If
                    RptPOS.PrintingSystem.Document.AutoFitToPagesWidth = 1


                    Dim listRpt As New List(Of DocumentoElectronico)
                    listRpt.Add(Provision)

                    'listRpt(0).Print_BarCode = datosRpta(3)
                    'listRpt(0).Print_DigestValue = datosRpta(2)
                    'listRpt(0).Sunat_Autorizacion = "Autorizado mediante Resolución Nº " & listRpt(0).Sunat_Autorizacion ' "018005000949/SUNAT"
                    'listRpt(0).Emisor_Web_Visualizacion = "Consulte su documento en " & "www.grupoortiz.pe/facturacion"
                    'listRpt(0).RepresentacionImpresaDela = "Representación Impresa de la " & listRpt(0).TipoDocumento_Descripcion

                    RptPOS.DataSource = listRpt
                    RptPOS.CreateDocument()
                    RptPOS.ExportToPdf((nProceso.ServidorRuta_Aceptado & nProceso.FileNamePDF))
                ElseIf Provision.Impresion_Modelo_Codigo = "003" Then
                    'Boleta Factura - Modelo administrativo
                    Dim RptPOS As New XtraReport
                    If Provision.TipoDocumento_Codigo = "01" Then
                        RptPOS = New Print_FE_CGN
                    ElseIf Provision.TipoDocumento_Codigo = "03" Then
                        RptPOS = New Print_BE_CGN
                    End If
                    RptPOS.PrintingSystem.Document.AutoFitToPagesWidth = 1


                    Dim listRpt As New List(Of DocumentoElectronico)
                    listRpt.Add(Provision)

                    'listRpt(0).Print_BarCode = datosRpta(3)
                    'listRpt(0).Print_DigestValue = datosRpta(2)
                    'listRpt(0).Sunat_Autorizacion = "Autorizado mediante Resolución Nº " & listRpt(0).Sunat_Autorizacion ' "018005000949/SUNAT"
                    'listRpt(0).Emisor_Web_Visualizacion = "Consulte su documento en " & "www.grupoortiz.pe/facturacion"
                    'listRpt(0).RepresentacionImpresaDela = "Representación Impresa de la " & listRpt(0).TipoDocumento_Descripcion

                    RptPOS.DataSource = listRpt
                    RptPOS.CreateDocument()
                    RptPOS.ExportToPdf((nProceso.ServidorRuta_Aceptado & nProceso.FileNamePDF))
                ElseIf Provision.Impresion_Modelo_Codigo = "004" Then
                    'Boleta Factura - Modelo administrativo
                    Dim RptPOS As New XtraReport
                    If Provision.TipoDocumento_Codigo = "01" Then
                        RptPOS = New Print_FE_AMC
                    ElseIf Provision.TipoDocumento_Codigo = "03" Then
                        RptPOS = New Print_BE_AMC
                    End If
                    RptPOS.PrintingSystem.Document.AutoFitToPagesWidth = 1


                    Dim listRpt As New List(Of DocumentoElectronico)
                    listRpt.Add(Provision)

                    'listRpt(0).Print_BarCode = datosRpta(3)
                    'listRpt(0).Print_DigestValue = datosRpta(2)
                    'listRpt(0).Sunat_Autorizacion = "Autorizado mediante Resolución Nº " & listRpt(0).Sunat_Autorizacion ' "018005000949/SUNAT"
                    'listRpt(0).Emisor_Web_Visualizacion = "Consulte su documento en " & "www.grupoortiz.pe/facturacion"
                    'listRpt(0).RepresentacionImpresaDela = "Representación Impresa de la " & listRpt(0).TipoDocumento_Descripcion

                    RptPOS.DataSource = listRpt
                    RptPOS.CreateDocument()
                    RptPOS.ExportToPdf((nProceso.ServidorRuta_Aceptado & nProceso.FileNamePDF))

                End If
            End If
            nProceso.PDF_Creado_Correctamente = True

        Catch ex As Exception
            'log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CREATEPDF", ex.Message)
        End Try
    End Sub
    Public Function InternetConnection(Optional CnLocal As Boolean = False, Optional ServidorReplica As String = "") As Boolean

        If CnLocal = False Then
            'Dim objUrl As New System.Uri("https://www.sunat.gob.pe/")  'http://www.google.com.pe/
            'Dim objWebReq As System.Net.WebRequest
            'objWebReq = System.Net.WebRequest.Create(objUrl)
            'Dim objResp As System.Net.WebResponse
            Try
                ' Attempt to get response and return True 
                'objResp = objWebReq.GetResponse
                'objResp.Close()
                'objWebReq = Nothing
                'Return True
                Return My.Computer.Network.Ping("e-factura.sunat.gob.pe", 500)
            Catch ex As Exception
                ' Error, exit and return False 
                'objWebReq = Nothing
                Return False
            End Try
            'Return True
        Else
            Dim milisegundos As Integer = 100
            Return My.Computer.Network.Ping(ServidorReplica, milisegundos)
        End If

    End Function

    Public Shared Function Consultar_Estado_Sunat(ByVal pRuc_Remitente As String, pTipo_Doc As String, ByVal pDoc_Serie As String, pDoc_Numero As Integer) As String
        Dim RptaST As String = "-#-"
        Try
            Dim LogCertificado As New Log_CERTIFICADO
            Dim CertificadoDigital As eCertificadoCredencial = LogCertificado.Buscar(pRuc_Remitente.Trim)

            Net.ServicePointManager.UseNagleAlgorithm = True
            Net.ServicePointManager.Expect100Continue = False
            Net.ServicePointManager.CheckCertificateRevocationList = True
            Dim binding As New BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential)
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None
            binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None
            binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName
            binding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Default
            Dim remoteAddress As New EndpointAddress("https://www.sunat.gob.pe/ol-it-wsconscpegem/billConsultService") 'https://www.sunat.gob.pe:443/ol-it-wsconscpegem/billConsultService
            Dim client As New ws_sunat_consulta.billServiceClient(binding, remoteAddress)
            client.ClientCredentials.UserName.UserName = pRuc_Remitente.Trim & CertificadoDigital.Sunat_Usuario
            client.ClientCredentials.UserName.Password = CertificadoDigital.Sunat_Contrasena
            Dim bindingElementsInTopDownChannelStackOrder As BindingElementCollection = client.Endpoint.Binding.CreateBindingElements
            bindingElementsInTopDownChannelStackOrder.Find(Of SecurityBindingElement).EnableUnsecuredResponse = True
            client.Endpoint.Binding = New CustomBinding(bindingElementsInTopDownChannelStackOrder)

            Try
                client.Open()
                Dim response As ws_sunat_consulta.statusResponse = client.getStatus(pRuc_Remitente.Trim, pTipo_Doc.Trim, pDoc_Serie.Trim, pDoc_Numero)
                client.Close()
                RptaST = response.statusCode & " # " & response.statusMessage
                If response.statusCode.Trim = "0011" Then
                    RptaST = Consultar_Estado_OSE(pRuc_Remitente.Trim, pTipo_Doc.Trim, pDoc_Serie.Trim, pDoc_Numero.ToString, RptaST)
                End If
            Catch exception As FaultException
                client.Open()
            End Try

        Catch exception2 As Exception

        Finally
            'If File.Exists(pathX) Then
            '    File.Delete(pathX)
            'End If
        End Try
        Return RptaST.Trim
    End Function
    Private Shared Function Consultar_Estado_OSE(ByVal pRuc_Remitente As String, pTipo_Doc As String, ByVal pDoc_Serie As String, pDoc_Numero As String, Rpta As String) As String
        Try
            Dim LogCertificado As New Log_CERTIFICADO
            Dim CertificadoDigital As eCertificadoCredencial = LogCertificado.Buscar(pRuc_Remitente.Trim)

            Net.ServicePointManager.UseNagleAlgorithm = True
            Net.ServicePointManager.Expect100Continue = False
            Net.ServicePointManager.CheckCertificateRevocationList = True
            Dim binding As New BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential)
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None
            binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None
            binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName
            binding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Default
            Dim remoteAddress As New EndpointAddress("https://ose.nubefact.com/ol-ti-itcpe/billService") 'https://www.sunat.gob.pe:443/ol-it-wsconscpegem/billConsultService
            Dim client As New ws_OSE.billServiceClient(binding, remoteAddress)
            client.ClientCredentials.UserName.UserName = pRuc_Remitente.Trim & CertificadoDigital.Sunat_Usuario
            client.ClientCredentials.UserName.Password = CertificadoDigital.Sunat_Contrasena
            Dim bindingElementsInTopDownChannelStackOrder As BindingElementCollection = client.Endpoint.Binding.CreateBindingElements
            bindingElementsInTopDownChannelStackOrder.Find(Of SecurityBindingElement).EnableUnsecuredResponse = True
            client.Endpoint.Binding = New CustomBinding(bindingElementsInTopDownChannelStackOrder)

            Try
                'client.Open()
                Dim response As ws_OSE.statusCdr = client.getStatusCdr(pRuc_Remitente.Trim, pTipo_Doc.Trim, pDoc_Serie.Trim, pDoc_Numero.ToString)
                'client.Close()
                If (Not response.content Is Nothing) Then
                    Rpta = "0001" & " # " & "El comprobante fue enviado a OSE correctamente"
                End If

            Catch exception As FaultException
                If exception.Message.Trim = "0125" Then
                    Rpta = "0011" & " # " & "El comprobante no esta informado a OSE"
                End If
            End Try

        Catch exception2 As Exception

        Finally
            'If File.Exists(pathX) Then
            '    File.Delete(pathX)
            'End If
        End Try
        Return Rpta.Trim
    End Function

    Public Shared Function EnviarCorreo(provision As eProvisionFacturacion, nProceso As fcProceso) As Boolean
        If Not String.IsNullOrEmpty(provision.Cliente_Correo_Electronico) Then
            '-------------------------------------------------------ENVIAR CORREO
            Try
                Using mailMsg As New System.Net.Mail.MailMessage()
                    Using smtp As New System.Net.Mail.SmtpClient
                        Dim Anexos As New ArrayList()
                        smtp.DeliveryMethod = Net.Mail.SmtpDeliveryMethod.Network
                        smtp.Host = provision.CertificadoCredencial.Correo_Servidor_SMTP 'servidor de correo
                        smtp.Port = provision.CertificadoCredencial.Correo_Servidor_Puerto

                        If provision.CertificadoCredencial.Correo_Servidor_SSL Then
                            smtp.EnableSsl = True
                        End If

                        smtp.Credentials = New System.Net.NetworkCredential(provision.CertificadoCredencial.Correo_Usuario, provision.CertificadoCredencial.Correo_Contrasena)

                        Dim Correo_Destinatario As String = provision.Cliente_Correo_Electronico 'direccion de correo a la que se le enviará el mail

                        Dim BodyHtml As String = "<div style='width:100%;font-size:13px;font-family:arial,helvetica,sans-serif'><div style='max-width:500px;margin:auto;border:4px solid #cdced6;background:#fafafa;color:#444444'><div style='border:3px solid #97979e'><div style='margin:15px'>"
                        BodyHtml = BodyHtml & "<div style='padding:15px 0;text-align:center;color:#444444'>" & provision.Emisor_RazonSocial_Nombre & " te envió una " & provision.TpDc_Descripcion & "</div>"
                        BodyHtml = BodyHtml & "<h1 style='padding:5px 0;text-align:center;margin:0;color:#444444'>" & provision.TpDc_Descripcion & "</h1>"
                        BodyHtml = BodyHtml & "<h1 style='padding:5px 0;text-align:center;margin:0;color:#444444'>No. " & provision.Dvt_VTSerie & " - " & provision.Dvt_VTNumer & "</h1>"
                        BodyHtml = BodyHtml & "<div style='padding:5px 0;text-align:center;color:#444444'>Fecha de emisión:" & provision.Dvt_Fecha_Emision.ToShortDateString & "</div>"
                        BodyHtml = BodyHtml & "<h1 style='padding:5px 0;text-align:center;font-size:50px;font-weight:bold;margin:10px 0;color:#444444'>" & provision.Dvt_Simbol_Moneda & " " & Format(provision.Dvt_Total_Importe, "#,##0.00") & "</h1>"

                        If Not String.IsNullOrEmpty(provision.Vnt_Observacion) Then
                            BodyHtml = BodyHtml & "<div style='padding:5px 0;text-align:center;color:#444444'>" & provision.Vnt_Observacion & "</div>"
                        End If

                        BodyHtml = BodyHtml & "<div style='padding:5px 0;margin:10px 0'><div style='margin:5px 0'><a href='http://efacturacion.grupoortiz.pe:8078/Documento/" & provision.PostIDActionModel & "' style='display:block;padding:15px;background:#160064;color:#ffffff;text-align:center;text-decoration:none' target='_blank'>VER DOCUMENTO</a></div></div>"
                        BodyHtml = BodyHtml & "<div style='padding:15px 0 5px;text-align:center;color:#444444'>Si el link no funciona, usa el siguiente enlace en tu navegador: <a style='color:#444444' href='http://efacturacion.grupoortiz.pe:8078/Documento/" & provision.PostIDActionModel & "' target='_blank'>" & "http://efacturacion.grupoortiz.pe:8078/Documento/" & provision.PostIDActionModel & "</a></div>"
                        BodyHtml = BodyHtml & "<div style='padding:5px 0;text-align:center;color:#444444'>También se adjunta en PDF el mismo documento. Que puede ser impresa y usada como una Factura emitida de manera tradicional.</div>"
                        BodyHtml = BodyHtml & "</div></div></div></div>"
                        BodyHtml = BodyHtml & "<div><img src='http://efacturacion.grupoortiz.pe:8078/Documento/GetLogoURL_CheckReadMail?RUC_Emisor=" & provision.Emisor_RUC & "&ID_Document=" & provision.Doc_NombreArchivoWeb & "'/></div>"
                        If File.Exists(nProceso.ServidorRuta_Aceptado & nProceso.FileNameXML) Then
                            Anexos.Add(nProceso.ServidorRuta_Aceptado & nProceso.FileNameXML)
                        End If
                        If File.Exists(nProceso.ServidorRuta_Aceptado & nProceso.FileNamePDF) Then
                            Anexos.Add(nProceso.ServidorRuta_Aceptado & nProceso.FileNamePDF)
                        End If
                        If File.Exists(nProceso.ServidorRuta_Aceptado & "R-" & nProceso.FileNameXML) Then
                            Anexos.Add(nProceso.ServidorRuta_Aceptado & "R-" & nProceso.FileNameXML)
                        End If

                        'Anexos.Add(nProceso.FileNameCDR)
                        With (mailMsg)
                            .From = New System.Net.Mail.MailAddress(provision.CertificadoCredencial.Correo_Usuario, provision.Emisor_RazonSocial_Nombre, System.Text.Encoding.UTF8)
                            .To.Add(New System.Net.Mail.MailAddress(Correo_Destinatario, "Cliente", System.Text.Encoding.UTF8))
                            '.CC.Add("donny.huaman@grupoortiz.pe")
                            .Subject = provision.Emisor_RazonSocial_Nombre & " " & provision.TpDc_Codigo_St & " " & provision.Dvt_VTSerie & " - " & provision.Dvt_VTNumer
                            .SubjectEncoding = System.Text.Encoding.UTF8
                            .Body = BodyHtml
                            .BodyEncoding = System.Text.Encoding.UTF8
                            .IsBodyHtml = True
                            If (Anexos.Count > 0) Then
                                For i As Integer = 0 To Anexos.Count - 1
                                    If (System.IO.File.Exists(Anexos(i))) Then
                                        .Attachments.Add(New System.Net.Mail.Attachment(Anexos(i)))
                                        'Else
                                        '    MsgBox("El archivo " + Anexos(i) + " No existe")
                                        'Exit Function
                                    End If
                                Next
                            End If
                        End With
                        Try
                            smtp.Send(mailMsg)
                            smtp.Dispose()
                            mailMsg.Dispose()
                            Return True
                            'MsgBox("Mensaje enviado satisfactoriamente")
                        Catch ex As Exception
                            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "SENDMAIL", ex.Message & ", " & ex.InnerException.ToString & ", " & ex.HResult.ToString)
                            'MsgBox("ERROR: " & ex.Message)
                            Return False
                        End Try
                    End Using
                End Using
            Catch ex As Exception
                Return False
            End Try
        End If

        Return False
        '--------------efactura 6666669991011Aa
    End Function

    Public Shared Function SoloNumeros(ByVal strCadena As String) As String
        Dim SoloNumero As String = ""
        strCadena = strCadena.Trim
        Dim index As Integer
        For index = 1 To Len(strCadena)
            If (Mid$(strCadena, index, 1) Like "#") _
                Or Mid$(strCadena, index, 1) = "-" Then
                SoloNumero = SoloNumero & Mid$(strCadena, index, 1)
            End If
        Next
        If SoloNumero.Length > 0 Then
            SoloNumero = CodeFormat(SoloNumero, 4)
        End If
        Return SoloNumero
    End Function
    Private Shared Function CodeFormat(ByVal Id As String, ByVal Tamanio As Int32) As String
        If Id.Length < Tamanio Then
            For i = Id.Length To Tamanio - 1
                Id = "0" & Id
            Next
        End If
        Return Id
    End Function
End Class
