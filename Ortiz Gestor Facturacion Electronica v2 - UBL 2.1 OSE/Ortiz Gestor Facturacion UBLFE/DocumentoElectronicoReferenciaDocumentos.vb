﻿'//Invoice/cac:DespatchDocumentReference/cbc:ID  SERIE-NUMERO
'//Invoice/cac:DespatchDocumentReference/cbc:DocumentTypeCode TIPO(09)
Public Class DocumentoElectronicoReferenciaDocumentos
    Property Documento_Tipo As String 'BOL(25)2:Obligatorio  --Default:09 --//Invoice/cac:DespatchDocumentReference/cbc:DocumentTypeCode
    Property Documento_Serie As String 'BOL(25)1-1:Obligatorio  --//Invoice/cac:DespatchDocumentReference/cbc:ID
    Property Documento_Numero As String 'BOL(25)1-2:Obligatorio
    ReadOnly Property Documento_Serie_Numero As String
        Get
            Return Documento_Serie.Trim & "-" & Documento_Numero.Trim
        End Get
    End Property
    Property Documento_Fecha_Emision As Date
    Property Documento_Importe_Total As Double
    Property Documento_Moneda_Codigo As String
    Property Documento_Ref_Tipo As String
    Property Documento_Ref_Observaciones As String
    Property Documento_PagoCobro_Fecha As Date
    Property Documento_PagoCobro_Correlativo As String
    Property Documento_PagoCobro_SinRetencionPercepcion As Double
    Property Documento_PagoCobro_Moneda_Codigo As Double
    Property Documento_Importe_RetenidoPercibido As Double
    Property Documento_Fecha_RetencionPercepcion As Date
    Property Documento_Total_Pagar_Incluye_RetencionPercepcion As Double
    Property Documento_TipoCambio_Valor As Double
    Property Documento_TipoCambio_Fecha As Date
End Class
