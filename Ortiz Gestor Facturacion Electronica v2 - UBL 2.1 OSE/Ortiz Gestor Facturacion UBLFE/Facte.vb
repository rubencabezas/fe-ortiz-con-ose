﻿Public Class Facte
    Shared Function Registrar_Para_Enviar(pDoc As DocumentoElectronico) As String
        Dim settings As New Xml.XmlWriterSettings With {.ConformanceLevel = Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}
        Using writer As Xml.XmlWriter = Xml.XmlWriter.Create("DocFactura.xml", settings)
            Dim typeToSerialize As Type = GetType(DocumentoElectronico)
            Dim xs As New Xml.Serialization.XmlSerializer(typeToSerialize)
            xs.Serialize(writer, pDoc)
        End Using

        Dim doc12 As New Xml.XmlDocument()
        doc12.Load("DocFactura.xml")

        Dim PruebaXML__1 As New WS_FACTE_2016_10.ws_documentoelectronico
        PruebaXML__1.Url = DocumentoElectronicoURLWebService.URl_WebService
        Dim RptaWS As String = PruebaXML__1.Documento_RegistrarActualizar(doc12)
        Return RptaWS
    End Function
    Shared Function Registrar_y_Enviar(pDoc As DocumentoElectronico) As String
        Dim settings As New Xml.XmlWriterSettings With {.ConformanceLevel = Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}
        Using writer As Xml.XmlWriter = Xml.XmlWriter.Create("DocFactura.xml", settings)
            Dim typeToSerialize As Type = GetType(DocumentoElectronico)
            Dim xs As New Xml.Serialization.XmlSerializer(typeToSerialize)
            xs.Serialize(writer, pDoc)
        End Using

        Dim doc12 As New Xml.XmlDocument()
        doc12.Load("DocFactura.xml")

        Dim PruebaXML__1 As New WS_FACTE_2016_10.ws_documentoelectronico

        PruebaXML__1.Url = DocumentoElectronicoURLWebService.URl_WebService

        Dim RptaWS As String = PruebaXML__1.Documento_RegistrarActualizar_Enviar(doc12)
        Return RptaWS
    End Function
    Shared Function Dar_Baja(pDoc As DocumentoElectronico) As String
        Dim ComBaja As New DocumentoElectronicoComunicacionBaja
        Dim ListaAnulados As New List(Of DocumentoElectronico)
        ListaAnulados.Add(pDoc)
        ComBaja.Documentos = ListaAnulados.ToArray

        Dim settings As New Xml.XmlWriterSettings With {.ConformanceLevel = Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}
        Using writer As Xml.XmlWriter = Xml.XmlWriter.Create("DocBajaFactura.xml", settings)
            Dim typeToSerialize As Type = GetType(DocumentoElectronicoComunicacionBaja)
            Dim xs As New Xml.Serialization.XmlSerializer(typeToSerialize)
            xs.Serialize(writer, ComBaja)
        End Using

        Dim doc12 As New Xml.XmlDocument()
        doc12.Load("DocBajaFactura.xml")

        Dim PruebaXML__1 As New WS_FACTE_2016_10.ws_documentoelectronico

        PruebaXML__1.Url = DocumentoElectronicoURLWebService.URl_WebService

        Dim RptaWS As String = PruebaXML__1.Documento_DarBaja(doc12)
        Return RptaWS
    End Function
    Shared Function Consultar_Estado(pDoc As DocumentoElectronico) As String
        Dim PruebaXML__1 As New WS_FACTE_2016_10.ws_documentoelectronico
        PruebaXML__1.Url = DocumentoElectronicoURLWebService.URl_WebService
        Dim RptaWS As String = PruebaXML__1.Documento_Estado(pDoc.Emisor_Documento_Numero, pDoc.TipoDocumento_Codigo, pDoc.TipoDocumento_Serie, pDoc.TipoDocumento_Numero)
        Return RptaWS
    End Function

    Shared Function Enviar_o(pDoc As DocumentoElectronico) As String
        Dim PruebaXML__1 As New WS_FACTE_2016_10.ws_documentoelectronico
        PruebaXML__1.Url = DocumentoElectronicoURLWebService.URl_WebService
        Dim RptaWS As String = PruebaXML__1.Documento_Estado(pDoc.Emisor_Documento_Numero, pDoc.TipoDocumento_Codigo, pDoc.TipoDocumento_Serie, pDoc.TipoDocumento_Numero)
        Return RptaWS
    End Function
    Shared Function Enviar_Correo(pDoc As DocumentoElectronico) As String
        Dim PruebaXML__1 As New WS_FACTE_2016_10.ws_documentoelectronico
        PruebaXML__1.Url = DocumentoElectronicoURLWebService.URl_WebService
        Dim RptaWS As String = PruebaXML__1.Documento_EnviarCorreo(pDoc.Emisor_Documento_Numero, pDoc.TipoDocumento_Codigo, pDoc.TipoDocumento_Serie, pDoc.TipoDocumento_Numero, pDoc.Fecha_Emision, pDoc.Cliente_Correo)
        Return RptaWS
    End Function
End Class
