﻿Public Class DocumentoElectronicoLeyenda
    Property Codigo As String
    Property Descripcion As String
End Class
'-----------1000-Monto en letras - BOL:Opcional
'-----------1002-“Transferencia gratuita” o “Servicio prestado Gratuitamente” - BOL:Opcional  ---Aplicable solo en el caso que todas las operaciones comprendidas en la boleta de venta electrónica sean gratuitas
'-----------2000-COMPROBANTE DE PERCEPCION - BOL:Opcional
'-----------2001-Bienes transferidos en la Amazonía - BOL:Opcional
'-----------2002-Servicios prestados en la Amazonía - BOL:Opcional
'-----------2003-Contratos de construcción ejecutados en la Amazonía - BOL:Opcional