﻿Public Class fcProceso
    Property Emisor_RUC As String
    Property SUNAT_Servicio_URL As String
    Property SUNAT_Usuario As String
    Property SUNAT_Contrasena As String
    Property ArchivoCreadoCorrectamente As Boolean = False
    Property ArchivoFirmadoCorrectamente As Boolean = False
    Property ArchivoComprimidoCorrectamente As Boolean = False
    Property GeneradosGuardadoServidorCorrectamente As Boolean = False
    Property EnviadoSUNATGuardadoServidorCorrectamente As Boolean = False
    Property PDF_Creado_Correctamente As Boolean
    Property ServidorRutaLocal As String
    Property ServidorRutaServidor As String
    Property FTP_Direccion As String
    Property FTP_Carpeta As String
    Property FTP_Usuario As String
    Property FTP_Contrasena As String
    Property Correo_Usuario As String
    Property Correo_Contrasena As String
    Property Correo_Servidor_SMTP As String
    Property Correo_Servidor_Puerto As String
    Property Correo_Servidor_SSL As Boolean
    Property Correo_Enviado_Cliente As Boolean
    ReadOnly Property ServidorRuta_Temporal As String
        Get
            Return ServidorRutaLocal & "\xml_temp\"
        End Get
    End Property
    ReadOnly Property ServidorRuta_Generado As String
        Get
            If ResumenDiario Then
                Return (ServidorRutaLocal & RutasServidor.ResumenDiario.Generado)
            End If
            If ComunicacionBaja Then
                Return (ServidorRutaLocal & RutasServidor.ComunicacionBaja.Generado)
            End If
            Dim var As String = ""
            If DocumentoTipo = SUNATDocumento.Boleta Then
                var = ServidorRutaLocal & RutasServidor.Boleta.Generado
            ElseIf DocumentoTipo = SUNATDocumento.Factura Then
                var = ServidorRutaLocal & RutasServidor.Factura.Generado
            ElseIf DocumentoTipo = SUNATDocumento.NotaCredito Then
                var = ServidorRutaLocal & RutasServidor.NotaCredito.Generado
            ElseIf DocumentoTipo = SUNATDocumento.NotaDebito Then
                var = ServidorRutaLocal & RutasServidor.NotaDebito.Generado
            ElseIf DocumentoTipo = SUNATDocumento.GuiaRemisionRemitente Then
                var = ServidorRutaLocal & RutasServidor.GuiaRemisionRemitente.Generado
            ElseIf DocumentoTipo = SUNATDocumento.ComprobantePercepcion Then
                var = ServidorRutaLocal & RutasServidor.Percepcion.Generado
            ElseIf DocumentoTipo = SUNATDocumento.ComprobanteRetencion Then
                var = ServidorRutaLocal & RutasServidor.Retencion.Generado
            End If
            Return var
        End Get
    End Property
    ReadOnly Property ServidorRuta_Aceptado As String
        Get
            If ResumenDiario Then
                Return (ServidorRutaLocal & RutasServidor.ResumenDiario.Aceptado)
            End If
            If ComunicacionBaja Then
                Return (ServidorRutaLocal & RutasServidor.ComunicacionBaja.Aceptado)
            End If
            Dim var As String = ""
            If DocumentoTipo = SUNATDocumento.Boleta Then
                var = ServidorRutaLocal & RutasServidor.Boleta.Aceptado
            ElseIf DocumentoTipo = SUNATDocumento.Factura Then
                var = ServidorRutaLocal & RutasServidor.Factura.Aceptado
            ElseIf DocumentoTipo = SUNATDocumento.NotaCredito Then
                var = ServidorRutaLocal & RutasServidor.NotaCredito.Aceptado
            ElseIf DocumentoTipo = SUNATDocumento.NotaDebito Then
                var = ServidorRutaLocal & RutasServidor.NotaDebito.Aceptado
            ElseIf DocumentoTipo = SUNATDocumento.GuiaRemisionRemitente Then
                var = ServidorRutaLocal & RutasServidor.GuiaRemisionRemitente.Aceptado
            ElseIf DocumentoTipo = SUNATDocumento.ComprobantePercepcion Then
                var = ServidorRutaLocal & RutasServidor.Percepcion.Aceptado
            ElseIf DocumentoTipo = SUNATDocumento.ComprobanteRetencion Then
                var = ServidorRutaLocal & RutasServidor.Retencion.Aceptado
            End If
            Return var
        End Get
    End Property
    ReadOnly Property ServidorRuta_Rechazado As String
        Get
            If ResumenDiario Then
                Return (ServidorRutaLocal & RutasServidor.ResumenDiario.Rechazado)
            End If
            If ComunicacionBaja Then
                Return (ServidorRutaLocal & RutasServidor.ComunicacionBaja.Rechazado)
            End If
            Dim var As String = ""
            If DocumentoTipo = SUNATDocumento.Boleta Then
                var = ServidorRutaLocal & RutasServidor.Boleta.Rechazado
            ElseIf DocumentoTipo = SUNATDocumento.Factura Then
                var = ServidorRutaLocal & RutasServidor.Factura.Rechazado
            ElseIf DocumentoTipo = SUNATDocumento.NotaCredito Then
                var = ServidorRutaLocal & RutasServidor.NotaCredito.Rechazado
            ElseIf DocumentoTipo = SUNATDocumento.NotaDebito Then
                var = ServidorRutaLocal & RutasServidor.NotaDebito.Rechazado
            ElseIf DocumentoTipo = SUNATDocumento.GuiaRemisionRemitente Then
                var = ServidorRutaLocal & RutasServidor.GuiaRemisionRemitente.Rechazado
            ElseIf DocumentoTipo = SUNATDocumento.ComprobantePercepcion Then
                var = ServidorRutaLocal & RutasServidor.Percepcion.Rechazado
            ElseIf DocumentoTipo = SUNATDocumento.ComprobanteRetencion Then
                var = ServidorRutaLocal & RutasServidor.Retencion.Rechazado
            End If
            Return var
        End Get
    End Property
    Property DocumentoTipo As String
    Property ResumenDiario As Boolean
    Property ComunicacionBaja As Boolean
    Property FileNameWithOutExtension As String
    ReadOnly Property FileNameXML As String
        Get
            Return FileNameWithOutExtension & ".xml"
        End Get
    End Property
    ReadOnly Property FileNameZIP As String
        Get
            Return FileNameWithOutExtension & ".zip"
        End Get
    End Property
    ReadOnly Property FileNamePDF As String
        Get
            Return FileNameWithOutExtension & ".pdf"
        End Get
    End Property
    Property Etapa As String
    Property CDRNameZIP As String
    Property CDRNameXML As String
    Property TicketNumero As String
    Property ExtensionContent_Indice As Integer
    Property Es_OSE As Boolean
End Class
