﻿Public Class UrlSUNAT
    Property GrpTipo_Codigo As String
    Property Etapa_Codigo As String
    Property GrpTipo_URL As String
    Property GrpTipo_URL_Validez As String
    Property GrpTipo_URL_Estado_Envio As String
    Sub New(pGrpTipo_Codigo As String, pEtapa_Codigo As String, pGrpTipo_URL As String, pGrpTipo_URL_Validez As String, pGrpTipo_URL_Estado_Envio As String)
        GrpTipo_Codigo = pGrpTipo_Codigo
        Etapa_Codigo = pEtapa_Codigo
        GrpTipo_URL = pGrpTipo_URL
        GrpTipo_URL_Validez = pGrpTipo_URL_Validez
        GrpTipo_URL_Estado_Envio = pGrpTipo_URL_Estado_Envio
    End Sub
End Class
