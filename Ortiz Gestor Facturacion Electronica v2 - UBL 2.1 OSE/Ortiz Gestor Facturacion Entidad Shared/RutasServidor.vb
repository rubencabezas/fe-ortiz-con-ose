﻿Public Class RutasServidor
    Shared Property Root As String
    Shared Property Root_Generado As String = Root & "\xml_generated\"
    Shared Property Root_Aceptado As String = Root & "\xml_sunat_aceptados\"
    Shared Property Root_Rechazado As String = Root & "\xml_sunat_rechazados\"
    Shared Property Root_Adjuntos As String = Root & "\xml_pos_attached\"
    Partial Class Factura
        Shared pRoot As String = "factura\"
        Shared ReadOnly Property Generado As String
            Get
                Return Root_Generado & pRoot
            End Get
        End Property
        Shared ReadOnly Property Aceptado As String
            Get
                Return Root_Aceptado & pRoot
            End Get
        End Property
        Shared ReadOnly Property Rechazado As String
            Get
                Return Root_Rechazado & pRoot
            End Get
        End Property
    End Class
    Partial Class Boleta
        Shared pRoot As String = "boleta\"
        Shared ReadOnly Property Generado As String
            Get
                Return Root_Generado & pRoot
            End Get
        End Property
        Shared ReadOnly Property Aceptado As String
            Get
                Return Root_Aceptado & pRoot
            End Get
        End Property
        Shared ReadOnly Property Rechazado As String
            Get
                Return Root_Rechazado & pRoot
            End Get
        End Property
    End Class
    Partial Class GuiaRemisionRemitente
        Shared pRoot As String = "guia_remision_remitente\"
        Shared ReadOnly Property Generado As String
            Get
                Return Root_Generado & pRoot
            End Get
        End Property
        Shared ReadOnly Property Aceptado As String
            Get
                Return Root_Aceptado & pRoot
            End Get
        End Property
        Shared ReadOnly Property Rechazado As String
            Get
                Return Root_Rechazado & pRoot
            End Get
        End Property
    End Class
    Partial Class NotaCredito
        Shared pRoot As String = "nota_credito\"
        Shared ReadOnly Property Generado As String
            Get
                Return Root_Generado & pRoot
            End Get
        End Property
        Shared ReadOnly Property Aceptado As String
            Get
                Return Root_Aceptado & pRoot
            End Get
        End Property
        Shared ReadOnly Property Rechazado As String
            Get
                Return Root_Rechazado & pRoot
            End Get
        End Property
    End Class
    Partial Class NotaDebito
        Shared pRoot As String = "nota_debito\"
        Shared ReadOnly Property Generado As String
            Get
                Return Root_Generado & pRoot
            End Get
        End Property
        Shared ReadOnly Property Aceptado As String
            Get
                Return Root_Aceptado & pRoot
            End Get
        End Property
        Shared ReadOnly Property Rechazado As String
            Get
                Return Root_Rechazado & pRoot
            End Get
        End Property
    End Class
    Partial Class ResumenDiario
        Shared pRoot As String = "resumen_diario\"
        Shared ReadOnly Property Generado As String
            Get
                Return Root_Generado & pRoot
            End Get
        End Property
        Shared ReadOnly Property Aceptado As String
            Get
                Return Root_Aceptado & pRoot
            End Get
        End Property
        Shared ReadOnly Property Rechazado As String
            Get
                Return Root_Rechazado & pRoot
            End Get
        End Property
    End Class
    Partial Class ComunicacionBaja
        Shared pRoot As String = "comunicacion_baja\"
        Shared ReadOnly Property Generado As String
            Get
                Return Root_Generado & pRoot
            End Get
        End Property
        Shared ReadOnly Property Aceptado As String
            Get
                Return Root_Aceptado & pRoot
            End Get
        End Property
        Shared ReadOnly Property Rechazado As String
            Get
                Return Root_Rechazado & pRoot
            End Get
        End Property
    End Class
    Partial Class Percepcion
        Shared pRoot As String = "percepcion\"
        Shared ReadOnly Property Generado As String
            Get
                Return Root_Generado & pRoot
            End Get
        End Property
        Shared ReadOnly Property Aceptado As String
            Get
                Return Root_Aceptado & pRoot
            End Get
        End Property
        Shared ReadOnly Property Rechazado As String
            Get
                Return Root_Rechazado & pRoot
            End Get
        End Property
    End Class
    Partial Class Retencion
        Shared pRoot As String = "retencion\"
        Shared ReadOnly Property Generado As String
            Get
                Return Root_Generado & pRoot
            End Get
        End Property
        Shared ReadOnly Property Aceptado As String
            Get
                Return Root_Aceptado & pRoot
            End Get
        End Property
        Shared ReadOnly Property Rechazado As String
            Get
                Return Root_Rechazado & pRoot
            End Get
        End Property
    End Class
End Class
