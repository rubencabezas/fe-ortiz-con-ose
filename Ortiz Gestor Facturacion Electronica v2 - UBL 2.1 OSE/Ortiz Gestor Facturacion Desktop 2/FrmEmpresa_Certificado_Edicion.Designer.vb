﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEmpresa_Certificado_Edicion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmEmpresa_Certificado_Edicion))
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.BtnSalir = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.BtnExportXls = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnExportPdf = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnExportRtf = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnExportHtml = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TxtEmpRazonSocial = New dhsoft.TextBoxNew(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TxtEmpRUC = New dhsoft.TextBoxNew(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.DgvGeneral = New DevExpress.XtraGrid.GridControl()
        Me.VwDgv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.BtnQuitar = New System.Windows.Forms.Button()
        Me.BtnAgregar = New System.Windows.Forms.Button()
        Me.BtnNuevo = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.TxtCodigo = New dhsoft.TextBoxNew(Me.components)
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TxtPropieRazonSocial = New dhsoft.TextBoxNew(Me.components)
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TxtPropieRUC = New dhsoft.TextBoxNew(Me.components)
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ChkEsPropio = New dhsoft.CheckBoxNew()
        Me.BtnBuscarRuta = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TxtRutaCertificado = New dhsoft.TextBoxNew(Me.components)
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TxtContrasena = New dhsoft.TextBoxNew(Me.components)
        Me.TxtVigHasta = New dhsoft.MaskedTextBoxNew()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TxtVigDesde = New dhsoft.MaskedTextBoxNew()
        Me.Label4 = New System.Windows.Forms.Label()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DgvGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VwDgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BtnExportXls, Me.BtnExportPdf, Me.BtnExportRtf, Me.BtnExportHtml, Me.BtnSalir, Me.BarButtonItem1})
        Me.BarManager1.MaxItemId = 19
        '
        'Bar1
        '
        Me.Bar1.BarName = "Herramientas"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BtnSalir)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Herramientas"
        '
        'BtnSalir
        '
        Me.BtnSalir.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.BtnSalir.Caption = "Salir"
        Me.BtnSalir.Glyph = CType(resources.GetObject("BtnSalir.Glyph"), System.Drawing.Image)
        Me.BtnSalir.Id = 10
        Me.BtnSalir.ImageIndex = 15
        Me.BtnSalir.Name = "BtnSalir"
        Me.BtnSalir.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(570, 35)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 408)
        Me.barDockControlBottom.Size = New System.Drawing.Size(570, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 35)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 373)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(570, 35)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 373)
        '
        'BtnExportXls
        '
        Me.BtnExportXls.Caption = "Excel"
        Me.BtnExportXls.Id = 4
        Me.BtnExportXls.ImageIndex = 10
        Me.BtnExportXls.Name = "BtnExportXls"
        '
        'BtnExportPdf
        '
        Me.BtnExportPdf.Caption = "Pdf"
        Me.BtnExportPdf.Id = 5
        Me.BtnExportPdf.ImageIndex = 11
        Me.BtnExportPdf.Name = "BtnExportPdf"
        '
        'BtnExportRtf
        '
        Me.BtnExportRtf.Caption = "Rtf"
        Me.BtnExportRtf.Id = 6
        Me.BtnExportRtf.ImageIndex = 12
        Me.BtnExportRtf.Name = "BtnExportRtf"
        '
        'BtnExportHtml
        '
        Me.BtnExportHtml.Caption = "Html"
        Me.BtnExportHtml.Id = 7
        Me.BtnExportHtml.ImageIndex = 13
        Me.BtnExportHtml.Name = "BtnExportHtml"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Cancelar"
        Me.BarButtonItem1.Id = 11
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TxtEmpRazonSocial)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.TxtEmpRUC)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 41)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(545, 45)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(173, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Razón Social"
        '
        'TxtEmpRazonSocial
        '
        Me.TxtEmpRazonSocial.BackColor = System.Drawing.Color.White
        Me.TxtEmpRazonSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtEmpRazonSocial.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtEmpRazonSocial.DecimalPrecision = 0
        Me.TxtEmpRazonSocial.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtEmpRazonSocial.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtEmpRazonSocial.Enabled = False
        Me.TxtEmpRazonSocial.Enganche = Nothing
        Me.TxtEmpRazonSocial.EnterEmuleTab = True
        Me.TxtEmpRazonSocial.IsDecimalNegative = False
        Me.TxtEmpRazonSocial.LinkKeyDown = Nothing
        Me.TxtEmpRazonSocial.Location = New System.Drawing.Point(250, 15)
        Me.TxtEmpRazonSocial.MaskFormat = Nothing
        Me.TxtEmpRazonSocial.Name = "TxtEmpRazonSocial"
        Me.TxtEmpRazonSocial.Requiere = False
        Me.TxtEmpRazonSocial.Size = New System.Drawing.Size(285, 20)
        Me.TxtEmpRazonSocial.TabIndex = 10
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Núm. RUC"
        '
        'TxtEmpRUC
        '
        Me.TxtEmpRUC.BackColor = System.Drawing.Color.White
        Me.TxtEmpRUC.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtEmpRUC.DecimalPrecision = 0
        Me.TxtEmpRUC.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtEmpRUC.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtEmpRUC.Enabled = False
        Me.TxtEmpRUC.Enganche = Nothing
        Me.TxtEmpRUC.EnterEmuleTab = True
        Me.TxtEmpRUC.IsDecimalNegative = False
        Me.TxtEmpRUC.LinkKeyDown = Nothing
        Me.TxtEmpRUC.Location = New System.Drawing.Point(82, 15)
        Me.TxtEmpRUC.MaskFormat = Nothing
        Me.TxtEmpRUC.Name = "TxtEmpRUC"
        Me.TxtEmpRUC.Requiere = False
        Me.TxtEmpRUC.Size = New System.Drawing.Size(86, 20)
        Me.TxtEmpRUC.TabIndex = 8
        Me.TxtEmpRUC.Text = "10421713559"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.DgvGeneral)
        Me.GroupBox2.Controls.Add(Me.BtnQuitar)
        Me.GroupBox2.Controls.Add(Me.BtnAgregar)
        Me.GroupBox2.Controls.Add(Me.BtnNuevo)
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 91)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(545, 310)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        '
        'DgvGeneral
        '
        Me.DgvGeneral.Cursor = System.Windows.Forms.Cursors.Default
        Me.DgvGeneral.Location = New System.Drawing.Point(9, 128)
        Me.DgvGeneral.MainView = Me.VwDgv
        Me.DgvGeneral.Name = "DgvGeneral"
        Me.DgvGeneral.Size = New System.Drawing.Size(527, 173)
        Me.DgvGeneral.TabIndex = 163
        Me.DgvGeneral.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.VwDgv})
        '
        'VwDgv
        '
        Me.VwDgv.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.VwDgv.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.VwDgv.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.VwDgv.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.VwDgv.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.VwDgv.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.VwDgv.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.Empty.BackColor2 = System.Drawing.Color.White
        Me.VwDgv.Appearance.Empty.Options.UseBackColor = True
        Me.VwDgv.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.VwDgv.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.EvenRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.EvenRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.VwDgv.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.VwDgv.Appearance.FilterCloseButton.Options.UseForeColor = True
        Me.VwDgv.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
        Me.VwDgv.Appearance.FilterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FilterPanel.Options.UseBackColor = True
        Me.VwDgv.Appearance.FilterPanel.Options.UseForeColor = True
        Me.VwDgv.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.VwDgv.Appearance.FixedLine.Options.UseBackColor = True
        Me.VwDgv.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
        Me.VwDgv.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.FocusedCell.Options.UseBackColor = True
        Me.VwDgv.Appearance.FocusedCell.Options.UseForeColor = True
        Me.VwDgv.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.VwDgv.Appearance.FocusedRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.FocusedRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FooterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.FooterPanel.Options.UseBackColor = True
        Me.VwDgv.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.VwDgv.Appearance.FooterPanel.Options.UseForeColor = True
        Me.VwDgv.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.GroupButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.GroupButton.Options.UseBackColor = True
        Me.VwDgv.Appearance.GroupButton.Options.UseBorderColor = True
        Me.VwDgv.Appearance.GroupButton.Options.UseForeColor = True
        Me.VwDgv.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.GroupFooter.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.GroupFooter.Options.UseBackColor = True
        Me.VwDgv.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.VwDgv.Appearance.GroupFooter.Options.UseForeColor = True
        Me.VwDgv.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
        Me.VwDgv.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.GroupPanel.Options.UseBackColor = True
        Me.VwDgv.Appearance.GroupPanel.Options.UseForeColor = True
        Me.VwDgv.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.GroupRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.GroupRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.GroupRow.Options.UseBorderColor = True
        Me.VwDgv.Appearance.GroupRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.VwDgv.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.VwDgv.Appearance.HeaderPanel.Options.UseForeColor = True
        Me.VwDgv.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(195, Byte), Integer))
        Me.VwDgv.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.VwDgv.Appearance.HorzLine.Options.UseBackColor = True
        Me.VwDgv.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.VwDgv.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.OddRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.OddRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.VwDgv.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
        Me.VwDgv.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.Preview.Options.UseBackColor = True
        Me.VwDgv.Appearance.Preview.Options.UseFont = True
        Me.VwDgv.Appearance.Preview.Options.UseForeColor = True
        Me.VwDgv.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.VwDgv.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.Row.Options.UseBackColor = True
        Me.VwDgv.Appearance.Row.Options.UseForeColor = True
        Me.VwDgv.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
        Me.VwDgv.Appearance.RowSeparator.Options.UseBackColor = True
        Me.VwDgv.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(107, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(179, Byte), Integer))
        Me.VwDgv.Appearance.SelectedRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.SelectedRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.SelectedRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
        Me.VwDgv.Appearance.TopNewRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.VwDgv.Appearance.VertLine.Options.UseBackColor = True
        Me.VwDgv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4})
        Me.VwDgv.GridControl = Me.DgvGeneral
        Me.VwDgv.Name = "VwDgv"
        Me.VwDgv.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.VwDgv.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.VwDgv.OptionsBehavior.Editable = False
        Me.VwDgv.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.VwDgv.OptionsView.EnableAppearanceEvenRow = True
        Me.VwDgv.OptionsView.EnableAppearanceOddRow = True
        Me.VwDgv.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Corr."
        Me.GridColumn1.FieldName = "Correlativo"
        Me.GridColumn1.MaxWidth = 40
        Me.GridColumn1.MinWidth = 40
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 40
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Vig.Desde"
        Me.GridColumn2.FieldName = "Valido_Desde"
        Me.GridColumn2.MaxWidth = 80
        Me.GridColumn2.MinWidth = 80
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 80
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Vig.Hasta"
        Me.GridColumn3.FieldName = "Valido_Hasta"
        Me.GridColumn3.MaxWidth = 80
        Me.GridColumn3.MinWidth = 80
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 80
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Ruta"
        Me.GridColumn4.FieldName = "Ruta"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 3
        '
        'BtnQuitar
        '
        Me.BtnQuitar.Location = New System.Drawing.Point(483, 61)
        Me.BtnQuitar.Name = "BtnQuitar"
        Me.BtnQuitar.Size = New System.Drawing.Size(53, 23)
        Me.BtnQuitar.TabIndex = 3
        Me.BtnQuitar.Text = "Quitar"
        Me.BtnQuitar.UseVisualStyleBackColor = True
        '
        'BtnAgregar
        '
        Me.BtnAgregar.Location = New System.Drawing.Point(483, 38)
        Me.BtnAgregar.Name = "BtnAgregar"
        Me.BtnAgregar.Size = New System.Drawing.Size(53, 23)
        Me.BtnAgregar.TabIndex = 2
        Me.BtnAgregar.Text = "Guardar"
        Me.BtnAgregar.UseVisualStyleBackColor = True
        '
        'BtnNuevo
        '
        Me.BtnNuevo.Location = New System.Drawing.Point(483, 15)
        Me.BtnNuevo.Name = "BtnNuevo"
        Me.BtnNuevo.Size = New System.Drawing.Size(53, 23)
        Me.BtnNuevo.TabIndex = 0
        Me.BtnNuevo.Text = "Nuevo"
        Me.BtnNuevo.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.TxtCodigo)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.TxtPropieRazonSocial)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.TxtPropieRUC)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.ChkEsPropio)
        Me.GroupBox3.Controls.Add(Me.BtnBuscarRuta)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.TxtRutaCertificado)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.TxtContrasena)
        Me.GroupBox3.Controls.Add(Me.TxtVigHasta)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.TxtVigDesde)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Enabled = False
        Me.GroupBox3.Location = New System.Drawing.Point(9, 11)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(468, 112)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Registro de Certificado"
        '
        'TxtCodigo
        '
        Me.TxtCodigo.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtCodigo.DecimalPrecision = 0
        Me.TxtCodigo.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.TxtCodigo.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtCodigo.Enganche = Nothing
        Me.TxtCodigo.EnterEmuleTab = True
        Me.TxtCodigo.IsDecimalNegative = False
        Me.TxtCodigo.LinkKeyDown = Nothing
        Me.TxtCodigo.Location = New System.Drawing.Point(71, 15)
        Me.TxtCodigo.MaskFormat = Nothing
        Me.TxtCodigo.Name = "TxtCodigo"
        Me.TxtCodigo.Requiere = False
        Me.TxtCodigo.Size = New System.Drawing.Size(122, 20)
        Me.TxtCodigo.TabIndex = 8
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(7, 18)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(40, 13)
        Me.Label9.TabIndex = 23
        Me.Label9.Text = "Código"
        '
        'TxtPropieRazonSocial
        '
        Me.TxtPropieRazonSocial.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtPropieRazonSocial.DecimalPrecision = 0
        Me.TxtPropieRazonSocial.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtPropieRazonSocial.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtPropieRazonSocial.Enabled = False
        Me.TxtPropieRazonSocial.Enganche = Nothing
        Me.TxtPropieRazonSocial.EnterEmuleTab = True
        Me.TxtPropieRazonSocial.IsDecimalNegative = False
        Me.TxtPropieRazonSocial.LinkKeyDown = Nothing
        Me.TxtPropieRazonSocial.Location = New System.Drawing.Point(203, 38)
        Me.TxtPropieRazonSocial.MaskFormat = Nothing
        Me.TxtPropieRazonSocial.Name = "TxtPropieRazonSocial"
        Me.TxtPropieRazonSocial.Requiere = False
        Me.TxtPropieRazonSocial.Size = New System.Drawing.Size(259, 20)
        Me.TxtPropieRazonSocial.TabIndex = 13
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(153, 41)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(47, 13)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "R.Social"
        '
        'TxtPropieRUC
        '
        Me.TxtPropieRUC.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtPropieRUC.DecimalPrecision = 0
        Me.TxtPropieRUC.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtPropieRUC.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtPropieRUC.Enabled = False
        Me.TxtPropieRUC.Enganche = Nothing
        Me.TxtPropieRUC.EnterEmuleTab = True
        Me.TxtPropieRUC.IsDecimalNegative = False
        Me.TxtPropieRUC.LinkKeyDown = Nothing
        Me.TxtPropieRUC.Location = New System.Drawing.Point(71, 38)
        Me.TxtPropieRUC.MaskFormat = Nothing
        Me.TxtPropieRUC.Name = "TxtPropieRUC"
        Me.TxtPropieRUC.Requiere = False
        Me.TxtPropieRUC.Size = New System.Drawing.Size(79, 20)
        Me.TxtPropieRUC.TabIndex = 11
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 41)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 13)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "Prov. RUC"
        '
        'ChkEsPropio
        '
        Me.ChkEsPropio.AutoSize = True
        Me.ChkEsPropio.Checked = True
        Me.ChkEsPropio.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkEsPropio.Location = New System.Drawing.Point(199, 17)
        Me.ChkEsPropio.Name = "ChkEsPropio"
        Me.ChkEsPropio.Size = New System.Drawing.Size(71, 17)
        Me.ChkEsPropio.TabIndex = 9
        Me.ChkEsPropio.Text = "Es Propio"
        Me.ChkEsPropio.UseVisualStyleBackColor = True
        '
        'BtnBuscarRuta
        '
        Me.BtnBuscarRuta.Location = New System.Drawing.Point(437, 82)
        Me.BtnBuscarRuta.Name = "BtnBuscarRuta"
        Me.BtnBuscarRuta.Size = New System.Drawing.Size(25, 23)
        Me.BtnBuscarRuta.TabIndex = 22
        Me.BtnBuscarRuta.Text = "..."
        Me.BtnBuscarRuta.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(7, 87)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 13)
        Me.Label6.TabIndex = 20
        Me.Label6.Text = "Ruta Cert."
        '
        'TxtRutaCertificado
        '
        Me.TxtRutaCertificado.BackColor = System.Drawing.Color.White
        Me.TxtRutaCertificado.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtRutaCertificado.DecimalPrecision = 0
        Me.TxtRutaCertificado.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtRutaCertificado.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtRutaCertificado.Enabled = False
        Me.TxtRutaCertificado.Enganche = Nothing
        Me.TxtRutaCertificado.EnterEmuleTab = True
        Me.TxtRutaCertificado.IsDecimalNegative = False
        Me.TxtRutaCertificado.LinkKeyDown = Nothing
        Me.TxtRutaCertificado.Location = New System.Drawing.Point(71, 84)
        Me.TxtRutaCertificado.MaskFormat = Nothing
        Me.TxtRutaCertificado.Name = "TxtRutaCertificado"
        Me.TxtRutaCertificado.Requiere = False
        Me.TxtRutaCertificado.Size = New System.Drawing.Size(365, 20)
        Me.TxtRutaCertificado.TabIndex = 21
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(278, 64)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 18
        Me.Label5.Text = "Contraseña"
        '
        'TxtContrasena
        '
        Me.TxtContrasena.BackColor = System.Drawing.Color.White
        Me.TxtContrasena.Contenido = dhsoft.Enums.TipoTexto.NormalText
        Me.TxtContrasena.DecimalPrecision = 0
        Me.TxtContrasena.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TxtContrasena.DisabledForeColor = System.Drawing.Color.Black
        Me.TxtContrasena.Enganche = Nothing
        Me.TxtContrasena.EnterEmuleTab = True
        Me.TxtContrasena.IsDecimalNegative = False
        Me.TxtContrasena.LinkKeyDown = Nothing
        Me.TxtContrasena.Location = New System.Drawing.Point(342, 61)
        Me.TxtContrasena.MaskFormat = Nothing
        Me.TxtContrasena.Name = "TxtContrasena"
        Me.TxtContrasena.Requiere = False
        Me.TxtContrasena.Size = New System.Drawing.Size(120, 20)
        Me.TxtContrasena.TabIndex = 19
        '
        'TxtVigHasta
        '
        Me.TxtVigHasta.IsValueDate = False
        Me.TxtVigHasta.Location = New System.Drawing.Point(203, 61)
        Me.TxtVigHasta.Mask = "00/00/0000"
        Me.TxtVigHasta.Name = "TxtVigHasta"
        Me.TxtVigHasta.Requiere = False
        Me.TxtVigHasta.Size = New System.Drawing.Size(70, 20)
        Me.TxtVigHasta.TabIndex = 17
        Me.TxtVigHasta.ValidatingType = GetType(Date)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(146, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 13)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Vig. Hasta"
        '
        'TxtVigDesde
        '
        Me.TxtVigDesde.IsValueDate = False
        Me.TxtVigDesde.Location = New System.Drawing.Point(71, 61)
        Me.TxtVigDesde.Mask = "00/00/0000"
        Me.TxtVigDesde.Name = "TxtVigDesde"
        Me.TxtVigDesde.Requiere = False
        Me.TxtVigDesde.Size = New System.Drawing.Size(70, 20)
        Me.TxtVigDesde.TabIndex = 15
        Me.TxtVigDesde.ValidatingType = GetType(Date)
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 13)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "Vig. Desde"
        '
        'FrmEmpresa_Certificado_Edicion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(570, 408)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmEmpresa_Certificado_Edicion"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Certificados"
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.DgvGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VwDgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BtnSalir As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BtnExportXls As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnExportPdf As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnExportRtf As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnExportHtml As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TxtEmpRazonSocial As dhsoft.TextBoxNew
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TxtEmpRUC As dhsoft.TextBoxNew
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents TxtVigHasta As dhsoft.MaskedTextBoxNew
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TxtVigDesde As dhsoft.MaskedTextBoxNew
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TxtRutaCertificado As dhsoft.TextBoxNew
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TxtContrasena As dhsoft.TextBoxNew
    Friend WithEvents BtnQuitar As System.Windows.Forms.Button
    Friend WithEvents BtnAgregar As System.Windows.Forms.Button
    Friend WithEvents BtnNuevo As System.Windows.Forms.Button
    Friend WithEvents BtnBuscarRuta As System.Windows.Forms.Button
    Friend WithEvents DgvGeneral As DevExpress.XtraGrid.GridControl
    Public WithEvents VwDgv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ChkEsPropio As dhsoft.CheckBoxNew
    Friend WithEvents TxtPropieRazonSocial As dhsoft.TextBoxNew
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TxtPropieRUC As dhsoft.TextBoxNew
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TxtCodigo As dhsoft.TextBoxNew
    Friend WithEvents Label9 As System.Windows.Forms.Label
End Class
