﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTablaGeneral_Busqueda
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.BarraHerramienta = New DevExpress.XtraBars.Bar()
        Me.BtnSeleccionar = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnSalir = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.DgvGeneral = New DevExpress.XtraGrid.GridControl()
        Me.VwDgv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DgvGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VwDgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.BarraHerramienta})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BtnSeleccionar, Me.BtnSalir})
        Me.BarManager1.MaxItemId = 6
        '
        'BarraHerramienta
        '
        Me.BarraHerramienta.BarName = "Herramientas"
        Me.BarraHerramienta.DockCol = 0
        Me.BarraHerramienta.DockRow = 0
        Me.BarraHerramienta.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.BarraHerramienta.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BtnSeleccionar), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnSalir)})
        Me.BarraHerramienta.OptionsBar.AllowCollapse = True
        Me.BarraHerramienta.OptionsBar.AllowQuickCustomization = False
        Me.BarraHerramienta.OptionsBar.AllowRename = True
        Me.BarraHerramienta.OptionsBar.DisableClose = True
        Me.BarraHerramienta.OptionsBar.DisableCustomization = True
        Me.BarraHerramienta.OptionsBar.DrawDragBorder = False
        Me.BarraHerramienta.OptionsBar.UseWholeRow = True
        Me.BarraHerramienta.Text = "Herramientas"
        '
        'BtnSeleccionar
        '
        Me.BtnSeleccionar.Caption = "Seleccionar"
        Me.BtnSeleccionar.Id = 0
        Me.BtnSeleccionar.Name = "BtnSeleccionar"
        Me.BtnSeleccionar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnSalir
        '
        Me.BtnSalir.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.BtnSalir.Caption = "Salir"
        Me.BtnSalir.Id = 5
        Me.BtnSalir.Name = "BtnSalir"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(485, 29)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 372)
        Me.barDockControlBottom.Size = New System.Drawing.Size(485, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 29)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 343)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(485, 29)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 343)
        '
        'DgvGeneral
        '
        Me.DgvGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DgvGeneral.Location = New System.Drawing.Point(0, 29)
        Me.DgvGeneral.MainView = Me.VwDgv
        Me.DgvGeneral.Name = "DgvGeneral"
        Me.DgvGeneral.Size = New System.Drawing.Size(485, 343)
        Me.DgvGeneral.TabIndex = 13
        Me.DgvGeneral.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.VwDgv})
        '
        'VwDgv
        '
        Me.VwDgv.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.VwDgv.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.VwDgv.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.VwDgv.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.VwDgv.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.VwDgv.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.VwDgv.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.Empty.BackColor2 = System.Drawing.Color.White
        Me.VwDgv.Appearance.Empty.Options.UseBackColor = True
        Me.VwDgv.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.VwDgv.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.EvenRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.EvenRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.VwDgv.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.VwDgv.Appearance.FilterCloseButton.Options.UseForeColor = True
        Me.VwDgv.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
        Me.VwDgv.Appearance.FilterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FilterPanel.Options.UseBackColor = True
        Me.VwDgv.Appearance.FilterPanel.Options.UseForeColor = True
        Me.VwDgv.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.FixedLine.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.FixedLine.Options.UseBackColor = True
        Me.VwDgv.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
        Me.VwDgv.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.FocusedCell.Options.UseBackColor = True
        Me.VwDgv.Appearance.FocusedCell.Options.UseForeColor = True
        Me.VwDgv.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.VwDgv.Appearance.FocusedRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.FocusedRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FooterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.FooterPanel.Options.UseBackColor = True
        Me.VwDgv.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.VwDgv.Appearance.FooterPanel.Options.UseForeColor = True
        Me.VwDgv.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.GroupButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.GroupButton.Options.UseBackColor = True
        Me.VwDgv.Appearance.GroupButton.Options.UseBorderColor = True
        Me.VwDgv.Appearance.GroupButton.Options.UseForeColor = True
        Me.VwDgv.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.GroupFooter.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.GroupFooter.Options.UseBackColor = True
        Me.VwDgv.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.VwDgv.Appearance.GroupFooter.Options.UseForeColor = True
        Me.VwDgv.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
        Me.VwDgv.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.GroupPanel.Options.UseBackColor = True
        Me.VwDgv.Appearance.GroupPanel.Options.UseForeColor = True
        Me.VwDgv.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.GroupRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.GroupRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.GroupRow.Options.UseBorderColor = True
        Me.VwDgv.Appearance.GroupRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(96, Byte), Integer), CType(CType(115, Byte), Integer), CType(CType(148, Byte), Integer))
        Me.VwDgv.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.VwDgv.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.VwDgv.Appearance.HeaderPanel.Options.UseForeColor = True
        Me.VwDgv.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(195, Byte), Integer))
        Me.VwDgv.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.VwDgv.Appearance.HorzLine.Options.UseBackColor = True
        Me.VwDgv.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.VwDgv.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.OddRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.OddRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.VwDgv.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
        Me.VwDgv.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.Preview.Options.UseBackColor = True
        Me.VwDgv.Appearance.Preview.Options.UseFont = True
        Me.VwDgv.Appearance.Preview.Options.UseForeColor = True
        Me.VwDgv.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.VwDgv.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.Row.Options.UseBackColor = True
        Me.VwDgv.Appearance.Row.Options.UseForeColor = True
        Me.VwDgv.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
        Me.VwDgv.Appearance.RowSeparator.Options.UseBackColor = True
        Me.VwDgv.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(107, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(179, Byte), Integer))
        Me.VwDgv.Appearance.SelectedRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.SelectedRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.SelectedRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
        Me.VwDgv.Appearance.TopNewRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.VwDgv.Appearance.VertLine.Options.UseBackColor = True
        Me.VwDgv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3})
        Me.VwDgv.GridControl = Me.DgvGeneral
        Me.VwDgv.Name = "VwDgv"
        Me.VwDgv.OptionsView.EnableAppearanceEvenRow = True
        Me.VwDgv.OptionsView.EnableAppearanceOddRow = True
        Me.VwDgv.OptionsView.ShowAutoFilterRow = True
        Me.VwDgv.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Código"
        Me.GridColumn1.FieldName = "TAD_CODIGO_OFICIAL"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 100
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Descripción"
        Me.GridColumn2.FieldName = "TAD_DESCRIPCION"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsColumn.AllowEdit = False
        Me.GridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 400
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Código Sistema"
        Me.GridColumn3.FieldName = "TAD_CODIGO"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.OptionsColumn.AllowEdit = False
        Me.GridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Me.GridColumn3.Width = 111
        '
        'FrmTablaGeneral_Busqueda
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(485, 372)
        Me.Controls.Add(Me.DgvGeneral)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "FrmTablaGeneral_Busqueda"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Estados"
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DgvGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VwDgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents BarraHerramienta As DevExpress.XtraBars.Bar
    Friend WithEvents BtnSeleccionar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnSalir As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents DgvGeneral As DevExpress.XtraGrid.GridControl
    Friend WithEvents VwDgv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
End Class
