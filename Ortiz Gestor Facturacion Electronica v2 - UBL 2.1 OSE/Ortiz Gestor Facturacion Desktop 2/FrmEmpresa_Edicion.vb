﻿Public Class FrmEmpresa_Edicion
    Public Empresa As eEmpresa
    Private EsNuevo As Boolean = False
    Private Sub FrmEmpresa_Edicion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Cargar()
    End Sub
    Sub Cargar()
        If Not IsNothing(Empresa) Then
            TxtEmpRUC.Tag = Empresa.Emp_Codigo
            TxtEmpRUC.Text = Empresa.Emp_NumRuc
            TxtEmpRazonSocial.Text = Empresa.Emp_NombreRazonSocial
            'ChkEnviaResumen.Checked = Empresa.Emp_EnviaResumenBoletas
            TxtSUNATUsuario.Text = Empresa.Sunat_Usuario
            TxtSUNATContrasena.Text = Empresa.Sunat_Contrasena
            TxtSUNATAutorizacion.Text = Empresa.Sunat_Autorizacion
            TxtSUNATWebConsulta.Text = Empresa.Emisor_Web_Visualizacion
            TxtCorreoUsuario.Text = Empresa.Correo_Usuario
            TxtCorreoContrasena.Text = Empresa.Correo_Contrasena
            TxtCorreoServidorSMTP.Text = Empresa.Correo_ServidorSMTP
            TxtCorreoPuerto.Text = Empresa.Correo_ServidorPuerto
            ChkCorreoSSLHabilitado.Checked = Empresa.Correo_ServidorSSL
            TxtEtapaCodigo.Text = Empresa.Etapa_Codigo
            TxtEtapaDescripcion.Text = Empresa.Etapa_Descripcion
            TxtRutaXMLCliente.Text = Empresa.Path_Root_App
            'TxtRutaAplicacion.Text = Empresa.PathLocal
            'TxtRutaWebAcceso.Text = Empresa.PathServer
            TxtFTPDireccion.Text = Empresa.FTP_Direccion
            TxtFTPCarpeta.Text = Empresa.FTP_Carpeta
            TxtFTPUsuario.Text = Empresa.FTP_Usuario
            TxtFTPContrasena.Text = Empresa.FTP_Contrasena
            ChkActualizarReceptores.Checked = Empresa.ActualizarClientes
        Else
            Empresa = New eEmpresa
            EsNuevo = True
        End If
    End Sub
    Sub CargarDatos()
        'Empresa = New eEmpresa
        Empresa.Emp_NumRuc = TxtEmpRUC.Text
        Empresa.Emp_NombreRazonSocial = TxtEmpRazonSocial.Text
        'Empresa.Emp_EnviaResumenBoletas = ChkEnviaResumen.Checked
        Empresa.Sunat_Usuario = TxtSUNATUsuario.Text
        Empresa.Sunat_Contrasena = TxtSUNATContrasena.Text
        Empresa.Sunat_Autorizacion = TxtSUNATAutorizacion.Text
        Empresa.Emisor_Web_Visualizacion = TxtSUNATWebConsulta.Text
        Empresa.Correo_Usuario = TxtCorreoUsuario.Text
        Empresa.Correo_Contrasena = TxtCorreoContrasena.Text
        Empresa.Correo_ServidorSMTP = TxtCorreoServidorSMTP.Text
        Empresa.Correo_ServidorPuerto = TxtCorreoPuerto.Text
        Empresa.Correo_ServidorSSL = ChkCorreoSSLHabilitado.Checked
        Empresa.Etapa_Codigo = TxtEtapaCodigo.Text
        Empresa.Etapa_Descripcion = TxtEtapaDescripcion.Text
        Empresa.Path_Root_App = TxtRutaXMLCliente.Text
        'Empresa.PathLocal = TxtRutaAplicacion.Text
        'Empresa.PathServer = TxtRutaWebAcceso.Text
        Empresa.FTP_Direccion = TxtFTPDireccion.Text
        Empresa.FTP_Carpeta = TxtFTPCarpeta.Text
        Empresa.FTP_Usuario = TxtFTPUsuario.Text
        Empresa.FTP_Contrasena = TxtFTPContrasena.Text
        Empresa.ActualizarClientes = ChkActualizarReceptores.Checked
    End Sub

    Private Sub BtnGuardar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnGuardar.ItemClick

        Try '--NUEVO
            IO.Directory.CreateDirectory(TxtRutaXMLCliente.Text & "/" & TxtEmpRUC.Text)
            IO.Directory.CreateDirectory(TxtRutaXMLCliente.Text & "/" & TxtEmpRUC.Text & "/xml_pos")
            IO.Directory.CreateDirectory(TxtRutaXMLCliente.Text & "/" & TxtEmpRUC.Text & "/xml_pos_attached")
            IO.Directory.CreateDirectory(TxtRutaXMLCliente.Text & "/" & TxtEmpRUC.Text & "/xml_temp")
            IO.Directory.CreateDirectory(TxtRutaXMLCliente.Text & "/" & TxtEmpRUC.Text & "/xml_generated")
            IO.Directory.CreateDirectory(TxtRutaXMLCliente.Text & "/" & TxtEmpRUC.Text & "/xml_sunat_aceptados")
            IO.Directory.CreateDirectory(TxtRutaXMLCliente.Text & "/" & TxtEmpRUC.Text & "/xml_sunat_rechazados")
        Catch ex As Exception
        End Try

        Dim Log As New Log_FACTE_EMPRESA
        CargarDatos()
        If EsNuevo Then
            If Log.Insertar(Empresa) Then




                DialogResult = Windows.Forms.DialogResult.OK
            Else
                MessageBox.Show("Error al intentar guardar")
            End If
        Else
            If Log.Actualizar(Empresa) Then
                DialogResult = Windows.Forms.DialogResult.OK
            Else
                MessageBox.Show("Error al intentar guardar")
            End If
        End If
    End Sub

    Private Sub BtnSalir_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnSalir.ItemClick
        Me.Close()
    End Sub

    Private Sub TxtEtapaCodigo_KeyDown(sender As Object, e As KeyEventArgs) Handles TxtEtapaCodigo.KeyDown
        If Not String.IsNullOrEmpty(TxtEtapaCodigo.Text.Trim) Then
            Try
                If e.KeyCode = Keys.Enter And TxtEtapaCodigo.Text.Substring(TxtEtapaCodigo.Text.Trim.Length - 1, 1) = "*" Then
                    Using f As New FrmEtapa_Busqueda
                        If f.ShowDialog = DialogResult.OK Then
                            TxtEtapaCodigo.Text = f.Generales(f.VwDgv.GetFocusedDataSourceRowIndex).Etapa_Codigo
                            TxtEtapaDescripcion.Text = f.Generales(f.VwDgv.GetFocusedDataSourceRowIndex).Etapa_Descripcion
                            SendKeys.Send("{TAB}")
                        End If
                    End Using
                ElseIf e.KeyCode = Keys.Enter And TxtEtapaCodigo.Text.Trim <> "" And TxtEtapaDescripcion.Text.Trim = "" Then
                    Try
                        TxtEtapaCodigo.Text = CodeFormat(TxtEtapaCodigo.Text, 3)
                        Dim log As New Log_FACTE_ETAPA
                        Dim Documentos As New List(Of eEtapa)
                        Documentos = log.Buscar(New eEtapa With {.Etapa_Codigo = TxtEtapaCodigo.Text})
                        If Documentos.Count > 0 Then
                            TxtEtapaDescripcion.Text = Documentos(0).Etapa_Descripcion
                        End If
                    Catch ex As Exception
                        'Herramientas.ShowError(ex.Message)
                    End Try
                End If
            Catch ex As Exception
                'Herramientas.ShowError(ex.Message)
            End Try
        End If
    End Sub

    Private Sub TxtEtapaCodigo_TextChanged(sender As Object, e As EventArgs) Handles TxtEtapaCodigo.TextChanged
        If TxtEtapaCodigo.Focus Then
            TxtEtapaDescripcion.Clear()
        End If
    End Sub
    Function CodeFormat(ByVal Id As String, ByVal Tamanio As Int32) As String
        If Id.Length < Tamanio Then
            For i = Id.Length To Tamanio - 1
                Id = "0" & Id
            Next
        End If
        Return Id
    End Function
End Class