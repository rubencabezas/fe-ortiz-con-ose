﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEmpresa_Lista_Editable
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmEmpresa_Lista_Editable))
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.BtnNuevo = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnModificar = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnEliminar = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnCertificados = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnSalir = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.BtnExportXls = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnExportPdf = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnExportRtf = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnExportHtml = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.DgvGeneral = New DevExpress.XtraGrid.GridControl()
        Me.VwDgv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.BtnProgramacionEnvios = New DevExpress.XtraBars.BarButtonItem()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DgvGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VwDgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BtnNuevo, Me.BtnExportXls, Me.BtnExportPdf, Me.BtnExportRtf, Me.BtnExportHtml, Me.BtnSalir, Me.BarButtonItem1, Me.BtnModificar, Me.BtnEliminar, Me.BtnCertificados, Me.BtnProgramacionEnvios})
        Me.BarManager1.MaxItemId = 21
        '
        'Bar1
        '
        Me.Bar1.BarName = "Herramientas"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BtnNuevo), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnModificar), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnEliminar), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnCertificados), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnProgramacionEnvios), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnSalir)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Herramientas"
        '
        'BtnNuevo
        '
        Me.BtnNuevo.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left
        Me.BtnNuevo.Caption = "Nuevo"
        Me.BtnNuevo.Glyph = CType(resources.GetObject("BtnNuevo.Glyph"), System.Drawing.Image)
        Me.BtnNuevo.Id = 1
        Me.BtnNuevo.ImageIndex = 0
        Me.BtnNuevo.Name = "BtnNuevo"
        Me.BtnNuevo.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnModificar
        '
        Me.BtnModificar.Caption = "Modificar"
        Me.BtnModificar.Glyph = CType(resources.GetObject("BtnModificar.Glyph"), System.Drawing.Image)
        Me.BtnModificar.Id = 17
        Me.BtnModificar.Name = "BtnModificar"
        Me.BtnModificar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnEliminar
        '
        Me.BtnEliminar.Caption = "Eliminar"
        Me.BtnEliminar.Glyph = CType(resources.GetObject("BtnEliminar.Glyph"), System.Drawing.Image)
        Me.BtnEliminar.Id = 18
        Me.BtnEliminar.Name = "BtnEliminar"
        Me.BtnEliminar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnCertificados
        '
        Me.BtnCertificados.Caption = "Certificados"
        Me.BtnCertificados.Glyph = CType(resources.GetObject("BtnCertificados.Glyph"), System.Drawing.Image)
        Me.BtnCertificados.Id = 19
        Me.BtnCertificados.Name = "BtnCertificados"
        Me.BtnCertificados.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnSalir
        '
        Me.BtnSalir.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.BtnSalir.Caption = "Salir"
        Me.BtnSalir.Glyph = CType(resources.GetObject("BtnSalir.Glyph"), System.Drawing.Image)
        Me.BtnSalir.Id = 10
        Me.BtnSalir.ImageIndex = 15
        Me.BtnSalir.Name = "BtnSalir"
        Me.BtnSalir.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(868, 39)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 416)
        Me.barDockControlBottom.Size = New System.Drawing.Size(868, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 39)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 377)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(868, 39)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 377)
        '
        'BtnExportXls
        '
        Me.BtnExportXls.Caption = "Excel"
        Me.BtnExportXls.Id = 4
        Me.BtnExportXls.ImageIndex = 10
        Me.BtnExportXls.Name = "BtnExportXls"
        '
        'BtnExportPdf
        '
        Me.BtnExportPdf.Caption = "Pdf"
        Me.BtnExportPdf.Id = 5
        Me.BtnExportPdf.ImageIndex = 11
        Me.BtnExportPdf.Name = "BtnExportPdf"
        '
        'BtnExportRtf
        '
        Me.BtnExportRtf.Caption = "Rtf"
        Me.BtnExportRtf.Id = 6
        Me.BtnExportRtf.ImageIndex = 12
        Me.BtnExportRtf.Name = "BtnExportRtf"
        '
        'BtnExportHtml
        '
        Me.BtnExportHtml.Caption = "Html"
        Me.BtnExportHtml.Id = 7
        Me.BtnExportHtml.ImageIndex = 13
        Me.BtnExportHtml.Name = "BtnExportHtml"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Cancelar"
        Me.BarButtonItem1.Id = 11
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'DgvGeneral
        '
        Me.DgvGeneral.Cursor = System.Windows.Forms.Cursors.Default
        Me.DgvGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DgvGeneral.Location = New System.Drawing.Point(0, 39)
        Me.DgvGeneral.MainView = Me.VwDgv
        Me.DgvGeneral.Name = "DgvGeneral"
        Me.DgvGeneral.Size = New System.Drawing.Size(868, 377)
        Me.DgvGeneral.TabIndex = 162
        Me.DgvGeneral.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.VwDgv})
        '
        'VwDgv
        '
        Me.VwDgv.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.VwDgv.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.VwDgv.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.VwDgv.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.VwDgv.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.VwDgv.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.VwDgv.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.Empty.BackColor2 = System.Drawing.Color.White
        Me.VwDgv.Appearance.Empty.Options.UseBackColor = True
        Me.VwDgv.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.VwDgv.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.EvenRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.EvenRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.VwDgv.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.VwDgv.Appearance.FilterCloseButton.Options.UseForeColor = True
        Me.VwDgv.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
        Me.VwDgv.Appearance.FilterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FilterPanel.Options.UseBackColor = True
        Me.VwDgv.Appearance.FilterPanel.Options.UseForeColor = True
        Me.VwDgv.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.VwDgv.Appearance.FixedLine.Options.UseBackColor = True
        Me.VwDgv.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
        Me.VwDgv.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.FocusedCell.Options.UseBackColor = True
        Me.VwDgv.Appearance.FocusedCell.Options.UseForeColor = True
        Me.VwDgv.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.VwDgv.Appearance.FocusedRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.FocusedRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.FooterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.FooterPanel.Options.UseBackColor = True
        Me.VwDgv.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.VwDgv.Appearance.FooterPanel.Options.UseForeColor = True
        Me.VwDgv.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.GroupButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.GroupButton.Options.UseBackColor = True
        Me.VwDgv.Appearance.GroupButton.Options.UseBorderColor = True
        Me.VwDgv.Appearance.GroupButton.Options.UseForeColor = True
        Me.VwDgv.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.GroupFooter.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.GroupFooter.Options.UseBackColor = True
        Me.VwDgv.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.VwDgv.Appearance.GroupFooter.Options.UseForeColor = True
        Me.VwDgv.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
        Me.VwDgv.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.GroupPanel.Options.UseBackColor = True
        Me.VwDgv.Appearance.GroupPanel.Options.UseForeColor = True
        Me.VwDgv.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.GroupRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.GroupRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.GroupRow.Options.UseBorderColor = True
        Me.VwDgv.Appearance.GroupRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.VwDgv.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.VwDgv.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.VwDgv.Appearance.HeaderPanel.Options.UseForeColor = True
        Me.VwDgv.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(195, Byte), Integer))
        Me.VwDgv.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.VwDgv.Appearance.HorzLine.Options.UseBackColor = True
        Me.VwDgv.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.VwDgv.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.OddRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.OddRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.VwDgv.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
        Me.VwDgv.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.VwDgv.Appearance.Preview.Options.UseBackColor = True
        Me.VwDgv.Appearance.Preview.Options.UseFont = True
        Me.VwDgv.Appearance.Preview.Options.UseForeColor = True
        Me.VwDgv.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.VwDgv.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.VwDgv.Appearance.Row.Options.UseBackColor = True
        Me.VwDgv.Appearance.Row.Options.UseForeColor = True
        Me.VwDgv.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
        Me.VwDgv.Appearance.RowSeparator.Options.UseBackColor = True
        Me.VwDgv.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(107, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(179, Byte), Integer))
        Me.VwDgv.Appearance.SelectedRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.VwDgv.Appearance.SelectedRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.SelectedRow.Options.UseForeColor = True
        Me.VwDgv.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
        Me.VwDgv.Appearance.TopNewRow.Options.UseBackColor = True
        Me.VwDgv.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.VwDgv.Appearance.VertLine.Options.UseBackColor = True
        Me.VwDgv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn3, Me.GridColumn2, Me.GridColumn1, Me.GridColumn4, Me.GridColumn5})
        Me.VwDgv.GridControl = Me.DgvGeneral
        Me.VwDgv.Name = "VwDgv"
        Me.VwDgv.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.VwDgv.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.VwDgv.OptionsBehavior.Editable = False
        Me.VwDgv.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.VwDgv.OptionsView.EnableAppearanceEvenRow = True
        Me.VwDgv.OptionsView.EnableAppearanceOddRow = True
        Me.VwDgv.OptionsView.ShowAutoFilterRow = True
        Me.VwDgv.OptionsView.ShowGroupPanel = False
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "RUC"
        Me.GridColumn3.FieldName = "Emp_NumRuc"
        Me.GridColumn3.MaxWidth = 82
        Me.GridColumn3.MinWidth = 82
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 0
        Me.GridColumn3.Width = 82
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Descripción"
        Me.GridColumn2.FieldName = "Emp_NombreRazonSocial"
        Me.GridColumn2.MinWidth = 300
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsColumn.AllowEdit = False
        Me.GridColumn2.OptionsColumn.FixedWidth = True
        Me.GridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 20
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Envia Resumen Boletas"
        Me.GridColumn1.FieldName = "Emp_EnviaResumenBoletas"
        Me.GridColumn1.MaxWidth = 80
        Me.GridColumn1.MinWidth = 80
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 2
        Me.GridColumn1.Width = 80
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Etapa"
        Me.GridColumn4.FieldName = "Etapa_Descripcion"
        Me.GridColumn4.MaxWidth = 100
        Me.GridColumn4.MinWidth = 100
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 3
        Me.GridColumn4.Width = 100
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Actualizar Clientes"
        Me.GridColumn5.FieldName = "ActualizarClientes"
        Me.GridColumn5.MaxWidth = 80
        Me.GridColumn5.MinWidth = 80
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 4
        Me.GridColumn5.Width = 80
        '
        'BtnProgramacionEnvios
        '
        Me.BtnProgramacionEnvios.Caption = "Programación de Envíos"
        Me.BtnProgramacionEnvios.Glyph = CType(resources.GetObject("BtnProgramacionEnvios.Glyph"), System.Drawing.Image)
        Me.BtnProgramacionEnvios.Id = 20
        Me.BtnProgramacionEnvios.Name = "BtnProgramacionEnvios"
        Me.BtnProgramacionEnvios.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'FrmEmpresa_Lista_Editable
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(868, 416)
        Me.Controls.Add(Me.DgvGeneral)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "FrmEmpresa_Lista_Editable"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Empresas"
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DgvGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VwDgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BtnNuevo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnSalir As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BtnExportXls As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnExportPdf As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnExportRtf As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnExportHtml As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents DgvGeneral As DevExpress.XtraGrid.GridControl
    Public WithEvents VwDgv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BtnModificar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnEliminar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnCertificados As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnProgramacionEnvios As DevExpress.XtraBars.BarButtonItem
End Class
