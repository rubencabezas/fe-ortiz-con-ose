﻿Imports UblLarsen.Ubl2.Udt
Imports System.Xml
Imports System.Xml.Serialization
Imports UblLarsen.Ubl2.Ext
Imports UblLarsen.Ubl2.Cac
Imports UblLarsen.Ubl2.Qdt
Imports System.Security.Cryptography.X509Certificates
Imports System.Security.Cryptography.Xml
Imports System.IO.Compression
Imports System.IO
Imports System.ServiceModel
Imports System.ServiceModel.Channels
Imports System.ServiceModel.Security
Imports System.Collections

Public Class GestorFacturacionElectronica

#Region "DATOS DBBMS"
    Dim DireccionDescripcion As String = ""
    Dim Codigo_Autorizacion_Sunat, Elect_Documento_Sunat, Elect_Documento_Descripcion, Tipo_Doc_SUNAT_Cliente As String
    Function Generar_XMLPos_Provision(ByVal Provision As eProvisionFacturacion)
        'Dim LogNP As New Log_VTS_DOC_TRANSAC_CAB
        'Dim DocFiltro As New Ent_DOC_TRANSAC_CAB
        'Dim Venta As New Ent_DOC_TRANSAC_CAB
        'Cargar_Empresa(Provision)
        'If Provision.TpDc_Codigo_St.Trim = "01" Or Provision.TpDc_Codigo_St.Trim = "03" Then 'BOLETA O FACTURA

        '    DocFiltro.MyOrigen = "20"
        '    DocFiltro.Dvt_VTFecha = Provision.Dvt_Fecha_Emision
        '    DocFiltro.TpDc_Codigo_St = Provision.TpDc_Codigo_St.Trim
        '    DocFiltro.Pan_cAnio = (Provision.Dvt_Fecha_Emision.Year).ToString.Trim
        '    DocFiltro.Per_cPeriodo = String.Format("{0:00}", Provision.Dvt_Fecha_Emision.Month).Trim
        '    DocFiltro.Dvt_VTSerie = Provision.Dvt_VTSerie.Trim
        '    DocFiltro.Dvt_VTNumer = Provision.Dvt_VTNumer.Trim
        '    DocFiltro.Vnt_Es_Electronico = True

        '    Venta = LogNP.Buscar_DocVenta(DocFiltro)(0)
        '    Venta.Dvt_Detalle = LogNP.Buscar_Detalle_DocVenta(Venta)
        '    Venta.Dvt_Medios_Pago = LogNP.Buscar_Detalle_MediosPago(Venta)
        'ElseIf Provision.TpDc_Codigo_St.Trim = "08" Then 'NOTA DE DEBITO
        '    DocFiltro.MyOrigen = "21"
        '    DocFiltro.Dvt_VTFecha = Provision.Dvt_Fecha_Emision
        '    DocFiltro.TpDc_Codigo_St = Provision.TpDc_Codigo_St.Trim
        '    DocFiltro.Pan_cAnio = (Provision.Dvt_Fecha_Emision.Year).ToString.Trim
        '    DocFiltro.Per_cPeriodo = String.Format("{0:00}", Provision.Dvt_Fecha_Emision.Month).Trim
        '    DocFiltro.Dvt_VTSerie = Provision.Dvt_VTSerie.Trim
        '    DocFiltro.Dvt_VTNumer = Provision.Dvt_VTNumer.Trim
        '    DocFiltro.Vnt_Es_Electronico = True
        '    DocFiltro.SoloVenta = False
        '    DocFiltro.IncluirAniosAnteriores = False
        '    Venta = LogNP.Buscar_NotaDebito(DocFiltro)(0)
        '    Venta.Dvt_Detalle = LogNP.Buscar_Detalle_DocVenta(Venta)
        '    Venta.Dvt_Medios_Pago = LogNP.Buscar_Detalle_MediosPago(Venta)
        'ElseIf Provision.TpDc_Codigo_St.Trim = "07" Then 'NOTA DE CREDITO
        '    DocFiltro.MyOrigen = "22"
        '    DocFiltro.Dvt_VTFecha = Provision.Dvt_Fecha_Emision
        '    DocFiltro.TpDc_Codigo_St = Provision.TpDc_Codigo_St.Trim
        '    DocFiltro.Pan_cAnio = (Provision.Dvt_Fecha_Emision.Year).ToString.Trim
        '    DocFiltro.Per_cPeriodo = String.Format("{0:00}", Provision.Dvt_Fecha_Emision.Month).Trim
        '    DocFiltro.Dvt_VTSerie = Provision.Dvt_VTSerie.Trim
        '    DocFiltro.Dvt_VTNumer = Provision.Dvt_VTNumer.Trim
        '    DocFiltro.Vnt_Es_Electronico = True
        '    DocFiltro.SoloVenta = False
        '    DocFiltro.IncluirAniosAnteriores = False
        '    Venta = LogNP.Buscar_NotaCredito(DocFiltro)(0)
        '    Venta.Dvt_Detalle = LogNP.Buscar_Detalle_DocVenta(Venta)
        '    Venta.Dvt_Medios_Pago = LogNP.Buscar_Detalle_MediosPago(Venta)
        'End If

        'If Venta.Ent_cCodEntidad.Trim <> "0000000006" Then
        '    Dim LogDirec As New Log_VTS_ENTIDAD
        '    Dim MisDirecciones As New List(Of Ent_ENTIDAD_DIRECCION)
        '    MisDirecciones = LogDirec.Buscar(New Ent_ENTIDAD_DIRECCION With {.Ent_cDNIRUC = Venta.Ent_DNIRUC.Trim,
        '                                                               .Ten_cTipoEntidad = "C", .Dir_Codigo = Venta.Dir_Codigo.Trim})
        '    If MisDirecciones.Count > 0 Then
        '        DireccionDescripcion = MisDirecciones.Item(0).Dir_Descripcion
        '    End If
        'Else
        '    DireccionDescripcion = ""
        'End If

        'Generar_Comprobante_Electronico(Venta, Provision)
        Dim Root_File = Provision.RootPath_Enterprise.Trim & "\xml_pos\" & Provision.SQL_XML_Nombre.Trim
        If IO.File.Exists(Root_File) Then
            Dim document As New XmlDocument
            document.Load((Root_File))
            'Convertimos el XML en Objeto
            Dim Provi As New DocumentoElectronico
            Dim reader As New System.Xml.Serialization.XmlSerializer(Provi.[GetType]())
            Dim reader2 As XmlNodeReader = New XmlNodeReader(document)
            Provi = DirectCast(reader.Deserialize(reader2), DocumentoElectronico)
            Provi.Print_DigestValue = Provision.DigestValue
            Provi.Print_BarCode = Provi.Print_BarCode_Armar
            Provision.XML_POS = Provi
        End If
        Return Provision

    End Function
    Function Generar_Comprobante_Electronico(Doc As Ent_DOC_TRANSAC_CAB, Provision As eProvisionFacturacion)

        Codigo_Autorizacion_Sunat = Doc.Vnt_Cod_Autorizacion_Sunat
        Elect_Documento_Sunat = Doc.TpDc_Codigo_St.Trim
        Elect_Documento_Descripcion = Doc.TpDc_Descripcion.Trim
        Tipo_Doc_SUNAT_Cliente = Doc.Ent_TpDcEnt_SUNAT
        Dim Modelo_Impresion As String = "000"
        Dim Doc_Electronico_Bol_Fact As DocumentoElectronico
        Doc_Electronico_Bol_Fact = Convertir_Cliente_Objet(Doc)
        Provision.XML_POS = Doc_Electronico_Bol_Fact
        Return Provision
    End Function

    Function Convertir_Cliente_Objet(Documento_Cliente As Ent_DOC_TRANSAC_CAB) As DocumentoElectronico

        Dim log_ As New Log_Establecimiento
        Dim Ent_ As New Ent_Establecimiento

        Ent_.Emp_cCodigo = DatosActualConexion.CodigoEmpresa
        Ent_.Est_PDV = Documento_Cliente.Pdv_Codigo
        Dim List As List(Of Ent_Establecimiento) = log_.Buscar_Direccion(Ent_)
        Dim Doc As New DocumentoElectronico

        If List.Count > 0 Then
            Ent_ = List(0)
            Doc.Emisor_Establecimiento = "Establecimiento: " & Ent_.Est_Nombre.Trim
            Doc.Emisor_Establecimiento_Direccion = Ent_.Est_Direccion.Trim
        End If
        Doc.Fecha_Emision = Documento_Cliente.Dvt_VTFecha
        Doc.Hora_Emision = IIf(String.IsNullOrWhiteSpace(Documento_Cliente.Dvt_AtencHora), Now, Documento_Cliente.Dvt_AtencHora)
        Doc.Emisor_ApellidosNombres_RazonSocial = DatosActualConexion.EmpresaRazonSocial
        Doc.Emisor_Documento_Tipo = "6"
        Doc.Emisor_Documento_Numero = DatosActualConexion.EmpresaRuc.Trim
        '----------------------------

        '--- Domicilio fiscal -- PostalAddress
        Doc.Emisor_Direccion_Ubigeo = "020101"
        Doc.Emisor_Direccion_Calle = DatosActualConexion.Emp_DireccionCorta
        Doc.Emisor_Direccion_Urbanizacion = DatosActualConexion.Emp_Urbanizacion
        Doc.Emisor_Direccion_Departamento = DatosActualConexion.Emp_Direccion_Departamento
        Doc.Emisor_Direccion_Provincia = DatosActualConexion.Emp_Direccion_Provincia
        Doc.Emisor_Direccion_Distrito = DatosActualConexion.Emp_Direccion_Distrito
        Doc.Emisor_Direccion_PaisCodigo = "PE"
        '---DATOS DEL ESTABLECIMIENTO
        Doc.Fecha_Vencimiento = Documento_Cliente.Dvt_FechaVenci
        Doc.TipoDocumento_Codigo = Elect_Documento_Sunat.Trim '"01" ' Si es Boleta o Factura segun Sunat ==>
        Doc.TipoDocumento_Serie = Documento_Cliente.Dvt_VTSerie.Trim
        Doc.TipoDocumento_Numero = Documento_Cliente.Dvt_VTNumer.Trim
        Doc.TipoDocumento_Descripcion = Elect_Documento_Descripcion & " ELECTRÓNICA" 'BSITipoDocumento.Caption 

        Doc.Cliente_Documento_Tipo = Tipo_Doc_SUNAT_Cliente.Trim '"6" 'Si es DNI o RUC segun sunat
        Doc.Cliente_Documento_Numero = IIf(Documento_Cliente.Ent_DNIRUC = "00000001", "-", Documento_Cliente.Ent_DNIRUC.Trim)
        Doc.Cliente_RazonSocial_Nombre = Documento_Cliente.Ent_NombreCompleto
        Doc.Cliente_Direccion = DireccionDescripcion
        Doc.Numero_Placa_del_Vehiculo = Documento_Cliente.Vnt_placa_Vehiculo
        Doc.Sunat_Autorizacion = Documento_Cliente.Vnt_Cod_Autorizacion_Sunat
        Doc.Cajero_Nombre = Documento_Cliente.Dvt_VenEnt_NombreCompleto
        Doc.Forma_De_Pago = Documento_Cliente.Con_cDescripcion
        Doc.Observaciones = Documento_Cliente.Dvt_Observaciones

        'Doc.Detalles = New DocumentoElectronicoDetalle() {New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "Cap-258963", .Producto_Descripcion = "CAPTOPRIL 25mg X 30", .Producto_UnidadMedida_Codigo = "ZZ", .Producto_Cantidad = 2, .Unitario_Precio_Venta = 180, .Unitario_Precio_Venta_Tipo_Codigo = "01", .Item_IGV_Total = 32.4, .Unitario_IGV_Porcentaje = 18, .Item_Tipo_Afectacion_IGV_Codigo = "13", .Item_ISC_Total = 0, .Unitario_Valor_Unitario = 147.6, .Item_ValorVenta_Total = 295.2}}
        Dim feDetalles As New List(Of DocumentoElectronicoDetalle)
        Dim DetalleVenta As New List(Of Ent_DOC_VENTA_DET)
        DetalleVenta = Agrupar_Detalles(Documento_Cliente.Dvt_Detalle)


        For Each bf In DetalleVenta
            Dim det As New DocumentoElectronicoDetalle With {.Item = bf.DvtD_NumItem,
                                                             .Producto_Codigo = bf.CTC_Codigo,
                                                             .Producto_Descripcion = (bf.CTC_DescripcionItem & " " & IIf((bf.CTC_DescripcionAdicional Is Nothing Or String.IsNullOrWhiteSpace(bf.CTC_DescripcionAdicional)), "", bf.CTC_DescripcionAdicional.Trim & " ")).ToString.Trim & IIf(bf.DvtD_TipoCod = "1012", "", " x " & bf.Unm_Descripcion),
                                                             .Producto_UnidadMedida_Codigo = bf.UNM_CoigodFE,
                                                             .Producto_Cantidad = bf.DvtD_Cantidad,
                                                             .Unitario_Precio_Venta = bf.DvtD_PrecUnitario,
                                                             .Unitario_Precio_Venta_Tipo_Codigo = "01",
                                                             .Item_IGV_Total = bf.DvtD_IGVMonto_Cabecera,
                                                             .Unitario_IGV_Porcentaje = bf.DvtD_IGVTasaPorcentaje,
                                                             .Item_Tipo_Afectacion_IGV_Codigo = "13",
                                                             .Item_ISC_Total = 0,
                                                             .Unitario_Valor_Unitario = bf.DvtD_ValorVenta,
                                                             .Item_ValorVenta_Total = bf.DvtD_TotalValorVenta,
                                                             .Item_Total = bf.DvtD_MontoTotal}
            feDetalles.Add(det)
        Next

        Doc.Detalles = feDetalles.ToArray
        Doc.Total_ValorVenta_OperacionesGravadas = Documento_Cliente.Dvt_MntoBaseImponible
        Doc.Total_ValorVenta_OperacionesInafectas = Documento_Cliente.Dvt_MntoInafecta
        Doc.Total_ValorVenta_OperacionesExoneradas = Documento_Cliente.Dvt_MntoExonerado
        Doc.Total_ValorVenta_OperacionesGratuitas = 0
        Doc.Total_IGV = Documento_Cliente.Dvt_MntoIGV
        Doc.IGV_Porcentaje = Documento_Cliente.Dvt_PrcIGV
        Doc.Total_ISC = Documento_Cliente.Dvt_MntoISC
        Doc.Total_OtrosTributos = 0
        Doc.Total_OtrosCargos = 0
        'Doc.Descuentos_Globales = 0
        Doc.Total_Importe_Venta = Documento_Cliente.Dvt_MntoTotal

        Doc.Moneda_Codigo = "PEN"
        Doc.Moneda_Descripcion = "SOLES"
        Doc.Moneda_Simbolo = "S/"

        Dim log As New Log_VTS_MONEDA
        Dim Monedas As New List(Of Ent_MONEDA)
        Monedas = log.Buscar(New Ent_MONEDA With {.Opcion = 1})
        If Monedas.Count > 0 Then
            For Each T In Monedas
                If (T.Mon_cCodigo).Trim.ToUpper = Documento_Cliente.Dvt_Cod_Moneda.Trim.ToUpper Then
                    Doc.Moneda_Codigo = T.Mon_Sunat_CodigoNuevo.Trim
                    Doc.Moneda_Descripcion = T.Mon_cNombreLargo.Trim
                    Doc.Moneda_Simbolo = T.Mon_cNombreCorto.Trim
                    Exit For
                End If
            Next
        End If
        Doc.Total_Importe_Venta_Texto = New Imprimir_Letras().NroEnLetras(Doc.Total_Importe_Venta) & " " & Doc.Moneda_Descripcion
        Doc.Impresion_Modelo_Codigo = Documento_Cliente.Vnt_Modelo_Impresion
        'Venta percepcion
        Doc.Tipo_Operacion_Codigo = Nothing 'Cat 17 --05 Venta itinerante, 06 factura guia, 08 factura comprobante percepcion , Venta interna, exportacion, no domiciliados, etc
        Doc.Total_Importe_Percepcion = 0
        Doc.Percep_Importe_Total_Cobrado = 0

        Doc.DocumentoRelacionados = New DocumentoElectronicoReferenciaDocumentos() {New DocumentoElectronicoReferenciaDocumentos With {.Documento_Tipo = "09", .Documento_Serie = "0001", .Documento_Numero = "00000009"}}

        '   45 Leyendas
        Dim ListaLeyendas As New List(Of DocumentoElectronicoLeyenda)
        ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "1000", .Descripcion = New Imprimir_Letras().NroEnLetras(Math.Truncate(Doc.Total_Importe_Venta)) & " " & Doc.Moneda_Descripcion})
        If Doc.Total_ValorVenta_OperacionesGratuitas > 0 Then
            ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "1002", .Descripcion = "TRANSFERENCIA GRATUITA DE UN BIEN Y/O SERVICIO PRESTADO GRATUITAMENTE"})
        End If
        If Doc.Total_Importe_Percepcion > 0 Then
            ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "2000", .Descripcion = "COMPROBANTE DE PERCEPCION"})
        End If
        If Not String.IsNullOrEmpty(Doc.Tipo_Operacion_Codigo) Then
            If Doc.Tipo_Operacion_Codigo = "05" Then
                ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "2005", .Descripcion = "VENTA REALIZADA POR EMISOR ITINERANTE"})
            End If
        End If

        Doc.Leyendas = ListaLeyendas.ToArray

        Dim feDetallesPago As New List(Of DocumentoElectronicoPagoTarjeta)
        For Each bf In Documento_Cliente.Dvt_Medios_Pago
            Dim det As New DocumentoElectronicoPagoTarjeta With {.TarjetaDescripcion = bf.TarjetaDescripcion, .TarjetaImporte = bf.MedioImporte, .TarjetaLote = bf.TarjetaLote, .TarjetaNumero = bf.TarjetaNumero}
            feDetallesPago.Add(det)
        Next
        Doc.PagoTarjeta = feDetallesPago.ToArray

        Return Doc
    End Function
    Function Agrupar_Detalles(ByVal Detalle As List(Of Ent_DOC_VENTA_DET)) As List(Of Ent_DOC_VENTA_DET)
        Dim ListaDet As New List(Of Ent_DOC_VENTA_DET)
        Dim PrivView = From item In Detalle Group item By item.DvtD_OperCodigo, item.DvtD_TipoCod, item.CTC_Codigo, item.CTC_DescripcionItem,
                       item.CTC_DescripcionAdicional, item.UNM_Descripcion, item.UNM_CoigodFE, item.DvtD_IGVTasaPorcentaje, item.DvtD_ValorVenta
        Into Cantidad = Sum(item.DvtD_Cantidad) Order By DvtD_TipoCod, CTC_Codigo
                           Select DvtD_OperCodigo, DvtD_TipoCod, CTC_Codigo, CTC_DescripcionItem, CTC_DescripcionAdicional, UNM_Descripcion,
                           UNM_CoigodFE, DvtD_IGVTasaPorcentaje, DvtD_ValorVenta, Cantidad

        ListaDet.Clear()
        For Each T In PrivView.ToArray
            Dim Det As New Ent_DOC_VENTA_DET With {.DvtD_OperCodigo = T.DvtD_OperCodigo, .DvtD_TipoCod = T.DvtD_TipoCod,
                                                   .CTC_Codigo = T.CTC_Codigo, .CTC_DescripcionItem = T.CTC_DescripcionItem, .CTC_DescripcionAdicional = T.CTC_DescripcionAdicional,
                                                   .UNM_Descripcion = T.UNM_Descripcion, .UNM_CoigodFE = T.UNM_CoigodFE, .DvtD_IGVTasaPorcentaje = T.DvtD_IGVTasaPorcentaje,
                                                   .DvtD_ValorVenta = T.DvtD_ValorVenta, .DvtD_Cantidad = T.Cantidad
}

            ListaDet.Add(Det)
        Next

        For i = 0 To ListaDet.Count - 1
            ListaDet.Item(i).DvtD_NumItem = (i + 1)
        Next
        Return ListaDet
    End Function
    Sub Cargar_Empresa(ByVal Provision As eProvisionFacturacion)
        Dim LogEmp As New Log_EmpresaBMS
        Dim x As New Ent_Empresa With {.User = "adm_power"}
        Dim Emp As New List(Of Ent_Empresa)
        Emp.Clear()
        Emp = LogEmp.Listar(x)

        For Each i In Emp
            If i.Ruc.Trim = Provision.Emisor_RUC.Trim Then
                DatosActualConexion.CodigoEmpresa = i.Codigo
                DatosActualConexion.EmpresaRuc = i.Ruc.Trim
                DatosActualConexion.EmpresaRazonSocial = i.Nombre.Trim
                DatosActualConexion.DireccionEmpresa = i.Direccion.Trim
                'DatosActualConexion.Codigo_Autorizacion_Sunat = i.Cod_Autorizacion

                DatosActualConexion.Emp_DireccionCorta = i.Emp_DireccionCorta
                DatosActualConexion.Emp_Urbanizacion = i.Emp_Urbanizacion
                DatosActualConexion.Emp_Direccion_Departamento = i.Emp_Direccion_Departamento
                DatosActualConexion.Emp_Direccion_Provincia = i.Emp_Direccion_Provincia
                DatosActualConexion.Emp_Direccion_Distrito = i.Emp_Direccion_Distrito
                Exit For
            End If
        Next
    End Sub
    Private Function MdiNoDuplicate(ByVal vcNombre As String) As Boolean
        If Me.MdiChildren.Count() > 0 Then
            For Each frm As Form In Me.MdiChildren
                If frm.Name = vcNombre Then
                    frm.Activate()
                    Return True
                End If
            Next
        End If

        Return False
    End Function
#End Region
    Private Sub GestorFacturacionElectronica_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        log.Instancia.datosInicio("\loggueo", Application.StartupPath)
        'Cargamos las URL por de los servicios
        'Dim logDat As New Log_FACTE_DOCUMENTO_TIPO_GRUPO_URL
        'URLsSUNAT.URls = logDat.Buscar()
    End Sub
    Private Sub BtnDocumentos_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnDocumentos.ItemClick
            Dim f As New FrmDocumento_Busqueda
            f.MdiParent = Me
        f.Show()
        f.Activate()
    End Sub

    Private Sub BtnReceptores_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnReceptores.ItemClick
        Dim f As New FrmReceptores_Busqueda
        f.MdiParent = Me
        f.Show()
        f.Activate()
    End Sub

    Private Sub BtnEmpresas_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnEmpresas.ItemClick
        Dim f As New FrmEmpresa_Lista_Editable
        f.MdiParent = Me
        f.Show()
        f.Activate()
    End Sub
    Private Sub BtnSocNegocios_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnSocNegocios.ItemClick
        Dim f As New FrmSociosNegocio_Busqueda
        f.MdiParent = Me
        f.Show()
        f.Activate()
        'Dim Her As New HerramientasFE
        'Dim hot As Boolean = Her.InternetConnection()
    End Sub

    Private ListaTareasProgramadas As New List(Of eProvisionFacturacion)
    Private ActualizandoTareas As Boolean
    Private EjecutandoTareas As Boolean = False
    Private Ejecutando_Envio_Resumen As Boolean = False
    Private Sub TimerTareasProgramadasActualizar_Tick(sender As Object, e As EventArgs) Handles TimerBillTareasProgramadasActualizar.Tick

        If Not EjecutandoTareas Then
            Dim thActualizarTareas As New Threading.Thread(AddressOf ListarTareasProgramadas)
            thActualizarTareas.Start()
        End If
    End Sub
    Public Function SoloNumeros(ByVal strCadena As String) As Object
        Dim SoloNumero As String = ""
        Dim index As Integer
        For index = 1 To Len(strCadena)
            If (Mid$(strCadena, index, 1) Like "#") _
                Or Mid$(strCadena, index, 1) = "-" Then
                SoloNumero = SoloNumero & Mid$(strCadena, index, 1)
            End If
        Next
        Return SoloNumero
    End Function
    Sub ListarTareasProgramadas()
        Try
            EjecutandoTareas = True
            ActualizandoTareas = True
            Dim logProgramados As New Log_FACTE_PROGRAMACIONES_ENVIO
            ListaTareasProgramadas.Clear()
            ListaTareasProgramadas = logProgramados.Buscar_Lista_Habilitados()
            ActualizandoTareas = False

            'Ejecutamos las tareas
            EnviarDesdeTareasProgramadas()
            EjecutandoTareas = False
        Catch ex As Exception
            'MessageBox.Show(ex.Message)
            EjecutandoTareas = False
            'Application.Restart()
        End Try

    End Sub
    Dim CertificadoDigital As eCertificadoCredencial
    Function BuscarCredenciales(pProvision As eProvisionFacturacion) As eCertificadoCredencial
        Dim LogCertificado As New Log_CERTIFICADO
        Return LogCertificado.Buscar(pProvision.Emisor_RUC, pProvision.Dvt_Fecha_Emision, Nothing)
    End Function
    Sub EnviarDesdeTareasProgramadas()

        Try
            If Not ActualizandoTareas Then
                Dim Her As New HerramientasFE
                If ListaTareasProgramadas.Count > 0 And Her.InternetConnection() Then
                    If IsNothing(CertificadoDigital) Then
                        CertificadoDigital = BuscarCredenciales(ListaTareasProgramadas(0))
                    End If
                    If CertificadoDigital.Emisor_RUC <> ListaTareasProgramadas(0).Emisor_RUC Then
                        CertificadoDigital = BuscarCredenciales(ListaTareasProgramadas(0))
                    End If

                    Dim Provision As eProvisionFacturacion = ListaTareasProgramadas(0)
                    Provision.CertificadoCredencial = CertificadoDigital

                    Dim nProceso As New fcProceso
                    'nProceso.
                    nProceso.Emisor_RUC = Provision.Emisor_RUC
                    nProceso.SUNAT_Usuario = Provision.CertificadoCredencial.Sunat_Usuario
                    nProceso.SUNAT_Contrasena = Provision.CertificadoCredencial.Sunat_Contrasena

                    'nProceso.ServidorRutaLocal = 0

                    nProceso.ServidorRutaLocal = Provision.CertificadoCredencial.Path_Root_App & "\" & Provision.Emisor_RUC

                    nProceso.DocumentoTipo = Provision.TpDc_Codigo_St
                    nProceso.FTP_Direccion = Provision.CertificadoCredencial.FTP_Direccion
                    nProceso.FTP_Carpeta = Provision.CertificadoCredencial.FTP_Carpeta
                    nProceso.FTP_Usuario = Provision.CertificadoCredencial.FTP_Usuario
                    nProceso.FTP_Contrasena = Provision.CertificadoCredencial.FTP_Contrasena

                    nProceso.Etapa = Provision.Etapa_Codigo
                    nProceso.Es_OSE = Provision.CertificadoCredencial.Es_OSE
                    'nProceso.FileNameWithOutExtension = Provision.Nombre_XML
                    nProceso.FileNameWithOutExtension = Path.GetFileNameWithoutExtension(Provision.Nombre_XML)
                    Dim Rspta As New eSunatRespuesta

                    If nProceso.DocumentoTipo = SUNATDocumento.Boleta Or nProceso.DocumentoTipo = SUNATDocumento.Factura Or nProceso.DocumentoTipo = SUNATDocumento.NotaCredito Or nProceso.DocumentoTipo = SUNATDocumento.NotaDebito Then
                        Rspta = ZIPEnviar_BoletaVentaFactura_NotaCreditoNotaDebito_Bill_Alta(nProceso)
                    ElseIf nProceso.DocumentoTipo = SUNATDocumento.ComprobanteRetencion Or nProceso.DocumentoTipo = SUNATDocumento.ComprobantePercepcion Then
                        'Rspta = ZIPEnviar_RetencionPercepcion(nProceso)
                    ElseIf nProceso.DocumentoTipo = SUNATDocumento.GuiaRemisionRemitente Then
                        'Rspta = ZIPEnviar_GuiaRemision_Bill_Alta(nProceso)
                    End If
                    If Rspta.Estado = EstadoDocumento.Aceptado Then

                        'RptaSUNAT.Codigo = childNodes.ItemOf(1).InnerText
                        'RptaSUNAT.Mensaje = childNodes.ItemOf(2).InnerText

                        Provision.Sunat_Respuesta_Codigo = Rspta.Codigo
                        Provision.Sunat_Respuesta_Descripcion = Rspta.Mensaje
                        Provision.Sunat_Respuesta_Fecha = Rspta.Fecha_Respuesta

                        Provision.EstadoElectronico = "4107" 'BMS
                        Provision.Vnt_EstadoEnvio = "003" 'FACTE
                        Provision.Nombre_PDF = nProceso.FileNamePDF
                        Provision.RootPath_Enterprise = nProceso.ServidorRutaLocal
                        'Cambiamos el estado al documento
                        Dim LogAcep As New Log_FACTE_DOCUMENTOS
                        LogAcep.Documento_CambiarEstado(Provision)
                        'Genera PDF
                        Provision = Generar_XMLPos_Provision(Provision)

                        GenerarPDF(Provision, nProceso)
                        'Copiamos a la carpeta aceptados
                        XMLCopiarAceptado(nProceso)
                        'Enviamos el correo al cliente
                        If HerramientasFE.EnviarCorreo(Provision, nProceso) Then
                            'actualizamos que se envio el correo al cliente
                            Try
                                LogAcep.Actualizar_Envio_Correo(Provision)
                            Catch ex As Exception

                            End Try

                        End If
                    End If
                    If Rspta.Estado = EstadoDocumento.Rechazado Then

                        Provision.Sunat_Respuesta_Codigo = Rspta.Codigo
                        Provision.Sunat_Respuesta_Descripcion = Rspta.Mensaje
                        Provision.Nombre_PDF = nProceso.FileNamePDF
                        Provision.EstadoElectronico = "4108"
                        Provision.Vnt_EstadoEnvio = "004" 'FACTE
                        Dim LogRech As New Log_FACTE_DOCUMENTOS
                        LogRech.Documento_CambiarEstado(Provision)
                        XMLCopiarRechazado(nProceso)
                        'Eliminamos de la lista
                        'Lista_Bill_Pendientes_BoletaVentaFactura_NotaCreditoNotaDebito.Remove(Provision)
                    End If

                End If
            End If
        Catch ex As Exception
            EjecutandoTareas = False
            'MessageBox.Show(ex.Message)
        End Try
    End Sub
    Sub GenerarPDF(Provision2 As eProvisionFacturacion, nProceso As fcProceso)
        Try
            Dim GenPDF As New HerramientasFE

            GenPDF.GenerarPDF(Provision2.XML_POS, nProceso)
            ''Generando PDF
            ''Dim FirmaImpresion As String = "RUC_EMISOR | TIPO DE COMPROBANTE PAGO | SERIE | NUMERO | SUMATORIA IGV | MONTO TOTAL DEL COMPROBANTE | FECHA DE EMISION | TIPO DE DOCUMENTO ADQUIRENTE | NUMERO DE DOCUMENTO ADQUIRENTE | VALOR RESUMEN (DigestValue) | VALOR DE LA FIRMA (SignatureValue) |"
            ''FirmaImpresion = String.Format("{0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} | {10}|", Provision2.XML_POS.Emisor_Documento_Numero, Provision2.XML_POS.TipoDocumento_Codigo, Provision2.XML_POS.TipoDocumento_Serie, Provision2.XML_POS.TipoDocumento_Numero, Format(Provision2.XML_POS.Total_IGV, "#0.00"), Format(Provision2.XML_POS.Total_Importe_Venta, "#0.00"), Provision2.XML_POS.Fecha_Emision.ToShortDateString(), Provision2.XML_POS.Cliente_Documento_Tipo, Provision2.XML_POS.Cliente_Documento_Numero, Provision2.CertificadoCredencial.Firma_DigestValue, Provision2.CertificadoCredencial.Firma_SignatureValue)
            ''Provision.Vnt_Cert_Firma_PDF417 = FirmaImpresion

            ' ''Provision.TpDc_Descripcion = Provision.TpDc_Descripcion & " ELECTRONICA"

            ''Dim Rpte As New ImprFacte_A5_FacturaElectronica
            ' ''Para que se autoajuste al tamaño del papel
            ''Rpte.PrintingSystem.Document.AutoFitToPagesWidth = 1

            ''Dim NotasAdicionales As String = ""
            ''If Provision.XML_POS.Fecha_Emision = Provision.XML_POS.Fecha_Vencimiento Then
            ''    'Contado - No mostramos nada
            ''    Rpte.XrLabel16.Visible = False
            ''    Rpte.XrLabel43.Visible = False

            ''    Rpte.XrLabel18.LocationF = Rpte.XrLabel17.LocationF
            ''    Rpte.XrLabel9.LocationF = Rpte.XrLabel44.LocationF

            ''    Rpte.XrLabel17.LocationF = Rpte.XrLabel16.LocationF
            ''    Rpte.XrLabel44.LocationF = Rpte.XrLabel43.LocationF
            ''Else
            ''    NotasAdicionales = NotasAdicionales & "- EL PAGO DE ESTA FACTURA DESPUES DE LA FECHA DE VENCIMIENTO PODRIA GENERAR INTERESES COMPENSATORIOS Y MORATORIOS A LAS TASAS MAXIMAS PERMITIDAS POR LEY." & vbNewLine & vbNewLine
            ''End If

            ''If Provision.SPOT_Afecto Then
            ''    NotasAdicionales = NotasAdicionales & "- OPERACION SUJETA AL SISTEMA DE PAGO DE OBLIGACIONES TRIBUTARIAS CON EL GOBIERNO CENTRAL." & vbNewLine & vbNewLine
            ''    If Provision.SPOT_Reg_Codigo = "01" Then
            ''        NotasAdicionales = NotasAdicionales & "- LA DETRACCION SOBRE EL PRECIO DE VENTA DEL SERVICIO ES " & Format(Provision.SPOT_Monto, "#,##0.00") & " (" & Format(Provision.SPOT_Porcentaje, "#,##0.00") & "%)." & vbNewLine & vbNewLine
            ''    End If
            ''End If

            ''Dim rptLista As New List(Of DocumentoElectronico)
            ''rptLista.Add(Provision.XML_POS)
            ''Rpte.DataSource = rptLista

            ' ''Codigo barras PDF417
            ''Rpte.XrBarCode1.BinaryData = System.Text.Encoding.UTF8.GetBytes(Provision.Vnt_Cert_Firma_PDF417)

            ' ''Notas adicionales
            ''Rpte.XrRichText1.Text = NotasAdicionales

            ''Dim Lista As New List(Of eProvisionFacturacion)
            ''Lista.Add(Provision)
            ''Rpte.DataSource = Lista

            ''Rpte.CreateDocument()
            ''Rpte.ExportToPdf(nProceso.FileNamePDF)



            'Dim Provision As DocumentoElectronico = Provision2.XML_POS
            'Try
            '    If String.IsNullOrEmpty(Provision.Impresion_Modelo_Codigo) Then
            '        Provision.Impresion_Modelo_Codigo = "000"
            '    End If
            '    If Provision.Impresion_Modelo_Codigo = "000" Then
            '        Dim RptePOS As New Print_Invoice_POS
            '        RptePOS.PrintingSystem.Document.AutoFitToPagesWidth = 1
            '        'If Provision.Fecha_Emision = Provision.Fecha_Vencimiento Then
            '        '    'Contado - No mostramos nada
            '        '    RptePOS.XrLabel16.Visible = False
            '        '    RptePOS.XrLabel43.Visible = False

            '        '    RptePOS.XrLabel18.LocationF = RptePOS.XrLabel17.LocationF
            '        '    RptePOS.XrLabel9.LocationF = RptePOS.XrLabel44.LocationF

            '        '    RptePOS.XrLabel17.LocationF = RptePOS.XrLabel16.LocationF
            '        '    RptePOS.XrLabel44.LocationF = RptePOS.XrLabel43.LocationF
            '        'End If

            '        Dim rptLista As New List(Of DocumentoElectronico)
            '        rptLista.Add(Provision)
            '        RptePOS.DataSource = rptLista

            '        'Codigo barras PDF417
            '        RptePOS.XrBarCode1.BinaryData = System.Text.Encoding.UTF8.GetBytes(Provision.Print_BarCode)

            '        ''Notas adicionales
            '        'RptePOS.XrRichText1.Text = NotasAdicionales

            '        RptePOS.CreateDocument()
            '        RptePOS.ExportToPdf(nProceso.FileNamePDF)
            '    ElseIf Provision.Impresion_Modelo_Codigo = "001" Then
            '        Dim RptePOS As New Print_FE_ESO
            '        RptePOS.PrintingSystem.Document.AutoFitToPagesWidth = 1
            '        If Provision.Fecha_Emision = Provision.Fecha_Vencimiento Or Year(Provision.Fecha_Vencimiento) = 1 Then
            '            'Contado - No mostramos nada
            '            RptePOS.XrLabel26.Visible = False
            '        End If
            '        Dim rptLista As New List(Of DocumentoElectronico)
            '        rptLista.Add(Provision)
            '        RptePOS.DataSource = rptLista

            '        'Codigo barras PDF417
            '        RptePOS.XrBarCode1.BinaryData = System.Text.Encoding.UTF8.GetBytes(Provision.Print_BarCode)

            '        ''Notas adicionales
            '        'RptePOS.XrRichText1.Text = NotasAdicionales

            '        RptePOS.CreateDocument()
            '        RptePOS.ExportToPdf(nProceso.FileNamePDF)

            '    ElseIf Provision.Impresion_Modelo_Codigo = "002" Then
            '        Dim RptePOS As New Print_FE_SO
            '        RptePOS.PrintingSystem.Document.AutoFitToPagesWidth = 1
            '        If Provision.Fecha_Emision = Provision.Fecha_Vencimiento Or Year(Provision.Fecha_Vencimiento) = 1 Then
            '            'Contado - No mostramos nada
            '            RptePOS.XrLabel26.Visible = False
            '        End If
            '        Dim rptLista As New List(Of DocumentoElectronico)
            '        rptLista.Add(Provision)
            '        RptePOS.DataSource = rptLista

            '        'Codigo barras PDF417
            '        RptePOS.XrBarCode1.BinaryData = System.Text.Encoding.UTF8.GetBytes(Provision.Print_BarCode)

            '        RptePOS.CreateDocument()
            '        RptePOS.ExportToPdf(nProceso.FileNamePDF)
            '    ElseIf Provision.Impresion_Modelo_Codigo = "003" Then
            '        Dim RptePOS As New Print_FE_Nota_Credito
            '        RptePOS.PrintingSystem.Document.AutoFitToPagesWidth = 1
            '        Dim rptLista As New List(Of DocumentoElectronico)
            '        rptLista.Add(Provision)
            '        RptePOS.DataSource = rptLista

            '        'Codigo barras PDF417
            '        RptePOS.XrBarCode1.BinaryData = System.Text.Encoding.UTF8.GetBytes(Provision.Print_BarCode)
            '        RptePOS.CreateDocument()
            '        RptePOS.ExportToPdf(nProceso.FileNamePDF)
            '    ElseIf Provision.Impresion_Modelo_Codigo = "004" Then
            '        Dim RptePOS As New Print_FE_Nota_Debito
            '        RptePOS.PrintingSystem.Document.AutoFitToPagesWidth = 1
            '        Dim rptLista As New List(Of DocumentoElectronico)
            '        rptLista.Add(Provision)
            '        RptePOS.DataSource = rptLista

            '        'Codigo barras PDF417
            '        RptePOS.XrBarCode1.BinaryData = System.Text.Encoding.UTF8.GetBytes(Provision.Print_BarCode)
            '        RptePOS.CreateDocument()
            '        RptePOS.ExportToPdf(nProceso.FileNamePDF)
            '    ElseIf Provision.Impresion_Modelo_Codigo = "005" Then
            '        Dim RptePOS As New Print_FE_Retencion
            '        RptePOS.PrintingSystem.Document.AutoFitToPagesWidth = 1
            '        Dim rptLista As New List(Of DocumentoElectronico)
            '        rptLista.Add(Provision)
            '        RptePOS.DataSource = rptLista

            '        'Codigo barras PDF417
            '        'RptePOS.XrBarCode1.BinaryData = System.Text.Encoding.UTF8.GetBytes(Provision.Print_BarCode)
            '        RptePOS.CreateDocument()
            '        RptePOS.ExportToPdf(nProceso.FileNamePDF)
            '    End If


            '    nProceso.PDF_Creado_Correctamente = True
            'Catch ex As Exception
            '    'log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CREATEPDF", ex.Message)
            'End Try
        Catch ex As Exception
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CREATEPDF", ex.Message)
        End Try
    End Sub
    Sub XMLCopiarAceptado(nProceso As fcProceso)
        Try
            'Creamos el directorio en caso no exista
            If Not IO.Directory.Exists(nProceso.ServidorRuta_Aceptado) Then
                IO.Directory.CreateDirectory(nProceso.ServidorRuta_Aceptado)
            End If
            If IO.File.Exists(nProceso.ServidorRuta_Generado & nProceso.FileNameXML) Then
                IO.File.Copy(nProceso.ServidorRuta_Generado & nProceso.FileNameXML, nProceso.ServidorRuta_Aceptado & nProceso.FileNameXML, True)
            End If
            If IO.File.Exists(nProceso.ServidorRuta_Generado & nProceso.FileNameZIP) Then
                IO.File.Copy(nProceso.ServidorRuta_Generado & nProceso.FileNameZIP, nProceso.ServidorRuta_Aceptado & nProceso.FileNameZIP, True)
            End If
            If IO.File.Exists(nProceso.ServidorRuta_Generado & nProceso.FileNamePDF) Then
                IO.File.Copy(nProceso.ServidorRuta_Generado & nProceso.FileNamePDF, nProceso.ServidorRuta_Aceptado & nProceso.FileNamePDF, True)
            End If
            If IO.File.Exists(nProceso.ServidorRuta_Temporal & nProceso.CDRNameZIP) Then
                IO.File.Copy(nProceso.ServidorRuta_Temporal & nProceso.CDRNameZIP, nProceso.ServidorRuta_Aceptado & nProceso.CDRNameZIP, True)
            End If

            'Copiamos en el servidor si no tenemos FTP
            'If String.IsNullOrEmpty(nProceso.FTP_Carpeta) Then
            '    IO.File.Copy(nProceso.ServidorRuta_Generado & nProceso.FileNameXML, nProceso.ServidorRutaServidor & nProceso.FileNameXML, True)
            '    If IO.File.Exists(nProceso.ServidorRuta_Generado & nProceso.FileNamePDF) Then
            '        IO.File.Copy(nProceso.ServidorRuta_Generado & nProceso.FileNamePDF, nProceso.ServidorRutaServidor & nProceso.FileNamePDF, True)
            '    End If
            '    If IO.File.Exists(nProceso.ServidorRuta_Temporal & nProceso.CDRNameZIP) Then
            '        IO.File.Copy(nProceso.ServidorRuta_Temporal & nProceso.CDRNameZIP, nProceso.ServidorRutaServidor & nProceso.CDRNameZIP, True)
            '    End If
            'End If
            'copiamos al FPT
            'FALTA
            nProceso.GeneradosGuardadoServidorCorrectamente = True
        Catch ex As Exception
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "XMLCOPIAR-ACEP-FACTBOL", ex.Message)

            nProceso.GeneradosGuardadoServidorCorrectamente = False
        End Try
    End Sub
    Sub XMLCopiarRechazado(nProceso As fcProceso)
        Try
            'Creamos el directorio en caso no exista
            If Not IO.Directory.Exists(nProceso.ServidorRuta_Rechazado) Then
                IO.Directory.CreateDirectory(nProceso.ServidorRuta_Rechazado)
            End If
            If IO.File.Exists(nProceso.ServidorRuta_Generado & nProceso.FileNameXML) Then
                IO.File.Copy(nProceso.ServidorRuta_Generado & nProceso.FileNameXML, nProceso.ServidorRuta_Rechazado & nProceso.FileNameXML, True)
            End If
            If IO.File.Exists(nProceso.ServidorRuta_Generado & nProceso.FileNameZIP) Then
                IO.File.Copy(nProceso.ServidorRuta_Generado & nProceso.FileNameZIP, nProceso.ServidorRuta_Rechazado & nProceso.FileNameZIP, True)
            End If
            If IO.File.Exists(nProceso.ServidorRuta_Aceptado & nProceso.FileNamePDF) Then
                IO.File.Copy(nProceso.ServidorRuta_Aceptado & nProceso.FileNamePDF, nProceso.ServidorRuta_Rechazado & nProceso.FileNamePDF, True)
            End If
            If IO.File.Exists(nProceso.ServidorRuta_Aceptado & nProceso.CDRNameZIP) Then
                IO.File.Copy(nProceso.ServidorRuta_Aceptado & nProceso.CDRNameZIP, nProceso.ServidorRuta_Rechazado & nProceso.CDRNameZIP, True)
            End If
            nProceso.GeneradosGuardadoServidorCorrectamente = True
        Catch ex As Exception
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "XMLCOPIAR-RECHAZ-FACTBOL", ex.Message)

            nProceso.GeneradosGuardadoServidorCorrectamente = False
        End Try
    End Sub
    'Public Function EnviarCorreo(provision As eProvisionFacturacion, nProceso As fcProceso) As Boolean
    '    If Not String.IsNullOrEmpty(provision.Cliente_Correo_Electronico) Then
    '        '-------------------------------------------------------ENVIAR CORREO
    '        Try


    '        Using mailMsg As New System.Net.Mail.MailMessage()
    '            Using smtp As New System.Net.Mail.SmtpClient
    '                Dim Anexos As New ArrayList()
    '                smtp.DeliveryMethod = Net.Mail.SmtpDeliveryMethod.Network
    '                smtp.Host = provision.CertificadoCredencial.Correo_Servidor_SMTP 'servidor de correo
    '                smtp.Port = provision.CertificadoCredencial.Correo_Servidor_Puerto

    '                If provision.CertificadoCredencial.Correo_Servidor_SSL Then
    '                    smtp.EnableSsl = True
    '                End If

    '                smtp.Credentials = New System.Net.NetworkCredential(provision.CertificadoCredencial.Correo_Usuario, provision.CertificadoCredencial.Correo_Contrasena)

    '                Dim Correo_Destinatario As String = provision.Cliente_Correo_Electronico 'direccion de correo a la que se le enviará el mail

    '                Dim BodyHtml As String = "<div style='width:100%;font-size:13px;font-family:arial,helvetica,sans-serif'><div style='max-width:500px;margin:auto;border:4px solid #cdced6;background:#fafafa;color:#444444'><div style='border:3px solid #97979e'><div style='margin:15px'>"
    '                BodyHtml = BodyHtml & "<div style='padding:15px 0;text-align:center;color:#444444'>" & provision.Emisor_RazonSocial_Nombre & " te envió una " & provision.TpDc_Descripcion & "</div>"
    '                BodyHtml = BodyHtml & "<h1 style='padding:5px 0;text-align:center;margin:0;color:#444444'>" & provision.TpDc_Descripcion & "</h1>"
    '                BodyHtml = BodyHtml & "<h1 style='padding:5px 0;text-align:center;margin:0;color:#444444'>No. " & provision.Dvt_VTSerie & " - " & provision.Dvt_VTNumer & "</h1>"
    '                BodyHtml = BodyHtml & "<div style='padding:5px 0;text-align:center;color:#444444'>Fecha de emisión:" & provision.Dvt_Fecha_Emision.ToShortDateString & "</div>"
    '                BodyHtml = BodyHtml & "<h1 style='padding:5px 0;text-align:center;font-size:50px;font-weight:bold;margin:10px 0;color:#444444'>" & provision.Dvt_Simbol_Moneda & " " & Format(provision.Dvt_Total_Importe, "#,##0.00") & "</h1>"

    '                If Not String.IsNullOrEmpty(provision.Vnt_Observacion) Then
    '                    BodyHtml = BodyHtml & "<div style='padding:5px 0;text-align:center;color:#444444'>" & provision.Vnt_Observacion & "</div>"
    '                End If

    '                BodyHtml = BodyHtml & "<div style='padding:5px 0;margin:10px 0'><div style='margin:5px 0'><a href='http://efacturacion.grupoortiz.pe:8078/Documento/" & provision.PostIDActionModel & "' style='display:block;padding:15px;background:#160064;color:#ffffff;text-align:center;text-decoration:none' target='_blank'>VER DOCUMENTO</a></div></div>"
    '                BodyHtml = BodyHtml & "<div style='padding:15px 0 5px;text-align:center;color:#444444'>Si el link no funciona, usa el siguiente enlace en tu navegador: <a style='color:#444444' href='http://efacturacion.grupoortiz.pe:8078/Documento/" & provision.PostIDActionModel & "' target='_blank'>" & "http://efacturacion.grupoortiz.pe:8078/Documento/" & provision.PostIDActionModel & "</a></div>"
    '                BodyHtml = BodyHtml & "<div style='padding:5px 0;text-align:center;color:#444444'>También se adjunta en PDF el mismo documento. Que puede ser impresa y usada como una Factura emitida de manera tradicional.</div>"
    '                BodyHtml = BodyHtml & "</div></div></div></div>"
    '                BodyHtml = BodyHtml & "<div><img src='http://efacturacion.grupoortiz.pe:8078/Documento/GetLogoURL_CheckReadMail?RUC_Emisor=" & provision.Emisor_RUC & "&ID_Document=" & provision.Doc_NombreArchivoWeb & "'/></div>"
    '                If File.Exists(nProceso.ServidorRuta_Aceptado & nProceso.FileNameXML) Then
    '                    Anexos.Add(nProceso.ServidorRuta_Aceptado & nProceso.FileNameXML)
    '                End If
    '                If File.Exists(nProceso.ServidorRuta_Aceptado & nProceso.FileNamePDF) Then
    '                    Anexos.Add(nProceso.ServidorRuta_Aceptado & nProceso.FileNamePDF)
    '                End If

    '                'Anexos.Add(nProceso.FileNameCDR)
    '                With (mailMsg)
    '                    .From = New System.Net.Mail.MailAddress(provision.CertificadoCredencial.Correo_Usuario, provision.Emisor_RazonSocial_Nombre, System.Text.Encoding.UTF8)
    '                    .To.Add(New System.Net.Mail.MailAddress(Correo_Destinatario, "Cliente", System.Text.Encoding.UTF8))
    '                    '.CC.Add("donny.huaman@grupoortiz.pe")
    '                        .Subject = provision.Emisor_RazonSocial_Nombre & " " & provision.TpDc_Codigo_St & " " & provision.Dvt_VTSerie & " - " & provision.Dvt_VTNumer
    '                    .SubjectEncoding = System.Text.Encoding.UTF8
    '                    .Body = BodyHtml
    '                    .BodyEncoding = System.Text.Encoding.UTF8
    '                    .IsBodyHtml = True
    '                    If (Anexos.Count > 0) Then
    '                        For i As Integer = 0 To Anexos.Count - 1
    '                            If (System.IO.File.Exists(Anexos(i))) Then
    '                                .Attachments.Add(New System.Net.Mail.Attachment(Anexos(i)))
    '                                'Else
    '                                '    MsgBox("El archivo " + Anexos(i) + " No existe")
    '                                'Exit Function
    '                            End If
    '                        Next
    '                    End If
    '                End With
    '                Try
    '                    smtp.Send(mailMsg)
    '                    smtp.Dispose()
    '                    mailMsg.Dispose()
    '                    Return True
    '                    'MsgBox("Mensaje enviado satisfactoriamente")
    '                Catch ex As Exception
    '                    log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "SENDMAIL", ex.Message & ", " & ex.InnerException.ToString & ", " & ex.HResult.ToString)
    '                    'MsgBox("ERROR: " & ex.Message)
    '                    Return False
    '                End Try
    '            End Using
    '            End Using
    '        Catch ex As Exception
    '            Return False
    '        End Try
    '    End If

    '    Return False
    '    '--------------efactura 6666669991011Aa
    'End Function
    Function ZIPEnviar_BoletaVentaFactura_NotaCreditoNotaDebito_Bill_Alta(nProceso As fcProceso) As eSunatRespuesta
        Dim RptaSUNAT As New eSunatRespuesta
        If nProceso.Etapa = "001" Then 'BETA
            RptaSUNAT = SUNAT_Beta_BoletaVentaFactura_NotaCreditoNotaDebito_Bill_Alta(nProceso)
        ElseIf nProceso.Etapa = "002" Then 'HOMOLOGACION
            RptaSUNAT = SUNAT_Homologacion_BoletaVentaFactura_NotaCreditoNotaDebito_Bill_Alta(nProceso)
        ElseIf nProceso.Etapa = "003" Then 'PRODUCCION
            If nProceso.Es_OSE Then
                RptaSUNAT = SUNAT_Produccion_BoletaVentaFactura_NotaCreditoNotaDebito_Bill_Alta_OSE(nProceso)
            Else
                RptaSUNAT = SUNAT_Produccion_BoletaVentaFactura_NotaCreditoNotaDebito_Bill_Alta(nProceso)
            End If

        End If
        Return RptaSUNAT
    End Function
    Public Function SUNAT_Beta_BoletaVentaFactura_NotaCreditoNotaDebito_Bill_Alta(nProceso As fcProceso) As eSunatRespuesta
        Dim RptaSUNAT As New eSunatRespuesta

        Dim CDR_Name_Zip As String = nProceso.FileNameWithOutExtension & "-CDR.ZIP"
        Dim CDR_Name_Xml As String = "R-" & nProceso.FileNameWithOutExtension & ".XML"
        Dim CDR_Directory_Descompression As String = nProceso.ServidorRuta_Aceptado '& "\" 'Application.StartupPath
        nProceso.CDRNameZIP = CDR_Name_Zip
        nProceso.CDRNameXML = CDR_Name_Xml
        '--BETA
        Dim DataByteArray As Byte() = System.IO.File.ReadAllBytes(nProceso.ServidorRuta_Generado & nProceso.FileNameZIP)

        System.Net.ServicePointManager.UseNagleAlgorithm = True
        System.Net.ServicePointManager.Expect100Continue = False
        System.Net.ServicePointManager.CheckCertificateRevocationList = True
        Dim binding As New ServiceModel.BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential)
        binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None
        binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None
        binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName
        binding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Default
        'Dim remoteAddress As New EndpointAddress("https://e-beta.sunat.gob.pe/ol-ti-itcpfegem-beta/billService?wsdl")
        Dim remoteAddress As New EndpointAddress("https://e-beta.sunat.gob.pe/ol-ti-itcpfegem-beta/billService")
        Dim client As New WSSUNAT_Beta_FactBoleta_Bill.billServiceClient(binding, remoteAddress)
        client.ClientCredentials.UserName.UserName = nProceso.Emisor_RUC & "MODDATOS"
        client.ClientCredentials.UserName.Password = "moddatos"
        Dim bindingElementsInTopDownChannelStackOrder As BindingElementCollection = client.Endpoint.Binding.CreateBindingElements
        bindingElementsInTopDownChannelStackOrder.Find(Of SecurityBindingElement).EnableUnsecuredResponse = True
        client.Endpoint.Binding = New CustomBinding(bindingElementsInTopDownChannelStackOrder)

        Dim RetornoCDR_Byte As Byte()

        Try
            RetornoCDR_Byte = client.sendBill(nProceso.FileNameZIP, DataByteArray, "")
        Catch exception As FaultException
            Dim str10 As String = exception.Code.Name.ToUpper.Replace("SOAP-ENV-SERVER", "").Replace("CLIENT.", "")
            Dim str11 As String = ("FaultException Validación: " & exception.Message)
            RptaSUNAT.Codigo = str10
            RptaSUNAT.Mensaje = str11
            RptaSUNAT.Estado = EstadoDocumento.Rechazado

            log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL", RptaSUNAT.Codigo & "|" & RptaSUNAT.Mensaje)

            Return RptaSUNAT
        Catch exception3 As Exception
            RptaSUNAT.Codigo = "0"
            RptaSUNAT.Mensaje = "El archivo XML ha sido generado, pero aún no ha sido válidado con la SUNAT debido a que no hay conexión con la URL: https://www.sunat.gob.pe:443/ol-ti-itcpgem-beta/billService"

            log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL", RptaSUNAT.Codigo & "|" & RptaSUNAT.Mensaje)

            Return RptaSUNAT
        End Try

        If (Not RetornoCDR_Byte Is Nothing) Then
            Try
                File.WriteAllBytes(nProceso.ServidorRuta_Aceptado & "\" & CDR_Name_Zip, RetornoCDR_Byte)
                If File.Exists((CDR_Directory_Descompression & CDR_Name_Xml)) Then
                    File.Delete((CDR_Directory_Descompression & CDR_Name_Xml))
                End If
                Compression.ZipFile.ExtractToDirectory(nProceso.ServidorRuta_Aceptado & "\" & CDR_Name_Zip, CDR_Directory_Descompression)
                Dim document As New XmlDocument
                document.Load((CDR_Directory_Descompression & CDR_Name_Xml))
                Dim childNodes As XmlNodeList = document.GetElementsByTagName("DocumentResponse", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2").Item(0).FirstChild.ChildNodes
                RptaSUNAT.Codigo = childNodes.ItemOf(1).InnerText
                RptaSUNAT.Mensaje = childNodes.ItemOf(2).InnerText
                Dim Nodes As XmlNode = document.GetElementsByTagName("ResponseDate", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2").Item(0)
                Dim Fecha As Date = CDate(Nodes.InnerText)
                Nodes = document.GetElementsByTagName("ResponseTime", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2").Item(0)
                Dim Hora As TimeSpan = TimeSpan.Parse(Nodes.InnerText)
                RptaSUNAT.Fecha_Respuesta = Fecha.Add(Hora)

            Catch ex As Exception
                'Sospecho disco lleno
                log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL-COMP", ex.Message)
            End Try

        End If

        If (Convert.ToInt32(RptaSUNAT.Codigo) = 0) Then
            RptaSUNAT.Estado = EstadoDocumento.Aceptado
            '    log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "ACEPTADO|" & nProceso.FileNameXML)
            'ElseIf ((Convert.ToInt32(RptaSUNAT.Codigo) >= 1) AndAlso (Convert.ToInt32(RptaSUNAT.Codigo) <= 199)) Then
            '    RptaSUNAT.Estado = EstadoDocumento.Invalido
            '    log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "INVALIDO|" & nProceso.FileNameXML)
            'ElseIf (Convert.ToInt32(RptaSUNAT.Codigo) >= 200) Then
        Else
            RptaSUNAT.Estado = EstadoDocumento.Rechazado
            log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "RECHAZADO|" & nProceso.FileNameXML)
        End If

        Return RptaSUNAT
    End Function
    Public Function SUNAT_Homologacion_BoletaVentaFactura_NotaCreditoNotaDebito_Bill_Alta(nProceso As fcProceso) As eSunatRespuesta
        Dim RptaSUNAT As New eSunatRespuesta

        Dim CDR_Name_Zip As String = nProceso.FileNameWithOutExtension & "-CDR.ZIP"
        Dim CDR_Name_Xml As String = "R-" & nProceso.FileNameWithOutExtension & ".XML"
        Dim CDR_Directory_Descompression As String = nProceso.ServidorRuta_Aceptado '& "\" 'Application.StartupPath & "\"
        nProceso.CDRNameZIP = CDR_Name_Zip
        nProceso.CDRNameXML = CDR_Name_Xml

        '--BETA
        Dim DataByteArray As Byte() = System.IO.File.ReadAllBytes(nProceso.ServidorRuta_Generado & nProceso.FileNameZIP)

        System.Net.ServicePointManager.UseNagleAlgorithm = True
        System.Net.ServicePointManager.Expect100Continue = False
        System.Net.ServicePointManager.CheckCertificateRevocationList = True
        Dim binding As New ServiceModel.BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential)
        binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None
        binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None
        binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName
        binding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Default
        Dim remoteAddress As New EndpointAddress("https://www.sunat.gob.pe/ol-ti-itcpgem-sqa/billService")
        Dim client As New WSSUNAT_Homologacion_FactBoleta_Bill.billServiceClient(binding, remoteAddress)
        client.ClientCredentials.UserName.UserName = nProceso.Emisor_RUC & nProceso.SUNAT_Usuario
        client.ClientCredentials.UserName.Password = nProceso.SUNAT_Contrasena
        Dim bindingElementsInTopDownChannelStackOrder As BindingElementCollection = client.Endpoint.Binding.CreateBindingElements
        bindingElementsInTopDownChannelStackOrder.Find(Of SecurityBindingElement).EnableUnsecuredResponse = True
        client.Endpoint.Binding = New CustomBinding(bindingElementsInTopDownChannelStackOrder)

        Dim RetornoCDR_Byte As Byte()

        Try
            RetornoCDR_Byte = client.sendBill(nProceso.FileNameZIP, DataByteArray)
        Catch exception As FaultException
            Dim str10 As String = exception.Code.Name.ToUpper.Replace("SOAP-ENV-SERVER.", "").Replace("SOAP-ENV-SERVER", "")
            Dim str11 As String = ("FaultException Validación: " & exception.Message)
            RptaSUNAT.Codigo = str10
            RptaSUNAT.Mensaje = str11
            RptaSUNAT.Estado = EstadoDocumento.Rechazado

            log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL1", RptaSUNAT.Codigo & "|" & RptaSUNAT.Mensaje)

            Return RptaSUNAT
        Catch exception3 As Exception
            RptaSUNAT.Codigo = "0"
            RptaSUNAT.Mensaje = "El archivo XML ha sido generado, pero aún no ha sido válidado con la SUNAT debido a que no hay conexión con la URL: https://www.sunat.gob.pe:443/ol-ti-itcpgem-beta/billService"

            log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL2", RptaSUNAT.Codigo & "|" & RptaSUNAT.Mensaje)

            Return RptaSUNAT
        End Try

        If (Not RetornoCDR_Byte Is Nothing) Then
            Try
                File.WriteAllBytes(nProceso.ServidorRuta_Aceptado & "\" & CDR_Name_Zip, RetornoCDR_Byte)
                If File.Exists((CDR_Directory_Descompression & CDR_Name_Xml)) Then
                    File.Delete((CDR_Directory_Descompression & CDR_Name_Xml))
                End If
                Compression.ZipFile.ExtractToDirectory(nProceso.ServidorRuta_Aceptado & "\" & CDR_Name_Zip, CDR_Directory_Descompression)
                Dim document As New XmlDocument
                document.Load((CDR_Directory_Descompression & CDR_Name_Xml))
                Dim childNodes As XmlNodeList = document.GetElementsByTagName("DocumentResponse", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2").Item(0).FirstChild.ChildNodes
                RptaSUNAT.Codigo = childNodes.ItemOf(1).InnerText
                RptaSUNAT.Mensaje = childNodes.ItemOf(2).InnerText
                Dim Nodes As XmlNode = document.GetElementsByTagName("ResponseDate", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2").Item(0)
                Dim Fecha As Date = CDate(Nodes.InnerText)
                Nodes = document.GetElementsByTagName("ResponseTime", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2").Item(0)
                Dim Hora As TimeSpan = TimeSpan.Parse(Nodes.InnerText)
                RptaSUNAT.Fecha_Respuesta = Fecha.Add(Hora)
            Catch ex As Exception
                'Sospecho disco lleno
                log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL-COMP", ex.Message)
            End Try

        End If
        If (Convert.ToInt32(RptaSUNAT.Codigo) = 0) Then
            RptaSUNAT.Estado = EstadoDocumento.Aceptado
            '    log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "ACEPTADO|" & nProceso.FileNameXML)
            'ElseIf ((Convert.ToInt32(RptaSUNAT.Codigo) >= 1) AndAlso (Convert.ToInt32(RptaSUNAT.Codigo) <= 199)) Then
            '    RptaSUNAT.Estado = EstadoDocumento.Invalido
            '    log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "INVALIDO|" & nProceso.FileNameXML)
            'ElseIf (Convert.ToInt32(RptaSUNAT.Codigo) >= 200) Then
        Else
            RptaSUNAT.Estado = EstadoDocumento.Rechazado
            log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "RECHAZADO|" & nProceso.FileNameXML)
        End If

        Return RptaSUNAT
    End Function
    Public Function SUNAT_Produccion_BoletaVentaFactura_NotaCreditoNotaDebito_Bill_Alta(nProceso As fcProceso) As eSunatRespuesta
        Dim RptaSUNAT As New eSunatRespuesta

        Dim CDR_Name_Zip As String = nProceso.FileNameWithOutExtension & "-CDR.ZIP"
        Dim CDR_Name_Xml As String = "R-" & nProceso.FileNameWithOutExtension & ".XML"
        Dim CDR_Directory_Descompression As String = nProceso.ServidorRuta_Aceptado '& "\" 'Application.StartupPath
        nProceso.CDRNameZIP = CDR_Name_Zip
        nProceso.CDRNameXML = CDR_Name_Xml

        '--BETA
        Dim DataByteArray As Byte() = System.IO.File.ReadAllBytes(nProceso.ServidorRuta_Generado & nProceso.FileNameZIP)

        System.Net.ServicePointManager.UseNagleAlgorithm = True
        System.Net.ServicePointManager.Expect100Continue = False
        System.Net.ServicePointManager.CheckCertificateRevocationList = True

        Dim binding As New ServiceModel.BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential)
        binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None
        binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None
        binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName
        binding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Default
        Dim remoteAddress As New EndpointAddress("https://e-factura.sunat.gob.pe/ol-ti-itcpfegem/billService")  'e-factura.sunat.gob.pe/ol-ti-itcpfegem/billService
        Dim client As New WSSUNAT_Produccion_FactBoleta_Bill.billServiceClient(binding, remoteAddress)
        client.ClientCredentials.UserName.UserName = nProceso.Emisor_RUC & nProceso.SUNAT_Usuario
        client.ClientCredentials.UserName.Password = nProceso.SUNAT_Contrasena
        Dim bindingElementsInTopDownChannelStackOrder As BindingElementCollection = client.Endpoint.Binding.CreateBindingElements
        bindingElementsInTopDownChannelStackOrder.Find(Of SecurityBindingElement).EnableUnsecuredResponse = True
        client.Endpoint.Binding = New CustomBinding(bindingElementsInTopDownChannelStackOrder)

        Dim RetornoCDR_Byte As Byte()

        Try
            RetornoCDR_Byte = client.sendBill(nProceso.FileNameZIP, DataByteArray, "")
        Catch exception As FaultException
            Dim str10 As String = exception.Code.Name.ToUpper.Replace("SOAP-ENV-SERVER.", "").Replace("SOAP-ENV-SERVER", "")
            str10 = HerramientasFE.SoloNumeros(str10)
            Dim str11 As String = ("FaultException Validación: " & exception.Message)
            RptaSUNAT.Codigo = str10
            RptaSUNAT.Mensaje = str11
            If RptaSUNAT.Codigo <> "0109" And RptaSUNAT.Codigo <> "0130" And RptaSUNAT.Codigo <> "0100" Then
                RptaSUNAT.Estado = EstadoDocumento.Rechazado
            End If

            If RptaSUNAT.Codigo = "1033" Then
                RptaSUNAT.Estado = EstadoDocumento.Aceptado
            End If
            If RptaSUNAT.Codigo = "1032" Then
                RptaSUNAT.Estado = EstadoDocumento.Rechazado
            End If

            log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "PRODBILL-FACTBOL1", RptaSUNAT.Codigo & "|" & RptaSUNAT.Mensaje)
            Return RptaSUNAT

        Catch exception3 As Exception
            RptaSUNAT.Codigo = "0"
            RptaSUNAT.Mensaje = "El archivo XML ha sido generado, pero aún no ha sido válidado con la SUNAT debido a que no hay conexión con la URL: https://e-factura.sunat.gob.pe/ol-ti-itcpfegem/billService"
            RptaSUNAT.Estado = EstadoDocumento.Error
            log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "PRODBILL-FACTBOL2", RptaSUNAT.Codigo & "|" & RptaSUNAT.Mensaje)

            Return RptaSUNAT
        End Try

        If (Not RetornoCDR_Byte Is Nothing) Then
            Try
                File.WriteAllBytes(nProceso.ServidorRuta_Aceptado & "\" & CDR_Name_Zip, RetornoCDR_Byte)
                If File.Exists((CDR_Directory_Descompression & CDR_Name_Xml)) Then
                    File.Delete((CDR_Directory_Descompression & CDR_Name_Xml))
                End If
                Compression.ZipFile.ExtractToDirectory(nProceso.ServidorRuta_Aceptado & "\" & CDR_Name_Zip, CDR_Directory_Descompression)
                Dim document As New XmlDocument
                document.Load((CDR_Directory_Descompression & CDR_Name_Xml))
                Dim childNodes As XmlNodeList = document.GetElementsByTagName("DocumentResponse", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2").Item(0).FirstChild.ChildNodes
                RptaSUNAT.Codigo = childNodes.ItemOf(1).InnerText
                RptaSUNAT.Mensaje = childNodes.ItemOf(2).InnerText
                Dim Nodes As XmlNode = document.GetElementsByTagName("ResponseDate", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2").Item(0)
                Dim Fecha As Date = CDate(Nodes.InnerText)
                Nodes = document.GetElementsByTagName("ResponseTime", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2").Item(0)
                Dim Hora As TimeSpan = TimeSpan.Parse(Nodes.InnerText)
                RptaSUNAT.Fecha_Respuesta = Fecha.Add(Hora)

            Catch ex As Exception
                'Sospecho disco lleno
                log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "PRODBILL-FACTBOL-COMP", ex.Message)
            End Try

        End If

        If (Convert.ToInt32(RptaSUNAT.Codigo) = 0) Then
            RptaSUNAT.Estado = EstadoDocumento.Aceptado
            '    log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "ACEPTADO|" & nProceso.FileNameXML)
            'ElseIf ((Convert.ToInt32(RptaSUNAT.Codigo) >= 1) AndAlso (Convert.ToInt32(RptaSUNAT.Codigo) <= 199)) Then
            '    RptaSUNAT.Estado = EstadoDocumento.Invalido
            '    log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "INVALIDO|" & nProceso.FileNameXML)
            'ElseIf (Convert.ToInt32(RptaSUNAT.Codigo) >= 200) Then
        Else
            RptaSUNAT.Estado = EstadoDocumento.Rechazado
            log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "RECHAZADO|" & nProceso.FileNameXML)
        End If

        Return RptaSUNAT
    End Function
    Function ZIPEnviar_GuiaRemision_Bill_Alta(nProceso As fcProceso) As eSunatRespuesta
        Dim RptaSUNAT As New eSunatRespuesta
        If nProceso.Etapa = "001" Then 'BETA
            RptaSUNAT = SUNAT_Beta_GuiaRemision_Bill_Alta(nProceso)
        ElseIf nProceso.Etapa = "002" Then 'HOMOLOGACION
            RptaSUNAT = SUNAT_Beta_GuiaRemision_Bill_Alta(nProceso)
        ElseIf nProceso.Etapa = "003" Then 'PRODUCCION
            RptaSUNAT = SUNAT_Beta_GuiaRemision_Bill_Alta(nProceso)
        End If
        Return RptaSUNAT
    End Function
    Public Function SUNAT_Beta_GuiaRemision_Bill_Alta(nProceso As fcProceso) As eSunatRespuesta
        Dim RptaSUNAT As New eSunatRespuesta

        Dim CDR_Name_Zip As String = nProceso.FileNameWithOutExtension & "-CDR.ZIP"
        Dim CDR_Name_Xml As String = "R-" & nProceso.FileNameWithOutExtension & ".XML"
        'Dim CDR_Directory_Descompression As String = Application.StartupPath & "\"
        nProceso.CDRNameZIP = CDR_Name_Zip
        nProceso.CDRNameXML = CDR_Name_Xml
        '--BETA
        'Dim DataByteArray As Byte() = System.IO.File.ReadAllBytes(nProceso.ServidorRuta_Generado & "20131312955-09-T001-00000001.ZIP")
        Dim DataByteArray As Byte() = System.IO.File.ReadAllBytes(nProceso.ServidorRuta_Generado & nProceso.FileNameZIP)

        System.Net.ServicePointManager.UseNagleAlgorithm = True
        System.Net.ServicePointManager.Expect100Continue = False
        System.Net.ServicePointManager.CheckCertificateRevocationList = True

        Dim binding As New ServiceModel.BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential)
        binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None
        binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None
        binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName
        binding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Default
        'Dim remoteAddress As New EndpointAddress("https://e-beta.sunat.gob.pe/ol-ti-itcpfegem-beta/billService")
        Dim remoteAddress As New EndpointAddress("https://e-beta.sunat.gob.pe/ol-ti-itemision-guia-gem-beta/billService") 'https://e-beta.sunat.gob.pe/ol-ti-itemision-guia-gem-beta/billService?wsdl
        Dim client As New WSSUNAT_Beta_FactBoleta_Bill.billServiceClient(binding, remoteAddress)
        client.ClientCredentials.UserName.UserName = nProceso.Emisor_RUC & "MODDATOS"
        client.ClientCredentials.UserName.Password = "moddatos"
        Dim bindingElementsInTopDownChannelStackOrder As BindingElementCollection = client.Endpoint.Binding.CreateBindingElements
        bindingElementsInTopDownChannelStackOrder.Find(Of SecurityBindingElement).EnableUnsecuredResponse = True
        client.Endpoint.Binding = New CustomBinding(bindingElementsInTopDownChannelStackOrder)

        Dim RetornoCDR_Byte As Byte()

        Try
            RetornoCDR_Byte = client.sendBill(nProceso.FileNameZIP, DataByteArray, "")
            'RetornoCDR_Byte = client.sendBill("20131312955-09-T001-00000001.ZIP", DataByteArray)
        Catch exception As FaultException
            Dim str10 As String = exception.Code.Name.ToUpper.Replace("SOAP-ENV-SERVER.", "").Replace("SOAP-ENV-SERVER", "").Replace("CLIENT.", "")
            Dim str11 As String = ("FaultException Validación: " & exception.Message) '2335
            RptaSUNAT.Codigo = exception.Message ' str10
            RptaSUNAT.Mensaje = str11
            RptaSUNAT.Estado = EstadoDocumento.Rechazado

            'Log.Instancia.escribirLinea(Log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL", RptaSUNAT.Codigo & "|" & RptaSUNAT.Mensaje)

            'Return RptaSUNAT
        Catch exception3 As Exception
            RptaSUNAT.Codigo = "0"
            RptaSUNAT.Mensaje = "El archivo XML ha sido generado, pero aún no ha sido válidado con la SUNAT debido a que no hay conexión con la URL: https://www.sunat.gob.pe:443/ol-ti-itcpgem-beta/billService"

            'Log.Instancia.escribirLinea(Log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL", RptaSUNAT.Codigo & "|" & RptaSUNAT.Mensaje)

            Return RptaSUNAT
        End Try

        If (Not RetornoCDR_Byte Is Nothing) Then
            Try
                File.WriteAllBytes(nProceso.ServidorRuta_Temporal & CDR_Name_Zip, RetornoCDR_Byte)
                If File.Exists((nProceso.ServidorRuta_Temporal & CDR_Name_Xml)) Then
                    File.Delete((nProceso.ServidorRuta_Temporal & CDR_Name_Xml))
                End If
                Compression.ZipFile.ExtractToDirectory(nProceso.ServidorRuta_Temporal & CDR_Name_Zip, nProceso.ServidorRuta_Temporal)
                Dim document As New XmlDocument
                document.Load((nProceso.ServidorRuta_Temporal & CDR_Name_Xml))
                Dim childNodes As XmlNodeList = document.GetElementsByTagName("DocumentResponse", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2").Item(0).FirstChild.ChildNodes
                RptaSUNAT.Codigo = childNodes.ItemOf(1).InnerText
                RptaSUNAT.Mensaje = childNodes.ItemOf(2).InnerText
            Catch ex As Exception
                'Sospecho disco lleno
                'Log.Instancia.escribirLinea(Log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL-COMP", ex.Message)
            End Try

        End If

        If (Convert.ToInt32(RptaSUNAT.Codigo) = 0) Then
            RptaSUNAT.Estado = EstadoDocumento.Aceptado
            '    log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "ACEPTADO|" & nProceso.FileNameXML)
            'ElseIf ((Convert.ToInt32(RptaSUNAT.Codigo) >= 1) AndAlso (Convert.ToInt32(RptaSUNAT.Codigo) <= 199)) Then
            '    RptaSUNAT.Estado = EstadoDocumento.Invalido
            '    log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "INVALIDO|" & nProceso.FileNameXML)
            'ElseIf (Convert.ToInt32(RptaSUNAT.Codigo) >= 200) Then
        Else
            RptaSUNAT.Estado = EstadoDocumento.Rechazado
            log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "RECHAZADO|" & nProceso.FileNameXML)
        End If

        Return RptaSUNAT
    End Function

    Dim CertificadoDigital_Resumen As eCertificadoCredencial
    Private ResumenesPendientesEnvio As New List(Of eProvisionFacturacion)
    Private Resumen_Ejecutando As Boolean
    Private Resumen_Actualizando As Boolean = False
    'Private Ejecutando_Envio_Resumen As Boolean = False
    Private Sub TimerEnviarResumenActualizar_Tick(sender As Object, e As EventArgs) Handles TimerEnviarResumenActualizar.Tick
        If Not Resumen_Ejecutando Then
            Dim thResumen_Pendientes As New Threading.Thread(AddressOf ListaTareasDeResumenProgramadas)
            thResumen_Pendientes.Start()
        End If
    End Sub
    Sub ListaTareasDeResumenProgramadas()
        Resumen_Ejecutando = True
        Resumen_Actualizando = True
        Armar_Resumen_Empresa_Fecha()
        Dim logProgramados As New Log_FACTE_PROGRAMACIONES_ENVIO
        ResumenesPendientesEnvio = logProgramados.Buscar_Lista_Pendientes_Resumen()
        Resumen_Actualizando = False

        'Ejecutamos las tareas
        Ejecutar_Envio_Resumen_Pendientes()
        Resumen_Ejecutando = False
    End Sub
    Sub Armar_Resumen_Empresa_Fecha()

    End Sub
    Sub Ejecutar_Envio_Resumen_Pendientes()
        If Not Resumen_Actualizando Then
            Dim Her As New HerramientasFE
            If ResumenesPendientesEnvio.Count > 0 And Her.InternetConnection() Then

                Dim log_Pendientes As New Log_FACTE_PROGRAMACIONES_ENVIO
                For Each Doc_Pendiente In ResumenesPendientesEnvio

                    If IsNothing(CertificadoDigital_Resumen) Then
                        CertificadoDigital_Resumen = BuscarCredenciales(Doc_Pendiente)
                    End If
                    If CertificadoDigital_Resumen.Emisor_RUC <> Doc_Pendiente.Emisor_RUC Then
                        CertificadoDigital_Resumen = BuscarCredenciales(Doc_Pendiente)
                    End If

                    Dim Doc_Detalle As List(Of eProvisionFacturacion)
                    Doc_Detalle = log_Pendientes.Buscar_Lista_Pendientes_Resumen_Documento(Doc_Pendiente)

                    Dim Provision As eProvisionFacturacion = Doc_Pendiente

                    'CUANDO EL RESSUMEN ES DE NOTAS DE CREDITO O DEBITO Y BUSCAMOS SUS RELACIONADOS
                    Anadir_Relacionados_Resumen(Doc_Detalle, Provision)
                    Provision.CertificadoCredencial = CertificadoDigital_Resumen

                    Dim nProceso As New fcProceso
                    nProceso = Generar_Resumen_Diario_XML_SUNAT(Provision, Doc_Detalle)

                    If nProceso.ArchivoCreadoCorrectamente Then

                        nProceso.Emisor_RUC = Provision.Emisor_RUC
                        nProceso.SUNAT_Usuario = Provision.CertificadoCredencial.Sunat_Usuario
                        nProceso.SUNAT_Contrasena = Provision.CertificadoCredencial.Sunat_Contrasena

                        nProceso.ServidorRutaLocal = Provision.CertificadoCredencial.Path_Root_App & "\" & Provision.Emisor_RUC
                        nProceso.DocumentoTipo = Provision.TpDc_Codigo_St
                        nProceso.FTP_Direccion = Provision.CertificadoCredencial.FTP_Direccion
                        nProceso.FTP_Carpeta = Provision.CertificadoCredencial.FTP_Carpeta
                        nProceso.FTP_Usuario = Provision.CertificadoCredencial.FTP_Usuario
                        nProceso.FTP_Contrasena = Provision.CertificadoCredencial.FTP_Contrasena
                        nProceso.Etapa = Provision.Etapa_Codigo
                        nProceso.Es_OSE = Provision.CertificadoCredencial.Es_OSE
                        '2-XML FIRMA
                        XMLFirmar(Provision, CertificadoDigital_Resumen, nProceso)

                    End If

                    If nProceso.ArchivoFirmadoCorrectamente Then
                        XMLZip(nProceso)
                    End If
                    If nProceso.ArchivoComprimidoCorrectamente Then
                        XMLCopiarGenerado(nProceso)


                    End If
                    If nProceso.GeneradosGuardadoServidorCorrectamente Then
                        LimpiarDirectorio(nProceso)

                        Dim Rspta As eSunatRespuesta
                        Rspta = ZIPEnviar_Resumen_Diario_Comunicacion_Baja(nProceso)

                        Provision.Sunat_Respuesta_Codigo = Rspta.Codigo
                        Provision.Sunat_Respuesta_Descripcion = Rspta.Mensaje
                        Provision.Sunat_Respuesta_Ticket = Rspta.Ticket

                        Provision.Nombre_XML = nProceso.FileNameXML
                        If Rspta.Estado = EstadoDocumento.Aceptado Then
                            Provision.Vnt_EstadoEnvio = "006"
                        ElseIf Rspta.Estado = EstadoDocumento.Rechazado Then
                            Provision.Vnt_EstadoEnvio = "013"
                        End If
                        'Provision.EstadoElectronico = "4107" 'BMS
                        'Provision.Vnt_EstadoEnvio = "003" 'FACTE
                        'Cambiamos el estado al documento
                        If Rspta.Estado <> EstadoDocumento.Error Then
                            Dim LogAcep As New Log_FACTE_DOCUMENTOS
                            LogAcep.Resumen_Cambiar_Estado(Provision)
                        End If
                        
                        'Genera PDF
                        'GenerarPDF(Provision, nProceso)
                        'Copiamos a la carpeta aceptados
                        'XMLCopiarAceptado(nProceso)
                        ''Enviamos el correo al cliente
                        'If EnviarCorreo(Provision, nProceso) Then
                        '    'actualizamos que se envio el correo al cliente
                        '    LogAcep.Actualizar_Envio_Correo(Provision, nProceso)
                        'End If
                    End If
                    'nProceso.FileNameWithOutExtension = Path.GetFileNameWithoutExtension(Provision.Nombre_XML)
                Next
            End If
        End If
    End Sub
    Sub Anadir_Relacionados_Resumen(ByVal Det As List(Of eProvisionFacturacion), ByVal Provision As eProvisionFacturacion)
        For Each T In Det
            If ((T.TpDc_Codigo_St.Trim = "07" Or T.TpDc_Codigo_St.Trim = "08")) Then
                Dim Root_File = Provision.RootPath_Enterprise.Trim & "\xml_pos\" & T.SQL_XML_Nombre.Trim
                If IO.File.Exists(Root_File) Then
                    Dim document As New XmlDocument
                    document.Load((Root_File))
                    'Convertimos el XML en Objeto
                    Dim Provi As New DocumentoElectronico
                    Dim reader As New System.Xml.Serialization.XmlSerializer(Provi.[GetType]())
                    Dim reader2 As XmlNodeReader = New XmlNodeReader(document)
                    Provi = DirectCast(reader.Deserialize(reader2), DocumentoElectronico)
                    T.XML_POS = Provi
                End If

            End If

        Next
    End Sub
    Function ZIPEnviar_Resumen_Diario_Comunicacion_Baja(nProceso As fcProceso) As eSunatRespuesta
        Dim RptaSUNAT As New eSunatRespuesta
        If nProceso.Es_OSE Then
            RptaSUNAT = SUNAT_Summary_Voided_Envio_OSE(nProceso)
        Else
            RptaSUNAT = SUNAT_Summary_Voided_Envio(nProceso)
        End If

        Return RptaSUNAT
    End Function
    Function Generar_Resumen_Diario_XML_SUNAT(Cabecera As eProvisionFacturacion, Detalle As List(Of eProvisionFacturacion)) As fcProceso
        Dim ProcesoCompleto As New fcProceso With {.ArchivoCreadoCorrectamente = False}
        ProcesoCompleto.ServidorRutaLocal = Cabecera.RootPath_Enterprise
        ProcesoCompleto.ResumenDiario = True

        ProcesoCompleto.DocumentoTipo = Cabecera.TpDc_Codigo_St
        Dim TipoResumen As String = "RC" 'RA - Com baja
        Dim IDResumen As String = TipoResumen & "-" & Cabecera.Dvt_Fecha_Generacion.Year.ToString & Format(Cabecera.Dvt_Fecha_Generacion.Month, "00") & Format(Cabecera.Dvt_Fecha_Generacion.Day, "00") & "-" & Cabecera.Rs_Correlativo
        Dim Filename As String = Cabecera.Emisor_RUC & "-" & IDResumen
        Dim xmlFilename As String = Filename & ".XML"
        Dim ZipFilename As String = Filename & ".ZIP"

        ProcesoCompleto.FileNameWithOutExtension = Filename

        Dim DespatchAdvice As UblLarsen.Ubl2.SummaryDocumentsType
        UblLarsen.Ubl2.UblBaseDocumentType.GlbUblVersionID = "2.0"
        UblLarsen.Ubl2.UblBaseDocumentType.GlbCustomizationID = "1.1"
        'AmountType.TlsDefaultCurrencyID = Provision.Moneda_Codigo

        'Creando los detalles
        Dim ListaDetalles As New List(Of SummaryDocumentsLineType)
        If Not IsNothing(Detalle) Then
            For Each ProvisionItem In Detalle
                Dim ItemDoc As New SummaryDocumentsLineType
                ItemDoc.LineID = ProvisionItem.Rs_Correlativo
                ItemDoc.DocumentTypeCode = ProvisionItem.TpDc_Codigo_St
                ItemDoc.ID = ProvisionItem.Dvt_VTSerie & "-" & ProvisionItem.Dvt_VTNumer 'V2
                ItemDoc.AccountingCustomerParty = New SupplierPartyType With {.CustomerAssignedAccountID = New IdentifierType With {.Value = ProvisionItem.Cliente_Documento_Numero},
                                                                   .AdditionalAccountID = New IdentifierType() {New IdentifierType With {.Value = ProvisionItem.Cliente_Documento_Tipo}}} 'V2
                'tipo, Serie numero de la boleta que modifica
                If Not IsNothing(ProvisionItem.XML_POS) Then
                    If Not IsNothing(ProvisionItem.XML_POS.DocumentoAfectados) Then
                        Dim Ref As New List(Of BillingReferenceType)
                        Ref.Clear()
                        For Each T In ProvisionItem.XML_POS.DocumentoAfectados
                            Dim RefIt As New BillingReferenceType With { _
                                .InvoiceDocumentReference = New DocumentReferenceType With {.ID = T.Documento_Serie_Numero,
                                                                                            .DocumentTypeCode = T.Documento_Tipo
                                                                                            }}

                            Ref.Add(RefIt)
                        Next
                        ItemDoc.BillingReference = Ref.ToArray
                    End If
                End If
                'ItemDoc.BillingReference = New BillingReferenceType() {New BillingReferenceType With {.InvoiceDocumentReference = New DocumentReferenceType With {.ID = ProvisionItem.Modif_Doc_Serie_Numero, .DocumentTypeCode = ProvisionItem.Modif_Doc_Tipo}}}
                'Estado
                ItemDoc.Status = New StatusType() {New StatusType With {.ConditionCode = ProvisionItem.EstadoDoc_Codigo}}
                ItemDoc.TotalAmount = New AmountType With {.currencyID = ProvisionItem.Dvt_Cod_Moneda, .Value = Format(ProvisionItem.Dvt_Total_Importe, "#0.00")}
                'Gravado -Exonerado - Inafecto - Gratuitas
                Dim Lista_GRA_EXO_INA_GRA As New List(Of BillingPaymentType)
                If ProvisionItem.Dvt_Total_Gravado > 0 Then
                    Lista_GRA_EXO_INA_GRA.Add(New BillingPaymentType With {.PaidAmount = New AmountType With {.currencyID = ProvisionItem.Dvt_Cod_Moneda, .Value = Format(ProvisionItem.Dvt_Total_Gravado, "#0.00")}, .InstructionID = "01"})
                End If
                If ProvisionItem.Dvt_Total_Exonerado > 0 Then
                    Lista_GRA_EXO_INA_GRA.Add(New BillingPaymentType With {.PaidAmount = New AmountType With {.currencyID = ProvisionItem.Dvt_Cod_Moneda, .Value = Format(ProvisionItem.Dvt_Total_Exonerado, "#0.00")}, .InstructionID = "02"})
                End If
                If ProvisionItem.Dvt_Total_Inafecto > 0 Then
                    Lista_GRA_EXO_INA_GRA.Add(New BillingPaymentType With {.PaidAmount = New AmountType With {.currencyID = ProvisionItem.Dvt_Cod_Moneda, .Value = Format(ProvisionItem.Dvt_Total_Inafecto, "#0.00")}, .InstructionID = "03"})
                End If
                If ProvisionItem.Dvt_Total_Gratuitas > 0 Then
                    Lista_GRA_EXO_INA_GRA.Add(New BillingPaymentType With {.PaidAmount = New AmountType With {.currencyID = ProvisionItem.Dvt_Cod_Moneda, .Value = Format(ProvisionItem.Dvt_Total_Gratuitas, "#0.00")}, .InstructionID = "05"})
                End If
                ItemDoc.BillingPayment = Lista_GRA_EXO_INA_GRA.ToArray
                'Importe total de otros cargos del ítem
                'ItemDoc.AllowanceCharge = New AllowanceChargeType With {.ChargeIndicator = False, .Amount = New AmountType With {.currencyID = ProvisionItem.Dvt_Cod_Moneda, .Value = 0}}
                'ISC-IGV-OTROS
                Dim Lista_ISC_IGV_OTR As New List(Of TaxTotalType)
                If ProvisionItem.Dvt_Total_ISC > 0 Then
                    Lista_ISC_IGV_OTR.Add(New TaxTotalType With {.TaxAmount = New AmountType With {.currencyID = ProvisionItem.Dvt_Cod_Moneda, .Value = Format(ProvisionItem.Dvt_Total_ISC, "#0.00")}, .TaxSubtotal = New TaxSubtotalType() {New TaxSubtotalType With {.TaxAmount = New AmountType With {.currencyID = ProvisionItem.Dvt_Cod_Moneda, .Value = Format(ProvisionItem.Dvt_Total_ISC, "#0.00")}, .TaxCategory = New TaxCategoryType With {.TaxScheme = New TaxSchemeType With {.ID = "2000", .Name = "ISC", .TaxTypeCode = "EXC"}}}}})
                End If
                If ProvisionItem.Dvt_Total_IGV > 0 Then
                    Lista_ISC_IGV_OTR.Add(New TaxTotalType With {.TaxAmount = New AmountType With {.currencyID = ProvisionItem.Dvt_Cod_Moneda, .Value = Format(ProvisionItem.Dvt_Total_IGV, "#0.00")}, .TaxSubtotal = New TaxSubtotalType() {New TaxSubtotalType With {.TaxAmount = New AmountType With {.currencyID = ProvisionItem.Dvt_Cod_Moneda, .Value = Format(ProvisionItem.Dvt_Total_IGV, "#0.00")}, .TaxCategory = New TaxCategoryType With {.TaxScheme = New TaxSchemeType With {.ID = "1000", .Name = "IGV", .TaxTypeCode = "VAT"}}}}})
                End If
                If ProvisionItem.Dvt_Total_Otros > 0 Then
                    Lista_ISC_IGV_OTR.Add(New TaxTotalType With {.TaxAmount = New AmountType With {.currencyID = ProvisionItem.Dvt_Cod_Moneda, .Value = Format(ProvisionItem.Dvt_Total_Otros, "#0.00")}, .TaxSubtotal = New TaxSubtotalType() {New TaxSubtotalType With {.TaxAmount = New AmountType With {.currencyID = ProvisionItem.Dvt_Cod_Moneda, .Value = Format(ProvisionItem.Dvt_Total_Otros, "#0.00")}, .TaxCategory = New TaxCategoryType With {.TaxScheme = New TaxSchemeType With {.ID = "9999", .Name = "OTROS", .TaxTypeCode = "OTH"}}}}})
                End If
                ItemDoc.TaxTotal = Lista_ISC_IGV_OTR.ToArray


                ListaDetalles.Add(ItemDoc)
            Next
        End If

        'Creando cabecera
        '--IssueDate:fecha de generacion del resumen --ReferenceDate--fecha de emision de los documentos
        DespatchAdvice = New UblLarsen.Ubl2.SummaryDocumentsType() With {
            .ID = IDResumen,
            .ReferenceDate = Cabecera.Dvt_Fecha_Emision,
            .IssueDate = Cabecera.Dvt_Fecha_Generacion,
             .Signature = New SignatureType() {New SignatureType With {.ID = "IDSignKG",
                                                                       .SignatoryParty = New PartyType With {
                         .PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType With {.ID = Cabecera.Emisor_RUC}},
                         .PartyName = New PartyNameType() {New PartyNameType With {.Name = Cabecera.Emisor_RazonSocial_Nombre}}},
                                                                       .DigitalSignatureAttachment = New AttachmentType With {
                                                                           .ExternalReference = New ExternalReferenceType With {.URI = "#SIGSERV"}}}},
            .AccountingSupplierParty = New SupplierPartyType With {.CustomerAssignedAccountID = New IdentifierType With {.Value = Cabecera.Emisor_RUC},
                                                                   .AdditionalAccountID = New IdentifierType() {New IdentifierType With {.Value = "6"}},
                                                                   .Party = New PartyType With {.PartyName = New PartyNameType() {New PartyNameType With {.Name = Cabecera.Emisor_RazonSocial_Nombre}},
                                                                                                .PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType With {.RegistrationName = Cabecera.Emisor_RazonSocial_Nombre}}}},
            .SummaryDocumentsLine = ListaDetalles.ToArray
         }
        DespatchAdvice.UBLExtensions = New UBLExtensionType() {New UBLExtensionType() With {.ExtensionContent = New ExtensionContentType()}}


        'Agregando namespace
        Dim ns As XmlSerializerNamespaces = New XmlSerializerNamespaces()
        ns.Add("ds", "http://www.w3.org/2000/09/xmldsig#")
        ns.Add("sac", "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")

        'ns.Add("ccts", "urn:oasis:names:specification:ubl:schema:xsd:CoreComponentParameters-2")
        'ns.Add("stat", "urn:oasis:names:specification:ubl:schema:xsd:DocumentStatusCode-1.0")

        Dim settings As New XmlWriterSettings With {.ConformanceLevel = ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}
        Using writer As XmlWriter = XmlWriter.Create(Cabecera.RootPath_Enterprise & "\xml_temp\" & xmlFilename, settings)
            Dim typeToSerialize As Type = GetType(UblLarsen.Ubl2.SummaryDocumentsType)
            Dim xs As New XmlSerializer(typeToSerialize)
            xs.Serialize(writer, DespatchAdvice, ns)
        End Using

        ProcesoCompleto.ExtensionContent_Indice = 0

        '-remuevo los namespace q no sirven
        'Dim docx As New XmlDocument
        'docx.Load(Cabecera.RootPath_Enterprise & "\xml_temp\" & xmlFilename)
        'Dim rootx As XmlElement = docx.DocumentElement
        'rootx.RemoveAttributeAt(1)
        'rootx.RemoveAttributeAt(2)
        'docx.Save(Cabecera.RootPath_Enterprise & "\xml_temp\" & xmlFilename)

        ProcesoCompleto.FileNameWithOutExtension = Filename
        ProcesoCompleto.ArchivoCreadoCorrectamente = True

        Return ProcesoCompleto
    End Function
    Sub XMLFirmar(Provision As eProvisionFacturacion, Credenciales As eCertificadoCredencial, pProceso As fcProceso)
        Try
            ' Vamos a firmar el XML con la ruta del certificado que está como serializado.

            Dim certificate = New X509Certificate2()
            certificate.Import(Credenciales.Ruta, Credenciales.Contrasena, X509KeyStorageFlags.MachineKeySet)

            Dim xmlDoc = New XmlDocument()
            xmlDoc.PreserveWhitespace = True
            xmlDoc.Load(Provision.RootPath_Enterprise & "\xml_temp\" & pProceso.FileNameXML)

            Dim nodoExtension = xmlDoc.GetElementsByTagName("ExtensionContent", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2").Item(pProceso.ExtensionContent_Indice) '1 - 2 nodos, 0 un solo nodo
            If nodoExtension Is Nothing Then
                Throw New InvalidOperationException("No se pudo encontrar el nodo ExtensionContent en el XML")
            End If
            nodoExtension.RemoveAll()

            ' Creamos el objeto SignedXml.
            Dim signedXml = New SignedXml(xmlDoc) With {
            .SigningKey = DirectCast(certificate.PrivateKey, System.Security.Cryptography.RSA)
        }
            signedXml.SigningKey = certificate.PrivateKey
            Dim xmlSignature = signedXml.Signature

            Dim env = New XmlDsigEnvelopedSignatureTransform()

            Dim reference = New Reference(String.Empty)
            reference.AddTransform(env)
            xmlSignature.SignedInfo.AddReference(reference)

            Dim keyInfo = New KeyInfo()
            Dim x509Data = New KeyInfoX509Data(certificate)

            x509Data.AddSubjectName(certificate.Subject)

            keyInfo.AddClause(x509Data)
            xmlSignature.KeyInfo = keyInfo
            xmlSignature.Id = "#signatureGO"
            signedXml.ComputeSignature()

            'Dim oDigestValue As String
            'Dim oValorFirma As String

            '' Recuperamos el valor Hash de la firma para este documento.
            'If reference.DigestValue IsNot Nothing Then
            '    oDigestValue = Convert.ToBase64String(reference.DigestValue)
            '    Provision.Print_DigestValue = oDigestValue
            'End If
            'oValorFirma = Convert.ToBase64String(signedXml.SignatureValue)

            'Provision.Print_SignatureValue = oValorFirma

            nodoExtension.AppendChild(signedXml.GetXml())

            xmlDoc.Save(Provision.RootPath_Enterprise & "\xml_temp\" & pProceso.FileNameXML)

            pProceso.ArchivoFirmadoCorrectamente = True
        Catch ex As Exception
            pProceso.ArchivoFirmadoCorrectamente = False
        End Try


    End Sub
    Sub XMLZip(pProceso As fcProceso)
        Try
            If IO.File.Exists(pProceso.ServidorRutaLocal & "\xml_temp\" & pProceso.FileNameZIP) Then
                IO.File.Delete(pProceso.ServidorRutaLocal & "\xml_temp\" & pProceso.FileNameZIP)
            End If

            Using archive As ZipArchive = ZipFile.Open(pProceso.ServidorRutaLocal & "\xml_temp\" & pProceso.FileNameZIP, ZipArchiveMode.Create)
                ZipFileExtensions.CreateEntryFromFile(archive, (pProceso.ServidorRutaLocal & "\xml_temp\" & pProceso.FileNameXML), pProceso.FileNameXML, CompressionLevel.Fastest)
            End Using
            pProceso.ArchivoComprimidoCorrectamente = True
        Catch ex As Exception
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "XMLZIP-SUB", ex.Message)
            pProceso.ArchivoComprimidoCorrectamente = False
        End Try
    End Sub
    Sub XMLCopiarGenerado(nProceso As fcProceso)
        Try
            'Creamos el directorio en caso no exista, en caso sea un nuevo documento
            If Not IO.Directory.Exists(nProceso.ServidorRuta_Generado) Then
                IO.Directory.CreateDirectory(nProceso.ServidorRuta_Generado)
            End If
            If IO.File.Exists(nProceso.ServidorRuta_Temporal & nProceso.FileNameXML) Then
                IO.File.Copy(nProceso.ServidorRuta_Temporal & nProceso.FileNameXML, nProceso.ServidorRuta_Generado & nProceso.FileNameXML, True)
            End If
            If IO.File.Exists(nProceso.ServidorRuta_Temporal & nProceso.FileNameZIP) Then
                IO.File.Copy(nProceso.ServidorRuta_Temporal & nProceso.FileNameZIP, nProceso.ServidorRuta_Generado & nProceso.FileNameZIP, True)
            End If
            nProceso.GeneradosGuardadoServidorCorrectamente = True
        Catch ex As Exception
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "XMLCOPIAR-GENERADO", ex.Message)
            nProceso.GeneradosGuardadoServidorCorrectamente = False
        End Try
    End Sub
    Sub LimpiarDirectorio(nProceso As fcProceso)
        Try
            IO.File.Delete(nProceso.ServidorRuta_Temporal & nProceso.FileNameXML)
        Catch ex As Exception
            'log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CLEARDIREC", ex.Message)
        End Try
        Try
            IO.File.Delete(nProceso.ServidorRuta_Temporal & nProceso.FileNameZIP)
        Catch ex As Exception
            'log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CLEARDIREC", ex.Message)
        End Try
        Try
            IO.File.Delete(nProceso.ServidorRuta_Temporal & nProceso.FileNamePDF)
        Catch ex As Exception
            'log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CLEARDIREC", ex.Message)
        End Try
        Try
            IO.File.Delete(nProceso.ServidorRuta_Temporal & nProceso.CDRNameZIP)
        Catch ex As Exception
            'log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CLEARDIREC", ex.Message)
        End Try
        Try
            IO.File.Delete(nProceso.ServidorRuta_Temporal & nProceso.CDRNameXML)
        Catch ex As Exception
            'log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CLEARDIREC", ex.Message)
        End Try
    End Sub

    Public Function SUNAT_Summary_Voided_Envio(nProceso As fcProceso) As eSunatRespuesta
        Dim RptaSUNAT As New eSunatRespuesta
        Dim CDR_Name_Zip As String = nProceso.FileNameWithOutExtension & "-CDR.ZIP"
        Dim CDR_Name_Xml As String = "R-" & nProceso.FileNameWithOutExtension & ".XML"
        nProceso.CDRNameZIP = CDR_Name_Zip
        nProceso.CDRNameXML = CDR_Name_Xml
        '--BETA
        Dim DataByteArray As Byte() = System.IO.File.ReadAllBytes(nProceso.ServidorRuta_Generado & nProceso.FileNameZIP)

        System.Net.ServicePointManager.UseNagleAlgorithm = True
        System.Net.ServicePointManager.Expect100Continue = False
        System.Net.ServicePointManager.CheckCertificateRevocationList = True

        Dim binding As New ServiceModel.BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential)
        binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None
        binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None
        binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName
        binding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Default


        Dim SunatWebService As String = "https://e-beta.sunat.gob.pe/ol-ti-itcpfegem-beta/billService"
        Dim UserSUNAT As String = "MODDATOS"
        Dim PassSUNAT As String = "moddatos"
        If nProceso.Etapa = "001" Then
            SunatWebService = "https://e-beta.sunat.gob.pe/ol-ti-itcpfegem-beta/billService"
        ElseIf nProceso.Etapa = "002" Then
            SunatWebService = "https://www.sunat.gob.pe/ol-ti-itcpgem-sqa/billService"
            UserSUNAT = nProceso.SUNAT_Usuario.Trim
            PassSUNAT = nProceso.SUNAT_Contrasena.Trim
        ElseIf nProceso.Etapa = "003" Then
            SunatWebService = " https://e-factura.sunat.gob.pe/ol-ti-itcpfegem/billService"
            UserSUNAT = nProceso.SUNAT_Usuario.Trim
            PassSUNAT = nProceso.SUNAT_Contrasena.Trim
        End If


        Dim remoteAddress As New EndpointAddress(SunatWebService)
        Dim client As New WSSUNAT_Beta_FactBoleta_Bill.billServiceClient(binding, remoteAddress)
        client.ClientCredentials.UserName.UserName = nProceso.Emisor_RUC & UserSUNAT
        client.ClientCredentials.UserName.Password = PassSUNAT
        Dim bindingElementsInTopDownChannelStackOrder As BindingElementCollection = client.Endpoint.Binding.CreateBindingElements
        bindingElementsInTopDownChannelStackOrder.Find(Of SecurityBindingElement).EnableUnsecuredResponse = True
        client.Endpoint.Binding = New CustomBinding(bindingElementsInTopDownChannelStackOrder)

        Dim Retorno_Ticket_ID As String = ""

        Try
            Retorno_Ticket_ID = client.sendSummary(nProceso.FileNameZIP, DataByteArray, "")
        Catch exception As FaultException
            Dim str10 As String = exception.Code.Name.ToUpper.Replace("SOAP-ENV-SERVER.", "").Replace("SOAP-ENV-SERVER", "").Replace("CLIENT.", "")
            str10 = HerramientasFE.SoloNumeros(str10)
            Dim str11 As String = ("FaultException Validación: " & exception.Message)
            RptaSUNAT.Codigo = str10 'exception.Message' str10
            RptaSUNAT.Mensaje = str11
            RptaSUNAT.Estado = EstadoDocumento.Rechazado

            'Log.Instancia.escribirLinea(Log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL", RptaSUNAT.Codigo & "|" & RptaSUNAT.Mensaje)

            Return RptaSUNAT
        Catch exception3 As Exception
            RptaSUNAT.Codigo = "-1"
            RptaSUNAT.Mensaje = "El archivo XML ha sido generado, pero aún no ha sido válidado con la SUNAT debido a que no hay conexión con la URL: " & SunatWebService
            'MessageBox.Show(RptaSUNAT.Mensaje)
            'Log.Instancia.escribirLinea(Log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL", RptaSUNAT.Codigo & "|" & RptaSUNAT.Mensaje)

            Return RptaSUNAT
        End Try

        If String.IsNullOrWhiteSpace(Retorno_Ticket_ID) Then
            RptaSUNAT.Ticket = String.Empty
            RptaSUNAT.Codigo = "-1"
            RptaSUNAT.Mensaje = "El archivo XML ha sido generado, pero no se ha generado el Ticket"
            'MessageBox.Show(RptaSUNAT.Mensaje)
            'sunat.RutaXML_DLL = pRutaXML
            'sunat.Estado = Estado.Generado
        Else
            RptaSUNAT.Ticket = Retorno_Ticket_ID
            RptaSUNAT.Codigo = "0"
            RptaSUNAT.Mensaje = ("Se acaba de generar el Ticket : " & Retorno_Ticket_ID)
            'MessageBox.Show(RptaSUNAT.Mensaje)
            'sunat.RutaXML_DLL = pRutaXML
            'sunat.Estado = Estado.Aceptado
        End If

        If (Convert.ToInt32(RptaSUNAT.Codigo) = 0) Then
            RptaSUNAT.Estado = EstadoDocumento.Aceptado
            log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "ACEPTADO|" & nProceso.FileNameXML & "-" & RptaSUNAT.Mensaje)
            'ElseIf ((Convert.ToInt32(RptaSUNAT.Codigo) >= 1) AndAlso (Convert.ToInt32(RptaSUNAT.Codigo) <= 199)) Then
            '    RptaSUNAT.Estado = EstadoDocumento.Invalido
            '    log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "INVALIDO|" & nProceso.FileNameXML)
            'ElseIf (Convert.ToInt32(RptaSUNAT.Codigo) >= 200) Then
        Else
            RptaSUNAT.Estado = EstadoDocumento.Rechazado
            log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "RECHAZADO|" & nProceso.FileNameXML)
        End If

        Return RptaSUNAT
    End Function
    Dim Ejecutando_Verificacion_Tickets As Boolean
    Private Sub TimerActualizarEstadoResumen_Tick(sender As Object, e As EventArgs) Handles TimerActualizarEstadoResumen.Tick
        If Not Ejecutando_Verificacion_Tickets Then
            Dim thResumen_estado_ticket As New Threading.Thread(AddressOf Consultar_Estado_Ticket_Resumen_Diario)
            thResumen_estado_ticket.Start()
        End If
    End Sub
    Dim CertificadoDigital_Resumen_Verificar_Estado_Ticket As eCertificadoCredencial
    Sub Consultar_Estado_Ticket_Resumen_Diario()
        Ejecutando_Verificacion_Tickets = True
        Try
            Dim Pendientes_Verificacion_Resumen As List(Of eProvisionFacturacion)
            Dim Log As New Log_FACTE_DOCUMENTOS
            Pendientes_Verificacion_Resumen = Log.Buscar_Resumen_Boleta_Pendientes_Respuesta_Busqueda
            Dim Her As New HerramientasFE
            If Her.InternetConnection() Then
                For Each Pend In Pendientes_Verificacion_Resumen

                    If IsNothing(CertificadoDigital_Resumen) Then
                        CertificadoDigital_Resumen = BuscarCredenciales(Pend)
                    End If
                    If CertificadoDigital_Resumen.Emisor_RUC <> Pend.Emisor_RUC Then
                        CertificadoDigital_Resumen = BuscarCredenciales(Pend)
                    End If

                    Dim nProceso As New fcProceso
                    nProceso.Emisor_RUC = CertificadoDigital_Resumen.Emisor_RUC
                    nProceso.SUNAT_Usuario = CertificadoDigital_Resumen.Sunat_Usuario
                    nProceso.SUNAT_Contrasena = CertificadoDigital_Resumen.Sunat_Contrasena
                    nProceso.Etapa = CertificadoDigital_Resumen.Etapa_Codigo
                    nProceso.ServidorRutaLocal = Pend.RootPath_Enterprise
                    nProceso.TicketNumero = Pend.Sunat_Respuesta_Ticket
                    nProceso.FileNameWithOutExtension = Path.GetFileNameWithoutExtension(Pend.Nombre_XML)
                    nProceso.ResumenDiario = True
                    nProceso.Es_OSE = CertificadoDigital_Resumen.Es_OSE
                    Dim Rspta = SummaryVoided_BoletaVenta_EstadoTicket(nProceso)
                    'Rspta.Estado = EstadoDocumento.Aceptado
                    If Rspta.Estado = EstadoDocumento.Aceptado Then
                        'nProceso.ResumenDiario = True
                        Pend.Sunat_Respuesta_Codigo = Rspta.Codigo
                        Pend.Sunat_Respuesta_Fecha = Rspta.Fecha_Respuesta
                        'Pend.Sunat_Respuesta_Descripcion = Rspta.Mensaje
                        'Pend.Sunat_Respuesta_Ticket = Rspta.Ticket
                        Pend.Vnt_EstadoEnvio = "007"
                        Log.Resumen_Cambiar_Estado(Pend)
                        XMLCopiarAceptado(nProceso)
                        'No se envia nada por correo al cliente
                    ElseIf Rspta.Estado = EstadoDocumento.Rechazado Then

                        Pend.Sunat_Respuesta_Codigo = Rspta.Codigo
                        'Pend.Sunat_Respuesta_Descripcion = Rspta.Mensaje
                        'Pend.Sunat_Respuesta_Ticket = Rspta.Ticket
                        Pend.Vnt_EstadoEnvio = "008"
                        Log.Resumen_Cambiar_Estado(Pend)
                        XMLCopiarRechazado(nProceso)
                    ElseIf Rspta.Estado = EstadoDocumento.Invalido Or Rspta.Estado = EstadoDocumento.Error Then
                        Pend.Sunat_Respuesta_Codigo = Rspta.Codigo
                        'Pend.Sunat_Respuesta_Descripcion = Rspta.Mensaje
                        'Pend.Sunat_Respuesta_Ticket = Rspta.Ticket

                        Pend.Vnt_EstadoEnvio = "013"

                        Log.Resumen_Cambiar_Estado(Pend)
                    End If
                Next
            End If
            Consultar_Estado_Ticket_Comunicacion_Baja()
        Catch ex As Exception

        End Try
        Ejecutando_Verificacion_Tickets = False
    End Sub
    Sub Consultar_Estado_Ticket_Comunicacion_Baja()
        Try
            Dim Pendientes_Verificacion_Baja As List(Of eProvisionFacturacion)
            Dim Log As New Log_FACTE_DOCUMENTOS
            Pendientes_Verificacion_Baja = Log.Buscar_Comunicacion_Baja_Pendientes_Respuesta_Busqueda
            For Each Pend In Pendientes_Verificacion_Baja
                Dim Her As New HerramientasFE
                If Her.InternetConnection() Then
                    If IsNothing(CertificadoDigital_Resumen) Then
                        CertificadoDigital_Resumen = BuscarCredenciales(Pend)
                    End If
                    If CertificadoDigital_Resumen.Emisor_RUC <> Pend.Emisor_RUC Then
                        CertificadoDigital_Resumen = BuscarCredenciales(Pend)
                    End If

                    Dim nProceso As New fcProceso
                    nProceso.Emisor_RUC = CertificadoDigital_Resumen.Emisor_RUC
                    nProceso.SUNAT_Usuario = CertificadoDigital_Resumen.Sunat_Usuario
                    nProceso.SUNAT_Contrasena = CertificadoDigital_Resumen.Sunat_Contrasena
                    nProceso.Etapa = CertificadoDigital_Resumen.Etapa_Codigo
                    nProceso.ServidorRutaLocal = Pend.RootPath_Enterprise
                    nProceso.TicketNumero = Pend.Sunat_Respuesta_Ticket
                    nProceso.FileNameWithOutExtension = Path.GetFileNameWithoutExtension(Pend.Nombre_XML)
                    nProceso.ComunicacionBaja = True
                    nProceso.Es_OSE = CertificadoDigital_Resumen.Es_OSE
                    Dim Rspta = SummaryVoided_BoletaVenta_EstadoTicket(nProceso)
                    'Rspta.Estado = EstadoDocumento.Aceptado
                    If Rspta.Estado = EstadoDocumento.Aceptado Then
                        'nProceso.ResumenDiario = True
                        Pend.Sunat_Respuesta_Codigo = Rspta.Codigo
                        Pend.Sunat_Respuesta_Fecha = Rspta.Fecha_Respuesta
                        'Pend.Sunat_Respuesta_Descripcion = Rspta.Mensaje
                        'Pend.Sunat_Respuesta_Ticket = Rspta.Ticket
                        Pend.Vnt_EstadoEnvio = "011"
                        Log.Baja_Cambiar_Estado(Pend)
                        XMLCopiarAceptado(nProceso)
                        'No se envia nada por correo al cliente
                    ElseIf Rspta.Estado = EstadoDocumento.Rechazado Then

                        Pend.Sunat_Respuesta_Codigo = Rspta.Codigo
                        'Pend.Sunat_Respuesta_Descripcion = Rspta.Mensaje
                        'Pend.Sunat_Respuesta_Ticket = Rspta.Ticket
                        Pend.Vnt_EstadoEnvio = "012"
                        Log.Baja_Cambiar_Estado(Pend)
                        XMLCopiarRechazado(nProceso)
                    ElseIf Rspta.Estado = EstadoDocumento.Invalido Or Rspta.Estado = EstadoDocumento.Error Then
                        Pend.Sunat_Respuesta_Codigo = Rspta.Codigo
                        Pend.Vnt_EstadoEnvio = "013"
                        Log.Baja_Cambiar_Estado(Pend)
                    End If
                End If
            Next
        Catch ex As Exception
        End Try
    End Sub
    Function SummaryVoided_BoletaVenta_EstadoTicket(nProceso As fcProceso) As eSunatRespuesta
        Dim RptaSUNAT As New eSunatRespuesta
        'If nProceso.Etapa = "001" Then 'BETA
        If nProceso.Es_OSE Then
            RptaSUNAT = SUNAT_Produccion_BoletaVenta_SummaryVoided_EstadoTicket_OSE(nProceso)
        Else
            RptaSUNAT = SUNAT_Produccion_BoletaVenta_SummaryVoided_EstadoTicket(nProceso)
        End If

        'ElseIf nProceso.Etapa = "002" Then 'HOMOLOGACION
        '    RptaSUNAT = SUNAT_Produccion_BoletaVenta_SummaryVoided_EstadoTicket(nProceso)
        'ElseIf nProceso.Etapa = "003" Then 'PRODUCCION
        '    RptaSUNAT = SUNAT_Produccion_BoletaVenta_SummaryVoided_EstadoTicket(nProceso)
        'End If
        '--------------------------------- Lista de activos
        'statusCode : 0 = Procesó correctamente 98 = En proceso 99 = Proceso con errores
        If RptaSUNAT.Codigo = "0" Then
            RptaSUNAT.Estado = EstadoDocumento.Aceptado
        ElseIf RptaSUNAT.Codigo = "99" Then
            RptaSUNAT.Estado = EstadoDocumento.Rechazado
        ElseIf RptaSUNAT.Codigo = "-1" Then
            RptaSUNAT.Estado = EstadoDocumento.Error
        Else
            RptaSUNAT.Estado = EstadoDocumento.Invalido
        End If
        Return RptaSUNAT
    End Function
    Public Function SUNAT_Produccion_BoletaVenta_SummaryVoided_EstadoTicket(nProceso As fcProceso) As eSunatRespuesta
        Dim sunat As New eSunatRespuesta
        Dim CDR_Name_Zip As String = nProceso.FileNameWithOutExtension & "-CDR.ZIP"
        Dim CDR_Name_Xml As String = "R-" & nProceso.FileNameWithOutExtension & ".XML"
        Dim CDR_Directory_Descompression As String = nProceso.ServidorRuta_Aceptado  'Application.StartupPath
        nProceso.CDRNameZIP = CDR_Name_Zip

        Try
            Net.ServicePointManager.UseNagleAlgorithm = True
            Net.ServicePointManager.Expect100Continue = False
            Net.ServicePointManager.CheckCertificateRevocationList = True
            Dim binding As New BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential)
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None
            binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None
            binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName
            binding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Default
            Dim SunatWebService As String = "https://e-beta.sunat.gob.pe/ol-ti-itcpfegem-beta/billService"
            Dim UserSUNAT As String = "MODDATOS"
            Dim PassSUNAT As String = "moddatos"
            If nProceso.Etapa = "001" Then
                SunatWebService = "https://e-beta.sunat.gob.pe/ol-ti-itcpfegem-beta/billService"
            ElseIf nProceso.Etapa = "002" Then
                SunatWebService = "https://www.sunat.gob.pe/ol-ti-itcpgem-sqa/billService"
                UserSUNAT = nProceso.SUNAT_Usuario.Trim
                PassSUNAT = nProceso.SUNAT_Contrasena.Trim
            ElseIf nProceso.Etapa = "003" Then
                SunatWebService = " https://e-factura.sunat.gob.pe/ol-ti-itcpfegem/billService"
                UserSUNAT = nProceso.SUNAT_Usuario.Trim
                PassSUNAT = nProceso.SUNAT_Contrasena.Trim
            End If
            Dim remoteAddress As New EndpointAddress(SunatWebService)

            Dim client As New WSSUNAT_Produccion_FactBoleta_Bill.billServiceClient(binding, remoteAddress)
            client.ClientCredentials.UserName.UserName = nProceso.Emisor_RUC & nProceso.SUNAT_Usuario
            client.ClientCredentials.UserName.Password = nProceso.SUNAT_Contrasena
            Dim bindingElementsInTopDownChannelStackOrder As BindingElementCollection = client.Endpoint.Binding.CreateBindingElements
            bindingElementsInTopDownChannelStackOrder.Find(Of SecurityBindingElement).EnableUnsecuredResponse = True
            client.Endpoint.Binding = New CustomBinding(bindingElementsInTopDownChannelStackOrder)
            sunat.Ticket = nProceso.TicketNumero
            Dim response As WSSUNAT_Produccion_FactBoleta_Bill.statusResponse = client.getStatus(nProceso.TicketNumero)
            Try
                Dim content As Byte() = response.content 'client.getStatus(nProceso.TicketNumero).content
                If (Not content Is Nothing) Then
                    If Not IO.Directory.Exists(nProceso.ServidorRuta_Aceptado) Then
                        IO.Directory.CreateDirectory(nProceso.ServidorRuta_Aceptado)
                    End If
                    File.WriteAllBytes(nProceso.ServidorRuta_Aceptado & CDR_Name_Zip, content)
                    If File.Exists((CDR_Directory_Descompression & CDR_Name_Xml)) Then
                        File.Delete((CDR_Directory_Descompression & CDR_Name_Xml))
                    End If

                    If HerramientasFE.SoloNumeros(response.statusCode).Trim <> "0127" Then
                        Compression.ZipFile.ExtractToDirectory(nProceso.ServidorRuta_Aceptado & CDR_Name_Zip, CDR_Directory_Descompression)
                        Dim document As New XmlDocument
                        document.Load((CDR_Directory_Descompression & CDR_Name_Xml))
                        Dim childNodes As XmlNodeList = document.GetElementsByTagName("DocumentResponse", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2").Item(0).FirstChild.ChildNodes
                        Dim innerText As String = childNodes.ItemOf(1).InnerText
                        Dim str11 As String = childNodes.ItemOf(2).InnerText
                        sunat.Codigo = innerText
                        sunat.Mensaje = str11
                        Dim Nodes As XmlNode = document.GetElementsByTagName("ResponseDate", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2").Item(0)
                        Dim Fecha As Date = CDate(Nodes.InnerText)
                        Nodes = document.GetElementsByTagName("ResponseTime", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2").Item(0)
                        Dim Hora As TimeSpan = TimeSpan.Parse(Nodes.InnerText)
                        sunat.Fecha_Respuesta = Fecha.Add(Hora)

                        'sunat.RutaXML_DLL = pRutaXML
                        'sunat.RutaXML_SNT = (destinationDirectoryName & str7)
                    Else
                        Dim str12 As String = response.statusCode
                        Dim message As String = "El ticket " & nProceso.TicketNumero & " no existe"
                        sunat.Codigo = str12
                        sunat.Mensaje = message
                        log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETAGETSTATUS-SUMMARYBOL-PROD", sunat.Codigo & "|" & sunat.Mensaje)
                    End If
                End If

                Return sunat
            Catch exception As FaultException
                Dim str12 As String = exception.Code.Name.ToUpper.Replace("SOAP-ENV-SERVER.", "").Replace("SOAP-ENV-SERVER", "")
                Dim message As String = exception.Message
                sunat.Codigo = HerramientasFE.SoloNumeros(str12)
                sunat.Mensaje = message
                log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETAGETSTATUS-SUMMARYBOL-PROD", sunat.Codigo & "|" & sunat.Mensaje)
            End Try
            Return sunat
        Catch exception2 As Exception
            sunat.Codigo = "-1"
            sunat.Mensaje = exception2.Message
        Finally
            'If File.Exists(pathX) Then
            '    File.Delete(pathX)
            'End If
        End Try
        Return sunat
    End Function

    Dim CertificadoDigital_Baja As eCertificadoCredencial
    Private Bajas_PendienteEnvio As New List(Of eProvisionFacturacion)
    Dim Enviar_Bajas_Ejecutando As Boolean
    Private Sub TimerEnviarBajasFB_Tick(sender As Object, e As EventArgs) Handles TimerEnviarBajasFB.Tick
        If Not Enviar_Bajas_Ejecutando Then
            Dim thBajas_Pendientes As New Threading.Thread(AddressOf ListasTarea_EnviarBajas)
            thBajas_Pendientes.Start()
        End If
    End Sub
    Sub ListasTarea_EnviarBajas()
        Try
            Enviar_Bajas_Ejecutando = True
            'Resumen_Actualizando = True
            Dim logProgramados As New Log_FACTE_PROGRAMACIONES_ENVIO
            Bajas_PendienteEnvio = logProgramados.Buscar_Lista_Pendientes_Baja()
            'Resumen_Actualizando = False
            Dim Her As New HerramientasFE
            If Bajas_PendienteEnvio.Count > 0 And Her.InternetConnection() Then

                Dim log_Pendientes As New Log_FACTE_PROGRAMACIONES_ENVIO
                For Each Doc_Pendiente In Bajas_PendienteEnvio

                    'If IsNothing(CertificadoDigital_Resumen) Then
                    CertificadoDigital_Resumen = BuscarCredenciales(Doc_Pendiente)
                    'End If
                    If CertificadoDigital_Resumen.Emisor_RUC <> Doc_Pendiente.Emisor_RUC Then
                        CertificadoDigital_Resumen = BuscarCredenciales(Doc_Pendiente)
                    End If

                    Dim Doc_Detalle As List(Of eProvisionFacturacion)
                    Doc_Detalle = log_Pendientes.Buscar_Lista_Pendientes_Baja_Documento(Doc_Pendiente)

                    Dim Provision As eProvisionFacturacion = Doc_Pendiente
                    Provision.CertificadoCredencial = CertificadoDigital_Resumen

                    Dim nProceso As New fcProceso
                    nProceso = Generar_ComunicacionBaja_XML_SUNAT(Provision, Doc_Detalle)

                    If nProceso.ArchivoCreadoCorrectamente Then

                        nProceso.Emisor_RUC = Provision.Emisor_RUC
                        nProceso.SUNAT_Usuario = Provision.CertificadoCredencial.Sunat_Usuario
                        nProceso.SUNAT_Contrasena = Provision.CertificadoCredencial.Sunat_Contrasena

                        nProceso.ServidorRutaLocal = Provision.CertificadoCredencial.Path_Root_App & "\" & Provision.Emisor_RUC
                        nProceso.DocumentoTipo = Doc_Detalle(0).TpDc_Codigo_St 'Provision.TpDc_Codigo
                        nProceso.FTP_Direccion = Provision.CertificadoCredencial.FTP_Direccion
                        nProceso.FTP_Carpeta = Provision.CertificadoCredencial.FTP_Carpeta
                        nProceso.FTP_Usuario = Provision.CertificadoCredencial.FTP_Usuario
                        nProceso.FTP_Contrasena = Provision.CertificadoCredencial.FTP_Contrasena
                        nProceso.Etapa = Provision.CertificadoCredencial.Etapa_Codigo
                        nProceso.Es_OSE = Provision.CertificadoCredencial.Es_OSE
                        '2-XML FIRMA
                        XMLFirmar(Provision, CertificadoDigital_Resumen, nProceso)

                    End If

                    If nProceso.ArchivoFirmadoCorrectamente Then

                        XMLZip(nProceso)



                    End If
                    If nProceso.ArchivoComprimidoCorrectamente Then
                        XMLCopiarGenerado(nProceso)


                    End If
                    If nProceso.GeneradosGuardadoServidorCorrectamente Then
                        LimpiarDirectorio(nProceso)

                        Dim Rspta As eSunatRespuesta
                        Rspta = ZIPEnviar_Resumen_Diario_Comunicacion_Baja(nProceso)

                        Provision.Sunat_Respuesta_Codigo = Rspta.Codigo
                        Provision.Sunat_Respuesta_Descripcion = Rspta.Mensaje
                        Provision.Sunat_Respuesta_Ticket = Rspta.Ticket

                        Provision.Nombre_XML = nProceso.FileNameXML



                        'Provision.EstadoElectronico = "4107" 'BMS
                        'Provision.Vnt_EstadoEnvio = "003" 'FACTE
                        'Cambiamos el estado al documento
                        If Rspta.Estado = EstadoDocumento.Aceptado Then
                            Provision.Vnt_EstadoEnvio = "010"
                        ElseIf Rspta.Estado = EstadoDocumento.Rechazado Then
                            Provision.Vnt_EstadoEnvio = "013"
                        End If

                        If Rspta.Estado <> EstadoDocumento.Error Then
                            Dim LogAcep As New Log_FACTE_DOCUMENTOS
                            LogAcep.Baja_Cambiar_Estado(Provision)
                        End If
                        'Genera PDF
                        'GenerarPDF(Provision, nProceso)
                        ''Copiamos a la carpeta aceptados
                        'XMLCopiarAceptado(nProceso)
                        ''Enviamos el correo al cliente
                        'If EnviarCorreo(Provision, nProceso) Then
                        '    'actualizamos que se envio el correo al cliente
                        '    LogAcep.Actualizar_Envio_Correo(Provision, nProceso)
                        'End If

                    End If

                    'nProceso.FileNameWithOutExtension = Path.GetFileNameWithoutExtension(Provision.Nombre_XML)


                Next
            End If

            Enviar_Bajas_Ejecutando = False
        Catch ex As Exception
            Enviar_Bajas_Ejecutando = False
        End Try

    End Sub
    'Afecta_Boletas-facturas-NotasCredito-NotasDebito
    Function Generar_ComunicacionBaja_XML_SUNAT(Cabecera As eProvisionFacturacion, Detalle As List(Of eProvisionFacturacion)) As fcProceso
        Dim ProcesoCompleto As New fcProceso With {.ArchivoCreadoCorrectamente = False}
        Try
            ProcesoCompleto.ServidorRutaLocal = Cabecera.RootPath_Enterprise
            ProcesoCompleto.ComunicacionBaja = True

            Dim ResumenCorrelativo As String = "RA-" & Format(Cabecera.Dvt_Fecha_Generacion, "yyyyMMdd") & "-" & Cabecera.Rs_Correlativo
            Dim Filename As String = Cabecera.Emisor_RUC & "-" & ResumenCorrelativo
            Dim xmlFilename As String = Filename & ".XML"
            Dim ZipFilename As String = Filename & ".ZIP"

            Dim VoidedDocuments As UblLarsen.Ubl2.VoidedDocumentsType
            UblLarsen.Ubl2.UblBaseDocumentType.GlbCustomizationID = "1.0"
            UblLarsen.Ubl2.UblBaseDocumentType.GlbUblVersionID = "2.0"
            'AmountType.TlsDefaultCurrencyID = Provisiones(0).Dvt_Moneda_Internacional

            'Creando los detalles
            Dim ListaDetalles As VoidedDocumentsLineType() = New VoidedDocumentsLineType(Detalle.Count - 1) {}

            For i = 0 To Detalle.Count - 1
                ListaDetalles(i) = New VoidedDocumentsLineType() With { _
                     .DocumentTypeCode = Detalle(i).TpDc_Codigo_St, _
                     .DocumentSerialID = Detalle(i).Dvt_VTSerie, _
                     .DocumentNumberID = Detalle(i).Dvt_VTNumer, _
                     .VoidReasonDescription = Detalle(i).Vnt_AnulacionRazon,
                     .LineID = Detalle(i).Rs_Correlativo
                    }
            Next

            'Creando cabecera
            '--ReferenceDate -- Fecha de generacion del documento dado de baja
            '--IssueDate -- Fecha de generacion de la comunicacion
            VoidedDocuments = New UblLarsen.Ubl2.VoidedDocumentsType() With { _
                .ID = ResumenCorrelativo, _
                 .IssueDate = Cabecera.Dvt_Fecha_Generacion, _
                        .ReferenceDate = Cabecera.Dvt_Fecha_Emision, _
                .Signature = New SignatureType() {New SignatureType() With {.ID = Cabecera.Emisor_RUC, _
                            .SignatoryParty = New PartyType() With {.PartyIdentification = New PartyIdentificationType() {New PartyIdentificationType() With {.ID = Cabecera.Emisor_RUC}},
                                                                    .PartyName = New PartyNameType() {New PartyNameType() With {.Name = Cabecera.Emisor_RazonSocial_Nombre}}}, _
                            .DigitalSignatureAttachment = New AttachmentType() With {.ExternalReference = New ExternalReferenceType() With {.URI = "#signatureGO"}}}}, _
                 .AccountingSupplierParty = New SupplierPartyType() With { _
                     .CustomerAssignedAccountID = Cabecera.Emisor_RUC, _
                     .AdditionalAccountID = New IdentifierType() {New IdentifierType() With {.Value = "6"}}, _
                     .Party = New PartyType() With { _
                         .PartyLegalEntity = New PartyLegalEntityType() {New PartyLegalEntityType() With {.RegistrationName = Cabecera.Emisor_RazonSocial_Nombre}} _
                    } _
                }, _
                .VoidedDocumentsLine = ListaDetalles
            }


            'Agregando los extension: Firma del documento y campos personalizados
            VoidedDocuments.UBLExtensions = New UBLExtensionType() {New UBLExtensionType() With {.ExtensionContent = New ExtensionContentType()}}

            'Agregando namespace
            Dim ns As XmlSerializerNamespaces = New XmlSerializerNamespaces()
            ns.Add("ccts", "urn:un:unece:uncefact:documentation:2")
            ns.Add("ds", "http://www.w3.org/2000/09/xmldsig#")
            ns.Add("sac", "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")
            ns.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance")
            ns.Add("xsd", "http://www.w3.org/2001/XMLSchema")

            Dim settings As New XmlWriterSettings With {.ConformanceLevel = ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}

            Using writer As XmlWriter = XmlWriter.Create(Cabecera.RootPath_Enterprise & "\xml_temp\" & xmlFilename, settings)
                Dim typeToSerialize As Type = GetType(UblLarsen.Ubl2.VoidedDocumentsType)
                Dim xs As New XmlSerializer(typeToSerialize)
                xs.Serialize(writer, VoidedDocuments, ns)
            End Using
            'ProcesoCompleto.ExtensionContent_Indice = 1

            ProcesoCompleto.FileNameWithOutExtension = Filename
            ProcesoCompleto.ArchivoCreadoCorrectamente = True
        Catch ex As Exception

        End Try
        Return ProcesoCompleto

    End Function

    Private Sub BtnReplicacion_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnReplicacion.ItemClick
        Using f As New FrmReplicacion
            f.ShowDialog()
        End Using
    End Sub

    Private Sub BtnBackup_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnBackup.ItemClick
        Using f As New FrmBackup
            f.ShowDialog()
        End Using
    End Sub
    Dim cReplicador_Trabajando As Boolean = False
    Private Sub TimerReplicador_Tick(sender As Object, e As EventArgs) Handles TimerReplicador.Tick
        Try
            If Not cReplicador_Trabajando Then
                cReplicador_Trabajando = True
                Dim log As New Log_Configuracion_App
                Dim Lista As List(Of eConfiguracionApp) = log.Buscar
                If Lista.Count > 0 Then
                    If Lista(0).App_Replicacion And Lista(0).App_Es_Hora_Replicar Then
                        Dim Ent_ As New eConfiguracionApp

                        Ent_.App_Replicacion_RutaPrincipalArchivos = Lista.Item(0).App_Replicacion_RutaPrincipalArchivos
                        Ent_.App_Replicacion_Servidor = Lista.Item(0).App_Replicacion_Servidor
                        Ent_.App_Replicacion_BD = Lista.Item(0).App_Replicacion_BD
                        Ent_.App_Replicacion_Usuario = Lista.Item(0).App_Replicacion_Usuario
                        Ent_.App_Replicacion_Contrasenia = Lista.Item(0).App_Replicacion_Contrasenia

                        Dim thBajas_Pendientes As New Threading.Thread(AddressOf IniciarReplicacionPendientes)
                        thBajas_Pendientes.Start(Ent_)
                    Else
                        cReplicador_Trabajando = False
                    End If
                End If
            End If
        Catch ex As Exception
            cReplicador_Trabajando = False
        End Try

    End Sub
    Sub IniciarReplicacionPendientes(Cfg As eConfiguracionApp)
        Try
            Dim PathRoot As String = Cfg.App_Replicacion_RutaPrincipalArchivos
            Dim PathBackup_Descompress As String

            Dim CnStr As String = "Data Source=" & Cfg.App_Replicacion_Servidor & ";Initial Catalog=" & Cfg.App_Replicacion_BD & ";User ID=" & Cfg.App_Replicacion_Usuario & ";Password=" & Cfg.App_Replicacion_Contrasenia
            Dim Log As New Log_Replicacion

            Dim ListaSocNeg As List(Of BACK_01_FACTE_SOCIO_NEGOCIO) = Log.Replicar_Pendientes_Socios_Negocio
            For Each SocNeg In ListaSocNeg
                If Log.Replicar_Pendiente_ServidorX_Socio_Negocio(CnStr, SocNeg) Then
                    Log.Replicar_Pendiente_Cambio_Estado_Socio_Negocio(SocNeg)
                End If
            Next

            'For i = 0 To 5000 'Un Sleep artificial Thread.Sleep(5000)
            '    Console.Write(i)
            'Next


            Dim ListaDocCabecera As List(Of BACK_02_FACTE_DOCUMENTO_ELECTRONICO) = Log.Replicar_Pendientes_Documentos_Cabecera

            For Each DocCabecera In ListaDocCabecera
                Try
                    Dim Her As New HerramientasFE
                    If Her.InternetConnection(True, Cfg.App_Replicacion_Servidor) Then
                        If Log.Replicar_Pendiente_ServidorX_Documentos_Cabecera(CnStr, DocCabecera) Then

                            Dim ListaDocAdjuntos As List(Of BACK_03_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO) = Log.Replicar_Pendientes_Documentos_Adjunto(DocCabecera)
                            Dim ListaDocXMLCliente As List(Of BACK_04_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE) = Log.Replicar_Pendientes_Documentos_Cliente(DocCabecera)

                            For Each FileUser In ListaDocXMLCliente
                                PathBackup_Descompress = Path.Combine(PathRoot, FileUser.Doc_Emisor_RUC) ' 
                                If IO.File.Exists(FileUser.Path_Root_App_Empresa & "\xml_pos\" & FileUser.XML_Archivo) Then
                                    IO.File.Copy(FileUser.Path_Root_App_Empresa & "\xml_pos\" & FileUser.XML_Archivo, PathBackup_Descompress & "\xml_pos\" & FileUser.XML_Archivo, True)
                                End If
                            Next

                            For Each FileAttach In ListaDocAdjuntos
                                PathBackup_Descompress = Path.Combine(PathRoot, FileAttach.Doc_Emisor_RUC)

                                If IO.File.Exists(FileAttach.Path_Root_App_Empresa & "\xml_pos_attached\" & FileAttach.XML_Archivo_NombreLocal) Then
                                    IO.File.Copy(FileAttach.Path_Root_App_Empresa & "\xml_pos_attached\" & FileAttach.XML_Archivo_NombreLocal, PathBackup_Descompress & "\xml_pos_attached\" & FileAttach.XML_Archivo_NombreLocal, True)
                                End If
                            Next

                            For Each DocAdjuntos In ListaDocAdjuntos
                                If Log.Replicar_Pendiente_ServidorX_Documentos_Adjunto(CnStr, DocAdjuntos) Then
                                    Log.Replicar_Pendiente_Cambio_Estado_Documentos_Adjunto(DocAdjuntos)
                                End If
                            Next
                            For Each DocXMLCliente In ListaDocXMLCliente
                                If Log.Replicar_Pendiente_ServidorX_Documentos_XMLCliente(CnStr, DocXMLCliente) Then
                                    Log.Replicar_Pendiente_Cambio_Estado_Documentos_XMLCliente(DocXMLCliente)
                                End If
                            Next

                            ''COPIA DE ARCHIVOS
                            PathBackup_Descompress = Path.Combine(PathRoot, DocCabecera.Doc_Emisor_RUC)
                            DocCabecera.Doc_NombreArchivoPDF = IO.Path.GetFileNameWithoutExtension(DocCabecera.Doc_NombreArchivoXML).Trim & ".PDF"
                            If IO.File.Exists(DocCabecera.Path_Root_App_Empresa & ServidorRuta_Generado(DocCabecera.Doc_Tipo) & DocCabecera.Doc_NombreArchivoXML) Then
                                IO.File.Copy(DocCabecera.Path_Root_App_Empresa & ServidorRuta_Generado(DocCabecera.Doc_Tipo) & DocCabecera.Doc_NombreArchivoXML, PathBackup_Descompress & ServidorRuta_Generado(DocCabecera.Doc_Tipo) & DocCabecera.Doc_NombreArchivoXML, True)
                                If DocCabecera.EstEnv_Codigo = "003" Then
                                    If IO.File.Exists(DocCabecera.Path_Root_App_Empresa & ServidorRuta_Aceptado(DocCabecera.Doc_Tipo) & DocCabecera.Doc_NombreArchivoXML) Then
                                        IO.File.Copy(DocCabecera.Path_Root_App_Empresa & ServidorRuta_Aceptado(DocCabecera.Doc_Tipo) & DocCabecera.Doc_NombreArchivoXML, PathBackup_Descompress & ServidorRuta_Aceptado(DocCabecera.Doc_Tipo) & DocCabecera.Doc_NombreArchivoXML, True)
                                    End If
                                    If IO.File.Exists(DocCabecera.Path_Root_App_Empresa & ServidorRuta_Aceptado(DocCabecera.Doc_Tipo) & DocCabecera.Doc_NombreArchivoPDF) Then
                                        IO.File.Copy(DocCabecera.Path_Root_App_Empresa & ServidorRuta_Aceptado(DocCabecera.Doc_Tipo) & DocCabecera.Doc_NombreArchivoPDF, PathBackup_Descompress & ServidorRuta_Aceptado(DocCabecera.Doc_Tipo) & DocCabecera.Doc_NombreArchivoPDF, True)
                                    End If
                                End If

                                'If DocCabecera.EstEnv_Codigo = "004" Then
                                '    If IO.File.Exists(DocCabecera.Path_Root_App_Empresa & ServidorRuta_Rechazado(DocCabecera.Doc_Tipo) & DocCabecera.Doc_NombreArchivoXML) Then
                                '        IO.File.Copy(DocCabecera.Path_Root_App_Empresa & ServidorRuta_Rechazado(DocCabecera.Doc_Tipo) & DocCabecera.Doc_NombreArchivoXML, PathBackup_Descompress & ServidorRuta_Rechazado(DocCabecera.Doc_Tipo) & DocCabecera.Doc_NombreArchivoXML, True)
                                '    End If
                                '    If IO.File.Exists() Then
                                '        IO.File.Copy(PathBackup_Descompress & "\" & DocCabecera.Doc_NombreArchivoPDF, DocCabecera.Path_Root_App_Empresa & "\" & ServidorRuta_Rechazado(DocCabecera.Doc_Tipo) & DocCabecera.Doc_NombreArchivoPDF, True)
                                '    End If
                                'End If
                            End If

                            Log.Replicar_Pendiente_Cambio_Estado_Documentos_Cabecera(DocCabecera)
                        End If
                    End If
                Catch ex As Exception
                    cReplicador_Trabajando = False
                    Exit Sub
                End Try
            Next

            'For Each FileXMLGen In ListaDocCabecera
            '    PathBackup_Descompress = Path.Combine(PathRoot, FileXMLGen.Doc_Emisor_RUC)

            '    'Dim s As String = FileXMLGen.Path_Root_App_Empresa & ServidorRuta_Generado(FileXMLGen.Doc_Tipo) & FileXMLGen.Doc_NombreArchivoXML
            '    'Dim f As String = PathBackup_Descompress & ServidorRuta_Generado(FileXMLGen.Doc_Tipo) & FileXMLGen.Doc_NombreArchivoXML

            '    If IO.File.Exists(FileXMLGen.Path_Root_App_Empresa & ServidorRuta_Generado(FileXMLGen.Doc_Tipo) & FileXMLGen.Doc_NombreArchivoXML) Then


            '        IO.File.Copy(FileXMLGen.Path_Root_App_Empresa & ServidorRuta_Generado(FileXMLGen.Doc_Tipo) & FileXMLGen.Doc_NombreArchivoXML, PathBackup_Descompress & ServidorRuta_Generado(FileXMLGen.Doc_Tipo) & FileXMLGen.Doc_NombreArchivoXML, True)
            '        If FileXMLGen.EstEnv_Codigo = "003" Then
            '            IO.File.Copy(FileXMLGen.Path_Root_App_Empresa & ServidorRuta_Aceptado(FileXMLGen.Doc_Tipo) & FileXMLGen.Doc_NombreArchivoXML, PathBackup_Descompress & ServidorRuta_Aceptado(FileXMLGen.Doc_Tipo) & FileXMLGen.Doc_NombreArchivoXML, True)
            '            If IO.File.Exists(FileXMLGen.Path_Root_App_Empresa & ServidorRuta_Aceptado(FileXMLGen.Doc_Tipo) & FileXMLGen.Doc_NombreArchivoPDF) Then
            '                IO.File.Copy(FileXMLGen.Path_Root_App_Empresa & ServidorRuta_Aceptado(FileXMLGen.Doc_Tipo) & FileXMLGen.Doc_NombreArchivoPDF, PathBackup_Descompress & ServidorRuta_Aceptado(FileXMLGen.Doc_Tipo) & FileXMLGen.Doc_NombreArchivoPDF, True)
            '            End If
            '        End If
            '        If FileXMLGen.EstEnv_Codigo = "004" Then
            '            'IO.File.Copy(FileXMLGen.Path_Root_App_Empresa & ServidorRuta_Rechazado(FileXMLGen.Doc_Tipo) & FileXMLGen.Doc_NombreArchivoXML, PathBackup_Descompress & ServidorRuta_Rechazado(FileXMLGen.Doc_Tipo) & FileXMLGen.Doc_NombreArchivoXML, True)

            '            'IO.File.Copy(PathBackup_Descompress & "\" & FileXMLGen.Doc_NombreArchivoPDF, FileXMLGen.Path_Root_App_Empresa & "\" & ServidorRuta_Rechazado(FileXMLGen.Doc_Tipo) & FileXMLGen.Doc_NombreArchivoPDF, True)
            '        End If
            '    End If
            'Next

        Catch ex As Exception
            'MessageBox.Show(ex.Message)
        End Try


        cReplicador_Trabajando = False
    End Sub

    Function ServidorRuta_Generado(DocumentoTipo As String) As String

        'If ResumenDiario Then
        '    Return (ServidorRutaLocal & RutasServidor.ResumenDiario.Generado)
        'End If
        'If ComunicacionBaja Then
        '    Return (ServidorRutaLocal & RutasServidor.ComunicacionBaja.Generado)
        'End If
        Dim var As String = ""
        If DocumentoTipo = SUNATDocumento.Boleta Then
            var = RutasServidor.Boleta.Generado
        ElseIf DocumentoTipo = SUNATDocumento.Factura Then
            var = RutasServidor.Factura.Generado
        ElseIf DocumentoTipo = SUNATDocumento.NotaCredito Then
            var = RutasServidor.NotaCredito.Generado
        ElseIf DocumentoTipo = SUNATDocumento.NotaDebito Then
            var = RutasServidor.NotaDebito.Generado
        ElseIf DocumentoTipo = SUNATDocumento.GuiaRemisionRemitente Then
            var = RutasServidor.GuiaRemisionRemitente.Generado
        ElseIf DocumentoTipo = SUNATDocumento.ComprobantePercepcion Then
            var = RutasServidor.Percepcion.Generado
        ElseIf DocumentoTipo = SUNATDocumento.ComprobanteRetencion Then
            var = RutasServidor.Retencion.Generado
        End If
        Return var

    End Function
    Function ServidorRuta_Aceptado(DocumentoTipo As String) As String
        'Get
        'If ResumenDiario Then
        '    Return (ServidorRutaLocal & RutasServidor.ResumenDiario.Aceptado)
        'End If
        'If ComunicacionBaja Then
        '    Return (ServidorRutaLocal & RutasServidor.ComunicacionBaja.Aceptado)
        'End If
        Dim var As String = ""
        If DocumentoTipo = SUNATDocumento.Boleta Then
            var = RutasServidor.Boleta.Aceptado
        ElseIf DocumentoTipo = SUNATDocumento.Factura Then
            var = RutasServidor.Factura.Aceptado
        ElseIf DocumentoTipo = SUNATDocumento.NotaCredito Then
            var = RutasServidor.NotaCredito.Aceptado
        ElseIf DocumentoTipo = SUNATDocumento.NotaDebito Then
            var = RutasServidor.NotaDebito.Aceptado
        ElseIf DocumentoTipo = SUNATDocumento.GuiaRemisionRemitente Then
            var = RutasServidor.GuiaRemisionRemitente.Aceptado
        ElseIf DocumentoTipo = SUNATDocumento.ComprobantePercepcion Then
            var = RutasServidor.Percepcion.Aceptado
        ElseIf DocumentoTipo = SUNATDocumento.ComprobanteRetencion Then
            var = RutasServidor.Retencion.Aceptado
        End If
        Return var
        'End Get
    End Function
    Function ServidorRuta_Rechazado(DocumentoTipo As String) As String
        'Get
        'If ResumenDiario Then
        '    Return (ServidorRutaLocal & RutasServidor.ResumenDiario.Rechazado)
        'End If
        'If ComunicacionBaja Then
        '    Return (ServidorRutaLocal & RutasServidor.ComunicacionBaja.Rechazado)
        'End If

        Dim var As String = ""
        If DocumentoTipo = SUNATDocumento.Boleta Then
            var = RutasServidor.Boleta.Rechazado
        ElseIf DocumentoTipo = SUNATDocumento.Factura Then
            var = RutasServidor.Factura.Rechazado
        ElseIf DocumentoTipo = SUNATDocumento.NotaCredito Then
            var = RutasServidor.NotaCredito.Rechazado
        ElseIf DocumentoTipo = SUNATDocumento.NotaDebito Then
            var = RutasServidor.NotaDebito.Rechazado
        ElseIf DocumentoTipo = SUNATDocumento.GuiaRemisionRemitente Then
            var = RutasServidor.GuiaRemisionRemitente.Rechazado
        ElseIf DocumentoTipo = SUNATDocumento.ComprobantePercepcion Then
            var = RutasServidor.Percepcion.Rechazado
        ElseIf DocumentoTipo = SUNATDocumento.ComprobanteRetencion Then
            var = RutasServidor.Retencion.Rechazado
        End If
        Return var
        'End Get
    End Function

    Private Sub BtnTicket_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnTicket.ItemClick
        'Try
        '    Dim ticket As String = InputBox("Introduzca el número de ticket", "CONSULTA COMUNICACION DE BAJA").Trim
        '    Dim Directories As String = Microsoft.VisualBasic.FileIO.SpecialDirectories.Desktop
        '    Dim Zip_Name As String = Directories & "\Estado_consulta.ZIP"
        '    Dim CDR_Name_Xml As String = "R-Estado_consulta.XML"
        '    Net.ServicePointManager.UseNagleAlgorithm = True
        '    Net.ServicePointManager.Expect100Continue = False
        '    Net.ServicePointManager.CheckCertificateRevocationList = True
        '    Dim binding As New BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential)
        '    binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None
        '    binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None
        '    binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName
        '    binding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Default
        '    Dim remoteAddress As New EndpointAddress(" https://e-factura.sunat.gob.pe/ol-ti-itcpfegem/billService")
        '    Dim client As New WSSUNAT_Produccion_FactBoleta_Bill.billServiceClient(binding, remoteAddress)
        '    client.ClientCredentials.UserName.UserName = "20542134926EESO2017"
        '    client.ClientCredentials.UserName.Password = "EESO2017"
        '    Dim bindingElementsInTopDownChannelStackOrder As BindingElementCollection = client.Endpoint.Binding.CreateBindingElements
        '    bindingElementsInTopDownChannelStackOrder.Find(Of SecurityBindingElement).EnableUnsecuredResponse = True
        '    client.Endpoint.Binding = New CustomBinding(bindingElementsInTopDownChannelStackOrder)
        '    Dim response As WSSUNAT_Produccion_FactBoleta_Bill.statusResponse = client.getStatus(ticket)
        '    Try
        '        Dim content As Byte() = client.getStatus(ticket).content
        '        If (Not content Is Nothing) Then
        '            File.WriteAllBytes(Zip_Name, content)
        '            If File.Exists((Directories & "\" & CDR_Name_Xml)) Then
        '                File.Delete((Directories & "\" & CDR_Name_Xml))
        '            End If
        '            ZipFile.ExtractToDirectory(Zip_Name, Directories)
        '            'Dim document As New XmlDocument
        '            'document.Load((CDR_Directory_Descompression & CDR_Name_Xml))
        '            'Dim childNodes As XmlNodeList = document.GetElementsByTagName("DocumentResponse", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2").Item(0).FirstChild.ChildNodes
        '            'Dim innerText As String = childNodes.ItemOf(1).InnerText
        '            'Dim str11 As String = childNodes.ItemOf(2).InnerText
        '        End If
        '    Catch exception As FaultException
        '        Dim str12 As String = exception.Code.Name.ToUpper.Replace("SOAP-ENV-SERVER.", "").Replace("SOAP-ENV-SERVER", "")
        '        Dim message As String = exception.Message
        '        'log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETAGETSTATUS-SUMMARYBOL-PROD", sunat.Codigo & "|" & sunat.Mensaje)
        '    End Try
        'Catch exception2 As Exception

        'Finally
        '    'If File.Exists(pathX) Then
        '    '    File.Delete(pathX)
        '    'End If
        'End Try
        'Dim EResp As String = SUNAT_Produccion_BoletaVenta_SummaryVoided_EstadoDocumento_OSE("20115643216", "03", "BM01", "00000007")
        Dim EResp As String = HerramientasFE.Consultar_Estado_Sunat("20115643216", "03", "BM01", "00000006")
    End Sub

#Region "METODOS OSE"
    Public Function SUNAT_Produccion_BoletaVentaFactura_NotaCreditoNotaDebito_Bill_Alta_OSE(nProceso As fcProceso) As eSunatRespuesta
        Dim RptaSUNAT As New eSunatRespuesta

        Dim CDR_Name_Zip As String = nProceso.FileNameWithOutExtension & "-CDR.ZIP"
        Dim CDR_Name_Xml As String = "R-" & nProceso.FileNameWithOutExtension & ".XML"
        Dim CDR_Directory_Descompression As String = nProceso.ServidorRuta_Aceptado '& "\" 'Application.StartupPath
        nProceso.CDRNameZIP = CDR_Name_Zip
        nProceso.CDRNameXML = CDR_Name_Xml

        '--BETA
        Dim DataByteArray As Byte() = System.IO.File.ReadAllBytes(nProceso.ServidorRuta_Generado & nProceso.FileNameZIP)

        System.Net.ServicePointManager.UseNagleAlgorithm = True
        System.Net.ServicePointManager.Expect100Continue = False
        System.Net.ServicePointManager.CheckCertificateRevocationList = True

        Dim binding As New ServiceModel.BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential)
        binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None
        binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None
        binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName
        binding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Default
        Dim remoteAddress As New EndpointAddress("https://ose.nubefact.com/ol-ti-itcpe/billService")  'e-factura.sunat.gob.pe/ol-ti-itcpfegem/billService
        Dim client As New WSOSE_Nubefact.billServiceClient(binding, remoteAddress)
        client.ClientCredentials.UserName.UserName = nProceso.Emisor_RUC & nProceso.SUNAT_Usuario
        client.ClientCredentials.UserName.Password = nProceso.SUNAT_Contrasena
        Dim bindingElementsInTopDownChannelStackOrder As BindingElementCollection = client.Endpoint.Binding.CreateBindingElements
        bindingElementsInTopDownChannelStackOrder.Find(Of SecurityBindingElement).EnableUnsecuredResponse = True
        client.Endpoint.Binding = New CustomBinding(bindingElementsInTopDownChannelStackOrder)

        Dim RetornoCDR_Byte As Byte()

        Try
            RetornoCDR_Byte = client.sendBill(nProceso.FileNameZIP, DataByteArray, "")
        Catch exception As FaultException
            Dim str10 As String = HerramientasFE.SoloNumeros(exception.Message) 'exception.Code.Name.ToUpper.Replace("SOAP-ENV-SERVER.", "").Replace("SOAP-ENV-SERVER", "")
            Dim str11 As String = ("FaultException Validación: " & exception.Message)
            RptaSUNAT.Codigo = str10
            RptaSUNAT.Mensaje = str11
            If RptaSUNAT.Codigo <> "0109" And RptaSUNAT.Codigo <> "0130" And RptaSUNAT.Codigo <> "0100" And RptaSUNAT.Codigo <> "0138" Then
                RptaSUNAT.Estado = EstadoDocumento.Rechazado
            End If

            If RptaSUNAT.Codigo = "1033" Then
                RptaSUNAT.Estado = EstadoDocumento.Aceptado
            End If
            If RptaSUNAT.Codigo = "1032" Then
                RptaSUNAT.Estado = EstadoDocumento.Rechazado
            End If

            log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "OSEPRODBILL-FACTBOL1", RptaSUNAT.Codigo & "|" & RptaSUNAT.Mensaje)
            Return RptaSUNAT

        Catch exception3 As Exception
            RptaSUNAT.Codigo = "0"
            RptaSUNAT.Mensaje = "El archivo XML ha sido generado, pero aún no ha sido válidado con la SUNAT debido a que no hay conexión con la URL: https://e-factura.sunat.gob.pe/ol-ti-itcpfegem/billService"
            RptaSUNAT.Estado = EstadoDocumento.Error
            log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "OSEPRODBILL-FACTBOL2", RptaSUNAT.Codigo & "|" & RptaSUNAT.Mensaje)

            Return RptaSUNAT
        End Try

        If (Not RetornoCDR_Byte Is Nothing) Then
            Try
                File.WriteAllBytes(nProceso.ServidorRuta_Aceptado & "\" & CDR_Name_Zip, RetornoCDR_Byte)
                If File.Exists((CDR_Directory_Descompression & CDR_Name_Xml)) Then
                    File.Delete((CDR_Directory_Descompression & CDR_Name_Xml))
                End If
                Compression.ZipFile.ExtractToDirectory(nProceso.ServidorRuta_Aceptado & "\" & CDR_Name_Zip, CDR_Directory_Descompression)
                Dim document As New XmlDocument
                document.Load((CDR_Directory_Descompression & CDR_Name_Xml))
                Dim childNodes As XmlNodeList = document.GetElementsByTagName("DocumentResponse", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2").Item(0).FirstChild.ChildNodes
                RptaSUNAT.Codigo = childNodes.ItemOf(1).InnerText
                RptaSUNAT.Mensaje = childNodes.ItemOf(2).InnerText
                Dim Nodes As XmlNode = document.GetElementsByTagName("ResponseDate", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2").Item(0)
                Dim Fecha As Date = CDate(Nodes.InnerText)
                Nodes = document.GetElementsByTagName("ResponseTime", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2").Item(0)
                Dim Hora As TimeSpan = TimeSpan.Parse(Nodes.InnerText)
                RptaSUNAT.Fecha_Respuesta = Fecha.Add(Hora)

            Catch ex As Exception
                'Sospecho disco lleno
                log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "OSEPRODBILL-FACTBOL-COMP", ex.Message)
            End Try

        End If

        If (Convert.ToInt32(RptaSUNAT.Codigo) = 0) Then
            RptaSUNAT.Estado = EstadoDocumento.Aceptado
            '    log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "ACEPTADO|" & nProceso.FileNameXML)
            'ElseIf ((Convert.ToInt32(RptaSUNAT.Codigo) >= 1) AndAlso (Convert.ToInt32(RptaSUNAT.Codigo) <= 199)) Then
            '    RptaSUNAT.Estado = EstadoDocumento.Invalido
            '    log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "INVALIDO|" & nProceso.FileNameXML)
            'ElseIf (Convert.ToInt32(RptaSUNAT.Codigo) >= 200) Then
        Else
            RptaSUNAT.Estado = EstadoDocumento.Rechazado
            log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "OSEBILLFACTBOL", "RECHAZADO|" & nProceso.FileNameXML)
        End If

        Return RptaSUNAT
    End Function
    Public Function SUNAT_Summary_Voided_Envio_OSE(nProceso As fcProceso) As eSunatRespuesta
        Dim RptaSUNAT As New eSunatRespuesta
        Dim CDR_Name_Zip As String = nProceso.FileNameWithOutExtension & "-CDR.ZIP"
        Dim CDR_Name_Xml As String = "R-" & nProceso.FileNameWithOutExtension & ".XML"
        nProceso.CDRNameZIP = CDR_Name_Zip
        nProceso.CDRNameXML = CDR_Name_Xml
        '--BETA
        Dim DataByteArray As Byte() = System.IO.File.ReadAllBytes(nProceso.ServidorRuta_Generado & nProceso.FileNameZIP)

        System.Net.ServicePointManager.UseNagleAlgorithm = True
        System.Net.ServicePointManager.Expect100Continue = False
        System.Net.ServicePointManager.CheckCertificateRevocationList = True

        Dim binding As New ServiceModel.BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential)
        binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None
        binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None
        binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName
        binding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Default


        Dim SunatWebService As String = "https://e-beta.sunat.gob.pe/ol-ti-itcpfegem-beta/billService"
        Dim UserSUNAT As String = "MODDATOS"
        Dim PassSUNAT As String = "moddatos"
        If nProceso.Etapa = "001" Then
            SunatWebService = "https://e-beta.sunat.gob.pe/ol-ti-itcpfegem-beta/billService"
        ElseIf nProceso.Etapa = "002" Then
            SunatWebService = "https://www.sunat.gob.pe/ol-ti-itcpgem-sqa/billService"
            UserSUNAT = nProceso.SUNAT_Usuario.Trim
            PassSUNAT = nProceso.SUNAT_Contrasena.Trim
        ElseIf nProceso.Etapa = "003" Then
            SunatWebService = "https://ose.nubefact.com/ol-ti-itcpe/billService"
            UserSUNAT = nProceso.SUNAT_Usuario.Trim
            PassSUNAT = nProceso.SUNAT_Contrasena.Trim
        End If


        Dim remoteAddress As New EndpointAddress(SunatWebService)
        Dim client As New WSOSE_Nubefact.billServiceClient(binding, remoteAddress)
        client.ClientCredentials.UserName.UserName = nProceso.Emisor_RUC & UserSUNAT
        client.ClientCredentials.UserName.Password = PassSUNAT
        Dim bindingElementsInTopDownChannelStackOrder As BindingElementCollection = client.Endpoint.Binding.CreateBindingElements
        bindingElementsInTopDownChannelStackOrder.Find(Of SecurityBindingElement).EnableUnsecuredResponse = True
        client.Endpoint.Binding = New CustomBinding(bindingElementsInTopDownChannelStackOrder)

        Dim Retorno_Ticket_ID As String = ""

        Try
            Retorno_Ticket_ID = client.sendSummary(nProceso.FileNameZIP, DataByteArray, "")
        Catch exception As FaultException
            Dim str10 As String = HerramientasFE.SoloNumeros(exception.Message)
            str10 = HerramientasFE.SoloNumeros(str10)
            Dim str11 As String = ("FaultException Validación: " & exception.Message)
            RptaSUNAT.Codigo = str10 'exception.Message' str10
            RptaSUNAT.Mensaje = str11
            RptaSUNAT.Estado = EstadoDocumento.Rechazado

            'Log.Instancia.escribirLinea(Log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL", RptaSUNAT.Codigo & "|" & RptaSUNAT.Mensaje)

            Return RptaSUNAT
        Catch exception3 As Exception
            RptaSUNAT.Codigo = "-1"
            RptaSUNAT.Mensaje = "El archivo XML ha sido generado, pero aún no ha sido válidado con la SUNAT debido a que no hay conexión con la URL: " & SunatWebService
            'MessageBox.Show(RptaSUNAT.Mensaje)
            'Log.Instancia.escribirLinea(Log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL", RptaSUNAT.Codigo & "|" & RptaSUNAT.Mensaje)

            Return RptaSUNAT
        End Try

        If String.IsNullOrWhiteSpace(Retorno_Ticket_ID) Then
            RptaSUNAT.Ticket = String.Empty
            RptaSUNAT.Codigo = "-1"
            RptaSUNAT.Mensaje = "El archivo XML ha sido generado, pero no se ha generado el Ticket"
            'MessageBox.Show(RptaSUNAT.Mensaje)
            'sunat.RutaXML_DLL = pRutaXML
            'sunat.Estado = Estado.Generado
        Else
            RptaSUNAT.Ticket = Retorno_Ticket_ID
            RptaSUNAT.Codigo = "0"
            RptaSUNAT.Mensaje = ("Se acaba de generar el Ticket : " & Retorno_Ticket_ID)
            'MessageBox.Show(RptaSUNAT.Mensaje)
            'sunat.RutaXML_DLL = pRutaXML
            'sunat.Estado = Estado.Aceptado
        End If

        If (Convert.ToInt32(RptaSUNAT.Codigo) = 0) Then
            RptaSUNAT.Estado = EstadoDocumento.Aceptado
            log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "OSEBILLFACTBOL", "ACEPTADO|" & nProceso.FileNameXML & "-" & RptaSUNAT.Mensaje)
            'ElseIf ((Convert.ToInt32(RptaSUNAT.Codigo) >= 1) AndAlso (Convert.ToInt32(RptaSUNAT.Codigo) <= 199)) Then
            '    RptaSUNAT.Estado = EstadoDocumento.Invalido
            '    log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "INVALIDO|" & nProceso.FileNameXML)
            'ElseIf (Convert.ToInt32(RptaSUNAT.Codigo) >= 200) Then
        Else
            RptaSUNAT.Estado = EstadoDocumento.Rechazado
            log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "OSEBILLFACTBOL", "RECHAZADO|" & nProceso.FileNameXML)
        End If

        Return RptaSUNAT
    End Function
    Public Function SUNAT_Produccion_BoletaVenta_SummaryVoided_EstadoDocumento_OSE(ByVal pRuc_Remitente As String, pTipo_Doc As String, ByVal pDoc_Serie As String, pDoc_Numero As String) As String
        Dim RptaST As String = "-#-"
        Try
            Dim LogCertificado As New Log_CERTIFICADO
            Dim CertificadoDigital As eCertificadoCredencial = LogCertificado.Buscar(pRuc_Remitente.Trim)

            Net.ServicePointManager.UseNagleAlgorithm = True
            Net.ServicePointManager.Expect100Continue = False
            Net.ServicePointManager.CheckCertificateRevocationList = True
            Dim binding As New BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential)
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None
            binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None
            binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName
            binding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Default
            Dim remoteAddress As New EndpointAddress("https://ose.nubefact.com/ol-ti-itcpe/billService") 'https://www.sunat.gob.pe:443/ol-it-wsconscpegem/billConsultService
            Dim client As New WSOSE_Nubefact.billServiceClient(binding, remoteAddress)
            client.ClientCredentials.UserName.UserName = pRuc_Remitente.Trim & CertificadoDigital.Sunat_Usuario
            client.ClientCredentials.UserName.Password = CertificadoDigital.Sunat_Usuario
            Dim bindingElementsInTopDownChannelStackOrder As BindingElementCollection = client.Endpoint.Binding.CreateBindingElements
            bindingElementsInTopDownChannelStackOrder.Find(Of SecurityBindingElement).EnableUnsecuredResponse = True
            client.Endpoint.Binding = New CustomBinding(bindingElementsInTopDownChannelStackOrder)

            Try
                'client.Open()
                Dim response As WSOSE_Nubefact.statusCdr = client.getStatusCdr(pRuc_Remitente.Trim, pTipo_Doc.Trim, pDoc_Serie.Trim, pDoc_Numero.ToString)
                'client.Close()
                If (Not response.content Is Nothing) Then
                    Try
                        Dim RptaSUNAT As New eSunatRespuesta
                        Dim Ruta As String = Microsoft.VisualBasic.FileIO.SpecialDirectories.MyDocuments
                        Dim CDRZIP = Ruta & "\CDROSE.ZIP"
                        Dim CDRXML = Ruta & "\CDROSE.XML"
                        File.WriteAllBytes(CDRZIP, response.content)
                        If File.Exists(CDRXML) Then
                            File.Delete(CDRXML)
                        End If
                        Compression.ZipFile.ExtractToDirectory(CDRZIP, Ruta)
                        'Dim document As New XmlDocument
                        'document.Load((CDR_Directory_Descompression & CDR_Name_Xml))
                        'Dim childNodes As XmlNodeList = document.GetElementsByTagName("DocumentResponse", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2").Item(0).FirstChild.ChildNodes
                        'RptaSUNAT.Codigo = childNodes.ItemOf(1).InnerText
                        'RptaSUNAT.Mensaje = childNodes.ItemOf(2).InnerText
                        'Dim Nodes As XmlNode = document.GetElementsByTagName("ResponseDate", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2").Item(0)
                        'Dim Fecha As Date = CDate(Nodes.InnerText)
                        'Nodes = document.GetElementsByTagName("ResponseTime", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2").Item(0)
                        'Dim Hora As TimeSpan = TimeSpan.Parse(Nodes.InnerText)
                        'RptaSUNAT.Fecha_Respuesta = Fecha.Add(Hora)
                        'RptaST = RptaSUNAT.Codigo & " # " & RptaSUNAT.Mensaje
                    Catch ex As Exception
                        'Sospecho disco lleno
                        log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "OSEPRODBILL-FACTBOL-COMP", ex.Message)
                    End Try

                End If

            Catch exception As FaultException
                client.Close()
            End Try

        Catch exception2 As Exception

        Finally
            'If File.Exists(pathX) Then
            '    File.Delete(pathX)
            'End If
        End Try
        Return RptaST.Trim
    End Function
    Public Function SUNAT_Produccion_BoletaVenta_SummaryVoided_EstadoTicket_OSE(nProceso As fcProceso) As eSunatRespuesta
        Dim sunat As New eSunatRespuesta
        Dim CDR_Name_Zip As String = nProceso.FileNameWithOutExtension & "-CDR.ZIP"
        Dim CDR_Name_Xml As String = "R-" & nProceso.FileNameWithOutExtension & ".XML"
        Dim CDR_Directory_Descompression As String = nProceso.ServidorRuta_Aceptado  'Application.StartupPath
        nProceso.CDRNameZIP = CDR_Name_Zip

        Try
            Net.ServicePointManager.UseNagleAlgorithm = True
            Net.ServicePointManager.Expect100Continue = False
            Net.ServicePointManager.CheckCertificateRevocationList = True
            Dim binding As New BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential)
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None
            binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None
            binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName
            binding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Default
            Dim SunatWebService As String = "https://e-beta.sunat.gob.pe/ol-ti-itcpfegem-beta/billService"
            Dim UserSUNAT As String = "MODDATOS"
            Dim PassSUNAT As String = "moddatos"
            If nProceso.Etapa = "001" Then
                SunatWebService = "https://e-beta.sunat.gob.pe/ol-ti-itcpfegem-beta/billService"
            ElseIf nProceso.Etapa = "002" Then
                SunatWebService = "https://www.sunat.gob.pe/ol-ti-itcpgem-sqa/billService"
                UserSUNAT = nProceso.SUNAT_Usuario.Trim
                PassSUNAT = nProceso.SUNAT_Contrasena.Trim
            ElseIf nProceso.Etapa = "003" Then
                SunatWebService = " https://ose.nubefact.com/ol-ti-itcpe/billService"
                UserSUNAT = nProceso.Emisor_RUC & nProceso.SUNAT_Usuario.Trim
                PassSUNAT = nProceso.SUNAT_Contrasena.Trim
            End If
            Dim remoteAddress As New EndpointAddress(SunatWebService)

            Dim client As New WSOSE_Nubefact.billServiceClient(binding, remoteAddress)
            client.ClientCredentials.UserName.UserName = nProceso.Emisor_RUC & nProceso.SUNAT_Usuario
            client.ClientCredentials.UserName.Password = nProceso.SUNAT_Contrasena
            Dim bindingElementsInTopDownChannelStackOrder As BindingElementCollection = client.Endpoint.Binding.CreateBindingElements
            bindingElementsInTopDownChannelStackOrder.Find(Of SecurityBindingElement).EnableUnsecuredResponse = True
            client.Endpoint.Binding = New CustomBinding(bindingElementsInTopDownChannelStackOrder)
            sunat.Ticket = nProceso.TicketNumero
            Dim response As WSOSE_Nubefact.statusResponse = client.getStatus(nProceso.TicketNumero)
            Try
                Dim content As Byte() = response.content 'client.getStatus(nProceso.TicketNumero).content
                If (Not content Is Nothing) Then
                    If Not IO.Directory.Exists(nProceso.ServidorRuta_Aceptado) Then
                        IO.Directory.CreateDirectory(nProceso.ServidorRuta_Aceptado)
                    End If
                    File.WriteAllBytes(nProceso.ServidorRuta_Aceptado & CDR_Name_Zip, content)
                    If File.Exists((CDR_Directory_Descompression & CDR_Name_Xml)) Then
                        File.Delete((CDR_Directory_Descompression & CDR_Name_Xml))
                    End If

                    If HerramientasFE.SoloNumeros(response.statusCode).Trim <> "0127" Then
                        Compression.ZipFile.ExtractToDirectory(nProceso.ServidorRuta_Aceptado & CDR_Name_Zip, CDR_Directory_Descompression)
                        Dim document As New XmlDocument
                        document.Load((CDR_Directory_Descompression & CDR_Name_Xml))
                        Dim childNodes As XmlNodeList = document.GetElementsByTagName("DocumentResponse", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2").Item(0).FirstChild.ChildNodes
                        Dim innerText As String = childNodes.ItemOf(1).InnerText
                        Dim str11 As String = childNodes.ItemOf(2).InnerText
                        sunat.Codigo = innerText
                        sunat.Mensaje = str11
                        Dim Nodes As XmlNode = document.GetElementsByTagName("ResponseDate", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2").Item(0)
                        Dim Fecha As Date = CDate(Nodes.InnerText)
                        Nodes = document.GetElementsByTagName("ResponseTime", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2").Item(0)
                        Dim Hora As TimeSpan = TimeSpan.Parse(Nodes.InnerText)
                        sunat.Fecha_Respuesta = Fecha.Add(Hora)

                        'sunat.RutaXML_DLL = pRutaXML
                        'sunat.RutaXML_SNT = (destinationDirectoryName & str7)
                    Else
                        Dim str12 As String = response.statusCode
                        Dim message As String = "El ticket " & nProceso.TicketNumero & " no existe"
                        sunat.Codigo = str12
                        sunat.Mensaje = message
                        log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "OSEGETSTATUS-SUMMARYBOL-PROD", sunat.Codigo & "|" & sunat.Mensaje)
                    End If
                End If

                Return sunat
            Catch exception As FaultException
                Dim str12 As String = HerramientasFE.SoloNumeros(exception.Message)
                Dim message As String = exception.Message
                sunat.Codigo = str12
                sunat.Mensaje = message
                log.Instancia.escribirLinea(log.tipoLogEnum.Fatal_, "OSEGETSTATUS-SUMMARYBOL-PROD", sunat.Codigo & "|" & sunat.Mensaje)
            End Try
            Return sunat
        Catch exception2 As Exception
            sunat.Codigo = "-1"
            sunat.Mensaje = exception2.Message
        Finally
            'If File.Exists(pathX) Then
            '    File.Delete(pathX)
            'End If
        End Try
        Return sunat
    End Function
#End Region

End Class