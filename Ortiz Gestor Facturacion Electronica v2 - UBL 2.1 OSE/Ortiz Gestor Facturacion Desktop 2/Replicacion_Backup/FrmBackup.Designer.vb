﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmBackup
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BtnBackupCrear = New System.Windows.Forms.Button()
        Me.BtnBackupSubir = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'BtnBackupCrear
        '
        Me.BtnBackupCrear.Location = New System.Drawing.Point(25, 42)
        Me.BtnBackupCrear.Name = "BtnBackupCrear"
        Me.BtnBackupCrear.Size = New System.Drawing.Size(134, 71)
        Me.BtnBackupCrear.TabIndex = 0
        Me.BtnBackupCrear.Text = "Crear Backup de documentos electrónicos"
        Me.BtnBackupCrear.UseVisualStyleBackColor = True
        '
        'BtnBackupSubir
        '
        Me.BtnBackupSubir.Location = New System.Drawing.Point(177, 42)
        Me.BtnBackupSubir.Name = "BtnBackupSubir"
        Me.BtnBackupSubir.Size = New System.Drawing.Size(134, 71)
        Me.BtnBackupSubir.TabIndex = 1
        Me.BtnBackupSubir.Text = "Subir Backup"
        Me.BtnBackupSubir.UseVisualStyleBackColor = True
        '
        'FrmBackup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(338, 202)
        Me.Controls.Add(Me.BtnBackupSubir)
        Me.Controls.Add(Me.BtnBackupCrear)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmBackup"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Backup"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BtnBackupCrear As System.Windows.Forms.Button
    Friend WithEvents BtnBackupSubir As System.Windows.Forms.Button
End Class
