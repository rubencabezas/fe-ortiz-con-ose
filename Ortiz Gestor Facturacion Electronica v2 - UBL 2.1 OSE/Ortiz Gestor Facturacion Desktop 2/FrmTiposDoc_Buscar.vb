﻿Public Class FrmTiposDoc_Buscar
    Public ListaFiltrada As New List(Of eTipoDocumento)
    Private Sub FrmTiposDoc_Buscar_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim log As New Log_VTS_TIPO_DOC
        ListaFiltrada = log.Buscar(New eTipoDocumento)
        DgvGeneral.DataSource = Nothing
        DgvGeneral.DataSource = ListaFiltrada
    End Sub
    Private Sub BtnSeleccionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSeleccionar.ItemClick
        If GridView1.RowCount > 0 And GridView1.GetFocusedDataSourceRowIndex > -1 Then
            DialogResult = Windows.Forms.DialogResult.OK
        End If
    End Sub
    Private Sub BtnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSalir.ItemClick
        Close()
    End Sub

    Private Sub DgvGeneral_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DgvGeneral.DoubleClick
        BtnSeleccionar.PerformClick()
    End Sub

    Private Sub GridView1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridView1.KeyDown
        If e.KeyCode = Keys.Enter Then
            BtnSeleccionar.PerformClick()
            e.SuppressKeyPress = True
        End If
    End Sub

    Private Sub GridView1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridView1.KeyPress
        If e.KeyChar = Chr(13) Then
            BtnSeleccionar.PerformClick()
            e.KeyChar = Convert.ToChar(Keys.None)
        End If
    End Sub
End Class