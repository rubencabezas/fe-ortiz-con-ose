﻿Public Class FrmTarea_Edicion
    Public Emisor_RUC As String
    Public TareaProgramada As eProgramacion
    Private EsNuevo As Boolean = False
    Private Sub FrmTarea_Edicion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Cargar()
    End Sub
    Sub Cargar()
        If Not IsNothing(TareaProgramada) Then
            Emisor_RUC = TareaProgramada.Emisor_RUC
            TxtDescripcion.Tag = TareaProgramada.Progra_Correlativo
            TxtDescripcion.Text = TareaProgramada.Progra_Descripcion
            ChkHabilitado.Checked = TareaProgramada.Progra_Habilitado
            TxtTipoDocCodigo.Text = TareaProgramada.Progra_Appli_TipoDoc
            TxtTipoDocDescripcion.Text = TareaProgramada.Progra_Appli_TipoDoc_Descripcion
            ChkSerie.Checked = TareaProgramada.Progra_Appli_Filtra_Serie
            TxtSerie.Enabled = ChkSerie.Checked
            TxtSerie.Text = TareaProgramada.Progra_Appli_Serie
            ChkResumenBoletas.Checked = TareaProgramada.Progra_EsResumenBoletas
            ChkComunicacionBaja.Checked = TareaProgramada.Progra_EsComunicacionBaja

            RbnHora_Es_Establecido.Checked = TareaProgramada.Progra_FrecuEnvio_Hora_Es_Establecido
            RbnHora_Es_XCadaHora.Checked = TareaProgramada.Progra_FrecuEnvio_Hora_Es_XCadaHora
            RbnHora_Es_DespuesEmitido.Checked = TareaProgramada.Progra_FrecuEnvio_Hora_Es_DespuesEmitido
            'TareaProgramada.Progra_FrecuEnvio_Hora_Es_DespuesConfirmado = NO IMPLEMENTADO AUN

            TxtHora_Desde1.Enabled = RbnHora_Es_Establecido.Checked
            If RbnHora_Es_Establecido.Checked Then
                TxtHora_Desde1.Time = TareaProgramada.Progra_FrecuEnvio_Hora_Desde_12
                'TxtHora_Desde1.Text = TareaProgramada.Progra_FrecuEnvio_Hora_Desde
                'TareaProgramada.Progra_FrecuEnvio_Hora_Hasta = Date.MaxValue
                'TareaProgramada.Progra_FrecuEnvio_Hora_XCadaHora = 0
            End If
            TxtHora_Desde2.Enabled = RbnHora_Es_XCadaHora.Checked
            TxtHora_Hasta2.Enabled = RbnHora_Es_XCadaHora.Checked
            TxtXCadaHora1.Enabled = RbnHora_Es_XCadaHora.Checked
            If RbnHora_Es_XCadaHora.Checked Then
                TxtHora_Desde2.Time = TareaProgramada.Progra_FrecuEnvio_Hora_Desde_12
                TxtHora_Hasta2.Time = TareaProgramada.Progra_FrecuEnvio_Hora_Hasta_12
                TxtXCadaHora1.Text = TareaProgramada.Progra_FrecuEnvio_Hora_XCadaHora
            End If
            TxtXCadaHora2.Enabled = RbnHora_Es_XCadaHora.Checked
            If RbnHora_Es_DespuesEmitido.Checked Then
                'TareaProgramada.Progra_FrecuEnvio_Hora_Desde = Date.MinValue
                'TareaProgramada.Progra_FrecuEnvio_Hora_Hasta = Date.MaxValue
                TxtXCadaHora2.Text = TareaProgramada.Progra_FrecuEnvio_Hora_XCadaHora
            End If

            TxtFecha_Desde.Text = TareaProgramada.Progra_Fecha_Desde
            RbnFecha_SinFin.Checked = TareaProgramada.Progra_Fecha_SinFin
            If RbnFecha_SinFin.Checked Then
                TareaProgramada.Progra_Fecha_Hasta = Date.MinValue
            Else
                RbnFecha_ConFin.Checked = True
                TxtFecha_Hasta.Text = TareaProgramada.Progra_Fecha_Hasta
            End If

        Else
            EsNuevo = True
        End If
    End Sub
    Function CodeFormat(ByVal Id As String, ByVal Tamanio As Int32) As String
        If Id.Length < Tamanio Then
            For i = Id.Length To Tamanio - 1
                Id = "0" & Id
            Next
        End If
        Return Id
    End Function
    Function ValidarFormulario() As Boolean
        If String.IsNullOrEmpty(TxtDescripcion.Text) Then
            TxtDescripcion.Focus()
            Return False
        ElseIf ChkSerie.Checked And String.IsNullOrEmpty(TxtSerie.Text) Then
            TxtSerie.Focus()
            Return False
        ElseIf Not IsDate(TxtFecha_Desde.Text) Then
            TxtFecha_Desde.Focus()
            Return False
        ElseIf TxtFecha_Hasta.Enabled And Not IsDate(TxtFecha_Hasta.Text) Then
            TxtFecha_Hasta.Focus()
            Return False
        End If
        Return True
    End Function
    Private Sub BtnGuardar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnGuardar.ItemClick
        If ValidarFormulario() Then
            If EsNuevo Then
                Dim TareaEnvio As New eProgramacion
                TareaEnvio.Emisor_RUC = Emisor_RUC
                'TareaEnvio.Progra_Correlativo = TxtDescripcion.Tag
                TareaEnvio.Progra_Descripcion = TxtDescripcion.Text
                TareaEnvio.Progra_Habilitado = ChkHabilitado.Checked
                TareaEnvio.Progra_Appli_TipoDoc = TxtTipoDocCodigo.Text
                TareaEnvio.Progra_Appli_Filtra_Serie = ChkSerie.Checked
                If ChkSerie.Checked Then
                    TareaEnvio.Progra_Appli_Serie = TxtSerie.Text
                End If

                TareaEnvio.Progra_EsResumenBoletas = ChkResumenBoletas.Checked
                TareaEnvio.Progra_EsComunicacionBaja = ChkComunicacionBaja.Checked

                TareaEnvio.Progra_FrecuEnvio_Hora_Es_Establecido = RbnHora_Es_Establecido.Checked
                TareaEnvio.Progra_FrecuEnvio_Hora_Es_XCadaHora = RbnHora_Es_XCadaHora.Checked
                TareaEnvio.Progra_FrecuEnvio_Hora_Es_DespuesEmitido = RbnHora_Es_DespuesEmitido.Checked
                TareaEnvio.Progra_FrecuEnvio_Hora_Es_DespuesConfirmado = False ' NO IMPLEMENTADO AUN

                Dim hms As Date
                If RbnHora_Es_Establecido.Checked Then

                    hms = TxtHora_Desde1.Text

                    TareaEnvio.Progra_FrecuEnvio_Hora_Desde = New TimeSpan(0, hms.Hour, hms.Minute, hms.Second, hms.Millisecond)
                    TareaEnvio.Progra_FrecuEnvio_Hora_Hasta = New TimeSpan(0, 23, 59, 59, 0) ' TimeSpan.MaxValue
                    TareaEnvio.Progra_FrecuEnvio_Hora_XCadaHora = 0
                End If
                If RbnHora_Es_XCadaHora.Checked Then
                    hms = TxtHora_Desde2.Text
                    TareaEnvio.Progra_FrecuEnvio_Hora_Desde = New TimeSpan(0, hms.Hour, hms.Minute, hms.Second, hms.Millisecond)
                    hms = TxtHora_Hasta2.Text
                    TareaEnvio.Progra_FrecuEnvio_Hora_Hasta = New TimeSpan(0, hms.Hour, hms.Minute, hms.Second, hms.Millisecond)
                    TareaEnvio.Progra_FrecuEnvio_Hora_XCadaHora = TxtXCadaHora1.Text
                End If
                If RbnHora_Es_DespuesEmitido.Checked Then
                    TareaEnvio.Progra_FrecuEnvio_Hora_Desde = New TimeSpan(0, 0, 0, 0, 0) 'TimeSpan.MinValue 'MAX
                    TareaEnvio.Progra_FrecuEnvio_Hora_Hasta = New TimeSpan(0, 23, 59, 59, 0) ' TimeSpan.MaxValue 'MAX
                    TareaEnvio.Progra_FrecuEnvio_Hora_XCadaHora = TxtXCadaHora2.Text
                End If

                TareaEnvio.Progra_Fecha_Desde = TxtFecha_Desde.Text
                TareaEnvio.Progra_Fecha_SinFin = RbnFecha_SinFin.Checked

                If RbnFecha_SinFin.Checked Then
                    TareaEnvio.Progra_Fecha_Hasta = Date.MinValue
                Else
                    TareaEnvio.Progra_Fecha_Hasta = CDate(TxtFecha_Hasta.Text)
                End If

                Dim log As New Log_FACTE_PROGRAMACIONES_ENVIO
                If log.Insertar(TareaEnvio) Then
                    DialogResult = Windows.Forms.DialogResult.OK
                End If
            Else
                Dim TareaEnvio As New eProgramacion
                TareaEnvio.Emisor_RUC = Emisor_RUC
                TareaEnvio.Progra_Correlativo = TxtDescripcion.Tag
                TareaEnvio.Progra_Descripcion = TxtDescripcion.Text
                TareaEnvio.Progra_Habilitado = ChkHabilitado.Checked
                TareaEnvio.Progra_Appli_TipoDoc = TxtTipoDocCodigo.Text
                TareaEnvio.Progra_Appli_Filtra_Serie = ChkSerie.Checked
                If ChkSerie.Checked Then
                    TareaEnvio.Progra_Appli_Serie = TxtSerie.Text
                End If
                TareaEnvio.Progra_EsResumenBoletas = ChkResumenBoletas.Checked
                TareaEnvio.Progra_EsComunicacionBaja = ChkComunicacionBaja.Checked

                TareaEnvio.Progra_FrecuEnvio_Hora_Es_Establecido = RbnHora_Es_Establecido.Checked
                TareaEnvio.Progra_FrecuEnvio_Hora_Es_XCadaHora = RbnHora_Es_XCadaHora.Checked
                TareaEnvio.Progra_FrecuEnvio_Hora_Es_DespuesEmitido = RbnHora_Es_DespuesEmitido.Checked
                TareaEnvio.Progra_FrecuEnvio_Hora_Es_DespuesConfirmado = False ' NO IMPLEMENTADO AUN

                Dim hms As Date
                If RbnHora_Es_Establecido.Checked Then

                    hms = TxtHora_Desde1.Text

                    TareaEnvio.Progra_FrecuEnvio_Hora_Desde = New TimeSpan(0, hms.Hour, hms.Minute, hms.Second, hms.Millisecond)
                    TareaEnvio.Progra_FrecuEnvio_Hora_Hasta = New TimeSpan(0, 23, 59, 59, 0) ' TimeSpan.MaxValue
                    TareaEnvio.Progra_FrecuEnvio_Hora_XCadaHora = 0
                End If
                If RbnHora_Es_XCadaHora.Checked Then
                    hms = TxtHora_Desde2.Text
                    TareaEnvio.Progra_FrecuEnvio_Hora_Desde = New TimeSpan(0, hms.Hour, hms.Minute, hms.Second, hms.Millisecond)
                    hms = TxtHora_Hasta2.Text
                    TareaEnvio.Progra_FrecuEnvio_Hora_Hasta = New TimeSpan(0, hms.Hour, hms.Minute, hms.Second, hms.Millisecond)
                    TareaEnvio.Progra_FrecuEnvio_Hora_XCadaHora = TxtXCadaHora1.Text
                End If
                If RbnHora_Es_DespuesEmitido.Checked Then
                    TareaEnvio.Progra_FrecuEnvio_Hora_Desde = New TimeSpan(0, 0, 0, 0, 0) 'TimeSpan.MinValue 'MAX
                    TareaEnvio.Progra_FrecuEnvio_Hora_Hasta = New TimeSpan(0, 23, 59, 59, 0) ' TimeSpan.MaxValue 'MAX
                    TareaEnvio.Progra_FrecuEnvio_Hora_XCadaHora = TxtXCadaHora2.Text
                End If

                TareaEnvio.Progra_Fecha_Desde = TxtFecha_Desde.Text
                TareaEnvio.Progra_Fecha_SinFin = RbnFecha_SinFin.Checked

                If RbnFecha_SinFin.Checked Then
                    TareaEnvio.Progra_Fecha_Hasta = Date.MinValue
                Else
                    TareaEnvio.Progra_Fecha_Hasta = CDate(TxtFecha_Hasta.Text)
                End If
                Dim log As New Log_FACTE_PROGRAMACIONES_ENVIO
                If log.Actualizar(TareaEnvio) Then
                    DialogResult = Windows.Forms.DialogResult.OK
                End If
            End If
        End If

    End Sub

    Private Sub BtnSalir_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnSalir.ItemClick
        Me.Close()
    End Sub

    Private Sub ChkSerie_CheckedChanged(sender As Object, e As EventArgs) Handles ChkSerie.CheckedChanged
        TxtSerie.Enabled = ChkSerie.Checked
        If Not ChkSerie.Checked Then
            TxtSerie.Text = ""
        End If
    End Sub

    Private Sub RbnHora_Es_Establecido_CheckedChanged(sender As Object, e As EventArgs) Handles RbnHora_Es_Establecido.CheckedChanged
        TxtHora_Desde1.Enabled = RbnHora_Es_Establecido.Checked
    End Sub

    Private Sub RbnHora_Es_XCadaHora_CheckedChanged(sender As Object, e As EventArgs) Handles RbnHora_Es_XCadaHora.CheckedChanged
        TxtXCadaHora1.Enabled = RbnHora_Es_XCadaHora.Checked
        TxtHora_Desde2.Enabled = RbnHora_Es_XCadaHora.Checked
        TxtHora_Hasta2.Enabled = RbnHora_Es_XCadaHora.Checked
        If Not RbnHora_Es_XCadaHora.Checked Then
            TxtXCadaHora1.Text = 0
        End If
    End Sub

    Private Sub RbnHora_Es_DespuesEmitido_CheckedChanged(sender As Object, e As EventArgs) Handles RbnHora_Es_DespuesEmitido.CheckedChanged
        TxtXCadaHora2.Enabled = RbnHora_Es_DespuesEmitido.Checked
        If Not RbnHora_Es_DespuesEmitido.Checked Then
            TxtXCadaHora2.Text = 0
        End If
    End Sub

    Private Sub RbnFecha_ConFin_CheckedChanged(sender As Object, e As EventArgs) Handles RbnFecha_ConFin.CheckedChanged
        TxtFecha_Hasta.Enabled = RbnFecha_ConFin.Checked
        If Not RbnFecha_ConFin.Checked Then
            TxtFecha_Hasta.ResetText()
        End If
    End Sub

    Private Sub TxtSerie_LostFocus(sender As Object, e As EventArgs) Handles TxtSerie.LostFocus
        If TxtSerie.Text.Trim.Length > 0 Then
            TxtSerie.Text = CodeFormat(TxtSerie.Text, 4)
        End If
    End Sub

    Private Sub TxtTipoDocCodigo_KeyDown(sender As Object, e As Windows.Forms.KeyEventArgs) Handles TxtTipoDocCodigo.KeyDown
        If Not String.IsNullOrEmpty(TxtTipoDocCodigo.Text.Trim) Then
            Try
                If e.KeyCode = Keys.Enter And TxtTipoDocCodigo.Text.Substring(TxtTipoDocCodigo.Text.Trim.Length - 1, 1) = "*" Then
                    Using f As New FrmTiposDoc_Buscar
                        If f.ShowDialog = DialogResult.OK Then
                            TxtTipoDocCodigo.Text = f.ListaFiltrada(f.GridView1.GetFocusedDataSourceRowIndex).TipoDoc_Codigo
                            TxtTipoDocDescripcion.Text = f.ListaFiltrada(f.GridView1.GetFocusedDataSourceRowIndex).TipoDoc_Descripcion
                            SendKeys.Send("{TAB}")
                        End If
                    End Using
                ElseIf e.KeyCode = Keys.Enter And TxtTipoDocCodigo.Text.Trim <> "" And TxtTipoDocDescripcion.Text.Trim = "" Then
                    Try
                        TxtTipoDocCodigo.Text = CodeFormat(TxtTipoDocCodigo.Text, 2)
                        Dim log As New Log_VTS_TIPO_DOC
                        Dim Documentos As New List(Of eTipoDocumento)
                        Documentos = log.Buscar(New eTipoDocumento With {.TipoDoc_Codigo = TxtTipoDocCodigo.Text})
                        If Documentos.Count > 0 Then
                            TxtTipoDocDescripcion.Text = Documentos(0).TipoDoc_Descripcion
                        End If
                    Catch ex As Exception
                        'Herramientas.ShowError(ex.Message)
                    End Try
                End If
            Catch ex As Exception
                'Herramientas.ShowError(ex.Message)
            End Try
        End If
    End Sub

    Private Sub TxtTipoDocCodigo_TextChanged(sender As Object, e As EventArgs) Handles TxtTipoDocCodigo.TextChanged

        If TxtTipoDocCodigo.Focus Then
            TxtTipoDocDescripcion.Clear()
        End If
    End Sub

    Private Sub ChkResumenBoletas_CheckedChanged(sender As Object, e As EventArgs) Handles ChkResumenBoletas.CheckedChanged
        TxtTipoDocCodigo.Enabled = Not ChkResumenBoletas.Checked
        If ChkResumenBoletas.Checked Then
            ChkComunicacionBaja.Checked = False
            TxtTipoDocCodigo.Text = "03"
            TxtTipoDocDescripcion.Text = "BOLETA DE VENTA ELECTRONICA"
        End If
    End Sub

    Private Sub ChkComunicacionBaja_CheckedChanged(sender As Object, e As EventArgs) Handles ChkComunicacionBaja.CheckedChanged
        If ChkComunicacionBaja.Checked Then
            ChkResumenBoletas.Checked = False
        End If
    End Sub
End Class