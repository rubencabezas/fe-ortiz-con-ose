﻿'------------------------------------------------------------------------------
' <auto-generated>
'     Este código fue generado por una herramienta.
'     Versión de runtime:4.0.30319.42000
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Namespace WSOSE_Nubefact
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ServiceModel.ServiceContractAttribute([Namespace]:="http://service.sunat.gob.pe", ConfigurationName:="WSOSE_Nubefact.billService")>  _
    Public Interface billService
        
        'CODEGEN: El parámetro 'status' requiere información adicional de esquema que no se puede capturar con el modo de parámetros. El atributo específico es 'System.Xml.Serialization.XmlElementAttribute'.
        <System.ServiceModel.OperationContractAttribute(Action:="http://service.sunat.gob.pe/getStatus", ReplyAction:="*"),  _
         System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults:=true)>  _
        Function getStatus(ByVal request As WSOSE_Nubefact.getStatusRequest) As <System.ServiceModel.MessageParameterAttribute(Name:="status")> WSOSE_Nubefact.getStatusResponse
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://service.sunat.gob.pe/getStatus", ReplyAction:="*")>  _
        Function getStatusAsync(ByVal request As WSOSE_Nubefact.getStatusRequest) As System.Threading.Tasks.Task(Of WSOSE_Nubefact.getStatusResponse)
        
        'CODEGEN: El parámetro 'applicationResponse' requiere información adicional de esquema que no se puede capturar con el modo de parámetros. El atributo específico es 'System.Xml.Serialization.XmlElementAttribute'.
        <System.ServiceModel.OperationContractAttribute(Action:="http://service.sunat.gob.pe/sendBill", ReplyAction:="*"),  _
         System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults:=true)>  _
        Function sendBill(ByVal request As WSOSE_Nubefact.sendBillRequest) As <System.ServiceModel.MessageParameterAttribute(Name:="applicationResponse")> WSOSE_Nubefact.sendBillResponse
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://service.sunat.gob.pe/sendBill", ReplyAction:="*")>  _
        Function sendBillAsync(ByVal request As WSOSE_Nubefact.sendBillRequest) As System.Threading.Tasks.Task(Of WSOSE_Nubefact.sendBillResponse)
        
        'CODEGEN: El parámetro 'ticket' requiere información adicional de esquema que no se puede capturar con el modo de parámetros. El atributo específico es 'System.Xml.Serialization.XmlElementAttribute'.
        <System.ServiceModel.OperationContractAttribute(Action:="http://service.sunat.gob.pe/sendPack", ReplyAction:="*"),  _
         System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults:=true)>  _
        Function sendPack(ByVal request As WSOSE_Nubefact.sendPackRequest) As <System.ServiceModel.MessageParameterAttribute(Name:="ticket")> WSOSE_Nubefact.sendPackResponse
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://service.sunat.gob.pe/sendPack", ReplyAction:="*")>  _
        Function sendPackAsync(ByVal request As WSOSE_Nubefact.sendPackRequest) As System.Threading.Tasks.Task(Of WSOSE_Nubefact.sendPackResponse)
        
        'CODEGEN: El parámetro 'ticket' requiere información adicional de esquema que no se puede capturar con el modo de parámetros. El atributo específico es 'System.Xml.Serialization.XmlElementAttribute'.
        <System.ServiceModel.OperationContractAttribute(Action:="http://service.sunat.gob.pe/sendSummary", ReplyAction:="*"),  _
         System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults:=true)>  _
        Function sendSummary(ByVal request As WSOSE_Nubefact.sendSummaryRequest) As <System.ServiceModel.MessageParameterAttribute(Name:="ticket")> WSOSE_Nubefact.sendSummaryResponse
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://service.sunat.gob.pe/sendSummary", ReplyAction:="*")>  _
        Function sendSummaryAsync(ByVal request As WSOSE_Nubefact.sendSummaryRequest) As System.Threading.Tasks.Task(Of WSOSE_Nubefact.sendSummaryResponse)
        
        'CODEGEN: El parámetro 'statusCdr' requiere información adicional de esquema que no se puede capturar con el modo de parámetros. El atributo específico es 'System.Xml.Serialization.XmlElementAttribute'.
        <System.ServiceModel.OperationContractAttribute(Action:="http://service.sunat.gob.pe/getStatusCdr", ReplyAction:="*"),  _
         System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults:=true)>  _
        Function getStatusCdr(ByVal request As WSOSE_Nubefact.getStatusCdrRequest) As <System.ServiceModel.MessageParameterAttribute(Name:="statusCdr")> WSOSE_Nubefact.getStatusCdrResponse
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://service.sunat.gob.pe/getStatusCdr", ReplyAction:="*")>  _
        Function getStatusCdrAsync(ByVal request As WSOSE_Nubefact.getStatusCdrRequest) As System.Threading.Tasks.Task(Of WSOSE_Nubefact.getStatusCdrResponse)
    End Interface
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://service.sunat.gob.pe")>  _
    Partial Public Class statusResponse
        Inherits Object
        Implements System.ComponentModel.INotifyPropertyChanged
        
        Private contentField() As Byte
        
        Private statusCodeField As String
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType:="base64Binary", Order:=0)>  _
        Public Property content() As Byte()
            Get
                Return Me.contentField
            End Get
            Set
                Me.contentField = value
                Me.RaisePropertyChanged("content")
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, Order:=1)>  _
        Public Property statusCode() As String
            Get
                Return Me.statusCodeField
            End Get
            Set
                Me.statusCodeField = value
                Me.RaisePropertyChanged("statusCode")
            End Set
        End Property
        
        Public Event PropertyChanged As System.ComponentModel.PropertyChangedEventHandler Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged
        
        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As System.ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://service.sunat.gob.pe")>  _
    Partial Public Class statusCdr
        Inherits Object
        Implements System.ComponentModel.INotifyPropertyChanged
        
        Private contentField() As Byte
        
        Private statusCodeField As String
        
        Private statusMessageField As String
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType:="base64Binary", Order:=0)>  _
        Public Property content() As Byte()
            Get
                Return Me.contentField
            End Get
            Set
                Me.contentField = value
                Me.RaisePropertyChanged("content")
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, Order:=1)>  _
        Public Property statusCode() As String
            Get
                Return Me.statusCodeField
            End Get
            Set
                Me.statusCodeField = value
                Me.RaisePropertyChanged("statusCode")
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, Order:=2)>  _
        Public Property statusMessage() As String
            Get
                Return Me.statusMessageField
            End Get
            Set
                Me.statusMessageField = value
                Me.RaisePropertyChanged("statusMessage")
            End Set
        End Property
        
        Public Event PropertyChanged As System.ComponentModel.PropertyChangedEventHandler Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged
        
        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As System.ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(WrapperName:="getStatus", WrapperNamespace:="http://service.sunat.gob.pe", IsWrapped:=true)>  _
    Partial Public Class getStatusRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=0),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public ticket As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal ticket As String)
            MyBase.New
            Me.ticket = ticket
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(WrapperName:="getStatusResponse", WrapperNamespace:="http://service.sunat.gob.pe", IsWrapped:=true)>  _
    Partial Public Class getStatusResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=0),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public status As WSOSE_Nubefact.statusResponse
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal status As WSOSE_Nubefact.statusResponse)
            MyBase.New
            Me.status = status
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(WrapperName:="sendBill", WrapperNamespace:="http://service.sunat.gob.pe", IsWrapped:=true)>  _
    Partial Public Class sendBillRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=0),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public fileName As String
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=1),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType:="base64Binary")>  _
        Public contentFile() As Byte
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=2),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public partyType As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal fileName As String, ByVal contentFile() As Byte, ByVal partyType As String)
            MyBase.New
            Me.fileName = fileName
            Me.contentFile = contentFile
            Me.partyType = partyType
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(WrapperName:="sendBillResponse", WrapperNamespace:="http://service.sunat.gob.pe", IsWrapped:=true)>  _
    Partial Public Class sendBillResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=0),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType:="base64Binary")>  _
        Public applicationResponse() As Byte
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal applicationResponse() As Byte)
            MyBase.New
            Me.applicationResponse = applicationResponse
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(WrapperName:="sendPack", WrapperNamespace:="http://service.sunat.gob.pe", IsWrapped:=true)>  _
    Partial Public Class sendPackRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=0),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public fileName As String
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=1),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType:="base64Binary")>  _
        Public contentFile() As Byte
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=2),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public partyType As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal fileName As String, ByVal contentFile() As Byte, ByVal partyType As String)
            MyBase.New
            Me.fileName = fileName
            Me.contentFile = contentFile
            Me.partyType = partyType
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(WrapperName:="sendPackResponse", WrapperNamespace:="http://service.sunat.gob.pe", IsWrapped:=true)>  _
    Partial Public Class sendPackResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=0),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public ticket As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal ticket As String)
            MyBase.New
            Me.ticket = ticket
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(WrapperName:="sendSummary", WrapperNamespace:="http://service.sunat.gob.pe", IsWrapped:=true)>  _
    Partial Public Class sendSummaryRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=0),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public fileName As String
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=1),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType:="base64Binary")>  _
        Public contentFile() As Byte
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=2),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public partyType As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal fileName As String, ByVal contentFile() As Byte, ByVal partyType As String)
            MyBase.New
            Me.fileName = fileName
            Me.contentFile = contentFile
            Me.partyType = partyType
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(WrapperName:="sendSummaryResponse", WrapperNamespace:="http://service.sunat.gob.pe", IsWrapped:=true)>  _
    Partial Public Class sendSummaryResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=0),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public ticket As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal ticket As String)
            MyBase.New
            Me.ticket = ticket
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(WrapperName:="getStatusCdr", WrapperNamespace:="http://service.sunat.gob.pe", IsWrapped:=true)>  _
    Partial Public Class getStatusCdrRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=0),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public rucComprobante As String
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=1),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public tipoComprobante As String
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=2),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public serieComprobante As String
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=3),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public numeroComprobante As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal rucComprobante As String, ByVal tipoComprobante As String, ByVal serieComprobante As String, ByVal numeroComprobante As String)
            MyBase.New
            Me.rucComprobante = rucComprobante
            Me.tipoComprobante = tipoComprobante
            Me.serieComprobante = serieComprobante
            Me.numeroComprobante = numeroComprobante
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(WrapperName:="getStatusCdrResponse", WrapperNamespace:="http://service.sunat.gob.pe", IsWrapped:=true)>  _
    Partial Public Class getStatusCdrResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute([Namespace]:="http://service.sunat.gob.pe", Order:=0),  _
         System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public statusCdr As WSOSE_Nubefact.statusCdr
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal statusCdr As WSOSE_Nubefact.statusCdr)
            MyBase.New
            Me.statusCdr = statusCdr
        End Sub
    End Class
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")>  _
    Public Interface billServiceChannel
        Inherits WSOSE_Nubefact.billService, System.ServiceModel.IClientChannel
    End Interface
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")>  _
    Partial Public Class billServiceClient
        Inherits System.ServiceModel.ClientBase(Of WSOSE_Nubefact.billService)
        Implements WSOSE_Nubefact.billService
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String)
            MyBase.New(endpointConfigurationName)
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As String)
            MyBase.New(endpointConfigurationName, remoteAddress)
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
            MyBase.New(endpointConfigurationName, remoteAddress)
        End Sub
        
        Public Sub New(ByVal binding As System.ServiceModel.Channels.Binding, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
            MyBase.New(binding, remoteAddress)
        End Sub
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function WSOSE_Nubefact_billService_getStatus(ByVal request As WSOSE_Nubefact.getStatusRequest) As WSOSE_Nubefact.getStatusResponse Implements WSOSE_Nubefact.billService.getStatus
            Return MyBase.Channel.getStatus(request)
        End Function
        
        Public Function getStatus(ByVal ticket As String) As WSOSE_Nubefact.statusResponse
            Dim inValue As WSOSE_Nubefact.getStatusRequest = New WSOSE_Nubefact.getStatusRequest()
            inValue.ticket = ticket
            Dim retVal As WSOSE_Nubefact.getStatusResponse = CType(Me,WSOSE_Nubefact.billService).getStatus(inValue)
            Return retVal.status
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function WSOSE_Nubefact_billService_getStatusAsync(ByVal request As WSOSE_Nubefact.getStatusRequest) As System.Threading.Tasks.Task(Of WSOSE_Nubefact.getStatusResponse) Implements WSOSE_Nubefact.billService.getStatusAsync
            Return MyBase.Channel.getStatusAsync(request)
        End Function
        
        Public Function getStatusAsync(ByVal ticket As String) As System.Threading.Tasks.Task(Of WSOSE_Nubefact.getStatusResponse)
            Dim inValue As WSOSE_Nubefact.getStatusRequest = New WSOSE_Nubefact.getStatusRequest()
            inValue.ticket = ticket
            Return CType(Me,WSOSE_Nubefact.billService).getStatusAsync(inValue)
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function WSOSE_Nubefact_billService_sendBill(ByVal request As WSOSE_Nubefact.sendBillRequest) As WSOSE_Nubefact.sendBillResponse Implements WSOSE_Nubefact.billService.sendBill
            Return MyBase.Channel.sendBill(request)
        End Function
        
        Public Function sendBill(ByVal fileName As String, ByVal contentFile() As Byte, ByVal partyType As String) As Byte()
            Dim inValue As WSOSE_Nubefact.sendBillRequest = New WSOSE_Nubefact.sendBillRequest()
            inValue.fileName = fileName
            inValue.contentFile = contentFile
            inValue.partyType = partyType
            Dim retVal As WSOSE_Nubefact.sendBillResponse = CType(Me,WSOSE_Nubefact.billService).sendBill(inValue)
            Return retVal.applicationResponse
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function WSOSE_Nubefact_billService_sendBillAsync(ByVal request As WSOSE_Nubefact.sendBillRequest) As System.Threading.Tasks.Task(Of WSOSE_Nubefact.sendBillResponse) Implements WSOSE_Nubefact.billService.sendBillAsync
            Return MyBase.Channel.sendBillAsync(request)
        End Function
        
        Public Function sendBillAsync(ByVal fileName As String, ByVal contentFile() As Byte, ByVal partyType As String) As System.Threading.Tasks.Task(Of WSOSE_Nubefact.sendBillResponse)
            Dim inValue As WSOSE_Nubefact.sendBillRequest = New WSOSE_Nubefact.sendBillRequest()
            inValue.fileName = fileName
            inValue.contentFile = contentFile
            inValue.partyType = partyType
            Return CType(Me,WSOSE_Nubefact.billService).sendBillAsync(inValue)
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function WSOSE_Nubefact_billService_sendPack(ByVal request As WSOSE_Nubefact.sendPackRequest) As WSOSE_Nubefact.sendPackResponse Implements WSOSE_Nubefact.billService.sendPack
            Return MyBase.Channel.sendPack(request)
        End Function
        
        Public Function sendPack(ByVal fileName As String, ByVal contentFile() As Byte, ByVal partyType As String) As String
            Dim inValue As WSOSE_Nubefact.sendPackRequest = New WSOSE_Nubefact.sendPackRequest()
            inValue.fileName = fileName
            inValue.contentFile = contentFile
            inValue.partyType = partyType
            Dim retVal As WSOSE_Nubefact.sendPackResponse = CType(Me,WSOSE_Nubefact.billService).sendPack(inValue)
            Return retVal.ticket
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function WSOSE_Nubefact_billService_sendPackAsync(ByVal request As WSOSE_Nubefact.sendPackRequest) As System.Threading.Tasks.Task(Of WSOSE_Nubefact.sendPackResponse) Implements WSOSE_Nubefact.billService.sendPackAsync
            Return MyBase.Channel.sendPackAsync(request)
        End Function
        
        Public Function sendPackAsync(ByVal fileName As String, ByVal contentFile() As Byte, ByVal partyType As String) As System.Threading.Tasks.Task(Of WSOSE_Nubefact.sendPackResponse)
            Dim inValue As WSOSE_Nubefact.sendPackRequest = New WSOSE_Nubefact.sendPackRequest()
            inValue.fileName = fileName
            inValue.contentFile = contentFile
            inValue.partyType = partyType
            Return CType(Me,WSOSE_Nubefact.billService).sendPackAsync(inValue)
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function WSOSE_Nubefact_billService_sendSummary(ByVal request As WSOSE_Nubefact.sendSummaryRequest) As WSOSE_Nubefact.sendSummaryResponse Implements WSOSE_Nubefact.billService.sendSummary
            Return MyBase.Channel.sendSummary(request)
        End Function
        
        Public Function sendSummary(ByVal fileName As String, ByVal contentFile() As Byte, ByVal partyType As String) As String
            Dim inValue As WSOSE_Nubefact.sendSummaryRequest = New WSOSE_Nubefact.sendSummaryRequest()
            inValue.fileName = fileName
            inValue.contentFile = contentFile
            inValue.partyType = partyType
            Dim retVal As WSOSE_Nubefact.sendSummaryResponse = CType(Me,WSOSE_Nubefact.billService).sendSummary(inValue)
            Return retVal.ticket
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function WSOSE_Nubefact_billService_sendSummaryAsync(ByVal request As WSOSE_Nubefact.sendSummaryRequest) As System.Threading.Tasks.Task(Of WSOSE_Nubefact.sendSummaryResponse) Implements WSOSE_Nubefact.billService.sendSummaryAsync
            Return MyBase.Channel.sendSummaryAsync(request)
        End Function
        
        Public Function sendSummaryAsync(ByVal fileName As String, ByVal contentFile() As Byte, ByVal partyType As String) As System.Threading.Tasks.Task(Of WSOSE_Nubefact.sendSummaryResponse)
            Dim inValue As WSOSE_Nubefact.sendSummaryRequest = New WSOSE_Nubefact.sendSummaryRequest()
            inValue.fileName = fileName
            inValue.contentFile = contentFile
            inValue.partyType = partyType
            Return CType(Me,WSOSE_Nubefact.billService).sendSummaryAsync(inValue)
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function WSOSE_Nubefact_billService_getStatusCdr(ByVal request As WSOSE_Nubefact.getStatusCdrRequest) As WSOSE_Nubefact.getStatusCdrResponse Implements WSOSE_Nubefact.billService.getStatusCdr
            Return MyBase.Channel.getStatusCdr(request)
        End Function
        
        Public Function getStatusCdr(ByVal rucComprobante As String, ByVal tipoComprobante As String, ByVal serieComprobante As String, ByVal numeroComprobante As String) As WSOSE_Nubefact.statusCdr
            Dim inValue As WSOSE_Nubefact.getStatusCdrRequest = New WSOSE_Nubefact.getStatusCdrRequest()
            inValue.rucComprobante = rucComprobante
            inValue.tipoComprobante = tipoComprobante
            inValue.serieComprobante = serieComprobante
            inValue.numeroComprobante = numeroComprobante
            Dim retVal As WSOSE_Nubefact.getStatusCdrResponse = CType(Me,WSOSE_Nubefact.billService).getStatusCdr(inValue)
            Return retVal.statusCdr
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function WSOSE_Nubefact_billService_getStatusCdrAsync(ByVal request As WSOSE_Nubefact.getStatusCdrRequest) As System.Threading.Tasks.Task(Of WSOSE_Nubefact.getStatusCdrResponse) Implements WSOSE_Nubefact.billService.getStatusCdrAsync
            Return MyBase.Channel.getStatusCdrAsync(request)
        End Function
        
        Public Function getStatusCdrAsync(ByVal rucComprobante As String, ByVal tipoComprobante As String, ByVal serieComprobante As String, ByVal numeroComprobante As String) As System.Threading.Tasks.Task(Of WSOSE_Nubefact.getStatusCdrResponse)
            Dim inValue As WSOSE_Nubefact.getStatusCdrRequest = New WSOSE_Nubefact.getStatusCdrRequest()
            inValue.rucComprobante = rucComprobante
            inValue.tipoComprobante = tipoComprobante
            inValue.serieComprobante = serieComprobante
            inValue.numeroComprobante = numeroComprobante
            Return CType(Me,WSOSE_Nubefact.billService).getStatusCdrAsync(inValue)
        End Function
    End Class
End Namespace
