﻿Imports System.Xml
Imports System.Security.Cryptography.X509Certificates
Imports System.Security.Cryptography.Xml

Public Class FrmEmpresa_Certificado_Edicion
    Public Empresa As eEmpresa
    Dim MisCertificados As New List(Of eCertificadoCredencial)
    Dim Log As New Log_CERTIFICADO
    Private Sub FrmEmpresa_Certificado_Edicion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Cargar()
        CargarCerfificados()
        EstadoCambio("0")
    End Sub
    Sub Cargar()
        If Not IsNothing(Empresa) Then
            TxtEmpRUC.Tag = Empresa.Emp_Codigo
            TxtEmpRUC.Text = Empresa.Emp_NumRuc
            TxtEmpRazonSocial.Text = Empresa.Emp_NombreRazonSocial
        End If
    End Sub
    Sub CargarCerfificados()
        MisCertificados = Log.Buscar_Lista(Empresa.Emp_NumRuc)
        RefrescarVista()
    End Sub
    Sub RefrescarVista()
        DgvGeneral.DataSource = Nothing
        If MisCertificados.Count > 0 Then
            DgvGeneral.DataSource = MisCertificados
        End If
    End Sub
    Private Sub BtnSalir_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnSalir.ItemClick
        Me.Close()
    End Sub
    Dim Estado As String
    Sub LimpiarDatos()
        TxtCodigo.Text = ""
        ChkEsPropio.Checked = True
        TxtVigDesde.Text = ""
        TxtVigHasta.Text = ""
        TxtContrasena.Text = ""
        TxtRutaCertificado.Text = ""
    End Sub
    Sub EstadoCambio(nEstado As String)
        Estado = nEstado
        If Estado = "0" Then
            LimpiarDatos()
            GroupBox3.Enabled = False
            BtnNuevo.Enabled = True
            BtnAgregar.Enabled = False
            BtnQuitar.Enabled = False
        ElseIf Estado = "1" Then
            LimpiarDatos()
            GroupBox3.Enabled = True
            TxtCodigo.Focus()
            BtnNuevo.Enabled = True
            BtnAgregar.Enabled = True
            BtnQuitar.Enabled = False
        ElseIf Estado = "2" Then
            GroupBox3.Enabled = True
            TxtCodigo.Focus()
            BtnNuevo.Enabled = True
            BtnAgregar.Enabled = True
            BtnQuitar.Enabled = True
        End If
    End Sub
    Function LlenadoCorrecto() As Boolean
        If Not IsDate(TxtVigDesde.Text) Then
            MessageBox.Show("Ingrese una fecha correcta")
            TxtVigDesde.Focus()
            Return False
        ElseIf Not IsDate(TxtVigHasta.Text) Then
            MessageBox.Show("Ingrese una fecha correcta")
            TxtVigHasta.Focus()
            Return False
        ElseIf String.IsNullOrEmpty(TxtContrasena.Text) Then
            MessageBox.Show("Ingrese una contraseña")
            TxtContrasena.Focus()
            Return False
        ElseIf String.IsNullOrEmpty(TxtRutaCertificado.Text) Then
            MessageBox.Show("Seleccione una ruta")
            BtnBuscarRuta.Focus()
            Return False
        ElseIf ChkEsPropio.Checked = False And String.IsNullOrEmpty(TxtPropieRUC.Text) Then
            MessageBox.Show("Ingrese el RUC del proveedor de facturación electrónica")
            TxtPropieRUC.Focus()
            Return False
        ElseIf ChkEsPropio.Checked = False And String.IsNullOrEmpty(TxtPropieRazonSocial.Text) Then
            MessageBox.Show("Ingrese la razón social del proveedor de facturación electrónica")
            TxtPropieRazonSocial.Focus()
            Return False
        End If
        'BuscarDigestValue_SignatureValue()

        'If String.IsNullOrEmpty(pDigestValue) And String.IsNullOrEmpty(pSignatureValue) Then
        '    MessageBox.Show("No se pudo encontrar el DigestValue - SignatureValue")
        '    TxtRutaCertificado.Focus()
        '    Return False
        'End If
        Return True
    End Function
    Private Sub ChkEsPropio_CheckedChanged(sender As Object, e As EventArgs) Handles ChkEsPropio.CheckedChanged
        TxtPropieRUC.Enabled = Not ChkEsPropio.Checked
        TxtPropieRazonSocial.Enabled = Not ChkEsPropio.Checked
        If ChkEsPropio.Checked Then
            TxtPropieRUC.Text = Nothing
            TxtPropieRazonSocial.Text = Nothing
        End If
    End Sub
    Private Sub BtnNuevo_Click(sender As Object, e As EventArgs) Handles BtnNuevo.Click
        EstadoCambio("1")
    End Sub

    Private Sub BtnAgregar_Click(sender As Object, e As EventArgs) Handles BtnAgregar.Click
        If LlenadoCorrecto() Then
            If Estado = "1" Then 'Nuevo
                Dim Cert As New eCertificadoCredencial
                Cert.Emisor_RUC = Empresa.Emp_NumRuc
                Cert.Codigo = TxtCodigo.Text
                Cert.EsPropio = ChkEsPropio.Checked
                Cert.Propietario_RUC = TxtPropieRUC.Text
                Cert.Propietario_RazonSocial = TxtPropieRazonSocial.Text
                Cert.Valido_Desde = TxtVigDesde.Text
                Cert.Valido_Hasta = TxtVigHasta.Text
                Cert.Contrasena = TxtContrasena.Text
                Cert.Ruta = TxtRutaCertificado.Text

                ''--nuevo
                'Cert.Firma_DigestValue = pDigestValue
                'Cert.Firma_SignatureValue = pSignatureValue

                If Log.Insertar(Cert) Then
                    CargarCerfificados()
                    EstadoCambio("0")
                End If
            ElseIf Estado = "2" Then 'Modificar
                If VwDgv.GetFocusedDataSourceRowIndex > -1 Then
                    Dim Cert As New eCertificadoCredencial
                    Cert.Emisor_RUC = Empresa.Emp_NumRuc
                    Cert.Correlativo = MisCertificados(VwDgv.GetFocusedDataSourceRowIndex).Correlativo
                    Cert.Codigo = TxtCodigo.Text
                    Cert.EsPropio = ChkEsPropio.Checked
                    Cert.Propietario_RUC = TxtPropieRUC.Text
                    Cert.Propietario_RazonSocial = TxtPropieRazonSocial.Text
                    Cert.Valido_Desde = TxtVigDesde.Text
                    Cert.Valido_Hasta = TxtVigHasta.Text
                    Cert.Contrasena = TxtContrasena.Text
                    Cert.Ruta = TxtRutaCertificado.Text

                    ''--nuevo
                    'Cert.Firma_DigestValue = pDigestValue
                    'Cert.Firma_SignatureValue = pSignatureValue

                    If Log.Actualizar(Cert) Then
                        CargarCerfificados()
                        EstadoCambio("0")
                    End If
                End If
            End If
        End If
        'Limpiando variables
        'pDigestValue = ""
        'pSignatureValue = ""
    End Sub

    Private Sub BtnQuitar_Click(sender As Object, e As EventArgs) Handles BtnQuitar.Click
        If Estado = "2" Then
            If VwDgv.GetFocusedDataSourceRowIndex > -1 Then
                If Log.Eliminar(MisCertificados(VwDgv.GetFocusedDataSourceRowIndex)) Then
                    CargarCerfificados()
                    EstadoCambio("0")
                End If
            End If
        End If

    End Sub
    'Sub Renumerar()
    '    For i = 0 To MisCertificados.Count - 1
    '        MisCertificados(0).Correlativo = (i + 1)
    '    Next
    'End Sub

    Private Sub BtnBuscarRuta_Click(sender As Object, e As EventArgs) Handles BtnBuscarRuta.Click
        Using OFile As New OpenFileDialog()
            If OFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                TxtRutaCertificado.Text = OFile.FileName
            End If
        End Using

    End Sub

    Private Sub VwDgv_RowCellClick(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs) Handles VwDgv.RowCellClick
        If MisCertificados.Count > 0 Then
            EstadoCambio("2")
            TxtVigDesde.Text = MisCertificados(VwDgv.GetFocusedDataSourceRowIndex).Valido_Desde
            TxtVigHasta.Text = MisCertificados(VwDgv.GetFocusedDataSourceRowIndex).Valido_Hasta
            TxtContrasena.Text = MisCertificados(VwDgv.GetFocusedDataSourceRowIndex).Contrasena
            TxtRutaCertificado.Text = MisCertificados(VwDgv.GetFocusedDataSourceRowIndex).Ruta
            TxtCodigo.Text = MisCertificados(VwDgv.GetFocusedDataSourceRowIndex).Codigo
            ChkEsPropio.Checked = MisCertificados(VwDgv.GetFocusedDataSourceRowIndex).EsPropio
            If MisCertificados(VwDgv.GetFocusedDataSourceRowIndex).EsPropio Then
                TxtPropieRUC.Text = Nothing
                TxtPropieRazonSocial.Text = Nothing
            Else
                TxtPropieRUC.Text = MisCertificados(VwDgv.GetFocusedDataSourceRowIndex).Propietario_RUC
                TxtPropieRazonSocial.Text = MisCertificados(VwDgv.GetFocusedDataSourceRowIndex).Propietario_RazonSocial
            End If
        End If
    End Sub

#Region "Certificado"
    ''FIRMA-Inicio
    'Private pDigestValue As String
    'Private pSignatureValue As String
    'Private NombreXMLPrueba As String = "pruebafirma.xml"
    'Sub BuscarDigestValue_SignatureValue()
    '    Dim document As New XmlDocument()
    '    Dim node0 As XmlNode = document.CreateNode(XmlNodeType.Element, "", "facturacion", "ejemplo")
    '    node0.InnerText = "Ejemplo de firmado"
    '    document.AppendChild(node0)
    '    Dim xmltw As New XmlTextWriter(NombreXMLPrueba, New System.Text.UTF8Encoding(False))
    '    document.WriteTo(xmltw)
    '    xmltw.Close()

    '    Dim doc As New XmlDocument()
    '    doc.PreserveWhitespace = False
    '    doc.Load(New XmlTextReader(NombreXMLPrueba))

    '    Dim MiCertificado As X509Certificate2 = New X509Certificate2(TxtRutaCertificado.Text, TxtContrasena.Text)

    '    Dim signedXml As SignedXml = New SignedXml(doc)

    '    signedXml.SigningKey = MiCertificado.PrivateKey

    '    Dim KeyInfo As KeyInfo = New KeyInfo()
    '    Dim Reference As Reference = New Reference()

    '    Reference.Uri = ""
    '    Reference.AddTransform(New XmlDsigEnvelopedSignatureTransform())

    '    signedXml.AddReference(Reference)

    '    Dim X509Chain As X509Chain = New X509Chain()
    '    X509Chain.Build(MiCertificado)

    '    Dim local_element As X509ChainElement = X509Chain.ChainElements(0)
    '    Dim x509Data As KeyInfoX509Data = New KeyInfoX509Data(local_element.Certificate)
    '    Dim subjectName As String = local_element.Certificate.Subject

    '    x509Data.AddSubjectName(subjectName)
    '    KeyInfo.AddClause(x509Data)

    '    signedXml.KeyInfo = KeyInfo
    '    signedXml.ComputeSignature()

    '    Dim signature As XmlElement = signedXml.GetXml()
    '    signature.Prefix = "ds"

    '    For Each node As XmlNode In signature.SelectNodes("descendant-or-self::*[namespace-uri()='http://www.w3.org/2000/09/xmldsig#']")
    '        SetPrefix("ds", node)
    '        If Not String.IsNullOrEmpty(pDigestValue) And Not String.IsNullOrEmpty(pSignatureValue) Then
    '            Exit For
    '        End If
    '    Next node
    'End Sub
    'Private Sub SetPrefix(prefix As [String], node As XmlNode)
    '    If Not String.IsNullOrEmpty(pDigestValue) And Not String.IsNullOrEmpty(pSignatureValue) Then
    '        Exit Sub
    '    End If
    '    If node.Name = "DigestValue" Then
    '        pDigestValue = node.InnerText
    '    End If
    '    If node.Name = "SignatureValue" Then
    '        pSignatureValue = node.InnerText
    '    End If

    '    For Each n As XmlNode In node.ChildNodes
    '        SetPrefix(prefix, n)
    '        n.Prefix = prefix
    '    Next
    'End Sub
#End Region
End Class