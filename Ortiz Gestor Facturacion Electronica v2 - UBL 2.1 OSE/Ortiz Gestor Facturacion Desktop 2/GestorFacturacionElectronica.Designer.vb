﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GestorFacturacionElectronica
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(GestorFacturacionElectronica))
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.BtnEmpresas = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnDocumentos = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnReceptores = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnSocNegocios = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnReplicacion = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnBackup = New DevExpress.XtraBars.BarButtonItem()
        Me.BtnTicket = New DevExpress.XtraBars.BarButtonItem()
        Me.Bar3 = New DevExpress.XtraBars.Bar()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.TimerBillTareasProgramadasActualizar = New System.Windows.Forms.Timer(Me.components)
        Me.TimerEnviarResumenActualizar = New System.Windows.Forms.Timer(Me.components)
        Me.TimerActualizarEstadoResumen = New System.Windows.Forms.Timer(Me.components)
        Me.TimerEnviarBajasFB = New System.Windows.Forms.Timer(Me.components)
        Me.TimerReplicador = New System.Windows.Forms.Timer(Me.components)
        Me.Bar2 = New DevExpress.XtraBars.Bar()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1, Me.Bar3})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BtnDocumentos, Me.BtnReceptores, Me.BtnEmpresas, Me.BtnSocNegocios, Me.BtnReplicacion, Me.BtnBackup, Me.BtnTicket})
        Me.BarManager1.MaxItemId = 9
        Me.BarManager1.StatusBar = Me.Bar3
        '
        'Bar1
        '
        Me.Bar1.BarName = "Herramientas"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BtnEmpresas), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnDocumentos), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnReceptores), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnSocNegocios), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnReplicacion), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnBackup), New DevExpress.XtraBars.LinkPersistInfo(Me.BtnTicket)})
        Me.Bar1.Offset = 1
        Me.Bar1.Text = "Herramientas"
        '
        'BtnEmpresas
        '
        Me.BtnEmpresas.Caption = "Empresas"
        Me.BtnEmpresas.Glyph = CType(resources.GetObject("BtnEmpresas.Glyph"), System.Drawing.Image)
        Me.BtnEmpresas.Id = 3
        Me.BtnEmpresas.Name = "BtnEmpresas"
        Me.BtnEmpresas.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnDocumentos
        '
        Me.BtnDocumentos.Caption = "Documentos"
        Me.BtnDocumentos.Glyph = CType(resources.GetObject("BtnDocumentos.Glyph"), System.Drawing.Image)
        Me.BtnDocumentos.Id = 0
        Me.BtnDocumentos.Name = "BtnDocumentos"
        Me.BtnDocumentos.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnReceptores
        '
        Me.BtnReceptores.Caption = "Receptores Registrados"
        Me.BtnReceptores.Glyph = CType(resources.GetObject("BtnReceptores.Glyph"), System.Drawing.Image)
        Me.BtnReceptores.Id = 2
        Me.BtnReceptores.Name = "BtnReceptores"
        Me.BtnReceptores.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnSocNegocios
        '
        Me.BtnSocNegocios.Caption = "Socios Negocio"
        Me.BtnSocNegocios.Glyph = CType(resources.GetObject("BtnSocNegocios.Glyph"), System.Drawing.Image)
        Me.BtnSocNegocios.Id = 5
        Me.BtnSocNegocios.Name = "BtnSocNegocios"
        Me.BtnSocNegocios.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnReplicacion
        '
        Me.BtnReplicacion.Caption = "Replicacion"
        Me.BtnReplicacion.Glyph = CType(resources.GetObject("BtnReplicacion.Glyph"), System.Drawing.Image)
        Me.BtnReplicacion.Id = 6
        Me.BtnReplicacion.Name = "BtnReplicacion"
        Me.BtnReplicacion.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnBackup
        '
        Me.BtnBackup.Caption = "Backup"
        Me.BtnBackup.Glyph = CType(resources.GetObject("BtnBackup.Glyph"), System.Drawing.Image)
        Me.BtnBackup.Id = 7
        Me.BtnBackup.Name = "BtnBackup"
        Me.BtnBackup.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BtnTicket
        '
        Me.BtnTicket.Caption = "Consultar Ticket"
        Me.BtnTicket.Id = 8
        Me.BtnTicket.Name = "BtnTicket"
        Me.BtnTicket.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'Bar3
        '
        Me.Bar3.BarName = "Barra de estado"
        Me.Bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.Bar3.DockCol = 0
        Me.Bar3.DockRow = 0
        Me.Bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar3.OptionsBar.AllowQuickCustomization = False
        Me.Bar3.OptionsBar.DrawDragBorder = False
        Me.Bar3.OptionsBar.UseWholeRow = True
        Me.Bar3.Text = "Barra de estado"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(932, 39)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 543)
        Me.barDockControlBottom.Size = New System.Drawing.Size(932, 23)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 39)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 504)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(932, 39)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 504)
        '
        'TimerBillTareasProgramadasActualizar
        '
        Me.TimerBillTareasProgramadasActualizar.Enabled = True
        Me.TimerBillTareasProgramadasActualizar.Interval = 1033
        '
        'TimerEnviarResumenActualizar
        '
        Me.TimerEnviarResumenActualizar.Enabled = True
        Me.TimerEnviarResumenActualizar.Interval = 5000
        '
        'TimerActualizarEstadoResumen
        '
        Me.TimerActualizarEstadoResumen.Enabled = True
        Me.TimerActualizarEstadoResumen.Interval = 13000
        '
        'TimerEnviarBajasFB
        '
        Me.TimerEnviarBajasFB.Enabled = True
        Me.TimerEnviarBajasFB.Interval = 3700
        '
        'TimerReplicador
        '
        Me.TimerReplicador.Interval = 1000
        '
        'Bar2
        '
        Me.Bar2.BarName = "Menú principal"
        Me.Bar2.DockCol = 0
        Me.Bar2.DockRow = 0
        Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar2.OptionsBar.MultiLine = True
        Me.Bar2.OptionsBar.UseWholeRow = True
        Me.Bar2.Text = "Menú principal"
        '
        'GestorFacturacionElectronica
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(932, 566)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "GestorFacturacionElectronica"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = " "
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BtnDocumentos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnReceptores As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnEmpresas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Bar3 As DevExpress.XtraBars.Bar
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents TimerBillTareasProgramadasActualizar As System.Windows.Forms.Timer
    Friend WithEvents BtnSocNegocios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents TimerEnviarResumenActualizar As System.Windows.Forms.Timer
    Friend WithEvents TimerActualizarEstadoResumen As System.Windows.Forms.Timer
    Friend WithEvents TimerEnviarBajasFB As System.Windows.Forms.Timer
    Friend WithEvents BtnReplicacion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnBackup As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents TimerReplicador As System.Windows.Forms.Timer
    Private components As System.ComponentModel.IContainer
    Friend WithEvents BtnTicket As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Bar2 As DevExpress.XtraBars.Bar

End Class
