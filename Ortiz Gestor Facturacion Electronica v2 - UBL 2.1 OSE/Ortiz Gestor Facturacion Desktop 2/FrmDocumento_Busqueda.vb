﻿Imports System.IO
Imports System.Collections

Public Class FrmDocumento_Busqueda
    Dim Documentos As List(Of eProvisionFacturacion)
    Dim Provision As eProvisionFacturacion
    Dim CertificadoDigital As eCertificadoCredencial
    Private Sub FrmDocumento_Busqueda_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TxtFDesde.Text = "01/" & Format(Month(Now), "00") & "/" & Year(Now)
        TxtFHasta.Text = Now.ToShortDateString
    End Sub
    Function CodeFormat(ByVal Id As String, ByVal Tamanio As Int32) As String
        If Id.Length < Tamanio Then
            For i = Id.Length To Tamanio - 1
                Id = "0" & Id
            Next
        End If
        Return Id
    End Function
    Private Sub TxtEmpRucNum_KeyDown(sender As Object, e As KeyEventArgs) Handles TxtEmpRucNum.KeyDown
        If Not String.IsNullOrEmpty(TxtEmpRucNum.Text.Trim) Then
            Try
                If e.KeyCode = Keys.Enter And TxtEmpRucNum.Text.Substring(TxtEmpRucNum.Text.Trim.Length - 1, 1) = "*" Then
                    Using f As New FrmEmpresas_Busqueda
                        If f.ShowDialog = DialogResult.OK Then
                            TxtEmpRucNum.Text = f.Generales(f.VwDgv.GetFocusedDataSourceRowIndex).Emp_NumRuc
                            TxtEmpDescripcion.Text = f.Generales(f.VwDgv.GetFocusedDataSourceRowIndex).Emp_NombreRazonSocial
                            SendKeys.Send("{TAB}")
                        End If
                    End Using
                ElseIf e.KeyCode = Keys.Enter And TxtEmpRucNum.Text.Trim <> "" And TxtEmpDescripcion.Text.Trim = "" Then
                    Try
                        'TxtEmpCodigo.Text = CodeFormat(TxtEmpCodigo.Text, 11)
                        Dim log As New Log_EMPRESA
                        Dim Empresas As New List(Of eEmpresa)
                        Empresas = log.Buscar(New eEmpresa With {.Emp_NumRuc = TxtEmpRucNum.Text})
                        If Empresas.Count > 0 Then
                            TxtEmpDescripcion.Text = Empresas(0).Emp_NombreRazonSocial
                        End If
                    Catch ex As Exception
                        'Herramientas.ShowError(ex.Message)
                    End Try
                End If
            Catch ex As Exception
                'Herramientas.ShowError(ex.Message)
            End Try
        End If
    End Sub

    Private Sub TxtEmpRucNum_TextChanged(sender As Object, e As EventArgs) Handles TxtEmpRucNum.TextChanged
        If TxtEmpRucNum.Focus Then
            TxtEmpDescripcion.Clear()
        End If
    End Sub
    Private Sub TxtTipoDocCodigo_KeyDown(sender As Object, e As Windows.Forms.KeyEventArgs) Handles TxtTipoDocCodigo.KeyDown
        If Not String.IsNullOrEmpty(TxtTipoDocCodigo.Text.Trim) Then
            Try
                If e.KeyCode = Keys.Enter And TxtTipoDocCodigo.Text.Substring(TxtTipoDocCodigo.Text.Trim.Length - 1, 1) = "*" Then
                    Using f As New FrmTiposDoc_Buscar
                        If f.ShowDialog = DialogResult.OK Then
                            TxtTipoDocCodigo.Text = f.ListaFiltrada(f.GridView1.GetFocusedDataSourceRowIndex).TipoDoc_Codigo
                            TxtTipoDocDescripcion.Text = f.ListaFiltrada(f.GridView1.GetFocusedDataSourceRowIndex).TipoDoc_Descripcion
                            SendKeys.Send("{TAB}")
                        End If
                    End Using
                ElseIf e.KeyCode = Keys.Enter And TxtTipoDocCodigo.Text.Trim <> "" And TxtTipoDocDescripcion.Text.Trim = "" Then
                    Try
                        TxtTipoDocCodigo.Text = CodeFormat(TxtTipoDocCodigo.Text, 2)
                        Dim log As New Log_VTS_TIPO_DOC
                        Dim Documentos As New List(Of eTipoDocumento)
                        Documentos = log.Buscar(New eTipoDocumento With {.TipoDoc_Codigo = TxtTipoDocCodigo.Text})
                        If Documentos.Count > 0 Then
                            TxtTipoDocDescripcion.Text = Documentos(0).TipoDoc_Descripcion
                        End If
                    Catch ex As Exception
                        'Herramientas.ShowError(ex.Message)
                    End Try
                End If
            Catch ex As Exception
                'Herramientas.ShowError(ex.Message)
            End Try
        End If
    End Sub

    Private Sub TxtTipoDocCodigo_TextChanged(sender As Object, e As EventArgs) Handles TxtTipoDocCodigo.TextChanged
        If TxtTipoDocCodigo.Focus Then
            TxtTipoDocDescripcion.Clear()
        End If
    End Sub

    Private Sub TxtSerie_LostFocus(sender As Object, e As EventArgs) Handles TxtSerie.LostFocus
        If Not String.IsNullOrEmpty(TxtSerie.Text) Then
            TxtSerie.Text = CodeFormat(TxtSerie.Text, 4)
        End If
    End Sub

    Private Sub TxtNumero_LostFocus(sender As Object, e As EventArgs) Handles TxtNumero.LostFocus
        If Not String.IsNullOrEmpty(TxtNumero.Text) Then
            TxtNumero.Text = CodeFormat(TxtNumero.Text, 8)
        End If
    End Sub

    Private Sub BtnBuscar_Click(sender As Object, e As EventArgs) Handles BtnBuscar.Click

        Dim Log As New Log_FACTE_DOCUMENTOS
        Dim NBusq As New eProvisionFacturacion With {.Busq_Fecha_Inicio = Now, .Busq_Fecha_Fin = Now}
        Try
            NBusq.Busq_Fecha_Inicio = TxtFDesde.Text
            NBusq.Busq_Fecha_Fin = TxtFHasta.Text
            If Not String.IsNullOrEmpty(TxtEmpRucNum.Text) Then
                NBusq.Emisor_RUC = TxtEmpRucNum.Text
            End If
            If Not String.IsNullOrEmpty(TxtTipoDocCodigo.Text) Then
                NBusq.TpDc_Codigo_St = TxtTipoDocCodigo.Text
            End If
            If Not String.IsNullOrEmpty(TxtSerie.Text) Then
                NBusq.Dvt_VTSerie = TxtSerie.Text
            End If
            If Not String.IsNullOrEmpty(TxtNumero.Text) Then
                NBusq.Dvt_VTNumer = TxtNumero.Text
            End If
        Catch ex As Exception
        End Try
        Documentos = Log.Buscar_Documento(NBusq)
        GridControl1.DataSource = Nothing
        If Documentos.Count > 0 Then
            GridControl1.DataSource = Documentos
        Else
            TxtFDesde.Focus()
        End If

    End Sub

    Private Sub BtnEnviarCorreoCliente_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnEnviarCorreoCliente.ItemClick

        Try

            If IsNothing(CertificadoDigital) Then
                CertificadoDigital = BuscarCredenciales(Provision)
            End If
            If CertificadoDigital.Emisor_RUC <> Provision.Emisor_RUC Then
                CertificadoDigital = BuscarCredenciales(Provision)
            End If

            Provision.CertificadoCredencial = CertificadoDigital
            Dim nProceso As New fcProceso
            nProceso.Emisor_RUC = Provision.Emisor_RUC
            nProceso.SUNAT_Usuario = Provision.CertificadoCredencial.Sunat_Usuario
            nProceso.SUNAT_Contrasena = Provision.CertificadoCredencial.Sunat_Contrasena
            nProceso.ServidorRutaLocal = Provision.Path_Root_App & "\" & Provision.Emisor_RUC
            nProceso.DocumentoTipo = Provision.TpDc_Codigo_St
            'nProceso.FTP_Direccion = Provision.CertificadoCredencial.FTP_Direccion
            'nProceso.FTP_Carpeta = Provision.CertificadoCredencial.FTP_Carpeta
            'nProceso.FTP_Usuario = Provision.CertificadoCredencial.FTP_Usuario
            'nProceso.FTP_Contrasena = Provision.CertificadoCredencial.FTP_Contrasena
            nProceso.Etapa = Provision.Etapa_Codigo
            nProceso.FileNameWithOutExtension = Path.GetFileNameWithoutExtension(Provision.Nombre_XML)

            If HerramientasFE.EnviarCorreo(Provision, nProceso) Then
                Dim LogAcep As New Log_FACTE_DOCUMENTOS
                LogAcep.Actualizar_Envio_Correo(Provision)
                MsgBox("Mensaje enviado satisfactoriamente al correo: " & Provision.Cliente_Correo_Electronico)
            End If
        Catch ex As Exception
            Using fv As New FrmVersionesXMLCliente_Visor
                Dim ErrorCo As String = "Message:------ " & vbCrLf & ex.Message & ", InnerException:------ " & vbCrLf & ex.InnerException.ToString & ", HResult:------ " & vbCrLf & ex.HResult.ToString
                fv.FileName = ErrorCo
                fv.Show()
            End Using
        End Try

    End Sub
    Function BuscarCredenciales(pProvision As eProvisionFacturacion) As eCertificadoCredencial
        Dim LogCertificado As New Log_CERTIFICADO
        Return LogCertificado.Buscar(pProvision.Emisor_RUC, pProvision.Dvt_Fecha_Emision, Nothing)
    End Function
   
    Private Sub BtnVersiones_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnVersiones.ItemClick
        If GridView1.GetFocusedDataSourceRowIndex > -1 Then
            Using f As New FrmVersionesXMLCliente
                f.DocumentoElectronico = Documentos(GridView1.GetFocusedDataSourceRowIndex)
                f.ShowDialog()
            End Using
        End If

    End Sub

    Private Sub BtnVerXML_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnVerXML.ItemClick
        'If Me.GridView1.GetFocusedRowCellValue("Archivo") Is Nothing Then
        '    Return
        'End If
        If GridView1.GetFocusedDataSourceRowIndex > -1 Then

            Using f As New FrmVersionesXMLCliente_Visor
                Dim fAud As New fcProceso
                fAud.ServidorRutaLocal = Documentos(GridView1.GetFocusedDataSourceRowIndex).Path_Root_App & "\" & Documentos(GridView1.GetFocusedDataSourceRowIndex).Emisor_RUC
                fAud.Emisor_RUC = Documentos(GridView1.GetFocusedDataSourceRowIndex).Emisor_RUC
                fAud.DocumentoTipo = Documentos(GridView1.GetFocusedDataSourceRowIndex).TpDc_Codigo_St

                f.FileName = fAud.ServidorRuta_Generado & Documentos(GridView1.GetFocusedDataSourceRowIndex).Nombre_XML
                f.ShowDialog()
            End Using

        End If

    End Sub

    Private Sub BtnVerPDF_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnVerPDF.ItemClick
        If GridView1.GetFocusedDataSourceRowIndex > -1 Then

            'If Me.GridView1.GetFocusedRowCellValue("SUNAT") = "ACEPTADO" Then
            Using FPDF As New FrmVisorPDF

                Dim fAud As New fcProceso
                fAud.ServidorRutaLocal = Documentos(GridView1.GetFocusedDataSourceRowIndex).Path_Root_App & "\" & Documentos(GridView1.GetFocusedDataSourceRowIndex).Emisor_RUC
                fAud.Emisor_RUC = Documentos(GridView1.GetFocusedDataSourceRowIndex).Emisor_RUC
                fAud.DocumentoTipo = Documentos(GridView1.GetFocusedDataSourceRowIndex).TpDc_Codigo_St
                FPDF.FileName = fAud.ServidorRuta_Aceptado & IO.Path.GetFileNameWithoutExtension(Documentos(GridView1.GetFocusedDataSourceRowIndex).Nombre_XML) & ".PDF"

                FPDF.ShowDialog()
            End Using
            'Else
            '    MessageBox.Show("El documento fue rechazado, no posee PDF")
            'End If

        End If
    End Sub

    Private Sub BtnVerCDR_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnVerCDR.ItemClick
        If GridView1.GetFocusedDataSourceRowIndex > -1 Then



        End If
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        If GridView1.RowCount > 0 And GridView1.GetFocusedDataSourceRowIndex > -1 Then
            Provision = Documentos.Item(GridView1.GetFocusedDataSourceRowIndex)
        End If
    End Sub

    Private Sub BtnVerSunat_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnVerSunat.ItemClick
        Dim Rpta As String = HerramientasFE.Consultar_Estado_Sunat(Provision.Emisor_RUC, Provision.TpDc_Codigo_St, Provision.Dvt_VTSerie, Provision.Dvt_VTNumer)

        If Not String.IsNullOrWhiteSpace(Rpta) Then
            MessageBox.Show(Rpta, "ESTADO SUNAT")
        End If
        
    End Sub

    Private Sub BtnVerOSE_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BtnVerOSE.ItemClick

    End Sub
End Class