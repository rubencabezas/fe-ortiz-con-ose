﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TxtDocTipo = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TxtDocFechEmision = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TxtMotivoBajaDescripcion = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TxtDocNumero = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TxtDocSerie = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TxtEmisorRazonSocial = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TxtEmisorUbigeo = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TxtEmisorRUC = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.BtnEnviarDocumento = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.BtnItemAgregar = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.TxtItemSubtotal = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.TxtItemValorVenta = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TxtItemIGV = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TxtItemIGVPorc = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TxtItemPrecio = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TxtItemTipoIGV = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TxtItemCant = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TxtItemUnid = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TxtItemDescripcion = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TxtItemCodigo = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TxtItem = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.TxtTotalVenta = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.TxtTotalIGV = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.TxtTotalGravadas = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.Button20 = New System.Windows.Forms.Button()
        Me.Button21 = New System.Windows.Forms.Button()
        Me.Button22 = New System.Windows.Forms.Button()
        Me.Button24 = New System.Windows.Forms.Button()
        Me.Button23 = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Button25 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox7.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TxtDocTipo)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.TxtDocFechEmision)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.TxtMotivoBajaDescripcion)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.TxtDocNumero)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TxtDocSerie)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 86)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(360, 70)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Documento Cabecera"
        '
        'TxtDocTipo
        '
        Me.TxtDocTipo.Location = New System.Drawing.Point(44, 17)
        Me.TxtDocTipo.Name = "TxtDocTipo"
        Me.TxtDocTipo.Size = New System.Drawing.Size(23, 20)
        Me.TxtDocTipo.TabIndex = 9
        Me.TxtDocTipo.Text = "01"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(14, 20)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(28, 13)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "T.D."
        '
        'TxtDocFechEmision
        '
        Me.TxtDocFechEmision.Location = New System.Drawing.Point(288, 17)
        Me.TxtDocFechEmision.Name = "TxtDocFechEmision"
        Me.TxtDocFechEmision.Size = New System.Drawing.Size(64, 20)
        Me.TxtDocFechEmision.TabIndex = 7
        Me.TxtDocFechEmision.Text = "01-01-2016"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(259, 20)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(26, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "F.E."
        '
        'TxtMotivoBajaDescripcion
        '
        Me.TxtMotivoBajaDescripcion.Location = New System.Drawing.Point(104, 43)
        Me.TxtMotivoBajaDescripcion.Name = "TxtMotivoBajaDescripcion"
        Me.TxtMotivoBajaDescripcion.Size = New System.Drawing.Size(248, 20)
        Me.TxtMotivoBajaDescripcion.TabIndex = 5
        Me.TxtMotivoBajaDescripcion.Text = "Duplicado"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 46)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Razon Anulación"
        '
        'TxtDocNumero
        '
        Me.TxtDocNumero.Location = New System.Drawing.Point(195, 17)
        Me.TxtDocNumero.Name = "TxtDocNumero"
        Me.TxtDocNumero.Size = New System.Drawing.Size(59, 20)
        Me.TxtDocNumero.TabIndex = 3
        Me.TxtDocNumero.Text = "00000040"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(151, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Numero"
        '
        'TxtDocSerie
        '
        Me.TxtDocSerie.Location = New System.Drawing.Point(108, 17)
        Me.TxtDocSerie.Name = "TxtDocSerie"
        Me.TxtDocSerie.Size = New System.Drawing.Size(36, 20)
        Me.TxtDocSerie.TabIndex = 1
        Me.TxtDocSerie.Text = "F001"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(75, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Serie"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2})
        Me.DataGridView1.Location = New System.Drawing.Point(7, 17)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.Size = New System.Drawing.Size(226, 52)
        Me.DataGridView1.TabIndex = 5
        '
        'Column1
        '
        Me.Column1.HeaderText = "Column1"
        Me.Column1.Name = "Column1"
        '
        'Column2
        '
        Me.Column2.HeaderText = "Column2"
        Me.Column2.Name = "Column2"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(239, 17)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(56, 23)
        Me.Button5.TabIndex = 6
        Me.Button5.Text = "Agregar"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(239, 46)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(56, 23)
        Me.Button6.TabIndex = 7
        Me.Button6.Text = "Quitar"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(378, 168)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(121, 26)
        Me.Button7.TabIndex = 8
        Me.Button7.Text = "Estado de documento"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TxtEmisorRazonSocial)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.TxtEmisorUbigeo)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.TxtEmisorRUC)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(360, 71)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Emisor"
        '
        'TxtEmisorRazonSocial
        '
        Me.TxtEmisorRazonSocial.Location = New System.Drawing.Point(62, 42)
        Me.TxtEmisorRazonSocial.Name = "TxtEmisorRazonSocial"
        Me.TxtEmisorRazonSocial.Size = New System.Drawing.Size(286, 20)
        Me.TxtEmisorRazonSocial.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(10, 45)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(50, 13)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "R. Social"
        '
        'TxtEmisorUbigeo
        '
        Me.TxtEmisorUbigeo.Location = New System.Drawing.Point(248, 19)
        Me.TxtEmisorUbigeo.Name = "TxtEmisorUbigeo"
        Me.TxtEmisorUbigeo.Size = New System.Drawing.Size(100, 20)
        Me.TxtEmisorUbigeo.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(203, 22)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Ubigeo"
        '
        'TxtEmisorRUC
        '
        Me.TxtEmisorRUC.Location = New System.Drawing.Point(62, 19)
        Me.TxtEmisorRUC.Name = "TxtEmisorRUC"
        Me.TxtEmisorRUC.Size = New System.Drawing.Size(131, 20)
        Me.TxtEmisorRUC.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(10, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(30, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "RUC"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.BtnEnviarDocumento)
        Me.GroupBox3.Location = New System.Drawing.Point(378, 18)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(252, 65)
        Me.GroupBox3.TabIndex = 10
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Acciones con FactE"
        '
        'BtnEnviarDocumento
        '
        Me.BtnEnviarDocumento.Location = New System.Drawing.Point(9, 19)
        Me.BtnEnviarDocumento.Name = "BtnEnviarDocumento"
        Me.BtnEnviarDocumento.Size = New System.Drawing.Size(109, 28)
        Me.BtnEnviarDocumento.TabIndex = 0
        Me.BtnEnviarDocumento.Text = "Enviar Documento"
        Me.BtnEnviarDocumento.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(378, 351)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(121, 37)
        Me.Button8.TabIndex = 15
        Me.Button8.Text = "Generar XML - Guia Vehiculo Propio"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.DataGridView1)
        Me.GroupBox4.Controls.Add(Me.Button5)
        Me.GroupBox4.Controls.Add(Me.Button6)
        Me.GroupBox4.Location = New System.Drawing.Point(12, 389)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(360, 75)
        Me.GroupBox4.TabIndex = 11
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Adjuntos"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.BtnItemAgregar)
        Me.GroupBox5.Controls.Add(Me.Button9)
        Me.GroupBox5.Controls.Add(Me.TxtItemSubtotal)
        Me.GroupBox5.Controls.Add(Me.Label19)
        Me.GroupBox5.Controls.Add(Me.TxtItemValorVenta)
        Me.GroupBox5.Controls.Add(Me.Label18)
        Me.GroupBox5.Controls.Add(Me.TxtItemIGV)
        Me.GroupBox5.Controls.Add(Me.Label17)
        Me.GroupBox5.Controls.Add(Me.TxtItemIGVPorc)
        Me.GroupBox5.Controls.Add(Me.Label16)
        Me.GroupBox5.Controls.Add(Me.TxtItemPrecio)
        Me.GroupBox5.Controls.Add(Me.Label15)
        Me.GroupBox5.Controls.Add(Me.TxtItemTipoIGV)
        Me.GroupBox5.Controls.Add(Me.Label14)
        Me.GroupBox5.Controls.Add(Me.TxtItemCant)
        Me.GroupBox5.Controls.Add(Me.Label13)
        Me.GroupBox5.Controls.Add(Me.TxtItemUnid)
        Me.GroupBox5.Controls.Add(Me.Label12)
        Me.GroupBox5.Controls.Add(Me.TxtItemDescripcion)
        Me.GroupBox5.Controls.Add(Me.Label11)
        Me.GroupBox5.Controls.Add(Me.TxtItemCodigo)
        Me.GroupBox5.Controls.Add(Me.Label10)
        Me.GroupBox5.Controls.Add(Me.TxtItem)
        Me.GroupBox5.Controls.Add(Me.Label9)
        Me.GroupBox5.Location = New System.Drawing.Point(12, 159)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(360, 124)
        Me.GroupBox5.TabIndex = 12
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Edicion de detalle"
        '
        'BtnItemAgregar
        '
        Me.BtnItemAgregar.Location = New System.Drawing.Point(21, 94)
        Me.BtnItemAgregar.Name = "BtnItemAgregar"
        Me.BtnItemAgregar.Size = New System.Drawing.Size(56, 23)
        Me.BtnItemAgregar.TabIndex = 22
        Me.BtnItemAgregar.Text = "Agregar"
        Me.BtnItemAgregar.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(83, 94)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(56, 23)
        Me.Button9.TabIndex = 23
        Me.Button9.Text = "Quitar"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'TxtItemSubtotal
        '
        Me.TxtItemSubtotal.Enabled = False
        Me.TxtItemSubtotal.Location = New System.Drawing.Point(288, 68)
        Me.TxtItemSubtotal.Name = "TxtItemSubtotal"
        Me.TxtItemSubtotal.Size = New System.Drawing.Size(60, 20)
        Me.TxtItemSubtotal.TabIndex = 21
        Me.TxtItemSubtotal.Text = "18"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(257, 71)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(53, 13)
        Me.Label19.TabIndex = 20
        Me.Label19.Text = "Sub Total"
        '
        'TxtItemValorVenta
        '
        Me.TxtItemValorVenta.Enabled = False
        Me.TxtItemValorVenta.Location = New System.Drawing.Point(194, 68)
        Me.TxtItemValorVenta.Name = "TxtItemValorVenta"
        Me.TxtItemValorVenta.Size = New System.Drawing.Size(60, 20)
        Me.TxtItemValorVenta.TabIndex = 19
        Me.TxtItemValorVenta.Text = "18"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(158, 71)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(62, 13)
        Me.Label18.TabIndex = 18
        Me.Label18.Text = "Valor Venta"
        '
        'TxtItemIGV
        '
        Me.TxtItemIGV.Enabled = False
        Me.TxtItemIGV.Location = New System.Drawing.Point(97, 68)
        Me.TxtItemIGV.Name = "TxtItemIGV"
        Me.TxtItemIGV.Size = New System.Drawing.Size(57, 20)
        Me.TxtItemIGV.TabIndex = 17
        Me.TxtItemIGV.Text = "0"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(71, 71)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(25, 13)
        Me.Label17.TabIndex = 16
        Me.Label17.Text = "IGV"
        '
        'TxtItemIGVPorc
        '
        Me.TxtItemIGVPorc.Enabled = False
        Me.TxtItemIGVPorc.Location = New System.Drawing.Point(44, 68)
        Me.TxtItemIGVPorc.Name = "TxtItemIGVPorc"
        Me.TxtItemIGVPorc.Size = New System.Drawing.Size(23, 20)
        Me.TxtItemIGVPorc.TabIndex = 15
        Me.TxtItemIGVPorc.Text = "18"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(8, 71)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(36, 13)
        Me.Label16.TabIndex = 14
        Me.Label16.Text = "IGV %"
        '
        'TxtItemPrecio
        '
        Me.TxtItemPrecio.Location = New System.Drawing.Point(250, 45)
        Me.TxtItemPrecio.Name = "TxtItemPrecio"
        Me.TxtItemPrecio.Size = New System.Drawing.Size(48, 20)
        Me.TxtItemPrecio.TabIndex = 13
        Me.TxtItemPrecio.Text = "567.80"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(207, 48)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(37, 13)
        Me.Label15.TabIndex = 12
        Me.Label15.Text = "Precio"
        '
        'TxtItemTipoIGV
        '
        Me.TxtItemTipoIGV.Enabled = False
        Me.TxtItemTipoIGV.Location = New System.Drawing.Point(177, 45)
        Me.TxtItemTipoIGV.Name = "TxtItemTipoIGV"
        Me.TxtItemTipoIGV.Size = New System.Drawing.Size(20, 20)
        Me.TxtItemTipoIGV.TabIndex = 11
        Me.TxtItemTipoIGV.Text = "13"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(124, 48)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(49, 13)
        Me.Label14.TabIndex = 10
        Me.Label14.Text = "Tipo IGV"
        '
        'TxtItemCant
        '
        Me.TxtItemCant.Location = New System.Drawing.Point(97, 45)
        Me.TxtItemCant.Name = "TxtItemCant"
        Me.TxtItemCant.Size = New System.Drawing.Size(26, 20)
        Me.TxtItemCant.TabIndex = 9
        Me.TxtItemCant.Text = "1"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(71, 48)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(32, 13)
        Me.Label13.TabIndex = 8
        Me.Label13.Text = "Cant."
        '
        'TxtItemUnid
        '
        Me.TxtItemUnid.Enabled = False
        Me.TxtItemUnid.Location = New System.Drawing.Point(44, 45)
        Me.TxtItemUnid.Name = "TxtItemUnid"
        Me.TxtItemUnid.Size = New System.Drawing.Size(23, 20)
        Me.TxtItemUnid.TabIndex = 7
        Me.TxtItemUnid.Text = "ZZ"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(18, 48)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(32, 13)
        Me.Label12.TabIndex = 6
        Me.Label12.Text = "Unid."
        '
        'TxtItemDescripcion
        '
        Me.TxtItemDescripcion.Location = New System.Drawing.Point(189, 19)
        Me.TxtItemDescripcion.Name = "TxtItemDescripcion"
        Me.TxtItemDescripcion.Size = New System.Drawing.Size(159, 20)
        Me.TxtItemDescripcion.TabIndex = 5
        Me.TxtItemDescripcion.Text = resources.GetString("TxtItemDescripcion.Text")
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(148, 22)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(46, 13)
        Me.Label11.TabIndex = 4
        Me.Label11.Text = "Descrip."
        '
        'TxtItemCodigo
        '
        Me.TxtItemCodigo.Location = New System.Drawing.Point(97, 19)
        Me.TxtItemCodigo.Name = "TxtItemCodigo"
        Me.TxtItemCodigo.Size = New System.Drawing.Size(38, 20)
        Me.TxtItemCodigo.TabIndex = 3
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(69, 22)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(29, 13)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "Cod."
        '
        'TxtItem
        '
        Me.TxtItem.Location = New System.Drawing.Point(44, 19)
        Me.TxtItem.Name = "TxtItem"
        Me.TxtItem.Size = New System.Drawing.Size(23, 20)
        Me.TxtItem.TabIndex = 1
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(14, 22)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(27, 13)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Item"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.DataGridView2)
        Me.GroupBox6.Location = New System.Drawing.Point(12, 286)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(360, 59)
        Me.GroupBox6.TabIndex = 13
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Detalles"
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Location = New System.Drawing.Point(11, 14)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.RowHeadersVisible = False
        Me.DataGridView2.Size = New System.Drawing.Size(339, 41)
        Me.DataGridView2.TabIndex = 6
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.TxtTotalVenta)
        Me.GroupBox7.Controls.Add(Me.Label22)
        Me.GroupBox7.Controls.Add(Me.TxtTotalIGV)
        Me.GroupBox7.Controls.Add(Me.Label20)
        Me.GroupBox7.Controls.Add(Me.TxtTotalGravadas)
        Me.GroupBox7.Controls.Add(Me.Label21)
        Me.GroupBox7.Location = New System.Drawing.Point(12, 349)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(360, 36)
        Me.GroupBox7.TabIndex = 14
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Resumen"
        '
        'TxtTotalVenta
        '
        Me.TxtTotalVenta.Enabled = False
        Me.TxtTotalVenta.Location = New System.Drawing.Point(262, 11)
        Me.TxtTotalVenta.Name = "TxtTotalVenta"
        Me.TxtTotalVenta.Size = New System.Drawing.Size(38, 20)
        Me.TxtTotalVenta.TabIndex = 23
        Me.TxtTotalVenta.Text = "0"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(224, 14)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(31, 13)
        Me.Label22.TabIndex = 22
        Me.Label22.Text = "Total"
        '
        'TxtTotalIGV
        '
        Me.TxtTotalIGV.Enabled = False
        Me.TxtTotalIGV.Location = New System.Drawing.Point(177, 11)
        Me.TxtTotalIGV.Name = "TxtTotalIGV"
        Me.TxtTotalIGV.Size = New System.Drawing.Size(35, 20)
        Me.TxtTotalIGV.TabIndex = 21
        Me.TxtTotalIGV.Text = "0"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(147, 14)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(25, 13)
        Me.Label20.TabIndex = 20
        Me.Label20.Text = "IGV"
        '
        'TxtTotalGravadas
        '
        Me.TxtTotalGravadas.Enabled = False
        Me.TxtTotalGravadas.Location = New System.Drawing.Point(97, 11)
        Me.TxtTotalGravadas.Name = "TxtTotalGravadas"
        Me.TxtTotalGravadas.Size = New System.Drawing.Size(38, 20)
        Me.TxtTotalGravadas.TabIndex = 19
        Me.TxtTotalGravadas.Text = "0"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(26, 14)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(73, 13)
        Me.Label21.TabIndex = 18
        Me.Label21.Text = "Op. Gravadas"
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(378, 94)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(121, 37)
        Me.Button10.TabIndex = 16
        Me.Button10.Text = "Enviar Boleta de Venta"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(500, 94)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(121, 37)
        Me.Button11.TabIndex = 17
        Me.Button11.Text = "Enviar Factura"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(500, 137)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(121, 23)
        Me.Button13.TabIndex = 19
        Me.Button13.Text = "Baja de factura"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(378, 137)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(121, 23)
        Me.Button14.TabIndex = 20
        Me.Button14.Text = "Baja de Boleta"
        Me.Button14.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(500, 168)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(121, 26)
        Me.Button1.TabIndex = 21
        Me.Button1.Text = "Estado de documento"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(378, 206)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(121, 37)
        Me.Button2.TabIndex = 22
        Me.Button2.Text = "Enviar Nota Credito"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(500, 206)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(121, 37)
        Me.Button3.TabIndex = 23
        Me.Button3.Text = "Enviar Nota Debito"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(378, 249)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(121, 23)
        Me.Button4.TabIndex = 25
        Me.Button4.Text = "Baja de Nota Credito"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(500, 249)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(121, 23)
        Me.Button12.TabIndex = 24
        Me.Button12.Text = "Baja de Nota Debito"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(500, 282)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(121, 37)
        Me.Button15.TabIndex = 27
        Me.Button15.Text = "Enviar Percepcion"
        Me.Button15.UseVisualStyleBackColor = True
        '
        'Button16
        '
        Me.Button16.Location = New System.Drawing.Point(378, 282)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(121, 37)
        Me.Button16.TabIndex = 26
        Me.Button16.Text = "Enviar Retencion"
        Me.Button16.UseVisualStyleBackColor = True
        '
        'Button17
        '
        Me.Button17.Location = New System.Drawing.Point(378, 322)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(121, 23)
        Me.Button17.TabIndex = 29
        Me.Button17.Text = "Baja de Retencion"
        Me.Button17.UseVisualStyleBackColor = True
        '
        'Button18
        '
        Me.Button18.Location = New System.Drawing.Point(500, 322)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(121, 23)
        Me.Button18.TabIndex = 28
        Me.Button18.Text = "Baja de Percepcion"
        Me.Button18.UseVisualStyleBackColor = True
        '
        'Button19
        '
        Me.Button19.Location = New System.Drawing.Point(378, 394)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(193, 23)
        Me.Button19.TabIndex = 30
        Me.Button19.Text = "Enviar XML Sunat"
        Me.Button19.UseVisualStyleBackColor = True
        '
        'Button20
        '
        Me.Button20.Location = New System.Drawing.Point(378, 423)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(197, 23)
        Me.Button20.TabIndex = 31
        Me.Button20.Text = "Factura con Documento Relacionado"
        Me.Button20.UseVisualStyleBackColor = True
        '
        'Button21
        '
        Me.Button21.Location = New System.Drawing.Point(500, 351)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(121, 37)
        Me.Button21.TabIndex = 32
        Me.Button21.Text = "Generar XML - Guia Transporte Publico"
        Me.Button21.UseVisualStyleBackColor = True
        '
        'Button22
        '
        Me.Button22.Location = New System.Drawing.Point(378, 481)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(197, 23)
        Me.Button22.TabIndex = 33
        Me.Button22.Text = "Factura con Percepcion"
        Me.Button22.UseVisualStyleBackColor = True
        '
        'Button24
        '
        Me.Button24.Location = New System.Drawing.Point(378, 452)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(197, 23)
        Me.Button24.TabIndex = 35
        Me.Button24.Text = "Factura emisor iterante"
        Me.Button24.UseVisualStyleBackColor = True
        '
        'Button23
        '
        Me.Button23.Location = New System.Drawing.Point(627, 94)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(126, 37)
        Me.Button23.TabIndex = 36
        Me.Button23.Text = "Homologar"
        Me.Button23.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(581, 406)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(160, 66)
        Me.PictureBox1.TabIndex = 37
        Me.PictureBox1.TabStop = False
        '
        'Button25
        '
        Me.Button25.Location = New System.Drawing.Point(33, 475)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(193, 23)
        Me.Button25.TabIndex = 38
        Me.Button25.Text = "Enviar XML Cliente a Servicio"
        Me.Button25.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(808, 510)
        Me.Controls.Add(Me.Button25)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Button23)
        Me.Controls.Add(Me.Button24)
        Me.Controls.Add(Me.Button22)
        Me.Controls.Add(Me.Button21)
        Me.Controls.Add(Me.Button20)
        Me.Controls.Add(Me.Button19)
        Me.Controls.Add(Me.Button17)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button18)
        Me.Controls.Add(Me.Button15)
        Me.Controls.Add(Me.Button16)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button14)
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TxtDocNumero As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TxtDocSerie As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TxtMotivoBajaDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TxtEmisorRazonSocial As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TxtEmisorUbigeo As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TxtEmisorRUC As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TxtDocTipo As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TxtDocFechEmision As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents BtnEnviarDocumento As System.Windows.Forms.Button
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents TxtItem As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TxtItemIGVPorc As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TxtItemPrecio As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TxtItemTipoIGV As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TxtItemCant As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TxtItemUnid As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TxtItemDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TxtItemCodigo As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TxtItemSubtotal As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents TxtItemValorVenta As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TxtItemIGV As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents BtnItemAgregar As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents TxtTotalVenta As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents TxtTotalIGV As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents TxtTotalGravadas As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents Button17 As System.Windows.Forms.Button
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Friend WithEvents Button20 As System.Windows.Forms.Button
    Friend WithEvents Button21 As System.Windows.Forms.Button
    Friend WithEvents Button22 As System.Windows.Forms.Button
    Friend WithEvents Button24 As System.Windows.Forms.Button
    Friend WithEvents Button23 As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Button25 As System.Windows.Forms.Button

End Class
