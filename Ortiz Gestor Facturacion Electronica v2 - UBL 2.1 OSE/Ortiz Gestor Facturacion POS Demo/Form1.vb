﻿Imports System.IO
Imports System.Xml
Imports System.Security.Cryptography.X509Certificates
Imports System.Security.Cryptography.Xml
Imports System.ServiceModel
Imports System.ServiceModel.Security
Imports System.ServiceModel.Channels

Public Class Form1
    Private Emisor_RUC As String = "20542134926"
    Private Emisor_RazonSocial As String = "ESTACIONES DE SERVICIO ORTIZ S.A.C."
    Private Emisor_Ubigeo As String = "020101"

    Private Cliente_Boleta_DNI As String = "42171355"
    Private Cliente_Boleta_Nombres As String = "Donny Huaman Novoa"
    Private Cliente_Factura_RUC As String = "10421713559"
    Private Cliente_Factura_RazonSocial As String = "Donny Huaman Novoa"

    Private Documento_Boleta_Serie As String = "BP01"
    Private Documento_Factura_Serie As String = "FF11"
    Private Documento_NotaCredito_Serie As String = "FF11"
    Private Documento_NotaDebito_Serie As String = "FF11"

    Dim ListaDetalles As New List(Of DocumentoElectronicoDetalle)
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TxtEmisorRUC.Text = Emisor_RUC
        TxtEmisorRazonSocial.Text = Emisor_RazonSocial
        TxtEmisorUbigeo.Text = Emisor_Ubigeo
        TxtDocFechEmision.Text = Now.ToShortDateString()
    End Sub





    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Using Dlg As New OpenFileDialog
            If Dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
                Me.DataGridView1.Rows.Add(Dlg.SafeFileName, Dlg.FileName)
            End If
        End Using
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        Dim PruebaXML__1 As New ws_documentoelectronico
        Dim Rst As String = PruebaXML__1.Documento_Estado(Emisor_RUC, "03", Documento_Boleta_Serie, TxtDocNumero.Text)
        If Rst.Split("#").Count > 1 Then
            'Encontró los el documento
            MessageBox.Show(Rst)
        Else
            MessageBox.Show(Rst)
        End If

    End Sub

    Private Sub BtnEnviarDocumento_Click(sender As Object, e As EventArgs) Handles BtnEnviarDocumento.Click
        'GENERAR XML
        Dim Doc As New DocumentoElectronico
        Doc.Fecha_Emision = CDate(TxtDocFechEmision.Text)
        Doc.Emisor_ApellidosNombres_RazonSocial = Emisor_RazonSocial
        'Doc.Emisor_NombreComercial = ""
        Doc.Emisor_Documento_Tipo = "6"
        Doc.Emisor_Documento_Numero = Emisor_RUC

        '--- Domicilio fiscal -- PostalAddress
        Doc.Emisor_Direccion_Ubigeo = Emisor_Ubigeo
        Doc.Emisor_Direccion_Calle = "Calle Nº 456"
        Doc.Emisor_Direccion_Urbanizacion = "Ubanizacion "
        Doc.Emisor_Direccion_Departamento = "ANCASH"
        Doc.Emisor_Direccion_Provincia = "HUARAZ"
        Doc.Emisor_Direccion_Distrito = "HUARAZ"
        Doc.Emisor_Direccion_PaisCodigo = ""

        Doc.TipoDocumento_Codigo = TxtDocTipo.Text
        If TxtDocTipo.Text = "01" Then
            Doc.TipoDocumento_Descripcion = "FACTURA ELECTRONICA"
            Doc.Cliente_Documento_Tipo = "6"
            Doc.Cliente_Documento_Numero = Cliente_Factura_RUC
            Doc.Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial
            Doc.Cliente_Direccion = "Calle Los Geranios 2367 – Urb. Matellini – Chorrillos - Lima"
        Else
            Doc.TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA"
            Doc.Cliente_Documento_Tipo = "1"
            Doc.Cliente_Documento_Numero = Cliente_Boleta_DNI
            Doc.Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres
            Doc.Cliente_Direccion = "Calle Los Geranios 2367 – Urb. Matellini – Chorrillos - Lima"
        End If

        Doc.TipoDocumento_Serie = TxtDocSerie.Text
        Doc.TipoDocumento_Numero = TxtDocNumero.Text

        'Doc.Cliente_Documento_Tipo = "1"
        'Doc.Cliente_Documento_Numero = "42171355" '"10421713559"



        Doc.Detalles = ListaDetalles.ToArray ' New DocumentoElectronicoDetalle() {New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "Cap-258963", .Producto_Descripcion = "CAPTOPRIL 25mg X 30", .Producto_UnidadMedida_Codigo = "ZZ", .Producto_Cantidad = 2, .Unitario_Precio_Venta = 180, .Unitario_Precio_Tipo_Codigo = "01", .Item_IGV_Total = 32.4, .Unitario_IGV_Porcentaje = 18, .Item_Tipo_Afectacion_IGV_Codigo = "13", .Item_ISC_Total = 0, .Unitario_Valor_Unitario = 147.6, .Item_ValorVenta_Total = 295.2}}

        Doc.Total_ValorVenta_OperacionesGravadas = TxtTotalGravadas.Text
        Doc.Total_ValorVenta_OperacionesInafectas = 0
        Doc.Total_ValorVenta_OperacionesExoneradas = 0
        Doc.Total_ValorVenta_OperacionesGratuitas = 0
        Doc.Total_IGV = TxtTotalIGV.Text
        Doc.Total_ISC = 0
        Doc.Total_OtrosTributos = 0
        Doc.Total_OtrosCargos = 0
        ''Doc.Descuentos_Globales = 0

        Doc.Total_Importe_Venta = TxtTotalVenta.Text
        Doc.Moneda_Codigo = "PEN"
        Doc.Moneda_Descripcion = "SOLES"
        Doc.Moneda_Simbolo = "S/"

        Doc.GuiasRemision = New DocumentoElectronicoReferenciaDocumentos() {New DocumentoElectronicoReferenciaDocumentos With {.Documento_Tipo = "09", .Documento_Serie = "0001", .Documento_Numero = "00000009"}}

        'Doc.Leyendas = New DocumentoElectronicoLeyenda() {New DocumentoElectronicoLeyenda With {.Codigo = "1000", .Descripcion = "DOSCIENTOS SOLES CON 00/100"}, New DocumentoElectronicoLeyenda With {.Codigo = "2000", .Descripcion = "COMPROBANTE DE PERCEPCION"}}
        Doc.Leyendas = New DocumentoElectronicoLeyenda() {New DocumentoElectronicoLeyenda With {.Codigo = "1000", .Descripcion = "DOSCIENTOS SOLES CON 00/100"}}

        Doc.Total_Importe_Percepcion = 0

        Dim settings As New Xml.XmlWriterSettings With {.ConformanceLevel = Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}

        Using writer As Xml.XmlWriter = Xml.XmlWriter.Create("DocFactura.xml", settings)
            Dim typeToSerialize As Type = GetType(DocumentoElectronico)
            Dim xs As New Xml.Serialization.XmlSerializer(typeToSerialize)
            xs.Serialize(writer, Doc)
        End Using

        Dim docS As New Xml.XmlDocument()
        docS.Load("DocFactura.xml")

        'Dim xmlClientFile As New Xml.XmlDocument
        'xmlClientFile.Load("DocFactura.xml")
        'Dim DocXMLClass As New DocumentoElectronico
        'Dim reader As New System.Xml.Serialization.XmlSerializer(DocXMLClass.[GetType]())
        'Dim reader2 As XmlNodeReader = New XmlNodeReader(xmlClientFile)
        'DocXMLClass = DirectCast(reader.Deserialize(reader2), DocumentoElectronico)

        Dim PruebaXML__1 As New ws_documentoelectronico
        Dim RptaWS As String = PruebaXML__1.Documento_RegistrarActualizar(docS)
        Dim StrResult As String() = RptaWS.Split("#")

        'Devuelve: ( Value 0 | Value 1 | Value 2 | Value 3 )
        'Devuelve(0): - error, 0 documento enviado, no se puede actualizar, 1 documento registrado/actualizado
        'Devuelve(1): Key - Nombre unico web
        'Devuelve(2): DigestValue (Codigo Hash)
        'Devuelve(3): Código de Barras

        If StrResult(0) <> "0" Then 'Success
            'For i As Integer = 0 To DataGridView1.Rows.Count - 1

            '    Dim Archivo_RutaLocal As String = DataGridView1.Rows(i).Cells(1).Value.ToString
            '    Dim Archivo_Nombre As String = Path.GetFileName(DataGridView1.Rows(i).Cells(1).Value.ToString) 'Path.GetFileName(i i.ToString & Path.GetExtension(DataGridView1.Rows(i).Cells(1).Value.ToString)
            '    Dim Archivo_Alias As String = "01_" & TxtDocSerie.Text & "_" & TxtDocNumero.Text & "_" & StrResult(0).Trim & "_" & i.ToString & Path.GetExtension(DataGridView1.Rows(i).Cells(1).Value.ToString)

            '    Dim objfilestream As New FileStream(Archivo_RutaLocal, FileMode.Open, FileAccess.Read)
            '    Dim len As Integer = CInt(objfilestream.Length)
            '    Dim mybytearray As [Byte]() = New [Byte](len - 1) {}
            '    objfilestream.Read(mybytearray, 0, len)

            '    PruebaXML__1.Documento_Adjunto_Subir(mybytearray, Archivo_Alias, Archivo_Nombre, DocXMLClass.Emisor_Documento_Numero, DocXMLClass.TipoDocumento_Codigo, DocXMLClass.TipoDocumento_Serie, DocXMLClass.TipoDocumento_Numero, StrResult(0))
            '    objfilestream.Close()
            'Next
        End If

        MessageBox.Show(RptaWS)

        ListaDetalles.Clear()
        DataGridView2.DataSource = Nothing
        DataGridView2.Refresh()

        DataGridView1.DataSource = Nothing
        DataGridView1.Refresh()
    End Sub

    Private Sub BtnItemAgregar_Click(sender As Object, e As EventArgs) Handles BtnItemAgregar.Click
        Dim Det As New DocumentoElectronicoDetalle
        Det.Item = ListaDetalles.Count + 1
        Det.Producto_Codigo = TxtItemCodigo.Text
        Det.Producto_Descripcion = TxtItemDescripcion.Text
        Det.Producto_UnidadMedida_Codigo = TxtItemUnid.Text
        Det.Producto_Cantidad = TxtItemCant.Text
        Det.Unitario_Precio_Venta = TxtItemPrecio.Text
        Det.Unitario_Precio_Venta_Tipo_Codigo = "01"
        Det.Item_IGV_Total = TxtItemIGV.Text
        Det.Unitario_IGV_Porcentaje = 0.18
        Det.Item_Tipo_Afectacion_IGV_Codigo = TxtItemTipoIGV.Text
        Det.Item_ISC_Total = 0
        Det.Unitario_Valor_Unitario = TxtItemValorVenta.Text
        Det.Item_ValorVenta_Total = TxtItemSubtotal.Text
        ListaDetalles.Add(Det)
        DataGridView2.DataSource = Nothing
        DataGridView2.DataSource = ListaDetalles
        MostrarResumen()
    End Sub

    Private Sub TxtItemPrecio_TextChanged(sender As Object, e As EventArgs) Handles TxtItemPrecio.TextChanged
        If IsNumeric(TxtItemPrecio.Text) And IsNumeric(TxtItemCant.Text) Then
            TxtItemValorVenta.Text = Math.Round(CDbl(TxtItemPrecio.Text) / 1.18, 2)
            TxtItemIGV.Text = Math.Round((CDbl(TxtItemPrecio.Text) / 1.18) * 0.18, 2)
            TxtItemSubtotal.Text = Math.Round(CDbl(TxtItemPrecio.Text) * CDbl(TxtItemCant.Text), 2)
        End If
    End Sub
    Sub MostrarResumen()
        Dim OpGrav, IGV, Total As Double
        For Each Item In ListaDetalles
            OpGrav = OpGrav + Item.Unitario_Valor_Unitario
            IGV = IGV + Item.Item_IGV_Total
            Total = Total + Item.Item_ValorVenta_Total
        Next
        TxtTotalGravadas.Text = OpGrav
        TxtTotalIGV.Text = IGV
        TxtTotalVenta.Text = Total
    End Sub
    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        'GENERAR XML
        Dim Doc As New DocumentoElectronico

        Doc.Fecha_Emision = CDate(TxtDocFechEmision.Text)
        Doc.Emisor_ApellidosNombres_RazonSocial = Emisor_RazonSocial
        'Doc.Emisor_NombreComercial = ""
        Doc.Emisor_Documento_Tipo = "6"
        Doc.Emisor_Documento_Numero = Emisor_RUC

        '--- Domicilio fiscal -- PostalAddress
        Doc.Emisor_Direccion_Ubigeo = Emisor_Ubigeo
        Doc.Emisor_Direccion_Calle = "Car. Antigua Tacllan KM. 2"
        Doc.Emisor_Direccion_Urbanizacion = "Barrio de Tacllan "
        Doc.Emisor_Direccion_Departamento = "ANCASH"
        Doc.Emisor_Direccion_Provincia = "HUARAZ"
        Doc.Emisor_Direccion_Distrito = "HUARAZ"
        Doc.Emisor_Direccion_PaisCodigo = "PE"


        'If TxtDocTipo.Text = "01" Then
        '    Doc.TipoDocumento_Codigo = "01"
        '    Doc.TipoDocumento_Descripcion = "FACTURA ELECTRONICA"
        '    Doc.Cliente_Documento_Tipo = "6"
        '    Doc.Cliente_Documento_Numero = Cliente_Factura_RUC
        '    Doc.Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial
        '    Doc.Cliente_Direccion = "Calle Los Geranios 2367 – Urb. Matellini – Chorrillos - Lima"
        'Else
        Doc.TipoDocumento_Codigo = "03"
        Doc.TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA"
        Doc.Cliente_Documento_Tipo = "1"
        Doc.Cliente_Documento_Numero = Cliente_Boleta_DNI
        Doc.Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres
        Doc.Cliente_Direccion = "Calle Los Geranios 2367 – Urb. Matellini – Chorrillos - Lima"

        Doc.TipoDocumento_Serie = Documento_Boleta_Serie
        Doc.TipoDocumento_Numero = TxtDocNumero.Text

        Doc.Detalles = New DocumentoElectronicoDetalle() {New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "Cap-258963", .Producto_Descripcion = "CAPTOPRIL 25mg X 30", .Producto_UnidadMedida_Codigo = "ZZ", .Producto_Cantidad = 2, .Unitario_Precio_Venta = 180, .Unitario_Precio_Venta_Tipo_Codigo = "01", .Item_IGV_Total = 32.4, .Unitario_IGV_Porcentaje = 18, .Item_Tipo_Afectacion_IGV_Codigo = "13", .Item_ISC_Total = 0, .Unitario_Valor_Unitario = 147.6, .Item_ValorVenta_Total = 295.2}}

        Doc.Total_ValorVenta_OperacionesGravadas = 295.2
        Doc.Total_ValorVenta_OperacionesInafectas = 0
        Doc.Total_ValorVenta_OperacionesExoneradas = 0
        Doc.Total_ValorVenta_OperacionesGratuitas = 0
        Doc.Total_IGV = 64.8
        Doc.IGV_Porcentaje = 18
        Doc.Total_ISC = 0
        Doc.Total_OtrosTributos = 0
        Doc.Total_OtrosCargos = 0
        'Doc.Descuentos_Globales = 0

        Doc.Total_Importe_Venta = 360
        Doc.Moneda_Codigo = "PEN"
        Doc.Moneda_Descripcion = "SOLES"
        Doc.Moneda_Simbolo = "S/"
        Doc.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc.Total_Importe_Venta)) & " " & Doc.Moneda_Descripcion

        Doc.DocumentoRelacionados = New DocumentoElectronicoReferenciaDocumentos() {New DocumentoElectronicoReferenciaDocumentos With {.Documento_Tipo = "09", .Documento_Serie = "0001", .Documento_Numero = "00000009"}}

        'Doc.Leyendas = New DocumentoElectronicoLeyenda() {New DocumentoElectronicoLeyenda With {.Codigo = "1000", .Descripcion = "DOSCIENTOS SOLES CON 00/100"}, New DocumentoElectronicoLeyenda With {.Codigo = "2000", .Descripcion = "COMPROBANTE DE PERCEPCION"}}
        'Doc.Leyendas = New DocumentoElectronicoLeyenda() {New DocumentoElectronicoLeyenda With {.Codigo = "1000", .Descripcion = "DOSCIENTOS SOLES CON 00/100"}}

        '   45 Leyendas
        Dim ListaLeyendas As New List(Of DocumentoElectronicoLeyenda)
        ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "1000", .Descripcion = NroEnLetras(Math.Truncate(Doc.Total_Importe_Venta)) & " " & Doc.Moneda_Descripcion})
        If Doc.Total_Importe_Venta = 0 Then
            ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "1002", .Descripcion = "TRANSFERENCIA GRATUITA DE UN BIEN Y/O SERVICIO PRESTADO GRATUITAMENTE"})
        End If
        If Doc.Total_Importe_Percepcion > 0 Then
            ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "2000", .Descripcion = "COMPROBANTE DE PERCEPCION"})
        End If
        If Not String.IsNullOrEmpty(Doc.Tipo_Operacion_Codigo) Then
            If Doc.Tipo_Operacion_Codigo = "05" Then
                ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "2005", .Descripcion = "VENTA REALIZADA POR EMISOR ITINERANTE"})
            End If
        End If
        Doc.Leyendas = ListaLeyendas.ToArray

        Doc.Total_Importe_Percepcion = 0

        Doc.Impresion_Modelo_Codigo = "000"

        Dim settings As New Xml.XmlWriterSettings With {.ConformanceLevel = Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}
        Using writer As Xml.XmlWriter = Xml.XmlWriter.Create("DocFactura.xml", settings)
            Dim typeToSerialize As Type = GetType(DocumentoElectronico)
            Dim xs As New Xml.Serialization.XmlSerializer(typeToSerialize)
            xs.Serialize(writer, Doc)
        End Using

        Dim doc12 As New Xml.XmlDocument()
        doc12.Load("DocFactura.xml")

        Dim PruebaXML__1 As New ws_documentoelectronico
        'Dim RptaWS As String = PruebaXML__1.Documento_RegistrarActualizar(doc12)
        Dim RptaWS As String = PruebaXML__1.Documento_RegistrarActualizar(doc12)
        'If RptaWS <> "0" Then
        Dim ParmPrint As String() = RptaWS.Split("#")
        Doc.Print_DigestValue = ParmPrint(2)
        Dim VarBarras As String = ParmPrint(3)

        If ParmPrint(0).Trim <> "0" Then
            Imprimir(Doc, VarBarras)
        Else
            MessageBox.Show(RptaWS)
        End If



        'Dim PdF417Generator1 As DevExpress.XtraPrinting.BarCode.PDF417Generator = New DevExpress.XtraPrinting.BarCode.PDF417Generator()
        'Dim XrBarCode1 As New DevExpress.XtraReports.UI.XRBarCode()
        ''XrBarCode1
        ''
        'XrBarCode1.AutoModule = True
        'XrBarCode1.Borders = DevExpress.XtraPrinting.BorderSide.None
        'XrBarCode1.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        'XrBarCode1.LocationFloat = New DevExpress.Utils.PointFloat(17.93036!, 104.9392!)
        'XrBarCode1.Name = "XrBarCode1"
        'XrBarCode1.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
        'XrBarCode1.ShowText = False
        'XrBarCode1.SizeF = New System.Drawing.SizeF(282.1334!, 118.415!)
        'XrBarCode1.StylePriority.UseBorders = False
        'XrBarCode1.StylePriority.UseFont = False
        'PdF417Generator1.Columns = 12
        'PdF417Generator1.CompactionMode = DevExpress.XtraPrinting.BarCode.PDF417CompactionMode.Binary
        'PdF417Generator1.ErrorCorrectionLevel = DevExpress.XtraPrinting.BarCode.ErrorCorrectionLevel.Level5
        'PdF417Generator1.Rows = 40
        'XrBarCode1.Symbology = PdF417Generator1
        'XrBarCode1.BinaryData = System.Text.Encoding.UTF8.GetBytes(VarBarras)
        'PictureBox1.Image = XrBarCode1.ToImage()

        'Dim bc As DevExpress.XtraEditors.BarCodeControl = CreatePDF417BarCode(VarBarras)
        'Dim im As New Bitmap(500, 500)
        'bc.DrawToBitmap(im, New Rectangle(0, 0, 500, 500))
        'im.Save("Test.png", System.Drawing.Imaging.ImageFormat.Png)
    End Sub
    Public Function CreatePDF417BarCode(BarCodeText As String) As DevExpress.XtraEditors.BarCodeControl

        Dim PdF417Generator1 As DevExpress.XtraPrinting.BarCode.PDF417Generator = New DevExpress.XtraPrinting.BarCode.PDF417Generator()
        PdF417Generator1.Columns = 12
        PdF417Generator1.CompactionMode = DevExpress.XtraPrinting.BarCode.PDF417CompactionMode.Binary
        PdF417Generator1.ErrorCorrectionLevel = DevExpress.XtraPrinting.BarCode.ErrorCorrectionLevel.Level5
        PdF417Generator1.Rows = 40

        ' Create a bar code control.
        Dim barCode As New DevExpress.XtraEditors.BarCodeControl()
        barCode.AutoModule = True
        'barCode.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        barCode.Name = "XrBarCode1"
        barCode.ShowText = False
        barCode.Size = New System.Drawing.Size(282.1334!, 118.415!)
        barCode.Symbology = PdF417Generator1
        barCode.BinaryData = System.Text.Encoding.UTF8.GetBytes(BarCodeText)

        Return barCode

    End Function
    Sub Imprimir(Provision As DocumentoElectronico, pBarCodeValue As String)
        Dim Lista As New List(Of DocumentoElectronico)
        Lista.Add(Provision)

        Dim Rpte As New Print_Invoice
        'Para que se autoajuste al tamaño del papel
        Rpte.PrintingSystem.Document.AutoFitToPagesWidth = 1
        'Rpte.RollPaper = True '16.1
        Rpte.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.HundredthsOfAnInch
        'Rpte.PageWidth = 315 '315 (~ 80 mm)
        '   En el reporte en el desing
        'Me.BindingSource1.DataSource = GetType(Ortiz.DocumentoElectronico)

        'Codigo barras PDF417
        Rpte.XrBarCode1.BinaryData = System.Text.Encoding.UTF8.GetBytes(pBarCodeValue)

        Rpte.XrLabel8.Text = Now.ToShortTimeString
        Rpte.XrLabel28.Text = Now.ToShortDateString

        'Notas adicionales
        Dim NotasAdicionales As String = ""
        If Not IsNothing(Provision.Leyendas) Then
            If Provision.Leyendas.Count > 1 Then
                For i = 1 To Provision.Leyendas.Count - 1
                    NotasAdicionales = NotasAdicionales & Provision.Leyendas(0).Descripcion
                Next
            End If
        End If


        ''Fecha vencimiento
        'If Provision.Fecha_Emision = Provision.Fecha_Vencimiento Or Provision.Fecha_Vencimiento.Year = 1 Then
        '    'Contado - No mostramos nada
        '    Rpte.XrLabel16.Visible = False
        '    Rpte.XrLabel43.Visible = False
        'Else
        '    NotasAdicionales = NotasAdicionales & "- EL PAGO DE ESTA FACTURA DESPUES DE LA FECHA DE VENCIMIENTO PODRIA GENERAR INTERESES COMPENSATORIOS Y MORATORIOS A LAS TASAS MAXIMAS PERMITIDAS POR LEY." & vbNewLine & vbNewLine
        'End If
        Provision.Sunat_Autorizacion = "Autorizado mediante Resolución Nº " & "987645"
        Provision.Emisor_Web_Visualizacion = "Consulte su documento en " & "www.grupoortiz.pe/facturacion"
        Provision.RepresentacionImpresaDela = "Representación Impresa de la " & Provision.TipoDocumento_Descripcion


        'Rpte.XrRichText1.Text = NotasAdicionales

        Rpte.DataSource = Lista

        Rpte.CreateDocument()

        Rpte.PrinterName = "Star BSC10"

        'Rpte.print
        Using printTool As New DevExpress.XtraReports.UI.ReportPrintTool(Rpte)
            'or printTool.PrintDialog();
            'printTool.
            printTool.Print()
        End Using
    End Sub
    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        'GENERAR XML
        Dim Doc As New DocumentoElectronico
        Doc.Fecha_Emision = CDate(TxtDocFechEmision.Text)
        Doc.Emisor_ApellidosNombres_RazonSocial = Emisor_RazonSocial
        'Doc.Emisor_NombreComercial = ""
        Doc.Emisor_Documento_Tipo = "6"
        Doc.Emisor_Documento_Numero = Emisor_RUC

        '--- Domicilio fiscal -- PostalAddress
        Doc.Emisor_Direccion_Ubigeo = Emisor_Ubigeo
        Doc.Emisor_Direccion_Calle = "Calle Nº 456"
        Doc.Emisor_Direccion_Urbanizacion = "Ubanizacion "
        Doc.Emisor_Direccion_Departamento = "ANCASH"
        Doc.Emisor_Direccion_Provincia = "HUARAZ"
        Doc.Emisor_Direccion_Distrito = "HUARAZ"
        Doc.Emisor_Direccion_PaisCodigo = "PE"

        Doc.TipoDocumento_Codigo = "01"

        Doc.TipoDocumento_Descripcion = "FACTURA ELECTRONICA"
        Doc.Cliente_Documento_Tipo = "6"
        Doc.Cliente_Documento_Numero = Cliente_Factura_RUC
        Doc.Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial
        Doc.Cliente_Direccion = "Calle Los Geranios 2367 – Urb. Matellini – Chorrillos - Lima"
        Doc.Cliente_Correo = "kelvincabezas@gmail.com"

        Doc.TipoDocumento_Serie = Documento_Factura_Serie
        Doc.TipoDocumento_Numero = TxtDocNumero.Text

        Doc.Detalles = New DocumentoElectronicoDetalle() {New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "Cap-258963", .Producto_Descripcion = "CAPTOPRIL 25mg X 30", .Producto_UnidadMedida_Codigo = "ZZ", .Producto_Cantidad = 2, .Unitario_Precio_Venta = 180, .Unitario_Precio_Venta_Tipo_Codigo = "01", .Item_IGV_Total = 32.4, .Unitario_IGV_Porcentaje = 18, .Item_Tipo_Afectacion_IGV_Codigo = "13", .Item_ISC_Total = 0, .Unitario_Valor_Unitario = 147.6, .Item_ValorVenta_Total = 295.2}}

        Doc.Total_ValorVenta_OperacionesGravadas = 295.2
        Doc.Total_ValorVenta_OperacionesInafectas = 0
        Doc.Total_ValorVenta_OperacionesExoneradas = 0
        Doc.Total_ValorVenta_OperacionesGratuitas = 0
        Doc.Total_IGV = 64.8
        Doc.IGV_Porcentaje = 18
        Doc.Total_ISC = 0
        Doc.Total_OtrosTributos = 0
        Doc.Total_OtrosCargos = 0
        'Doc.Descuentos_Globales = 0

        Doc.Total_Importe_Venta = 360
        Doc.Moneda_Codigo = "PEN"
        Doc.Moneda_Descripcion = "SOLES"
        Doc.Moneda_Simbolo = "S/"
        Doc.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc.Total_Importe_Venta)) & " " & Doc.Moneda_Descripcion

        'Doc.DocumentoRelacionados = New DocumentoElectronicoReferenciaDocumentos() {New DocumentoElectronicoReferenciaDocumentos With {.Documento_Tipo = "09", .Documento_Serie = "0001", .Documento_Numero = "00000009"}}

        Dim ListaLeyendas As New List(Of DocumentoElectronicoLeyenda)
        ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "1000", .Descripcion = NroEnLetras(Math.Truncate(Doc.Total_Importe_Venta)) & " " & Doc.Moneda_Descripcion})
        If Doc.Total_Importe_Venta = 0 Then
            ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "1002", .Descripcion = "TRANSFERENCIA GRATUITA DE UN BIEN Y/O SERVICIO PRESTADO GRATUITAMENTE"})
        End If
        If Doc.Total_Importe_Percepcion > 0 Then
            ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "2000", .Descripcion = "COMPROBANTE DE PERCEPCION"})
        End If
        If Not String.IsNullOrEmpty(Doc.Tipo_Operacion_Codigo) Then
            If Doc.Tipo_Operacion_Codigo = "05" Then
                ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "2005", .Descripcion = "VENTA REALIZADA POR EMISOR ITINERANTE"})
            End If
        End If
        Doc.Leyendas = ListaLeyendas.ToArray

        Doc.Total_Importe_Percepcion = 0

        Doc.Impresion_Modelo_Codigo = "000"

        Dim settings As New Xml.XmlWriterSettings With {.ConformanceLevel = Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}

        Using writer As Xml.XmlWriter = Xml.XmlWriter.Create("DocFactura.xml", settings)
            Dim typeToSerialize As Type = GetType(DocumentoElectronico)
            Dim xs As New Xml.Serialization.XmlSerializer(typeToSerialize)
            xs.Serialize(writer, Doc)
        End Using

        Dim doc12 As New Xml.XmlDocument()
        doc12.Load("DocFactura.xml")

        Dim PruebaXML__1 As New ws_documentoelectronico
        'Dim RptaWS As String = PruebaXML__1.Documento_RegistrarActualizar(doc12)
        Dim RptaWS As String = PruebaXML__1.Documento_RegistrarActualizar_Enviar(doc12)
        'If RptaWS <> "0" Then
        Dim ParmPrint As String() = RptaWS.Split("#")
        Doc.Print_DigestValue = ParmPrint(2)
        Dim VarBarras As String = ParmPrint(3)

        If ParmPrint(0).Trim <> "0" Then
            Imprimir(Doc, VarBarras)
        Else
            MessageBox.Show(RptaWS)
        End If
    End Sub

    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click
        Dim Doc As New DocumentoElectronicoComunicacionBaja
        Doc.Documentos = New DocumentoElectronico() {New DocumentoElectronico With {.Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = Emisor_RUC, .TipoDocumento_Codigo = "01", .TipoDocumento_Serie = Documento_Factura_Serie, .TipoDocumento_Numero = TxtDocNumero.Text, .RazonAnulacion = TxtMotivoBajaDescripcion.Text}}

        Dim settings As New Xml.XmlWriterSettings With {.ConformanceLevel = Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}

        Using writer As Xml.XmlWriter = Xml.XmlWriter.Create("BajaDocumento.xml", settings)
            Dim typeToSerialize As Type = GetType(DocumentoElectronicoComunicacionBaja)
            Dim xs As New Xml.Serialization.XmlSerializer(typeToSerialize)
            xs.Serialize(writer, Doc)
        End Using

        'Dim stringwriter As IO.StringWriter = New IO.StringWriter()
        'Dim x As Xml.Serialization.XmlSerializer = New Xml.Serialization.XmlSerializer(Doc.GetType())
        ''Convierte a XML y lo almacena en un StringWriter
        'x.Serialize(stringwriter, Doc)
        'Using f As New FrmVersionesXMLCliente_Visor
        '    f.VarText = stringwriter.ToString()
        '    f.ShowDialog()
        'End Using

        '--El envio
        Dim doce As New Xml.XmlDocument()
        doce.Load("BajaDocumento.xml")

        Dim PruebaXML__1 As New ws_documentoelectronico

        MessageBox.Show(PruebaXML__1.Documento_DarBaja(doce))
    End Sub

    Private Sub Button14_Click(sender As Object, e As EventArgs) Handles Button14.Click

        Dim Doc As New DocumentoElectronicoComunicacionBaja

        Doc.Documentos = New DocumentoElectronico() {New DocumentoElectronico With {.Emisor_Documento_Tipo = "6",
                                                                                    .Emisor_Documento_Numero = Emisor_RUC,
                                                                                    .TipoDocumento_Codigo = "03",
                                                                                    .TipoDocumento_Serie = TxtDocSerie.Text,
                                                                                    .TipoDocumento_Numero = TxtDocNumero.Text,
                                                                                    .RazonAnulacion = TxtMotivoBajaDescripcion.Text}}

        Dim settings As New Xml.XmlWriterSettings With {.ConformanceLevel = Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}

        Using writer As Xml.XmlWriter = Xml.XmlWriter.Create("BajaDocumento.xml", settings)
            Dim typeToSerialize As Type = GetType(DocumentoElectronicoComunicacionBaja)
            Dim xs As New Xml.Serialization.XmlSerializer(typeToSerialize)
            xs.Serialize(writer, Doc)
        End Using

        'Dim stringwriter As IO.StringWriter = New IO.StringWriter()
        'Dim x As Xml.Serialization.XmlSerializer = New Xml.Serialization.XmlSerializer(Doc.GetType())
        ''Convierte a XML y lo almacena en un StringWriter
        'x.Serialize(stringwriter, Doc)
        'Using f As New FrmVersionesXMLCliente_Visor
        '    f.VarText = stringwriter.ToString()
        '    f.ShowDialog()
        'End Using

        '--El envio
        Dim doce As New Xml.XmlDocument()
        doce.Load("BajaDocumento.xml")

        Dim PruebaXML__1 As New ws_documentoelectronico

        MessageBox.Show(PruebaXML__1.Documento_DarBaja(doce))
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim PruebaXML__1 As New ws_documentoelectronico
        Dim Rst As String = PruebaXML__1.Documento_Estado(Emisor_RUC, "01", Documento_Factura_Serie, TxtDocNumero.Text)
        If Rst.Split("#").Count > 1 Then
            'Encontró los el documento
            MessageBox.Show(Rst)
        Else
            MessageBox.Show(Rst)
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        'Nota de Credito
        Dim Doc As New DocumentoElectronico
        Doc.Fecha_Emision = CDate(TxtDocFechEmision.Text)
        Doc.Emisor_ApellidosNombres_RazonSocial = Emisor_RazonSocial
        'Doc.Emisor_NombreComercial = ""
        Doc.Emisor_Documento_Tipo = "6"
        Doc.Emisor_Documento_Numero = Emisor_RUC

        '--- Domicilio fiscal -- PostalAddress
        Doc.Emisor_Direccion_Ubigeo = Emisor_Ubigeo
        Doc.Emisor_Direccion_Calle = "Calle Nº 456"
        Doc.Emisor_Direccion_Urbanizacion = "Ubanizacion "
        Doc.Emisor_Direccion_Departamento = "ANCASH"
        Doc.Emisor_Direccion_Provincia = "HUARAZ"
        Doc.Emisor_Direccion_Distrito = "HUARAZ"
        Doc.Emisor_Direccion_PaisCodigo = "PE"

        Doc.TipoDocumento_Codigo = "07"
        Doc.TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA"
        Doc.TipoDocumento_Serie = Documento_NotaCredito_Serie
        Doc.TipoDocumento_Numero = TxtDocNumero.Text

        Doc.Cliente_Documento_Tipo = "6"
        Doc.Cliente_Documento_Numero = Cliente_Factura_RUC
        Doc.Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial
        Doc.Cliente_Direccion = "Calle Los Geranios 2367 – Urb. Matellini – Chorrillos - Lima"
        Doc.Cliente_Correo = "ti@grupoortiz.pe"




        Doc.Detalles = New DocumentoElectronicoDetalle() {New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "Cap-258963", .Producto_Descripcion = "<![CDATA[CAPTOPRIL 25mg X 30]]>", .Producto_UnidadMedida_Codigo = "ZZ", .Producto_Cantidad = 2, .Unitario_Precio_Venta = 180, .Unitario_Precio_Venta_Tipo_Codigo = "01", .Item_IGV_Total = 32.4, .Unitario_IGV_Porcentaje = 18, .Item_Tipo_Afectacion_IGV_Codigo = "10", .Item_ISC_Total = 0, .Unitario_Valor_Unitario = 147.6, .Item_ValorVenta_Total = 295.2}}

        Doc.Total_ValorVenta_OperacionesGravadas = 295.2
        Doc.Total_ValorVenta_OperacionesInafectas = 0
        Doc.Total_ValorVenta_OperacionesExoneradas = 0
        Doc.Total_ValorVenta_OperacionesGratuitas = 0
        Doc.Total_IGV = 64.8
        Doc.Total_ISC = 0
        Doc.Total_OtrosTributos = 0
        Doc.Total_OtrosCargos = 0
        'Doc.Descuentos_Globales = 0

        Doc.Total_Importe_Venta = 360
        Doc.Total_Importe_Venta_Texto = NroEnLetras(Math.Truncate(Doc.Total_Importe_Venta)) & " " & Doc.Moneda_Descripcion
        Doc.Moneda_Codigo = "PEN"
        Doc.Moneda_Descripcion = "SOLES"
        Doc.Moneda_Simbolo = "S/"

        Dim ListaFacturasAfectadas As New List(Of DocumentoElectronicoReferenciaDocumentos)
        ListaFacturasAfectadas.Add(New DocumentoElectronicoReferenciaDocumentos With {.Documento_Tipo = "01", .Documento_Serie = "F001", .Documento_Numero = "00000001", .Documento_Ref_Tipo = "07", .Documento_Ref_Observaciones = "Devolucion por item"})
        Doc.DocumentoAfectados = ListaFacturasAfectadas.ToArray
        'Doc.GuiasRemision = New DocumentoElectronicoReferenciaDocumentos() {New DocumentoElectronicoReferenciaDocumentos With {.Documento_Tipo = "09", .Documento_Serie = "0001", .Documento_Numero = "00000009"}}

        'Doc.Leyendas = New DocumentoElectronicoLeyenda() {New DocumentoElectronicoLeyenda With {.Codigo = "1000", .Descripcion = "DOSCIENTOS SOLES CON 00/100"}, New DocumentoElectronicoLeyenda With {.Codigo = "2000", .Descripcion = "COMPROBANTE DE PERCEPCION"}}
        Doc.Leyendas = New DocumentoElectronicoLeyenda() {New DocumentoElectronicoLeyenda With {.Codigo = "1000", .Descripcion = "DOSCIENTOS SOLES CON 00/100"}}

        Doc.Total_Importe_Percepcion = 0

        Dim settings As New Xml.XmlWriterSettings With {.ConformanceLevel = Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}

        Using writer As Xml.XmlWriter = Xml.XmlWriter.Create("DocFactura.xml", settings)
            Dim typeToSerialize As Type = GetType(DocumentoElectronico)
            Dim xs As New Xml.Serialization.XmlSerializer(typeToSerialize)
            xs.Serialize(writer, Doc)
        End Using

        Dim doc12 As New Xml.XmlDocument()
        doc12.Load("DocFactura.xml")

        Dim PruebaXML__1 As New ws_documentoelectronico
        Dim RptaWS As String = PruebaXML__1.Documento_RegistrarActualizar(doc12)
        'If RptaWS <> "0" Then
        MessageBox.Show(RptaWS)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        'Nota de Credito
        Dim Doc As New DocumentoElectronico
        Doc.Fecha_Emision = CDate(TxtDocFechEmision.Text)
        Doc.Emisor_ApellidosNombres_RazonSocial = Emisor_RazonSocial
        'Doc.Emisor_NombreComercial = ""
        Doc.Emisor_Documento_Tipo = "6"
        Doc.Emisor_Documento_Numero = Emisor_RUC

        '--- Domicilio fiscal -- PostalAddress
        Doc.Emisor_Direccion_Ubigeo = Emisor_Ubigeo
        Doc.Emisor_Direccion_Calle = "Calle Nº 456"
        Doc.Emisor_Direccion_Urbanizacion = "Ubanizacion "
        Doc.Emisor_Direccion_Departamento = "ANCASH"
        Doc.Emisor_Direccion_Provincia = "HUARAZ"
        Doc.Emisor_Direccion_Distrito = "HUARAZ"
        Doc.Emisor_Direccion_PaisCodigo = "PE"

        Doc.TipoDocumento_Codigo = "08"
        Doc.TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA"
        Doc.TipoDocumento_Serie = Documento_NotaDebito_Serie
        Doc.TipoDocumento_Numero = TxtDocNumero.Text

        Doc.Cliente_Documento_Tipo = "6"
        Doc.Cliente_Documento_Numero = Cliente_Factura_RUC
        Doc.Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial
        Doc.Cliente_Direccion = "Calle Los Geranios 2367 – Urb. Matellini – Chorrillos - Lima"
        'Doc.Cliente_Correo = "donny.hn@gmail.com"




        Doc.Detalles = New DocumentoElectronicoDetalle() {New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "Cap-258963", .Producto_Descripcion = "<![CDATA[CAPTOPRIL 25mg X 30]]>", .Producto_UnidadMedida_Codigo = "ZZ", .Producto_Cantidad = 2, .Unitario_Precio_Venta = 180, .Unitario_Precio_Venta_Tipo_Codigo = "01", .Item_IGV_Total = 32.4, .Unitario_IGV_Porcentaje = 18, .Item_Tipo_Afectacion_IGV_Codigo = "13", .Item_ISC_Total = 0, .Unitario_Valor_Unitario = 147.6, .Item_ValorVenta_Total = 295.2}}

        Doc.Total_ValorVenta_OperacionesGravadas = 295.2
        Doc.Total_ValorVenta_OperacionesInafectas = 0
        Doc.Total_ValorVenta_OperacionesExoneradas = 0
        Doc.Total_ValorVenta_OperacionesGratuitas = 0
        Doc.Total_IGV = 64.8
        Doc.Total_ISC = 0
        Doc.Total_OtrosTributos = 0
        Doc.Total_OtrosCargos = 0
        'Doc.Descuentos_Globales = 0

        Doc.Total_Importe_Venta = 360
        Doc.Total_Importe_Venta_Texto = NroEnLetras(Math.Truncate(Doc.Total_Importe_Venta)) & " " & Doc.Moneda_Descripcion
        Doc.Moneda_Codigo = "PEN"
        Doc.Moneda_Descripcion = "SOLES"
        Doc.Moneda_Simbolo = "S/"

        Dim ListaFacturasAfectadas As New List(Of DocumentoElectronicoReferenciaDocumentos)
        ListaFacturasAfectadas.Add(New DocumentoElectronicoReferenciaDocumentos With {.Documento_Tipo = "01", .Documento_Serie = "F001", .Documento_Numero = "00000001", .Documento_Ref_Tipo = "07", .Documento_Ref_Observaciones = "Devolucion por item"})
        Doc.DocumentoAfectados = ListaFacturasAfectadas.ToArray
        'Doc.GuiasRemision = New DocumentoElectronicoReferenciaDocumentos() {New DocumentoElectronicoReferenciaDocumentos With {.Documento_Tipo = "09", .Documento_Serie = "0001", .Documento_Numero = "00000009"}}

        'Doc.Leyendas = New DocumentoElectronicoLeyenda() {New DocumentoElectronicoLeyenda With {.Codigo = "1000", .Descripcion = "DOSCIENTOS SOLES CON 00/100"}, New DocumentoElectronicoLeyenda With {.Codigo = "2000", .Descripcion = "COMPROBANTE DE PERCEPCION"}}
        Doc.Leyendas = New DocumentoElectronicoLeyenda() {New DocumentoElectronicoLeyenda With {.Codigo = "1000", .Descripcion = "DOSCIENTOS SOLES CON 00/100"}}

        Doc.Total_Importe_Percepcion = 0

        Dim settings As New Xml.XmlWriterSettings With {.ConformanceLevel = Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}

        Using writer As Xml.XmlWriter = Xml.XmlWriter.Create("DocFactura.xml", settings)
            Dim typeToSerialize As Type = GetType(DocumentoElectronico)
            Dim xs As New Xml.Serialization.XmlSerializer(typeToSerialize)
            xs.Serialize(writer, Doc)
        End Using

        Dim doc12 As New Xml.XmlDocument()
        doc12.Load("DocFactura.xml")

        Dim PruebaXML__1 As New ws_documentoelectronico
        Dim RptaWS As String = PruebaXML__1.Documento_RegistrarActualizar(doc12)
        'If RptaWS <> "0" Then
        MessageBox.Show(RptaWS)
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim Doc As New DocumentoElectronicoComunicacionBaja
        Doc.Documentos = New DocumentoElectronico() {New DocumentoElectronico With {.Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = Emisor_RUC, .TipoDocumento_Codigo = "07", .TipoDocumento_Serie = Documento_NotaCredito_Serie, .TipoDocumento_Numero = TxtDocNumero.Text, .RazonAnulacion = TxtMotivoBajaDescripcion.Text}}

        Dim settings As New Xml.XmlWriterSettings With {.ConformanceLevel = Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}

        Using writer As Xml.XmlWriter = Xml.XmlWriter.Create("BajaDocumento.xml", settings)
            Dim typeToSerialize As Type = GetType(DocumentoElectronicoComunicacionBaja)
            Dim xs As New Xml.Serialization.XmlSerializer(typeToSerialize)
            xs.Serialize(writer, Doc)
        End Using

        Dim doce As New Xml.XmlDocument()
        doce.Load("BajaDocumento.xml")

        Dim PruebaXML__1 As New ws_documentoelectronico

        MessageBox.Show(PruebaXML__1.Documento_DarBaja(doce))
    End Sub

    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        Dim Doc As New DocumentoElectronicoComunicacionBaja
        Doc.Documentos = New DocumentoElectronico() {New DocumentoElectronico With {.Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = Emisor_RUC, .TipoDocumento_Codigo = "08", .TipoDocumento_Serie = Documento_NotaDebito_Serie, .TipoDocumento_Numero = TxtDocNumero.Text, .RazonAnulacion = TxtMotivoBajaDescripcion.Text}}

        Dim settings As New Xml.XmlWriterSettings With {.ConformanceLevel = Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}

        Using writer As Xml.XmlWriter = Xml.XmlWriter.Create("BajaDocumento.xml", settings)
            Dim typeToSerialize As Type = GetType(DocumentoElectronicoComunicacionBaja)
            Dim xs As New Xml.Serialization.XmlSerializer(typeToSerialize)
            xs.Serialize(writer, Doc)
        End Using

        Dim doce As New Xml.XmlDocument()
        doce.Load("BajaDocumento.xml")

        Dim PruebaXML__1 As New ws_documentoelectronico

        MessageBox.Show(PruebaXML__1.Documento_DarBaja(doce))
    End Sub

    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button19.Click
        SUNAT_Beta_GuiaRemision_Bill_Alta()
    End Sub
    Public Sub SUNAT_Beta_GuiaRemision_Bill_Alta()
        'Dim RptaSUNAT As New eSunatRespuesta
        Dim CDR_Name_Zip As String
        Dim CDR_Name_Zip_Archivo As String
        Using dLg As New OpenFileDialog
            If dLg.ShowDialog = Windows.Forms.DialogResult.OK Then
                CDR_Name_Zip_Archivo = dLg.FileName
                CDR_Name_Zip = Path.GetFileNameWithoutExtension(CDR_Name_Zip_Archivo) & "-CDR.ZIP"


                Dim DataByteArray As Byte() = System.IO.File.ReadAllBytes(CDR_Name_Zip_Archivo)
                CDR_Name_Zip_Archivo = Path.GetFileNameWithoutExtension(CDR_Name_Zip_Archivo) & ".ZIP"

                System.Net.ServicePointManager.UseNagleAlgorithm = True
                System.Net.ServicePointManager.Expect100Continue = False
                System.Net.ServicePointManager.CheckCertificateRevocationList = True

                Dim binding As New ServiceModel.BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential)
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None
                binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None
                binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName
                binding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Default
                'Dim remoteAddress As New EndpointAddress("https://e-beta.sunat.gob.pe/ol-ti-itcpfegem-beta/billService")
                Dim remoteAddress As New EndpointAddress("https://e-beta.sunat.gob.pe/ol-ti-itemision-guia-gem-beta/billService") 'https://e-beta.sunat.gob.pe/ol-ti-itemision-guia-gem-beta/billService?wsdl
                Dim client As New SUNAT_WS.billServiceClient(binding, remoteAddress)
                client.ClientCredentials.UserName.UserName = "20131312955MODDATOS"
                client.ClientCredentials.UserName.Password = "moddatos"
                Dim bindingElementsInTopDownChannelStackOrder As BindingElementCollection = client.Endpoint.Binding.CreateBindingElements
                bindingElementsInTopDownChannelStackOrder.Find(Of SecurityBindingElement).EnableUnsecuredResponse = True
                client.Endpoint.Binding = New CustomBinding(bindingElementsInTopDownChannelStackOrder)

                'System.Net.ServicePointManager.ServerCertificateValidationCallback = New System.Net.Security.RemoteCertificateValidationCallback(AddressOf Devolcer)

                Dim RetornoCDR_Byte As Byte()

                Try
                    RetornoCDR_Byte = client.sendBill(CDR_Name_Zip_Archivo, DataByteArray)
                Catch exception As FaultException
                    Dim str10 As String = exception.Code.Name.ToUpper.Replace("SOAP-ENV-SERVER.", "").Replace("SOAP-ENV-SERVER", "").Replace("CLIENT.", "")
                    Dim str11 As String = ("FaultException Validación: " & exception.Message) '151'2335
                    'RptaSUNAT.Codigo = exception.Message ' str10
                    'RptaSUNAT.Mensaje = str11

                    'Log.Instancia.escribirLinea(Log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL", RptaSUNAT.Codigo & "|" & RptaSUNAT.Mensaje)

                    'Return RptaSUNAT
                Catch exception3 As Exception
                    'RptaSUNAT.Codigo = "0"
                    'RptaSUNAT.Mensaje = "El archivo XML ha sido generado, pero aún no ha sido válidado con la SUNAT debido a que no hay conexión con la URL: https://www.sunat.gob.pe:443/ol-ti-itcpgem-beta/billService"

                    'Log.Instancia.escribirLinea(Log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL", RptaSUNAT.Codigo & "|" & RptaSUNAT.Mensaje)

                    'Return RptaSUNAT
                End Try

                If (Not RetornoCDR_Byte Is Nothing) Then
                    Try
                        File.WriteAllBytes(CDR_Name_Zip, RetornoCDR_Byte)
                        'If File.Exists((nProceso.ServidorRuta_Temporal & CDR_Name_Xml)) Then
                        '    File.Delete((nProceso.ServidorRuta_Temporal & CDR_Name_Xml))
                        'End If
                        Compression.ZipFile.ExtractToDirectory(CDR_Name_Zip, "D:\BCK")
                        'Dim document As New XmlDocument
                        'document.Load((nProceso.ServidorRuta_Temporal & CDR_Name_Xml))
                        'Dim childNodes As XmlNodeList = document.GetElementsByTagName("DocumentResponse", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2").Item(0).FirstChild.ChildNodes
                        'RptaSUNAT.Codigo = childNodes.ItemOf(1).InnerText
                        'RptaSUNAT.Mensaje = childNodes.ItemOf(2).InnerText
                    Catch ex As Exception
                        'Sospecho disco lleno
                        'Log.Instancia.escribirLinea(Log.tipoLogEnum.Fatal_, "BETABILL-FACTBOL-COMP", ex.Message)
                    End Try

                End If

                'If (Convert.ToInt32(RptaSUNAT.Codigo) = 0) Then
                '    RptaSUNAT.Estado = EstadoDocumento.Aceptado
                '    'Log.Instancia.escribirLinea(Log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "ACEPTADO|" & nProceso.FileNameXML)
                'ElseIf ((Convert.ToInt32(RptaSUNAT.Codigo) >= 1) AndAlso (Convert.ToInt32(RptaSUNAT.Codigo) <= 199)) Then
                '    RptaSUNAT.Estado = EstadoDocumento.Invalido
                '    'Log.Instancia.escribirLinea(Log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "INVALIDO|" & nProceso.FileNameXML)
                'ElseIf (Convert.ToInt32(RptaSUNAT.Codigo) >= 200) Then
                '    RptaSUNAT.Estado = EstadoDocumento.Rechazado
                '    'Log.Instancia.escribirLinea(Log.tipoLogEnum.Fatal_, "BETABILLFACTBOL", "RECHAZADO|" & nProceso.FileNameXML)
                'End If

                'Return RptaSUNAT

            End If
        End Using

    End Sub

    Private Sub Button16_Click(sender As Object, e As EventArgs) Handles Button16.Click

    End Sub

    Private Sub Button15_Click(sender As Object, e As EventArgs) Handles Button15.Click

    End Sub

    Private Sub Button17_Click(sender As Object, e As EventArgs) Handles Button17.Click

    End Sub

    Private Sub Button18_Click(sender As Object, e As EventArgs) Handles Button18.Click

    End Sub

    Private Sub Button20_Click(sender As Object, e As EventArgs) Handles Button20.Click
        'GENERAR XML
        Dim Doc As New DocumentoElectronico
        Doc.Fecha_Emision = CDate(TxtDocFechEmision.Text)
        Doc.Emisor_ApellidosNombres_RazonSocial = Emisor_RazonSocial
        'Doc.Emisor_NombreComercial = ""
        Doc.Emisor_Documento_Tipo = "6"
        Doc.Emisor_Documento_Numero = Emisor_RUC

        '--- Domicilio fiscal -- PostalAddress
        Doc.Emisor_Direccion_Ubigeo = Emisor_Ubigeo
        Doc.Emisor_Direccion_Calle = "Calle Nº 456"
        Doc.Emisor_Direccion_Urbanizacion = "Ubanizacion "
        Doc.Emisor_Direccion_Departamento = "ANCASH"
        Doc.Emisor_Direccion_Provincia = "HUARAZ"
        Doc.Emisor_Direccion_Distrito = "HUARAZ"
        Doc.Emisor_Direccion_PaisCodigo = "PE"

        Doc.TipoDocumento_Codigo = "01"

        Doc.TipoDocumento_Descripcion = "FACTURA ELECTRONICA"
        Doc.Cliente_Documento_Tipo = "6"
        Doc.Cliente_Documento_Numero = Cliente_Factura_RUC
        Doc.Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial
        Doc.Cliente_Direccion = "Calle Los Geranios 2367 – Urb. Matellini – Chorrillos - Lima"
        Doc.Cliente_Correo = "donny.hn@gmail.com"

        Doc.TipoDocumento_Serie = Documento_Factura_Serie
        Doc.TipoDocumento_Numero = TxtDocNumero.Text


        Doc.Detalles = New DocumentoElectronicoDetalle() {New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "Cap-258963", .Producto_Descripcion = "CAPTOPRIL 25mg X 30", .Producto_UnidadMedida_Codigo = "ZZ", .Producto_Cantidad = 2, .Unitario_Precio_Venta = 180, .Unitario_Precio_Venta_Tipo_Codigo = "01", .Item_IGV_Total = 32.4, .Unitario_IGV_Porcentaje = 18, .Item_Tipo_Afectacion_IGV_Codigo = "13", .Item_ISC_Total = 0, .Unitario_Valor_Unitario = 147.6, .Item_ValorVenta_Total = 295.2}}

        Doc.Total_ValorVenta_OperacionesGravadas = 295.2
        Doc.Total_ValorVenta_OperacionesInafectas = 0
        Doc.Total_ValorVenta_OperacionesExoneradas = 0
        Doc.Total_ValorVenta_OperacionesGratuitas = 0
        Doc.Total_IGV = 64.8
        Doc.Total_ISC = 0
        Doc.Total_OtrosTributos = 0
        Doc.Total_OtrosCargos = 0
        'Doc.Descuentos_Globales = 0

        Doc.Total_Importe_Venta = 360

        Doc.Moneda_Codigo = "PEN"
        Doc.Moneda_Descripcion = "SOLES"
        Doc.Moneda_Simbolo = "S/"

        Doc.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc.Total_Importe_Venta)) & " " & Doc.Moneda_Descripcion

        Doc.Total_Importe_Percepcion = 0

        Doc.Impresion_Modelo_Codigo = "001"

        'Venta itinerante
        'Doc.Tipo_Operacion_Codigo = "05" 'Cat 17 --05 Venta itinerante, 06 factura guia, 08 factura comprobante percepcion , Venta interna, exportacion, no domiciliados, etc

        'Doc.Adic_Lugar_Entrega_Bien_PrestaServicio_Ubigeo = Emisor_Ubigeo
        'Doc.Adic_Lugar_Entrega_Bien_PrestaServicio_Calle = "Calle Nº 456"
        'Doc.Adic_Lugar_Entrega_Bien_PrestaServicio_Urbanizacion = "Ubanizacion "
        'Doc.Adic_Lugar_Entrega_Bien_PrestaServicio_Departamento = "ANCASH"
        'Doc.Adic_Lugar_Entrega_Bien_PrestaServicio_Provincia = "HUARAZ"
        'Doc.Adic_Lugar_Entrega_Bien_PrestaServicio_Distrito = "HUARAZ"
        'Doc.Adic_Lugar_Entrega_Bien_PrestaServicio_PaisCodigo = "PE"

        Doc.DocumentoRelacionados = New DocumentoElectronicoReferenciaDocumentos() {New DocumentoElectronicoReferenciaDocumentos With {.Documento_Tipo = "09", .Documento_Serie = "0001", .Documento_Numero = "00000009"}}

        '   45 Leyendas
        Dim ListaLeyendas As New List(Of DocumentoElectronicoLeyenda)
        ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "1000", .Descripcion = NroEnLetras(Math.Truncate(Doc.Total_Importe_Venta)) & " " & Doc.Moneda_Descripcion})
        If Doc.Total_Importe_Venta = 0 Then
            ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "1002", .Descripcion = "TRANSFERENCIA GRATUITA DE UN BIEN Y/O SERVICIO PRESTADO GRATUITAMENTE"})
        End If
        If Doc.Total_Importe_Percepcion > 0 Then
            ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "2000", .Descripcion = "COMPROBANTE DE PERCEPCION"})
        End If
        If Not String.IsNullOrEmpty(Doc.Tipo_Operacion_Codigo) Then
            If Doc.Tipo_Operacion_Codigo = "05" Then
                ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "2005", .Descripcion = "VENTA REALIZADA POR EMISOR ITINERANTE"})
            End If
        End If
        Doc.Leyendas = ListaLeyendas.ToArray

        'Numero de placa
        'Doc.Numero_Placa_del_Vehiculo = "CFD-654"

        'Factura Guía
        'Doc.GuiaFactura_Documento_Tipo_Codigo = "09"
        'Doc.GuiaFactura_Documento_Serie = "T001"
        'Doc.GuiaFactura_Documento_Numero = "00000001"
        'Doc.GuiaFactura_RemitenteDocumentoOriginal_NumDoc = "1042171355"


        'Doc.MotivoTraslad_Codigo = "01" 'Catalogo 20
        'Doc.MotivoTraslad_Detalle = "VENTA"

        'Doc.PesoBrutoTotalGuia_UnidMed = "KGM" 'CAT 03
        'Doc.PesoBrutoTotalGuia_Valor = 2 '

        'Doc.ModalidadTraslado_Codigo = "02" 'Cat 18 :--01:Tranporte publico; 02:Transporte Privado

        'Doc.Fecha_Inicio_Traslado_o_Entrega_Bienes_Al_Transportista = Doc.Fecha_Emision

        ''--Transportista (Transporte Público)
        ''Doc.Transportista_Documento_Tipo = "6" 'cat 06
        ''Doc.Transportista_Documento_Numero = "10421713559"
        ''Doc.Transportista_RazonSocial_Nombre = "Transporte publico sac"
        'Doc.Transportista_Documento_Tipo = Nothing 'cat 06
        'Doc.Transportista_Documento_Numero = Nothing
        'Doc.Transportista_RazonSocial_Nombre = Nothing

        ''--VEHICULO (Transporte Privado)
        'Dim ListaTransportes As New List(Of DocumentoElectronicoTransporte)
        'ListaTransportes.Add(New DocumentoElectronicoTransporte With {.Placa = "PLACA-001", .NumeroInscripcion_CertificadoHabilitacionVehicular = "gdshdg"})
        'Doc.Transportes = ListaTransportes.ToArray
        ''--CONDUCTOR (Transporte Privado)
        'Doc.Conductor_Documento_Tipo = "1"
        'Doc.Conductor_Documento_Numero = "42171355" 'CAT 06
        ''--Direccion punto de llegada
        'Doc.Llegada_Ubigeo = "020101"

        'Doc.Llegada_Direccion = "AV. LOS PROCERES NO 234" '185 -2015
        ''Doc.Llegada_Urbanizacion = "Ubanizacion 1" '185 -2015
        ''Doc.Llegada_Departamento = "ANCASH" '185 -2015
        ''Doc.Llegada_Provincia = "HUARAZ" '185 -2015
        ''Doc.Llegada_Distrito = "HUARAZ" '185 -2015
        ''Doc.Llegada_Pais = "PE" '185 -2015

        ''--Datos del contenedor (Motivo Importación)

        ''--Direccion del punto de partida
        'Doc.Salida_Ubigeo = "020101"
        'Doc.Salida_Direccion = "Av. Los Gallinazos NO 345"
        'Doc.Salida_Direccion = "AV. LOS PROCERES NO 234" '185 -2015
        ''Doc.Salida_Urbanizacion = "Ubanizacion 2" '185 -2015
        ''Doc.Salida_Departamento = "ANCASH" '185 -2015
        ''Doc.Salida_Provincia = "HUARAZ" '185 -2015
        ''Doc.Salida_Distrito = "HUARAZ" '185 -2015
        ''Doc.Salida_Pais = "PE" '185 -2015


        Dim settings As New Xml.XmlWriterSettings With {.ConformanceLevel = Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}

        Using writer As Xml.XmlWriter = Xml.XmlWriter.Create("DocFactura.xml", settings)
            Dim typeToSerialize As Type = GetType(DocumentoElectronico)
            Dim xs As New Xml.Serialization.XmlSerializer(typeToSerialize)
            xs.Serialize(writer, Doc)
        End Using

        Dim doc12 As New Xml.XmlDocument()
        doc12.Load("DocFactura.xml")

        Dim PruebaXML__1 As New ws_documentoelectronico
        Dim RptaWS As String = PruebaXML__1.Documento_RegistrarActualizar(doc12)
        'If RptaWS <> "0" Then
        MessageBox.Show(RptaWS)
    End Sub
    Public Function NroEnLetras(ByVal curNumero As Double, Optional ByVal blnO_Final As Boolean = True) As String
        Dim dblCentavos As Double
        Dim lngContUnit As Long
        Dim lngContDec As Long
        Dim lngContCent As Long
        Dim lngContMil As Long
        Dim lngContMillon As Long
        Dim strNumLetras As String = ""
        Dim strNumero(20) As String
        Dim strDecenas(10) As String
        Dim strCentenas(9) As String
        Dim blnNegativo As Boolean
        Dim blnPlural As Boolean


        If Int(curNumero) = 0.0# Then
            strNumLetras = "CERO"
        End If

        strNumero(0) = ""
        strNumero(1) = "UN"
        strNumero(2) = "DOS"
        strNumero(3) = "TRES"
        strNumero(4) = "CUATRO"
        strNumero(5) = "CINCO"
        strNumero(6) = "SEIS"
        strNumero(7) = "SIETE"
        strNumero(8) = "OCHO"
        strNumero(9) = "NUEVE"
        strNumero(10) = "DIEZ"
        strNumero(11) = "ONCE"
        strNumero(12) = "DOCE"
        strNumero(13) = "TRECE"
        strNumero(14) = "CATORCE"
        strNumero(15) = "QUINCE"
        strNumero(16) = "DIECISEIS"
        strNumero(17) = "DIECISIETE"
        strNumero(18) = "DIECIOCHO"
        strNumero(19) = "DIECINUEVE"
        strNumero(20) = "VEINTE"

        strDecenas(0) = ""
        strDecenas(1) = ""
        strDecenas(2) = "VEINTI"
        strDecenas(3) = "TREINTA"
        strDecenas(4) = "CUARENTA"
        strDecenas(5) = "CINCUENTA"
        strDecenas(6) = "SESENTA"
        strDecenas(7) = "SETENTA"
        strDecenas(8) = "OCHENTA"
        strDecenas(9) = "NOVENTA"
        strDecenas(10) = "CIEN"

        strCentenas(0) = ""
        strCentenas(1) = "CIENTO"
        strCentenas(2) = "DOSCIENTOS"
        strCentenas(3) = "TRESCIENTOS"
        strCentenas(4) = "CUATROCIENTOS"
        strCentenas(5) = "QUINIENTOS"
        strCentenas(6) = "SEISCIENTOS"
        strCentenas(7) = "SETECIENTOS"
        strCentenas(8) = "OCHOCIENTOS"
        strCentenas(9) = "NOVECIENTOS"

        If curNumero < 0.0# Then
            blnNegativo = True
            curNumero = Math.Abs(curNumero)
        End If

        If Int(curNumero) <> curNumero Then
            dblCentavos = Math.Abs(curNumero - Int(curNumero))
            curNumero = Int(curNumero)
        End If

        Do While curNumero >= 1000000.0#
            lngContMillon = lngContMillon + 1
            curNumero = curNumero - 1000000.0#
        Loop

        Do While curNumero >= 1000.0#
            lngContMil = lngContMil + 1
            curNumero = curNumero - 1000.0#
        Loop

        Do While curNumero >= 100.0#
            lngContCent = lngContCent + 1
            curNumero = curNumero - 100.0#
        Loop


        If Not (curNumero > 10.0# And curNumero <= 20.0#) Then
            Do While curNumero >= 10.0#
                lngContDec = lngContDec + 1
                curNumero = curNumero - 10.0#
            Loop
        End If


        Do While curNumero >= 1.0#
            lngContUnit = lngContUnit + 1
            curNumero = curNumero - 1.0#
        Loop

        If lngContMillon > 0 Then
            If lngContMillon >= 1 Then   'si el número es >1000000 usa recursividad
                strNumLetras = NroEnLetras(lngContMillon, False)
                If Not blnPlural Then blnPlural = (lngContMillon > 1)
                lngContMillon = 0
            End If
            strNumLetras = Trim(strNumLetras) & strNumero(lngContMillon) & " MILLON" & IIf(blnPlural, "ES ", " ")
        End If

        If lngContMil > 0 Then
            If lngContMil >= 1 Then   'si el número es >100000 usa recursividad
                strNumLetras = strNumLetras & NroEnLetras(lngContMil, False)
                lngContMil = 0
            End If
            strNumLetras = Trim(strNumLetras) & strNumero(lngContMil) & " MIL "
        End If

        If lngContCent > 0 Then
            If lngContCent = 1 And lngContDec = 0 And lngContUnit = 0.0# Then
                strNumLetras = strNumLetras & "CIEN"
            Else
                strNumLetras = strNumLetras & strCentenas(lngContCent) & " "
            End If
        End If

        If lngContDec >= 1 Then
            If lngContDec = 1 Then
                strNumLetras = strNumLetras & strNumero(10)
            ElseIf lngContDec = 2 And lngContUnit = 0.0# Then
                strNumLetras = strNumLetras & strNumero(20)
            Else
                strNumLetras = strNumLetras & strDecenas(lngContDec)
            End If
            If lngContDec >= 3 And lngContUnit > 0.0# Then
                strNumLetras = strNumLetras & " Y "
            End If
        End If

        If lngContUnit >= 0 Then
            If Not blnO_Final Then
                If lngContUnit >= 0.0# And lngContUnit <= 20.0# Then
                    strNumLetras = strNumLetras & strNumero(lngContUnit)
                    If lngContUnit = 1.0# And blnO_Final Then
                        strNumLetras = strNumLetras & "O"
                    End If
                    NroEnLetras = strNumLetras
                    Exit Function
                End If
            Else
                If lngContUnit >= 0.0# And lngContUnit <= 20.0# Then
                    strNumLetras = strNumLetras & strNumero(lngContUnit)
                    If lngContUnit = 1.0# And blnO_Final Then
                        strNumLetras = strNumLetras & "O"
                    End If
                    If dblCentavos >= 0.0# Then
                        strNumLetras = strNumLetras & " CON " + Format$(CInt(dblCentavos * 100.0#), "00") & "/100"
                    End If
                    NroEnLetras = strNumLetras
                    Exit Function
                End If
            End If
        End If
        If lngContUnit >= 0.0# Then
            If lngContUnit > 0 Then
                strNumLetras = strNumLetras & NroEnLetras(lngContUnit, True)
            End If
        End If


        NroEnLetras = IIf(blnNegativo, "(" & strNumLetras & ")", strNumLetras)
    End Function

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        'GENERAR XML
        Dim Doc As New DocumentoElectronico
        Doc.Fecha_Emision = Now.ToShortDateString
        Doc.Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text
        Doc.Emisor_NombreComercial = ""
        Doc.Emisor_Documento_Tipo = "6"
        Doc.Emisor_Documento_Numero = "20542134926" ' TxtEmisorRUC.Text

        '--- Domicilio fiscal -- PostalAddress
        Doc.Emisor_Direccion_Ubigeo = "020101"
        Doc.Emisor_Direccion_Calle = "JR. JULIAN DE MORALES NRO. 793"
        Doc.Emisor_Direccion_Urbanizacion = "CENTRO ZONA COMERCIAL"
        Doc.Emisor_Direccion_Departamento = "ANCASH"
        Doc.Emisor_Direccion_Provincia = "HUARAZ"
        Doc.Emisor_Direccion_Distrito = "HUARAZ"
        Doc.Emisor_Direccion_PaisCodigo = "PE"

        Doc.TipoDocumento_Codigo = "09"
        Doc.TipoDocumento_Descripcion = "GUIA DE REMISION ELECTRONICO - REMITENTE"
        Doc.TipoDocumento_Serie = "T001" 'TextBox1.Text
        Doc.TipoDocumento_Numero = TxtDocNumero.Text
        Doc.Observaciones = "OC001 00000289"
        'Doc.Cliente_Documento_Tipo = "1"
        'Doc.Cliente_Documento_Numero = "42171355" '"10421713559"

        Doc.Cliente_Documento_Tipo = "6"
        Doc.Cliente_Documento_Numero = "20100070970"
        Doc.Cliente_RazonSocial_Nombre = "PLAZA VEA"
        Doc.Cliente_Direccion = "Calle Los Geranios 2367 – Urb. Matellini – Chorrillos - Lima"

        Doc.MotivoTraslad_Codigo = "01" 'Catalogo 20
        Doc.MotivoTraslad_Detalle = "VENTA"

        Doc.IndicTransProg_Codigo = "00" 'SplitConsignmentIndicator INDICADOR DE TRANSBORDO PROGRAMADO 00 falso, 01 verdadero

        Doc.PesoBrutoTotalGuia_UnidMed = "KGM" 'CAT 03
        Doc.PesoBrutoTotalGuia_Valor = 2 '

        Doc.ModalidadTraslado_Codigo = "02" 'Cat 18 :--01:Tranporte publico; 02:Transporte Privado
        Doc.ModalidadTraslado_Descripcion = "Transporte Privado"

        Doc.Fecha_Inicio_Traslado_o_Entrega_Bienes_Al_Transportista = Doc.Fecha_Emision

        Doc.NumeroBultosoPallets = 1 'nUMERO DE BULTOS

        Doc.Contenedor_Numero = "CONTENEDOR"

        '--Transportista (Transporte Público)
        'Doc.Transportista_Documento_Tipo = "6" 'cat 06
        'Doc.Transportista_Documento_Numero = "10421713559"
        'Doc.Transportista_RazonSocial_Nombre = "Transporte publico sac"
        Doc.Transportista_Documento_Tipo = Nothing 'cat 06
        Doc.Transportista_Documento_Numero = Nothing
        Doc.Transportista_RazonSocial_Nombre = Nothing

        '--VEHICULO (Transporte Privado)
        Dim ListaTransportes As New List(Of DocumentoElectronicoTransporte)
        ListaTransportes.Add(New DocumentoElectronicoTransporte With {.Placa = "PGY-0988"})
        ListaTransportes.Add(New DocumentoElectronicoTransporte With {.Placa = "Contenedor"})
        Doc.Transportes = ListaTransportes.ToArray
        '--CONDUCTOR (Transporte Privado)
        Doc.Conductor_Documento_Tipo = "1"
        Doc.Conductor_Documento_Numero = "42171355" 'CAT 06
        '--Direccion punto de llegada
        Doc.Llegada_Ubigeo = "020101"
        Doc.Llegada_Direccion = "AV. LOS PROCERES NO 234"
        '--Datos del contenedor (Motivo Importación)

        '--Direccion del punto de partida
        Doc.Salida_Ubigeo = "020101"
        Doc.Salida_Direccion = "Av. Los Gallinazos NO 345"
        '--Puerto o Aeropuerto de embarque/desembarque
        'Doc.PuertoAeropuerto_EmbarqueDesembarque_Codigo = "PAI"
        Doc.PuertoAeropuerto_EmbarqueDesembarque_Codigo = Nothing
        '--BIENES A TRANSPORTAR
        Doc.Detalles = New DocumentoElectronicoDetalle() {New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "Cap-258963", .Producto_Descripcion = "CAPTOPRIL 25mg X 30", .Producto_UnidadMedida_Codigo = "KGM", .Producto_Cantidad = 2}}

        Dim settings As New Xml.XmlWriterSettings With {.ConformanceLevel = Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}

        Using writer As Xml.XmlWriter = Xml.XmlWriter.Create("DocFactura.xml", settings)
            Dim typeToSerialize As Type = GetType(DocumentoElectronico)
            Dim xs As New Xml.Serialization.XmlSerializer(typeToSerialize)
            xs.Serialize(writer, Doc)
        End Using

        Dim doc12 As New Xml.XmlDocument()
        doc12.Load("DocFactura.xml")

        Dim PruebaXML__1 As New ws_documentoelectronico
        Dim RptaWS As String = PruebaXML__1.Documento_RegistrarActualizar_Enviar(doc12)
        'If RptaWS <> "0" Then
        MessageBox.Show(RptaWS)
    End Sub

    Private Sub Button21_Click(sender As Object, e As EventArgs) Handles Button21.Click
        'GENERAR XML
        Dim Doc As New DocumentoElectronico
        Doc.Fecha_Emision = Now.ToShortDateString
        Doc.Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text
        Doc.Emisor_NombreComercial = ""
        Doc.Emisor_Documento_Tipo = "6"
        Doc.Emisor_Documento_Numero = TxtEmisorRUC.Text

        '--- Domicilio fiscal -- PostalAddress
        Doc.Emisor_Direccion_Ubigeo = "020101"
        Doc.Emisor_Direccion_Calle = "JR. JULIAN DE MORALES NRO. 793"
        Doc.Emisor_Direccion_Urbanizacion = "CENTRO ZONA COMERCIAL"
        Doc.Emisor_Direccion_Departamento = "ANCASH"
        Doc.Emisor_Direccion_Provincia = "HUARAZ"
        Doc.Emisor_Direccion_Distrito = "HUARAZ"
        Doc.Emisor_Direccion_PaisCodigo = "PE"

        Doc.TipoDocumento_Codigo = "09"
        Doc.TipoDocumento_Descripcion = "GUIA DE REMISION ELECTRONICO - REMITENTE"
        Doc.TipoDocumento_Serie = "T001" 'TextBox1.Text
        Doc.TipoDocumento_Numero = TxtDocNumero.Text
        Doc.Observaciones = "OC001 00000289"
        'Doc.Cliente_Documento_Tipo = "1"
        'Doc.Cliente_Documento_Numero = "42171355" '"10421713559"

        Doc.Cliente_Documento_Tipo = "6"
        Doc.Cliente_Documento_Numero = "20100070970"
        Doc.Cliente_RazonSocial_Nombre = "PLAZA VEA"
        Doc.Cliente_Direccion = "Calle Los Geranios 2367 – Urb. Matellini – Chorrillos - Lima"

        Doc.MotivoTraslad_Codigo = "01" 'Catalogo 20
        Doc.MotivoTraslad_Detalle = "VENTA"

        Doc.IndicTransProg_Codigo = "00" 'SplitConsignmentIndicator INDICADOR DE TRANSBORDO PROGRAMADO 00 falso, 01 verdadero

        Doc.PesoBrutoTotalGuia_UnidMed = "KGM" 'CAT 03
        Doc.PesoBrutoTotalGuia_Valor = 2 '

        Doc.ModalidadTraslado_Codigo = "01" 'Cat 18 :--01:Tranporte publico; 02:Transporte Privado
        Doc.ModalidadTraslado_Descripcion = "Transporte Público"

        Doc.Fecha_Inicio_Traslado_o_Entrega_Bienes_Al_Transportista = Doc.Fecha_Emision

        Doc.NumeroBultosoPallets = 1 'nUMERO DE BULTOS

        Doc.Contenedor_Numero = "CONTENEDOR"

        '--Transportista (Transporte Público)
        Doc.Transportista_Documento_Tipo = "6" 'cat 06
        Doc.Transportista_Documento_Numero = "10421713559"
        Doc.Transportista_RazonSocial_Nombre = "Transporte publico sac"
        'Doc.Transportista_Documento_Tipo = Nothing 'cat 06
        'Doc.Transportista_Documento_Numero = Nothing
        'Doc.Transportista_RazonSocial_Nombre = Nothing

        '--VEHICULO (Transporte Privado)
        'Dim ListaTransportes As New List(Of DocumentoElectronicoTransporte)
        'ListaTransportes.Add(New DocumentoElectronicoTransporte With {.Placa = "PGY-0988"})
        'ListaTransportes.Add(New DocumentoElectronicoTransporte With {.Placa = "Contenedor"})
        'Doc.Transportes = ListaTransportes.ToArray
        '--CONDUCTOR (Transporte Privado)
        'Doc.Conductor_Documento_Tipo = "1"
        'Doc.Conductor_Documento_Numero = "42171355" 'CAT 06
        Doc.Conductor_Documento_Tipo = Nothing
        Doc.Conductor_Documento_Numero = Nothing
        '--Direccion punto de llegada
        Doc.Llegada_Ubigeo = "020101"
        Doc.Llegada_Direccion = "AV. LOS PROCERES NO 234"
        '--Datos del contenedor (Motivo Importación)

        '--Direccion del punto de partida
        Doc.Salida_Ubigeo = "020101"
        Doc.Salida_Direccion = "Av. Los Gallinazos NO 345"
        '--Puerto o Aeropuerto de embarque/desembarque
        'Doc.PuertoAeropuerto_EmbarqueDesembarque_Codigo = "PAI"
        Doc.PuertoAeropuerto_EmbarqueDesembarque_Codigo = Nothing
        '--BIENES A TRANSPORTAR
        Doc.Detalles = New DocumentoElectronicoDetalle() {New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "Cap-258963", .Producto_Descripcion = "CAPTOPRIL 25mg X 30", .Producto_UnidadMedida_Codigo = "KGM", .Producto_Cantidad = 2}}

        Dim settings As New Xml.XmlWriterSettings With {.ConformanceLevel = Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}

        Using writer As Xml.XmlWriter = Xml.XmlWriter.Create("DocFactura.xml", settings)
            Dim typeToSerialize As Type = GetType(DocumentoElectronico)
            Dim xs As New Xml.Serialization.XmlSerializer(typeToSerialize)
            xs.Serialize(writer, Doc)
        End Using

        Dim doc12 As New Xml.XmlDocument()
        doc12.Load("DocFactura.xml")

        Dim PruebaXML__1 As New ws_documentoelectronico
        Dim RptaWS As String = PruebaXML__1.Documento_RegistrarActualizar(doc12)
        'If RptaWS <> "0" Then
        MessageBox.Show(RptaWS)
    End Sub

    Private Sub Button22_Click(sender As Object, e As EventArgs) Handles Button22.Click
        'GENERAR XML
        Dim Doc As New DocumentoElectronico
        Doc.Fecha_Emision = CDate(TxtDocFechEmision.Text)
        Doc.Emisor_ApellidosNombres_RazonSocial = Emisor_RazonSocial
        'Doc.Emisor_NombreComercial = ""
        Doc.Emisor_Documento_Tipo = "6"
        Doc.Emisor_Documento_Numero = Emisor_RUC

        '--- Domicilio fiscal -- PostalAddress
        Doc.Emisor_Direccion_Ubigeo = Emisor_Ubigeo
        Doc.Emisor_Direccion_Calle = "Calle Nº 456"
        Doc.Emisor_Direccion_Urbanizacion = "Ubanizacion "
        Doc.Emisor_Direccion_Departamento = "ANCASH"
        Doc.Emisor_Direccion_Provincia = "HUARAZ"
        Doc.Emisor_Direccion_Distrito = "HUARAZ"
        Doc.Emisor_Direccion_PaisCodigo = "PE"

        Doc.TipoDocumento_Codigo = "01"

        Doc.TipoDocumento_Descripcion = "FACTURA ELECTRONICA"
        Doc.Cliente_Documento_Tipo = "6"
        Doc.Cliente_Documento_Numero = Cliente_Factura_RUC
        Doc.Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial
        Doc.Cliente_Direccion = "Calle Los Geranios 2367 – Urb. Matellini – Chorrillos - Lima"
        Doc.Cliente_Correo = "donny.hn@gmail.com"

        Doc.TipoDocumento_Serie = Documento_Factura_Serie
        Doc.TipoDocumento_Numero = TxtDocNumero.Text


        Doc.Detalles = New DocumentoElectronicoDetalle() {New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "Cap-258963", .Producto_Descripcion = "CAPTOPRIL 25mg X 30", .Producto_UnidadMedida_Codigo = "ZZ", .Producto_Cantidad = 2, .Unitario_Precio_Venta = 180, .Unitario_Precio_Venta_Tipo_Codigo = "01", .Item_IGV_Total = 32.4, .Unitario_IGV_Porcentaje = 18, .Item_Tipo_Afectacion_IGV_Codigo = "13", .Item_ISC_Total = 0, .Unitario_Valor_Unitario = 147.6, .Item_ValorVenta_Total = 295.2}}

        Doc.Total_ValorVenta_OperacionesGravadas = 295.2
        Doc.Total_ValorVenta_OperacionesInafectas = 0
        Doc.Total_ValorVenta_OperacionesExoneradas = 0
        Doc.Total_ValorVenta_OperacionesGratuitas = 0
        Doc.Total_IGV = 64.8
        Doc.Total_ISC = 0
        Doc.Total_OtrosTributos = 0
        Doc.Total_OtrosCargos = 0
        'Doc.Descuentos_Globales = 0

        Doc.Total_Importe_Venta = 360

        Doc.Moneda_Codigo = "PEN"
        Doc.Moneda_Descripcion = "SOLES"
        Doc.Moneda_Simbolo = "S/"

        Doc.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc.Total_Importe_Venta)) & " " & Doc.Moneda_Descripcion

        Doc.Impresion_Modelo_Codigo = "001"

        'Venta percepcion
        Doc.Tipo_Operacion_Codigo = "08" 'Cat 17 --05 Venta itinerante, 06 factura guia, 08 factura comprobante percepcion , Venta interna, exportacion, no domiciliados, etc
        Doc.Total_Importe_Percepcion = 3.6
        Doc.Percep_Importe_Total_Cobrado = 363.6

        Doc.DocumentoRelacionados = New DocumentoElectronicoReferenciaDocumentos() {New DocumentoElectronicoReferenciaDocumentos With {.Documento_Tipo = "09", .Documento_Serie = "0001", .Documento_Numero = "00000009"}}

        '   45 Leyendas
        Dim ListaLeyendas As New List(Of DocumentoElectronicoLeyenda)
        ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "1000", .Descripcion = NroEnLetras(Math.Truncate(Doc.Total_Importe_Venta)) & " " & Doc.Moneda_Descripcion})
        If Doc.Total_Importe_Venta = 0 Then
            ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "1002", .Descripcion = "TRANSFERENCIA GRATUITA DE UN BIEN Y/O SERVICIO PRESTADO GRATUITAMENTE"})
        End If
        If Doc.Total_Importe_Percepcion > 0 Then
            ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "2000", .Descripcion = "COMPROBANTE DE PERCEPCION"})
        End If
        If Not String.IsNullOrEmpty(Doc.Tipo_Operacion_Codigo) Then
            If Doc.Tipo_Operacion_Codigo = "05" Then
                ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "2005", .Descripcion = "VENTA REALIZADA POR EMISOR ITINERANTE"})
            End If
        End If
        Doc.Leyendas = ListaLeyendas.ToArray

        'Numero de placa
        'Doc.Numero_Placa_del_Vehiculo = "CFD-654"

        'Factura Guía
        'Doc.GuiaFactura_Documento_Tipo_Codigo = "09"
        'Doc.GuiaFactura_Documento_Serie = "T001"
        'Doc.GuiaFactura_Documento_Numero = "00000001"
        'Doc.GuiaFactura_RemitenteDocumentoOriginal_NumDoc = "1042171355"


        'Doc.MotivoTraslad_Codigo = "01" 'Catalogo 20
        'Doc.MotivoTraslad_Detalle = "VENTA"

        'Doc.PesoBrutoTotalGuia_UnidMed = "KGM" 'CAT 03
        'Doc.PesoBrutoTotalGuia_Valor = 2 '

        'Doc.ModalidadTraslado_Codigo = "02" 'Cat 18 :--01:Tranporte publico; 02:Transporte Privado

        'Doc.Fecha_Inicio_Traslado_o_Entrega_Bienes_Al_Transportista = Doc.Fecha_Emision

        ''--Transportista (Transporte Público)
        ''Doc.Transportista_Documento_Tipo = "6" 'cat 06
        ''Doc.Transportista_Documento_Numero = "10421713559"
        ''Doc.Transportista_RazonSocial_Nombre = "Transporte publico sac"
        'Doc.Transportista_Documento_Tipo = Nothing 'cat 06
        'Doc.Transportista_Documento_Numero = Nothing
        'Doc.Transportista_RazonSocial_Nombre = Nothing

        ''--VEHICULO (Transporte Privado)
        'Dim ListaTransportes As New List(Of DocumentoElectronicoTransporte)
        'ListaTransportes.Add(New DocumentoElectronicoTransporte With {.Placa = "PLACA-001", .NumeroInscripcion_CertificadoHabilitacionVehicular = "gdshdg"})
        'Doc.Transportes = ListaTransportes.ToArray
        ''--CONDUCTOR (Transporte Privado)
        'Doc.Conductor_Documento_Tipo = "1"
        'Doc.Conductor_Documento_Numero = "42171355" 'CAT 06
        ''--Direccion punto de llegada
        'Doc.Llegada_Ubigeo = "020101"

        'Doc.Llegada_Direccion = "AV. LOS PROCERES NO 234" '185 -2015
        ''Doc.Llegada_Urbanizacion = "Ubanizacion 1" '185 -2015
        ''Doc.Llegada_Departamento = "ANCASH" '185 -2015
        ''Doc.Llegada_Provincia = "HUARAZ" '185 -2015
        ''Doc.Llegada_Distrito = "HUARAZ" '185 -2015
        ''Doc.Llegada_Pais = "PE" '185 -2015

        ''--Datos del contenedor (Motivo Importación)

        ''--Direccion del punto de partida
        'Doc.Salida_Ubigeo = "020101"
        'Doc.Salida_Direccion = "Av. Los Gallinazos NO 345"
        'Doc.Salida_Direccion = "AV. LOS PROCERES NO 234" '185 -2015
        ''Doc.Salida_Urbanizacion = "Ubanizacion 2" '185 -2015
        ''Doc.Salida_Departamento = "ANCASH" '185 -2015
        ''Doc.Salida_Provincia = "HUARAZ" '185 -2015
        ''Doc.Salida_Distrito = "HUARAZ" '185 -2015
        ''Doc.Salida_Pais = "PE" '185 -2015


        Dim settings As New Xml.XmlWriterSettings With {.ConformanceLevel = Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}

        Using writer As Xml.XmlWriter = Xml.XmlWriter.Create("DocFactura.xml", settings)
            Dim typeToSerialize As Type = GetType(DocumentoElectronico)
            Dim xs As New Xml.Serialization.XmlSerializer(typeToSerialize)
            xs.Serialize(writer, Doc)
        End Using

        Dim doc12 As New Xml.XmlDocument()
        doc12.Load("DocFactura.xml")

        Dim PruebaXML__1 As New ws_documentoelectronico
        Dim RptaWS As String = PruebaXML__1.Documento_RegistrarActualizar(doc12)
        'If RptaWS <> "0" Then
        MessageBox.Show(RptaWS)
    End Sub



    Private Sub Button24_Click(sender As Object, e As EventArgs) Handles Button24.Click
        'GENERAR XML
        Dim Doc As New DocumentoElectronico
        Doc.Fecha_Emision = CDate(TxtDocFechEmision.Text)
        Doc.Emisor_ApellidosNombres_RazonSocial = Emisor_RazonSocial
        'Doc.Emisor_NombreComercial = ""
        Doc.Emisor_Documento_Tipo = "6"
        Doc.Emisor_Documento_Numero = Emisor_RUC

        '--- Domicilio fiscal -- PostalAddress
        Doc.Emisor_Direccion_Ubigeo = Emisor_Ubigeo
        Doc.Emisor_Direccion_Calle = "Calle Nº 456"
        Doc.Emisor_Direccion_Urbanizacion = "Ubanizacion "
        Doc.Emisor_Direccion_Departamento = "ANCASH"
        Doc.Emisor_Direccion_Provincia = "HUARAZ"
        Doc.Emisor_Direccion_Distrito = "HUARAZ"
        Doc.Emisor_Direccion_PaisCodigo = "PE"

        Doc.TipoDocumento_Codigo = "01"

        Doc.TipoDocumento_Descripcion = "FACTURA ELECTRONICA"
        Doc.Cliente_Documento_Tipo = "6"
        Doc.Cliente_Documento_Numero = Cliente_Factura_RUC
        Doc.Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial
        Doc.Cliente_Direccion = "Calle Los Geranios 2367 – Urb. Matellini – Chorrillos - Lima"
        Doc.Cliente_Correo = "donny.hn@gmail.com"

        Doc.TipoDocumento_Serie = Documento_Factura_Serie
        Doc.TipoDocumento_Numero = TxtDocNumero.Text


        Doc.Detalles = New DocumentoElectronicoDetalle() {New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "Cap-258963", .Producto_Descripcion = "CAPTOPRIL 25mg X 30", .Producto_UnidadMedida_Codigo = "ZZ", .Producto_Cantidad = 2, .Unitario_Precio_Venta = 180, .Unitario_Precio_Venta_Tipo_Codigo = "01", .Item_IGV_Total = 32.4, .Unitario_IGV_Porcentaje = 18, .Item_Tipo_Afectacion_IGV_Codigo = "13", .Item_ISC_Total = 0, .Unitario_Valor_Unitario = 147.6, .Item_ValorVenta_Total = 295.2}}

        Doc.Total_ValorVenta_OperacionesGravadas = 295.2
        Doc.Total_ValorVenta_OperacionesInafectas = 0
        Doc.Total_ValorVenta_OperacionesExoneradas = 0
        Doc.Total_ValorVenta_OperacionesGratuitas = 0
        Doc.Total_IGV = 64.8
        Doc.Total_ISC = 0
        Doc.Total_OtrosTributos = 0
        Doc.Total_OtrosCargos = 0
        'Doc.Descuentos_Globales = 0

        Doc.Total_Importe_Venta = 360

        Doc.Moneda_Codigo = "PEN"
        Doc.Moneda_Descripcion = "SOLES"
        Doc.Moneda_Simbolo = "S/"

        Doc.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc.Total_Importe_Venta)) & " " & Doc.Moneda_Descripcion

        Doc.Total_Importe_Percepcion = 0

        Doc.Impresion_Modelo_Codigo = "001"

        'Venta itinerante
        Doc.Tipo_Operacion_Codigo = "05" 'Cat 17 --05 Venta itinerante, 06 factura guia, 08 factura comprobante percepcion , Venta interna, exportacion, no domiciliados, etc

        Doc.Adic_Lugar_Entrega_Bien_PrestaServicio_Ubigeo = Emisor_Ubigeo
        Doc.Adic_Lugar_Entrega_Bien_PrestaServicio_Calle = "Calle Nº 456"
        Doc.Adic_Lugar_Entrega_Bien_PrestaServicio_Urbanizacion = "Ubanizacion "
        Doc.Adic_Lugar_Entrega_Bien_PrestaServicio_Departamento = "ANCASH"
        Doc.Adic_Lugar_Entrega_Bien_PrestaServicio_Provincia = "HUARAZ"
        Doc.Adic_Lugar_Entrega_Bien_PrestaServicio_Distrito = "HUARAZ"
        Doc.Adic_Lugar_Entrega_Bien_PrestaServicio_PaisCodigo = "PE"

        'Doc.DocumentoRelacionados = New DocumentoElectronicoReferenciaDocumentos() {New DocumentoElectronicoReferenciaDocumentos With {.Documento_Tipo = "09", .Documento_Serie = "0001", .Documento_Numero = "00000009"}}

        '   45 Leyendas
        Dim ListaLeyendas As New List(Of DocumentoElectronicoLeyenda)
        ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "1000", .Descripcion = NroEnLetras(Math.Truncate(Doc.Total_Importe_Venta)) & " " & Doc.Moneda_Descripcion})
        If Doc.Total_Importe_Venta = 0 Then
            ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "1002", .Descripcion = "TRANSFERENCIA GRATUITA DE UN BIEN Y/O SERVICIO PRESTADO GRATUITAMENTE"})
        End If
        If Doc.Total_Importe_Percepcion > 0 Then
            ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "2000", .Descripcion = "COMPROBANTE DE PERCEPCION"})
        End If
        If Not String.IsNullOrEmpty(Doc.Tipo_Operacion_Codigo) Then
            If Doc.Tipo_Operacion_Codigo = "05" Then
                ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "2005", .Descripcion = "VENTA REALIZADA POR EMISOR ITINERANTE"})
            End If
        End If
        Doc.Leyendas = ListaLeyendas.ToArray

        'Numero de placa
        'Doc.Numero_Placa_del_Vehiculo = "CFD-654"

        'Factura Guía
        'Doc.GuiaFactura_Documento_Tipo_Codigo = "09"
        'Doc.GuiaFactura_Documento_Serie = "T001"
        'Doc.GuiaFactura_Documento_Numero = "00000001"
        'Doc.GuiaFactura_RemitenteDocumentoOriginal_NumDoc = "1042171355"


        'Doc.MotivoTraslad_Codigo = "01" 'Catalogo 20
        'Doc.MotivoTraslad_Detalle = "VENTA"

        'Doc.PesoBrutoTotalGuia_UnidMed = "KGM" 'CAT 03
        'Doc.PesoBrutoTotalGuia_Valor = 2 '

        'Doc.ModalidadTraslado_Codigo = "02" 'Cat 18 :--01:Tranporte publico; 02:Transporte Privado

        'Doc.Fecha_Inicio_Traslado_o_Entrega_Bienes_Al_Transportista = Doc.Fecha_Emision

        ''--Transportista (Transporte Público)
        ''Doc.Transportista_Documento_Tipo = "6" 'cat 06
        ''Doc.Transportista_Documento_Numero = "10421713559"
        ''Doc.Transportista_RazonSocial_Nombre = "Transporte publico sac"
        'Doc.Transportista_Documento_Tipo = Nothing 'cat 06
        'Doc.Transportista_Documento_Numero = Nothing
        'Doc.Transportista_RazonSocial_Nombre = Nothing

        ''--VEHICULO (Transporte Privado)
        'Dim ListaTransportes As New List(Of DocumentoElectronicoTransporte)
        'ListaTransportes.Add(New DocumentoElectronicoTransporte With {.Placa = "PLACA-001", .NumeroInscripcion_CertificadoHabilitacionVehicular = "gdshdg"})
        'Doc.Transportes = ListaTransportes.ToArray
        ''--CONDUCTOR (Transporte Privado)
        'Doc.Conductor_Documento_Tipo = "1"
        'Doc.Conductor_Documento_Numero = "42171355" 'CAT 06
        ''--Direccion punto de llegada
        'Doc.Llegada_Ubigeo = "020101"

        'Doc.Llegada_Direccion = "AV. LOS PROCERES NO 234" '185 -2015
        ''Doc.Llegada_Urbanizacion = "Ubanizacion 1" '185 -2015
        ''Doc.Llegada_Departamento = "ANCASH" '185 -2015
        ''Doc.Llegada_Provincia = "HUARAZ" '185 -2015
        ''Doc.Llegada_Distrito = "HUARAZ" '185 -2015
        ''Doc.Llegada_Pais = "PE" '185 -2015

        ''--Datos del contenedor (Motivo Importación)

        ''--Direccion del punto de partida
        'Doc.Salida_Ubigeo = "020101"
        'Doc.Salida_Direccion = "Av. Los Gallinazos NO 345"
        'Doc.Salida_Direccion = "AV. LOS PROCERES NO 234" '185 -2015
        ''Doc.Salida_Urbanizacion = "Ubanizacion 2" '185 -2015
        ''Doc.Salida_Departamento = "ANCASH" '185 -2015
        ''Doc.Salida_Provincia = "HUARAZ" '185 -2015
        ''Doc.Salida_Distrito = "HUARAZ" '185 -2015
        ''Doc.Salida_Pais = "PE" '185 -2015


        Dim settings As New Xml.XmlWriterSettings With {.ConformanceLevel = Xml.ConformanceLevel.Auto, .Indent = True, .IndentChars = ChrW(9), .Encoding = System.Text.Encoding.UTF8}

        Using writer As Xml.XmlWriter = Xml.XmlWriter.Create("DocFactura.xml", settings)
            Dim typeToSerialize As Type = GetType(DocumentoElectronico)
            Dim xs As New Xml.Serialization.XmlSerializer(typeToSerialize)
            xs.Serialize(writer, Doc)
        End Using

        Dim doc12 As New Xml.XmlDocument()
        doc12.Load("DocFactura.xml")

        Dim PruebaXML__1 As New ws_documentoelectronico
        Dim RptaWS As String = PruebaXML__1.Documento_RegistrarActualizar(doc12)
        'If RptaWS <> "0" Then
        MessageBox.Show(RptaWS)
    End Sub

    Private Sub Button23_Click(sender As Object, e As EventArgs) Handles Button23.Click
        Using f As New FrmHomologacion
            f.ShowDialog()
        End Using
    End Sub



    Private Sub Button25_Click(sender As Object, e As EventArgs) Handles Button25.Click
        Using f As New OpenFileDialog
            If f.ShowDialog = Windows.Forms.DialogResult.OK Then

                Dim doc12 As New Xml.XmlDocument()
                doc12.Load(f.FileName)

                Dim PruebaXML__1 As New ws_documentoelectronico
                Dim RptaWS As String = PruebaXML__1.Documento_RegistrarActualizar(doc12)
                'If RptaWS <> "0" Then
                MessageBox.Show(RptaWS)

            End If

        End Using


    End Sub
End Class
