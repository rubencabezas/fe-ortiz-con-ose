﻿Public Class FrmHomologacion
    Private Cliente_Boleta_DNI As String = "42171355"
    Private Cliente_Boleta_Nombres As String = "Donny Huaman Novoa"
    Private Cliente_Factura_RUC As String = "10421713559"
    Private Cliente_Factura_RazonSocial As String = "Donny Huaman Novoa"

    Dim CasosHomologacion As New List(Of DocumentoElectronico)
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        '1 Facturas y Notas FF11 Ventas Gravadas IGV
        '2 Facturas y Notas FF12 Ventas inafectas y/o exoneradas
        '3 Facturas y Notas FF13 Ventas gratuitas
        '4 Facturas y Notas FF14 Ventas con descuento global
        '5 Facturas y Notas FF30 Operaciones gravadas con ISC
        '6 Facturas y Notas FF40 Operaciones con percepción
        '7 Facturas y Notas FF50 Operaciones con otro tipo de moneda
        '8 Boletas y Notas BB11 Ventas Gravadas IGV
        '9 Boletas y Notas BB12 Ventas inafectas y/o exoneradas
        '10 Boletas y Notas BB13 Ventas gratuitas
        '11 Boletas y Notas BB14 Ventas con descuento global
        '12 Boletas y Notas BB50 Operaciones con otro tipo de moneda
        '13 Resúmenes Diarios - Resumen diario de Boletas
        '14 Comunicación de Baja - Comunicación de Baja

        '-------GRUPO 01 : FF11 Ventas Gravadas IGV
        '-------    Factura 1 con 3 ítems
        Dim Doc_01_FF11_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF11", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF11_00000001.Homogacion_Grupo_Codigo = "1"
        Doc_01_FF11_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF11_00000001.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_01_FF11_00000001 = LlenarDetalles_Afecto(Doc_01_FF11_00000001, 3)
        Doc_01_FF11_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF11_00000001.Total_Importe_Venta)) & " " & Doc_01_FF11_00000001.Moneda_Descripcion
        Doc_01_FF11_00000001 = LlenarLeyendas(Doc_01_FF11_00000001)

        CasosHomologacion.Add(Doc_01_FF11_00000001)

        '-------    Factura 2 con 2 ítems
        Dim Doc_01_FF11_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                          .TipoDocumento_Serie = "FF11", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                          .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                          .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                          .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF11_00000002.Homogacion_Grupo_Codigo = "1"
        Doc_01_FF11_00000002.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF11_00000002.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_01_FF11_00000002 = LlenarDetalles_Afecto(Doc_01_FF11_00000002, 2)
        Doc_01_FF11_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF11_00000002.Total_Importe_Venta)) & " " & Doc_01_FF11_00000002.Moneda_Descripcion
        Doc_01_FF11_00000002 = LlenarLeyendas(Doc_01_FF11_00000002)

        CasosHomologacion.Add(Doc_01_FF11_00000002)

        '-------    Factura 3 con 1 ítems
        Dim Doc_01_FF11_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                          .TipoDocumento_Serie = "FF11", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                          .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                          .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                         .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF11_00000003.Homogacion_Grupo_Codigo = "1"
        Doc_01_FF11_00000003.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF11_00000003.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_01_FF11_00000003 = LlenarDetalles_Afecto(Doc_01_FF11_00000003, 1)
        Doc_01_FF11_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF11_00000003.Total_Importe_Venta)) & " " & Doc_01_FF11_00000003.Moneda_Descripcion
        Doc_01_FF11_00000003 = LlenarLeyendas(Doc_01_FF11_00000003)

        CasosHomologacion.Add(Doc_01_FF11_00000003)

        '-------    Factura 4 con 5 ítems
        Dim Doc_01_FF11_00000004 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF11", .TipoDocumento_Numero = "00000004", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF11_00000004.Homogacion_Grupo_Codigo = "1"
        Doc_01_FF11_00000004.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF11_00000004.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_01_FF11_00000004 = LlenarDetalles_Afecto(Doc_01_FF11_00000004, 5)
        Doc_01_FF11_00000004.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF11_00000004.Total_Importe_Venta)) & " " & Doc_01_FF11_00000004.Moneda_Descripcion
        Doc_01_FF11_00000004 = LlenarLeyendas(Doc_01_FF11_00000004)

        CasosHomologacion.Add(Doc_01_FF11_00000004)

        '-------    Factura 5 con 4 ítems
        Dim Doc_01_FF11_00000005 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF11", .TipoDocumento_Numero = "00000005", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF11_00000005.Homogacion_Grupo_Codigo = "1"
        Doc_01_FF11_00000005.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF11_00000005.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_01_FF11_00000005 = LlenarDetalles_Afecto(Doc_01_FF11_00000005, 4)
        Doc_01_FF11_00000005.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF11_00000005.Total_Importe_Venta)) & " " & Doc_01_FF11_00000005.Moneda_Descripcion
        Doc_01_FF11_00000005 = LlenarLeyendas(Doc_01_FF11_00000005)

        CasosHomologacion.Add(Doc_01_FF11_00000005)

        '-------    Nota de crédito de factura 2
        Dim Doc_07_FF11_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF11", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_FF11_00000001.Homogacion_Grupo_Codigo = "1"
        Doc_07_FF11_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_07_FF11_00000001.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_07_FF11_00000001 = NotaCreditoDeFactura(Doc_07_FF11_00000001, Doc_01_FF11_00000002)
        Doc_07_FF11_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_FF11_00000001.Total_Importe_Venta)) & " " & Doc_07_FF11_00000001.Moneda_Descripcion
        Doc_07_FF11_00000001 = LlenarLeyendas(Doc_07_FF11_00000001)

        CasosHomologacion.Add(Doc_07_FF11_00000001)

        '-------    Nota de crédito de factura 3
        Dim Doc_07_FF11_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF11", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_FF11_00000002.Homogacion_Grupo_Codigo = "1"
        Doc_07_FF11_00000002.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_07_FF11_00000002.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_07_FF11_00000002 = NotaCreditoDeFactura(Doc_07_FF11_00000002, Doc_01_FF11_00000003)
        Doc_07_FF11_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_FF11_00000002.Total_Importe_Venta)) & " " & Doc_07_FF11_00000002.Moneda_Descripcion
        Doc_07_FF11_00000002 = LlenarLeyendas(Doc_07_FF11_00000002)

        CasosHomologacion.Add(Doc_07_FF11_00000002)

        '-------    Nota de crédito de factura 4
        Dim Doc_07_FF11_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF11", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_FF11_00000003.Homogacion_Grupo_Codigo = "1"
        Doc_07_FF11_00000003.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_07_FF11_00000003.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_07_FF11_00000003 = NotaCreditoDeFactura(Doc_07_FF11_00000003, Doc_01_FF11_00000004)
        Doc_07_FF11_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_FF11_00000003.Total_Importe_Venta)) & " " & Doc_07_FF11_00000003.Moneda_Descripcion
        Doc_07_FF11_00000003 = LlenarLeyendas(Doc_07_FF11_00000003)

        CasosHomologacion.Add(Doc_07_FF11_00000003)

        '-------    Nota de débito de factura 2
        Dim Doc_08_FF11_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF11", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_FF11_00000001.Homogacion_Grupo_Codigo = "1"
        Doc_08_FF11_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_08_FF11_00000001.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_08_FF11_00000001 = NotaDebitoDeFactura(Doc_08_FF11_00000001, Doc_01_FF11_00000002)
        Doc_08_FF11_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_FF11_00000001.Total_Importe_Venta)) & " " & Doc_08_FF11_00000001.Moneda_Descripcion
        Doc_08_FF11_00000001 = LlenarLeyendas(Doc_08_FF11_00000001)

        CasosHomologacion.Add(Doc_08_FF11_00000001)

        '-------    Nota de débito de factura 3
        Dim Doc_08_FF11_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF11", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_FF11_00000002.Homogacion_Grupo_Codigo = "1"
        Doc_08_FF11_00000002.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_08_FF11_00000002.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_08_FF11_00000002 = NotaDebitoDeFactura(Doc_08_FF11_00000002, Doc_01_FF11_00000003)
        Doc_08_FF11_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_FF11_00000002.Total_Importe_Venta)) & " " & Doc_08_FF11_00000002.Moneda_Descripcion
        Doc_08_FF11_00000002 = LlenarLeyendas(Doc_08_FF11_00000002)

        CasosHomologacion.Add(Doc_08_FF11_00000002)

        '-------    Nota de débito de factura 4
        Dim Doc_08_FF11_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF11", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_FF11_00000003.Homogacion_Grupo_Codigo = "1"
        Doc_08_FF11_00000003.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_08_FF11_00000003.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_08_FF11_00000003 = NotaDebitoDeFactura(Doc_08_FF11_00000003, Doc_01_FF11_00000004)
        Doc_08_FF11_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_FF11_00000003.Total_Importe_Venta)) & " " & Doc_08_FF11_00000003.Moneda_Descripcion
        Doc_08_FF11_00000003 = LlenarLeyendas(Doc_08_FF11_00000003)

        CasosHomologacion.Add(Doc_08_FF11_00000003)

        '-------GRUPO 02 : FF12 Ventas inafectas y/o exoneradas
        '-------    Factura 1 con 1 ítem
        Dim Doc_01_FF12_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF12", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF12_00000001.Homogacion_Grupo_Codigo = "1"
        Doc_01_FF12_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF12_00000001.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_01_FF12_00000001 = LlenarDetalles_InafectaExonerada(Doc_01_FF12_00000001, 1, "20") '20 Exonerado - Operacion Onerosa '30 Inafecto - Operacion Onerosa
        Doc_01_FF12_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF12_00000001.Total_Importe_Venta)) & " " & Doc_01_FF12_00000001.Moneda_Descripcion
        Doc_01_FF12_00000001 = LlenarLeyendas(Doc_01_FF12_00000001)

        CasosHomologacion.Add(Doc_01_FF12_00000001)

        '-------    Factura 2 con 4 ítems
        Dim Doc_01_FF12_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF12", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF12_00000002.Homogacion_Grupo_Codigo = "1"
        Doc_01_FF12_00000002.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF12_00000002.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_01_FF12_00000002 = LlenarDetalles_InafectaExonerada(Doc_01_FF12_00000002, 4, "20") '20 Exonerado - Operacion Onerosa '30 Inafecto - Operacion Onerosa
        Doc_01_FF12_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF12_00000002.Total_Importe_Venta)) & " " & Doc_01_FF12_00000002.Moneda_Descripcion
        Doc_01_FF12_00000002 = LlenarLeyendas(Doc_01_FF12_00000002)

        CasosHomologacion.Add(Doc_01_FF12_00000002)

        '-------    Factura 3 con 7 ítems
        Dim Doc_01_FF12_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF12", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF12_00000003.Homogacion_Grupo_Codigo = "1"
        Doc_01_FF12_00000003.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF12_00000003.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_01_FF12_00000003 = LlenarDetalles_InafectaExonerada(Doc_01_FF12_00000003, 7, "30") '20 Exonerado - Operacion Onerosa '30 Inafecto - Operacion Onerosa
        Doc_01_FF12_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF12_00000003.Total_Importe_Venta)) & " " & Doc_01_FF12_00000003.Moneda_Descripcion
        Doc_01_FF12_00000003 = LlenarLeyendas(Doc_01_FF12_00000003)

        CasosHomologacion.Add(Doc_01_FF12_00000003)

        '-------    Factura 4 con 5 ítems
        Dim Doc_01_FF12_00000004 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF12", .TipoDocumento_Numero = "00000004", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF12_00000004.Homogacion_Grupo_Codigo = "1"
        Doc_01_FF12_00000004.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF12_00000004.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_01_FF12_00000004 = LlenarDetalles_InafectaExonerada(Doc_01_FF12_00000004, 5, "20") '20 Exonerado - Operacion Onerosa '30 Inafecto - Operacion Onerosa
        Doc_01_FF12_00000004.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF12_00000004.Total_Importe_Venta)) & " " & Doc_01_FF12_00000004.Moneda_Descripcion
        Doc_01_FF12_00000004 = LlenarLeyendas(Doc_01_FF12_00000004)

        CasosHomologacion.Add(Doc_01_FF12_00000004)

        '-------    Factura 5 con 6 ítems
        Dim Doc_01_FF12_00000005 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF12", .TipoDocumento_Numero = "00000005", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF12_00000005.Homogacion_Grupo_Codigo = "1"
        Doc_01_FF12_00000005.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF12_00000005.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_01_FF12_00000005 = LlenarDetalles_InafectaExonerada(Doc_01_FF12_00000005, 6, "20") '20 Exonerado - Operacion Onerosa '30 Inafecto - Operacion Onerosa
        Doc_01_FF12_00000005.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF12_00000005.Total_Importe_Venta)) & " " & Doc_01_FF12_00000005.Moneda_Descripcion
        Doc_01_FF12_00000005 = LlenarLeyendas(Doc_01_FF12_00000005)

        CasosHomologacion.Add(Doc_01_FF12_00000005)

        '-------    Nota de crédito de factura 1
        Dim Doc_07_FF12_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF12", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_FF12_00000001.Homogacion_Grupo_Codigo = "1"
        Doc_07_FF12_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_07_FF12_00000001.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_07_FF12_00000001 = NotaCreditoDeFactura(Doc_07_FF12_00000001, Doc_01_FF12_00000001)
        Doc_07_FF12_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_FF12_00000001.Total_Importe_Venta)) & " " & Doc_07_FF12_00000001.Moneda_Descripcion
        Doc_07_FF12_00000001 = LlenarLeyendas(Doc_07_FF12_00000001)

        CasosHomologacion.Add(Doc_07_FF12_00000001)

        '-------    Nota de crédito de factura 3
        Dim Doc_07_FF12_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF12", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_FF12_00000002.Homogacion_Grupo_Codigo = "1"
        Doc_07_FF12_00000002.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_07_FF12_00000002.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_07_FF12_00000002 = NotaCreditoDeFactura(Doc_07_FF12_00000002, Doc_01_FF12_00000003)
        Doc_07_FF12_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_FF12_00000002.Total_Importe_Venta)) & " " & Doc_07_FF12_00000002.Moneda_Descripcion
        Doc_07_FF12_00000002 = LlenarLeyendas(Doc_07_FF12_00000002)

        CasosHomologacion.Add(Doc_07_FF12_00000002)

        '-------    Nota de crédito de factura 5
        Dim Doc_07_FF12_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF12", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_FF12_00000003.Homogacion_Grupo_Codigo = "1"
        Doc_07_FF12_00000003.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_07_FF12_00000003.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_07_FF12_00000003 = NotaCreditoDeFactura(Doc_07_FF12_00000003, Doc_01_FF12_00000005)
        Doc_07_FF12_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_FF12_00000003.Total_Importe_Venta)) & " " & Doc_07_FF12_00000003.Moneda_Descripcion
        Doc_07_FF12_00000003 = LlenarLeyendas(Doc_07_FF12_00000003)

        CasosHomologacion.Add(Doc_07_FF12_00000003)

        '-------    Nota de débito de factura 1
        Dim Doc_08_FF12_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF12", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_FF12_00000001.Homogacion_Grupo_Codigo = "1"
        Doc_08_FF12_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_08_FF12_00000001.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_08_FF12_00000001 = NotaDebitoDeFactura(Doc_08_FF12_00000001, Doc_01_FF12_00000001)
        Doc_08_FF12_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_FF12_00000001.Total_Importe_Venta)) & " " & Doc_08_FF12_00000001.Moneda_Descripcion
        Doc_08_FF12_00000001 = LlenarLeyendas(Doc_08_FF12_00000001)

        CasosHomologacion.Add(Doc_08_FF12_00000001)

        '-------    Nota de débito de factura 3
        Dim Doc_08_FF12_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF12", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_FF12_00000002.Homogacion_Grupo_Codigo = "1"
        Doc_08_FF12_00000002.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_08_FF12_00000002.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_08_FF12_00000002 = NotaDebitoDeFactura(Doc_08_FF12_00000002, Doc_01_FF12_00000003)
        Doc_08_FF12_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_FF12_00000002.Total_Importe_Venta)) & " " & Doc_08_FF12_00000002.Moneda_Descripcion
        Doc_08_FF12_00000002 = LlenarLeyendas(Doc_08_FF12_00000002)

        CasosHomologacion.Add(Doc_08_FF12_00000002)

        '-------    Nota de débito de factura 5
        Dim Doc_08_FF12_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF12", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_FF12_00000003.Homogacion_Grupo_Codigo = "1"
        Doc_08_FF12_00000003.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_08_FF12_00000003.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_08_FF12_00000003 = NotaDebitoDeFactura(Doc_08_FF12_00000003, Doc_01_FF12_00000005)
        Doc_08_FF12_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_FF12_00000003.Total_Importe_Venta)) & " " & Doc_08_FF12_00000003.Moneda_Descripcion
        Doc_08_FF12_00000003 = LlenarLeyendas(Doc_08_FF12_00000003)

        CasosHomologacion.Add(Doc_08_FF12_00000003)

        '-------GRUPO 03 : FF13 Ventas gratuitas
        '-------    Factura 1 con 7 ítems
        Dim Doc_01_FF13_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF13", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF13_00000001.Homogacion_Grupo_Codigo = "1"
        Doc_01_FF13_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF13_00000001.Homogacion_Grupo_Operacion = "Ventas gratuitas"

        Doc_01_FF13_00000001 = LlenarDetalles_Gratuitas(Doc_01_FF13_00000001, 7)
        Doc_01_FF13_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF13_00000001.Total_Importe_Venta)) & " " & Doc_01_FF13_00000001.Moneda_Descripcion
        Doc_01_FF13_00000001 = LlenarLeyendas(Doc_01_FF13_00000001)

        CasosHomologacion.Add(Doc_01_FF13_00000001)

        '-------    Factura 2 con 2 ítems
        Dim Doc_01_FF13_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF13", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF13_00000002.Homogacion_Grupo_Codigo = "1"
        Doc_01_FF13_00000002.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF13_00000002.Homogacion_Grupo_Operacion = "Ventas gratuitas"

        Doc_01_FF13_00000002 = LlenarDetalles_Gratuitas(Doc_01_FF13_00000002, 2)
        Doc_01_FF13_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF13_00000002.Total_Importe_Venta)) & " " & Doc_01_FF13_00000002.Moneda_Descripcion
        Doc_01_FF13_00000002 = LlenarLeyendas(Doc_01_FF13_00000002)

        CasosHomologacion.Add(Doc_01_FF13_00000002)

        '-------    Factura 3 con 5 ítems
        Dim Doc_01_FF13_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF13", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF13_00000003.Homogacion_Grupo_Codigo = "1"
        Doc_01_FF13_00000003.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF13_00000003.Homogacion_Grupo_Operacion = "Ventas gratuitas"

        Doc_01_FF13_00000003 = LlenarDetalles_Gratuitas(Doc_01_FF13_00000003, 5)
        Doc_01_FF13_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF13_00000003.Total_Importe_Venta)) & " " & Doc_01_FF13_00000003.Moneda_Descripcion
        Doc_01_FF13_00000003 = LlenarLeyendas(Doc_01_FF13_00000003)

        CasosHomologacion.Add(Doc_01_FF13_00000003)

        '-------    Factura 4 con 4 ítems
        Dim Doc_01_FF13_00000004 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF13", .TipoDocumento_Numero = "00000004", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF13_00000004.Homogacion_Grupo_Codigo = "1"
        Doc_01_FF13_00000004.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF13_00000004.Homogacion_Grupo_Operacion = "Ventas gratuitas"

        Doc_01_FF13_00000004 = LlenarDetalles_Gratuitas(Doc_01_FF13_00000004, 4)
        Doc_01_FF13_00000004.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF13_00000004.Total_Importe_Venta)) & " " & Doc_01_FF13_00000004.Moneda_Descripcion
        Doc_01_FF13_00000004 = LlenarLeyendas(Doc_01_FF13_00000004)

        CasosHomologacion.Add(Doc_01_FF13_00000004)

        '-------    Factura 5 con 3 ítems
        Dim Doc_01_FF13_00000005 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF13", .TipoDocumento_Numero = "00000005", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF13_00000005.Homogacion_Grupo_Codigo = "1"
        Doc_01_FF13_00000005.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF13_00000005.Homogacion_Grupo_Operacion = "Ventas gratuitas"

        Doc_01_FF13_00000005 = LlenarDetalles_Gratuitas(Doc_01_FF13_00000005, 3)
        Doc_01_FF13_00000005.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF13_00000005.Total_Importe_Venta)) & " " & Doc_01_FF13_00000005.Moneda_Descripcion
        Doc_01_FF13_00000005 = LlenarLeyendas(Doc_01_FF13_00000005)

        CasosHomologacion.Add(Doc_01_FF13_00000005)

        '-------GRUPO 04 : FF14 Ventas con descuento global
        '-------    Factura 1 con 2 ítems
        Dim Doc_01_FF14_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF14", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF14_00000001.Homogacion_Grupo_Codigo = "4"
        Doc_01_FF14_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF14_00000001.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_01_FF14_00000001 = LlenarDetalles_AfectoConDescuentos(Doc_01_FF14_00000001, 2)
        Doc_01_FF14_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF14_00000001.Total_Importe_Venta)) & " " & Doc_01_FF14_00000001.Moneda_Descripcion
        Doc_01_FF14_00000001 = LlenarLeyendas(Doc_01_FF14_00000001)

        CasosHomologacion.Add(Doc_01_FF14_00000001)

        '-------    Factura 2 con 1 ítems
        Dim Doc_01_FF14_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF14", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF14_00000002.Homogacion_Grupo_Codigo = "4"
        Doc_01_FF14_00000002.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF14_00000002.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_01_FF14_00000002 = LlenarDetalles_AfectoConDescuentos(Doc_01_FF14_00000002, 1)
        Doc_01_FF14_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF14_00000002.Total_Importe_Venta)) & " " & Doc_01_FF14_00000002.Moneda_Descripcion
        Doc_01_FF14_00000002 = LlenarLeyendas(Doc_01_FF14_00000002)

        CasosHomologacion.Add(Doc_01_FF14_00000002)

        '-------    Factura 3 con 4 ítems
        Dim Doc_01_FF14_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF14", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF14_00000003.Homogacion_Grupo_Codigo = "4"
        Doc_01_FF14_00000003.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF14_00000003.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_01_FF14_00000003 = LlenarDetalles_AfectoConDescuentos(Doc_01_FF14_00000003, 4)
        Doc_01_FF14_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF14_00000003.Total_Importe_Venta)) & " " & Doc_01_FF14_00000003.Moneda_Descripcion
        Doc_01_FF14_00000003 = LlenarLeyendas(Doc_01_FF14_00000003)

        CasosHomologacion.Add(Doc_01_FF14_00000003)

        '-------    Factura 4 con 3 ítems
        Dim Doc_01_FF14_00000004 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF14", .TipoDocumento_Numero = "00000004", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF14_00000004.Homogacion_Grupo_Codigo = "4"
        Doc_01_FF14_00000004.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF14_00000004.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_01_FF14_00000004 = LlenarDetalles_AfectoConDescuentos(Doc_01_FF14_00000004, 3)
        Doc_01_FF14_00000004.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF14_00000004.Total_Importe_Venta)) & " " & Doc_01_FF14_00000004.Moneda_Descripcion
        Doc_01_FF14_00000004 = LlenarLeyendas(Doc_01_FF14_00000004)

        CasosHomologacion.Add(Doc_01_FF14_00000004)

        '-------    Factura 5 con 5 ítems
        Dim Doc_01_FF14_00000005 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF14", .TipoDocumento_Numero = "00000005", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF14_00000005.Homogacion_Grupo_Codigo = "4"
        Doc_01_FF14_00000005.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF14_00000005.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_01_FF14_00000005 = LlenarDetalles_AfectoConDescuentos(Doc_01_FF14_00000005, 5)
        Doc_01_FF14_00000005.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF14_00000005.Total_Importe_Venta)) & " " & Doc_01_FF14_00000005.Moneda_Descripcion
        Doc_01_FF14_00000005 = LlenarLeyendas(Doc_01_FF14_00000005)

        CasosHomologacion.Add(Doc_01_FF14_00000005)

        '-------    Nota de crédito de factura 2
        Dim Doc_07_FF14_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF14", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_FF14_00000001.Homogacion_Grupo_Codigo = "4"
        Doc_07_FF14_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_07_FF14_00000001.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_07_FF14_00000001 = NotaCreditoDeFactura(Doc_07_FF14_00000001, Doc_01_FF14_00000002)
        Doc_07_FF14_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_FF14_00000001.Total_Importe_Venta)) & " " & Doc_07_FF14_00000001.Moneda_Descripcion
        Doc_07_FF14_00000001 = LlenarLeyendas(Doc_07_FF14_00000001)

        CasosHomologacion.Add(Doc_07_FF14_00000001)

        '-------    Nota de crédito de factura 3
        Dim Doc_07_FF14_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF14", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_FF14_00000002.Homogacion_Grupo_Codigo = "4"
        Doc_07_FF14_00000002.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_07_FF14_00000002.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_07_FF14_00000002 = NotaCreditoDeFactura(Doc_07_FF14_00000002, Doc_01_FF14_00000003)
        Doc_07_FF14_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_FF14_00000002.Total_Importe_Venta)) & " " & Doc_07_FF14_00000002.Moneda_Descripcion
        Doc_07_FF14_00000002 = LlenarLeyendas(Doc_07_FF14_00000002)

        CasosHomologacion.Add(Doc_07_FF14_00000002)

        '-------    Nota de crédito de factura 5
        Dim Doc_07_FF14_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF14", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_FF14_00000003.Homogacion_Grupo_Codigo = "4"
        Doc_07_FF14_00000003.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_07_FF14_00000003.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_07_FF14_00000003 = NotaCreditoDeFactura(Doc_07_FF14_00000003, Doc_01_FF14_00000005)
        Doc_07_FF14_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_FF14_00000003.Total_Importe_Venta)) & " " & Doc_07_FF14_00000003.Moneda_Descripcion
        Doc_07_FF14_00000003 = LlenarLeyendas(Doc_07_FF14_00000003)

        CasosHomologacion.Add(Doc_07_FF14_00000003)

        '-------    Nota de débito de factura 2
        Dim Doc_08_FF14_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF14", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_FF14_00000001.Homogacion_Grupo_Codigo = "4"
        Doc_08_FF14_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_08_FF14_00000001.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_08_FF14_00000001 = NotaDebitoDeFactura(Doc_08_FF14_00000001, Doc_01_FF14_00000002)
        Doc_08_FF14_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_FF14_00000001.Total_Importe_Venta)) & " " & Doc_08_FF14_00000001.Moneda_Descripcion
        Doc_08_FF14_00000001 = LlenarLeyendas(Doc_08_FF14_00000001)

        CasosHomologacion.Add(Doc_08_FF14_00000001)

        '-------    Nota de débito de factura 3
        Dim Doc_08_FF14_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF14", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_FF14_00000002.Homogacion_Grupo_Codigo = "4"
        Doc_08_FF14_00000002.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_08_FF14_00000002.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_08_FF14_00000002 = NotaDebitoDeFactura(Doc_08_FF14_00000002, Doc_01_FF14_00000003)
        Doc_08_FF14_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_FF14_00000002.Total_Importe_Venta)) & " " & Doc_08_FF14_00000002.Moneda_Descripcion
        Doc_08_FF14_00000002 = LlenarLeyendas(Doc_08_FF14_00000002)

        CasosHomologacion.Add(Doc_08_FF14_00000002)


        '-------    Nota de débito de factura 5
        Dim Doc_08_FF14_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF14", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_FF14_00000003.Homogacion_Grupo_Codigo = "4"
        Doc_08_FF14_00000003.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_08_FF14_00000003.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_08_FF14_00000003 = NotaDebitoDeFactura(Doc_08_FF14_00000003, Doc_01_FF14_00000005)
        Doc_08_FF14_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_FF14_00000003.Total_Importe_Venta)) & " " & Doc_08_FF14_00000003.Moneda_Descripcion
        Doc_08_FF14_00000003 = LlenarLeyendas(Doc_08_FF14_00000003)

        CasosHomologacion.Add(Doc_08_FF14_00000003)

        '-------GRUPO 05 : FF30 Operaciones gravadas con ISC
        '-------    Factura 1 con 5 ítems
        Dim Doc_01_FF30_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF30", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF30_00000001.Homogacion_Grupo_Codigo = "5"
        Doc_01_FF30_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF30_00000001.Homogacion_Grupo_Operacion = "Operaciones gravadas con ISC"

        Doc_01_FF30_00000001 = LlenarDetalles_GravadasConISC(Doc_01_FF30_00000001, 5)
        Doc_01_FF30_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF30_00000001.Total_Importe_Venta)) & " " & Doc_01_FF30_00000001.Moneda_Descripcion
        Doc_01_FF30_00000001 = LlenarLeyendas(Doc_01_FF30_00000001)

        CasosHomologacion.Add(Doc_01_FF30_00000001)

        '-------    Nota de crédito de factura 1
        Dim Doc_07_FF30_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF30", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_FF30_00000001.Homogacion_Grupo_Codigo = "5"
        Doc_07_FF30_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_07_FF30_00000001.Homogacion_Grupo_Operacion = "Operaciones gravadas con ISC"

        Doc_07_FF30_00000001 = NotaCreditoDeFactura(Doc_07_FF30_00000001, Doc_01_FF30_00000001)
        Doc_07_FF30_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_FF30_00000001.Total_Importe_Venta)) & " " & Doc_07_FF30_00000001.Moneda_Descripcion
        Doc_07_FF30_00000001 = LlenarLeyendas(Doc_07_FF30_00000001)

        CasosHomologacion.Add(Doc_07_FF30_00000001)

        '-------    Nota de débito de factura 1
        Dim Doc_08_FF30_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF30", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_FF30_00000001.Homogacion_Grupo_Codigo = "5"
        Doc_08_FF30_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_08_FF30_00000001.Homogacion_Grupo_Operacion = "Operaciones gravadas con ISC"

        Doc_08_FF30_00000001 = NotaDebitoDeFactura(Doc_08_FF30_00000001, Doc_01_FF30_00000001)
        Doc_08_FF30_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_FF30_00000001.Total_Importe_Venta)) & " " & Doc_08_FF30_00000001.Moneda_Descripcion
        Doc_08_FF30_00000001 = LlenarLeyendas(Doc_08_FF30_00000001)

        CasosHomologacion.Add(Doc_08_FF30_00000001)

        '-------GRUPO 06 : FF40 Operaciones con Percepcion ---Falta
        '-------    Factura 1 con 5 ítems
        Dim Doc_01_FF40_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF40", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF40_00000001.Homogacion_Grupo_Codigo = "6"
        Doc_01_FF40_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF40_00000001.Homogacion_Grupo_Operacion = "Operaciones con Percepcion - Falta concluir"

        Doc_01_FF40_00000001 = LlenarDetalles_GravadasConISC(Doc_01_FF40_00000001, 5)
        Doc_01_FF40_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF40_00000001.Total_Importe_Venta)) & " " & Doc_01_FF40_00000001.Moneda_Descripcion
        Doc_01_FF40_00000001 = LlenarLeyendas(Doc_01_FF40_00000001)

        CasosHomologacion.Add(Doc_01_FF40_00000001)

        '-------    Nota de crédito de factura 1
        Dim Doc_07_FF40_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF40", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_FF40_00000001.Homogacion_Grupo_Codigo = "6"
        Doc_07_FF40_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_07_FF40_00000001.Homogacion_Grupo_Operacion = "Operaciones con Percepcion - Falta concluir"

        Doc_07_FF40_00000001 = NotaCreditoDeFactura(Doc_07_FF40_00000001, Doc_01_FF40_00000001)
        Doc_07_FF40_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_FF40_00000001.Total_Importe_Venta)) & " " & Doc_07_FF40_00000001.Moneda_Descripcion
        Doc_07_FF40_00000001 = LlenarLeyendas(Doc_07_FF40_00000001)

        CasosHomologacion.Add(Doc_07_FF40_00000001)

        '-------    Nota de débito de factura 1
        Dim Doc_08_FF40_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF40", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_FF40_00000001.Homogacion_Grupo_Codigo = "6"
        Doc_08_FF40_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_08_FF40_00000001.Homogacion_Grupo_Operacion = "Operaciones con Percepcion - Falta concluir"

        Doc_08_FF40_00000001 = NotaDebitoDeFactura(Doc_08_FF40_00000001, Doc_01_FF40_00000001)
        Doc_08_FF40_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_FF40_00000001.Total_Importe_Venta)) & " " & Doc_08_FF40_00000001.Moneda_Descripcion
        Doc_08_FF40_00000001 = LlenarLeyendas(Doc_08_FF40_00000001)

        CasosHomologacion.Add(Doc_08_FF40_00000001)

        '-------GRUPO 07 : FF50 Operaciones con otro tipo de moneda
        '-------    Factura 1 con 5 ítems
        Dim Doc_01_FF50_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "01", .TipoDocumento_Descripcion = "FACTURA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF50", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "USD", .Moneda_Descripcion = "DOLARES", .Moneda_Simbolo = "US$.",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_01_FF50_00000001.Homogacion_Grupo_Codigo = "7"
        Doc_01_FF50_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_01_FF50_00000001.Homogacion_Grupo_Operacion = "Operaciones con otro tipo de moneda"

        Doc_01_FF50_00000001 = LlenarDetalles_Afecto(Doc_01_FF50_00000001, 5)
        Doc_01_FF50_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_01_FF50_00000001.Total_Importe_Venta)) & " " & Doc_01_FF50_00000001.Moneda_Descripcion
        Doc_01_FF50_00000001 = LlenarLeyendas(Doc_01_FF50_00000001)

        CasosHomologacion.Add(Doc_01_FF50_00000001)

        '-------    Nota de crédito de factura 1
        Dim Doc_07_FF50_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF50", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "USD", .Moneda_Descripcion = "DOLARES", .Moneda_Simbolo = "US$.",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_FF50_00000001.Homogacion_Grupo_Codigo = "7"
        Doc_07_FF50_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_07_FF50_00000001.Homogacion_Grupo_Operacion = "Operaciones con otro tipo de moneda"

        Doc_07_FF50_00000001 = NotaCreditoDeFactura(Doc_07_FF50_00000001, Doc_01_FF50_00000001)
        Doc_07_FF50_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_FF50_00000001.Total_Importe_Venta)) & " " & Doc_07_FF50_00000001.Moneda_Descripcion
        Doc_07_FF50_00000001 = LlenarLeyendas(Doc_07_FF50_00000001)

        CasosHomologacion.Add(Doc_07_FF50_00000001)

        '-------    Nota de débito de factura 1
        Dim Doc_08_FF50_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF50", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "6", .Cliente_Documento_Numero = Cliente_Factura_RUC, .Cliente_RazonSocial_Nombre = Cliente_Factura_RazonSocial, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "USD", .Moneda_Descripcion = "DOLARES", .Moneda_Simbolo = "US$.",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_FF50_00000001.Homogacion_Grupo_Codigo = "7"
        Doc_08_FF50_00000001.Homogacion_Grupo_Descripcion = "Facturas y Notas"
        Doc_08_FF50_00000001.Homogacion_Grupo_Operacion = "Operaciones con otro tipo de moneda"

        Doc_08_FF50_00000001 = NotaDebitoDeFactura(Doc_08_FF50_00000001, Doc_01_FF50_00000001)
        Doc_08_FF50_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_FF50_00000001.Total_Importe_Venta)) & " " & Doc_08_FF50_00000001.Moneda_Descripcion
        Doc_08_FF50_00000001 = LlenarLeyendas(Doc_08_FF50_00000001)

        CasosHomologacion.Add(Doc_08_FF50_00000001)

        '-------GRUPO 08 : BB11 Ventas Gravadas IGV
        '-------    Boleta de Venta 1 con 4 ítems
        Dim Doc_03_BB11_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB11", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB11_00000001.Homogacion_Grupo_Codigo = "8"
        Doc_03_BB11_00000001.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB11_00000001.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_03_BB11_00000001 = LlenarDetalles_Afecto(Doc_03_BB11_00000001, 4)
        Doc_03_BB11_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB11_00000001.Total_Importe_Venta)) & " " & Doc_03_BB11_00000001.Moneda_Descripcion
        Doc_03_BB11_00000001 = LlenarLeyendas(Doc_03_BB11_00000001)

        CasosHomologacion.Add(Doc_03_BB11_00000001)

        '-------    Boleta de Venta 2 con 7 ítems
        Dim Doc_03_BB11_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                          .TipoDocumento_Serie = "BB11", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                          .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                          .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                          .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB11_00000002.Homogacion_Grupo_Codigo = "1"
        Doc_03_BB11_00000002.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB11_00000002.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_03_BB11_00000002 = LlenarDetalles_Afecto(Doc_03_BB11_00000002, 7)
        Doc_03_BB11_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB11_00000002.Total_Importe_Venta)) & " " & Doc_03_BB11_00000002.Moneda_Descripcion
        Doc_03_BB11_00000002 = LlenarLeyendas(Doc_03_BB11_00000002)

        CasosHomologacion.Add(Doc_03_BB11_00000002)

        '-------    Boleta de Venta 3 con 5 ítems
        Dim Doc_03_BB11_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                          .TipoDocumento_Serie = "BB11", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                          .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                          .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                         .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB11_00000003.Homogacion_Grupo_Codigo = "1"
        Doc_03_BB11_00000003.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB11_00000003.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_03_BB11_00000003 = LlenarDetalles_Afecto(Doc_03_BB11_00000003, 5)
        Doc_03_BB11_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB11_00000003.Total_Importe_Venta)) & " " & Doc_03_BB11_00000003.Moneda_Descripcion
        Doc_03_BB11_00000003 = LlenarLeyendas(Doc_03_BB11_00000003)

        CasosHomologacion.Add(Doc_03_BB11_00000003)

        '-------    Boleta de Venta 4 con 3 ítems
        Dim Doc_03_BB11_00000004 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB11", .TipoDocumento_Numero = "00000004", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB11_00000004.Homogacion_Grupo_Codigo = "1"
        Doc_03_BB11_00000004.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB11_00000004.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_03_BB11_00000004 = LlenarDetalles_Afecto(Doc_03_BB11_00000004, 3)
        Doc_03_BB11_00000004.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB11_00000004.Total_Importe_Venta)) & " " & Doc_03_BB11_00000004.Moneda_Descripcion
        Doc_03_BB11_00000004 = LlenarLeyendas(Doc_03_BB11_00000004)

        CasosHomologacion.Add(Doc_03_BB11_00000004)

        '-------    Boleta de Venta 5 con 2 ítems
        Dim Doc_03_BB11_00000005 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB11", .TipoDocumento_Numero = "00000005", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB11_00000005.Homogacion_Grupo_Codigo = "1"
        Doc_03_BB11_00000005.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB11_00000005.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_03_BB11_00000005 = LlenarDetalles_Afecto(Doc_03_BB11_00000005, 2)
        Doc_03_BB11_00000005.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB11_00000005.Total_Importe_Venta)) & " " & Doc_03_BB11_00000005.Moneda_Descripcion
        Doc_03_BB11_00000005 = LlenarLeyendas(Doc_03_BB11_00000005)

        CasosHomologacion.Add(Doc_03_BB11_00000005)

        '-------    Nota de crédito de Boleta de Venta 2
        Dim Doc_07_BB11_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB11", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_BB11_00000001.Homogacion_Grupo_Codigo = "1"
        Doc_07_BB11_00000001.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_07_BB11_00000001.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_07_BB11_00000001 = NotaCreditoDeFactura(Doc_07_BB11_00000001, Doc_03_BB11_00000002)
        Doc_07_BB11_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_BB11_00000001.Total_Importe_Venta)) & " " & Doc_07_BB11_00000001.Moneda_Descripcion
        Doc_07_BB11_00000001 = LlenarLeyendas(Doc_07_BB11_00000001)

        CasosHomologacion.Add(Doc_07_BB11_00000001)

        '-------    Nota de crédito de Boleta de Venta 3
        Dim Doc_07_BB11_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB11", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_BB11_00000002.Homogacion_Grupo_Codigo = "1"
        Doc_07_BB11_00000002.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_07_BB11_00000002.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_07_BB11_00000002 = NotaCreditoDeFactura(Doc_07_BB11_00000002, Doc_03_BB11_00000003)
        Doc_07_BB11_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_BB11_00000002.Total_Importe_Venta)) & " " & Doc_07_BB11_00000002.Moneda_Descripcion
        Doc_07_BB11_00000002 = LlenarLeyendas(Doc_07_BB11_00000002)

        CasosHomologacion.Add(Doc_07_BB11_00000002)

        '-------    Nota de crédito de Boleta de Venta 4
        Dim Doc_07_BB11_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB11", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_BB11_00000003.Homogacion_Grupo_Codigo = "1"
        Doc_07_BB11_00000003.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_07_BB11_00000003.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_07_BB11_00000003 = NotaCreditoDeFactura(Doc_07_BB11_00000003, Doc_03_BB11_00000004)
        Doc_07_BB11_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_BB11_00000003.Total_Importe_Venta)) & " " & Doc_07_BB11_00000003.Moneda_Descripcion
        Doc_07_BB11_00000003 = LlenarLeyendas(Doc_07_BB11_00000003)

        CasosHomologacion.Add(Doc_07_BB11_00000003)

        '-------   Nota de débito de Boleta de Venta 2
        Dim Doc_08_BB11_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB11", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_BB11_00000001.Homogacion_Grupo_Codigo = "1"
        Doc_08_BB11_00000001.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_08_BB11_00000001.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_08_BB11_00000001 = NotaDebitoDeFactura(Doc_08_BB11_00000001, Doc_03_BB11_00000002)
        Doc_08_BB11_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_BB11_00000001.Total_Importe_Venta)) & " " & Doc_08_BB11_00000001.Moneda_Descripcion
        Doc_08_BB11_00000001 = LlenarLeyendas(Doc_08_BB11_00000001)

        CasosHomologacion.Add(Doc_08_BB11_00000001)

        '-------    Nota de débito de Boleta de Venta 3
        Dim Doc_08_BB11_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB11", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_BB11_00000002.Homogacion_Grupo_Codigo = "1"
        Doc_08_BB11_00000002.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_08_BB11_00000002.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_08_BB11_00000002 = NotaDebitoDeFactura(Doc_08_BB11_00000002, Doc_03_BB11_00000003)
        Doc_08_BB11_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_BB11_00000002.Total_Importe_Venta)) & " " & Doc_08_BB11_00000002.Moneda_Descripcion
        Doc_08_BB11_00000002 = LlenarLeyendas(Doc_08_BB11_00000002)

        CasosHomologacion.Add(Doc_08_BB11_00000002)

        '-------    Nota de débito de Boleta de Venta 4
        Dim Doc_08_BB11_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB11", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_BB11_00000003.Homogacion_Grupo_Codigo = "1"
        Doc_08_BB11_00000003.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_08_BB11_00000003.Homogacion_Grupo_Operacion = "Ventas Gravadas IGV"

        Doc_08_BB11_00000003 = NotaDebitoDeFactura(Doc_08_BB11_00000003, Doc_03_BB11_00000004)
        Doc_08_BB11_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_BB11_00000003.Total_Importe_Venta)) & " " & Doc_08_BB11_00000003.Moneda_Descripcion
        Doc_08_BB11_00000003 = LlenarLeyendas(Doc_08_BB11_00000003)

        CasosHomologacion.Add(Doc_08_BB11_00000003)

        '-------GRUPO 09 : BB12 Ventas inafectas y/o exoneradas
        '-------    Boleta de Venta 1 con 2 ítems
        Dim Doc_03_BB12_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB12", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB12_00000001.Homogacion_Grupo_Codigo = "9"
        Doc_03_BB12_00000001.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB12_00000001.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_03_BB12_00000001 = LlenarDetalles_InafectaExonerada(Doc_03_BB12_00000001, 2, "20") '20 Exonerado - Operacion Onerosa '30 Inafecto - Operacion Onerosa
        Doc_03_BB12_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB12_00000001.Total_Importe_Venta)) & " " & Doc_03_BB12_00000001.Moneda_Descripcion
        Doc_03_BB12_00000001 = LlenarLeyendas(Doc_03_BB12_00000001)

        CasosHomologacion.Add(Doc_03_BB12_00000001)

        '-------    Boleta de Venta 2 con 4 ítems
        Dim Doc_03_BB12_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB12", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB12_00000002.Homogacion_Grupo_Codigo = "9"
        Doc_03_BB12_00000002.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB12_00000002.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_03_BB12_00000002 = LlenarDetalles_InafectaExonerada(Doc_03_BB12_00000002, 4, "20") '20 Exonerado - Operacion Onerosa '30 Inafecto - Operacion Onerosa
        Doc_03_BB12_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB12_00000002.Total_Importe_Venta)) & " " & Doc_03_BB12_00000002.Moneda_Descripcion
        Doc_03_BB12_00000002 = LlenarLeyendas(Doc_03_BB12_00000002)

        CasosHomologacion.Add(Doc_03_BB12_00000002)

        '-------    Boleta de Venta 3 con 7 ítems
        Dim Doc_03_BB12_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB12", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB12_00000003.Homogacion_Grupo_Codigo = "9"
        Doc_03_BB12_00000003.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB12_00000003.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_03_BB12_00000003 = LlenarDetalles_InafectaExonerada(Doc_03_BB12_00000003, 7, "30") '20 Exonerado - Operacion Onerosa '30 Inafecto - Operacion Onerosa
        Doc_03_BB12_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB12_00000003.Total_Importe_Venta)) & " " & Doc_03_BB12_00000003.Moneda_Descripcion
        Doc_03_BB12_00000003 = LlenarLeyendas(Doc_03_BB12_00000003)

        CasosHomologacion.Add(Doc_03_BB12_00000003)

        '-------    Boleta de Venta 4 con 5 ítems
        Dim Doc_03_BB12_00000004 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB12", .TipoDocumento_Numero = "00000004", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB12_00000004.Homogacion_Grupo_Codigo = "9"
        Doc_03_BB12_00000004.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB12_00000004.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_03_BB12_00000004 = LlenarDetalles_InafectaExonerada(Doc_03_BB12_00000004, 5, "20") '20 Exonerado - Operacion Onerosa '30 Inafecto - Operacion Onerosa
        Doc_03_BB12_00000004.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB12_00000004.Total_Importe_Venta)) & " " & Doc_03_BB12_00000004.Moneda_Descripcion
        Doc_03_BB12_00000004 = LlenarLeyendas(Doc_03_BB12_00000004)

        CasosHomologacion.Add(Doc_03_BB12_00000004)

        '-------    Boleta de Venta 5 con 1 ítem
        Dim Doc_03_BB12_00000005 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB12", .TipoDocumento_Numero = "00000005", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB12_00000005.Homogacion_Grupo_Codigo = "9"
        Doc_03_BB12_00000005.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB12_00000005.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_03_BB12_00000005 = LlenarDetalles_InafectaExonerada(Doc_03_BB12_00000005, 6, "20") '20 Exonerado - Operacion Onerosa '30 Inafecto - Operacion Onerosa
        Doc_03_BB12_00000005.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB12_00000005.Total_Importe_Venta)) & " " & Doc_03_BB12_00000005.Moneda_Descripcion
        Doc_03_BB12_00000005 = LlenarLeyendas(Doc_03_BB12_00000005)

        CasosHomologacion.Add(Doc_03_BB12_00000005)

        '-------    Nota de crédito de Boleta de Venta 1
        Dim Doc_07_BB12_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB12", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_BB12_00000001.Homogacion_Grupo_Codigo = "9"
        Doc_07_BB12_00000001.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_07_BB12_00000001.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_07_BB12_00000001 = NotaCreditoDeFactura(Doc_07_BB12_00000001, Doc_03_BB12_00000001)
        Doc_07_BB12_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_BB12_00000001.Total_Importe_Venta)) & " " & Doc_07_BB12_00000001.Moneda_Descripcion
        Doc_07_BB12_00000001 = LlenarLeyendas(Doc_07_BB12_00000001)

        CasosHomologacion.Add(Doc_07_BB12_00000001)

        '-------    Nota de crédito de Boleta de Venta 4
        Dim Doc_07_BB12_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB12", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_BB12_00000002.Homogacion_Grupo_Codigo = "9"
        Doc_07_BB12_00000002.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_07_BB12_00000002.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_07_BB12_00000002 = NotaCreditoDeFactura(Doc_07_BB12_00000002, Doc_03_BB12_00000004)
        Doc_07_BB12_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_BB12_00000002.Total_Importe_Venta)) & " " & Doc_07_BB12_00000002.Moneda_Descripcion
        Doc_07_BB12_00000002 = LlenarLeyendas(Doc_07_BB12_00000002)

        CasosHomologacion.Add(Doc_07_BB12_00000002)

        '-------    Nota de crédito de Boleta de Venta 5
        Dim Doc_07_BB12_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB12", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_BB12_00000003.Homogacion_Grupo_Codigo = "1"
        Doc_07_BB12_00000003.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_07_BB12_00000003.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_07_BB12_00000003 = NotaCreditoDeFactura(Doc_07_BB12_00000003, Doc_03_BB12_00000005)
        Doc_07_BB12_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_BB12_00000003.Total_Importe_Venta)) & " " & Doc_07_BB12_00000003.Moneda_Descripcion
        Doc_07_BB12_00000003 = LlenarLeyendas(Doc_07_BB12_00000003)

        CasosHomologacion.Add(Doc_07_BB12_00000003)

        '-------    Nota de débito de Boleta de Venta 1
        Dim Doc_08_BB12_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB12", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_BB12_00000001.Homogacion_Grupo_Codigo = "9"
        Doc_08_BB12_00000001.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_08_BB12_00000001.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_08_BB12_00000001 = NotaDebitoDeFactura(Doc_08_BB12_00000001, Doc_03_BB12_00000001)
        Doc_08_BB12_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_BB12_00000001.Total_Importe_Venta)) & " " & Doc_08_BB12_00000001.Moneda_Descripcion
        Doc_08_BB12_00000001 = LlenarLeyendas(Doc_08_BB12_00000001)

        CasosHomologacion.Add(Doc_08_BB12_00000001)

        '-------    Nota de débito de Boleta de Venta 4
        Dim Doc_08_BB12_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB12", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_BB12_00000002.Homogacion_Grupo_Codigo = "9"
        Doc_08_BB12_00000002.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_08_BB12_00000002.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_08_BB12_00000002 = NotaDebitoDeFactura(Doc_08_BB12_00000002, Doc_03_BB12_00000004)
        Doc_08_BB12_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_BB12_00000002.Total_Importe_Venta)) & " " & Doc_08_BB12_00000002.Moneda_Descripcion
        Doc_08_BB12_00000002 = LlenarLeyendas(Doc_08_BB12_00000002)

        CasosHomologacion.Add(Doc_08_BB12_00000002)

        '-------    Nota de débito de Boleta de Venta 5
        Dim Doc_08_BB12_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB12", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_BB12_00000003.Homogacion_Grupo_Codigo = "9"
        Doc_08_BB12_00000003.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_08_BB12_00000003.Homogacion_Grupo_Operacion = "Ventas inafectas y/o exoneradas"

        Doc_08_BB12_00000003 = NotaDebitoDeFactura(Doc_08_BB12_00000003, Doc_03_BB12_00000005)
        Doc_08_BB12_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_BB12_00000003.Total_Importe_Venta)) & " " & Doc_08_BB12_00000003.Moneda_Descripcion
        Doc_08_BB12_00000003 = LlenarLeyendas(Doc_08_BB12_00000003)

        CasosHomologacion.Add(Doc_08_BB12_00000003)

        '-------GRUPO 10 : BB13 Ventas gratuitas
        '-------    Boleta de Venta 1 con 7 ítems
        Dim Doc_03_BB13_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB13", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB13_00000001.Homogacion_Grupo_Codigo = "10"
        Doc_03_BB13_00000001.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB13_00000001.Homogacion_Grupo_Operacion = "Ventas gratuitas"

        Doc_03_BB13_00000001 = LlenarDetalles_Gratuitas(Doc_03_BB13_00000001, 7)
        Doc_03_BB13_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB13_00000001.Total_Importe_Venta)) & " " & Doc_03_BB13_00000001.Moneda_Descripcion
        Doc_03_BB13_00000001 = LlenarLeyendas(Doc_03_BB13_00000001)

        CasosHomologacion.Add(Doc_03_BB13_00000001)

        '-------    Boleta de Venta 2 con 2 ítems
        Dim Doc_03_BB13_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB13", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB13_00000002.Homogacion_Grupo_Codigo = "10"
        Doc_03_BB13_00000002.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB13_00000002.Homogacion_Grupo_Operacion = "Ventas gratuitas"

        Doc_03_BB13_00000002 = LlenarDetalles_Gratuitas(Doc_03_BB13_00000002, 2)
        Doc_03_BB13_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB13_00000002.Total_Importe_Venta)) & " " & Doc_03_BB13_00000002.Moneda_Descripcion
        Doc_03_BB13_00000002 = LlenarLeyendas(Doc_03_BB13_00000002)

        CasosHomologacion.Add(Doc_03_BB13_00000002)

        '-------    Boleta de Venta 3 con 5 ítems
        Dim Doc_03_BB13_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB13", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB13_00000003.Homogacion_Grupo_Codigo = "10"
        Doc_03_BB13_00000003.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB13_00000003.Homogacion_Grupo_Operacion = "Ventas gratuitas"

        Doc_03_BB13_00000003 = LlenarDetalles_Gratuitas(Doc_03_BB13_00000003, 5)
        Doc_03_BB13_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB13_00000003.Total_Importe_Venta)) & " " & Doc_03_BB13_00000003.Moneda_Descripcion
        Doc_03_BB13_00000003 = LlenarLeyendas(Doc_03_BB13_00000003)

        CasosHomologacion.Add(Doc_03_BB13_00000003)

        '-------    Boleta de Venta 4 con 4 ítems
        Dim Doc_03_BB13_00000004 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB13", .TipoDocumento_Numero = "00000004", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB13_00000004.Homogacion_Grupo_Codigo = "10"
        Doc_03_BB13_00000004.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB13_00000004.Homogacion_Grupo_Operacion = "Ventas gratuitas"

        Doc_03_BB13_00000004 = LlenarDetalles_Gratuitas(Doc_03_BB13_00000004, 4)
        Doc_03_BB13_00000004.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB13_00000004.Total_Importe_Venta)) & " " & Doc_03_BB13_00000004.Moneda_Descripcion
        Doc_03_BB13_00000004 = LlenarLeyendas(Doc_03_BB13_00000004)

        CasosHomologacion.Add(Doc_03_BB13_00000004)

        '-------    Boleta de Venta 5 con 9 ítems
        Dim Doc_03_BB13_00000005 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB13", .TipoDocumento_Numero = "00000005", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB13_00000005.Homogacion_Grupo_Codigo = "10"
        Doc_03_BB13_00000005.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB13_00000005.Homogacion_Grupo_Operacion = "Ventas gratuitas"

        Doc_03_BB13_00000005 = LlenarDetalles_Gratuitas(Doc_03_BB13_00000005, 9)
        Doc_03_BB13_00000005.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB13_00000005.Total_Importe_Venta)) & " " & Doc_03_BB13_00000005.Moneda_Descripcion
        Doc_03_BB13_00000005 = LlenarLeyendas(Doc_03_BB13_00000005)

        CasosHomologacion.Add(Doc_03_BB13_00000005)

        '-------GRUPO 11 : BB14 Ventas con descuento global
        '-------    Boleta de Venta 1 con 10 ítems
        Dim Doc_03_BB14_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB14", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB14_00000001.Homogacion_Grupo_Codigo = "11"
        Doc_03_BB14_00000001.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB14_00000001.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_03_BB14_00000001 = LlenarDetalles_AfectoConDescuentos(Doc_03_BB14_00000001, 10)
        Doc_03_BB14_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB14_00000001.Total_Importe_Venta)) & " " & Doc_03_BB14_00000001.Moneda_Descripcion
        Doc_03_BB14_00000001 = LlenarLeyendas(Doc_03_BB14_00000001)

        CasosHomologacion.Add(Doc_03_BB14_00000001)

        '-------    Boleta de Venta 2 con 7 ítems
        Dim Doc_03_BB14_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB14", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB14_00000002.Homogacion_Grupo_Codigo = "11"
        Doc_03_BB14_00000002.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB14_00000002.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_03_BB14_00000002 = LlenarDetalles_AfectoConDescuentos(Doc_03_BB14_00000002, 7)
        Doc_03_BB14_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB14_00000002.Total_Importe_Venta)) & " " & Doc_03_BB14_00000002.Moneda_Descripcion
        Doc_03_BB14_00000002 = LlenarLeyendas(Doc_03_BB14_00000002)

        CasosHomologacion.Add(Doc_03_BB14_00000002)

        '-------    Boleta de Venta 3 con 6 ítems
        Dim Doc_03_BB14_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB14", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB14_00000003.Homogacion_Grupo_Codigo = "11"
        Doc_03_BB14_00000003.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB14_00000003.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_03_BB14_00000003 = LlenarDetalles_AfectoConDescuentos(Doc_03_BB14_00000003, 4)
        Doc_03_BB14_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB14_00000003.Total_Importe_Venta)) & " " & Doc_03_BB14_00000003.Moneda_Descripcion
        Doc_03_BB14_00000003 = LlenarLeyendas(Doc_03_BB14_00000003)

        CasosHomologacion.Add(Doc_03_BB14_00000003)

        '-------    Boleta de Venta 4 con 9 ítems
        Dim Doc_03_BB14_00000004 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB14", .TipoDocumento_Numero = "00000004", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB14_00000004.Homogacion_Grupo_Codigo = "11"
        Doc_03_BB14_00000004.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB14_00000004.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_03_BB14_00000004 = LlenarDetalles_AfectoConDescuentos(Doc_03_BB14_00000004, 9)
        Doc_03_BB14_00000004.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB14_00000004.Total_Importe_Venta)) & " " & Doc_03_BB14_00000004.Moneda_Descripcion
        Doc_03_BB14_00000004 = LlenarLeyendas(Doc_03_BB14_00000004)

        CasosHomologacion.Add(Doc_03_BB14_00000004)

        '-------    Boleta de Venta 5 con 4 ítems
        Dim Doc_03_BB14_00000005 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB14", .TipoDocumento_Numero = "00000005", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB14_00000005.Homogacion_Grupo_Codigo = "11"
        Doc_03_BB14_00000005.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB14_00000005.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_03_BB14_00000005 = LlenarDetalles_AfectoConDescuentos(Doc_03_BB14_00000005, 4)
        Doc_03_BB14_00000005.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB14_00000005.Total_Importe_Venta)) & " " & Doc_03_BB14_00000005.Moneda_Descripcion
        Doc_03_BB14_00000005 = LlenarLeyendas(Doc_03_BB14_00000005)

        CasosHomologacion.Add(Doc_03_BB14_00000005)

        '-------    Nota de crédito de Boleta de Venta 1
        Dim Doc_07_BB14_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB14", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_BB14_00000001.Homogacion_Grupo_Codigo = "4"
        Doc_07_BB14_00000001.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_07_BB14_00000001.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_07_BB14_00000001 = NotaCreditoDeFactura(Doc_07_BB14_00000001, Doc_03_BB14_00000001)
        Doc_07_BB14_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_BB14_00000001.Total_Importe_Venta)) & " " & Doc_07_BB14_00000001.Moneda_Descripcion
        Doc_07_BB14_00000001 = LlenarLeyendas(Doc_07_BB14_00000001)

        CasosHomologacion.Add(Doc_07_BB14_00000001)

        '-------    Nota de crédito de Boleta de Venta 2
        Dim Doc_07_BB14_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB14", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_BB14_00000002.Homogacion_Grupo_Codigo = "4"
        Doc_07_BB14_00000002.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_07_BB14_00000002.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_07_BB14_00000002 = NotaCreditoDeFactura(Doc_07_BB14_00000002, Doc_03_BB14_00000002)
        Doc_07_BB14_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_BB14_00000002.Total_Importe_Venta)) & " " & Doc_07_BB14_00000002.Moneda_Descripcion
        Doc_07_BB14_00000002 = LlenarLeyendas(Doc_07_BB14_00000002)

        CasosHomologacion.Add(Doc_07_BB14_00000002)

        '-------    Nota de crédito de Boleta de Venta 4
        Dim Doc_07_BB14_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB14", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_BB14_00000003.Homogacion_Grupo_Codigo = "4"
        Doc_07_BB14_00000003.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_07_BB14_00000003.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_07_BB14_00000003 = NotaCreditoDeFactura(Doc_07_BB14_00000003, Doc_03_BB14_00000004)
        Doc_07_BB14_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_BB14_00000003.Total_Importe_Venta)) & " " & Doc_07_BB14_00000003.Moneda_Descripcion
        Doc_07_BB14_00000003 = LlenarLeyendas(Doc_07_BB14_00000003)

        CasosHomologacion.Add(Doc_07_BB14_00000003)

        '-------    Nota de débito de Boleta de Venta 1
        Dim Doc_08_BB14_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB14", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_BB14_00000001.Homogacion_Grupo_Codigo = "4"
        Doc_08_BB14_00000001.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_08_BB14_00000001.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_08_BB14_00000001 = NotaDebitoDeFactura(Doc_08_BB14_00000001, Doc_03_BB14_00000001)
        Doc_08_BB14_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_BB14_00000001.Total_Importe_Venta)) & " " & Doc_08_BB14_00000001.Moneda_Descripcion
        Doc_08_BB14_00000001 = LlenarLeyendas(Doc_08_BB14_00000001)

        CasosHomologacion.Add(Doc_08_BB14_00000001)

        '-------    Nota de débito de Boleta de Venta 2
        Dim Doc_08_BB14_00000002 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB14", .TipoDocumento_Numero = "00000002", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_BB14_00000002.Homogacion_Grupo_Codigo = "4"
        Doc_08_BB14_00000002.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_08_BB14_00000002.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_08_BB14_00000002 = NotaDebitoDeFactura(Doc_08_BB14_00000002, Doc_03_BB14_00000002)
        Doc_08_BB14_00000002.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_BB14_00000002.Total_Importe_Venta)) & " " & Doc_08_BB14_00000002.Moneda_Descripcion
        Doc_08_BB14_00000002 = LlenarLeyendas(Doc_08_BB14_00000002)

        CasosHomologacion.Add(Doc_08_BB14_00000002)

        '-------    Nota de débito de Boleta de Venta 4
        Dim Doc_08_BB14_00000003 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "BB14", .TipoDocumento_Numero = "00000003", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "PEN", .Moneda_Descripcion = "SOLES", .Moneda_Simbolo = "S/",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_BB14_00000003.Homogacion_Grupo_Codigo = "4"
        Doc_08_BB14_00000003.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_08_BB14_00000003.Homogacion_Grupo_Operacion = "Ventas con descuento global"

        Doc_08_BB14_00000003 = NotaDebitoDeFactura(Doc_08_BB14_00000003, Doc_03_BB14_00000004)
        Doc_08_BB14_00000003.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_BB14_00000003.Total_Importe_Venta)) & " " & Doc_08_BB14_00000003.Moneda_Descripcion
        Doc_08_BB14_00000003 = LlenarLeyendas(Doc_08_BB14_00000003)

        CasosHomologacion.Add(Doc_08_BB14_00000003)

        '-------GRUPO 12 : BB50 Operaciones con otro tipo de moneda
        '-------    Boleta de Venta 1 con 3 ítems
        Dim Doc_03_BB50_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "03", .TipoDocumento_Descripcion = "BOLETA DE VENTA ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF50", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "USD", .Moneda_Descripcion = "DOLARES", .Moneda_Simbolo = "US$.",
                                                  .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_03_BB50_00000001.Homogacion_Grupo_Codigo = "12"
        Doc_03_BB50_00000001.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_03_BB50_00000001.Homogacion_Grupo_Operacion = "Operaciones con otro tipo de moneda"

        Doc_03_BB50_00000001 = LlenarDetalles_GravadasConISC(Doc_03_BB50_00000001, 3)
        Doc_03_BB50_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_03_BB50_00000001.Total_Importe_Venta)) & " " & Doc_03_BB50_00000001.Moneda_Descripcion
        Doc_03_BB50_00000001 = LlenarLeyendas(Doc_03_BB50_00000001)

        CasosHomologacion.Add(Doc_03_BB50_00000001)

        '-------    Nota de crédito de Boleta de Venta 1
        Dim Doc_07_BB50_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "07", .TipoDocumento_Descripcion = "NOTA DE CREDITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF50", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "USD", .Moneda_Descripcion = "DOLARES", .Moneda_Simbolo = "US$.",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_07_BB50_00000001.Homogacion_Grupo_Codigo = "12"
        Doc_07_BB50_00000001.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_07_BB50_00000001.Homogacion_Grupo_Operacion = "Operaciones con otro tipo de moneda"

        Doc_07_BB50_00000001 = NotaCreditoDeFactura(Doc_07_BB50_00000001, Doc_03_BB50_00000001)
        Doc_07_BB50_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_07_BB50_00000001.Total_Importe_Venta)) & " " & Doc_07_BB50_00000001.Moneda_Descripcion
        Doc_07_BB50_00000001 = LlenarLeyendas(Doc_07_BB50_00000001)

        CasosHomologacion.Add(Doc_07_BB50_00000001)

        '-------    Nota de débito de Boleta de Venta 1
        Dim Doc_08_BB50_00000001 As New DocumentoElectronico With {.Fecha_Emision = Now, .TipoDocumento_Codigo = "08", .TipoDocumento_Descripcion = "NOTA DE DEBITO ELECTRONICA",
                                                  .TipoDocumento_Serie = "FF50", .TipoDocumento_Numero = "00000001", .Emisor_ApellidosNombres_RazonSocial = TxtEmisorRazonSocial.Text, .Emisor_Documento_Tipo = "6", .Emisor_Documento_Numero = TxtEmisorRUC.Text, .Emisor_Direccion_Ubigeo = TxtEmisorUbigeo.Text, .Emisor_Direccion_Calle = TxtEmisorDirCalle.Text, .Emisor_Direccion_Urbanizacion = TxtEmisorDirUrbanizacion.Text, .Emisor_Direccion_Departamento = TxtEmisorDirDepartamento.Text, .Emisor_Direccion_Provincia = TxtEmisorDirProvincia.Text, .Emisor_Direccion_Distrito = TxtEmisorDirDistrito.Text, .Emisor_Direccion_PaisCodigo = "PE",
                                                  .Cliente_Documento_Tipo = "1", .Cliente_Documento_Numero = Cliente_Boleta_DNI, .Cliente_RazonSocial_Nombre = Cliente_Boleta_Nombres, .Cliente_Direccion = TxtClienteDireccion.Text, .Cliente_Correo = TxtClienteCorreo.Text,
                                                  .Moneda_Codigo = "USD", .Moneda_Descripcion = "DOLARES", .Moneda_Simbolo = "US$.",
                                                   .Total_Importe_Percepcion = 0, .Impresion_Modelo_Codigo = "001"}

        Doc_08_BB50_00000001.Homogacion_Grupo_Codigo = "12"
        Doc_08_BB50_00000001.Homogacion_Grupo_Descripcion = "Boletas y Notas"
        Doc_08_BB50_00000001.Homogacion_Grupo_Operacion = "Operaciones con otro tipo de moneda"

        Doc_08_BB50_00000001 = NotaDebitoDeFactura(Doc_08_BB50_00000001, Doc_03_BB50_00000001)
        Doc_08_BB50_00000001.Total_Importe_Venta_Texto = "SON " & NroEnLetras(Math.Truncate(Doc_08_BB50_00000001.Total_Importe_Venta)) & " " & Doc_08_BB50_00000001.Moneda_Descripcion
        Doc_08_BB50_00000001 = LlenarLeyendas(Doc_08_BB50_00000001)

        CasosHomologacion.Add(Doc_08_BB50_00000001)

        GridControl1.DataSource = CasosHomologacion

    End Sub
    Public Function NroEnLetras(ByVal curNumero As Double, Optional ByVal blnO_Final As Boolean = True) As String
        Dim dblCentavos As Double
        Dim lngContUnit As Long
        Dim lngContDec As Long
        Dim lngContCent As Long
        Dim lngContMil As Long
        Dim lngContMillon As Long
        Dim strNumLetras As String = ""
        Dim strNumero(20) As String
        Dim strDecenas(10) As String
        Dim strCentenas(9) As String
        Dim blnNegativo As Boolean
        Dim blnPlural As Boolean


        If Int(curNumero) = 0.0# Then
            strNumLetras = "CERO"
        End If

        strNumero(0) = ""
        strNumero(1) = "UN"
        strNumero(2) = "DOS"
        strNumero(3) = "TRES"
        strNumero(4) = "CUATRO"
        strNumero(5) = "CINCO"
        strNumero(6) = "SEIS"
        strNumero(7) = "SIETE"
        strNumero(8) = "OCHO"
        strNumero(9) = "NUEVE"
        strNumero(10) = "DIEZ"
        strNumero(11) = "ONCE"
        strNumero(12) = "DOCE"
        strNumero(13) = "TRECE"
        strNumero(14) = "CATORCE"
        strNumero(15) = "QUINCE"
        strNumero(16) = "DIECISEIS"
        strNumero(17) = "DIECISIETE"
        strNumero(18) = "DIECIOCHO"
        strNumero(19) = "DIECINUEVE"
        strNumero(20) = "VEINTE"

        strDecenas(0) = ""
        strDecenas(1) = ""
        strDecenas(2) = "VEINTI"
        strDecenas(3) = "TREINTA"
        strDecenas(4) = "CUARENTA"
        strDecenas(5) = "CINCUENTA"
        strDecenas(6) = "SESENTA"
        strDecenas(7) = "SETENTA"
        strDecenas(8) = "OCHENTA"
        strDecenas(9) = "NOVENTA"
        strDecenas(10) = "CIEN"

        strCentenas(0) = ""
        strCentenas(1) = "CIENTO"
        strCentenas(2) = "DOSCIENTOS"
        strCentenas(3) = "TRESCIENTOS"
        strCentenas(4) = "CUATROCIENTOS"
        strCentenas(5) = "QUINIENTOS"
        strCentenas(6) = "SEISCIENTOS"
        strCentenas(7) = "SETECIENTOS"
        strCentenas(8) = "OCHOCIENTOS"
        strCentenas(9) = "NOVECIENTOS"

        If curNumero < 0.0# Then
            blnNegativo = True
            curNumero = Math.Abs(curNumero)
        End If

        If Int(curNumero) <> curNumero Then
            dblCentavos = Math.Abs(curNumero - Int(curNumero))
            curNumero = Int(curNumero)
        End If

        Do While curNumero >= 1000000.0#
            lngContMillon = lngContMillon + 1
            curNumero = curNumero - 1000000.0#
        Loop

        Do While curNumero >= 1000.0#
            lngContMil = lngContMil + 1
            curNumero = curNumero - 1000.0#
        Loop

        Do While curNumero >= 100.0#
            lngContCent = lngContCent + 1
            curNumero = curNumero - 100.0#
        Loop


        If Not (curNumero > 10.0# And curNumero <= 20.0#) Then
            Do While curNumero >= 10.0#
                lngContDec = lngContDec + 1
                curNumero = curNumero - 10.0#
            Loop
        End If


        Do While curNumero >= 1.0#
            lngContUnit = lngContUnit + 1
            curNumero = curNumero - 1.0#
        Loop

        If lngContMillon > 0 Then
            If lngContMillon >= 1 Then   'si el número es >1000000 usa recursividad
                strNumLetras = NroEnLetras(lngContMillon, False)
                If Not blnPlural Then blnPlural = (lngContMillon > 1)
                lngContMillon = 0
            End If
            strNumLetras = Trim(strNumLetras) & strNumero(lngContMillon) & " MILLON" & IIf(blnPlural, "ES ", " ")
        End If

        If lngContMil > 0 Then
            If lngContMil >= 1 Then   'si el número es >100000 usa recursividad
                strNumLetras = strNumLetras & NroEnLetras(lngContMil, False)
                lngContMil = 0
            End If
            strNumLetras = Trim(strNumLetras) & strNumero(lngContMil) & " MIL "
        End If

        If lngContCent > 0 Then
            If lngContCent = 1 And lngContDec = 0 And lngContUnit = 0.0# Then
                strNumLetras = strNumLetras & "CIEN"
            Else
                strNumLetras = strNumLetras & strCentenas(lngContCent) & " "
            End If
        End If

        If lngContDec >= 1 Then
            If lngContDec = 1 Then
                strNumLetras = strNumLetras & strNumero(10)
            ElseIf lngContDec = 2 And lngContUnit = 0.0# Then
                strNumLetras = strNumLetras & strNumero(20)
            Else
                strNumLetras = strNumLetras & strDecenas(lngContDec)
            End If
            If lngContDec >= 3 And lngContUnit > 0.0# Then
                strNumLetras = strNumLetras & " Y "
            End If
        End If

        If lngContUnit >= 0 Then
            If Not blnO_Final Then
                If lngContUnit >= 0.0# And lngContUnit <= 20.0# Then
                    strNumLetras = strNumLetras & strNumero(lngContUnit)
                    If lngContUnit = 1.0# And blnO_Final Then
                        strNumLetras = strNumLetras & "O"
                    End If
                    NroEnLetras = strNumLetras
                    Exit Function
                End If
            Else
                If lngContUnit >= 0.0# And lngContUnit <= 20.0# Then
                    strNumLetras = strNumLetras & strNumero(lngContUnit)
                    If lngContUnit = 1.0# And blnO_Final Then
                        strNumLetras = strNumLetras & "O"
                    End If
                    If dblCentavos >= 0.0# Then
                        strNumLetras = strNumLetras & " CON " + Format$(CInt(dblCentavos * 100.0#), "00") & "/100"
                    End If
                    NroEnLetras = strNumLetras
                    Exit Function
                End If
            End If
        End If
        If lngContUnit >= 0.0# Then
            If lngContUnit > 0 Then
                strNumLetras = strNumLetras & NroEnLetras(lngContUnit, True)
            End If
        End If


        NroEnLetras = IIf(blnNegativo, "(" & strNumLetras & ")", strNumLetras)
    End Function
    Function LlenarLeyendas(pDoc As DocumentoElectronico) As DocumentoElectronico
        Dim ListaLeyendas As New List(Of DocumentoElectronicoLeyenda)
        ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "1000", .Descripcion = NroEnLetras(Math.Truncate(pDoc.Total_Importe_Venta)) & " " & pDoc.Moneda_Descripcion})
        If pDoc.Total_ValorVenta_OperacionesGratuitas > 0 Then
            ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "1002", .Descripcion = "TRANSFERENCIA GRATUITA DE UN BIEN Y/O SERVICIO PRESTADO GRATUITAMENTE"})
        End If
        If pDoc.Total_Importe_Percepcion > 0 Then
            ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "2000", .Descripcion = "COMPROBANTE DE PERCEPCION"})
        End If
        If Not String.IsNullOrEmpty(pDoc.Tipo_Operacion_Codigo) Then
            If pDoc.Tipo_Operacion_Codigo = "05" Then
                ListaLeyendas.Add(New DocumentoElectronicoLeyenda With {.Codigo = "2005", .Descripcion = "VENTA REALIZADA POR EMISOR ITINERANTE"})
            End If
        End If
        pDoc.Leyendas = ListaLeyendas.ToArray

        Return pDoc
    End Function
    Function LlenarDetalles_Afecto(pDoc As DocumentoElectronico, NumDetalles As Integer) As DocumentoElectronico
        Dim pListaDetalles As New List(Of DocumentoElectronicoDetalle)

        Dim sGrava As Double = 0
        Dim sIGV As Double = 0
        Dim sTotal As Double = 0

        For i = 1 To NumDetalles
            Dim VarRan As New Random
            Dim AleatorioPrecioVenta As Double = VarRan.Next(10, 200) + VarRan.Next(0, 9)
            Dim AleatorioCant As Integer = VarRan.Next(1, 7)

            Dim ValorGravado As Double = (AleatorioPrecioVenta / 1.18)

            Dim pDetalle As New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "PROD00" & i.ToString,
                                                                     .Producto_Descripcion = "Producto de Ejemplo " & i.ToString,
                                                                     .Producto_UnidadMedida_Codigo = "NIU", .Producto_Cantidad = AleatorioCant,
                                                                     .Unitario_Precio_Venta = AleatorioPrecioVenta,
                                                                     .Unitario_Precio_Venta_Tipo_Codigo = "01",
                                                                     .Item_IGV_Total = (ValorGravado * 0.18) * AleatorioCant, .Unitario_IGV_Porcentaje = 18,
                                                                     .Item_Tipo_Afectacion_IGV_Codigo = "10",
                                                                     .Item_ISC_Total = 0, .Unitario_Valor_Unitario = ValorGravado,
                                                                     .Item_ValorVenta_Total = (ValorGravado * AleatorioCant)}

            pListaDetalles.Add(pDetalle)
            sGrava = sGrava + pDetalle.Item_ValorVenta_Total
            sIGV = sIGV + pDetalle.Item_IGV_Total
            sTotal = sTotal + (AleatorioPrecioVenta * AleatorioCant)
        Next

        pDoc.Total_ValorVenta_OperacionesGravadas = sGrava
        pDoc.Total_ValorVenta_OperacionesInafectas = 0
        pDoc.Total_ValorVenta_OperacionesExoneradas = 0
        pDoc.Total_ValorVenta_OperacionesGratuitas = 0
        pDoc.Total_IGV = sIGV
        pDoc.Total_ISC = 0
        pDoc.Total_OtrosTributos = 0
        pDoc.Total_OtrosCargos = 0
        'pDoc.Descuentos_Globales = 0
        pDoc.Total_Importe_Venta = sTotal

        'pDoc.Detalles = New DocumentoElectronicoDetalle() {New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "Cap-258963", .Producto_Descripcion = "CAPTOPRIL 25mg X 30", .Producto_UnidadMedida_Codigo = "ZZ", .Producto_Cantidad = 2, .Unitario_Precio_Venta = 180, .Unitario_Precio_Venta_Tipo_Codigo = "01", .Unitario_IGV = 32.4, .Unitario_IGV_Porcentaje = 18, .Item_Tipo_Afectacion_IGV_Codigo = "13", .Unitario_ISC = 0, .Unitario_Valor_Unitario = 147.6, .Item_SubTotal = 295.2}}
        pDoc.Detalles = pListaDetalles.ToArray
        Return pDoc
    End Function
    Function NotaCreditoDeFactura(pNotaCredito As DocumentoElectronico, pFactura As DocumentoElectronico) As DocumentoElectronico
        Dim ListaFacturasAfectadas As New List(Of DocumentoElectronicoReferenciaDocumentos)
        ListaFacturasAfectadas.Add(New DocumentoElectronicoReferenciaDocumentos With {.Documento_Tipo = pFactura.TipoDocumento_Codigo, .Documento_Serie = pFactura.TipoDocumento_Serie, .Documento_Numero = pFactura.TipoDocumento_Numero, .Documento_Ref_Tipo = "03", .Documento_Ref_Observaciones = "Correcion por error en la descripcion"})
        pNotaCredito.DocumentoAfectados = ListaFacturasAfectadas.ToArray
        pNotaCredito.Detalles = pFactura.Detalles

        pNotaCredito.Total_ValorVenta_OperacionesGravadas = pFactura.Total_ValorVenta_OperacionesGravadas
        pNotaCredito.Total_ValorVenta_OperacionesInafectas = pFactura.Total_ValorVenta_OperacionesInafectas
        pNotaCredito.Total_ValorVenta_OperacionesExoneradas = pFactura.Total_ValorVenta_OperacionesExoneradas
        pNotaCredito.Total_ValorVenta_OperacionesGratuitas = pFactura.Total_ValorVenta_OperacionesGratuitas
        pNotaCredito.Total_IGV = pFactura.Total_IGV
        pNotaCredito.Total_ISC = pFactura.Total_ISC
        pNotaCredito.Total_OtrosTributos = pFactura.Total_OtrosTributos
        pNotaCredito.Total_OtrosCargos = pFactura.Total_OtrosCargos
        'pNotaCredito.Descuentos_Globales = pFactura.Descuentos_Globales
        pNotaCredito.Total_Importe_Venta = pFactura.Total_Importe_Venta
        Return pNotaCredito
    End Function
    Function NotaDebitoDeFactura(pNotaDebito As DocumentoElectronico, pFactura As DocumentoElectronico) As DocumentoElectronico
        Dim ListaFacturasAfectadas As New List(Of DocumentoElectronicoReferenciaDocumentos)
        ListaFacturasAfectadas.Add(New DocumentoElectronicoReferenciaDocumentos With {.Documento_Tipo = pFactura.TipoDocumento_Codigo, .Documento_Serie = pFactura.TipoDocumento_Serie, .Documento_Numero = pFactura.TipoDocumento_Numero, .Documento_Ref_Tipo = "01", .Documento_Ref_Observaciones = "Intereses por mora"})
        pNotaDebito.DocumentoAfectados = ListaFacturasAfectadas.ToArray
        pNotaDebito.Detalles = pFactura.Detalles

        pNotaDebito.Total_ValorVenta_OperacionesGravadas = pFactura.Total_ValorVenta_OperacionesGravadas
        pNotaDebito.Total_ValorVenta_OperacionesInafectas = pFactura.Total_ValorVenta_OperacionesInafectas
        pNotaDebito.Total_ValorVenta_OperacionesExoneradas = pFactura.Total_ValorVenta_OperacionesExoneradas
        pNotaDebito.Total_ValorVenta_OperacionesGratuitas = pFactura.Total_ValorVenta_OperacionesGratuitas
        pNotaDebito.Total_IGV = pFactura.Total_IGV
        pNotaDebito.Total_ISC = pFactura.Total_ISC
        pNotaDebito.Total_OtrosTributos = pFactura.Total_OtrosTributos
        pNotaDebito.Total_OtrosCargos = pFactura.Total_OtrosCargos
        'pNotaDebito.Descuentos_Globales = pFactura.Descuentos_Globales
        pNotaDebito.Total_Importe_Venta = pFactura.Total_Importe_Venta
        Return pNotaDebito
    End Function
    Function LlenarDetalles_InafectaExonerada(pDoc As DocumentoElectronico, NumDetalles As Integer, pTipoInafectExo As String) As DocumentoElectronico
        Dim pListaDetalles As New List(Of DocumentoElectronicoDetalle)

        Dim sGrava As Double = 0
        Dim sIGV As Double = 0
        Dim sTotal As Double = 0

        For i = 1 To NumDetalles
            Dim VarRan As New Random
            Dim AleatorioPrecioVenta As Double = VarRan.Next(10, 200) + VarRan.Next(0, 9)
            Dim AleatorioCant As Integer = VarRan.Next(1, 7)
            Dim pDetalle As New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "PROD00" & i.ToString,
                                                                     .Producto_Descripcion = "Producto de Ejemplo " & i.ToString,
                                                                     .Producto_UnidadMedida_Codigo = "NIU", .Producto_Cantidad = AleatorioCant,
                                                                     .Unitario_Precio_Venta = AleatorioPrecioVenta,
                                                                     .Unitario_Precio_Venta_Tipo_Codigo = "02",
                                                                     .Item_IGV_Total = 0, .Unitario_IGV_Porcentaje = 18,
                                                                     .Item_Tipo_Afectacion_IGV_Codigo = pTipoInafectExo,
                                                                     .Item_ISC_Total = 0, .Unitario_Valor_Unitario = AleatorioPrecioVenta,
                                                                     .Item_ValorVenta_Total = (AleatorioPrecioVenta * AleatorioCant)}

            pListaDetalles.Add(pDetalle)
            sGrava = sGrava + pDetalle.Item_ValorVenta_Total
            sIGV = 0
            sTotal = sTotal + (AleatorioPrecioVenta * AleatorioCant)
        Next

        If pTipoInafectExo = "20" Then
            pDoc.Total_ValorVenta_OperacionesInafectas = 0
            pDoc.Total_ValorVenta_OperacionesExoneradas = sGrava
        ElseIf pTipoInafectExo = "30" Then
            pDoc.Total_ValorVenta_OperacionesInafectas = sGrava
            pDoc.Total_ValorVenta_OperacionesExoneradas = 0
        End If

        pDoc.Total_ValorVenta_OperacionesGravadas = 0

        pDoc.Total_ValorVenta_OperacionesGratuitas = 0
        pDoc.Total_IGV = sIGV
        pDoc.Total_ISC = 0
        pDoc.Total_OtrosTributos = 0
        pDoc.Total_OtrosCargos = 0
        'pDoc.Descuentos_Globales = 0
        pDoc.Total_Importe_Venta = sTotal

        'pDoc.Detalles = New DocumentoElectronicoDetalle() {New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "Cap-258963", .Producto_Descripcion = "CAPTOPRIL 25mg X 30", .Producto_UnidadMedida_Codigo = "ZZ", .Producto_Cantidad = 2, .Unitario_Precio_Venta = 180, .Unitario_Precio_Venta_Tipo_Codigo = "01", .Unitario_IGV = 32.4, .Unitario_IGV_Porcentaje = 18, .Item_Tipo_Afectacion_IGV_Codigo = "13", .Unitario_ISC = 0, .Unitario_Valor_Unitario = 147.6, .Item_SubTotal = 295.2}}
        pDoc.Detalles = pListaDetalles.ToArray
        Return pDoc
    End Function
    Function LlenarDetalles_Gratuitas(pDoc As DocumentoElectronico, NumDetalles As Integer) As DocumentoElectronico
        Dim pListaDetalles As New List(Of DocumentoElectronicoDetalle)

        Dim sGrava As Double = 0
        Dim sIGV As Double = 0
        Dim sTotal As Double = 0

        For i = 1 To NumDetalles
            Dim VarRan As New Random
            Dim AleatorioPrecioVenta As Double = VarRan.Next(10, 200) + VarRan.Next(0, 9)
            Dim AleatorioCant As Integer = VarRan.Next(1, 7)
            Dim pDetalle As New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "PROD00" & i.ToString,
                                                                     .Producto_Descripcion = "Producto de Ejemplo " & i.ToString,
                                                                     .Producto_UnidadMedida_Codigo = "NIU", .Producto_Cantidad = AleatorioCant,
                                                                     .Unitario_Precio_Venta = AleatorioPrecioVenta,
                                                                     .Unitario_Precio_Venta_Tipo_Codigo = "02",
                                                                     .Item_IGV_Total = 0, .Unitario_IGV_Porcentaje = 18,
                                                                     .Item_Tipo_Afectacion_IGV_Codigo = "11",
                                                                     .Item_ISC_Total = 0, .Unitario_Valor_Unitario = AleatorioPrecioVenta,
                                                                     .Item_ValorVenta_Total = (AleatorioPrecioVenta * AleatorioCant)}

            pListaDetalles.Add(pDetalle)
            sGrava = sGrava + pDetalle.Item_ValorVenta_Total
            sIGV = 0
            sTotal = 0
        Next


        pDoc.Total_ValorVenta_OperacionesInafectas = 0
        pDoc.Total_ValorVenta_OperacionesExoneradas = 0
        pDoc.Total_ValorVenta_OperacionesGravadas = 0

        pDoc.Total_ValorVenta_OperacionesGratuitas = sGrava
        pDoc.Total_IGV = sIGV
        pDoc.Total_ISC = 0
        pDoc.Total_OtrosTributos = 0
        pDoc.Total_OtrosCargos = 0
        'pDoc.Descuentos_Globales = 0
        pDoc.Total_Importe_Venta = sTotal

        'pDoc.Detalles = New DocumentoElectronicoDetalle() {New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "Cap-258963", .Producto_Descripcion = "CAPTOPRIL 25mg X 30", .Producto_UnidadMedida_Codigo = "ZZ", .Producto_Cantidad = 2, .Unitario_Precio_Venta = 180, .Unitario_Precio_Venta_Tipo_Codigo = "01", .Unitario_IGV = 32.4, .Unitario_IGV_Porcentaje = 18, .Item_Tipo_Afectacion_IGV_Codigo = "13", .Unitario_ISC = 0, .Unitario_Valor_Unitario = 147.6, .Item_SubTotal = 295.2}}
        pDoc.Detalles = pListaDetalles.ToArray
        Return pDoc
    End Function
    Function LlenarDetalles_AfectoConDescuentos(pDoc As DocumentoElectronico, NumDetalles As Integer) As DocumentoElectronico
        Dim pListaDetalles As New List(Of DocumentoElectronicoDetalle)

        Dim VarRan As New Random

        Dim sGrava As Double = 0
        Dim sIGV As Double = 0
        Dim sTotal As Double = 0

        Dim DescGlobalPorcentaje As Double
        Dim DescGlobalMontoValorVenta As Double

        Dim TotalDescuentos As Double = 0
        Do
            DescGlobalPorcentaje = VarRan.NextDouble
        Loop While DescGlobalPorcentaje = 1

        For i = 1 To NumDetalles

            Dim AleatorioPrecioVentaUnitario As Double = VarRan.Next(10, 200) + VarRan.Next(0, 9)
            Dim AleatorioCant As Integer = VarRan.Next(1, 7)
            Dim AleatorioDescuentoUnitario As Double = VarRan.NextDouble '0.0 - 1.0

            Dim BrutoValorVentaUnitario As Double = AleatorioPrecioVentaUnitario / 1.18 'Solo los gravados
            Dim BrutoValorVentaTotal As Double = BrutoValorVentaUnitario * AleatorioCant

            Dim DescuentoTotalItem As Double = BrutoValorVentaTotal * AleatorioDescuentoUnitario

            Dim NetoValorVentaTotal As Double = BrutoValorVentaTotal - DescuentoTotalItem
            Dim NetpIGVTotal As Double = NetoValorVentaTotal * 0.18

            TotalDescuentos = TotalDescuentos + DescuentoTotalItem

            Dim pDetalle As New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "PROD00" & i.ToString,
                                                                     .Producto_Descripcion = "Producto de Ejemplo " & i.ToString,
                                                                     .Producto_UnidadMedida_Codigo = "NIU", .Producto_Cantidad = AleatorioCant,
                                                                     .Unitario_Precio_Venta = AleatorioPrecioVentaUnitario,
                                                                     .Unitario_Precio_Venta_Tipo_Codigo = "01",
                                                                     .Item_IGV_Total = NetpIGVTotal, .Unitario_IGV_Porcentaje = 18,
                                                                     .Item_Tipo_Afectacion_IGV_Codigo = "10",
                                                                     .Item_ISC_Total = 0,
                                                                  .Unitario_Valor_Unitario = BrutoValorVentaUnitario,
                                                                     .Item_ValorVenta_Total = NetoValorVentaTotal}

            pListaDetalles.Add(pDetalle)
            sGrava = sGrava + pDetalle.Item_ValorVenta_Total
            sIGV = sIGV + NetpIGVTotal
            sTotal = sTotal + (sGrava + sIGV)
        Next
        DescGlobalMontoValorVenta = sGrava * DescGlobalPorcentaje

        TotalDescuentos = TotalDescuentos + DescGlobalMontoValorVenta

        pDoc.Total_ValorVenta_OperacionesGravadas = sGrava - (sGrava * DescGlobalPorcentaje)
        pDoc.Total_ValorVenta_OperacionesInafectas = 0
        pDoc.Total_ValorVenta_OperacionesExoneradas = 0
        pDoc.Total_ValorVenta_OperacionesGratuitas = 0
        pDoc.Total_IGV = sIGV - (sIGV * DescGlobalPorcentaje)
        pDoc.Total_ISC = 0
        pDoc.Total_OtrosTributos = 0
        pDoc.Total_OtrosCargos = 0
        'pDoc.Descuentos_Globales = 0
        pDoc.Total_Importe_Venta = pDoc.Total_ValorVenta_OperacionesGravadas + pDoc.Total_IGV
        pDoc.Total_Descuentos = TotalDescuentos

        'pDoc.Detalles = New DocumentoElectronicoDetalle() {New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "Cap-258963", .Producto_Descripcion = "CAPTOPRIL 25mg X 30", .Producto_UnidadMedida_Codigo = "ZZ", .Producto_Cantidad = 2, .Unitario_Precio_Venta = 180, .Unitario_Precio_Venta_Tipo_Codigo = "01", .Unitario_IGV = 32.4, .Unitario_IGV_Porcentaje = 18, .Item_Tipo_Afectacion_IGV_Codigo = "13", .Unitario_ISC = 0, .Unitario_Valor_Unitario = 147.6, .Item_SubTotal = 295.2}}
        pDoc.Detalles = pListaDetalles.ToArray
        Return pDoc
    End Function
    Function LlenarDetalles_GravadasConISC(pDoc As DocumentoElectronico, NumDetalles As Integer) As DocumentoElectronico
        Dim pListaDetalles As New List(Of DocumentoElectronicoDetalle)

        Dim sGrava As Double = 0
        Dim sIGV As Double = 0
        Dim sTotal As Double = 0
        Dim sISC As Double = 0

        For i = 1 To NumDetalles
            Dim VarRan As New Random
            Dim AleatorioPrecioVenta As Double = VarRan.Next(10, 200) + VarRan.Next(0, 9)
            Dim AleatorioCant As Integer = VarRan.Next(1, 7)
            Dim AleatorioPorcentajeISC As Double = VarRan.NextDouble

            Dim ValorGravado As Double = (AleatorioPrecioVenta / 1.18)
            Dim MontoISCUnit As Double = ValorGravado * AleatorioPorcentajeISC

            Dim pDetalle As New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "PROD00" & i.ToString,
                                                                     .Producto_Descripcion = "Producto de Ejemplo " & i.ToString,
                                                                     .Producto_UnidadMedida_Codigo = "NIU", .Producto_Cantidad = AleatorioCant,
                                                                     .Unitario_Precio_Venta = AleatorioPrecioVenta,
                                                                     .Unitario_Precio_Venta_Tipo_Codigo = "01",
                                                                     .Item_IGV_Total = ((ValorGravado * 0.18) + MontoISCUnit) * AleatorioCant, .Unitario_IGV_Porcentaje = 18,
                                                                     .Item_Tipo_Afectacion_IGV_Codigo = "10",
                                                                     .Item_ISC_Total = MontoISCUnit * AleatorioCant, .Unitario_Valor_Unitario = ValorGravado,
                                                                     .Item_ValorVenta_Total = (ValorGravado * AleatorioCant)}

            pListaDetalles.Add(pDetalle)
            sISC = sISC + pDetalle.Item_ISC_Total
            sGrava = sGrava + pDetalle.Item_ValorVenta_Total
            sIGV = sIGV + pDetalle.Item_IGV_Total
            sTotal = sTotal + (AleatorioPrecioVenta * AleatorioCant)
        Next

        pDoc.Total_ValorVenta_OperacionesGravadas = sGrava
        pDoc.Total_ValorVenta_OperacionesInafectas = 0
        pDoc.Total_ValorVenta_OperacionesExoneradas = 0
        pDoc.Total_ValorVenta_OperacionesGratuitas = 0
        pDoc.Total_IGV = sIGV
        pDoc.Total_ISC = sISC
        pDoc.Total_OtrosTributos = 0
        pDoc.Total_OtrosCargos = 0
        'pDoc.Descuentos_Globales = 0
        pDoc.Total_Importe_Venta = (sTotal + sISC)

        'pDoc.Detalles = New DocumentoElectronicoDetalle() {New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "Cap-258963", .Producto_Descripcion = "CAPTOPRIL 25mg X 30", .Producto_UnidadMedida_Codigo = "ZZ", .Producto_Cantidad = 2, .Unitario_Precio_Venta = 180, .Unitario_Precio_Venta_Tipo_Codigo = "01", .Unitario_IGV = 32.4, .Unitario_IGV_Porcentaje = 18, .Item_Tipo_Afectacion_IGV_Codigo = "13", .Unitario_ISC = 0, .Unitario_Valor_Unitario = 147.6, .Item_SubTotal = 295.2}}
        pDoc.Detalles = pListaDetalles.ToArray
        Return pDoc
    End Function
    Function LlenarDetalles_GravadasConPercepcion(pDoc As DocumentoElectronico, NumDetalles As Integer) As DocumentoElectronico
        'Falta aqui
        Dim pListaDetalles As New List(Of DocumentoElectronicoDetalle)

        Dim sGrava As Double = 0
        Dim sIGV As Double = 0
        Dim sTotal As Double = 0

        For i = 1 To NumDetalles
            Dim VarRan As New Random
            Dim AleatorioPrecioVenta As Double = VarRan.Next(10, 200) + VarRan.Next(0, 9)
            Dim AleatorioCant As Integer = VarRan.Next(1, 7)

            Dim ValorGravado As Double = (AleatorioPrecioVenta / 1.18)

            Dim pDetalle As New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "PROD00" & i.ToString,
                                                                     .Producto_Descripcion = "Producto de Ejemplo " & i.ToString,
                                                                     .Producto_UnidadMedida_Codigo = "NIU", .Producto_Cantidad = AleatorioCant,
                                                                     .Unitario_Precio_Venta = AleatorioPrecioVenta,
                                                                     .Unitario_Precio_Venta_Tipo_Codigo = "01",
                                                                     .Item_IGV_Total = (ValorGravado * 0.18) * AleatorioCant, .Unitario_IGV_Porcentaje = 18,
                                                                     .Item_Tipo_Afectacion_IGV_Codigo = "10",
                                                                     .Item_ISC_Total = 0, .Unitario_Valor_Unitario = ValorGravado,
                                                                     .Item_ValorVenta_Total = (ValorGravado * AleatorioCant)}

            pListaDetalles.Add(pDetalle)
            sGrava = sGrava + pDetalle.Item_ValorVenta_Total
            sIGV = sIGV + pDetalle.Item_IGV_Total
            sTotal = sTotal + (AleatorioPrecioVenta * AleatorioCant)
        Next

        pDoc.Total_ValorVenta_OperacionesGravadas = sGrava
        pDoc.Total_ValorVenta_OperacionesInafectas = 0
        pDoc.Total_ValorVenta_OperacionesExoneradas = 0
        pDoc.Total_ValorVenta_OperacionesGratuitas = 0
        pDoc.Total_IGV = sIGV
        pDoc.Total_ISC = 0
        pDoc.Total_OtrosTributos = 0
        pDoc.Total_OtrosCargos = 0
        'pDoc.Descuentos_Globales = 0
        pDoc.Total_Importe_Venta = sTotal

        'pDoc.Detalles = New DocumentoElectronicoDetalle() {New DocumentoElectronicoDetalle With {.Item = 1, .Producto_Codigo = "Cap-258963", .Producto_Descripcion = "CAPTOPRIL 25mg X 30", .Producto_UnidadMedida_Codigo = "ZZ", .Producto_Cantidad = 2, .Unitario_Precio_Venta = 180, .Unitario_Precio_Venta_Tipo_Codigo = "01", .Unitario_IGV = 32.4, .Unitario_IGV_Porcentaje = 18, .Item_Tipo_Afectacion_IGV_Codigo = "13", .Unitario_ISC = 0, .Unitario_Valor_Unitario = 147.6, .Item_SubTotal = 295.2}}
        pDoc.Detalles = pListaDetalles.ToArray
        Return pDoc
    End Function
End Class