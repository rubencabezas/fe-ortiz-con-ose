﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Pop3Config1 As ComponentPro.Net.Mail.Pop3Config = New ComponentPro.Net.Mail.Pop3Config()
        Me.TimerCheckMail = New System.Windows.Forms.Timer(Me.components)
        Me.pop3Client = New ComponentPro.Net.Mail.Pop3(Me.components)
        Me.bounceFilter = New ComponentPro.Net.Mail.BounceInspector(Me.components)
        Me.SuspendLayout()
        '
        'TimerCheckMail
        '
        Me.TimerCheckMail.Enabled = True
        Me.TimerCheckMail.Interval = 6000
        '
        'pop3Client
        '
        Me.pop3Client.Config = Pop3Config1
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TimerCheckMail As System.Windows.Forms.Timer
    Private WithEvents pop3Client As ComponentPro.Net.Mail.Pop3
    Private WithEvents bounceFilter As ComponentPro.Net.Mail.BounceInspector

End Class
