﻿Imports System.Data.SqlClient

Public Class Dat_FACTE_DOCUMENTOS
    Public Function Insertar_Documento(ByVal pEntidad As eProvisionFacturacion, pArchivo As fcProceso) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTOS_ACEPTADOS_INSERT", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = pEntidad.TpDc_Codigo_St
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = pEntidad.Dvt_VTSerie
                    Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = pEntidad.Dvt_VTNumer
                    Cmd.Parameters.Add("@PDoc_FechaEmision", SqlDbType.Date).Value = pEntidad.Dvt_Fecha_Emision
                    Cmd.Parameters.Add("@PDoc_Moneda", SqlDbType.Char, 3).Value = pEntidad.Dvt_Moneda_Internacional
                    Cmd.Parameters.Add("@PDoc_Importe", SqlDbType.Decimal, 10, 2).Value = pEntidad.Dvt_Total_Importe
                    Cmd.Parameters.Add("@PDoc_Cliente_NumDoc", SqlDbType.VarChar, 11).Value = pEntidad.Cliente_Documento_Numero
                    Cmd.Parameters.Add("@PDoc_Cliente_Rs", SqlDbType.VarChar, 150).Value = pEntidad.Cliente_RazonSocial_Nombre_Completo
                    Cmd.Parameters.Add("@PDoc_NombreArchivoXML", SqlDbType.VarChar, 50).Value = pArchivo.FileNameXML
                    Cmd.Parameters.Add("@PDoc_NombreArchivoPDF", SqlDbType.VarChar, 50).Value = pArchivo.FileNamePDF
                    Cmd.Parameters.Add("@PDoc_NombreArchivoWeb", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output
                    Cmd.Parameters.Add("@PDoc_Estado", SqlDbType.Char, 4).Value = pEntidad.EstadoElectronico
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                    pEntidad.Doc_NombreArchivoWeb = Cmd.Parameters("@PDoc_NombreArchivoWeb").Value
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Resumen_Correlativo(ByVal pFecha As Date) As Integer
        Dim nID As Integer
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTO_EMPRESAS_SUMMARY_ID", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_FechaGeneracion", SqlDbType.Date).Value = pFecha
                    Cmd.Parameters.Add("@PDoc_Resumen_Correlativo", SqlDbType.Int).Direction = ParameterDirection.Output
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                    nID = Cmd.Parameters("@PDoc_Resumen_Correlativo").Value
                End Using
            End Using
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return nID
    End Function
    Public Function Actualizar_Envio_Correo(ByVal pEntidad As DocumentoElectronico) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTOS_ACEPTADOS_ACTUALIZAR_ESTADO_ENVIO", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_Documento_Numero
                    Cmd.Parameters.Add("@PDoc_NombreArchivoWeb", SqlDbType.VarChar, 50).Value = pEntidad.Doc_NombreArchivoWeb
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Actualizar_Envio_Correo(ByVal pEntidad As eProvisionFacturacion) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTOS_ACEPTADOS_ACTUALIZAR_ESTADO_ENVIO", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_NombreArchivoWeb", SqlDbType.VarChar, 50).Value = pEntidad.Doc_NombreArchivoWeb
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Insertar_Resumen(ByVal pEntidad As eProvisionFacturacion, pArchivo As fcProceso) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTO_EMPRESAS_SUMMARY_INSERT", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_FechaGeneracion", SqlDbType.Date).Value = pEntidad.Dvt_Fecha_Generacion
                    Cmd.Parameters.Add("@PDoc_Resumen_Correlativo", SqlDbType.Int).Value = pEntidad.Rs_Correlativo
                    Cmd.Parameters.Add("@PDoc_FechaEmision", SqlDbType.Date).Value = pEntidad.Dvt_Fecha_Emision
                    Cmd.Parameters.Add("@PDoc_Moneda", SqlDbType.Char, 3).Value = pEntidad.Dvt_Moneda_Internacional
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = pEntidad.TpDc_Codigo_St
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = pEntidad.Dvt_VTSerie
                    Cmd.Parameters.Add("@PDoc_NumeroInicio", SqlDbType.Char, 8).Value = pEntidad.Rs_NumeroInicio
                    Cmd.Parameters.Add("@PDoc_NumeroFin", SqlDbType.Char, 8).Value = pEntidad.Rs_NumeroFin
                    Cmd.Parameters.Add("@PDoc_ImporteResumen", SqlDbType.Decimal, 10, 2).Value = pEntidad.Rs_TotalResumen

                    Cmd.Parameters.Add("@PDoc_SUNATTicket_ID", SqlDbType.VarChar, 50).Value = pArchivo.TicketNumero
                    Cmd.Parameters.Add("@PDoc_NombreArchivoXML", SqlDbType.VarChar, 50).Value = pArchivo.FileNameXML
                    'Cmd.Parameters.Add("@PDoc_NombreArchivoPDF", SqlDbType.VarChar, 50).Value = pArchivo.FileNamePDF
                    Cmd.Parameters.Add("@PDoc_NombreArchivoWeb", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output
                    Cmd.Parameters.Add("@PDoc_Estado", SqlDbType.Char, 4).Value = pEntidad.EstadoElectronico
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                    pEntidad.Doc_NombreArchivoWeb = Cmd.Parameters("@PDoc_NombreArchivoWeb").Value
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_TicketPendientes_Resumen_Summary() As List(Of eTicket)
        Dim Tickets As New List(Of eTicket)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTO_EMPRESAS_SUMMARY_TICKET_PENDIENTES", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read()
                            Tickets.Add(New eTicket With {.SUNATTicket_ID = Dr.GetString(0), .Emisor_RUC = Dr.GetString(1)})
                        End While
                    End Using
                End Using
            End Using
            Return Tickets
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_TicketPendientes_ComunicacionBaja_Voided() As List(Of eTicket)
        Dim Tickets As New List(Of eTicket)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_RESUMEN_BAJA_DOCUMENTO_TICKET_PENDIENTES", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read()
                            Tickets.Add(New eTicket With {.SUNATTicket_ID = Dr.GetString(0), .Emisor_RUC = Dr.GetString(1)})
                        End While
                    End Using
                End Using
            End Using
            Return Tickets
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Cambiar_Estado_Resumen(pEntidad As eTicket) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTO_EMPRESAS_SUMMARY_TICKET_CAMBIARESTADO", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_SUNATTicket_ID", SqlDbType.VarChar, 50).Value = pEntidad.SUNATTicket_ID
                    Cmd.Parameters.Add("@PDoc_Estado", SqlDbType.Char, 4).Value = pEntidad.Estado_Codigo
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Resumen_BoletaVenta_Detalle(pEntidad As eTicket) As List(Of eProvisionFacturacion)
        Dim Documentos As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_RESUMEN_BAJA_DOCUMENTO_TICKET_PENDIENTES", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_SUNATTicket_ID", SqlDbType.VarChar, 50).Value = pEntidad.SUNATTicket_ID
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read()
                            Documentos.Add(New eProvisionFacturacion With {.Emisor_RUC = Dr.GetString(0), .TpDc_Codigo_St = Dr.GetString(1), .Dvt_VTSerie = Dr.GetString(2), .Rs_NumeroInicio = Dr.GetString(3), .Rs_NumeroFin = Dr.GetString(4)})
                        End While
                    End Using
                End Using
            End Using
            Return Documentos
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Sub Buscar_Correlativo_Comunicacion_Baja(ByRef pFechaGeneracion As Date, ByRef pCorrelativo As Integer)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_RESUMEN_BAJA_OBTENER_CORRELATIVO", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PRsm_FechaGeneracion", SqlDbType.Date).Direction = ParameterDirection.Output
                    Cmd.Parameters.Add("@PRsm_Correlativo", SqlDbType.Int).Direction = ParameterDirection.Output
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                    pFechaGeneracion = Cmd.Parameters("@PRsm_FechaGeneracion").Value
                    pCorrelativo = Cmd.Parameters("@PRsm_Correlativo").Value
                End Using
            End Using
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Public Function Insertar_Comunicacion_Baja(ByVal pDocumentos As List(Of eProvisionFacturacion), pArchivo As fcProceso) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_RESUMEN_BAJA_INSERT", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PRsm_Emisor_RUC", SqlDbType.Char, 11).Value = pDocumentos(0).Emisor_RUC
                    Cmd.Parameters.Add("@PRsm_FechaGeneracion", SqlDbType.Date).Value = pDocumentos(0).Dvt_Fecha_Generacion
                    Cmd.Parameters.Add("@PRsm_Correlativo", SqlDbType.Int).Value = pDocumentos(0).Rs_Correlativo
                    Cmd.Parameters.Add("@PRsm_SUNATTicket_ID", SqlDbType.VarChar, 50).Value = pArchivo.TicketNumero
                    Cmd.Parameters.Add("@PRsm_NombreArchivoXML", SqlDbType.VarChar, 50).Value = pArchivo.FileNameXML
                    Cmd.Parameters.Add("@PRsm_NombreArchivoWeb", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                    pDocumentos(0).Doc_NombreArchivoWeb = Cmd.Parameters("@PRsm_NombreArchivoWeb").Value

                    For Each Documento In pDocumentos
                        Cmd.Parameters.Clear()
                        Cmd.CommandText = "UP_FACTE_RESUMEN_BAJA_DOCUMENTO_INSERT"
                        Cmd.Parameters.Add("@PRsm_Emisor_RUC", SqlDbType.Char, 11).Value = pDocumentos(0).Emisor_RUC
                        Cmd.Parameters.Add("@PRsm_FechaGeneracion", SqlDbType.Date).Value = pDocumentos(0).Dvt_Fecha_Generacion
                        Cmd.Parameters.Add("@PRsm_Correlativo", SqlDbType.Int).Value = pDocumentos(0).Rs_Correlativo
                        Cmd.Parameters.Add("@PRsm_Doc_Item", SqlDbType.Int).Value = Documento.Item

                        Cmd.Parameters.Add("@PRsm_Doc_Tipo", SqlDbType.Char, 2).Value = Documento.TpDc_Codigo_St
                        Cmd.Parameters.Add("@PRsm_Doc_Serie", SqlDbType.Char, 4).Value = Documento.Dvt_VTSerie
                        Cmd.Parameters.Add("@PRsm_Doc_Numero", SqlDbType.Char, 8).Value = Documento.Dvt_VTNumer
                        Cmd.ExecuteNonQuery()
                    Next

                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Comunicacion_Baja_CambiarEstado(pEntidad As eTicket) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_RESUMEN_BAJA_DOCUMENTO_TICKET_CAMBIARESTADO", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PRsm_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PRsm_SUNATTicket_ID", SqlDbType.VarChar, 50).Value = pEntidad.SUNATTicket_ID
                    Cmd.Parameters.Add("@PRsm_Estado", SqlDbType.Char, 4).Value = pEntidad.Estado_Codigo
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Comunicacion_Baja_Lista_Documentos(pEntidad As eTicket) As List(Of eProvisionFacturacion)
        Dim Documentos As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_RESUMEN_BAJA_DOCUMENTO_TICKET_PENDIENTES", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PRsm_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PRsm_SUNATTicket_ID", SqlDbType.VarChar, 50).Value = pEntidad.SUNATTicket_ID
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read()
                            Documentos.Add(New eProvisionFacturacion With {.Emisor_RUC = Dr.GetString(0), .TpDc_Codigo_St = Dr.GetString(1), .Dvt_VTSerie = Dr.GetString(2), .Dvt_VTNumer = Dr.GetString(3)})
                        End While
                    End Using
                End Using
            End Using
            Return Documentos
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Documento(pEntidad As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Dim Documentos As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTO_ELECTRONICO_BUSQUEDA", Cn) With {.CommandType = CommandType.StoredProcedure, .CommandTimeout = 0}
                    'Cmd.Parameters.Add("@PDoc_Fecha_Desde", SqlDbType.Date).Value = IIf(pEntidad.Busq_Fecha_Inicio = Date.MinValue, Nothing, pEntidad.Busq_Fecha_Inicio)
                    'Cmd.Parameters.Add("@PDoc_Fecha_Hasta", SqlDbType.Date).Value = IIf(pEntidad.Busq_Fecha_Fin = Date.MinValue, Nothing, pEntidad.Busq_Fecha_Fin)
                    Cmd.Parameters.Add("@PDoc_Fecha_Desde", SqlDbType.Date).Value = pEntidad.Busq_Fecha_Inicio
                    Cmd.Parameters.Add("@PDoc_Fecha_Hasta", SqlDbType.Date).Value = pEntidad.Busq_Fecha_Fin
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = pEntidad.TpDc_Codigo_St
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = pEntidad.Dvt_VTSerie
                    Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = pEntidad.Dvt_VTNumer
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read()
                            Documentos.Add(New eProvisionFacturacion With {.Emisor_RUC = Dr.GetString(0), .TpDc_Codigo_St = Dr.GetString(1), .TpDc_Descripcion = Dr.GetString(2), .Dvt_VTSerie = Dr.GetString(3), .Dvt_VTNumer = Dr.GetString(4), .Dvt_Fecha_Emision = Dr.GetDateTime(5), .Dvt_Moneda_Internacional = Dr.GetString(6), .Dvt_Simbol_Moneda = Dr.GetString(7), .Dvt_Total_Importe = Dr.GetDecimal(8), .Cliente_Documento_Tipo = Dr.GetString(9), .Cliente_Documento_Numero = Dr.GetString(10), .Cliente_RazonSocial_Nombre = Dr.GetString(11), .EstadoDoc_Codigo = Dr.GetString(12), .EstadoDoc_Descripcion = Dr.GetString(13), .EstadoElectronico = Dr.GetString(14),
                                                                           .Estado_Descripcion = Dr.GetString(15), .Path_Root_App = Dr.GetString(16), .Nombre_XML = Dr.GetString(17), .Nombre_PDF = Dr.GetString(18), .Doc_NombreArchivoWeb = Dr.GetString(19), .Doc_Correo_Enviado = Dr.GetBoolean(20), .Doc_Correo_EstaLeido = Dr.GetBoolean(21), .Cliente_Correo_Electronico = Dr.GetString(26), .Emisor_RazonSocial_Nombre = Dr.GetString(27)})
                        End While
                    End Using
                End Using
            End Using
            Return Documentos
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Documento_Versiones(pEntidad As eProvisionFacturacion) As DataTable
        Dim DtVersiones As New DataTable
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTO_ELECTRONICO_VERSIONES", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = pEntidad.TpDc_Codigo_St
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = pEntidad.Dvt_VTSerie
                    Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = pEntidad.Dvt_VTNumer
                    Cn.Open()
                    'Using Dr As SqlDataReader = Cmd.ExecuteReader()
                    '    While Dr.Read()
                    '        Documentos.Add(New eProvisionFacturacion With {.Emisor_RUC = Dr.GetString(0), .TpDc_Codigo_St = Dr.GetString(1), .TpDc_Descripcion = Dr.GetString(2), .Dvt_VTSerie = Dr.GetString(3), .Dvt_VTNumer = Dr.GetString(4), .Dvt_Fecha_Emision = Dr.GetDateTime(5), .Dvt_Moneda_Internacional = Dr.GetString(6), .Dvt_Simbol_Moneda = Dr.GetString(7), .Dvt_Total_Importe = Dr.GetDecimal(8), .Cliente_Documento_Numero = Dr.GetString(9), .Cliente_RazonSocial_Nombre = Dr.GetString(10), .EstadoElectronico = Dr.GetString(11), .Estado_Descripcion = Dr.GetString(12), .PathLocal = Dr.GetString(13), .Nombre_XML = Dr.GetString(14), .Doc_Correo_Enviado = Dr.GetBoolean(17), .Doc_Correo_EstaLeido = Dr.GetBoolean(18)})
                    '    End While
                    'End Using
                    Using Da As New SqlDataAdapter(Cmd)
                        Da.Fill(DtVersiones)
                    End Using
                End Using
            End Using
            Return DtVersiones
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Documento_CambiarEstado(pEntidad As eProvisionFacturacion) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTO_ELECTRONICO_ENVIADO", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = pEntidad.TpDc_Codigo_St
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = pEntidad.Dvt_VTSerie
                    Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = pEntidad.Dvt_VTNumer
                    Cmd.Parameters.Add("@PXML_Correlativo", SqlDbType.Int).Value = pEntidad.Version_Correlativo
                    Cmd.Parameters.Add("@PEstEnv_Codigo", SqlDbType.Char, 3).Value = pEntidad.Vnt_EstadoEnvio
                    Cmd.Parameters.Add("@PSUNAT_Respuesta_Codigo", SqlDbType.Char, 4).Value = pEntidad.Sunat_Respuesta_Codigo
                    Cmd.Parameters.Add("@PNombre_PDF", SqlDbType.VarChar, 50).Value = IIf(String.IsNullOrEmpty(pEntidad.Nombre_PDF), DBNull.Value, pEntidad.Nombre_PDF.Trim)
                    Cmd.Parameters.Add("@PSUNAT_Respuesta_Fecha", SqlDbType.DateTime).Value = IIf(pEntidad.Sunat_Respuesta_Fecha = "#12:00:00 AM#", DBNull.Value, pEntidad.Sunat_Respuesta_Fecha)
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Documento_CambiarEstado(pEntidad As DocumentoElectronico) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTO_ELECTRONICO_ENVIADO", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_Documento_Numero
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = pEntidad.TipoDocumento_Codigo
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = pEntidad.TipoDocumento_Serie
                    Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = pEntidad.TipoDocumento_Numero
                    Cmd.Parameters.Add("@PXML_Correlativo", SqlDbType.Int).Value = pEntidad.SQL_XML_Correlativo
                    Cmd.Parameters.Add("@PEstEnv_Codigo", SqlDbType.Char, 3).Value = pEntidad.Doc_XML_Estado
                    Cmd.Parameters.Add("@PSUNAT_Respuesta_Codigo", SqlDbType.Char, 4).Value = pEntidad.Doc_XML_SUNAT_Resp_Code
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Resumen_Boleta_Busqueda(pEntidad As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Dim Documentos As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTOS_RESUMEN_BOLETAS_LISTA", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_Fecha_Desde", SqlDbType.Date).Value = pEntidad.Busq_Fecha_Inicio
                    Cmd.Parameters.Add("@PDoc_Fecha_Hasta", SqlDbType.Date).Value = pEntidad.Busq_Fecha_Fin
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_Tipo", SqlDbType.Char, 2).Value = pEntidad.TpDc_Codigo_St
                    Cmd.Parameters.Add("@PDoc_Serie", SqlDbType.Char, 4).Value = pEntidad.Dvt_VTSerie
                    Cmd.Parameters.Add("@PDoc_Numero", SqlDbType.Char, 8).Value = pEntidad.Dvt_VTNumer
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read()
                            Documentos.Add(New eProvisionFacturacion With {.Emisor_RUC = Dr.GetString(0), .Dvt_Fecha_Generacion = Dr.GetDateTime(1), .Rs_Correlativo = Dr.GetInt32(2), .Dvt_Moneda_Internacional = Dr.GetString(3), .Dvt_Simbol_Moneda = Dr.GetString(4), .Dvt_Fecha_Emision = Dr.GetDateTime(5), .TpDc_Codigo_St = Dr.GetString(6), .TpDc_Descripcion = Dr.GetString(7), .Dvt_VTSerie = Dr.GetString(8), .Rs_NumeroInicio = Dr.GetString(9), .Rs_NumeroFin = Dr.GetString(10), .Dvt_Total_Importe = Dr.GetDecimal(11), .Path_Root_App = Dr.GetString(12), .Nombre_XML = Dr.GetString(13), .EstadoElectronico = Dr.GetString(15), .Estado_Descripcion = Dr.GetString(16)})
                        End While
                    End Using
                End Using
            End Using
            Return Documentos
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Resumen_Cambiar_Estado(pEntidad As eProvisionFacturacion) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_RESUMEN_DIARIO_CAMBIAR_ESTADO_ENVIO", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_FechaGeneracion", SqlDbType.Date).Value = pEntidad.Dvt_Fecha_Generacion
                    Cmd.Parameters.Add("@PDoc_Resumen_Correlativo", SqlDbType.Int).Value = pEntidad.Rs_Correlativo
                    Cmd.Parameters.Add("@PDoc_NombreArchivoXML", SqlDbType.VarChar, 50).Value = pEntidad.Nombre_XML
                    Cmd.Parameters.Add("@PDoc_SUNATTicket_ID", SqlDbType.VarChar, 50).Value = pEntidad.Sunat_Respuesta_Ticket
                    Cmd.Parameters.Add("@PSUNAT_Respuesta_Codigo", SqlDbType.Char, 4).Value = pEntidad.Sunat_Respuesta_Codigo
                    Cmd.Parameters.Add("@PEstEnv_Codigo", SqlDbType.Char, 3).Value = pEntidad.Vnt_EstadoEnvio
                    Cmd.Parameters.Add("@PSUNAT_Respuesta_Fecha", SqlDbType.DateTime).Value = IIf(pEntidad.Sunat_Respuesta_Fecha = "#12:00:00 AM#", DBNull.Value, pEntidad.Sunat_Respuesta_Fecha)
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Baja_Cambiar_Estado(pEntidad As eProvisionFacturacion) As Boolean
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_COMUNICACION_BAJA_CAMBIAR_ESTADO_ENVIO", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PGrpTipo_Codigo", SqlDbType.Char, 3).Value = pEntidad.Grupo_Codigo
                    Cmd.Parameters.Add("@PDoc_FechaGeneracion", SqlDbType.Date).Value = pEntidad.Dvt_Fecha_Generacion
                    Cmd.Parameters.Add("@PDoc_Resumen_Correlativo", SqlDbType.Int).Value = pEntidad.Rs_Correlativo
                    Cmd.Parameters.Add("@PDoc_NombreArchivoXML", SqlDbType.VarChar, 50).Value = pEntidad.Nombre_XML
                    Cmd.Parameters.Add("@PDoc_SUNATTicket_ID", SqlDbType.VarChar, 50).Value = pEntidad.Sunat_Respuesta_Ticket
                    Cmd.Parameters.Add("@PSUNAT_Respuesta_Codigo", SqlDbType.Char, 4).Value = pEntidad.Sunat_Respuesta_Codigo
                    Cmd.Parameters.Add("@PEstEnv_Codigo", SqlDbType.Char, 3).Value = pEntidad.Vnt_EstadoEnvio
                    Cmd.Parameters.Add("@PSUNAT_Respuesta_Fecha", SqlDbType.DateTime).Value = IIf(pEntidad.Sunat_Respuesta_Fecha = "#12:00:00 AM#", DBNull.Value, pEntidad.Sunat_Respuesta_Fecha)
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Resumen_Boleta_Pendientes_Respuesta_Busqueda() As List(Of eProvisionFacturacion)
        Dim Documentos As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_RESUMEN_DIARIO_ESPERA_RESPUESTA_TICKET", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read()
                            Documentos.Add(New eProvisionFacturacion With {.Emisor_RUC = Dr.GetString(0), .Dvt_Fecha_Generacion = Dr.GetDateTime(1), .Rs_Correlativo = Dr.GetInt32(2), .Dvt_Fecha_Emision = Dr.GetDateTime(3), .Nombre_XML = Dr.GetString(4), .Sunat_Respuesta_Ticket = Dr.GetString(5), .RootPath_Enterprise = Dr.GetString(6), .Etapa_Codigo = Dr.GetString(7)})
                        End While
                    End Using
                End Using
            End Using
            Return Documentos
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Comunicacion_Baja_Pendientes_Respuesta_Busqueda() As List(Of eProvisionFacturacion)
        Dim Documentos As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_COMUNICACION_BAJA_ESPERA_RESPUESTA_TICKET", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read()
                            Documentos.Add(New eProvisionFacturacion With {.Emisor_RUC = Dr.GetString(0), .Dvt_Fecha_Generacion = Dr.GetDateTime(1), .Rs_Correlativo = Dr.GetInt32(2), .Dvt_Fecha_Emision = Dr.GetDateTime(3), .Nombre_XML = Dr.GetString(4), .Sunat_Respuesta_Ticket = Dr.GetString(5), .RootPath_Enterprise = Dr.GetString(6), .Etapa_Codigo = Dr.GetString(7), .Grupo_Codigo = Dr.GetString(8)})
                        End While
                    End Using
                End Using
            End Using
            Return Documentos
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
