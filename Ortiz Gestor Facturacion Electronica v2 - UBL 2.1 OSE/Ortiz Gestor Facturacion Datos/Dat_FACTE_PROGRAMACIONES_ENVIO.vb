﻿Imports System.Data.SqlClient

Public Class Dat_FACTE_PROGRAMACIONES_ENVIO
    Public Function Buscar_Lista(ByVal pEmp_RUC As String) As List(Of eProgramacion)
        Dim rCertificados As New List(Of eProgramacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection())
                Using Cmd As New SqlCommand("UP_FACTE_PROGRAMACIONES_ENVIO_LISTA", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cmd.Parameters.Add("@PEmp_RUC", SqlDbType.Char, 11).Value = pEmp_RUC
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            rCertificados.Add(New eProgramacion With {.Emisor_RUC = Dr.GetString(0), .Progra_Correlativo = Dr.GetInt32(1), .Progra_Descripcion = Dr.GetString(2), .Progra_Habilitado = Dr.GetBoolean(3), .Progra_Appli_TipoDoc = Dr.GetString(4), .Progra_Appli_Filtra_Serie = Dr.GetBoolean(5), .Progra_Appli_Serie = Dr.GetString(6), .Progra_EsResumenBoletas = Dr.GetBoolean(7), .Progra_EsComunicacionBaja = Dr.GetBoolean(8), .Progra_FrecuEnvio_Hora_Es_Establecido = Dr.GetBoolean(9), .Progra_FrecuEnvio_Hora_Es_XCadaHora = Dr.GetBoolean(10), .Progra_FrecuEnvio_Hora_Es_DespuesEmitido = Dr.GetBoolean(11), .Progra_FrecuEnvio_Hora_Es_DespuesConfirmado = Dr.GetBoolean(12), .Progra_FrecuEnvio_Hora_Desde = Dr.GetTimeSpan(13), .Progra_FrecuEnvio_Hora_Hasta = Dr.GetTimeSpan(14), .Progra_FrecuEnvio_Hora_XCadaHora = Dr.GetInt32(15), .Progra_Fecha_Desde = Dr.GetDateTime(16), .Progra_Fecha_SinFin = Dr.GetBoolean(17), .Progra_Fecha_Hasta = Dr.GetDateTime(18), .Progra_FechaHoy = Dr.GetDateTime(20), .Progra_Appli_TipoDoc_Descripcion = Dr.GetString(21)})
                        End While
                    End Using
                End Using
            End Using
            Return rCertificados
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Lista_Habilitados() As List(Of eProvisionFacturacion)
        Dim rCertificados As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection())
                Using Cmd As New SqlCommand("UP_FACTE_PROGRAMACIONES_ENVIO_LISTA_HABILITADOS", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()

                        While Dr.Read
                            rCertificados.Add(New eProvisionFacturacion With {.Emisor_RUC = Dr.GetString(0), .Etapa_Codigo = Dr.GetString(1), .Path_Root_App = Dr.GetString(2), .Nombre_XML = Dr.GetString(3), .Doc_NombreArchivoWeb = Dr.GetString(4), .TpDc_Codigo_St = Dr.GetString(5), .Dvt_VTSerie = Dr.GetString(6), .Dvt_VTNumer = Dr.GetString(7), .Version_Correlativo = Dr.GetInt32(8), .Dvt_Fecha_Emision = Dr.GetDateTime(9), .Dvt_Cod_Moneda = Dr.GetString(10), .Dvt_Total_IGV = Dr.GetDecimal(11), .Dvt_Total_Importe = Dr.GetDecimal(12), .Cliente_Documento_Tipo = Dr.GetString(13), .Cliente_Documento_Numero = Dr.GetString(14), .DigestValue = Dr.GetString(15), .SignatureValue = Dr.GetString(16), .Cliente_Correo_Electronico = Dr.GetString(17), .Emisor_RazonSocial_Nombre = Dr.GetString(18), .TpDc_Descripcion = Dr.GetString(19), .SQL_XML_Nombre = Dr.GetString(21)})
                        End While
                    End Using
                End Using
            End Using
            Return rCertificados
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Sub Insertar(ByVal pEntidad As eProgramacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_PROGRAMACIONES_ENVIO_INSERT", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PProgra_Correlativo", SqlDbType.Int).Direction = ParameterDirection.Output
                    Cmd.Parameters.Add("@PProgra_Descripcion", SqlDbType.VarChar, 100).Value = pEntidad.Progra_Descripcion
                    Cmd.Parameters.Add("@PProgra_Habilitado", SqlDbType.Bit).Value = pEntidad.Progra_Habilitado
                    Cmd.Parameters.Add("@PProgra_Appli_TipoDoc", SqlDbType.Char, 2).Value = pEntidad.Progra_Appli_TipoDoc
                    Cmd.Parameters.Add("@PProgra_Appli_Filtra_Serie", SqlDbType.Bit).Value = pEntidad.Progra_Appli_Filtra_Serie
                    Cmd.Parameters.Add("@PProgra_Appli_Serie", SqlDbType.Char, 4).Value = pEntidad.Progra_Appli_Serie
                    Cmd.Parameters.Add("@PProgra_EsResumenBoletas", SqlDbType.Bit).Value = pEntidad.Progra_EsResumenBoletas
                    Cmd.Parameters.Add("@PProgra_EsComunicacionBaja", SqlDbType.Bit).Value = pEntidad.Progra_EsComunicacionBaja
                    Cmd.Parameters.Add("@PProgra_FrecuEnvio_Hora_Es_Establecido", SqlDbType.Bit).Value = pEntidad.Progra_FrecuEnvio_Hora_Es_Establecido
                    Cmd.Parameters.Add("@PProgra_FrecuEnvio_Hora_Es_XCadaHora", SqlDbType.Bit).Value = pEntidad.Progra_FrecuEnvio_Hora_Es_XCadaHora
                    Cmd.Parameters.Add("@PProgra_FrecuEnvio_Hora_Es_DespuesEmitido", SqlDbType.Bit).Value = pEntidad.Progra_FrecuEnvio_Hora_Es_DespuesEmitido
                    Cmd.Parameters.Add("@PProgra_FrecuEnvio_Hora_Es_DespuesConfirmado", SqlDbType.Bit).Value = pEntidad.Progra_FrecuEnvio_Hora_Es_DespuesConfirmado
                    Cmd.Parameters.Add("@PProgra_FrecuEnvio_Hora_Desde", SqlDbType.Time).Value = pEntidad.Progra_FrecuEnvio_Hora_Desde
                    Cmd.Parameters.Add("@PProgra_FrecuEnvio_Hora_Hasta", SqlDbType.Time).Value = pEntidad.Progra_FrecuEnvio_Hora_Hasta
                    Cmd.Parameters.Add("@PProgra_FrecuEnvio_Hora_XCadaHora", SqlDbType.Int).Value = pEntidad.Progra_FrecuEnvio_Hora_XCadaHora

                    Cmd.Parameters.Add("@PProgra_Fecha_Desde", SqlDbType.Date).Value = pEntidad.Progra_Fecha_Desde
                    Cmd.Parameters.Add("@PProgra_Fecha_SinFin", SqlDbType.Bit).Value = pEntidad.Progra_Fecha_SinFin
                    Cmd.Parameters.Add("@PProgra_Fecha_Hasta", SqlDbType.Date).Value = IIf(pEntidad.Progra_Fecha_Hasta = Date.MinValue, Nothing, pEntidad.Progra_Fecha_Hasta)

                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                    pEntidad.Progra_Correlativo = Cmd.Parameters("@PProgra_Correlativo").Value
                End Using
            End Using
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Public Sub Actualizar(ByVal pEntidad As eProgramacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_PROGRAMACIONES_ENVIO_UPDATE", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PProgra_Correlativo", SqlDbType.Int).Value = pEntidad.Progra_Correlativo
                    Cmd.Parameters.Add("@PProgra_Descripcion", SqlDbType.VarChar, 100).Value = pEntidad.Progra_Descripcion
                    Cmd.Parameters.Add("@PProgra_Habilitado", SqlDbType.Bit).Value = pEntidad.Progra_Habilitado
                    Cmd.Parameters.Add("@PProgra_Appli_TipoDoc", SqlDbType.Char, 2).Value = pEntidad.Progra_Appli_TipoDoc
                    Cmd.Parameters.Add("@PProgra_Appli_Filtra_Serie", SqlDbType.Bit).Value = pEntidad.Progra_Appli_Filtra_Serie
                    Cmd.Parameters.Add("@PProgra_Appli_Serie", SqlDbType.Char, 4).Value = pEntidad.Progra_Appli_Serie
                    Cmd.Parameters.Add("@PProgra_EsResumenBoletas", SqlDbType.Bit).Value = pEntidad.Progra_EsResumenBoletas
                    Cmd.Parameters.Add("@PProgra_EsComunicacionBaja", SqlDbType.Bit).Value = pEntidad.Progra_EsComunicacionBaja
                    Cmd.Parameters.Add("@PProgra_FrecuEnvio_Hora_Es_Establecido", SqlDbType.Bit).Value = pEntidad.Progra_FrecuEnvio_Hora_Es_Establecido
                    Cmd.Parameters.Add("@PProgra_FrecuEnvio_Hora_Es_XCadaHora", SqlDbType.Bit).Value = pEntidad.Progra_FrecuEnvio_Hora_Es_XCadaHora
                    Cmd.Parameters.Add("@PProgra_FrecuEnvio_Hora_Es_DespuesEmitido", SqlDbType.Bit).Value = pEntidad.Progra_FrecuEnvio_Hora_Es_DespuesEmitido
                    Cmd.Parameters.Add("@PProgra_FrecuEnvio_Hora_Es_DespuesConfirmado", SqlDbType.Bit).Value = pEntidad.Progra_FrecuEnvio_Hora_Es_DespuesConfirmado
                    Cmd.Parameters.Add("@PProgra_FrecuEnvio_Hora_Desde", SqlDbType.Time).Value = pEntidad.Progra_FrecuEnvio_Hora_Desde
                    Cmd.Parameters.Add("@PProgra_FrecuEnvio_Hora_Hasta", SqlDbType.Time).Value = pEntidad.Progra_FrecuEnvio_Hora_Hasta
                    Cmd.Parameters.Add("@PProgra_FrecuEnvio_Hora_XCadaHora", SqlDbType.Int).Value = pEntidad.Progra_FrecuEnvio_Hora_XCadaHora

                    Cmd.Parameters.Add("@PProgra_Fecha_Desde", SqlDbType.Date).Value = pEntidad.Progra_Fecha_Desde
                    Cmd.Parameters.Add("@PProgra_Fecha_SinFin", SqlDbType.Bit).Value = pEntidad.Progra_Fecha_SinFin
                    Cmd.Parameters.Add("@PProgra_Fecha_Hasta", SqlDbType.Date).Value = IIf(pEntidad.Progra_Fecha_Hasta = Date.MinValue, Nothing, pEntidad.Progra_Fecha_Hasta)
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Public Sub Eliminar(ByVal pEntidad As eProgramacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_PROGRAMACIONES_ENVIO_DELETE", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_RUC", SqlDbType.Char, 11).Value = pEntidad.Emisor_RUC
                    Cmd.Parameters.Add("@PProgra_Correlativo", SqlDbType.Int).Value = pEntidad.Progra_Correlativo
                    Cn.Open()
                    Cmd.ExecuteNonQuery()
                End Using
            End Using
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Public Function Buscar_Lista_Pendientes_Resumen() As List(Of eProvisionFacturacion)
        Dim rCertificados As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection())
                Using Cmd As New SqlCommand("UP_FACTE_RESUMEN_DIARIO_PENDIENTES_CABECERA", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            rCertificados.Add(New eProvisionFacturacion With {.Emisor_RUC = Dr.GetString(0), .Emisor_RazonSocial_Nombre = Dr.GetString(1), .Dvt_Fecha_Generacion = Dr.GetDateTime(2), .Rs_Correlativo = Dr.GetInt32(3), .Dvt_Fecha_Emision = Dr.GetDateTime(4), .RootPath_Enterprise = Dr.GetString(5), .Etapa_Codigo = Dr.GetString(6)})
                        End While
                    End Using
                End Using
            End Using
            Return rCertificados
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Lista_Pendientes_Resumen_Documento(pResumen As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Dim rDocumentos As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection())
                Using Cmd As New SqlCommand("UP_FACTE_RESUMEN_DIARIO_PENDIENTES_DETALLE", Cn)
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pResumen.Emisor_RUC
                    Cmd.Parameters.Add("@PDoc_FechaGeneracion", SqlDbType.Date).Value = pResumen.Dvt_Fecha_Generacion
                    Cmd.Parameters.Add("@PDoc_Resumen_Correlativo", SqlDbType.Int).Value = pResumen.Rs_Correlativo
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            rDocumentos.Add(New eProvisionFacturacion With {.Rs_Correlativo = Dr.GetInt32(0), .Dvt_Cod_Moneda = Dr.GetString(1), .TpDc_Codigo_St = Dr.GetString(2), .Dvt_VTSerie = Dr.GetString(3), .Dvt_VTNumer = Dr.GetString(4), .Cliente_Documento_Numero = Dr.GetString(5), .Cliente_Documento_Tipo = Dr.GetString(6), .Modif_Doc_Serie_Numero = Dr.GetString(7), .Modif_Doc_Tipo = Dr.GetString(8), .EstadoDoc_Codigo = Dr.GetString(9), .Dvt_Total_Gravado = Dr.GetDecimal(10), .Dvt_Total_Exonerado = Dr.GetDecimal(11), .Dvt_Total_Inafecto = Dr.GetDecimal(12), .Dvt_Total_Gratuitas = Dr.GetDecimal(13), .Dvt_Total_ISC = Dr.GetDecimal(14), .Dvt_Total_IGV = Dr.GetDecimal(15), .Dvt_Total_Otros = Dr.GetDecimal(16), .Dvt_Total_Importe = Dr.GetDecimal(17), .SQL_XML_Nombre = Dr.GetString(19)})
                        End While
                    End Using
                End Using
            End Using
            Return rDocumentos
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Lista_Pendientes_Baja() As List(Of eProvisionFacturacion)
        Dim rCertificados As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection())
                Using Cmd As New SqlCommand("UP_FACTE_COMUNICACION_BAJA_PENDIENTES_ENVIO", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            rCertificados.Add(New eProvisionFacturacion With {.Emisor_RUC = Dr.GetString(0), .Emisor_RazonSocial_Nombre = Dr.GetString(1), .Dvt_Fecha_Generacion = Dr.GetDateTime(2), .Rs_Correlativo = Dr.GetInt32(3), .Dvt_Fecha_Emision = Dr.GetDateTime(4), .RootPath_Enterprise = Dr.GetString(5), .Grupo_Codigo = Dr.GetString(6)})
                        End While
                    End Using
                End Using
            End Using
            Return rCertificados
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Lista_Pendientes_Baja_Documento(pResumen As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Dim rDocumentos As New List(Of eProvisionFacturacion)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection())
                Using Cmd As New SqlCommand("UP_FACTE_COMUNICACION_BAJA_PENDIENTES_DETALLE", Cn)
                    Cmd.Parameters.Add("@PDoc_Emisor_RUC", SqlDbType.Char, 11).Value = pResumen.Emisor_RUC
                    Cmd.Parameters.Add("@PGrpTipo_Codigo", SqlDbType.Char, 3).Value = pResumen.Grupo_Codigo
                    Cmd.Parameters.Add("@PDoc_FechaGeneracion", SqlDbType.Date).Value = pResumen.Dvt_Fecha_Generacion
                    Cmd.Parameters.Add("@PDoc_Resumen_Correlativo", SqlDbType.Int).Value = pResumen.Rs_Correlativo
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            rDocumentos.Add(New eProvisionFacturacion With {.Rs_Correlativo = Dr.GetInt32(0), .Dvt_Cod_Moneda = Dr.GetString(1), .TpDc_Codigo_St = Dr.GetString(2), .Dvt_VTSerie = Dr.GetString(3), .Dvt_VTNumer = Dr.GetString(4), .Vnt_AnulacionRazon = Dr.GetString(5)})
                        End While
                    End Using
                End Using
            End Using
            Return rDocumentos
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
