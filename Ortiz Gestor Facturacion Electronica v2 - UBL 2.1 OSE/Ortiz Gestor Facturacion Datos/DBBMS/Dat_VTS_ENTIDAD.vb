﻿Imports System.Data.SqlClient
Public Class Dat_VTS_ENTIDAD
   
#Region "Direccion de entidad"
  
    Public Function Buscar(ByVal Clas_Enti As Ent_ENTIDAD_DIRECCION) As List(Of Ent_ENTIDAD_DIRECCION)
        Dim ListaItems As New List(Of Ent_ENTIDAD_DIRECCION)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnectionDBMBS())
                Using Cmd As New SqlCommand("paVTS_VTS_ENTIDAD_DIRECCION_Find", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    Cmd.Parameters.Add("@PEnt_cCodEntidad", SqlDbType.Char, 10).Value = IIf(String.IsNullOrWhiteSpace(Clas_Enti.Ent_cCodEntidad), DBNull.Value, Clas_Enti.Ent_cCodEntidad)
                    Cmd.Parameters.Add("@PTen_cTipoEntidad", SqlDbType.Char, 1).Value = Clas_Enti.Ten_cTipoEntidad
                    Cmd.Parameters.Add("@PDir_Codigo", SqlDbType.Char, 4).Value = Clas_Enti.Dir_Codigo
                    Cmd.Parameters.Add("@PEnt_DNIRUC", SqlDbType.VarChar, 15).Value = IIf(String.IsNullOrWhiteSpace(Clas_Enti.Ent_cDNIRUC), DBNull.Value, Clas_Enti.Ent_cDNIRUC)
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read
                            ListaItems.Add(New Ent_ENTIDAD_DIRECCION With {.Ent_cCodEntidad = Dr.GetString(0),
                                                                           .Ten_cTipoEntidad = Dr.GetString(1),
                                                                           .Dir_Codigo = Dr.GetString(2),
                                                                           .Dir_Cod_TipoVia = Dr.GetString(3),
                                                                           .Dir_Ofi_TipoVia = Dr.GetString(4),
                                                                           .Dir_Des_TipoVia = Dr.GetString(5),
                                                                           .Dir_Nomb_TipVia = Dr.GetString(6),
                                                                           .Dir_Num_TipVia = Dr.GetString(7),
                                                                           .Dir_Inte_TipVia = Dr.GetString(8),
                                                                           .Dir_Cod_Zona = Dr.GetString(9),
                                                                           .Dir_Ofi_Zona = Dr.GetString(10),
                                                                           .Dir_Des_Zona = Dr.GetString(11),
                                                                           .Dir_Nomb_Zona = Dr.GetString(12),
                                                                           .Dir_Referencia = Dr.GetString(13),
                                                                           .Dir_Telef = Dr.GetString(14),
                                                                           .Dir_Fax = Dr.GetString(15),
                                                                           .Dir_Pais = Dr.GetString(16),
                                                                           .Dir_Depart = Dr.GetString(17),
                                                                           .Dir_Provin = Dr.GetString(18),
                                                                           .Dir_Distri = Dr.GetString(19),
                                                                           .Dir_Distri_Ds = Dr.GetString(20),
                                                                          .Direccion_Completa = Dr.GetString(21)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Sub ModificarDireccionCompleta(ByVal Clas_Enti As Ent_ENTIDAD_DIRECCION)
        Dim Trs As SqlTransaction = Nothing
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("paVTS_VTS_ENTIDAD_DIRECCION_Update_Completo", Cn)
                    Cmd.CommandType = CommandType.StoredProcedure
                    'Cmd.Parameters.Add("@PEmp_cCodigo", SqlDbType.Char, 3).Value = DatosActualConexion.CodigoEmpresa
                    Cmd.Parameters.Add("@PEnt_cCodEntidad", SqlDbType.Char, 10).Value = Clas_Enti.Ent_cCodEntidad
                    Cmd.Parameters.Add("@PDir_Codigo", SqlDbType.Char, 4).Value = Clas_Enti.Dir_Codigo
                    Cmd.Parameters.Add("@PAutor", SqlDbType.VarChar).Value = DatosActualConexion.UserName
                    Cmd.Parameters.Add("@PMachine", SqlDbType.VarChar).Value = DatosActualConexion.MachineCurrent
                    Cmd.Parameters.Add("@PMed_Direccion_Completa", SqlDbType.VarChar, 200).Value = Clas_Enti.Direccion_Completa
                    Cn.Open()
                    Trs = Cn.BeginTransaction()
                    Cmd.Transaction = Trs
                    Cmd.ExecuteNonQuery()
                    Trs.Commit()
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
#End Region
End Class
