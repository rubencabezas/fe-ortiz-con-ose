﻿Imports System.Data.SqlClient

Public Class Dat_FACTE_SOCIO_NEGOCIO
    Public Function Buscar(ByVal pEntidad As eSocioNegocio) As List(Of eSocioNegocio)
        Dim ListaItems As New List(Of eSocioNegocio)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_SOCIO_NEGOCIO_BUSQUEDA", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PReceptor_TipoDocIdent_Codigo", SqlDbType.Char, 1).Value = pEntidad.TipoDocIdent_Codigo
                    Cmd.Parameters.Add("@PReceptor_TipoDocIdent_Numero", SqlDbType.VarChar, 11).Value = pEntidad.SocNeg_TipoDocIdent_Numero
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read()
                            ListaItems.Add(New eSocioNegocio With {.TipoDocIdent_Codigo = Dr.GetString(0), .TipoDocIdent_Corto = Dr.GetString(1), .SocNeg_TipoDocIdent_Numero = Dr.GetString(2), .SocNeg_RazonSocialNombres = Dr.GetString(3)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
