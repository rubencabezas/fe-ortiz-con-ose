﻿Imports System.Data.SqlClient

Public Class Dat_EMPRESA
    Public Function Buscar(ByVal pEntidad As eEmpresa) As List(Of eEmpresa)
        Dim ListaItems As New List(Of eEmpresa)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_EMPRESA_LISTA", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cmd.Parameters.Add("@PEmp_RUC", SqlDbType.Char, 11).Value = pEntidad.Emp_NumRuc
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read()
                            'ListaItems.Add(New eEmpresa With {.Emp_NumRuc = Dr.GetString(0), .Emp_NombreRazonSocial = Dr.GetString(1), .Emp_EnviaResumenBoletas = Dr.GetBoolean(2), .Sunat_Usuario = Dr.GetString(3), .Sunat_Contrasena = Dr.GetString(4), .Sunat_Autorizacion = Dr.GetString(5), .Emisor_Web_Visualizacion = Dr.GetString(6), .Correo_Usuario = Dr.GetString(7), .Correo_Contrasena = Dr.GetString(8), .Correo_ServidorSMTP = Dr.GetString(9), .Correo_ServidorPuerto = Dr.GetString(10), .Correo_ServidorSSL = Dr.GetBoolean(11), .Etapa_Codigo = Dr.GetString(12), .Etapa_Descripcion = Dr.GetString(13), .PathLocal = Dr.GetString(14), .PathServer = Dr.GetString(15), .FTP_Direccion = Dr.GetString(16), .FTP_Carpeta = Dr.GetString(17), .FTP_Usuario = Dr.GetString(18), .FTP_Contrasena = Dr.GetString(19), .ActualizarClientes = Dr.GetBoolean(20)})
                            ListaItems.Add(New eEmpresa With {.Emp_NumRuc = Dr.GetString(0), .Emp_NombreRazonSocial = Dr.GetString(1), .Sunat_Usuario = Dr.GetString(2), .Sunat_Contrasena = Dr.GetString(3), .Sunat_Autorizacion = Dr.GetString(4), .Emisor_Web_Visualizacion = Dr.GetString(5), .Correo_Usuario = Dr.GetString(6), .Correo_Contrasena = Dr.GetString(7), .Correo_ServidorSMTP = Dr.GetString(8), .Correo_ServidorPuerto = Dr.GetString(9), .Correo_ServidorSSL = Dr.GetBoolean(10), .Etapa_Codigo = Dr.GetString(11), .Etapa_Descripcion = Dr.GetString(12), .Path_Root_App = Dr.GetString(13), .FTP_Direccion = Dr.GetString(14), .FTP_Carpeta = Dr.GetString(15), .FTP_Usuario = Dr.GetString(16), .FTP_Contrasena = Dr.GetString(17), .ActualizarClientes = Dr.GetBoolean(18)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
