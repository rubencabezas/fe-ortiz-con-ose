﻿Imports System.Data.SqlClient

Public Class Dat_FACTE_DOCUMENTO_TIPO_GRUPO_URL
    Public Function Buscar() As List(Of FACTE_DOCUMENTO_TIPO_GRUPO_URL)
        Dim ListaItems As New List(Of FACTE_DOCUMENTO_TIPO_GRUPO_URL)
        Try
            Using Cn As New SqlConnection(Conexion.ObtenerConnection)
                Using Cmd As New SqlCommand("UP_FACTE_DOCUMENTO_TIPO_GRUPO_URL_LISTA", Cn) With {.CommandType = CommandType.StoredProcedure}
                    Cn.Open()
                    Using Dr As SqlDataReader = Cmd.ExecuteReader()
                        While Dr.Read()
                            ListaItems.Add(New FACTE_DOCUMENTO_TIPO_GRUPO_URL With {.GrpTipo_Codigo = Dr.GetString(0), .Etapa_Codigo = Dr.GetString(1), .GrpTipo_URL = Dr.GetString(2), .GrpTipo_URL_Validez = Dr.GetString(3), .GrpTipo_URL_Estado_Envio = Dr.GetString(4)})
                        End While
                    End Using
                End Using
            End Using
            Return ListaItems
        Catch SqlEx As SqlException
            Throw New Exception(SqlEx.Message)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
