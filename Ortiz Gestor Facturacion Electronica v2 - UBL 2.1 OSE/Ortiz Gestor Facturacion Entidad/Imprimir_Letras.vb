﻿Public Class Imprimir_Letras
    Public Shared Function NroEnLetras(ByVal curNumero As Double, Optional ByVal blnO_Final As Boolean = True) As String
        Dim dblCentavos As Double
        Dim lngContUnit As Long
        Dim lngContDec As Long
        Dim lngContCent As Long
        Dim lngContMil As Long
        Dim lngContMillon As Long
        Dim strNumLetras As String = ""
        Dim strNumero(20) As String
        Dim strDecenas(10) As String
        Dim strCentenas(9) As String
        Dim blnNegativo As Boolean
        Dim blnPlural As Boolean


        If Int(curNumero) = 0.0# Then
            strNumLetras = "CERO"
        End If

        strNumero(0) = ""
        strNumero(1) = "UN"
        strNumero(2) = "DOS"
        strNumero(3) = "TRES"
        strNumero(4) = "CUATRO"
        strNumero(5) = "CINCO"
        strNumero(6) = "SEIS"
        strNumero(7) = "SIETE"
        strNumero(8) = "OCHO"
        strNumero(9) = "NUEVE"
        strNumero(10) = "DIEZ"
        strNumero(11) = "ONCE"
        strNumero(12) = "DOCE"
        strNumero(13) = "TRECE"
        strNumero(14) = "CATORCE"
        strNumero(15) = "QUINCE"
        strNumero(16) = "DIECISEIS"
        strNumero(17) = "DIECISIETE"
        strNumero(18) = "DIECIOCHO"
        strNumero(19) = "DIECINUEVE"
        strNumero(20) = "VEINTE"

        strDecenas(0) = ""
        strDecenas(1) = ""
        strDecenas(2) = "VEINTI"
        strDecenas(3) = "TREINTA"
        strDecenas(4) = "CUARENTA"
        strDecenas(5) = "CINCUENTA"
        strDecenas(6) = "SESENTA"
        strDecenas(7) = "SETENTA"
        strDecenas(8) = "OCHENTA"
        strDecenas(9) = "NOVENTA"
        strDecenas(10) = "CIEN"

        strCentenas(0) = ""
        strCentenas(1) = "CIENTO"
        strCentenas(2) = "DOSCIENTOS"
        strCentenas(3) = "TRESCIENTOS"
        strCentenas(4) = "CUATROCIENTOS"
        strCentenas(5) = "QUINIENTOS"
        strCentenas(6) = "SEISCIENTOS"
        strCentenas(7) = "SETECIENTOS"
        strCentenas(8) = "OCHOCIENTOS"
        strCentenas(9) = "NOVECIENTOS"

        If curNumero < 0.0# Then
            blnNegativo = True
            curNumero = Math.Abs(curNumero)
        End If

        If Int(curNumero) <> curNumero Then
            dblCentavos = Math.Abs(curNumero - Int(curNumero))
            curNumero = Int(curNumero)
        End If

        Do While curNumero >= 1000000.0#
            lngContMillon = lngContMillon + 1
            curNumero = curNumero - 1000000.0#
        Loop

        Do While curNumero >= 1000.0#
            lngContMil = lngContMil + 1
            curNumero = curNumero - 1000.0#
        Loop

        Do While curNumero >= 100.0#
            lngContCent = lngContCent + 1
            curNumero = curNumero - 100.0#
        Loop


        If Not (curNumero > 10.0# And curNumero <= 20.0#) Then
            Do While curNumero >= 10.0#
                lngContDec = lngContDec + 1
                curNumero = curNumero - 10.0#
            Loop
        End If


        Do While curNumero >= 1.0#
            lngContUnit = lngContUnit + 1
            curNumero = curNumero - 1.0#
        Loop

        If lngContMillon > 0 Then
            If lngContMillon >= 1 Then   'si el número es >1000000 usa recursividad
                strNumLetras = NroEnLetras(lngContMillon, False)
                If Not blnPlural Then blnPlural = (lngContMillon > 1)
                lngContMillon = 0
            End If
            strNumLetras = Trim(strNumLetras) & strNumero(lngContMillon) & " MILLON" & IIf(blnPlural, "ES ", " ")
        End If

        If lngContMil > 0 Then
            If lngContMil >= 1 Then   'si el número es >100000 usa recursividad
                strNumLetras = strNumLetras & NroEnLetras(lngContMil, False)
                lngContMil = 0
            End If
            strNumLetras = Trim(strNumLetras) & strNumero(lngContMil) & " MIL "
        End If

        If lngContCent > 0 Then
            If lngContCent = 1 And lngContDec = 0 And lngContUnit = 0.0# Then
                strNumLetras = strNumLetras & "CIEN"
            Else
                strNumLetras = strNumLetras & strCentenas(lngContCent) & " "
            End If
        End If

        If lngContDec >= 1 Then
            If lngContDec = 1 Then
                strNumLetras = strNumLetras & strNumero(10)
            ElseIf lngContDec = 2 And lngContUnit = 0.0# Then
                strNumLetras = strNumLetras & strNumero(20)
            Else
                strNumLetras = strNumLetras & strDecenas(lngContDec)
            End If
            If lngContDec >= 3 And lngContUnit > 0.0# Then
                strNumLetras = strNumLetras & " Y "
            End If
        End If

        If lngContUnit >= 0 Then
            If Not blnO_Final Then
                If lngContUnit >= 0.0# And lngContUnit <= 20.0# Then
                    strNumLetras = strNumLetras & strNumero(lngContUnit)
                    If lngContUnit = 1.0# And blnO_Final Then
                        strNumLetras = strNumLetras & "O"
                    End If
                    NroEnLetras = strNumLetras
                    Exit Function
                End If
            Else
                If lngContUnit >= 0.0# And lngContUnit <= 20.0# Then
                    strNumLetras = strNumLetras & strNumero(lngContUnit)
                    If lngContUnit = 1.0# And blnO_Final Then
                        strNumLetras = strNumLetras & "O"
                    End If
                    If dblCentavos >= 0.0# Then
                        strNumLetras = strNumLetras & " CON " + Format$(CInt(dblCentavos * 100.0#), "00") & "/100"
                    End If
                    NroEnLetras = strNumLetras
                    Exit Function
                End If
            End If
        End If
        If lngContUnit >= 0.0# Then
            If lngContUnit > 0 Then
                strNumLetras = strNumLetras & NroEnLetras(lngContUnit, True)
            End If
        End If


        NroEnLetras = IIf(blnNegativo, "(" & strNumLetras & ")", strNumLetras)
    End Function
End Class
