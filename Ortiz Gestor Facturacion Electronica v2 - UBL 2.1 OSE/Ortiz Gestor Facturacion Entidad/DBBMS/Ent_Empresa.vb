﻿Public Class Ent_Empresa
    Property Codigo As String
    Property Nombre As String
    Property NombreCorto As String
    Property Direccion As String
    Property Ruc As String

    Property User As String
    Property Conceder As Boolean 'Necesario para cargar las empresas concedidas
    Property Emp_DireccionCorta As String
    Property Emp_Urbanizacion As String
    Property Emp_Direccion_Departamento As String
    Property Emp_Direccion_Provincia As String
    Property Emp_Direccion_Distrito As String
    Sub New()

    End Sub
    'Sub New(ByVal c_Codigo As String, ByVal C_Nombre As String, ByVal c_NombreCorto As String, ByVal c_Direccion As String, ByVal c_Ruc As String)
    '    Codigo = c_Codigo
    '    Nombre = C_Nombre
    '    NombreCorto = c_NombreCorto
    '    Direccion = c_Direccion
    '    Ruc = c_Ruc

    'End Sub
    Sub New(ByVal c_Codigo As String, ByVal C_Nombre As String, ByVal c_NombreCorto As String, ByVal c_Direccion As String, ByVal c_Ruc As String, c_Emp_DireccionCorta As String, c_Emp_Urbanizacion As String, c_Emp_Direccion_Departamento As String, c_Emp_Direccion_Provincia As String, c_Emp_Direccion_Distrito As String)
        Codigo = c_Codigo
        Nombre = C_Nombre
        NombreCorto = c_NombreCorto
        Direccion = c_Direccion
        Ruc = c_Ruc
        Emp_DireccionCorta = c_Emp_DireccionCorta
        Emp_Urbanizacion = c_Emp_Urbanizacion
        Emp_Direccion_Departamento = c_Emp_Direccion_Departamento
        Emp_Direccion_Provincia = c_Emp_Direccion_Provincia
        Emp_Direccion_Distrito = c_Emp_Direccion_Distrito
    End Sub
    Sub New(ByVal c_Codigo As String, ByVal C_Nombre As String, ByVal C_Conceder As Boolean)
        Codigo = c_Codigo
        Nombre = C_Nombre
        NombreCorto = ""
        Direccion = ""
        Ruc = ""
        Conceder = C_Conceder
    End Sub
    Property Cod_Autorizacion As String
End Class
