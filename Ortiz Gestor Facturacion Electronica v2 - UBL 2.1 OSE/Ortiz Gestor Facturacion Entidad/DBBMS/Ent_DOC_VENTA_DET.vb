﻿Public Class Ent_DOC_VENTA_DET
    Property Emp_cCodigo As String
    Property Pan_cAnio As String
    Property Per_cPeriodo As String
    ReadOnly Property Per_Descripcion As String
        Get
            If Per_cPeriodo = "01" Then
                Return Per_cPeriodo & " Enero"
            ElseIf Per_cPeriodo = "02" Then
                Return Per_cPeriodo & " Febrero"
            ElseIf Per_cPeriodo = "03" Then
                Return Per_cPeriodo & " Marzo"
            ElseIf Per_cPeriodo = "04" Then
                Return Per_cPeriodo & " Abril"
            ElseIf Per_cPeriodo = "05" Then
                Return Per_cPeriodo & " Mayo"
            ElseIf Per_cPeriodo = "06" Then
                Return Per_cPeriodo & " Junio"
            ElseIf Per_cPeriodo = "07" Then
                Return Per_cPeriodo & " Julio"
            ElseIf Per_cPeriodo = "08" Then
                Return Per_cPeriodo & " Agosto"
            ElseIf Per_cPeriodo = "09" Then
                Return Per_cPeriodo & " Setiembre"
            ElseIf Per_cPeriodo = "10" Then
                Return Per_cPeriodo & " Octubre"
            ElseIf Per_cPeriodo = "11" Then
                Return Per_cPeriodo & " Noviembre"
            ElseIf Per_cPeriodo = "12" Then
                Return Per_cPeriodo & " Diciembre"
            ElseIf Per_cPeriodo = "00" Then
                Return Per_cPeriodo & " Apertura"
            Else
                Return Per_cPeriodo & " Otros"
            End If
        End Get
    End Property
    Property Dvt_Codigo As String
    Property DvtD_NumItem As Integer

    Property DvtD_TipoCod As String
    Property DvtD_TipoOfi As String
    Property DvtD_TipoDes As String

    Property DvtD_AlmaCod As String
    Property DvtD_AlmaDes As String

    Property CTC_Codigo As String
    Property CTC_DescripcionItem As String
    Property CTC_DescripcionAdicional As String
    Property DvtD_UnidadDs As String
    Property DvtD_UnidadCod As String
    Property UNM_CoigodFE As String

    Property DvtD_Cantidad As Double
    'Property DvtD_PrecioMain As Ent_VALOR_VENTA 'for playing price max and min

    Property Valor_Precio As Decimal

    Property DvtD_CodTipoCentro As String
    Property DvtD_OfiTipoCentro As String
    Property DvtD_NomTipoCentro As String
    Property DvtD_CodCentro As String
    Property DvtD_NomCentro As String
    Property DvtD_CodUnidMaquinaria As String
    Property DvtD_NomUnidMaquinaria As String

    Property DvtD_ValorVenta As Double 'Valor de venta Contado

    Property DvtD_ValorEspecial As Double




    Property TipoPrecio As String 'CONTADO - ESPECIAL

    Property DvtD_IGVTasaPorcentaje As Double 'Tasa IGV, por defecto es 0.0
    ReadOnly Property DvtD_IGVTasa As Double 'Tasa IGV, por defecto es 0.0
        Get

            Return Math.Round(DvtD_IGVTasaPorcentaje / 100, 2)
        End Get
    End Property

    ReadOnly Property DvtD_TotalValorVenta As Double
        Get
            Return Math.Round(DvtD_ValorVenta * ((100 - DvtD_DsctoUnitTasaPorcentaje - DvtD_DsctoGralTasaPorcentaje) / 100), 4)
        End Get
    End Property

    Property DvtD_DsctoUnitTasaPorcentaje As Double

    Property DvtD_DsctoGralTasaPorcentaje As Double

    Property ResultadoEspecial As Double

    ReadOnly Property DvtD_PrecUnitario As Double
        Get
            Return Math.Round(DvtD_ValorVenta * ((100 - DvtD_DsctoUnitTasaPorcentaje - DvtD_DsctoGralTasaPorcentaje) / 100) * (1 + DvtD_IGVTasa), 3)
        End Get
    End Property

    ReadOnly Property DvtD_IGVUNitario As Double
        Get
            If DvtD_OperCodigo = "0565" Then
                'Return Math.Round(DvtD_PrecUnitario * DvtD_IGVTasa, 2)
                Return Math.Round(DvtD_TotalValorVenta * DvtD_IGVTasa, 4)
            Else
                Return 0
            End If
        End Get
    End Property

    ReadOnly Property DvtD_SubTotal As Double
        Get
            Return Math.Round(DvtD_MontoTotal / (1 + DvtD_IGVTasa), 2)
            'Return Math.Round(DvtD_TotalValorVenta * DvtD_Cantidad, 2)
        End Get
    End Property

    ReadOnly Property DvtD_IGVMonto As Double
        Get
            If DvtD_OperCodigo = "0565" Then
                Return Math.Round((DvtD_MontoTotal / (1 + DvtD_IGVTasa)) * DvtD_IGVTasa, 2)
                'Return Math.Round(DvtD_SubTotal * DvtD_IGVTasa, 2)
            Else
                Return 0
            End If
        End Get
    End Property
    ReadOnly Property DvtD_IGVMonto_Cabecera As Double
        Get
            If DvtD_OperCodigo = "0565" Then
                Return Math.Round((DvtD_MontoTotal / (1 + DvtD_IGVTasa)) * DvtD_IGVTasa, 3)
                'Return Math.Round(DvtD_SubTotal * DvtD_IGVTasa, 2)
            Else
                Return 0
            End If
        End Get
    End Property

    ReadOnly Property DvtD_MontoTotal As Double
        Get

            If DvtD_OperCodigo = "0565" Then
                Dim Redondeo, Total As Decimal 'cuando hay diferencia de 0.01 es decir 10.99=11 
                'If CTC_Codigo = "00000007" Then
                '    If Valor_Precio <> 0 Then
                '        Total = Valor_Precio
                '        Return Total
                '    Else
                '        Total = Math.Round(DvtD_PrecUnitario * DvtD_Cantidad, 2)
                '        Return Total
                '    End If
                If Valor_Precio <> 0 Then
                    Total = Valor_Precio
                    Return Total
                Else
                    Total = Math.Round(DvtD_PrecUnitario * DvtD_Cantidad, 2)
                    Redondeo = Math.Truncate(DvtD_PrecUnitario * DvtD_Cantidad)
                    If Total = (Redondeo + 0.99) Then
                        Return Total + 0.01
                        'ElseIf Total = (Redondeo + 0.01) Then
                        '    Return Total - 0.01
                    Else
                        Return Total
                    End If
                End If
                'Return Math.Round(DvtD_PrecUnitario * DvtD_Cantidad, 2)
                'Return Math.Round(DvtD_SubTotal + DvtD_IGVMonto, 2)
            Else
                Return Math.Round(DvtD_TotalValorVenta * DvtD_Cantidad, 2)
                'Return Math.Round(DvtD_SubTotal, 2)
            End If

        End Get
    End Property

    Property DvtD_CantidadSaldo As Double 'era DvtD_ImporteSaldo cambiado el 06/08/2012
    Property DvtD_IncluirAhora As Double 'El monto de este detalle q se va incluir en la venta
    Property DvtD_CorrelativoPedido As String

    Property DvtD_CcbleCodigo As String
    Property DvtD_CcbleOficial As String
    Property DvtD_CcbleDescripcion As String

    Property DvtD_OperCodigo As String
    Property DvtD_OperOficial As String
    Property DvtD_OperDescripcion As String
    Property Tan_Codigo_Tanque As String
    Property Sur_Codigo_Surtidor As String
    Property Contometro_Calculo As Decimal


    ReadOnly Property DvtD_ValorFactExportacion As Double
        Get
            If DvtD_OperCodigo = "0564" Then
                Return DvtD_SubTotal
            ElseIf DvtD_OperCodigo = "0565" Then
                Return 0
            ElseIf DvtD_OperCodigo = "0566" Then
                Return 0
            ElseIf DvtD_OperCodigo = "0567" Then
                Return 0
            Else
                Return 0
            End If
        End Get
    End Property
    ReadOnly Property DvtD_BaseImpobleOperacionGravada As Double
        Get
            If DvtD_OperCodigo = "0564" Then
                Return 0
            ElseIf DvtD_OperCodigo = "0565" Then
                Return DvtD_SubTotal 'Format(DvtD_Importe - DvtD_IGV_IPM, "#" & Mil & "##0" & Dec & "00")
            ElseIf DvtD_OperCodigo = "0566" Then
                Return 0
            ElseIf DvtD_OperCodigo = "0567" Then
                Return 0
            Else
                Return 0
            End If
        End Get
    End Property
    ReadOnly Property DvtD_Exonerada As Double
        Get
            If DvtD_OperCodigo = "0564" Then
                Return 0
            ElseIf DvtD_OperCodigo = "0565" Then
                Return 0
            ElseIf DvtD_OperCodigo = "0566" Then
                Return DvtD_SubTotal
            ElseIf DvtD_OperCodigo = "0567" Then
                Return 0
            Else
                Return 0
            End If
        End Get
    End Property

    ReadOnly Property DvtD_Inafecta As Double
        Get
            If DvtD_OperCodigo = "0564" Then
                Return 0
            ElseIf DvtD_OperCodigo = "0565" Then
                Return 0
            ElseIf DvtD_OperCodigo = "0566" Then
                Return 0
            ElseIf DvtD_OperCodigo = "0567" Then
                Return DvtD_SubTotal
            Else
                Return 0
            End If
        End Get
    End Property
    ReadOnly Property DvtD_ISC As Double
        Get
            If DvtD_OperCodigo = "0564" Then
                Return 0
            ElseIf DvtD_OperCodigo = "0565" Then
                Return 0
            ElseIf DvtD_OperCodigo = "0566" Then
                Return 0
            ElseIf DvtD_OperCodigo = "0567" Then
                Return 0
            Else
                Return 0
            End If
        End Get
    End Property
    ReadOnly Property DvtD_IGV_IPM As Double
        Get
            If DvtD_OperCodigo = "0564" Then
                Return 0
            ElseIf DvtD_OperCodigo = "0565" Then
                Return DvtD_IGVMonto 'Format((DvtD_IGVMonto * DvtD_Cantidad), "#" & Mil & "##0" & Dec & "00")
            ElseIf DvtD_OperCodigo = "0566" Then
                Return 0
            ElseIf DvtD_OperCodigo = "0567" Then
                Return 0
            Else
                Return 0
            End If
        End Get
    End Property

    Property MyOrigen As String = ""
    'Para los pedidos
    Property Ped_Origen As String = ""
    Property Ped_Anio As String
    Property Ped_Periodo As String
    Property Ped_Correlativo As String
    Property Ped_Item As String
    ReadOnly Property EsDePedido As String
        Get
            If String.IsNullOrEmpty(Ped_Correlativo) Then
                Return ""
            Else
                Return "P"
            End If
        End Get
    End Property
    ReadOnly Property EsPedido As Boolean
        Get
            If EsDePedido = "P" Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    Property MedioPagCodigo As String
    Property MedioPagSunat As String
    Property MedioPagDescripcion As String
    Property TarjetaCodigo As String
    Property TarjetaDescripcion As String
    Property TarjetaCtaCble As String
    Property TarjetaTipoCodigo As String
    Property TarjetaTipoDescripcion As String
    Property TarjetaNumero As String
    Property TarjetaLote As String
    Property MedioReferencia As String
    Property MedioImporte As Double

    Property TarjetaLibro As String = ""
    Property TarjetaInterno As String = ""
    Property TarjetaVoucher As String = ""


    Property PuntoVenta_Descripcion As String
    Property Moneda_Descripcion As String
    Property Condicion_Descripcion As String
    Property TipDoc_Sunat As String
    Property TipDoc_Descripcion As String
    Property TipDoc_Serie As String
    Property TipDoc_Numero As String
    Property Fecha_Doc As Date
    Property Cliente_Ruc As String
    Property Cliente_Nombres As String

    'Pedidos
    Property Dvt_PersCntCsto As String
    Property Dvt_PersVehiPla As String
    Sub New()
    End Sub
    Sub New(ByVal c_Emp_cCodigo As String, ByVal c_Pan_cAnio As String, ByVal c_Dvt_Codigo As String, ByVal c_DvtD_NumItem As Integer, ByVal c_CTC_Codigo As String, ByVal c_CTC_DescripcionItem As String, ByVal c_DvtD_Unidad As String, ByVal c_Cpt_Codigo As String, ByVal c_DvtD_Cantidad As Double, ByVal c_DvtD_ValorVenta As Double, ByVal c_DvtD_IGVTasa As Double, ByVal c_DvtD_IGVMonto As Double, ByVal c_DvtD_DsctoTasa As Double, ByVal c_DvtD_PrecNeto As Double, ByVal c_DvtD_Importe As Double, ByVal c_DvtD_PrecNeto_MN As Double, ByVal c_DvtD_Importe_MN As Double)
        Emp_cCodigo = c_Emp_cCodigo
        Pan_cAnio = c_Pan_cAnio
        Dvt_Codigo = c_Dvt_Codigo
        DvtD_NumItem = c_DvtD_NumItem
        CTC_Codigo = c_CTC_Codigo
        CTC_DescripcionItem = c_CTC_DescripcionItem
        DvtD_UnidadCod = c_DvtD_Unidad
        DvtD_Cantidad = c_DvtD_Cantidad
        DvtD_ValorVenta = c_DvtD_ValorVenta
    End Sub

    Property LadoCodigo As String
    Property Unm_Descripcion As String
    Property Lado_Descripcion As String
    Property Producto_Descripcion As String
    Public Function Truncate(ByVal number As Decimal) As Decimal
        Dim Multiplicador = Math.Pow(10, 4)
        Return Math.Truncate(number * Multiplicador) / Multiplicador
    End Function


    '''''''
    Property VntD_IGVMonto As Decimal
    Property VntD_PrecNeto As Decimal
    Property VntD_Importe As Decimal
    Property VntD_Operaci As String
    Property TAD_CODIGO_OFICIAL As String
    Property TAD_DESCRIPCION As String
    Property VntD_ConcCble As String
    Property Alm_Codigo As String
    Property ALM_Descripcion As String
    Property VntD_TipoBSA As String
    Property TAD_CODIGO_OFICIALBien As String
    Property TAD_DESCRIPCIONBien As String
    Property VntD_Est_Fila As String
    Property OtroPrd As Boolean = False

    Property DvtD_Importe_2D As Decimal

    Property ProductoIngreso As String

    Property IDDespacho As Integer


End Class
