﻿Public Class Log_VTS_TIPO_DOC
    Private CapDato_Ent As New Dat_VTS_TIPO_DOC
    Public Function Buscar(ByVal Cls_Enti As eTipoDocumento) As List(Of eTipoDocumento)
        Try
            Return CapDato_Ent.Buscar(Cls_Enti)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
