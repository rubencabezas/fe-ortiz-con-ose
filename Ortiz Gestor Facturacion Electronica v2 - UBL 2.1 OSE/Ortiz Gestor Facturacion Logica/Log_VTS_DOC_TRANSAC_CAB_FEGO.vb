﻿Public Class Log_VTS_DOC_TRANSAC_CAB_FEGO
    Private CapDato_Ent As New Dat_VTS_DOC_TRANSAC_CAB_FEGO
    Public Function Buscar_DocVenta(ByVal Cls_Enti As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Try
            Return CapDato_Ent.Buscar_DocVenta(Cls_Enti)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_DocDetalle(ByVal Cls_Enti As eProvisionFacturacion) As List(Of eProvisionFacturacionDetalle)
        Try
            Return CapDato_Ent.Buscar_DocDetalle(Cls_Enti)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Resumen_DocVenta(ByVal Cls_Enti As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Try
            Return CapDato_Ent.Buscar_Resumen_DocVenta(Cls_Enti)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Retencion_DocVenta(ByVal Cls_Enti As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Try
            Return CapDato_Ent.Buscar_Retencion_DocVenta(Cls_Enti)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Percepcion_DocVenta(ByVal Cls_Enti As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Try
            Return CapDato_Ent.Buscar_Percepcion_DocVenta(Cls_Enti)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
