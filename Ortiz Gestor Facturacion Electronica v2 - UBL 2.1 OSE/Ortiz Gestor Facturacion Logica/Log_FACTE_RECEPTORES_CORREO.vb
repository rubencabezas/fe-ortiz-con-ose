﻿Public Class Log_FACTE_RECEPTORES_CORREO
    Private cDato As New Dat_FACTE_RECEPTORES_CORREO
    Public Function Buscar() As List(Of eReceptor)
        Try
            Return cDato.Buscar()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Cambiar_Estado(pEntidad As eReceptor) As Boolean
        Try
            cDato.Cambiar_Estado(pEntidad)
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Lista(pEntidad As eReceptor) As List(Of eReceptor)
        Try
            Return cDato.Buscar_Lista(pEntidad)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
