﻿Public Class Log_Configuracion_App
    Private cDato As New Dat_Configuracion_App
    Public Function Insertar(ByVal pEntidad As eConfiguracionApp) As Boolean
        Try
            cDato.Insertar(pEntidad)
        Catch ex As Exception
            'Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "Log_FACTE_EMPRESA-INS", ex.Message)
        End Try
        Return True
    End Function
    Public Function Buscar() As List(Of eConfiguracionApp)
        Try
            Return cDato.Buscar()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
