﻿Public Class Log_VTS_MONEDA
    Private CapDatoMon As New Dat_VTS_MONEDA
    Public Function Buscar(ByVal Moneda As Ent_MONEDA) As List(Of Ent_MONEDA)
        Try
            Return CapDatoMon.Buscar(Moneda)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function BuscarAll() As List(Of Ent_MONEDA)
        Try
            Return CapDatoMon.BuscarMonedasAll()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
