--DB_FEGO
--USE master;
--GO
--DENY VIEW ANY DATABASE TO fegosa; 
GO
USE DB_FEGO;
GO
--DROP USER fegosa;
--GO
--USE master;
--GO
--ALTER AUTHORIZATION ON DATABASE::DB_FEGO TO fegosa;
GO
CREATE TABLE FACTE_MONEDA
(
	Mon_Codigo CHAR(3),
	Mon_Descripcion VARCHAR(50),
	Mon_Simbolo VARCHAR(4),
	CONSTRAINT PK_FACTE_MONEDA PRIMARY KEY CLUSTERED 
	(
		Mon_Codigo ASC
	)
)
GO
INSERT INTO dbo.FACTE_MONEDA ( Mon_Codigo , Mon_Descripcion , Mon_Simbolo ) VALUES ( 'PEN' , 'SOLES' ,  'S/' );
INSERT INTO dbo.FACTE_MONEDA ( Mon_Codigo , Mon_Descripcion , Mon_Simbolo ) VALUES ( 'USD' , 'DOLARES' ,  'US$.' );
INSERT INTO dbo.FACTE_MONEDA ( Mon_Codigo , Mon_Descripcion , Mon_Simbolo ) VALUES ( 'EUR' , 'EUROS' ,  'E' );
INSERT INTO dbo.FACTE_MONEDA ( Mon_Codigo , Mon_Descripcion , Mon_Simbolo ) VALUES ( 'JPY' , 'YEN' ,  '�' );
GO
CREATE TABLE FACTE_DOCUMENTO_TIPO
(
	TipoDoc_Codigo CHAR(2),
	TipoDoc_Descripcion VARCHAR(100),
	CONSTRAINT PK_FACTE_DOCUMENTO_TIPO PRIMARY KEY CLUSTERED 
	(
		TipoDoc_Codigo ASC
	)
)
GO
INSERT INTO dbo.FACTE_DOCUMENTO_TIPO ( TipoDoc_Codigo , TipoDoc_Descripcion ) VALUES ( '03' , 'BOLETA DE VENTA ELECTRONICA' );
INSERT INTO dbo.FACTE_DOCUMENTO_TIPO ( TipoDoc_Codigo , TipoDoc_Descripcion ) VALUES ( '01' , 'FACTURA ELECTRONICA' );
INSERT INTO dbo.FACTE_DOCUMENTO_TIPO ( TipoDoc_Codigo , TipoDoc_Descripcion ) VALUES ( '07' , 'NOTA DE CREDITO ELECTRONICA' );
INSERT INTO dbo.FACTE_DOCUMENTO_TIPO ( TipoDoc_Codigo , TipoDoc_Descripcion ) VALUES ( '08' , 'NOTA DE DEBITO ELECTRONICA' );
INSERT INTO dbo.FACTE_DOCUMENTO_TIPO ( TipoDoc_Codigo , TipoDoc_Descripcion ) VALUES ( '09' , 'GUIA DE REMISION ELECTRONICA - REMITENTE' );
INSERT INTO dbo.FACTE_DOCUMENTO_TIPO ( TipoDoc_Codigo , TipoDoc_Descripcion ) VALUES ( '20' , 'COMPROBANTE DE RETENCION' );
INSERT INTO dbo.FACTE_DOCUMENTO_TIPO ( TipoDoc_Codigo , TipoDoc_Descripcion ) VALUES ( '40' , 'COMPROBANTE DE PERCEPCION' );
GO
CREATE PROC UP_FACTE_DOCUMENTO_TIPO_LISTA
(
@PTipoDoc_Codigo CHAR(2)=NULL
)
AS
BEGIN
	SELECT Dt.TipoDoc_Codigo, Dt.TipoDoc_Descripcion FROM FACTE_DOCUMENTO_TIPO Dt WITH(NOLOCK)
	WHERE (@PTipoDoc_Codigo IS NULL OR Dt.TipoDoc_Codigo = @PTipoDoc_Codigo);
END
GO
CREATE TABLE FACTE_ETAPA
(
	Etapa_Codigo CHAR(3),
	Etapa_Descripcion VARCHAR(50),
	CONSTRAINT PK_FACTE_ETAPA PRIMARY KEY CLUSTERED 
	(
		Etapa_Codigo ASC
	)
)
GO
INSERT INTO dbo.FACTE_ETAPA ( Etapa_Codigo, Etapa_Descripcion ) VALUES ( '001', 'BETA');
INSERT INTO dbo.FACTE_ETAPA ( Etapa_Codigo, Etapa_Descripcion ) VALUES ( '002', 'HOMOLOGACION');
INSERT INTO dbo.FACTE_ETAPA ( Etapa_Codigo, Etapa_Descripcion ) VALUES ( '003', 'PRODUCCION');
GO
CREATE PROC UP_FACTE_ETAPA_LISTA
(
@PEtapa_Codigo CHAR(3)=NULL
)
AS
BEGIN
	SELECT Etapa_Codigo, Etapa_Descripcion FROM dbo.FACTE_ETAPA
	WHERE (@PEtapa_Codigo IS NULL OR Etapa_Codigo = @PEtapa_Codigo);
END
GO
CREATE TABLE FACTE_EMPRESA
(
	Emp_RUC CHAR(11),
	Emp_NombreRazonSocial VARCHAR(150),
	--Emp_EnviaResumenBoletas BIT, REEMPLAZADO POR LAS PROGRAMACIONES

	Sunat_Usuario VARCHAR(50),
	Sunat_Contrasena VARCHAR(50),
	Sunat_Autorizacion VARCHAR(50),---esto deberia estar registrado en las series

	Emisor_Web_Visualizacion VARCHAR(50),--web donde va ver el cliente su factura boleta

	Correo_Usuario VARCHAR(50),
	Correo_Contrasena VARCHAR(50),
	Correo_ServidorSMTP VARCHAR(50),
	Correo_ServidorPuerto VARCHAR(6),
	Correo_ServidorSSL BIT,

	Etapa_Codigo CHAR(3),

	PathLocal_XMLCliente VARCHAR(150), ---Ruta donde se almacenan en forma local los archivos xml

	PathLocal VARCHAR(150), --Ruta que se va almacenar localmente el xml original
	PathServer VARCHAR(150), --Ruta del servidor

	FTP_Direccion VARCHAR(50),
	FTP_Carpeta VARCHAR(200),
	FTP_Usuario VARCHAR(50),
	FTP_Contrasena VARCHAR(50),

	ActualizarClientes BIT,--1-Actualiza el catalogo de los clientes en la BD del cliente

	Aud_Actualizacion_Fecha DATETIME,
	Aud_Actualizacion_Autor VARCHAR(50),
	Aud_Actualizacion_Equipo VARCHAR(50)

	CONSTRAINT PK_FACTE_EMPRESA PRIMARY KEY CLUSTERED 
	(
		Emp_RUC ASC
	)
)
GO
ALTER TABLE FACTE_EMPRESA  WITH CHECK ADD  CONSTRAINT FK_FACTE_EMPRESA_FACTE_ETAPA FOREIGN KEY(Etapa_Codigo)
REFERENCES FACTE_ETAPA (Etapa_Codigo)
GO
--DEMO-INICIO
GO
INSERT FACTE_EMPRESA (Emp_RUC, Emp_NombreRazonSocial, Sunat_Usuario, Sunat_Contrasena, Sunat_Autorizacion, Emisor_Web_Visualizacion, Correo_Usuario, Correo_Contrasena, Correo_ServidorSMTP, Correo_ServidorPuerto, Correo_ServidorSSL, Etapa_Codigo, PathLocal, PathServer, FTP_Direccion, FTP_Carpeta, FTP_Usuario, FTP_Contrasena, ActualizarClientes, Aud_Actualizacion_Fecha, Aud_Actualizacion_Autor, Aud_Actualizacion_Equipo, PathLocal_XMLCliente) VALUES (N'20115643216', N'SERVICENTRO ORTIZ', N'SERV2016', N'SERV2016', N'0340050003066/SUNAT', N'www.grupoortiz.pe/facturacion', N'e-facturacion@grupoortiz.pe', N'6666669991011', N'smtp.gmail.com', N'587', 1, N'001', N'files/servi', N'D:\WebAppsFiles\FacturacionElectronica\Aceptados\', N'', N'', N'', N'', 1, CAST(N'2016-05-16 18:15:27.460' AS DateTime), NULL, N'CGNTIC01', N'files_xml_client/servi')
INSERT FACTE_EMPRESA (Emp_RUC, Emp_NombreRazonSocial, Sunat_Usuario, Sunat_Contrasena, Sunat_Autorizacion, Emisor_Web_Visualizacion, Correo_Usuario, Correo_Contrasena, Correo_ServidorSMTP, Correo_ServidorPuerto, Correo_ServidorSSL, Etapa_Codigo, PathLocal, PathServer, FTP_Direccion, FTP_Carpeta, FTP_Usuario, FTP_Contrasena, ActualizarClientes, Aud_Actualizacion_Fecha, Aud_Actualizacion_Autor, Aud_Actualizacion_Equipo, PathLocal_XMLCliente) VALUES (N'20542134926', N'ESTACIONES DE SERVICIO ORTIZ S.A.C.', N'usuario', N'contrase�a', N'0340050003066/SUNAT', N'www.grupoortiz.pe/facturacion', N'e-facturacion@grupoortiz.pe', N'6666669991011', N'smtp.gmail.com', N'587', 1, N'001', N'files/servi', N'D:\WebAppsFiles\FacturacionElectronica\Aceptados\', N'', N'', N'', N'', 1, NULL, NULL, NULL, N'files_xml_client/eess')
GO
--DEMO-FIN
GO
CREATE PROC UP_FACTE_EMPRESA_LISTA
(
@PEmp_RUC CHAR(11)=NULL
)
AS
BEGIN
	SELECT Emp.Emp_RUC, Emp.Emp_NombreRazonSocial, --Emp.Emp_EnviaResumenBoletas,
	Emp.Sunat_Usuario, Emp.Sunat_Contrasena, Emp.Sunat_Autorizacion,
	Emp.Emisor_Web_Visualizacion,
	Emp.Correo_Usuario, Emp.Correo_Contrasena, Emp.Correo_ServidorSMTP, Emp.Correo_ServidorPuerto, Emp.Correo_ServidorSSL,
	Emp.Etapa_Codigo,Etapa.Etapa_Descripcion,
	Emp.PathLocal_XMLCliente, Emp.PathLocal, Emp.PathServer,
	Emp.FTP_Direccion, Emp.FTP_Carpeta, Emp.FTP_Usuario, Emp.FTP_Contrasena, Emp.ActualizarClientes
	FROM dbo.FACTE_EMPRESA Emp WITH(NOLOCK)
	INNER JOIN dbo.FACTE_ETAPA Etapa WITH(NOLOCK) ON Etapa.Etapa_Codigo = Emp.Etapa_Codigo
	WHERE (@PEmp_RUC IS NULL OR Emp.Emp_RUC = @PEmp_RUC);
END
GO
CREATE PROC UP_FACTE_EMPRESA_INSERT
(
@PEmp_RUC CHAR(11),
@PEmp_NombreRazonSocial VARCHAR(150),
--@PEmp_EnviaResumenBoletas BIT,

@PSunat_Usuario VARCHAR(50),
@PSunat_Contrasena VARCHAR(50),
@PSunat_Autorizacion VARCHAR(50),---esto deberia estar registrado en las series

@PEmisor_Web_Visualizacion VARCHAR(50),--web donde va ver el cliente su factura boleta

@PCorreo_Usuario VARCHAR(50),
@PCorreo_Contrasena VARCHAR(50),
@PCorreo_ServidorSMTP VARCHAR(50),
@PCorreo_ServidorPuerto VARCHAR(6),
@PCorreo_ServidorSSL BIT,

@PEtapa_Codigo CHAR(3),

@PPathLocal_XMLCliente VARCHAR(150),

@PPathLocal VARCHAR(150), --Ruta que se va almacenar localmente el xml original
@PPathServer VARCHAR(150), --Ruta del servidor

@PFTP_Direccion VARCHAR(50),
@PFTP_Carpeta VARCHAR(200),
@PFTP_Usuario VARCHAR(50),
@PFTP_Contrasena VARCHAR(50),

@PActualizarClientes BIT
)
AS
BEGIN
	INSERT INTO dbo.FACTE_EMPRESA
	        ( Emp_RUC , Emp_NombreRazonSocial , --Emp_EnviaResumenBoletas ,
	          Sunat_Usuario , Sunat_Contrasena , Sunat_Autorizacion , Emisor_Web_Visualizacion ,
	          Correo_Usuario , Correo_Contrasena , Correo_ServidorSMTP , Correo_ServidorPuerto , Correo_ServidorSSL ,
	          Etapa_Codigo , PathLocal_XMLCliente, PathLocal , PathServer ,
	          FTP_Direccion , FTP_Carpeta , FTP_Usuario , FTP_Contrasena ,
	          ActualizarClientes ,
	          Aud_Actualizacion_Fecha , Aud_Actualizacion_Autor , Aud_Actualizacion_Equipo
	        )
	VALUES  ( 
			@PEmp_RUC , @PEmp_NombreRazonSocial , --@PEmp_EnviaResumenBoletas ,
			@PSunat_Usuario , @PSunat_Contrasena , @PSunat_Autorizacion , @PEmisor_Web_Visualizacion ,--web donde va ver el cliente su factura boleta
			@PCorreo_Usuario , @PCorreo_Contrasena , @PCorreo_ServidorSMTP , @PCorreo_ServidorPuerto , @PCorreo_ServidorSSL ,
			@PEtapa_Codigo , @PPathLocal_XMLCliente, @PPathLocal , @PPathServer , --Ruta del servidor
			@PFTP_Direccion , @PFTP_Carpeta , @PFTP_Usuario , @PFTP_Contrasena ,
			@PActualizarClientes,
			GETDATE(), 'App', HOST_NAME() );
END
GO
CREATE PROC UP_FACTE_EMPRESA_UPDATE
(
@PEmp_RUC CHAR(11),
@PEmp_NombreRazonSocial VARCHAR(150),
--@PEmp_EnviaResumenBoletas BIT,

@PSunat_Usuario VARCHAR(50),
@PSunat_Contrasena VARCHAR(50),
@PSunat_Autorizacion VARCHAR(50),---esto deberia estar registrado en las series

@PEmisor_Web_Visualizacion VARCHAR(50),--web donde va ver el cliente su factura boleta

@PCorreo_Usuario VARCHAR(50),
@PCorreo_Contrasena VARCHAR(50),
@PCorreo_ServidorSMTP VARCHAR(50),
@PCorreo_ServidorPuerto VARCHAR(6),
@PCorreo_ServidorSSL BIT,

@PEtapa_Codigo CHAR(3),

@PPathLocal_XMLCliente VARCHAR(150),

@PPathLocal VARCHAR(150), --Ruta que se va almacenar localmente el xml original
@PPathServer VARCHAR(150), --Ruta del servidor

@PFTP_Direccion VARCHAR(50),
@PFTP_Carpeta VARCHAR(200),
@PFTP_Usuario VARCHAR(50),
@PFTP_Contrasena VARCHAR(50),

@PActualizarClientes BIT
)
AS
BEGIN
	UPDATE FACTE_EMPRESA SET
	Emp_RUC = @PEmp_RUC, Emp_NombreRazonSocial = @PEmp_NombreRazonSocial, --Emp_EnviaResumenBoletas = @PEmp_EnviaResumenBoletas,
	Sunat_Usuario = @PSunat_Usuario , Sunat_Contrasena = @PSunat_Contrasena, Sunat_Autorizacion = @PSunat_Autorizacion, Emisor_Web_Visualizacion = @PEmisor_Web_Visualizacion ,
	Correo_Usuario = @PCorreo_Usuario, Correo_Contrasena = @PCorreo_Contrasena, Correo_ServidorSMTP = @PCorreo_ServidorSMTP, Correo_ServidorPuerto = @PCorreo_ServidorPuerto, Correo_ServidorSSL = @PCorreo_ServidorSSL,
	Etapa_Codigo = @PEtapa_Codigo, PathLocal_XMLCliente = @PPathLocal_XMLCliente, PathLocal = @PPathLocal , PathServer = @PPathServer,
	FTP_Direccion = @PFTP_Direccion, FTP_Carpeta = @PFTP_Carpeta, FTP_Usuario = @PFTP_Usuario, FTP_Contrasena = @PFTP_Contrasena,
	ActualizarClientes = @PActualizarClientes,
	Aud_Actualizacion_Fecha = GETDATE(), Aud_Actualizacion_Equipo = HOST_NAME()
	WHERE  Emp_RUC = @PEmp_RUC ;
END
GO
--DROP PROCEDURE UP_FACTE_EMPRESA_ENVIA_RESUMENES
--ALTER PROC UP_FACTE_EMPRESA_ENVIA_RESUMENES
--(
--@Emp_RUC CHAR(11),
--@EnviaResumen BIT OUTPUT,
--@Etapa_Codigo CHAR(3) OUTPUT
--)
--AS
--BEGIN
--	SELECT @EnviaResumen = Emp.Emp_EnviaResumenBoletas, @Etapa_Codigo = Emp.Etapa_Codigo
--	FROM FACTE_EMPRESA Emp WHERE Emp.Emp_RUC = @Emp_RUC;
--	SET @EnviaResumen = ISNULL(@EnviaResumen,0);
--	--SET @Etapa_Codigo = ISNULL(@Etapa_Codigo,'');
--END
GO
CREATE TABLE FACTE_CERTIFICADO
(
	Emp_RUC CHAR(11),
	Cert_Correlativo INT,

	Cert_Codigo VARCHAR(15), ---N
	Cert_EsPropio BIT, --N
	Cert_Ext_RUC VARCHAR(15), --N
	Cert_Ext_RazonSocial VARCHAR(150), --N

	Cert_Vigencia_Desde DATE,
	Cert_Vigencia_Hasta DATE,
	Cert_Contrasena VARCHAR(300),
	Cert_Ruta VARCHAR(300),

	Aud_Registro_Fecha DATETIME,
	Aud_Registro_Autor VARCHAR(50),
	Aud_Registro_Equipo VARCHAR(50),
	Aud_Actualizacion_Fecha DATETIME,
	Aud_Actualizacion_Autor VARCHAR(50),
	Aud_Actualizacion_Equipo VARCHAR(50)
	CONSTRAINT PK_FACTE_CERTIFICADO PRIMARY KEY CLUSTERED 
	(
		Emp_RUC ASC,
		Cert_Correlativo ASC
	)
)
GO
ALTER TABLE FACTE_CERTIFICADO  WITH CHECK ADD  CONSTRAINT FK_FACTE_CERTIFICADO_FACTE_EMPRESA FOREIGN KEY(Emp_RUC)
REFERENCES FACTE_EMPRESA (Emp_RUC)
GO
--DEMO-INICIO
GO
INSERT FACTE_CERTIFICADO (Emp_RUC, Cert_Correlativo, Cert_Codigo ,Cert_EsPropio ,Cert_Ext_RUC ,Cert_Ext_RazonSocial, Cert_Vigencia_Desde, Cert_Vigencia_Hasta, Cert_Contrasena, Cert_Ruta, Aud_Registro_Fecha, Aud_Registro_Autor, Aud_Registro_Equipo, Aud_Actualizacion_Fecha, Aud_Actualizacion_Autor, Aud_Actualizacion_Equipo) VALUES (N'20115643216', 1,'',1,NULL,NULL, CAST(N'2016-03-08' AS Date), CAST(N'2017-03-07' AS Date), N'GOServiOrtiz2016FactElectDHNTI', N'D:\DesktopApps\FacturacionElectronica\key\MPS20160310292537.pfx', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT FACTE_CERTIFICADO (Emp_RUC, Cert_Correlativo, Cert_Codigo ,Cert_EsPropio ,Cert_Ext_RUC ,Cert_Ext_RazonSocial, Cert_Vigencia_Desde, Cert_Vigencia_Hasta, Cert_Contrasena, Cert_Ruta, Aud_Registro_Fecha, Aud_Registro_Autor, Aud_Registro_Equipo, Aud_Actualizacion_Fecha, Aud_Actualizacion_Autor, Aud_Actualizacion_Equipo) VALUES (N'20542134926', 1,'',1,NULL,NULL, CAST(N'2016-01-01' AS Date), CAST(N'2017-03-07' AS Date), N'GOServiOrtiz2016FactElectDHNTI', N'D:\DesktopApps\FacturacionElectronica\key\MPS20160310292537.pfx', NULL, NULL, NULL, NULL, NULL, NULL)
GO
--DEMO-FIN
GO
ALTER PROC UP_FACTE_CERTIFICADO_OBTENER_VIGENTE
(
@PEmp_RUC CHAR(11),
@PFecha_Emision DATE
)
AS
BEGIN
	SELECT Certi.Cert_Ruta, Certi.Cert_Contrasena, 
	Config.Sunat_Usuario, Config.Sunat_Contrasena,
	Config.Correo_Usuario, Config.Correo_Contrasena, Config.Correo_ServidorSMTP, Config.Correo_ServidorPuerto, Config.Correo_ServidorSSL,
	Config.Etapa_Codigo,
	Config.PathLocal,
	ISNULL(Config.PathServer,''), --En caso FTP no exista entonces solo guardamos en pathServer (Srvidor local), se supone ahi se ejecuta nuestra app
	ISNULL(Config.FTP_Direccion,''), ISNULL(Config.FTP_Carpeta,''), ISNULL(Config.FTP_Usuario,''), ISNULL(Config.FTP_Contrasena,''),
	Config.Sunat_Autorizacion, Config.Emisor_Web_Visualizacion, 
	Certi.Cert_Codigo,
	Certi.Cert_Vigencia_Desde, Certi.Cert_Vigencia_Hasta,
	Certi.Cert_EsPropio, (CASE WHEN Certi.Cert_EsPropio = 1 THEN Config.Emp_RUC ELSE Certi.Cert_Ext_RUC END) AS 'Propietario_RUC',
	(CASE WHEN Certi.Cert_EsPropio = 1 THEN Config.Emp_NombreRazonSocial ELSE Certi.Cert_Ext_RazonSocial END) AS 'Propietario_RazonSocial'
	FROM FACTE_CERTIFICADO Certi
	INNER JOIN FACTE_EMPRESA Config WITH(NOLOCK) ON Config.Emp_RUC = Certi.Emp_RUC
	WHERE Certi.Emp_RUC = @PEmp_RUC AND (Certi.Cert_Vigencia_Desde <= @PFecha_Emision AND @PFecha_Emision <=Certi.Cert_Vigencia_Hasta)
END
GO
CREATE PROC UP_FACTE_CERTIFICADO_OBTENER
(
@PEmp_RUC CHAR(11)
)
AS
BEGIN
	SELECT Config.Sunat_Usuario, Config.Sunat_Contrasena,
	Config.Correo_Usuario, Config.Correo_Contrasena, Config.Correo_ServidorSMTP, Config.Correo_ServidorPuerto, Config.Correo_ServidorSSL,
	Config.Etapa_Codigo,
	Config.PathLocal,
	ISNULL(Config.PathServer,''), --En caso FTP no exista entonces solo guardamos en pathServer (Srvidor local), se supone ahi se ejecuta nuestra app
	ISNULL(Config.FTP_Direccion,''), ISNULL(Config.FTP_Carpeta,''), ISNULL(Config.FTP_Usuario,''), ISNULL(Config.FTP_Contrasena,''),
	Config.Sunat_Autorizacion, Config.Emisor_Web_Visualizacion
	FROM FACTE_EMPRESA Config WITH(NOLOCK)
	WHERE Config.Emp_RUC = @PEmp_RUC
END
GO
ALTER PROC UP_FACTE_CERTIFICADO_LISTA
(
@PEmp_RUC CHAR(11)
)
AS
BEGIN
	SELECT Certi.Emp_RUC, Certi.Cert_Correlativo, Certi.Cert_Codigo, 
	Certi.Cert_Vigencia_Desde, Certi.Cert_Vigencia_Hasta, 
	Certi.Cert_Contrasena, Certi.Cert_Ruta,
	Certi.Cert_EsPropio, (CASE WHEN Certi.Cert_EsPropio = 1 THEN Config.Emp_RUC ELSE Certi.Cert_Ext_RUC END) AS 'Propietario_RUC',
	(CASE WHEN Certi.Cert_EsPropio = 1 THEN Config.Emp_NombreRazonSocial ELSE Certi.Cert_Ext_RazonSocial END) AS 'Propietario_RazonSocial'
	FROM dbo.FACTE_CERTIFICADO Certi WITH(NOLOCK)
	INNER JOIN FACTE_EMPRESA Config WITH(NOLOCK) ON Config.Emp_RUC = Certi.Emp_RUC
	WHERE Certi.Emp_RUC = @PEmp_RUC
	ORDER BY Certi.Cert_Vigencia_Desde DESC;
END
GO
ALTER PROC UP_FACTE_CERTIFICADO_INSERT
(
@PEmp_RUC CHAR(11),
@PCert_Correlativo INT OUTPUT,
@PCert_Codigo VARCHAR(15),
@PCert_Vigencia_Desde DATE,
@PCert_Vigencia_Hasta DATE,
@PCert_Contrasena VARCHAR(100),
@PCert_Ruta VARCHAR(MAX),
@PCert_EsPropio BIT,
@PCert_Ext_RUC VARCHAR(15)=NULL,
@PCert_Ext_RazonSocial VARCHAR(150)=NULL
)
AS
BEGIN
	 SET @PCert_Correlativo = (ISNULL((SELECT MAX(Certi.Cert_Correlativo) FROM dbo.FACTE_CERTIFICADO Certi WHERE Certi.Emp_RUC = @PEmp_RUC ),0) + 1);

	INSERT INTO dbo.FACTE_CERTIFICADO
	( Emp_RUC , Cert_Correlativo, Cert_Codigo , Cert_Vigencia_Desde , Cert_Vigencia_Hasta , Cert_Contrasena , Cert_Ruta ,
	 Cert_EsPropio, Cert_Ext_RUC, Cert_Ext_RazonSocial,
	 Aud_Registro_Fecha , Aud_Registro_Autor , Aud_Registro_Equipo )
	VALUES  
	( @PEmp_RUC , @PCert_Correlativo, @PCert_Codigo , @PCert_Vigencia_Desde , @PCert_Vigencia_Hasta , @PCert_Contrasena , @PCert_Ruta , -- Cert_Ruta - varchar(300)
	@PCert_EsPropio, @PCert_Ext_RUC, @PCert_Ext_RazonSocial,
	GETDATE() , 'User' , HOST_NAME() );
END
GO
ALTER PROC UP_FACTE_CERTIFICADO_UPDATE
(
@PEmp_RUC CHAR(11),
@PCert_Correlativo INT,
@PCert_Codigo VARCHAR(15),
@PCert_Vigencia_Desde DATE,
@PCert_Vigencia_Hasta DATE,
@PCert_Contrasena VARCHAR(100),
@PCert_Ruta VARCHAR(MAX),
@PCert_EsPropio BIT,
@PCert_Ext_RUC VARCHAR(15)=NULL,
@PCert_Ext_RazonSocial VARCHAR(150)=NULL
)
AS
BEGIN
	UPDATE dbo.FACTE_CERTIFICADO
	SET Cert_Codigo = @PCert_Codigo, Cert_Vigencia_Desde = @PCert_Vigencia_Desde,
	Cert_Vigencia_Hasta = @PCert_Vigencia_Hasta, Cert_Contrasena = @PCert_Contrasena, Cert_Ruta = @PCert_Ruta,
	Cert_EsPropio = @PCert_EsPropio, Cert_Ext_RUC = @PCert_Ext_RUC, Cert_Ext_RazonSocial = @PCert_Ext_RazonSocial,
	Aud_Actualizacion_Fecha = GETDATE(), Aud_Actualizacion_Autor ='USER', Aud_Actualizacion_Equipo = HOST_NAME()
	WHERE Emp_RUC = @PEmp_RUC AND Cert_Correlativo = @PCert_Correlativo ;
END
GO
CREATE PROC UP_FACTE_CERTIFICADO_DELETE
(
@PEmp_RUC CHAR(11),
@PCert_Correlativo INT
)
AS
BEGIN
	DELETE dbo.FACTE_CERTIFICADO
	WHERE Emp_RUC = @PEmp_RUC AND Cert_Correlativo = @PCert_Correlativo ;
END
GO
CREATE TABLE FACTE_DOCUMENTO_IDENTIFICACION_TIPO
(
	TipoDocIdent_Codigo CHAR(1),
	TipoDocIdent_Descripcion VARCHAR(100),
	TipoDocIdent_Corto VARCHAR(10),
	CONSTRAINT PK_FACTE_DOCUMENTO_IDENTIFICACION_TIPO PRIMARY KEY CLUSTERED 
	(
		TipoDocIdent_Codigo ASC
	)
)
GO
INSERT INTO dbo.FACTE_DOCUMENTO_IDENTIFICACION_TIPO ( TipoDocIdent_Codigo , TipoDocIdent_Descripcion , TipoDocIdent_Corto ) VALUES ( '0' ,  'Doc. Trib. No Domiciliados Sin RUC' ,  'SIN-RUC' );
INSERT INTO dbo.FACTE_DOCUMENTO_IDENTIFICACION_TIPO ( TipoDocIdent_Codigo , TipoDocIdent_Descripcion , TipoDocIdent_Corto ) VALUES ( '1' ,  'Documento Nacional de Identidad' ,  'DNI' );
INSERT INTO dbo.FACTE_DOCUMENTO_IDENTIFICACION_TIPO ( TipoDocIdent_Codigo , TipoDocIdent_Descripcion , TipoDocIdent_Corto ) VALUES ( '4' ,  'Carnet de extranjeria' ,  'CE' );
INSERT INTO dbo.FACTE_DOCUMENTO_IDENTIFICACION_TIPO ( TipoDocIdent_Codigo , TipoDocIdent_Descripcion , TipoDocIdent_Corto ) VALUES ( '6' ,  'Registro Unico de Contribuyentes' ,  'RUC' );
INSERT INTO dbo.FACTE_DOCUMENTO_IDENTIFICACION_TIPO ( TipoDocIdent_Codigo , TipoDocIdent_Descripcion , TipoDocIdent_Corto ) VALUES ( '7' ,  'Pasaporte' ,  'PAS' );
INSERT INTO dbo.FACTE_DOCUMENTO_IDENTIFICACION_TIPO ( TipoDocIdent_Codigo , TipoDocIdent_Descripcion , TipoDocIdent_Corto ) VALUES ( 'A' ,  'Ced. Diplomatica de Identidad' ,  'CDI' );
GO
CREATE TABLE FACTE_SOCIO_NEGOCIO
(
	TipoDocIdent_Codigo CHAR(1),
	SocNeg_TipoDocIdent_Numero VARCHAR(15),
	SocNeg_RazonSocialNombres VARCHAR(150),

	CONSTRAINT PK_FACTE_SOCIO_NEGOCIO PRIMARY KEY CLUSTERED
	(
		TipoDocIdent_Codigo ASC,
		SocNeg_TipoDocIdent_Numero ASC
	)
)
GO
ALTER TABLE FACTE_SOCIO_NEGOCIO  WITH CHECK ADD  CONSTRAINT FK_FACTE_SOCIO_NEGOCIO_FACTE_DOCUMENTO_IDENTIFICACION_TIPO FOREIGN KEY(TipoDocIdent_Codigo)
REFERENCES FACTE_DOCUMENTO_IDENTIFICACION_TIPO (TipoDocIdent_Codigo)
GO
ALTER PROC UP_FACTE_SOCIO_NEGOCIO_BUSQUEDA
(
@PReceptor_TipoDocIdent_Codigo CHAR(1)='6', --Por defecto es RUC
@PReceptor_TipoDocIdent_Numero VARCHAR(11)=NULL
)
AS
BEGIN
	SELECT SocNeg.TipoDocIdent_Codigo,Tip.TipoDocIdent_Corto,SocNeg.SocNeg_TipoDocIdent_Numero,SocNeg.SocNeg_RazonSocialNombres
	FROM FACTE_SOCIO_NEGOCIO SocNeg WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_IDENTIFICACION_TIPO Tip WITH(NOLOCK) ON Tip.TipoDocIdent_Codigo = SocNeg.TipoDocIdent_Codigo
END
GO
CREATE TABLE FACTE_RECEPTORES_CORREO
(
	Receptor_TipoDocIdent_Codigo CHAR(1),
	Receptor_TipoDocIdent_Numero VARCHAR(11),--PK-Por defecto es N�mero de RUC
	Receptor_Registro INT, ---PK
	Receptor_Email VARCHAR(50),
	Receptor_Validado BIT, -- Para ver si fue aceptado, inicialmente se encuentra en estado 0 (por validar)
	Receptor_CopiadoACliente BIT, -- Para ver si se copio a cliente
	Aud_Registro_Fecha DATETIME,
	CONSTRAINT PK_FACTE_RECEPTORES_CORREO PRIMARY KEY CLUSTERED 
	(
		Receptor_TipoDocIdent_Numero ASC,
		Receptor_Registro ASC
	)
)
GO
--GUARDO EL CORREO DEL CLIENTE
ALTER PROC UP_FACTE_RECEPTORES_CORREO_INSERT
(
@PReceptor_TipoDocIdent_Codigo CHAR(1)='6', --Por defecto es RUC
@PReceptor_TipoDocIdent_Numero VARCHAR(11),
@PReceptor_Email VARCHAR(50)
)
AS
BEGIN
	DECLARE @PReceptor_Registro INT;
	SET @PReceptor_Registro = ISNULL((SELECT MAX(Corr.Receptor_Registro)+ 1 FROM FACTE_RECEPTORES_CORREO Corr WHERE Corr.Receptor_TipoDocIdent_Numero = @PReceptor_TipoDocIdent_Numero),1);
	INSERT INTO dbo.FACTE_RECEPTORES_CORREO
	( Receptor_TipoDocIdent_Codigo, Receptor_TipoDocIdent_Numero , Receptor_Registro , 
	Receptor_Email , Receptor_Validado, Receptor_CopiadoACliente , Aud_Registro_Fecha )
	VALUES (
	@PReceptor_TipoDocIdent_Codigo, @PReceptor_TipoDocIdent_Numero , @PReceptor_Registro , 
	@PReceptor_Email , 0,  0 , GETDATE() );
END
GO
ALTER PROC UP_FACTE_RECEPTORES_CORREO_PENDIENTES_VALIDACION
AS
BEGIN
	SELECT Corr.Receptor_TipoDocIdent_Codigo, Corr.Receptor_TipoDocIdent_Numero, Corr.Receptor_Email 
	FROM FACTE_RECEPTORES_CORREO Corr
	WHERE Corr.Receptor_Validado = 0  
	AND EXISTS(SELECT Emp.ActualizarClientes FROM dbo.FACTE_EMPRESA Emp WHERE Emp.ActualizarClientes = 1);
	--AL MENOS UNO DEB ESTAR ACTIVADO PARA COPIAR LOS DATOS AL CLIENTE
END
GO
ALTER PROC UP_FACTE_RECEPTORES_CORREO_PENDIENTES_REGISTRO_CLIENTE
AS
BEGIN
	SELECT Corr.Receptor_TipoDocIdent_Numero, Corr.Receptor_Email FROM FACTE_RECEPTORES_CORREO Corr
	WHERE Corr.Receptor_CopiadoACliente = 0  
	AND EXISTS(SELECT Emp.ActualizarClientes FROM dbo.FACTE_EMPRESA Emp WHERE Emp.ActualizarClientes = 1);
	--AL MENOS UNO DEB ESTAR ACTIVADO PARA COPIAR LOS DATOS AL CLIENTE
END
GO
ALTER PROC UP_FACTE_RECEPTORES_CORREO_PENDIENTES_VALIDACION_CAMBIAR_ESTADO
(
@PReceptor_TipoDocIdent_Codigo CHAR(1)='6', --Por defecto es RUC
@PReceptor_TipoDocIdent_Numero VARCHAR(11),
@PReceptor_Registro INT,
@PReceptor_Email VARCHAR(50)
)
AS
BEGIN
	UPDATE FACTE_RECEPTORES_CORREO SET Receptor_Validado = 1
	WHERE Receptor_TipoDocIdent_Codigo = @PReceptor_TipoDocIdent_Codigo  
	AND Receptor_TipoDocIdent_Numero = @PReceptor_TipoDocIdent_Numero AND Receptor_Registro = @PReceptor_Registro ;
END
GO
CREATE TABLE FACTE_ESTADO_DOCUMENTO_ELECTRONICO
(
	EstDoc_Codigo CHAR(3),
	EstDoc_Descripcion VARCHAR(60),
	EstDoc_Codigo_Cliente VARCHAR(4),
	CONSTRAINT PK_FACTE_ESTADO_DOCUMENTO_ELECTRONICO PRIMARY KEY CLUSTERED 
	(
		EstDoc_Codigo ASC
	)
)
GO
	INSERT INTO dbo.FACTE_ESTADO_DOCUMENTO_ELECTRONICO ( EstDoc_Codigo , EstDoc_Descripcion , EstDoc_Codigo_Cliente ) VALUES ( '001' , 'EMITIDO' , '0001' );
	INSERT INTO dbo.FACTE_ESTADO_DOCUMENTO_ELECTRONICO ( EstDoc_Codigo , EstDoc_Descripcion , EstDoc_Codigo_Cliente ) VALUES ( '002' , 'ANULADO' , '0002' );
GO
CREATE TABLE FACTE_ESTADO_ENVIO
(
	EstEnv_Codigo CHAR(3),
	EstEnv_Descripcion VARCHAR(60),
	EstEnv_Codigo_Cliente VARCHAR(4),
	CONSTRAINT PK_FACTE_ESTADO_ENVIO PRIMARY KEY CLUSTERED 
	(
		EstEnv_Codigo ASC
	)
)
GO
	INSERT INTO dbo.FACTE_ESTADO_ENVIO ( EstEnv_Codigo , EstEnv_Descripcion , EstEnv_Codigo_Cliente ) VALUES ( '001' , 'SIN ENVIAR' , '4105' );
	INSERT INTO dbo.FACTE_ESTADO_ENVIO ( EstEnv_Codigo , EstEnv_Descripcion , EstEnv_Codigo_Cliente ) VALUES ( '002' , 'ENVIADO' , '4106' );
	INSERT INTO dbo.FACTE_ESTADO_ENVIO ( EstEnv_Codigo , EstEnv_Descripcion , EstEnv_Codigo_Cliente ) VALUES ( '003' , 'ACEPTADO' , '4107' );
	INSERT INTO dbo.FACTE_ESTADO_ENVIO ( EstEnv_Codigo , EstEnv_Descripcion , EstEnv_Codigo_Cliente ) VALUES ( '004' , 'RECHAZADO' , '4108' );
GO
CREATE TABLE FACTE_TIPO_XML
(
	TipXML_Codigo CHAR(3),
	TipXML_Descripcion VARCHAR(60),
	CONSTRAINT PK_FACTE_TIPO_XML PRIMARY KEY CLUSTERED 
	(
		TipXML_Codigo ASC
	)
)
GO
	INSERT INTO dbo.FACTE_TIPO_XML ( TipXML_Codigo , TipXML_Descripcion ) VALUES ( '001' , 'EMISI�N' );
	INSERT INTO dbo.FACTE_TIPO_XML ( TipXML_Codigo , TipXML_Descripcion ) VALUES ( '002' , 'ACTUALIZACI�N' );
	INSERT INTO dbo.FACTE_TIPO_XML ( TipXML_Codigo , TipXML_Descripcion ) VALUES ( '003' , 'BAJA' );	
GO
--DROP TABLE FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE;
--DROP TABLE FACTE_DOCUMENTO_ELECTRONICO;
GO
CREATE TABLE FACTE_DOCUMENTO_ELECTRONICO
(
	Doc_Emisor_RUC CHAR(11),
	Doc_Tipo CHAR(2),
	Doc_Serie CHAR(4),
	Doc_Numero CHAR(8),
	Doc_FechaEmision DATE,
	Doc_Moneda CHAR(3),
	Doc_Importe DECIMAL(15,2),

	Doc_Cliente_TipoDocIdent_Codigo CHAR(1),
	Doc_Cliente_TipoDocIdent_Numero VARCHAR(15),

	Doc_NombreArchivoXML VARCHAR(50),
	Doc_NombreArchivoPDF VARCHAR(50),

	Doc_NombreArchivoWeb VARCHAR(50),

	--Resumen - Inicio
	Doc_Resumen_FechaGeneracion DATE,
	Doc_Resumen_Correlativo INT,
	Doc_Resumen_TicketSUNAT VARCHAR(50),
	--Resumen - F�n

	--Baja - Inicio
	Doc_Baja_FechaGeneracion DATE,
	Doc_Baja_Correlativo INT,
	Doc_Baja_TicketSUNAT VARCHAR(50),
	--Doc_Baja_XML_Cliente VARCHAR(50), --Evaluar si debe existir aun, ya que existe en FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE
	--Baja - F�n

	Doc_Correo_Enviado BIT,
	Doc_Correo_EstaLeido BIT,
	Doc_Correo_ContadorLectura INT,
	Doc_Correo_LecturaFechaPrimera DATETIME,
	Doc_Correo_LecturaFechaUltima DATETIME,

	EstDoc_Codigo CHAR(3), --Estado documento: Emitido, Anulado

	--EstEnv_Codigo CHAR(3),  ---Estado envio: Aceptado Rechazado

	--Doc_XML_Creado_FechaHora DATETIME,
	--Doc_XML_Enviado_FechaHora DATETIME,

	CONSTRAINT PK_FACTE_DOCUMENTO_ELECTRONICO PRIMARY KEY CLUSTERED
	(
		Doc_Emisor_RUC ASC, Doc_Tipo ASC, Doc_Serie ASC, Doc_Numero ASC
	)
)
GO
ALTER TABLE FACTE_DOCUMENTO_ELECTRONICO  WITH CHECK ADD  CONSTRAINT FK_FACTE_DOCUMENTO_ELECTRONICO_FACTE_EMPRESA FOREIGN KEY(Doc_Emisor_RUC)
REFERENCES FACTE_EMPRESA (Emp_RUC)
GO
ALTER TABLE FACTE_DOCUMENTO_ELECTRONICO  WITH CHECK ADD  CONSTRAINT FK_FACTE_DOCUMENTO_ELECTRONICO_FACTE_DOCUMENTO_TIPO FOREIGN KEY(Doc_Tipo)
REFERENCES FACTE_DOCUMENTO_TIPO (TipoDoc_Codigo)
GO
ALTER TABLE FACTE_DOCUMENTO_ELECTRONICO  WITH CHECK ADD  CONSTRAINT FK_FACTE_DOCUMENTO_ELECTRONICO_FACTE_MONEDA FOREIGN KEY(Doc_Moneda)
REFERENCES FACTE_MONEDA (Mon_Codigo)
GO
ALTER TABLE FACTE_DOCUMENTO_ELECTRONICO  WITH CHECK ADD  CONSTRAINT FK_FACTE_DOCUMENTO_ELECTRONICO_FACTE_ESTADO_DOCUMENTO_ELECTRONICO FOREIGN KEY(EstDoc_Codigo)
REFERENCES FACTE_ESTADO_DOCUMENTO_ELECTRONICO (EstDoc_Codigo)
GO
ALTER TABLE FACTE_DOCUMENTO_ELECTRONICO  WITH CHECK ADD  CONSTRAINT FK_FACTE_DOCUMENTO_ELECTRONICO_FACTE_SOCIO_NEGOCIO FOREIGN KEY(Doc_Cliente_TipoDocIdent_Codigo, Doc_Cliente_TipoDocIdent_Numero)
REFERENCES FACTE_SOCIO_NEGOCIO (TipoDocIdent_Codigo, SocNeg_TipoDocIdent_Numero)
GO
--ALTER TABLE FACTE_DOCUMENTO_ELECTRONICO  WITH CHECK ADD  CONSTRAINT FK_FACTE_DOCUMENTO_ELECTRONICO_FACTE_ESTADO_ENVIO FOREIGN KEY(EstEnv_Codigo)
--REFERENCES FACTE_ESTADO_ENVIO (EstEnv_Codigo)
GO
CREATE TABLE FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE
(
	Doc_Emisor_RUC CHAR(11),
	--Doc_Anio CHAR(4),
	Doc_Tipo CHAR(2),
	Doc_Serie CHAR(4),
	Doc_Numero CHAR(8),

	XML_Correlativo INT,

	XML_Reciente BIT,

	XML_Archivo VARCHAR(150),

	TipXML_Codigo CHAR(3),	--si es nuevo o actualizacion o anulaci�n

	EstEnv_Codigo CHAR(3),  ---Estado envio:Pendiente, Aceptado Rechazado

	XML_FilaActiva BIT,

	XML_Revercion_Anulacion BIT DEFAULT 0,	--1 cuando se reverti�
	XML_Revercion_Anulacion_FechaHora DATETIME,
	XML_Revercion_Anulacion_Razon VARCHAR(150),

	XML_Creado_FechaHora DATETIME,
	XML_Enviado_FechaHora DATETIME,

	Aud_Registro_FechaHora DATETIME,
	Aud_Registro_Equipo VARCHAR(50),
	Aud_Registro_Usuario VARCHAR(50),
	CONSTRAINT PK_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE PRIMARY KEY CLUSTERED 
	(
		Doc_Emisor_RUC ASC,
		Doc_Tipo ASC, Doc_Serie ASC, Doc_Numero ASC, XML_Correlativo DESC
	)
)
GO
ALTER TABLE FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE WITH CHECK ADD CONSTRAINT FK_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE_FACTE_DOCUMENTO_ELECTRONICO FOREIGN KEY(Doc_Emisor_RUC,Doc_Tipo,Doc_Serie,Doc_Numero)
REFERENCES FACTE_DOCUMENTO_ELECTRONICO (Doc_Emisor_RUC,Doc_Tipo,Doc_Serie,Doc_Numero)
GO
ALTER TABLE FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE WITH CHECK ADD CONSTRAINT FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE_FACTE_TIPO_XML FOREIGN KEY(TipXML_Codigo)
REFERENCES FACTE_TIPO_XML (TipXML_Codigo)
GO
ALTER TABLE FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE  WITH CHECK ADD CONSTRAINT FK_FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE_FACTE_ESTADO_ENVIO FOREIGN KEY(EstEnv_Codigo)
REFERENCES FACTE_ESTADO_ENVIO (EstEnv_Codigo)
GO
CREATE TABLE FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO
(
	Doc_Emisor_RUC CHAR(11),
	Doc_Tipo CHAR(2),
	Doc_Serie CHAR(4),
	Doc_Numero CHAR(8),
	--XML_Correlativo INT,

	XML_Archivo_Item INT,
	XML_Archivo_NombreLocal VARCHAR(30),
	XML_Archivo_Nombre VARCHAR(150),
	

	Aud_Registro_FechaHora DATETIME,
	Aud_Registro_Equipo VARCHAR(50),
	Aud_Registro_Usuario VARCHAR(50),
	CONSTRAINT PK_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO PRIMARY KEY CLUSTERED 
	(
		Doc_Emisor_RUC ASC,
		Doc_Tipo ASC, Doc_Serie ASC, Doc_Numero ASC, ---XML_Correlativo ASC, 
		XML_Archivo_Item DESC
	)
)
GO
ALTER TABLE FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO WITH CHECK ADD CONSTRAINT FK_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO_FACTE_DOCUMENTO_ELECTRONICO FOREIGN KEY(Doc_Emisor_RUC,Doc_Tipo,Doc_Serie,Doc_Numero, XML_Correlativo)
REFERENCES FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE (Doc_Emisor_RUC,Doc_Tipo,Doc_Serie,Doc_Numero, XML_Correlativo)
GO
CREATE PROC UP_FACTE_DOCUMENTO_ELECTRONICO_MODIFICABLE
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_Tipo CHAR(2),
@PDoc_Serie CHAR(4),
@PDoc_Numero CHAR(8),
--@PEstado INT OUTPUT,
@PEstado BIT OUTPUT,
@PMensaje VARCHAR(50) OUTPUT
)
AS
BEGIN
	DECLARE @Existe INT, @EstadoEnvio CHAR(3), @TipoXML CHAR(3);
	SELECT TOP 1 @Existe = 1, @EstadoEnvio = cXML.EstEnv_Codigo, @TipoXML = cXML.TipXML_Codigo
	FROM dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE cXML
	WHERE cXML.XML_FilaActiva = 1 AND cXML.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND cXML.Doc_Tipo = @PDoc_Tipo 
	AND cXML.Doc_Serie = @PDoc_Serie AND cXML.Doc_Numero = @PDoc_Numero
	ORDER BY cXML.XML_Correlativo DESC
	;
	IF (@Existe IS NULL)
	BEGIN
		--SET @PEstado = -1 ;
		SET @PEstado = 0 ;
		SET @PMensaje = 'No existe el documento';
	END
	ELSE
    BEGIN
    	IF (@TipoXML <> '003')
		BEGIN
			IF (@EstadoEnvio = '001') --SIN ENVIAR
			BEGIN
				SET @PEstado = 1 ;
				SET @PMensaje = 'El documento aun no se envia a la sunat';
			END
			IF (@EstadoEnvio <> '001')
			BEGIN
				SET @PEstado = 0 ;
				SET @PMensaje = 'El documento fue enviado a la sunat';
			END
		END
		IF (@TipoXML = '003')
		BEGIN
			IF (@EstadoEnvio = '001') --SIN ENVIAR
			BEGIN
				--SET @PEstado = 2 ;
				SET @PEstado = 0 ;
				SET @PMensaje = 'El documento fue dado de baja, aun no se envia a la sunat';
			END
			IF (@EstadoEnvio <> '001')
			BEGIN
				SET @PEstado = 0 ;
				SET @PMensaje = 'El documento fue dado de baja y enviado a la sunat';
			END
		END
    END
END
GO
ALTER PROC UP_FACTE_DOCUMENTO_ELECTRONICO_INSERT_UPDATE
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_Tipo CHAR(2),
@PDoc_Serie CHAR(4),
@PDoc_Numero CHAR(8),
@PDoc_Moneda CHAR(3),
@PDoc_FechaEmision DATE,
@PDoc_Importe DECIMAL(15,2),
@PDoc_TipoDocIdent_Codigo CHAR(1),
@PDoc_TipoDocIdent_Numero VARCHAR(12),
@PDoc_SocNeg_RazonSocialNombres VARCHAR(150),

@Retorno VARCHAR(5) OUTPUT,
@Correlativo INT OUTPUT,
@PDoc_Usuario VARCHAR(50),
@PDoc_XML_Nombre VARCHAR(150) OUTPUT
)
AS
BEGIN
	DECLARE @Existe INT, @Estado CHAR(3), @TipXML_Codigo CHAR(3);

	--INSERTAMOS Y ACTUALIZAMOS -- CLIENTE -- INICIO
	UPDATE FACTE_SOCIO_NEGOCIO SET SocNeg_RazonSocialNombres = @PDoc_SocNeg_RazonSocialNombres
	WHERE TipoDocIdent_Codigo = @PDoc_TipoDocIdent_Codigo AND SocNeg_TipoDocIdent_Numero = @PDoc_TipoDocIdent_Numero ;

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO dbo.FACTE_SOCIO_NEGOCIO
				( TipoDocIdent_Codigo ,
				  SocNeg_TipoDocIdent_Numero ,
				  SocNeg_RazonSocialNombres
				)
		VALUES  ( @PDoc_TipoDocIdent_Codigo , -- TipoDocIdent_Codigo - char(1)
				  @PDoc_TipoDocIdent_Numero , -- SocNeg_TipoDocIdent_Numero - varchar(15)
				  @PDoc_SocNeg_RazonSocialNombres  -- SocNeg_RazonSocialNombres - varchar(150)
				);
	END
	--INSERTAMOS Y ACTUALIZAMOS -- CLIENTE -- FIN

	SELECT TOP 1 @Existe =cXML.XML_Correlativo, @Estado = cXML.EstEnv_Codigo, @TipXML_Codigo = cXML.TipXML_Codigo
	FROM FACTE_DOCUMENTO_ELECTRONICO DocElect WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE cXML ON cXML.Doc_Emisor_RUC = DocElect.Doc_Emisor_RUC AND cXML.Doc_Tipo = DocElect.Doc_Tipo AND cXML.Doc_Serie = DocElect.Doc_Serie AND cXML.Doc_Numero = DocElect.Doc_Numero
	WHERE cXML.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND cXML.Doc_Tipo = @PDoc_Tipo 
	AND cXML.Doc_Serie = @PDoc_Serie AND cXML.Doc_Numero = @PDoc_Numero
	AND cXML.XML_FilaActiva = 1
	ORDER BY cXML.XML_Correlativo DESC ;



	IF (@Existe > 0)
	BEGIN
		--IF (@TipXML_Codigo<>'003')
		--BEGIN
			IF (@Estado = '001') --SIN ENVIAR
			BEGIN
				UPDATE dbo.FACTE_DOCUMENTO_ELECTRONICO
				SET Doc_FechaEmision = @PDoc_FechaEmision, Doc_Moneda = @PDoc_Moneda,
				Doc_Importe = @PDoc_Importe, 
				Doc_Cliente_TipoDocIdent_Codigo = @PDoc_TipoDocIdent_Codigo, Doc_Cliente_TipoDocIdent_Numero = @PDoc_TipoDocIdent_Numero --, 
				--Doc_Cliente_Rs = @PDoc_SocNeg_RazonSocialNombres
				WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie AND Doc_Numero = @PDoc_Numero;

				SET @Correlativo = (ISNULL((SELECT MAX(Arch.XML_Correlativo) FROM dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE Arch WITH(NOLOCK) WHERE Arch.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Arch.Doc_Tipo = @PDoc_Tipo AND Arch.Doc_Serie = @PDoc_Serie AND Arch.Doc_Numero = @PDoc_Numero),0)+1);
				SET @PDoc_XML_Nombre = @PDoc_Emisor_RUC + '-' + @PDoc_Tipo + '-' + @PDoc_Serie + '-' + @PDoc_Numero + '_V' + CAST(@Correlativo AS VARCHAR(3))+ '.xml';

				INSERT INTO dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE
						( Doc_Emisor_RUC , Doc_Tipo , Doc_Serie , Doc_Numero ,
						  XML_Correlativo , XML_Reciente, XML_Archivo , TipXML_Codigo , EstEnv_Codigo, XML_FilaActiva,
						  Aud_Registro_FechaHora , Aud_Registro_Equipo , Aud_Registro_Usuario
						)
				VALUES  ( @PDoc_Emisor_RUC , @PDoc_Tipo , @PDoc_Serie , @PDoc_Numero ,
						  @Correlativo , 1, @PDoc_XML_Nombre , '002' , '001', 1,
						  GETDATE() , '' , @PDoc_Usuario
						)
				
				UPDATE FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE SET XML_Reciente = 0
				WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie AND Doc_Numero = @PDoc_Numero AND XML_Correlativo < @Correlativo AND TipXML_Codigo<>'003';

				SET @Retorno = 1;
			END
			IF (@Estado <> '001') --YA SE ENVIO
			BEGIN
				SET @Retorno = 0;--YA SE ENVIO NO SE PUEDE ACTUALIZAR EL DOCUMENTO
			END			
		--END
		--IF (@TipXML_Codigo='003')
		--BEGIN
		--	SET @Retorno = -1;--El ULTIMO XML LO DIO BAJA
		--END
	END
	ELSE
	BEGIN
		INSERT INTO dbo.FACTE_DOCUMENTO_ELECTRONICO
		        ( Doc_Emisor_RUC , Doc_Tipo , Doc_Serie , Doc_Numero , Doc_FechaEmision ,
		          Doc_Moneda , Doc_Importe , Doc_Cliente_TipoDocIdent_Codigo , Doc_Cliente_TipoDocIdent_Numero ,
		          EstDoc_Codigo--, EstEnv_Codigo
		        )
		VALUES  ( @PDoc_Emisor_RUC , @PDoc_Tipo , @PDoc_Serie , @PDoc_Numero , @PDoc_FechaEmision , -- Doc_FechaEmision - date
		          @PDoc_Moneda , @PDoc_Importe , @PDoc_TipoDocIdent_Codigo , @PDoc_TipoDocIdent_Numero , -- Doc_Cliente_Rs - varchar(150)
				  '001'--, '001'
		        );
		SET @Correlativo = 1;
		SET @PDoc_XML_Nombre = @PDoc_Emisor_RUC + '-' + @PDoc_Tipo + '-' + @PDoc_Serie + '-' + @PDoc_Numero + '_V' + CAST(@Correlativo AS VARCHAR(3)) + '.xml';

		INSERT INTO dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE
			    ( Doc_Emisor_RUC , Doc_Tipo , Doc_Serie , Doc_Numero ,
			        XML_Correlativo , XML_Reciente , XML_Archivo , TipXML_Codigo , EstEnv_Codigo , XML_FilaActiva,
			        Aud_Registro_FechaHora , Aud_Registro_Equipo , Aud_Registro_Usuario
			    )
		VALUES  ( @PDoc_Emisor_RUC , @PDoc_Tipo , @PDoc_Serie , @PDoc_Numero ,
			        @Correlativo , 1, @PDoc_XML_Nombre , '001' , '001' , 1,
			        GETDATE() , '' , @PDoc_Usuario
			    );
		SET @Retorno = 1;
	END
END
GO
ALTER PROC UP_FACTE_DOCUMENTO_ELECTRONICO_DISABLE
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_Tipo CHAR(2),
@PDoc_Serie CHAR(4),
@PDoc_Numero CHAR(8),
@PDoc_RazonAnulacion VARCHAR(150),

@Retorno VARCHAR(5) OUTPUT,

@PDoc_Usuario VARCHAR(50),
@PDoc_XML_NombreIN VARCHAR(150)=NULL,
@PDoc_XML_Nombre VARCHAR(150) OUTPUT
)
AS
BEGIN
	DECLARE @Existe INT, @Estado CHAR(3), @Correlativo INT, @TipXML_Codigo CHAR(3);

	SELECT TOP 1 @Existe = cXML.XML_Correlativo, @Estado = cXML.EstEnv_Codigo, @TipXML_Codigo = cXML.TipXML_Codigo
	FROM FACTE_DOCUMENTO_ELECTRONICO DocElect WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE cXML ON cXML.Doc_Emisor_RUC = DocElect.Doc_Emisor_RUC AND cXML.Doc_Tipo = DocElect.Doc_Tipo AND cXML.Doc_Serie = DocElect.Doc_Serie AND cXML.Doc_Numero = DocElect.Doc_Numero
	WHERE cXML.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND cXML.Doc_Tipo = @PDoc_Tipo 
	AND cXML.Doc_Serie = @PDoc_Serie AND cXML.Doc_Numero = @PDoc_Numero
	AND cXML.XML_FilaActiva = 1
	ORDER BY cXML.XML_Correlativo DESC ;

	IF (@Existe > 0)
	BEGIN
		IF (@Estado = '001')
		BEGIN
			--UPDATE dbo.FACTE_DOCUMENTO_ELECTRONICO
			--SET EstDoc_Codigo = ''
			--WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie AND Doc_Numero = @PDoc_Numero;
			IF (@PDoc_XML_NombreIN IS NULL)
			BEGIN
				SET @Correlativo = (ISNULL((SELECT MAX(Arch.XML_Correlativo) FROM dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE Arch WITH(NOLOCK) WHERE Arch.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Arch.Doc_Tipo = @PDoc_Tipo AND Arch.Doc_Serie = @PDoc_Serie AND Arch.Doc_Numero = @PDoc_Numero),0)+1);
				SET @PDoc_XML_Nombre = @PDoc_Emisor_RUC + '-' + @PDoc_Tipo + '-' + @PDoc_Serie + '-' + @PDoc_Numero + '_V' + CAST(@Correlativo AS VARCHAR(3))+ '.xml';			
			END

			INSERT INTO dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE
					( Doc_Emisor_RUC , Doc_Tipo , Doc_Serie , Doc_Numero ,
						XML_Correlativo , XML_Reciente, XML_Archivo , TipXML_Codigo , EstEnv_Codigo, XML_FilaActiva,
						Aud_Registro_FechaHora , Aud_Registro_Equipo , Aud_Registro_Usuario
					)
			VALUES  ( @PDoc_Emisor_RUC , @PDoc_Tipo , @PDoc_Serie , @PDoc_Numero ,
						@Correlativo , 1, @PDoc_XML_Nombre , '003' , '001', 1,
						GETDATE() , '' , @PDoc_Usuario
					);

			UPDATE FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE SET XML_Reciente = 0
			WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie AND Doc_Numero = @PDoc_Numero AND XML_Correlativo < @Correlativo AND TipXML_Codigo='003';

			SET @Retorno = 1;		
		END
		IF (@Estado <> '001')
		BEGIN
			SET @Retorno = 0;--El ULTIMO XML LO DIO BAJA
		END
	END
END
GO
CREATE PROC UP_FACTE_DOCUMENTO_ELECTRONICO_BUSQUEDA
(
@PDoc_Fecha_Desde DATE,
@PDoc_Fecha_Hasta DATE,
@PDoc_Emisor_RUC CHAR(11)=NULL,
@PDoc_Tipo CHAR(2)=NULL,
@PDoc_Serie CHAR(4)=NULL,
@PDoc_Numero CHAR(8)=NULL
)
AS
BEGIN
SELECT
DocX.Doc_Emisor_RUC, DocX.Doc_Tipo, Tipo.TipoDoc_Descripcion, 
DocX.Doc_Serie, DocX.Doc_Numero, DocX.Doc_FechaEmision, DocX.Doc_Moneda, Mon.Mon_Simbolo, DocX.Doc_Importe, 
Docx.Doc_Cliente_TipoDocIdent_Codigo, DocX.Doc_Cliente_TipoDocIdent_Numero, SocNeg.SocNeg_RazonSocialNombres,
DocX.EstDoc_Codigo, EstDoc.EstDoc_Descripcion, DocX.EstEnv_Codigo, Est.EstEnv_Descripcion,
Empr.PathLocal,
DocX.Doc_NombreArchivoXML, DocX.Doc_NombreArchivoPDF,
DocX.Doc_NombreArchivoWeb, DocX.Doc_Correo_Enviado, DocX.Doc_Correo_EstaLeido,
DocX.Doc_Resumen_FechaGeneracion, DocX.Doc_Resumen_Correlativo, DocX.Doc_Resumen_TicketSUNAT,
DocX.Doc_Baja_FechaGeneracion, DocX.Doc_Baja_Correlativo, DocX.Doc_Baja_TicketSUNAT
FROM (
	SELECT Doc.Doc_Emisor_RUC, Doc.Doc_Tipo, Doc.Doc_Serie, Doc.Doc_Numero, Doc.Doc_FechaEmision, Doc.Doc_Moneda, Doc.Doc_Importe,
	Doc.Doc_Cliente_TipoDocIdent_Codigo, Doc.Doc_Cliente_TipoDocIdent_Numero, Doc.EstDoc_Codigo,
	(SELECT TOP 1 cXML.EstEnv_Codigo FROM dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE cXML
	WHERE cXML.Doc_Emisor_RUC = Doc.Doc_Emisor_RUC AND cXML.Doc_Tipo = Doc.Doc_Tipo AND cXML.Doc_Serie = Doc.Doc_Serie AND cXML.Doc_Numero = Doc.Doc_Numero ORDER BY cXML.XML_Correlativo DESC) AS 'EstEnv_Codigo',
	ISNULL(Doc.Doc_NombreArchivoXML,'') AS 'Doc_NombreArchivoXML', ISNULL(Doc.Doc_NombreArchivoPDF,'') AS 'Doc_NombreArchivoPDF', 
	ISNULL(Doc.Doc_NombreArchivoWeb,'') AS 'Doc_NombreArchivoWeb', ISNULL(Doc.Doc_Correo_Enviado,0) AS 'Doc_Correo_Enviado', ISNULL(Doc.Doc_Correo_EstaLeido,0) AS 'Doc_Correo_EstaLeido',
	Doc.Doc_Resumen_FechaGeneracion, Doc.Doc_Resumen_Correlativo, Doc.Doc_Resumen_TicketSUNAT,
	Doc.Doc_Baja_FechaGeneracion, Doc.Doc_Baja_Correlativo, Doc.Doc_Baja_TicketSUNAT
	FROM dbo.FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK)
	WHERE (Doc.Doc_FechaEmision BETWEEN @PDoc_Fecha_Desde AND @PDoc_Fecha_Hasta)
	AND (@PDoc_Emisor_RUC IS NULL OR Doc.Doc_Emisor_RUC = @PDoc_Emisor_RUC)
	AND (@PDoc_Tipo IS NULL OR Doc.Doc_Tipo = @PDoc_Tipo)
	AND (@PDoc_Serie IS NULL OR Doc.Doc_Serie = @PDoc_Serie)
	AND (@PDoc_Numero IS NULL OR Doc.Doc_Numero = @PDoc_Numero)
	) DocX
	INNER JOIN dbo.FACTE_SOCIO_NEGOCIO SocNeg WITH(NOLOCK) ON DocX.Doc_Cliente_TipoDocIdent_Codigo = SocNeg.TipoDocIdent_Codigo AND DocX.Doc_Cliente_TipoDocIdent_Numero = SocNeg.SocNeg_TipoDocIdent_Numero
	INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tipo WITH(NOLOCK) ON DocX.Doc_Tipo = Tipo.TipoDoc_Codigo
	INNER JOIN dbo.FACTE_MONEDA Mon WITH(NOLOCK) ON DocX.Doc_Moneda = Mon.Mon_Codigo
	INNER JOIN dbo.FACTE_EMPRESA Empr WITH(NOLOCK) ON Empr.Emp_RUC = DocX.Doc_Emisor_RUC
	INNER JOIN dbo.FACTE_ESTADO_DOCUMENTO_ELECTRONICO EstDoc WITH(NOLOCK) ON DocX.EstDoc_Codigo = EstDoc.EstDoc_Codigo
	INNER JOIN dbo.FACTE_ESTADO_ENVIO Est WITH(NOLOCK) ON DocX.EstEnv_Codigo = Est.EstEnv_Codigo
	ORDER BY DocX.Doc_FechaEmision DESC, DocX.Doc_Emisor_RUC DESC, DocX.Doc_Tipo ASC, DocX.Doc_Serie DESC, DocX.Doc_Numero DESC ;
END
GO
CREATE PROC UP_FACTE_DOCUMENTO_ELECTRONICO_VERSIONES
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_Tipo CHAR(2),
@PDoc_Serie CHAR(4),
@PDoc_Numero CHAR(8)
)
AS
BEGIN
	SELECT Ver.XML_Correlativo AS 'Item', Ver.Aud_Registro_FechaHora AS 'Fecha', Ver.XML_Archivo AS 'Archivo', 
	Tip.TipXML_Descripcion AS 'Tipo', Est.EstEnv_Descripcion AS 'Estado'
	FROM dbo.FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE Ver WITH(NOLOCK) ON Ver.Doc_Emisor_RUC = Doc.Doc_Emisor_RUC AND Ver.Doc_Tipo = Doc.Doc_Tipo AND Ver.Doc_Serie = Doc.Doc_Serie AND Ver.Doc_Numero = Doc.Doc_Numero
	INNER JOIN dbo.FACTE_TIPO_XML Tip WITH(NOLOCK) ON Tip.TipXML_Codigo = Ver.TipXML_Codigo
	INNER JOIN dbo.FACTE_ESTADO_ENVIO Est WITH(NOLOCK) ON Est.EstEnv_Codigo = Ver.EstEnv_Codigo
	WHERE Doc.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc.Doc_Tipo = @PDoc_Tipo AND Doc.Doc_Serie = @PDoc_Serie AND Doc.Doc_Numero = @PDoc_Numero
	ORDER BY Doc.Doc_FechaEmision DESC, Doc.Doc_Emisor_RUC DESC, Doc.Doc_Tipo ASC, Doc.Doc_Serie DESC, Doc.Doc_Numero DESC;
END
GO
--
GO
--VERIFICAR
CREATE PROC UP_FACTE_DOCUMENTOS_ACEPTADOS_INSERT
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_Tipo CHAR(2),
@PDoc_Serie CHAR(4),
@PDoc_Numero CHAR(8),
@PDoc_Moneda CHAR(3),
@PDoc_FechaEmision DATE,
@PDoc_Importe DECIMAL(15,2),
@PDoc_Cliente_NumDoc VARCHAR(12),
@PDoc_Cliente_Rs VARCHAR(150),

@PDoc_NombreArchivoXML VARCHAR(50),
@PDoc_NombreArchivoPDF VARCHAR(50),
@PDoc_NombreArchivoWeb VARCHAR(50) OUTPUT,
@PDoc_Estado CHAR(4)
)
AS
BEGIN
	SET @PDoc_NombreArchivoWeb = newid();
	WHILE EXISTS(SELECT Acep.Doc_NombreArchivoWeb FROM dbo.FACTE_DOCUMENTO_ELECTRONICO Acep WHERE Acep.Doc_NombreArchivoWeb = @PDoc_NombreArchivoWeb)
	BEGIN
		SET @PDoc_NombreArchivoWeb = newid();
	END
	INSERT INTO dbo.FACTE_DOCUMENTO_ELECTRONICO
	( Doc_Emisor_RUC , Doc_Tipo , Doc_Serie , Doc_Numero , Doc_FechaEmision , Doc_Moneda , Doc_Importe , Doc_Cliente_NumDoc , Doc_Cliente_Rs , 
	Doc_NombreArchivoXML , Doc_NombreArchivoPDF , Doc_NombreArchivoWeb,
	Doc_Estado , 
	Doc_Aud_Registro_FechaHora , Doc_Aud_Registro_Equipo )
	VALUES 
	( @PDoc_Emisor_RUC , @PDoc_Tipo , @PDoc_Serie , @PDoc_Numero , @PDoc_FechaEmision , @PDoc_Moneda , @PDoc_Importe , @PDoc_Cliente_NumDoc , @PDoc_Cliente_Rs , 
	@PDoc_NombreArchivoXML , @PDoc_NombreArchivoPDF , @PDoc_NombreArchivoWeb ,
	@PDoc_Estado ,
    GETDATE() ,  HOST_NAME());
END
GO
CREATE PROC UP_FACTE_DOCUMENTOS_ACEPTADOS_BUSQUEDA
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_Tipo CHAR(2),
@PDoc_Serie CHAR(4),
@PDoc_Numero CHAR(8),
@PDoc_FechaEmision DATE,
@PDoc_Importe DECIMAL(15,2),
@PExiste BIT OUTPUT,
@PDoc_NombreArchivoWeb VARCHAR(50) OUTPUT
)
AS
BEGIN
	SELECT @PExiste = 1, @PDoc_NombreArchivoWeb = Acep.Doc_NombreArchivoWeb
	FROM FACTE_DOCUMENTO_ELECTRONICO Acep
	WHERE Acep.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Acep.Doc_Tipo = @PDoc_Tipo 
	AND Acep.Doc_Serie = @PDoc_Serie AND Acep.Doc_Numero = @PDoc_Numero AND Acep.Doc_FechaEmision = @PDoc_FechaEmision AND Acep.Doc_Importe = @PDoc_Importe ;
	SET @PExiste = ISNULL(@PExiste,0);
END
GO
CREATE PROC UP_FACTE_DOCUMENTOS_ACEPTADOS_ACTUALIZAR_ESTADO_ENVIO
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_NombreArchivoWeb VARCHAR(50)
)
AS
BEGIN
	UPDATE dbo.FACTE_DOCUMENTO_ELECTRONICO
	SET Doc_Correo_Enviado = 1
	WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_NombreArchivoWeb = @PDoc_NombreArchivoWeb ;
END
GO
ALTER PROC UP_FACTE_DOCUMENTOS_ACEPTADO_CONTAR_LEIDOS
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_NombreArchivoWeb VARCHAR(50)
)
AS
BEGIN
	DECLARE @Contador INT;
	SET @Contador =ISNULL((SELECT Doc.Doc_Correo_ContadorLectura FROM dbo.FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK)
	WHERE Doc.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc.Doc_NombreArchivoWeb = @PDoc_NombreArchivoWeb),0) + 1 ;
	
	UPDATE FACTE_DOCUMENTO_ELECTRONICO SET Doc_Correo_ContadorLectura = @Contador, Doc_Correo_EstaLeido = 1,
	Doc_Correo_LecturaFechaPrimera = (CASE WHEN @Contador = 1 THEN GETDATE() ELSE Doc_Correo_LecturaFechaPrimera END), Doc_Correo_LecturaFechaUltima = GETDATE()
	WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_NombreArchivoWeb = @PDoc_NombreArchivoWeb ;
END
GO
---WEB | Inicio
GO
CREATE PROC UP_FACTE_DOCUMENTOS_BUSQUEDA_IDWEB_XML
(
@PDoc_NombreArchivoWeb VARCHAR(50),
@PDoc_NombreArchivoXML VARCHAR(250) OUTPUT
)
AS
BEGIN
	SELECT @PDoc_NombreArchivoXML = Emp.PathServer + Acep.Doc_NombreArchivoXML
	FROM FACTE_DOCUMENTO_ELECTRONICO Acep
	INNER JOIN dbo.FACTE_EMPRESA Emp ON Emp.Emp_RUC = Acep.Doc_Emisor_RUC
	WHERE Acep.Doc_NombreArchivoWeb = @PDoc_NombreArchivoWeb ;
	SET @PDoc_NombreArchivoXML = ISNULL(@PDoc_NombreArchivoXML,'');
END
GO
CREATE PROC UP_FACTE_DOCUMENTOS_BUSQUEDA_IDWEB_PDF
(
@PDoc_NombreArchivoWeb VARCHAR(50),
@PDoc_NombreArchivoPDF VARCHAR(250) OUTPUT
)
AS
BEGIN
	SELECT @PDoc_NombreArchivoPDF = Emp.PathServer + Acep.Doc_NombreArchivoPDF
	FROM FACTE_DOCUMENTO_ELECTRONICO Acep
	INNER JOIN dbo.FACTE_EMPRESA Emp ON Emp.Emp_RUC = Acep.Doc_Emisor_RUC
	WHERE Acep.Doc_NombreArchivoWeb = @PDoc_NombreArchivoWeb ;
	SET @PDoc_NombreArchivoPDF = ISNULL(@PDoc_NombreArchivoPDF,'');
END
GO
CREATE PROC UP_FACTE_DOCUMENTOS_ACEPTADOS_IDWEB
(
@PDoc_NombreArchivoWeb VARCHAR(50),---PARA LOS ENLACES ENVIADOS POR CORREO
@PDoc_TipoDoc_Descripcion VARCHAR(40) OUTPUT,
@PDoc_Serie CHAR(4) OUTPUT,
@PDoc_Numero CHAR(8) OUTPUT,
@PDoc_FechaEmision DATE OUTPUT,
@PDoc_Moneda_Abreviado VARCHAR(5) OUTPUT,
@PDoc_Importe DECIMAL(15,2) OUTPUT,
@PDoc_Cliente_Rs VARCHAR(150) OUTPUT
)
AS
BEGIN
	SELECT @PDoc_TipoDoc_Descripcion = DocTipo.TipoDoc_Descripcion , @PDoc_Serie = Acep.Doc_Serie, @PDoc_Numero = Acep.Doc_Numero,
	@PDoc_FechaEmision = Acep.Doc_FechaEmision, @PDoc_Moneda_Abreviado = Mon.Mon_Simbolo ,
	@PDoc_Importe = Acep.Doc_Importe, @PDoc_Cliente_Rs = Acep.Doc_Cliente_Rs
	FROM FACTE_DOCUMENTO_ELECTRONICO Acep
	INNER JOIN dbo.FACTE_DOCUMENTO_TIPO DocTipo ON Acep.Doc_Tipo = DocTipo.TipoDoc_Codigo
	INNER JOIN dbo.FACTE_MONEDA Mon ON Acep.Doc_Moneda = Mon.Mon_Codigo
	WHERE Acep.Doc_NombreArchivoWeb = @PDoc_NombreArchivoWeb ;
END
GO
---WEB | F�n
GO
CREATE TABLE FACTE_RESUMEN_DOCUMENTOS_ACEPTADO
(
	Doc_Emisor_RUC CHAR(11),
	Doc_FechaGeneracion DATE,
	Doc_Resumen_Correlativo INT,

	Doc_Moneda CHAR(3),
	Doc_FechaEmision DATE,
	Doc_Tipo CHAR(2),
	Doc_Serie CHAR(4),
	Doc_NumeroInicio CHAR(8),
	Doc_NumeroFin CHAR(8),
	
	Doc_ImporteResumen DECIMAL(15,2),

	Doc_SUNATTicket_ID VARCHAR(50),

	Doc_NombreArchivoXML VARCHAR(50),

	Doc_NombreArchivoWeb VARCHAR(50),

	Doc_Estado CHAR(4),

	Doc_Aud_Registro_FechaHora DATETIME,
	Doc_Aud_Registro_Equipo VARCHAR(50),
	Doc_Aud_Aceptacion_FechaHoraCheck DATETIME,
	Doc_Aud_Aceptacion_Equipo VARCHAR(50),

	CONSTRAINT PK_FACTE_RESUMEN_DOCUMENTOS_ACEPTADO PRIMARY KEY CLUSTERED 
	(
		Doc_Emisor_RUC ASC, Doc_FechaGeneracion ASC, Doc_Resumen_Correlativo ASC
	)
)
GO
ALTER TABLE FACTE_RESUMEN_DOCUMENTOS_ACEPTADO  WITH CHECK ADD  CONSTRAINT FK_FACTE_RESUMEN_DOCUMENTOS_ACEPTADO_FACTE_EMPRESA FOREIGN KEY(Doc_Emisor_RUC)
REFERENCES FACTE_EMPRESA (Emp_RUC)
GO
---NO TODAS LAS EMPRESAS MANDAN RESUMENES
CREATE PROC UP_FACTE_DOCUMENTO_EMPRESAS_SUMMARY
AS
BEGIN
	SELECT Emp.Emp_RUC FROM dbo.FACTE_EMPRESA Emp WHERE Emp.Emp_EnviaResumenBoletas = 1 ;
END
GO
CREATE PROC UP_FACTE_DOCUMENTO_EMPRESAS_SUMMARY_ID
(
@PDoc_FechaGeneracion DATE,
@PDoc_Resumen_Correlativo INT OUTPUT
)
AS
BEGIN
	SET @PDoc_Resumen_Correlativo = (SELECT MAX(Rsm.Doc_Resumen_Correlativo ) FROM dbo.FACTE_RESUMEN_DOCUMENTOS_ACEPTADO Rsm WHERE Rsm.Doc_FechaGeneracion = @PDoc_FechaGeneracion);
	SET @PDoc_Resumen_Correlativo = (ISNULL(@PDoc_Resumen_Correlativo,0) + 1);
END
GO
CREATE PROC UP_FACTE_DOCUMENTO_EMPRESAS_SUMMARY_INSERT
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_FechaGeneracion DATE,
@PDoc_Resumen_Correlativo INT,
@PDoc_FechaEmision DATE,
@PDoc_Moneda CHAR(3),
@PDoc_Tipo CHAR(2),
@PDoc_Serie CHAR(4),
@PDoc_NumeroInicio CHAR(8),
@PDoc_NumeroFin CHAR(8),	
@PDoc_ImporteResumen DECIMAL(15,2),
@PDoc_SUNATTicket_ID VARCHAR(50),
@PDoc_NombreArchivoXML VARCHAR(50),
@PDoc_NombreArchivoWeb VARCHAR(50) OUTPUT,
@PDoc_Estado CHAR(4)
)
AS
BEGIN
	SET @PDoc_NombreArchivoWeb = newid();
	WHILE EXISTS(SELECT Acep.Doc_NombreArchivoWeb FROM dbo.FACTE_RESUMEN_DOCUMENTOS_ACEPTADO Acep WHERE Acep.Doc_NombreArchivoWeb = @PDoc_NombreArchivoWeb)
	BEGIN
		SET @PDoc_NombreArchivoWeb = newid();
	END
	INSERT INTO dbo.FACTE_RESUMEN_DOCUMENTOS_ACEPTADO
	        ( Doc_Emisor_RUC , Doc_FechaGeneracion , Doc_Resumen_Correlativo ,
	          Doc_FechaEmision , Doc_Moneda ,  Doc_Tipo , Doc_Serie , Doc_NumeroInicio , Doc_NumeroFin , Doc_ImporteResumen ,
	          Doc_SUNATTicket_ID , Doc_NombreArchivoXML , Doc_NombreArchivoWeb , Doc_Estado
	        )
	VALUES  ( @PDoc_Emisor_RUC , @PDoc_FechaGeneracion , @PDoc_Resumen_Correlativo, -- Doc_Resumen_Correlativo - int
	          @PDoc_FechaEmision, @PDoc_Moneda , @PDoc_Tipo , @PDoc_Serie , @PDoc_NumeroInicio , @PDoc_NumeroFin , @PDoc_ImporteResumen , -- Doc_ImporteResumen - decimal
	          @PDoc_SUNATTicket_ID , @PDoc_NombreArchivoXML , @PDoc_NombreArchivoWeb , @PDoc_Estado );
END
GO
CREATE PROC UP_FACTE_DOCUMENTO_EMPRESAS_SUMMARY_TICKET_PENDIENTES
AS
BEGIN
	SELECT Res.Doc_SUNATTicket_ID, Res.Doc_Emisor_RUC FROM dbo.FACTE_RESUMEN_DOCUMENTOS_ACEPTADO Res 
	WITH(NOLOCK) WHERE Res.Doc_Estado='4106'--4106-ENVIADO 4107-ACEPTADOS 4108-RECHAZADO
END
GO
CREATE PROC UP_FACTE_DOCUMENTO_EMPRESAS_SUMMARY_TICKET_CAMBIARESTADO
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_SUNATTicket_ID VARCHAR(50),
@PDoc_Estado CHAR(4)
)
AS
BEGIN
	UPDATE FACTE_RESUMEN_DOCUMENTOS_ACEPTADO SET Doc_Estado = @PDoc_Estado
	WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_SUNATTicket_ID = @PDoc_SUNATTicket_ID ;
END
GO
CREATE PROC UP_FACTE_DOCUMENTO_EMPRESAS_SUMMARY_TICKET_DETALLE
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_SUNATTicket_ID VARCHAR(50)
)
AS
BEGIN
	SELECT res.Doc_Emisor_RUC, Res.Doc_Tipo, Res.Doc_Serie, Res.Doc_NumeroInicio, Res.Doc_NumeroFin
	FROM FACTE_RESUMEN_DOCUMENTOS_ACEPTADO Res WITH(NOLOCK)
	WHERE Res.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Res.Doc_SUNATTicket_ID = @PDoc_SUNATTicket_ID ;
END
GO
--- RESUMEN DE BAJA
GO
CREATE TABLE FACTE_RESUMEN_BAJA
(
	Rsm_Emisor_RUC CHAR(11),
	Rsm_FechaGeneracion DATE,
	Rsm_Correlativo INT,

	Rsm_SUNATTicket_ID VARCHAR(50),
	Rsm_NombreArchivoXML VARCHAR(50),
	Rsm_NombreArchivoWeb VARCHAR(50),

	Rsm_Estado CHAR(4),

	Doc_Aud_Registro_FechaHora DATETIME,
	Doc_Aud_Registro_Equipo VARCHAR(50),
	Doc_Aud_Aceptacion_FechaHoraCheck DATETIME,
	Doc_Aud_Aceptacion_Equipo VARCHAR(50),

	CONSTRAINT PK_FACTE_RESUMEN_BAJA PRIMARY KEY CLUSTERED 
	(
		Rsm_Emisor_RUC ASC, Rsm_FechaGeneracion ASC, Rsm_Correlativo ASC
	)
)
GO
ALTER TABLE FACTE_RESUMEN_BAJA  WITH CHECK ADD  CONSTRAINT FK_FACTE_RESUMEN_BAJA_FACTE_EMPRESA FOREIGN KEY(Rsm_Emisor_RUC)
REFERENCES FACTE_EMPRESA (Emp_RUC)
GO
CREATE TABLE FACTE_RESUMEN_BAJA_DOCUMENTO
(
	Rsm_Emisor_RUC CHAR(11),
	Rsm_FechaGeneracion DATE,
	Rsm_Correlativo INT,
	Rsm_Doc_Item INT,

	Rsm_Doc_Tipo CHAR(2),
	Rsm_Doc_Serie CHAR(4),
	Rsm_Doc_Numero CHAR(8),

	CONSTRAINT PK_FACTE_RESUMEN_BAJA_DOCUMENTO PRIMARY KEY CLUSTERED 
	(
		Rsm_Emisor_RUC ASC, Rsm_FechaGeneracion ASC, Rsm_Correlativo ASC, Rsm_Doc_Item ASC
	)
)
GO
ALTER TABLE FACTE_RESUMEN_BAJA_DOCUMENTO  WITH CHECK ADD  CONSTRAINT FK_FACTE_RESUMEN_BAJA_DOCUMENTO_FACTE_RESUMEN_BAJA FOREIGN KEY(Rsm_Emisor_RUC, Rsm_FechaGeneracion, Rsm_Correlativo)
REFERENCES FACTE_RESUMEN_BAJA (Rsm_Emisor_RUC, Rsm_FechaGeneracion, Rsm_Correlativo)
GO
CREATE PROC UP_FACTE_RESUMEN_BAJA_OBTENER_CORRELATIVO
(
@PRsm_FechaGeneracion DATE OUTPUT,
@PRsm_Correlativo INT OUTPUT
)
AS
BEGIN
	SET @PRsm_FechaGeneracion = GETDATE();
	SET @PRsm_Correlativo = (SELECT MAX(Rsm.Rsm_Correlativo) FROM dbo.FACTE_RESUMEN_BAJA Rsm WHERE Rsm.Rsm_FechaGeneracion = @PRsm_FechaGeneracion);
	SET @PRsm_Correlativo = (ISNULL(@PRsm_Correlativo,0) + 1);
END
GO
CREATE PROC UP_FACTE_RESUMEN_BAJA_INSERT
(
@PRsm_Emisor_RUC CHAR(11),
@PRsm_FechaGeneracion DATE,
@PRsm_Correlativo INT,

@PRsm_SUNATTicket_ID VARCHAR(50),
@PRsm_NombreArchivoXML VARCHAR(50),
@PRsm_NombreArchivoWeb VARCHAR(50) OUTPUT
)
AS
BEGIN
	SET @PRsm_NombreArchivoWeb = newid();
	WHILE EXISTS(SELECT Acep.Rsm_NombreArchivoWeb FROM dbo.FACTE_RESUMEN_BAJA Acep WHERE Acep.Rsm_NombreArchivoWeb = @PRsm_NombreArchivoWeb)
	BEGIN
		SET @PRsm_NombreArchivoWeb = newid();
	END

	INSERT INTO dbo.FACTE_RESUMEN_BAJA
	( Rsm_Emisor_RUC , Rsm_FechaGeneracion , Rsm_Correlativo ,
	  Rsm_SUNATTicket_ID , Rsm_NombreArchivoXML , Rsm_NombreArchivoWeb , Rsm_Estado ,
	  Doc_Aud_Registro_FechaHora , Doc_Aud_Registro_Equipo )
	VALUES
	( @PRsm_Emisor_RUC , @PRsm_FechaGeneracion , @PRsm_Correlativo ,
	  @PRsm_SUNATTicket_ID , @PRsm_NombreArchivoXML , @PRsm_NombreArchivoWeb , '4106' ,
	  GETDATE() ,HOST_NAME());
END
GO
CREATE PROC UP_FACTE_RESUMEN_BAJA_DOCUMENTO_INSERT
(
@PRsm_Emisor_RUC CHAR(11),
@PRsm_FechaGeneracion DATE,
@PRsm_Correlativo INT,
@PRsm_Doc_Item INT,

@PRsm_Doc_Tipo CHAR(2),
@PRsm_Doc_Serie CHAR(4),
@PRsm_Doc_Numero CHAR(8)
)
AS
BEGIN
	INSERT INTO dbo.FACTE_RESUMEN_BAJA_DOCUMENTO
	        ( Rsm_Emisor_RUC , Rsm_FechaGeneracion , Rsm_Correlativo , Rsm_Doc_Item ,
	          Rsm_Doc_Tipo , Rsm_Doc_Serie , Rsm_Doc_Numero )
	VALUES  ( @PRsm_Emisor_RUC , @PRsm_FechaGeneracion, @PRsm_Correlativo , @PRsm_Doc_Item , -- Rsm_Doc_Item - int
	          @PRsm_Doc_Tipo , @PRsm_Doc_Serie , @PRsm_Doc_Numero );
END
GO
ALTER PROC UP_FACTE_RESUMEN_BAJA_DOCUMENTO_TICKET_PENDIENTES
AS
BEGIN
	SELECT Res.Rsm_SUNATTicket_ID, Res.Rsm_Emisor_RUC FROM dbo.FACTE_RESUMEN_BAJA Res 
	WITH(NOLOCK) WHERE Res.Rsm_Estado='4106'--4106-ENVIADO 4107-ACEPTADOS 4108-RECHAZADO
END
GO
CREATE PROC UP_FACTE_RESUMEN_BAJA_DOCUMENTO_TICKET_CAMBIARESTADO
(
@PRsm_Emisor_RUC CHAR(11),
@PRsm_SUNATTicket_ID VARCHAR(50),
@PRsm_Estado CHAR(4)
)
AS
BEGIN
	UPDATE FACTE_RESUMEN_BAJA SET Rsm_Estado = @PRsm_Estado
	WHERE Rsm_Emisor_RUC = @PRsm_Emisor_RUC AND Rsm_SUNATTicket_ID = @PRsm_SUNATTicket_ID ;
END
GO
CREATE PROC UP_FACTE_RESUMEN_BAJA_DOCUMENTO_LISTA
(
@PRsm_Emisor_RUC CHAR(11),
@PRsm_SUNATTicket_ID VARCHAR(50)
)
AS
BEGIN
	SELECT doc.Rsm_Emisor_RUC ,Doc.Rsm_Doc_Tipo, doc.Rsm_Doc_Serie, doc.Rsm_Doc_Numero FROM dbo.FACTE_RESUMEN_BAJA_DOCUMENTO Doc
	INNER JOIN dbo.FACTE_RESUMEN_BAJA Cab WITH(NOLOCK) ON Cab.Rsm_Emisor_RUC = Doc.Rsm_Emisor_RUC AND Cab.Rsm_FechaGeneracion = Doc.Rsm_FechaGeneracion AND Cab.Rsm_Correlativo = Doc.Rsm_Correlativo
	WHERE Cab.Rsm_Emisor_RUC = @PRsm_Emisor_RUC AND Cab.Rsm_SUNATTicket_ID = @PRsm_SUNATTicket_ID ;
END
GO
CREATE PROC UP_FACTE_DOCUMENTOS_LISTA
(
@PDoc_Fecha_Desde DATE,
@PDoc_Fecha_Hasta DATE,
@PDoc_Emisor_RUC CHAR(11)=NULL,
@PDoc_Tipo CHAR(2)=NULL,
@PDoc_Serie CHAR(4)=NULL,
@PDoc_Numero CHAR(8)=NULL
)
AS
BEGIN
	SELECT Doc_Emisor_RUC, Doc_Tipo, Tipo.TipoDoc_Descripcion, Doc_Serie, Doc_Numero, Doc_FechaEmision, Doc_Moneda, Mon.Mon_Simbolo, Doc_Importe, 
	Doc_Cliente_NumDoc, Doc_Cliente_Rs,doc.Doc_Estado, Est.Est_Descripcion,
	Empr.PathLocal,Doc_NombreArchivoXML, Doc_NombreArchivoPDF, Doc_NombreArchivoWeb, ISNULL(Doc.Doc_Correo_Enviado,0), ISNULL(Doc.Doc_Correo_EstaLeido,0)
	FROM dbo.FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tipo WITH(NOLOCK) ON Doc.Doc_Tipo = Tipo.TipoDoc_Codigo
	INNER JOIN dbo.FACTE_MONEDA Mon WITH(NOLOCK) ON Doc.Doc_Moneda = Mon.Mon_Codigo
	INNER JOIN dbo.FACTE_EMPRESA Empr WITH(NOLOCK) ON Empr.Emp_RUC = Doc.Doc_Emisor_RUC
	INNER JOIN dbo.FACTE_ESTADO Est WITH(NOLOCK) ON Doc.Doc_Estado = Est.Est_Codigo_Cliente
	WHERE (Doc.Doc_FechaEmision BETWEEN @PDoc_Fecha_Desde AND @PDoc_Fecha_Hasta)
	AND (@PDoc_Emisor_RUC IS NULL OR Doc.Doc_Emisor_RUC = @PDoc_Emisor_RUC)
	AND (@PDoc_Tipo IS NULL OR Doc.Doc_Tipo = @PDoc_Tipo)
	AND (@PDoc_Serie IS NULL OR Doc.Doc_Serie = @PDoc_Serie)
	AND (@PDoc_Numero IS NULL OR Doc.Doc_Numero = @PDoc_Numero);
END
GO
CREATE PROC UP_FACTE_DOCUMENTOS_RESUMEN_BOLETAS_LISTA
(
@PDoc_Fecha_Desde DATE,
@PDoc_Fecha_Hasta DATE,
@PDoc_Emisor_RUC CHAR(11)=NULL,
@PDoc_Tipo CHAR(2)=NULL,
@PDoc_Serie CHAR(4)=NULL,
@PDoc_Numero CHAR(8)=NULL
)
AS
BEGIN
	SELECT Resu.Doc_Emisor_RUC, Resu.Doc_FechaGeneracion, Resu.Doc_Resumen_Correlativo, Resu.Doc_Moneda, Mon.Mon_Simbolo, 
	Resu.Doc_FechaEmision, Resu.Doc_Tipo, Tipo.TipoDoc_Descripcion, Resu.Doc_Serie, Resu.Doc_NumeroInicio, Resu.Doc_NumeroFin, Resu.Doc_ImporteResumen, 	
	Empr.PathLocal, Resu.Doc_NombreArchivoXML, Resu.Doc_SUNATTicket_ID, Resu.Doc_Estado, Est.Est_Descripcion,
	Resu.Doc_NombreArchivoWeb
	FROM dbo.FACTE_RESUMEN_DOCUMENTOS_ACEPTADO Resu
	INNER JOIN dbo.FACTE_MONEDA Mon WITH(NOLOCK) ON Resu.Doc_Moneda = Mon.Mon_Codigo
	INNER JOIN dbo.FACTE_DOCUMENTO_TIPO Tipo WITH(NOLOCK) ON Resu.Doc_Tipo = Tipo.TipoDoc_Codigo	
	INNER JOIN dbo.FACTE_EMPRESA Empr WITH(NOLOCK) ON Empr.Emp_RUC = Resu.Doc_Emisor_RUC
	INNER JOIN dbo.FACTE_ESTADO Est WITH(NOLOCK) ON Resu.Doc_Estado = Est.Est_Codigo_Cliente
	WHERE (Resu.Doc_FechaEmision BETWEEN @PDoc_Fecha_Desde AND @PDoc_Fecha_Hasta)
	AND (@PDoc_Emisor_RUC IS NULL OR Resu.Doc_Emisor_RUC = @PDoc_Emisor_RUC)
	AND (@PDoc_Tipo IS NULL OR Resu.Doc_Tipo = @PDoc_Tipo)
	AND (@PDoc_Serie IS NULL OR Resu.Doc_Serie = @PDoc_Serie)
	AND (@PDoc_Numero IS NULL OR (CAST(@PDoc_Numero AS INT) BETWEEN CAST(Resu.Doc_NumeroInicio AS INT) AND CAST(Resu.Doc_NumeroFin AS INT)));
END
GO
ALTER PROC UP_FACTE_RECEPTORES_LISTA
(
@PReceptor_TipoDocIdent_Codigo CHAR(1)='6',
@PReceptor_TipoDocIdent_Numero VARCHAR(11)=NULL
)
AS
BEGIN
	SELECT Receptor_TipoDocIdent_Codigo, Receptor_TipoDocIdent_Numero, Receptor_Registro, 
	Receptor_Email, Receptor_Validado, Receptor_CopiadoACliente, Aud_Registro_Fecha
	FROM dbo.FACTE_RECEPTORES_CORREO
	WHERE (@PReceptor_TipoDocIdent_Numero IS NULL OR Receptor_TipoDocIdent_Numero = @PReceptor_TipoDocIdent_Numero)
	ORDER BY Receptor_TipoDocIdent_Numero ASC,Receptor_Registro DESC;
END
GO
---PROGRAMACION DE ENVIOS
GO
CREATE TABLE FACTE_PROGRAMACIONES_ENVIO
(
	Emp_RUC CHAR(11),
	Progra_Correlativo INT,
	Progra_Descripcion VARCHAR(100),
	Progra_Habilitado BIT,

	Progra_Appli_TipoDoc CHAR(2),
	Progra_Appli_Filtra_Serie BIT,
	Progra_Appli_Serie CHAR(4),

	Progra_EsResumenBoletas BIT,
	Progra_EsComunicacionBaja BIT,

	Progra_FrecuEnvio_Hora_Es_Establecido BIT, --Se establece la hora de envio
	Progra_FrecuEnvio_Hora_Es_XCadaHora BIT, --Se pone el cada cuanto se repite
	Progra_FrecuEnvio_Hora_Es_DespuesEmitido BIT, --Se pone las horas despues de emitido
	Progra_FrecuEnvio_Hora_Es_DespuesConfirmado BIT, --El usuario confirma para enviarlo

	Progra_FrecuEnvio_Hora_Desde TIME,
	Progra_FrecuEnvio_Hora_Hasta TIME,
	Progra_FrecuEnvio_Hora_XCadaHora INT,

	Progra_Fecha_Desde DATE,
	Progra_Fecha_SinFin BIT,
	Progra_Fecha_Hasta DATE,

	Progra_Ejecucion_Ultimo DATETIME, ---Por default (insert) es null, en caso de modificacion tambien se pone null

	Progra_Estado CHAR(3),

	Aud_Registro_FechaHora DATETIME,
	Aud_Registro_Equipo VARCHAR(50),
	Aud_Actualizacion_FechaHora DATETIME,
	Aud_Actualizacion_Equipo VARCHAR(50),

	CONSTRAINT PK_FACTE_PROGRAMACIONES_ENVIO PRIMARY KEY CLUSTERED 
	(
		Emp_RUC ASC, Progra_Correlativo ASC
	)
)
GO
--DEMO-INICIO
GO
INSERT FACTE_PROGRAMACIONES_ENVIO (Emp_RUC, Progra_Correlativo, Progra_Descripcion, Progra_Habilitado, Progra_Appli_TipoDoc, Progra_Appli_Filtra_Serie, Progra_Appli_Serie, Progra_EsResumenBoletas, Progra_EsComunicacionBaja, Progra_FrecuEnvio_Hora_Es_Establecido, Progra_FrecuEnvio_Hora_Es_XCadaHora, Progra_FrecuEnvio_Hora_Es_DespuesEmitido, Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, Progra_FrecuEnvio_Hora_Desde, Progra_FrecuEnvio_Hora_Hasta, Progra_FrecuEnvio_Hora_XCadaHora, Progra_Fecha_Desde, Progra_Fecha_SinFin, Progra_Fecha_Hasta, Progra_Estado, Aud_Registro_FechaHora, Aud_Registro_Equipo, Aud_Actualizacion_FechaHora, Aud_Actualizacion_Equipo) VALUES (N'20115643216', 1, N'ENVIAR TODAS LAS FACTURAS', 1, N'01', 0, NULL, 0, 0, 0, 1, 0, 0, CAST(N'00:00:00' AS Time), CAST(N'23:59:59' AS Time), 0, CAST(N'2016-05-01' AS Date), 1, NULL, N'001', CAST(N'2016-05-14 11:39:05.643' AS DateTime), N'CGNTIC01', NULL, NULL);
INSERT FACTE_PROGRAMACIONES_ENVIO (Emp_RUC, Progra_Correlativo, Progra_Descripcion, Progra_Habilitado, Progra_Appli_TipoDoc, Progra_Appli_Filtra_Serie, Progra_Appli_Serie, Progra_EsResumenBoletas, Progra_EsComunicacionBaja, Progra_FrecuEnvio_Hora_Es_Establecido, Progra_FrecuEnvio_Hora_Es_XCadaHora, Progra_FrecuEnvio_Hora_Es_DespuesEmitido, Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, Progra_FrecuEnvio_Hora_Desde, Progra_FrecuEnvio_Hora_Hasta, Progra_FrecuEnvio_Hora_XCadaHora, Progra_Fecha_Desde, Progra_Fecha_SinFin, Progra_Fecha_Hasta, Progra_Estado, Aud_Registro_FechaHora, Aud_Registro_Equipo, Aud_Actualizacion_FechaHora, Aud_Actualizacion_Equipo) VALUES (N'20115643216', 2, N'ENVIAR TODAS LAS BOLETAS', 1, N'03', 0, NULL, 0, 0, 0, 1, 0, 0, CAST(N'00:00:00' AS Time), CAST(N'23:59:59' AS Time), 0, CAST(N'2016-05-01' AS Date), 1, NULL, N'001', CAST(N'2016-05-14 11:40:17.360' AS DateTime), N'CGNTIC01', NULL, NULL);
INSERT FACTE_PROGRAMACIONES_ENVIO (Emp_RUC, Progra_Correlativo, Progra_Descripcion, Progra_Habilitado, Progra_Appli_TipoDoc, Progra_Appli_Filtra_Serie, Progra_Appli_Serie, Progra_EsResumenBoletas, Progra_EsComunicacionBaja, Progra_FrecuEnvio_Hora_Es_Establecido, Progra_FrecuEnvio_Hora_Es_XCadaHora, Progra_FrecuEnvio_Hora_Es_DespuesEmitido, Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, Progra_FrecuEnvio_Hora_Desde, Progra_FrecuEnvio_Hora_Hasta, Progra_FrecuEnvio_Hora_XCadaHora, Progra_Fecha_Desde, Progra_Fecha_SinFin, Progra_Fecha_Hasta, Progra_Estado, Aud_Registro_FechaHora, Aud_Registro_Equipo, Aud_Actualizacion_FechaHora, Aud_Actualizacion_Equipo) VALUES (N'20115643216', 3, N'COMUNICAR BAJA DE FACTURAS', 1, N'01', 0, NULL, 0, 1, 0, 1, 0, 0, CAST(N'00:00:00' AS Time), CAST(N'23:59:59' AS Time), 3, CAST(N'2016-05-01' AS Date), 1, NULL, N'001', CAST(N'2016-05-14 11:42:43.827' AS DateTime), N'CGNTIC01', NULL, NULL);
INSERT FACTE_PROGRAMACIONES_ENVIO (Emp_RUC, Progra_Correlativo, Progra_Descripcion, Progra_Habilitado, Progra_Appli_TipoDoc, Progra_Appli_Filtra_Serie, Progra_Appli_Serie, Progra_EsResumenBoletas, Progra_EsComunicacionBaja, Progra_FrecuEnvio_Hora_Es_Establecido, Progra_FrecuEnvio_Hora_Es_XCadaHora, Progra_FrecuEnvio_Hora_Es_DespuesEmitido, Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, Progra_FrecuEnvio_Hora_Desde, Progra_FrecuEnvio_Hora_Hasta, Progra_FrecuEnvio_Hora_XCadaHora, Progra_Fecha_Desde, Progra_Fecha_SinFin, Progra_Fecha_Hasta, Progra_Estado, Aud_Registro_FechaHora, Aud_Registro_Equipo, Aud_Actualizacion_FechaHora, Aud_Actualizacion_Equipo) VALUES (N'20115643216', 4, N'COMUNICAR BAJA DE BOLETAS', 1, N'03', 0, NULL, 0, 1, 0, 1, 0, 0, CAST(N'00:00:00' AS Time), CAST(N'23:59:59' AS Time), 3, CAST(N'2016-05-01' AS Date), 1, NULL, N'001', CAST(N'2016-05-14 11:44:13.370' AS DateTime), N'CGNTIC01', NULL, NULL);
INSERT FACTE_PROGRAMACIONES_ENVIO (Emp_RUC, Progra_Correlativo, Progra_Descripcion, Progra_Habilitado, Progra_Appli_TipoDoc, Progra_Appli_Filtra_Serie, Progra_Appli_Serie, Progra_EsResumenBoletas, Progra_EsComunicacionBaja, Progra_FrecuEnvio_Hora_Es_Establecido, Progra_FrecuEnvio_Hora_Es_XCadaHora, Progra_FrecuEnvio_Hora_Es_DespuesEmitido, Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, Progra_FrecuEnvio_Hora_Desde, Progra_FrecuEnvio_Hora_Hasta, Progra_FrecuEnvio_Hora_XCadaHora, Progra_Fecha_Desde, Progra_Fecha_SinFin, Progra_Fecha_Hasta, Progra_Estado, Aud_Registro_FechaHora, Aud_Registro_Equipo, Aud_Actualizacion_FechaHora, Aud_Actualizacion_Equipo) VALUES (N'20115643216', 5, N'ENVIAR TODAS NOTAS DE CREDITO', 1, N'07', 0, NULL, 0, 0, 0, 1, 0, 0, CAST(N'00:00:00' AS Time), CAST(N'23:59:59' AS Time), 3, CAST(N'2016-05-01' AS Date), 1, NULL, N'001', CAST(N'2016-05-14 11:45:35.537' AS DateTime), N'CGNTIC01', NULL, NULL);
INSERT FACTE_PROGRAMACIONES_ENVIO (Emp_RUC, Progra_Correlativo, Progra_Descripcion, Progra_Habilitado, Progra_Appli_TipoDoc, Progra_Appli_Filtra_Serie, Progra_Appli_Serie, Progra_EsResumenBoletas, Progra_EsComunicacionBaja, Progra_FrecuEnvio_Hora_Es_Establecido, Progra_FrecuEnvio_Hora_Es_XCadaHora, Progra_FrecuEnvio_Hora_Es_DespuesEmitido, Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, Progra_FrecuEnvio_Hora_Desde, Progra_FrecuEnvio_Hora_Hasta, Progra_FrecuEnvio_Hora_XCadaHora, Progra_Fecha_Desde, Progra_Fecha_SinFin, Progra_Fecha_Hasta, Progra_Estado, Aud_Registro_FechaHora, Aud_Registro_Equipo, Aud_Actualizacion_FechaHora, Aud_Actualizacion_Equipo) VALUES (N'20115643216', 6, N'ENVIAR TODAS NOTAS DE DEBITO', 1, N'08', 0, NULL, 0, 0, 0, 1, 0, 0, CAST(N'00:00:00' AS Time), CAST(N'23:59:59' AS Time), 3, CAST(N'2016-05-01' AS Date), 1, NULL, N'001', CAST(N'2016-05-14 11:46:42.397' AS DateTime), N'CGNTIC01', NULL, NULL);
INSERT FACTE_PROGRAMACIONES_ENVIO (Emp_RUC, Progra_Correlativo, Progra_Descripcion, Progra_Habilitado, Progra_Appli_TipoDoc, Progra_Appli_Filtra_Serie, Progra_Appli_Serie, Progra_EsResumenBoletas, Progra_EsComunicacionBaja, Progra_FrecuEnvio_Hora_Es_Establecido, Progra_FrecuEnvio_Hora_Es_XCadaHora, Progra_FrecuEnvio_Hora_Es_DespuesEmitido, Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, Progra_FrecuEnvio_Hora_Desde, Progra_FrecuEnvio_Hora_Hasta, Progra_FrecuEnvio_Hora_XCadaHora, Progra_Fecha_Desde, Progra_Fecha_SinFin, Progra_Fecha_Hasta, Progra_Estado, Aud_Registro_FechaHora, Aud_Registro_Equipo, Aud_Actualizacion_FechaHora, Aud_Actualizacion_Equipo) VALUES (N'20115643216', 7, N'COMUNICAR BAJA DE NOTA DE CREDITO', 1, N'07', 0, NULL, 0, 1, 0, 1, 0, 0, CAST(N'00:00:00' AS Time), CAST(N'23:59:59' AS Time), 3, CAST(N'2016-05-01' AS Date), 1, NULL, N'001', CAST(N'2016-05-14 11:47:55.553' AS DateTime), N'CGNTIC01', NULL, NULL);
INSERT FACTE_PROGRAMACIONES_ENVIO (Emp_RUC, Progra_Correlativo, Progra_Descripcion, Progra_Habilitado, Progra_Appli_TipoDoc, Progra_Appli_Filtra_Serie, Progra_Appli_Serie, Progra_EsResumenBoletas, Progra_EsComunicacionBaja, Progra_FrecuEnvio_Hora_Es_Establecido, Progra_FrecuEnvio_Hora_Es_XCadaHora, Progra_FrecuEnvio_Hora_Es_DespuesEmitido, Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, Progra_FrecuEnvio_Hora_Desde, Progra_FrecuEnvio_Hora_Hasta, Progra_FrecuEnvio_Hora_XCadaHora, Progra_Fecha_Desde, Progra_Fecha_SinFin, Progra_Fecha_Hasta, Progra_Estado, Aud_Registro_FechaHora, Aud_Registro_Equipo, Aud_Actualizacion_FechaHora, Aud_Actualizacion_Equipo) VALUES (N'20115643216', 8, N'COMUNICAR BAJA DE NOTA DE DEBITO', 1, N'08', 0, NULL, 0, 1, 0, 1, 0, 0, CAST(N'00:00:00' AS Time), CAST(N'23:59:59' AS Time), 3, CAST(N'2016-05-01' AS Date), 1, NULL, N'001', CAST(N'2016-05-14 11:48:51.440' AS DateTime), N'CGNTIC01', NULL, NULL);
INSERT FACTE_PROGRAMACIONES_ENVIO (Emp_RUC, Progra_Correlativo, Progra_Descripcion, Progra_Habilitado, Progra_Appli_TipoDoc, Progra_Appli_Filtra_Serie, Progra_Appli_Serie, Progra_EsResumenBoletas, Progra_EsComunicacionBaja, Progra_FrecuEnvio_Hora_Es_Establecido, Progra_FrecuEnvio_Hora_Es_XCadaHora, Progra_FrecuEnvio_Hora_Es_DespuesEmitido, Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, Progra_FrecuEnvio_Hora_Desde, Progra_FrecuEnvio_Hora_Hasta, Progra_FrecuEnvio_Hora_XCadaHora, Progra_Fecha_Desde, Progra_Fecha_SinFin, Progra_Fecha_Hasta, Progra_Estado, Aud_Registro_FechaHora, Aud_Registro_Equipo, Aud_Actualizacion_FechaHora, Aud_Actualizacion_Equipo) VALUES (N'20115643216', 9, N'SDDSDDS', 1, N'01', 1, N'0012', 0, 0, 1, 0, 0, 0, CAST(N'00:00:00' AS Time), CAST(N'23:59:59' AS Time), 0, CAST(N'2015-01-01' AS Date), 1, NULL, N'002', CAST(N'2016-05-16 16:53:03.927' AS DateTime), N'CGNTIC01', NULL, NULL);
INSERT FACTE_PROGRAMACIONES_ENVIO (Emp_RUC, Progra_Correlativo, Progra_Descripcion, Progra_Habilitado, Progra_Appli_TipoDoc, Progra_Appli_Filtra_Serie, Progra_Appli_Serie, Progra_EsResumenBoletas, Progra_EsComunicacionBaja, Progra_FrecuEnvio_Hora_Es_Establecido, Progra_FrecuEnvio_Hora_Es_XCadaHora, Progra_FrecuEnvio_Hora_Es_DespuesEmitido, Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, Progra_FrecuEnvio_Hora_Desde, Progra_FrecuEnvio_Hora_Hasta, Progra_FrecuEnvio_Hora_XCadaHora, Progra_Fecha_Desde, Progra_Fecha_SinFin, Progra_Fecha_Hasta, Progra_Estado, Aud_Registro_FechaHora, Aud_Registro_Equipo, Aud_Actualizacion_FechaHora, Aud_Actualizacion_Equipo) VALUES (N'20115643216', 10, N'VFBJDF', 1, N'01', 1, N'E013', 0, 0, 1, 0, 0, 0, CAST(N'00:00:00' AS Time), CAST(N'23:59:59' AS Time), 0, CAST(N'2015-01-01' AS Date), 1, NULL, N'002', CAST(N'2016-05-16 16:54:06.653' AS DateTime), N'CGNTIC01', NULL, NULL);
INSERT FACTE_PROGRAMACIONES_ENVIO (Emp_RUC, Progra_Correlativo, Progra_Descripcion, Progra_Habilitado, Progra_Appli_TipoDoc, Progra_Appli_Filtra_Serie, Progra_Appli_Serie, Progra_EsResumenBoletas, Progra_EsComunicacionBaja, Progra_FrecuEnvio_Hora_Es_Establecido, Progra_FrecuEnvio_Hora_Es_XCadaHora, Progra_FrecuEnvio_Hora_Es_DespuesEmitido, Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, Progra_FrecuEnvio_Hora_Desde, Progra_FrecuEnvio_Hora_Hasta, Progra_FrecuEnvio_Hora_XCadaHora, Progra_Fecha_Desde, Progra_Fecha_SinFin, Progra_Fecha_Hasta, Progra_Estado, Aud_Registro_FechaHora, Aud_Registro_Equipo, Aud_Actualizacion_FechaHora, Aud_Actualizacion_Equipo) VALUES (N'20115643216', 11, N'NGFG', 1, N'03', 1, N'F045', 1, 0, 1, 0, 0, 0, CAST(N'00:00:00' AS Time), CAST(N'23:59:59' AS Time), 0, CAST(N'2015-01-01' AS Date), 1, NULL, N'002', CAST(N'2016-05-16 16:56:12.550' AS DateTime), N'CGNTIC01', NULL, NULL);
INSERT FACTE_PROGRAMACIONES_ENVIO (Emp_RUC, Progra_Correlativo, Progra_Descripcion, Progra_Habilitado, Progra_Appli_TipoDoc, Progra_Appli_Filtra_Serie, Progra_Appli_Serie, Progra_EsResumenBoletas, Progra_EsComunicacionBaja, Progra_FrecuEnvio_Hora_Es_Establecido, Progra_FrecuEnvio_Hora_Es_XCadaHora, Progra_FrecuEnvio_Hora_Es_DespuesEmitido, Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, Progra_FrecuEnvio_Hora_Desde, Progra_FrecuEnvio_Hora_Hasta, Progra_FrecuEnvio_Hora_XCadaHora, Progra_Fecha_Desde, Progra_Fecha_SinFin, Progra_Fecha_Hasta, Progra_Estado, Aud_Registro_FechaHora, Aud_Registro_Equipo, Aud_Actualizacion_FechaHora, Aud_Actualizacion_Equipo) VALUES (N'20542134926', 1, N'ENVIAR FACTURAS', 1, N'01', 0, NULL, 0, 0, 0, 0, 1, 0, CAST(N'00:00:00' AS Time), CAST(N'23:59:59' AS Time), 1, CAST(N'2016-01-01' AS Date), 1, NULL, N'002', CAST(N'2016-05-04 15:39:36.060' AS DateTime), N'CGNTIC01', NULL, NULL);
INSERT FACTE_PROGRAMACIONES_ENVIO (Emp_RUC, Progra_Correlativo, Progra_Descripcion, Progra_Habilitado, Progra_Appli_TipoDoc, Progra_Appli_Filtra_Serie, Progra_Appli_Serie, Progra_EsResumenBoletas, Progra_EsComunicacionBaja, Progra_FrecuEnvio_Hora_Es_Establecido, Progra_FrecuEnvio_Hora_Es_XCadaHora, Progra_FrecuEnvio_Hora_Es_DespuesEmitido, Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, Progra_FrecuEnvio_Hora_Desde, Progra_FrecuEnvio_Hora_Hasta, Progra_FrecuEnvio_Hora_XCadaHora, Progra_Fecha_Desde, Progra_Fecha_SinFin, Progra_Fecha_Hasta, Progra_Estado, Aud_Registro_FechaHora, Aud_Registro_Equipo, Aud_Actualizacion_FechaHora, Aud_Actualizacion_Equipo) VALUES (N'20542134926', 2, N'ENVIAR RESUMENES DE BOLETAS', 1, N'03', 0, NULL, 1, 0, 0, 1, 0, 0, CAST(N'00:00:00' AS Time), CAST(N'23:59:59' AS Time), 4, CAST(N'2016-01-01' AS Date), 1, NULL, N'002', CAST(N'2016-05-04 15:50:11.330' AS DateTime), N'CGNTIC01', NULL, NULL);
GO
--DEMO-FIN
GO
CREATE PROC UP_FACTE_PROGRAMACIONES_ENVIO_LISTA
(
@PEmp_RUC char(11)
)
AS
BEGIN
	SELECT Prog.Emp_RUC, Prog.Progra_Correlativo, Prog.Progra_Descripcion, Prog.Progra_Habilitado, 
	Prog.Progra_Appli_TipoDoc, Prog.Progra_Appli_Filtra_Serie, ISNULL(Prog.Progra_Appli_Serie,''), Prog.Progra_EsResumenBoletas, Prog.Progra_EsComunicacionBaja, 
	Prog.Progra_FrecuEnvio_Hora_Es_Establecido, Prog.Progra_FrecuEnvio_Hora_Es_XCadaHora, Prog.Progra_FrecuEnvio_Hora_Es_DespuesEmitido, 
	Prog.Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, Prog.Progra_FrecuEnvio_Hora_Desde, Prog.Progra_FrecuEnvio_Hora_Hasta, 
	Prog.Progra_FrecuEnvio_Hora_XCadaHora,
	Prog.Progra_Fecha_Desde , Prog.Progra_Fecha_SinFin , ISNULL(Prog.Progra_Fecha_Hasta,CAST('0001-01-01' AS DATE)) ,
	Prog.Progra_Estado, GETDATE(), TDoc.TipoDoc_Descripcion
	FROM dbo.FACTE_PROGRAMACIONES_ENVIO Prog
	INNER JOIN dbo.FACTE_DOCUMENTO_TIPO TDoc ON Prog.Progra_Appli_TipoDoc = TDoc.TipoDoc_Codigo
	WHERE Prog.Emp_RUC = @PEmp_RUC AND Prog.Progra_Estado='001';
END
GO
ALTER PROC UP_FACTE_PROGRAMACIONES_ENVIO_LISTA_HABILITADOS
AS
BEGIN
	--Habilitados y listos para enviar
	DECLARE @AhoraFechaHora DATETIME = CONVERT(VARCHAR, GETDATE(), 120);--yyyy-mm-dd hh:mm:ss(24h)
	DECLARE @AhoraSoloHora TIME = @AhoraFechaHora;

	UPDATE FACTE_PROGRAMACIONES_ENVIO SET Progra_Ejecucion_Ultimo = GETDATE() WHERE Progra_Ejecucion_Ultimo IS NULL;

	--SOLO DOCUMENTOS ACTIVOS
	SELECT TOP 1 DocXML.TipXML_Codigo,doc.Doc_Emisor_RUC,Doc.Doc_Tipo,Doc.Doc_Serie,Doc.Doc_Numero,doc.Doc_FechaEmision,doc.Doc_Moneda,Doc.Doc_Importe,
	DocXML.XML_Correlativo,DocXML.XML_Archivo
	FROM dbo.FACTE_DOCUMENTO_ELECTRONICO Doc WITH(NOLOCK)
	INNER JOIN dbo.FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE DocXML WITH(NOLOCK) ON DocXML.Doc_Emisor_RUC = Doc.Doc_Emisor_RUC AND DocXML.Doc_Tipo = Doc.Doc_Tipo AND DocXML.Doc_Serie = Doc.Doc_Serie AND DocXML.Doc_Numero = Doc.Doc_Numero AND DocXML.XML_Reciente = 1
	INNER JOIN (
	SELECT ProgX.Emp_RUC, ProgX.Progra_Appli_TipoDoc,
	(CASE WHEN ProgX.Progra_Appli_Filtra_Serie = 1 THEN ProgX.Progra_Appli_Serie ELSE NULL END) AS 'TipoDoc_Serie', 
	ProgX.Progra_EsResumenBoletas, ProgX.Progra_EsComunicacionBaja
	FROM dbo.FACTE_PROGRAMACIONES_ENVIO ProgX
	WHERE ProgX.Progra_Habilitado = 1
	AND ProgX.Progra_Estado='001'
	AND @AhoraFechaHora >= ProgX.Progra_Fecha_Desde AND @AhoraFechaHora <= (CASE WHEN ProgX.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE ProgX.Progra_Fecha_Hasta END)
	AND DATEADD(HOUR,ProgX.Progra_FrecuEnvio_Hora_XCadaHora, ProgX.Progra_Ejecucion_Ultimo) <= @AhoraFechaHora
	AND ProgX.Progra_FrecuEnvio_Hora_Desde <= @AhoraSoloHora AND ProgX.Progra_FrecuEnvio_Hora_Hasta >= @AhoraSoloHora
	) AS Progra ON doc.Doc_Emisor_RUC = Progra.Emp_RUC AND Doc.Doc_Tipo =Progra.Progra_Appli_TipoDoc
	WHERE DocXML.EstEnv_Codigo='001'
	--SELECT Prog.Emp_RUC, Prog.Progra_Appli_TipoDoc, 
	----Prog.Progra_Appli_Filtra_Serie,
	--(CASE WHEN Prog.Progra_Appli_Filtra_Serie = 1 THEN Prog.Progra_Appli_Serie ELSE NULL END) AS 'TipoDoc_Serie', 
	--Prog.Progra_EsResumenBoletas, Prog.Progra_EsComunicacionBaja, 
	----Prog.Progra_FrecuEnvio_Hora_Es_Establecido, --Se produce a las x horas
	----Prog.Progra_FrecuEnvio_Hora_Es_XCadaHora, --Ocurre cada (Hora inicio - Hora fin)
	----Prog.Progra_FrecuEnvio_Hora_Es_DespuesEmitido, --Ocurre cada x horas emitido el documento
	----Prog.Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, --Falta
	 
	----Prog.Progra_FrecuEnvio_Hora_Desde, --Hora inicio
	----Prog.Progra_FrecuEnvio_Hora_Hasta, --Hora fin

	----Prog.Progra_FrecuEnvio_Hora_XCadaHora, --hora x

	--Prog.Progra_Fecha_Desde , 
	----Prog.Progra_Fecha_SinFin , 
	--(CASE WHEN Prog.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE Prog.Progra_Fecha_Hasta END)
	----, Prog.Progra_Estado

	--FROM dbo.FACTE_PROGRAMACIONES_ENVIO Prog
	--WHERE Prog.Progra_Habilitado = 1
	--AND Prog.Progra_Estado='001'
	
	--AND @AhoraFechaHora >= Prog.Progra_Fecha_Desde AND @AhoraFechaHora <= (CASE WHEN Prog.Progra_Fecha_SinFin = 1 THEN @AhoraFechaHora ELSE Prog.Progra_Fecha_Hasta END)

	--AND DATEADD(HOUR,Prog.Progra_FrecuEnvio_Hora_XCadaHora, Prog.Progra_Ejecucion_Ultimo) <= @AhoraFechaHora

	--AND Prog.Progra_FrecuEnvio_Hora_Desde <= @AhoraSoloHora AND Prog.Progra_FrecuEnvio_Hora_Hasta >= @AhoraSoloHora
	--ORDER BY Prog.Emp_RUC ASC,Prog.Progra_Appli_TipoDoc ASC,Prog.Progra_Appli_Filtra_Serie DESC,Prog.Progra_Appli_Serie ASC,
	--Prog.Progra_EsResumenBoletas ASC, Prog.Progra_EsComunicacionBaja ASC;

	--SELECT Prog.Emp_RUC, Prog.Progra_Correlativo, Prog.Progra_Descripcion, Prog.Progra_Habilitado, 
	--Prog.Progra_Appli_TipoDoc, Prog.Progra_Appli_Filtra_Serie, ISNULL(Prog.Progra_Appli_Serie,'9999'), Prog.Progra_EsResumenBoletas, Prog.Progra_EsComunicacionBaja, 
	--Prog.Progra_FrecuEnvio_Hora_Es_Establecido, Prog.Progra_FrecuEnvio_Hora_Es_XCadaHora, Prog.Progra_FrecuEnvio_Hora_Es_DespuesEmitido, 
	--Prog.Progra_FrecuEnvio_Hora_Es_DespuesConfirmado, Prog.Progra_FrecuEnvio_Hora_Desde, Prog.Progra_FrecuEnvio_Hora_Hasta, 
	--Prog.Progra_FrecuEnvio_Hora_XCadaHora,
	--Prog.Progra_Fecha_Desde , Prog.Progra_Fecha_SinFin , ISNULL(Prog.Progra_Fecha_Hasta,GETDATE()) ,
	--Prog.Progra_Estado, @Ahora
	--FROM dbo.FACTE_PROGRAMACIONES_ENVIO Prog
	--WHERE Prog.Progra_Habilitado = 1 
	--AND Prog.Progra_Estado='001'
	--ORDER BY Prog.Emp_RUC ASC,Prog.Progra_Appli_TipoDoc ASC,Prog.Progra_Appli_Filtra_Serie DESC,Prog.Progra_Appli_Serie ASC,
	--Prog.Progra_EsResumenBoletas ASC, Prog.Progra_EsComunicacionBaja ASC;

	--ESTE ORDEN ES IMPORTANTE PARA ORDENANAR PRIMERO A LOS FILTROS POR DOCUMENTO Y SERIE

END
GO
CREATE PROC UP_FACTE_DOCUMENTO_ELECTRONICO_ENVIADO
(
@PDoc_Emisor_RUC CHAR(11),
@PDoc_Tipo CHAR(2),
@PDoc_Serie CHAR(4),
@PDoc_Numero CHAR(8),
@PXML_Correlativo INT,
@PEstEnv_Codigo CHAR(3)
)
AS
BEGIN
	UPDATE FACTE_DOCUMENTO_ELECTRONICO_XML_CLIENTE SET EstEnv_Codigo = @PEstEnv_Codigo
	WHERE Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Doc_Tipo = @PDoc_Tipo AND Doc_Serie = @PDoc_Serie
	AND Doc_Numero = @PDoc_Numero AND XML_Correlativo = @PXML_Correlativo ;
END
GO
CREATE PROC UP_FACTE_PROGRAMACIONES_ENVIO_INSERT
(
@PEmp_RUC char(11),
@PProgra_Correlativo INT OUTPUT,
@PProgra_Descripcion varchar(100),
@PProgra_Habilitado BIT,
@PProgra_Appli_TipoDoc char(2),
@PProgra_Appli_Filtra_Serie BIT,
@PProgra_Appli_Serie char(4)=NULL,
@PProgra_EsResumenBoletas BIT,
@PProgra_EsComunicacionBaja BIT,
@PProgra_FrecuEnvio_Hora_Es_Establecido BIT,
@PProgra_FrecuEnvio_Hora_Es_XCadaHora BIT,
@PProgra_FrecuEnvio_Hora_Es_DespuesEmitido BIT,
@PProgra_FrecuEnvio_Hora_Es_DespuesConfirmado BIT,
@PProgra_FrecuEnvio_Hora_Desde TIME,
@PProgra_FrecuEnvio_Hora_Hasta TIME,
@PProgra_FrecuEnvio_Hora_XCadaHora INT,

@PProgra_Fecha_Desde DATE,
@PProgra_Fecha_SinFin BIT,
@PProgra_Fecha_Hasta DATE=NULL
)
AS
BEGIN
	SET @PProgra_Correlativo = (ISNULL((SELECT MAX(Prog.Progra_Correlativo) FROM dbo.FACTE_PROGRAMACIONES_ENVIO Prog WITH(NOLOCK) WHERE Prog.Emp_RUC = @PEmp_RUC),0) + 1 );
	INSERT INTO dbo.FACTE_PROGRAMACIONES_ENVIO
	        ( 
			  Emp_RUC , Progra_Correlativo , Progra_Descripcion , Progra_Habilitado ,
	          Progra_Appli_TipoDoc , Progra_Appli_Filtra_Serie , Progra_Appli_Serie ,
	          Progra_EsResumenBoletas , Progra_EsComunicacionBaja ,
	          Progra_FrecuEnvio_Hora_Es_Establecido , Progra_FrecuEnvio_Hora_Es_XCadaHora ,
	          Progra_FrecuEnvio_Hora_Es_DespuesEmitido , Progra_FrecuEnvio_Hora_Es_DespuesConfirmado ,
	          Progra_FrecuEnvio_Hora_Desde , Progra_FrecuEnvio_Hora_Hasta , Progra_FrecuEnvio_Hora_XCadaHora ,
			  Progra_Fecha_Desde,Progra_Fecha_SinFin,Progra_Fecha_Hasta,
	          Progra_Estado ,
	          Aud_Registro_FechaHora , Aud_Registro_Equipo
	        )
	VALUES  ( @PEmp_RUC , @PProgra_Correlativo , @PProgra_Descripcion , @PProgra_Habilitado ,
	          @PProgra_Appli_TipoDoc , @PProgra_Appli_Filtra_Serie , @PProgra_Appli_Serie ,
	          @PProgra_EsResumenBoletas , @PProgra_EsComunicacionBaja ,
	          @PProgra_FrecuEnvio_Hora_Es_Establecido , @PProgra_FrecuEnvio_Hora_Es_XCadaHora ,
	          @PProgra_FrecuEnvio_Hora_Es_DespuesEmitido , @PProgra_FrecuEnvio_Hora_Es_DespuesConfirmado ,
	          @PProgra_FrecuEnvio_Hora_Desde , @PProgra_FrecuEnvio_Hora_Hasta , @PProgra_FrecuEnvio_Hora_XCadaHora ,
			  @PProgra_Fecha_Desde,@PProgra_Fecha_SinFin,@PProgra_Fecha_Hasta,
	          '001' , GETDATE() , HOST_NAME()
	        )
END
GO
CREATE PROC UP_FACTE_PROGRAMACIONES_ENVIO_UPDATE
(
@PEmp_RUC char(11),
@PProgra_Correlativo INT,
@PProgra_Descripcion varchar(100),
@PProgra_Habilitado BIT,
@PProgra_Appli_TipoDoc char(2),
@PProgra_Appli_Filtra_Serie BIT,
@PProgra_Appli_Serie char(4)=NULL,
@PProgra_EsResumenBoletas BIT,
@PProgra_EsComunicacionBaja BIT,
@PProgra_FrecuEnvio_Hora_Es_Establecido BIT,
@PProgra_FrecuEnvio_Hora_Es_XCadaHora BIT,
@PProgra_FrecuEnvio_Hora_Es_DespuesEmitido BIT,
@PProgra_FrecuEnvio_Hora_Es_DespuesConfirmado BIT,
@PProgra_FrecuEnvio_Hora_Desde TIME,
@PProgra_FrecuEnvio_Hora_Hasta TIME,
@PProgra_FrecuEnvio_Hora_XCadaHora INT,

@PProgra_Fecha_Desde DATE,
@PProgra_Fecha_SinFin BIT,
@PProgra_Fecha_Hasta DATE=NULL
)
AS
BEGIN
	UPDATE dbo.FACTE_PROGRAMACIONES_ENVIO
	SET Progra_Descripcion = @PProgra_Descripcion , Progra_Habilitado = @PProgra_Habilitado,
	Progra_Appli_TipoDoc = @PProgra_Appli_TipoDoc, Progra_Appli_Filtra_Serie = @PProgra_Appli_Filtra_Serie, Progra_Appli_Serie = @PProgra_Appli_Serie,
	Progra_EsResumenBoletas = @PProgra_EsResumenBoletas, Progra_EsComunicacionBaja = @PProgra_EsComunicacionBaja,
	Progra_FrecuEnvio_Hora_Es_Establecido = @PProgra_FrecuEnvio_Hora_Es_Establecido, Progra_FrecuEnvio_Hora_Es_XCadaHora = @PProgra_FrecuEnvio_Hora_Es_XCadaHora,
	Progra_FrecuEnvio_Hora_Es_DespuesEmitido = @PProgra_FrecuEnvio_Hora_Es_DespuesEmitido, Progra_FrecuEnvio_Hora_Es_DespuesConfirmado = @PProgra_FrecuEnvio_Hora_Es_DespuesConfirmado,
	Progra_FrecuEnvio_Hora_Desde = @PProgra_FrecuEnvio_Hora_Desde, Progra_FrecuEnvio_Hora_Hasta = @PProgra_FrecuEnvio_Hora_Hasta, Progra_FrecuEnvio_Hora_XCadaHora = @PProgra_FrecuEnvio_Hora_XCadaHora,
	Progra_Fecha_Desde = @PProgra_Fecha_Desde, Progra_Fecha_SinFin = @PProgra_Fecha_SinFin, Progra_Fecha_Hasta = @PProgra_Fecha_Hasta,
	Progra_Ejecucion_Ultimo = NULL
	WHERE Emp_RUC = @PEmp_RUC AND Progra_Correlativo = @PProgra_Correlativo ;
END
GO
CREATE PROC UP_FACTE_PROGRAMACIONES_ENVIO_DELETE
(
@PEmp_RUC char(11),
@PProgra_Correlativo INT
)
AS
BEGIN
	UPDATE dbo.FACTE_PROGRAMACIONES_ENVIO
	SET Progra_Estado='002'
	WHERE Emp_RUC = @PEmp_RUC AND Progra_Correlativo = @PProgra_Correlativo ;
END
GO
CREATE PROC UP_FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO_INSERT
(
@PDoc_Emisor_RUC char(11),
@PDoc_Tipo char(2),
@PDoc_Serie char(4),
@PDoc_Numero char(8),
@PXML_Correlativo INT,
@PXML_Archivo_Item INT OUTPUT,
@PXML_Archivo_Alias VARCHAR(30),
@PXML_Archivo_Nombre varchar(150)
)
AS
BEGIN
	SET @PXML_Archivo_Item =ISNULL((SELECT MAX(Adj.XML_Archivo_Item) FROM dbo.FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO Adj 
	WHERE Adj.Doc_Emisor_RUC = @PDoc_Emisor_RUC AND Adj.Doc_Tipo = @PDoc_Tipo AND Adj.Doc_Serie = @PDoc_Serie
	AND Adj.Doc_Numero = @PDoc_Numero),0)+1;

	INSERT INTO dbo.FACTE_DOCUMENTO_ELECTRONICO_ADJUNTO
	( Doc_Emisor_RUC , Doc_Tipo , Doc_Serie , Doc_Numero ,
			  --XML_Correlativo ,
		XML_Archivo_Item ,
		XML_Archivo_NombreLocal, XML_Archivo_Nombre ,
		Aud_Registro_FechaHora , Aud_Registro_Equipo , Aud_Registro_Usuario
	)
	VALUES  
	( @PDoc_Emisor_RUC, @PDoc_Tipo, @PDoc_Serie, @PDoc_Numero,
			  --@PXML_Correlativo,
		@PXML_Archivo_Item,
		@PXML_Archivo_Alias, @PXML_Archivo_Nombre,
		GETDATE() , HOST_NAME(), 'User');			
END
GO
