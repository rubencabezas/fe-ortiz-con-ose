﻿Public Class Log_FACTE_DOCUMENTOS
    Private CapDato_Ent As New Dat_FACTE_DOCUMENTOS
    'Public Function Insertar_Documento(ByVal pEntidad As eProvisionFacturacion, pArchivo As fcProceso) As Boolean
    '    Try
    '        Return CapDato_Ent.Insertar_Documento(pEntidad, pArchivo)
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try
    'End Function
    'Public Function Buscar_Resumen_Correlativo(ByVal pFecha As Date) As Integer
    '    Try
    '        Return CapDato_Ent.Buscar_Resumen_Correlativo(pFecha)
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try
    'End Function
    Public Function Actualizar_Envio_Correo(ByVal pEntidad As DocumentoElectronico) As Boolean
        Try
            Return CapDato_Ent.Actualizar_Envio_Correo(pEntidad)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Actualizar_Envio_Correo(ByVal pEntidad As eProvisionFacturacion) As Boolean
        Try
            Return CapDato_Ent.Actualizar_Envio_Correo(pEntidad)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    'Public Function Insertar_Resumen(ByVal pEntidad As eProvisionFacturacion, pArchivo As fcProceso) As Boolean
    '    Try
    '        Return CapDato_Ent.Insertar_Resumen(pEntidad, pArchivo)
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try
    'End Function
    'Public Function Buscar_TicketPendientes_Resumen_Summary() As List(Of eTicket)
    '    Try
    '        Return CapDato_Ent.Buscar_TicketPendientes_Resumen_Summary()
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try
    'End Function
    'Public Function Buscar_TicketPendientes_ComunicacionBaja_Voided() As List(Of eTicket)
    '    Try
    '        Return CapDato_Ent.Buscar_TicketPendientes_ComunicacionBaja_Voided()
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try
    'End Function
    'Public Function Cambiar_Estado_Resumen(ByVal pEntidad As eTicket) As Boolean
    '    Try
    '        Return CapDato_Ent.Cambiar_Estado_Resumen(pEntidad)
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try
    'End Function
    'Public Function Resumen_BoletaVenta_Detalle(pEntidad As eTicket) As List(Of eProvisionFacturacion)
    '    Try
    '        Return CapDato_Ent.Resumen_BoletaVenta_Detalle(pEntidad)
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try
    'End Function
    'Public Sub Buscar_Correlativo_Comunicacion_Baja(ByRef pFechaGeneracion As Date, ByRef pCorrelativo As Integer)
    '    Try
    '        CapDato_Ent.Buscar_Correlativo_Comunicacion_Baja(pFechaGeneracion, pCorrelativo)
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try
    'End Sub
    'Public Function Insertar_Comunicacion_Baja(ByVal pDocumentos As List(Of eProvisionFacturacion), pArchivo As fcProceso) As Boolean
    '    Try
    '        Return CapDato_Ent.Insertar_Comunicacion_Baja(pDocumentos, pArchivo)
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try
    'End Function
    'Public Function Comunicacion_Baja_CambiarEstado(ByVal pEntidad As eTicket) As Boolean
    '    Try
    '        Return CapDato_Ent.Comunicacion_Baja_CambiarEstado(pEntidad)
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try
    'End Function
    'Public Function Comunicacion_Baja_Lista_Documentos(pEntidad As eTicket) As List(Of eProvisionFacturacion)
    '    Try
    '        Return CapDato_Ent.Comunicacion_Baja_Lista_Documentos(pEntidad)
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try
    'End Function
    Public Function Buscar_Documento(pEntidad As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Try
            Return CapDato_Ent.Buscar_Documento(pEntidad)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Documento_Versiones(pEntidad As eProvisionFacturacion) As DataTable
        Try
            Return CapDato_Ent.Buscar_Documento_Versiones(pEntidad)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Documento_CambiarEstado(ByVal pEntidad As eProvisionFacturacion) As Boolean
        Try
            Return CapDato_Ent.Documento_CambiarEstado(pEntidad)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Documento_CambiarEstado(ByVal pEntidad As DocumentoElectronico) As Boolean
        Try
            Return CapDato_Ent.Documento_CambiarEstado(pEntidad)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    'Public Function Buscar_Resumen_Boleta_Busqueda(pEntidad As eProvisionFacturacion) As List(Of eProvisionFacturacion)
    '    Try
    '        Return CapDato_Ent.Buscar_Resumen_Boleta_Busqueda(pEntidad)
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try
    'End Function
    Public Function Resumen_Cambiar_Estado(ByVal pEntidad As eProvisionFacturacion) As Boolean
        Try
            Return CapDato_Ent.Resumen_Cambiar_Estado(pEntidad)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Baja_Cambiar_Estado(ByVal pEntidad As eProvisionFacturacion) As Boolean
        Try
            Return CapDato_Ent.Baja_Cambiar_Estado(pEntidad)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Resumen_Boleta_Pendientes_Respuesta_Busqueda() As List(Of eProvisionFacturacion)
        Try
            Return CapDato_Ent.Buscar_Resumen_Boleta_Pendientes_Respuesta_Busqueda()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Comunicacion_Baja_Pendientes_Respuesta_Busqueda() As List(Of eProvisionFacturacion)
        Try
            Return CapDato_Ent.Buscar_Comunicacion_Baja_Pendientes_Respuesta_Busqueda()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
