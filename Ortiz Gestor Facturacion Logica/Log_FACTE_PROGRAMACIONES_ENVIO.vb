﻿Public Class Log_FACTE_PROGRAMACIONES_ENVIO
    Private cDato As New Dat_FACTE_PROGRAMACIONES_ENVIO
    Public Function Buscar_Lista(ByVal pEmp_RUC As String) As List(Of eProgramacion)
        Try
            Return cDato.Buscar_Lista(pEmp_RUC)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "Log_FACTE_PROGRAM-LIST", ex.Message)
        End Try
    End Function
    Public Function Buscar_Lista_Habilitados() As List(Of eProvisionFacturacion)
        Try
            Return cDato.Buscar_Lista_Habilitados()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "Log_FACTE_PROGRAM-LISTHabilitados", ex.Message)
        End Try
    End Function
    Public Function Insertar(ByVal pEntidad As eProgramacion) As Boolean
        Try
            cDato.Insertar(pEntidad)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "Log_FACTE_PROGRAM-INS", ex.Message)
        End Try
        Return True
    End Function
    Public Function Actualizar(ByVal pEntidad As eProgramacion) As Boolean
        Try
            cDato.Actualizar(pEntidad)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "Log_FACTE_PROGRAM-UPD", ex.Message)
        End Try
        Return True
    End Function
    Public Function Eliminar(ByVal pEntidad As eProgramacion) As Boolean
        Try
            cDato.Eliminar(pEntidad)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "Log_FACTE_PROGRAM-DEL", ex.Message)
        End Try
        Return True
    End Function
    Public Function Buscar_Lista_Pendientes_Resumen() As List(Of eProvisionFacturacion)
        Try
            Return cDato.Buscar_Lista_Pendientes_Resumen()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "Log_FACTE_PROGRAM-LISTHabilitados", ex.Message)
        End Try
    End Function
    Public Function Buscar_Lista_Pendientes_Resumen_Documento(pResumen As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Try
            Return cDato.Buscar_Lista_Pendientes_Resumen_Documento(pResumen)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "Log_FACTE_PROGRAM-LISTHabilitados", ex.Message)
        End Try
    End Function
    Public Function Buscar_Lista_Pendientes_Baja() As List(Of eProvisionFacturacion)
        Try
            Return cDato.Buscar_Lista_Pendientes_Baja()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "Log_FACTE_PROGRAM-LISTHabilitados", ex.Message)
        End Try
    End Function
    Public Function Buscar_Lista_Pendientes_Baja_Documento(pResumen As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Try
            Return cDato.Buscar_Lista_Pendientes_Baja_Documento(pResumen)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "Log_FACTE_PROGRAM-LISTHabilitados", ex.Message)
        End Try
    End Function
End Class
