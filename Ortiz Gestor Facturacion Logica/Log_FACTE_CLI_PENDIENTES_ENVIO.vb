﻿Public Class Log_FACTE_CLI_PENDIENTES_ENVIO
    Private CapDato_Ent As New Dat_FACTE_CLI_PENDIENTES_ENVIO
    Public Function Buscar_DocVenta(ByVal pTarea As eProgramacion) As List(Of eProvisionFacturacion)
        Try
            Return CapDato_Ent.Buscar_DocVenta(pTarea)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_DocDetalle(ByVal Cls_Enti As eProvisionFacturacion) As List(Of eProvisionFacturacionDetalle)
        Try
            Return CapDato_Ent.Buscar_DocDetalle(Cls_Enti)
        Catch ex As Exception
            'Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CLI-PEN-DOCDETALLE", ex.Message)
        End Try
    End Function
    Public Function Buscar_DocVentaDocumentoRelacionado(ByVal Cls_Enti As eProvisionFacturacion) As List(Of eProvisionFacturacionReferenciaDocumento)
        Try
            Return CapDato_Ent.Buscar_DocVentaDocumentoRelacionado(Cls_Enti)
        Catch ex As Exception
            'Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CLI-PEN-DOCDETALLE", ex.Message)
        End Try
    End Function
    Public Function Cambiar_EstadoDocumentoPendiente(ByVal Cls_Enti As eProvisionFacturacion) As Boolean
        Try
            Return CapDato_Ent.Cambiar_EstadoDocumentoPendiente(Cls_Enti)
        Catch ex As Exception
            'Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CLI-PEN-DOCCAMBEST", ex.Message)
        End Try
    End Function
    Public Function Cambiar_EstadoDocumentoPendiente_OtrosCPE(ByVal Cls_Enti As eProvisionFacturacion) As Boolean
        Try
            Return CapDato_Ent.Cambiar_EstadoDocumentoPendiente_OtrosCPE(Cls_Enti)
        Catch ex As Exception
            'Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CLI-PEN-DOCCAMBEST", ex.Message)
        End Try
    End Function
    Public Function Buscar_BoletasVenta_Resumen_Pendientes(ByVal pTarea As eProgramacion) As List(Of eProvisionFacturacion)
        Try
            Return CapDato_Ent.Buscar_BoletasVenta_Resumen_Pendientes(pTarea)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Obtener_Resumen_BoletaVenta(ByVal pPendiente As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Try
            Return CapDato_Ent.Obtener_Resumen_BoletaVenta(pPendiente)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Cambiar_EstadoDocumentoPendiente_Resumen(ByVal Cls_Enti As eProvisionFacturacion) As Boolean
        Try
            Return CapDato_Ent.Cambiar_EstadoDocumentoPendiente_Resumen(Cls_Enti)
        Catch ex As Exception
            'Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CLI-PEN-RESUCAMBEST", ex.Message)
        End Try
    End Function
    Public Function Resumen_BoletaVenta_Ticket_CambiarEstado(ByVal Cls_Enti As eProvisionFacturacion) As Boolean
        Try
            Return CapDato_Ent.Resumen_BoletaVenta_Ticket_CambiarEstado(Cls_Enti)
        Catch ex As Exception
            'Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CLI-PEN-RESUCAMBEST", ex.Message)
        End Try
    End Function
    Public Function Buscar_Retencion_Detalles(ByVal Cls_Enti As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Try
            Return CapDato_Ent.Buscar_Retencion_Detalles(Cls_Enti)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Retencion_ListaPendientes() As List(Of eProvisionFacturacion)
        Try
            Return CapDato_Ent.Buscar_Retencion_ListaPendientes()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Percepcion_DocVenta(ByVal Cls_Enti As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Try
            Return CapDato_Ent.Buscar_Percepcion_DocVenta(Cls_Enti)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    '-------------
    Public Function Actualizar_DatosReceptor(ByVal Cls_Enti As eReceptor) As Boolean
        Try
            Return CapDato_Ent.Actualizar_DatosReceptor(Cls_Enti)
        Catch ex As Exception
            'Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_BoletaVentaFactura_Pendientes_Baja(pTarea As eProgramacion) As List(Of eProvisionFacturacion)
        Try
            Return CapDato_Ent.Buscar_BoletaVentaFactura_Pendientes_Baja(pTarea)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_BoletaVentaFactura_Pendientes_Documentos_Baja(pEntidad As eProvisionFacturacion) As List(Of eProvisionFacturacion)
        Try
            Return CapDato_Ent.Buscar_BoletaVentaFactura_Pendientes_Documentos_Baja(pEntidad)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Cambiar_EstadoDocumentoPendiente_Baja(ByVal pDocumentos As List(Of eProvisionFacturacion)) As Boolean
        Try
            Return CapDato_Ent.Cambiar_EstadoDocumentoPendiente_Baja(pDocumentos)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Cambiar_EstadoDocumentoPendiente_Baja_Ticket(ByVal pDocumentos As List(Of eProvisionFacturacion)) As Boolean
        Try
            Return CapDato_Ent.Cambiar_EstadoDocumentoPendiente_Baja_Ticket(pDocumentos)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
