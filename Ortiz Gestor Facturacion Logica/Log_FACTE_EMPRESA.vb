﻿Public Class Log_FACTE_EMPRESA
    Private cDato As New Dat_FACTE_EMPRESA
    Public Sub Buscar_Configuracion(ByVal pEntidad As eEmpresa)
        Try
            cDato.Buscar_Configuracion(pEntidad)
        Catch ex As Exception
            'Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "Log_FACTE_EMPRESA-CONF", ex.Message)
        End Try
    End Sub
    Public Function Buscar_Empresas_Envio_Resumenes() As String
        Try
            Return cDato.Buscar_Empresas_Envio_Resumenes()
        Catch ex As Exception
            'Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "Log_FACTE_EMPRESA-RESUM", ex.Message)
        End Try
    End Function
    Public Function Insertar(ByVal pEntidad As eEmpresa) As Boolean
        Try
            cDato.Insertar(pEntidad)
        Catch ex As Exception
            'Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "Log_FACTE_EMPRESA-INS", ex.Message)
        End Try
        Return True
    End Function
    Public Function Actualizar(ByVal pEntidad As eEmpresa) As Boolean
        Try
            cDato.Actualizar(pEntidad)
        Catch ex As Exception
            'Throw New Exception(ex.Message)
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "Log_FACTE_EMPRESA-INS", ex.Message)
        End Try
        Return True
    End Function
End Class
