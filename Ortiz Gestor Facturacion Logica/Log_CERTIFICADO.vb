﻿Public Class Log_CERTIFICADO
    Private cDato As New Dat_CERTIFICADO
    Public Function Buscar(ByVal pEmp_RUC As String, pFechaEmision As Date, pTipDoc As String) As eCertificadoCredencial
        Try
            Return cDato.Buscar(pEmp_RUC, pFechaEmision, pTipDoc)
        Catch ex As Exception
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CERTBUSQ1", ex.Message)
            'Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar(ByVal pEmp_RUC As String) As eCertificadoCredencial
        Try
            Return cDato.Buscar(pEmp_RUC)
        Catch ex As Exception
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CERTBUSQ2", ex.Message)
            'Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_Lista(ByVal pEmp_RUC As String) As List(Of eCertificadoCredencial)
        Try
            Return cDato.Buscar_Lista(pEmp_RUC)
        Catch ex As Exception
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CERTBUSQ3", ex.Message)
            'Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Insertar(ByVal pEntidad As eCertificadoCredencial) As Boolean
        Try
            cDato.Insertar(pEntidad)
            Return True
        Catch ex As Exception
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CERTBUSQ4", ex.Message)
            'Throw New Exception(ex.Message)
            Return False
        End Try
    End Function
    Public Function Actualizar(ByVal pEntidad As eCertificadoCredencial) As Boolean
        Try
            cDato.Actualizar(pEntidad)
            Return True
        Catch ex As Exception
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CERTBUSQ5", ex.Message)
            'Throw New Exception(ex.Message)
            Return False
        End Try
    End Function
    Public Function Eliminar(ByVal pEntidad As eCertificadoCredencial) As Boolean
        Try
            cDato.Eliminar(pEntidad)
            Return True
        Catch ex As Exception
            log.Instancia.escribirLinea(log.tipoLogEnum.Info__, "CERTBUSQ6", ex.Message)
            'Throw New Exception(ex.Message)
            Return False
        End Try
    End Function
End Class
