﻿Public Class Log_VTS_ENTIDAD
    Private CapDato_Ent As New Dat_VTS_ENTIDAD
   
#Region "Direccion"
   

    Public Function Buscar(ByVal Cls_Enti As Ent_ENTIDAD_DIRECCION) As List(Of Ent_ENTIDAD_DIRECCION)
        Try
            Return CapDato_Ent.Buscar(Cls_Enti)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
   
#End Region
End Class
