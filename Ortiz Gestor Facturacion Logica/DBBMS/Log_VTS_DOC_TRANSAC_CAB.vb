﻿Public Class Log_VTS_DOC_TRANSAC_CAB
    Private CapDato_Ent As New Dat_VTS_DOC_TRANSAC_CAB
    Public Function Buscar_DocVenta(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB) As List(Of Ent_DOC_TRANSAC_CAB)
        Try
            Return CapDato_Ent.Buscar_DocVenta(Cls_Enti)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_NotaDebito(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB) As List(Of Ent_DOC_TRANSAC_CAB)
        Try
            Return CapDato_Ent.Buscar_NotaDebito(Cls_Enti)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Buscar_NotaCredito(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB) As List(Of Ent_DOC_TRANSAC_CAB)
        Try
            Return CapDato_Ent.Buscar_NotaCredito(Cls_Enti)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function Buscar_Detalle_DocVenta(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB) As List(Of Ent_DOC_VENTA_DET)
        Try
            Return CapDato_Ent.Buscar_Detalle_DocVenta(Cls_Enti)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function Buscar_Detalle_MediosPago(ByVal Cls_Enti As Ent_DOC_TRANSAC_CAB) As List(Of Ent_DOC_VENTA_DET)
        Try
            Return CapDato_Ent.Buscar_Detalle_MediosPago(Cls_Enti)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
