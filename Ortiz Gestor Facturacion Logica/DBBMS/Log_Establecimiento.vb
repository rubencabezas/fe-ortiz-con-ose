﻿Public Class Log_Establecimiento
    Private CapDato_Ent As New Dat_Establecimiento

    Public Function Buscar(ByVal Cls_Enti As Ent_Establecimiento) As List(Of Ent_Establecimiento)
        Try
            Return CapDato_Ent.Buscar(Cls_Enti)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function Buscar_Direccion(ByVal Cls_enti As Ent_Establecimiento) As List(Of Ent_Establecimiento)
        Try
            Return CapDato_Ent.Buscar_Direccion(Cls_enti)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

End Class
